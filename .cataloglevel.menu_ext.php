<?
use Bitrix\Main\Loader;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
$aMenuLinksExt = [];

if(Loader::includeModule("iblock"))
{

    $dbIBlock = CIBlock::GetList(
        [
            'SORT' => 'ASC',
            'ID' => 'ASC'
        ],
        [
            "TYPE" => "marketplace",
            "SITE_ID" => SITE_ID,
            "ID" => \Bitrix\Main\Config\Option::get("studio7spb.marketplace", "catalog_iblock_id")
        ]
    );


    $dbIBlock = new CIBlockResult($dbIBlock);

    if ($arIBlock = $dbIBlock->GetNext())
    {
        if(defined("BX_COMP_MANAGED_CACHE"))
            $GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_".$arIBlock["ID"]);

        if($arIBlock["ACTIVE"] == "Y")
        {
            $aMenuLinksExt = $APPLICATION->IncludeComponent("studio7sbp:menu.sections", "bootstrap_v4", array(
                "IS_SEF" => "Y",
                "SEF_BASE_URL" => "",
                "SECTION_PAGE_URL" => $arIBlock['SECTION_PAGE_URL'],
                "DETAIL_PAGE_URL" => $arIBlock['DETAIL_PAGE_URL'],
                "IBLOCK_TYPE" => $arIBlock['IBLOCK_TYPE_ID'],
                "IBLOCK_ID" => $arIBlock['ID'],
                "DEPTH_LEVEL" => "3",
                "CACHE_TYPE" => "N",
            ), false, Array('HIDE_ICONS' => 'Y'));
        }
    }

    if(defined("BX_COMP_MANAGED_CACHE"))
        $GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_new");
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);