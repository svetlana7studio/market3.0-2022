<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сотрудничество");
if($USER->IsAuthorized())
    LocalRedirect(SITE_DIR.'seller/');
?>
<div class="s7spb-container pb4">
    <div class="p3">
        <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "",
            Array(
                "PATH" => "",
                "SITE_ID" => SITE_ID,
                "START_FROM" => "0"
            ),
            false
        );?>
    </div>

    <?$APPLICATION->IncludeComponent("studio7sbp:seller.register","",false );?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>