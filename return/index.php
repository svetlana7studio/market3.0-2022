<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Возврат");
?>
    <div class="s7spb-container py4">
        <div class="p3">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "",
                Array(
                    "PATH" => "",
                    "SITE_ID" => SITE_ID,
                    "START_FROM" => "0"
                )
            );?>
        </div>

        <div class="bg-white p4 py3 mb3">
            <section>
                <h2>Какие товары можно вернуть?</h2>
                <p>
                    Можно вернуть любые товары, кроме:
                </p>
                <ul>
                    <li>
                        <p>
                            товаров, которые&nbsp;не&nbsp;подлежат возврату;
                        </p>
                    </li>
                    <li>
                        <p>
                            алкоголя;
                        </p>
                    </li>
                    <li>
                        <p>
                            товаров надлежащего качества (без брака) со&nbsp;следами использования, с&nbsp;нарушенной комплектацией, товарным видом или упаковкой&nbsp;— по&nbsp;правилам дистанционной торговли.
                        </p>
                        <br>
                    </li>
                </ul>
            </section><section>
                <h2>В какие сроки можно вернуть товар?</h2>
                <p>Товары ненадлежащего качества (с недостатками)</p>

                <p>
                    Если в&nbsp;купленном товаре есть недостатки, его&nbsp;по&nbsp;закону&nbsp;можно вернуть в&nbsp;течение гарантийного срока или срока годности.
                </p>
                <p>Примечание.&nbsp;Первые 14&nbsp;дней (для&nbsp;технически сложных товаров&nbsp;— 15&nbsp;дней) с&nbsp;момента покупки всеми возвратами с&nbsp;недостатками занимается Маркетплейс. После этого товар нужно возвращать компании, которая его предоставила: это может быть Маркетплейс или один из&nbsp;его партнеров. Когда вы будете оформлять возврат, служба поддержки подскажет вам, к&nbsp;кому обращаться.<br></p>
                <br>

                <p>Товары надлежащего качества (не понравились или не подошли)</p>
                <p>
                    Если с&nbsp;товаром все нормально, просто вы хотите его вернуть, то&nbsp;по&nbsp;закону&nbsp;это можно сделать в&nbsp;течение 14&nbsp;дней.
                </p>
            </section>
        </div>


        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            Array(
                "AREA_FILE_RECURSIVE" => "Y",
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "",
                "CART" => $componentElementParams["CART"],
                "COMPONENT_TEMPLATE" => ".default",
                "EDIT_TEMPLATE" => "standard.php",
                "PATH" => SITE_DIR."include/top.user.view.products.php"
            ),
            false,
            Array(
                'HIDE_ICONS' => 'Y'
            )
        );?>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>