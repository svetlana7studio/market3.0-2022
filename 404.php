<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("title", "404 Not Found");
$APPLICATION->SetTitle("404 Not Found");

$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "catalog.search.form",
    [
        "COMPONENT_TEMPLATE" => "cataloglevel",
        "ROOT_MENU_TYPE" => "catalog.level_1",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "36000000",
        "MENU_CACHE_USE_GROUPS" => "N",
        "MENU_CACHE_GET_VARS" => [],
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "catalog.level_1",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N"
    ],
    false,
    ["HIDE_ICONS" => "Y"]
);

?>


    <div class="s7spb-container py4">

        <div class="bg-white p4 py3 mb3">
            <h1 style="font-size: 30px; text-align: center; margin: 130px 0 130px">404 Not Found</h1>

            <?$APPLICATION->IncludeComponent("bitrix:main.map", ".default", Array(
                "LEVEL"	=>	"3",
                "COL_NUM"	=>	"2",
                "SHOW_DESCRIPTION"	=>	"Y",
                "SET_TITLE"	=>	"Y",
                "CACHE_TIME"	=>	"36000000"
                )
            );?>

        </div>

    </div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>