<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
$APPLICATION->SetPageProperty('SectionClass', "s7sbp--marketplace--index--catalog");
$APPLICATION->SetPageProperty('SectionClassRight', "s7sbp--marketplace--right--catalog");
$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "catalog.search.form",
    [
        "COMPONENT_TEMPLATE" => "cataloglevel",
        "ROOT_MENU_TYPE" => "catalog.level_1",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "36000000",
        "MENU_CACHE_USE_GROUPS" => "N",
        "MENU_CACHE_GET_VARS" => [],
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "catalog.level_1",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N"
    ],
    false,
    ["HIDE_ICONS" => "Y"]
); 



?>
    <section class="s7spb-container">

        <div class="mb3 px3">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "",
                Array(
                    "PATH" => "",
                    "SITE_ID" => SITE_ID,
                    "START_FROM" => "0",
                    "SEF_CATALOG_CODE_POINT" => "catalog",
                    "IBLOCK_TYPE" => "marketplace",
                    "IBLOCK_ID" => "2",
                ),
                false
            );?>
        </div>

        <div class="s7spb-row s7spb-row12">

            <div class="s7spb-col s7spb-col-12 s7spb-col-lg-auto bg-white w-min-240">

                <div class="mb3">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                        array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "PATH" => "/include/left_block/menu.left_menu.php",
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "",
                            "AREA_FILE_RECURSIVE" => "Y",
                            "EDIT_TEMPLATE" => "standard.php"
                        ),
                        false
                    );?>
                </div>

                <div class="mb3">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => "inc",
                            "AREA_FILE_RECURSIVE" => "Y",
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    );?>
                </div>

            </div>
            <div class="s7spb-col s7spb-col-12 s7spb-col-lg bg-white">

                <?$APPLICATION->IncludeComponent(
                    "studio7sbp:buyer.personal.section",
                    "",
                    Array(
                        "ACCOUNT_PAYMENT_SELL_USER_INPUT" => "Y",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_TYPE" => "A",
                        "CHECK_RIGHTS_PRIVATE" => "N",
                        "COMPATIBLE_LOCATION_MODE_PROFILE" => "N",
                        "CUSTOM_PAGES" => "",
                        "CUSTOM_SELECT_PROPS" => array(""),
                        "MAIN_CHAIN_NAME" => "Мой кабинет",
                        "NAV_TEMPLATE" => "",
                        "ORDERS_PER_PAGE" => "20",
                        "ORDER_DEFAULT_SORT" => "STATUS",
                        "ORDER_DISALLOW_CANCEL" => "N",
                        "ORDER_HIDE_USER_INFO" => array("0"),
                        "ORDER_HISTORIC_STATUSES" => array("F"),
                        "ORDER_REFRESH_PRICES" => "N",
                        "ORDER_RESTRICT_CHANGE_PAYSYSTEM" => array("0"),
                        "PATH_TO_BASKET" => "/basket/",
                        "PATH_TO_CATALOG" => "/catalog/",
                        "PATH_TO_CONTACT" => "/about/contacts/",
                        "PATH_TO_PAYMENT" => "/personal/orders/payment",
                        "PROFILES_PER_PAGE" => "20",
                        "SAVE_IN_SESSION" => "Y",
                        "SEF_MODE" => "Y",
                        "SEF_FOLDER" => "/personal/",
                        "SEND_INFO_PRIVATE" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_ACCOUNT_PAGE" => "Y",
                        "SHOW_BASKET_PAGE" => "Y",
                        "SHOW_CONTACT_PAGE" => "Y",
                        "SHOW_ORDER_PAGE" => "Y",
                        "SHOW_PRIVATE_PAGE" => "Y",
                        "SHOW_PROFILE_PAGE" => "Y",
                        "SHOW_SUBSCRIBE_PAGE" => "Y",
                        "USE_AJAX_LOCATIONS_PROFILE" => "N"
                    )
                );?>

            </div>

        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>