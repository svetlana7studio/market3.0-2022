<?
/**
 * @var CMain $APPLICATION
 */

use Bitrix\Main\Page\Asset;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/marketplace/module.authorization.css");
Asset::getInstance()->addCss( SITE_TEMPLATE_PATH . "/assets/css/marketplace/styles.css");

$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"catalog.search.form",
	[
		"COMPONENT_TEMPLATE" => "cataloglevel",
		"ROOT_MENU_TYPE" => "catalog.level_1",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "N",
		"MENU_CACHE_GET_VARS" => [],
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "catalog.level_1",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	],
	false,
	["HIDE_ICONS" => "Y"]
);
?>
<?if(!$USER->IsAuthorized()){?>

	<div class="s7spb-container py4">

		<div class="s7spb-row s7spb-content-center py3">

			<div class="s7spb-col-auto w-min-320 bg-white">
				<?$APPLICATION->IncludeComponent(
					"bitrix:system.auth.form",
					"main",
					Array(
						"REGISTER_URL" => SITE_DIR."auth/registration/",
						"PROFILE_URL" => SITE_DIR."auth/forgot-password/",
						"SHOW_ERRORS" => "Y"
					)
				);?>
			</div>

		</div>

	</div>

<?}
elseif( !empty( $_REQUEST["backurl"] ) ){
	LocalRedirect( $_REQUEST["backurl"] );
}else{
	LocalRedirect(SITE_DIR.'personal/');
}?>

<?


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>