<?

use Bitrix\Main\Page\Asset;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/marketplace/module.authorization.css");
Asset::getInstance()->addCss( SITE_TEMPLATE_PATH . "/assets/css/marketplace/styles.css");
// mode=register

if(!$USER->IsAuthorized()) {
    ?>
    <div class="s7spb-container pb4">
        <div class="p3">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "",
                Array(
                    "PATH" => "",
                    "SITE_ID" => SITE_ID,
                    "START_FROM" => "0"
                ),
                false
            );?>
        </div>

        <div class="s7spb-row s7spb-content-center py3 bg-white">

            <div class="webcomtorg__form-btn text-center mb5">
                <a href="/partner/" class="btn btn-danger px4">Зарегистрироваться как продавец</a>
                или
                <a href="/auth/registration/?mode=register" class="btn btn-danger px4">Зарегистрироваться как покупатель</a>
            </div>

            <div class="s7spb-col-auto w-min-320">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.register",
                    "main",
                    Array(
                        "USER_PROPERTY_NAME" => "",
                        "SHOW_FIELDS" => array("NAME", "SECOND_NAME", "LAST_NAME", "EMAIL","PERSONAL_PHONE"),
                        "REQUIRED_FIELDS" => array("NAME","EMAIL", "PERSONAL_PHONE"),
                        "AUTH" => "Y",
                        "USE_BACKURL" => "Y",
                        "SUCCESS_PAGE" => "",
                        "SET_TITLE" => "N",
                        "USER_PROPERTY" => array()
                    )
                );?>
            </div>

        </div>

    </div>

    <?
    $_REQUEST["REGISTER[LOGIN]"] = $_REQUEST["REGISTER[EMAIL]"];
} elseif(!empty( $_REQUEST["backurl"] )) {
    LocalRedirect( $_REQUEST["backurl"] );
} else {
    LocalRedirect(SITE_DIR.'personal/');
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>