<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Результат оплаты");
?>

    <div class="s7spb-container py4">
        <div class="p3">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "",
                Array(
                    "PATH" => "",
                    "SITE_ID" => SITE_ID,
                    "START_FROM" => "0"
                )
            );?>
        </div>
        <div class="bg-white p4 py3 mb3">

            <?$APPLICATION->IncludeComponent(
                "bitrix:sale.order.payment.receive",
                "",
                Array(
                    "PAY_SYSTEM_ID" => "",
                    "PERSON_TYPE_ID" => ""
                ),
                false
            );?>

        </div>


    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>