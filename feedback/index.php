<?
use Bitrix\Main\Page\Asset;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/marketplace/module.authorization.css");
$APPLICATION->SetTitle("Авторизация");
?>

<div class="s7spb-container">

    <div class="p1">
        <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "",
            Array(
                "PATH" => "",
                "SITE_ID" => SITE_ID,
                "START_FROM" => "0"
            )
        );?>
    </div>

    <div class="s7spb-row s7spb-row12 mb3">

        <div class="s7spb-col s7spb-col-12 s7spb-col-lg-auto bg-white py4">

            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "cataloglevel",
                array(
                    "COMPONENT_TEMPLATE" => "cataloglevel",
                    "ROOT_MENU_TYPE" => "cataloglevel",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "36000000",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "4",
                    "CHILD_MENU_TYPE" => "cataloglevel",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            );?>

        </div>
        <div class="s7spb-col s7spb-col-12 s7spb-col-lg bg-white">

            <div class="p3">
                <h1>Форма обратной связи</h1>
                <?$APPLICATION->IncludeComponent("bitrix:main.feedback", "bootstrap_v4", Array(
                    "EMAIL_TO" => "sfsdf@ddd.rr",	// E-mail, на который будет отправлено письмо
                    "EVENT_MESSAGE_ID" => "",	// Почтовые шаблоны для отправки письма
                    "OK_TEXT" => "Спасибо, ваше сообщение принято.",	// Сообщение, выводимое пользователю после отправки
                    "REQUIRED_FIELDS" => "",	// Обязательные поля для заполнения
                    "USE_CAPTCHA" => "Y",	// Использовать защиту от автоматических сообщений (CAPTCHA) для неавторизованных пользователей
                ),
                    false
                );?>
            </div>

        </div>

    </div>



    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        ".default",
        Array(
            "AREA_FILE_RECURSIVE" => "Y",
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "",
            //"CART" => $componentElementParams["CART"],
            "COMPONENT_TEMPLATE" => ".default",
            "EDIT_TEMPLATE" => "standard.php",
            "PATH" => SITE_DIR."include/catalog.top/view.products.php"
        ),
        false,
        Array(
            'HIDE_ICONS' => 'Y'
        )
    );?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>


