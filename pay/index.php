<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата и доставка");
$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "catalog.search.form",
    [
        "COMPONENT_TEMPLATE" => "cataloglevel",
        "ROOT_MENU_TYPE" => "catalog.level_1",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "36000000",
        "MENU_CACHE_USE_GROUPS" => "N",
        "MENU_CACHE_GET_VARS" => [],
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "catalog.level_1",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N"
    ],
    false,
    ["HIDE_ICONS" => "Y"]
);

?><div class="s7spb-container py4">
    <div class="p3">
        <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "",
            Array(
                "PATH" => "",
                "SITE_ID" => SITE_ID,
                "START_FROM" => "0"
            )
        );?>
    </div>
    <div class="bg-white p4 py3 mb3">
        <h1>Оплата и доставка</h1>
        <div class="pb4">
        <p style="color: #333333;">
            Получить товар вы можете следующими способами:<br>
        </p>
        <h2 style="font-weight: 500; color: #333333;">1. Самовывоз</h2>
        <p style="color: #333333;">
            Самовывоз&nbsp;товара из пункта выдачи бесплатно, осуществляется по адресу:&nbsp;
        </p>
        <p style="color: #333333;">
            г. Санкт-Петербург, наб. Обводного кан., 136, корп. 71
        </p>
        <p style="color: #333333;">
            <b>Часы работы:</b>
        </p>
        <p style="color: #333333;">
            <b>Пн - Пт</b>: с 10:00 до 20:00
        </p>
        <p style="color: #333333;">
            <b>Суббота, Воскресенье</b><b>:</b>&nbsp;выходной
        </p>
        <p style="color: #333333;">
            Телефон:&nbsp;<b>+7 (812) 909-05-05</b><br>
        </p>
        <h2 style="font-weight: 500;">2. Доставка в пункт выдачи</h2>
        <p style="color: #333333;">
        </p>
        <p style="color: #333333;">
            Время и дата доставки согласовывается с менеджером интернет-магазина, который обязательно свяжется с вами после размещения&nbsp;заказа.
        </p>
        <span style="color: #333333;"> </span><br>
        <p style="color: #333333;">
            Часы работы пунктов выдачи: 9.00-22.00, 7 дней в неделю
        </p>
        <p style="color: #333333;">
        </p>
        <h2 style="font-weight: 500;">3. Доставка на дом курьером</h2>
        <p style="color: #333333;">
        </p>
        <p style="color: #333333;">
            Доставка осуществляется по адресу, указанному при оформлении заказа. Если необходимо доставить товар по иному адресу, необходимо сообщить адрес менеджеру интернет-магазина, который свяжется с вами непосредственно после оформления заказа на сайте.
        </p>
        <h2 style="font-weight: 500; color: #333333;">Правила доставки</h2>
        <p style="color: #333333;">
            <b>Внимание!&nbsp;</b>Неправильно указанный номер телефона, неточный или неполный адрес могут привести к дополнительной задержке! Пожалуйста, внимательно проверяйте ваши персональные данные при регистрации и оформлении заказа.Конфиденциальность ваших регистрационных данных гарантируется.<br>
        </p>
        <p style="color: #333333;">
            При доставке вам будут переданы все необходимые документы на покупку.
        </p>
        <p style="color: #333333;">
            <b>Внимание!</b>&nbsp;Просим вас помнить, что все потребительские свойства приобретаемого товара вам следует уточнять у нашего менеджера до момента покупки товара. В обязанности работников Службы доставки не входит осуществление консультаций и комментариев относительно потребительских свойств товара.&nbsp;
        </p>
        <p style="color: #333333;">
            При доставке вам заказанного товара проверяйте комплектность доставленного товара, соответствие доставленного товара описанию на нашем сайте, также проверьте товар на наличие дефектов. При не заявлении вами при получении товара претензий по поводу повреждений, в дальнейшем подобные претензии не рассматриваются. В случае вопросов и пожеланий обращайтесь к нам по следующим координатам:
        </p>
        <p style="color: #333333;">
            Телефон:<b>&nbsp;</b><b>&nbsp;+7 (812)&nbsp;</b><b>909-05-05</b>
        </p>
        <p style="color: #333333;">
            Электронная почта:&nbsp;<a href="mailto:info@market.ru" target="_blank">info@market.ru</a>
        </p>
    </div>

    </div>

    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        ".default",
        Array(
            "AREA_FILE_RECURSIVE" => "Y",
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "",
            "CART" => $componentElementParams["CART"],
            "COMPONENT_TEMPLATE" => ".default",
            "EDIT_TEMPLATE" => "standard.php",
            "PATH" => SITE_DIR."include/top.user.view.products.php"
        ),
        false,
        Array(
            'HIDE_ICONS' => 'Y'
        )
    );?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>