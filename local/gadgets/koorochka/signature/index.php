<?
/**
 * Powered by Artem Koorochka
 * artem@koorochka.com
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$APPLICATION->SetAdditionalCSS('/local/gadgets/koorochka/signature/styles.css');
?>
<div class="bx-gadgets-content-layout-koorochka">
    <div class="bx-gadgets-title"><?=Loc::getMessage("KOOROCHKA_SIGNATURE_DESC")?></div>
    <div class="bx-gadget-bottom-cont">
        <a class="bx-gadget-button" target="_blank" href="https://7sait.spb.ru">
            <div class="bx-gadget-button-lamp"></div>
            <div class="bx-gadget-button-text"><?=Loc::getMessage("KOOROCHKA_SIGNATURE_VIEW")?></div>
        </a>
        <div class="bx-gadget-mark">
            <?=Loc::getMessage("KOOROCHKA_SIGNATURE_NAME")?> <span  id="icon-artem"></span>
            <div class="bx-gadget-mark-desc"><?=Loc::getMessage("KOOROCHKA_SIGNATURE_DESC2")?></div>
        </div>
    </div>
</div>
<div class="bx-gadget-shield"></div>