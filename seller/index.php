<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CJSCore::Init("jquery");
$APPLICATION->SetTitle("Личный кабинет");
$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"catalog.search.form",
	[
		"COMPONENT_TEMPLATE" => "cataloglevel",
		"ROOT_MENU_TYPE" => "catalog.level_1",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "N",
		"MENU_CACHE_GET_VARS" => [],
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "catalog.level_1",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
	],
	false,
	["HIDE_ICONS" => "Y"]
);
?>

	<section class="s7spb-container">

		<div class="mb3 px3">
			<?$APPLICATION->IncludeComponent(
				"bitrix:breadcrumb",
				"",
				Array(
					"PATH" => "",
					"SITE_ID" => SITE_ID,
					"START_FROM" => "0",
					"SEF_CATALOG_CODE_POINT" => "catalog",
					"IBLOCK_TYPE" => "marketplace",
					"IBLOCK_ID" => "2",
				),
				false
			);?>
		</div>

		<div class="s7spb-row s7spb-row12">

			<div class="s7spb-col s7spb-col-12 s7spb-col-lg-auto bg-white w-min-240">

				<div class="mb3">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
						array(
							"COMPONENT_TEMPLATE" => ".default",
							"PATH" => "/include/left_block/menu.left_menu.php",
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "",
							"AREA_FILE_RECURSIVE" => "Y",
							"EDIT_TEMPLATE" => "standard.php"
						),
						false
					);?>
				</div>

				<div class="mb3">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						".default",
						array(
							"COMPONENT_TEMPLATE" => ".default",
							"AREA_FILE_SHOW" => "sect",
							"AREA_FILE_SUFFIX" => "inc",
							"AREA_FILE_RECURSIVE" => "Y",
							"EDIT_TEMPLATE" => ""
						),
						false
					);?>
				</div>

			</div>
			<div class="s7spb-col s7spb-col-12 s7spb-col-lg bg-white">
			<div class="auth--seller">
				<?
				if($USER->IsAuthorized()):

					$APPLICATION->IncludeComponent(
						"studio7sbp:seller.personal.section",
						"",
						array(
							//"catalog_iblock_id" => $moduleOptions["catalog_iblock_id"],
							"catalog_iblock_id" => 2,
							"SEF_FOLDER" => "/seller/",
							"SEF_MODE" => "Y",
							"SEF_URL_TEMPLATES" => array(
							)
						),
						false,
						array('HIDE_ICONS' => 'Y')
					);
				else:
					$APPLICATION->IncludeComponent(
						"bitrix:system.auth.form",
						"main",
						Array(
							"REGISTER_URL" => SITE_DIR."auth/registration/",
							"PROFILE_URL" => SITE_DIR."auth/forgot-password/",
							"SHOW_ERRORS" => "Y"
						)
					);
				endif;
				?>
			</div>
			</div>

		</div>
	</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>