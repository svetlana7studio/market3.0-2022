<?
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL"] = "Личный<br>кабинет";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_HELLO"] = "Привет,";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_EXIT"] = "Выход";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_ORDERS"] = "Мои заказы";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_BASKET"] = "Мои корзины";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_ADDRESS"] = "Адреса доставки";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_FAVORITE"] = "Избранные товары";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_STORE"] = "Избранные торговые марки";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_MESSAGES"] = "Центр сообщений";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_REQUISITS"] = "Мои данные";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SETTINGS"] = "Настройки";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SETTINGS"] = "Настройки";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_TITLE"] = "Продавец";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_SHOP"] = "Данные вендора";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_COMPANY"] = "Компания";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_PRODUCTS"] = "Мои товары";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_ORDERS"] = "Заказы";
$MESS["T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_STAT"] = "Статистика";
?>