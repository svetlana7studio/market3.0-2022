<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
$frame = $this->createFrame()->begin('');
$frame->setBrowserStorage(true);

$oOption = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance();
$aGroups = array(
	$oOption->getOption("seller_admin_group"),
	$oOption->getOption("seller_moderator_group")
);

$isAuthorized = $USER->IsAuthorized();
$isPartner = false;


if($isAuthorized) {

    $userName = $USER->GetFirstName();
    if(!$userName) {
		$userName = $USER->GetLogin();
	}
    $userName = TruncateText($userName, 12);

    // $aGroups
    foreach ($USER->GetUserGroupArray() as $item) {
        if(in_array($item, $aGroups)){
            $isPartner = true;
            break;
        }
	}

    if(!empty(array_intersect($aGroups, CUser::GetUserGroup($USER->GetID()))))
        $isPartner = true;
    }

?>

<div class="s7sbp--marketplace--header--search-line--buttons--lk mob--btn-lk" <?=!$isAuthorized ? "onClick='location.href=\"/auth/\"'": ""?>>

    <div class="ic-m"><i class="icon icon-lk icon-mob-lk"></i></div>

	<?if(!$isAuthorized):?>

		<span class="s7sbp--marketplace--header--search-line--buttons--lk--titile text-13"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL")?></span>

	<?else:?>
		<span class="s7sbp--marketplace--header--search-line--buttons--lk--titile text-13"><?=$userName?></span>


		<div class="s7sbp--marketplace--lk--menu--wrapp" onclick="marketplaceLk.menuFadeToggle()">
			<div class="s7sbp--marketplace--lk--menu--text"></div>
			<div class="s7sbp--marketplace--lk--menu d-none" id="s7sbp--marketplace--lk--menu">
				<div class="s7sbp--marketplace--lk--menu--item order-im"><a href="<?=SITE_DIR?>personal/orders/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_ORDERS")?></a></div>
				<div class="s7sbp--marketplace--lk--menu--item baskets-im"><a href="<?=SITE_DIR?>personal/baskets/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_BASKET")?></a></div>
				<div class="s7sbp--marketplace--lk--menu--item address-im"><a href="<?=SITE_DIR?>personal/address/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_ADDRESS")?></a></div>
				<div class="s7sbp--marketplace--lk--menu--item favorite-im"><a href="<?=SITE_DIR?>personal/favorite/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_FAVORITE")?></a></div>
				<div class="s7sbp--marketplace--lk--menu--item store-im"><a href="<?=SITE_DIR?>personal/store/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_STORE")?></a></div>
				<div class="s7sbp--marketplace--lk--menu--item requisits-im"><a href="<?=SITE_DIR?>personal/requisits/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_REQUISITS")?></a></div>
				<div class="s7sbp--marketplace--lk--menu--item settings-im"><a href="<?=SITE_DIR?>personal/settings/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SETTINGS")?></a></div>

				<?if($isPartner):?>

					<div class="s7sbp--marketplace--lk--menu--item s7sbp--marketplace--lk--menu--item_title"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_TITLE")?></div>

					<div class="s7sbp--marketplace--lk--menu--item shop-im"><a href="<?=SITE_DIR?>seller/shop/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_SHOP")?></a></div>
					<div class="s7sbp--marketplace--lk--menu--item company-im"><a href="<?=SITE_DIR?>seller/company/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_COMPANY")?></a></div>
					<div class="s7sbp--marketplace--lk--menu--item products-im"><a href="<?=SITE_DIR?>seller/products/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_PRODUCTS")?></a></div>
					<div class="s7sbp--marketplace--lk--menu--item orders-im"><a href="<?=SITE_DIR?>seller/orders/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_ORDERS")?></a></div>
					<div class="s7sbp--marketplace--lk--menu--item statistics-im"><a href="<?=SITE_DIR?>seller/statistics/"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_SELLER_STAT")?></a></div>
				<?endif;?>

				<div class="s7sbp--marketplace--lk--menu--item s7sbp--marketplace--lk--menu--item_exit"><a href="./?logout=yes"><?=GetMessage("T_S7SPB_MARKETPLACE_HEADER_PERSONAL_MENU_EXIT")?></a></div>
			</div>
		</div>
	<?endif;?>
	
</div>
<?$frame->end();?>