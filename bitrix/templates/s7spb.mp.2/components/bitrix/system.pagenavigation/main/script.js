/**
 *
 * @param t
 */
function ajax_load_btn(t) {
    var url = $(t).closest('.module-pagination-wrapper').find('.modern-page-next').attr('href');
    $(t).text($(t).data("loading"));
    disableElement(t);


    $.ajax({
        url: url,
        data: {"ajax_get": "Y"},
        success: function(html){

            $(t).text($(t).data("text"));
            enableElement(t);

            if($('.s7sbp--marketplace--catalog-section--items').length){
                $('.s7sbp--marketplace--catalog-section--items').append($(html).filter('.s7sbp--marketplace--catalog-section--items').html());
            }


            $('.modern-page-navigation').html($(html).filter('.modern-page-navigation').html());
        }
    })
}

