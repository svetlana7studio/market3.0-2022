<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/catalog.section.js");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/catalog.section.css");

if (isset($arResult['ITEM'])):
    $item = $arResult['ITEM'];
?>
    <div id="products-viewed-item-<?=$arParams["KEY"]?>" class="products-viewed-item">

        <div class="s7spb-product-cell">
            <div class="position-relative">

                <?if(!empty($item["STIKER"])):?>
                    <div class="stiker-block">
                        <?
                        foreach ($item["STIKER"] as $stiker):
                            if($stiker["CODE"] == "mast")
                                continue;
                            ?>
                            <div class="<?=$stiker["CLASS"]?>" title="<?=$stiker["TITLE"]?>"></div>
                        <?endforeach;?>
                    </div>
                <?endif;?>

                <a href="<?=$item["DETAIL_PAGE_URL"]?>">
                    <div class="pb3 square-200">
                        <img src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>"
                             class="img-200"
                             width="<?=$item["PREVIEW_PICTURE"]["WIDTH"]?>"
                             height="<?=$item["PREVIEW_PICTURE"]["HEIGHT"]?>"
                             title="<?=$item["PREVIEW_PICTURE"]["TITLE"]?>"
                             alt="<?=$item["PREVIEW_PICTURE"]["ALT"]?>" />
                    </div>
                </a>

                <?if(!empty($arResult['PRICES'])):?>
                    <div class="py2 mob-tex">
                        <?
                        foreach ($arResult['PRICES'] as $codePrice=>$arPrice):
                            switch ($codePrice){
                                case "price_red":
                                    $arPrice["COLOR"] = "text-danger";
                                    break;
                                case "price_green":
                                    $arPrice["COLOR"] = "text-success-ultras";
                                    break;
                                case "price_yellow":
                                    $arPrice["COLOR"] = "text-warning";
                                    break;
                            }
                                $arPrice["PRINT_VALUE"] = $arResult['PRODUCT_PRICES'][$item["ID"]][$arPrice["ID"]]["PRINT_VALUE"];
                            $arPrice["VALUE"] = $arResult['PRODUCT_PRICES'][$item["ID"]][$arPrice["ID"]]["PRICE"];
                            ?>
                            <div class="s7spb-row s7spb-row-baseline mb2">
                                <?if($arPrice["VALUE"] > 999):?>
                                    <div class="s7spb-col text-14 text-height-16 <?=$arPrice["COLOR"]?>"><?=$arPrice["PRINT_VALUE"]?></div>
                                <?else:?>
                                    <div class="s7spb-col text-16 text-height-16 <?=$arPrice["COLOR"]?>"><?=$arPrice["PRINT_VALUE"]?></div>
                                <?endif;?>
                                <div class="s7spb-col text-11 d-none-600"><?=$arPrice["TITLE"]?></div>
                            </div>
                        <?endforeach;?>
                    </div>
                <?endif;?>

                <?if(is_array($item["DISPLAY_PROPERTIES"])):?>

                    <table class="pb2" align="center">
                        <tr>
                            <td>
                                <div class="s7spb-product-props">
                                    <?foreach ($item["DISPLAY_PROPERTIES"] as $arProperty):?>
                                        <div class="pb1 text-11"><?=$arProperty["NAME"]?>: <b><?=$arProperty["DISPLAY_VALUE"]?></b></div>
                                    <?endforeach;?>
                                    <div class="pb1 text-11">Остаток:
                                        <b>
                                            <?if($item["PRODUCT"]["QUANTITY_TRACE"] == "Y"):?>
                                                <?=$item["PRODUCT"]["QUANTITY"]?>  шт.
                                            <?else:?>
                                                >100
                                            <?endif;?>
                                        </b>
                                    </div>
                                </div>
                            </td>
                            <td class="s7spb-product-informer">
                                <div class="circle-24 text-20 ml1 cursor-pointer bg-gray text-bold text-gray d-inline-block"
                                     onmouseenter="anitosCatalogSection.priceInfoToggle(this)"
                                     onclick="return anitosCatalogSection.priceInfoPopup(this)">i</div>
                            </td>
                        </tr>
                    </table>

                <?endif;?>

                <div class="px5">
                    <form active="<?=$arResult["BUY_URL"]?>" method="post" onsubmit="return s7market.actionBuy(this)">

                        <input type="hidden" name="action" value="ADD2BASKET">
                        <input type="hidden" name="id" value="<?=$item["ID"]?>">
                        <?=bitrix_sessid_post();?>

                        <div class="s7spb-row quantity-counter"
                             data-moq="<?=$item["PROPERTIES"]["MOQ"]["VALUE"]?>"
                            <?if($item["PRODUCT"]["QUANTITY_TRACE"] == "Y"):?>
                                data-quantity="<?=$item["PRODUCT"]["QUANTITY"]?>"
                            <?endif;?>
                             data-multiplicity="<?=$item["PROPERTIES"]["MULTIPLICITY"]["VALUE"]?>">
                            <div class="s7spb-col quantity-counter-minus"
                                 onclick="quantityCounter.down(this)">&ndash;</div>
                            <input type="text"
                                   onkeyup="quantityCounter.inputIntValidator(this)"
                                   name="<?=$arParams["PRODUCT_QUANTITY_VARIABLE"]?>"
                                   value="<?
                                   $value = 1;
                                   if($item["PROPERTIES"]["MOQ"]["VALUE"] > $value)
                                       $value = $item["PROPERTIES"]["MOQ"]["VALUE"];
                                   if($item["PROPERTIES"]["MULTIPLICITY"]["VALUE"] > $value)
                                       $value = $item["PROPERTIES"]["MULTIPLICITY"]["VALUE"];
                                   echo $value;
                                   ?>"
                                   class="s7spb-col" />
                            <div class="s7spb-col quantity-counter-plus"
                                 onclick="quantityCounter.up(this)">+</div>
                        </div>
                        <input type="submit" value=" " />
                    </form>
                </div>

            </div>
        </div>

    </div>

<?endif;?>