<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

if (isset($arResult['ITEM'])):
    $item = $arResult['ITEM'];
    $areaId = $arResult['AREA_ID'];
    $itemIds = array(
        'ID' => $areaId,
        'PICT' => $areaId.'_pict',
        'SECOND_PICT' => $areaId.'_secondpict',
        'PICT_SLIDER' => $areaId.'_pict_slider',
        'STICKER_ID' => $areaId.'_sticker',
        'SECOND_STICKER_ID' => $areaId.'_secondsticker',
        'QUANTITY' => $areaId.'_quantity',
        'QUANTITY_DOWN' => $areaId.'_quant_down',
        'QUANTITY_UP' => $areaId.'_quant_up',
        'QUANTITY_MEASURE' => $areaId.'_quant_measure',
        'QUANTITY_LIMIT' => $areaId.'_quant_limit',
        'BUY_LINK' => $areaId.'_buy_link',
        'BASKET_ACTIONS' => $areaId.'_basket_actions',
        'NOT_AVAILABLE_MESS' => $areaId.'_not_avail',
        'SUBSCRIBE_LINK' => $areaId.'_subscribe',
        'COMPARE_LINK' => $areaId.'_compare_link',
        'FAVORITE_LINK' => $areaId.'_favorite_link',
        'PRICE' => $areaId.'_price',
        'PRICE_OLD' => $areaId.'_price_old',
        'PRICE_TOTAL' => $areaId.'_price_total',
        'DSC_PERC' => $areaId.'_dsc_perc',
        'SECOND_DSC_PERC' => $areaId.'_second_dsc_perc',
        'PROP_DIV' => $areaId.'_sku_tree',
        'PROP' => $areaId.'_prop_',
        'DISPLAY_PROP_DIV' => $areaId.'_sku_prop',
        'BASKET_PROP_DIV' => $areaId.'_basket_prop',
    );
?>

<!-------------->


    <div id="<?=$this->GetEditAreaId($item['ID']);?>" class="text-center pb5 position-relative s7spb-item-height">

        <div class="s7spb-product-cell-line row-cell">

            <?if(!empty($item["STIKER"])):?>
                <div class="stiker-block">
                    <?
                    foreach ($item["STIKER"] as $stiker):
                        if($stiker["CODE"] == "mast")
                            continue;
                        ?>
                        <div class="<?=$stiker["CLASS"]?>" title="<?=$stiker["TITLE"]?>"></div>
                    <?endforeach;?>
                </div>
            <?endif;?>

            <a href="<?=$item["DETAIL_PAGE_URL"]?>" class="d-block text-decoration-none text-tranqulity text-12 text-capitalize">

                <?if(is_array($item["PREVIEW_PICTURE"])):?>
                    <div class="pb3 img-line-view ">
                        <img src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>"
                             class="img-200"
                             height="<?=$item["PREVIEW_PICTURE"]["HEIGHT"]?>"
                             width="<?=$item["PREVIEW_PICTURE"]["WIDTH"]?>"
                             title="<?=$item["PREVIEW_PICTURE"]["TITLE"]?>"
                             alt="<?=$item["PREVIEW_PICTURE"]["ALT"]?>" />
                    </div>
                <?endif;?>

                <div class="s7spb-product-name"><?=$item["NAME"]?></div>
            </a>

            <?if(is_array($item["DISPLAY_PROPERTIES"])):?>
                <div class="s7spb-product-props">
                    <?foreach ($item["DISPLAY_PROPERTIES"] as $arProperty):?>
                        <div class="pb1 text-11"><?=$arProperty["NAME"]?>: <b><?=$arProperty["DISPLAY_VALUE"]?></b></div>
                    <?endforeach;?>
                    <div class="pb1 text-11">Остаток:
                        <b>
                            <?if($item["PRODUCT"]["QUANTITY_TRACE"] == "Y"):?>
                                <?=$item["PRODUCT"]["QUANTITY"]?>  шт.
                            <?else:?>
                                >100
                            <?endif;?>
                        </b>
                    </div>
                </div>
            <?endif;?>

            <?if(empty($item["PRICES"])):?>
                <?if(is_array($item["DISPLAY_PROPERTIES"])):?>
                    <div class="s7spb-product-props">
                        <?foreach ($item["DISPLAY_PROPERTIES"] as $arProperty):?>
                            <div class="pb1 text-11"><?=$arProperty["NAME"]?>: <b><?=$arProperty["DISPLAY_VALUE"]?></b></div>
                        <?endforeach;?>
                        <div class="pb1 text-11">Остаток:
                            <b>
                                <?if($item["PRODUCT"]["QUANTITY_TRACE"] == "Y"):?>
                                    <?=$item["PRODUCT"]["QUANTITY"]?>  шт.
                                <?else:?>
                                    >100
                                <?endif;?>
                            </b>
                        </div>
                    </div>
                <?endif;?>
            <?else:?>

                <div class="add-line-card add-line-card-line">
                    <table class="pb2" align="center">
                        <tr>
                            <td>
                                <?foreach($item["PRICES"] as $code=>$arPrice):?>
                                    <div class="text-<?=$code?> <?=$code == "price_yellow" ? "text-20" : "text-14"?>"
                                         title="<?=$arResult["PRICES"][$code]["TITLE"]?>">
                                        <?if($arPrice["VALUE"] > $arPrice["DISCOUNT_VALUE"]):?>
                                            <?=$arPrice["PRINT_DISCOUNT_VALUE"]?> <s class="text-gray"><?=$arPrice["PRINT_VALUE"]?></s>
                                        <?else:?>
                                            <?=$arPrice["PRINT_VALUE"]?>
                                        <?endif;?>
                                    </div>
                                <?endforeach;?>
                            </td>
                            <td class="s7spb-product-informer">
                                <div class="circle-24 text-20 ml1 cursor-pointer bg-gray text-bold text-gray d-inline-block"
                                     onmouseenter="anitosCatalogSection.priceInfoToggle(this)"
                                     onclick="return anitosCatalogSection.priceInfoPopup(this)">i</div>
                            </td>
                        </tr>
                    </table>
                    <div class="px5 row-line">
                        <form active="<?=$item["DETAIL_PAGE_URL"]?>" method="post" onsubmit="return s7market.actionBuy(this)">

                            <input type="hidden" name="action" value="ADD2BASKET">
                            <input type="hidden" name="id" value="<?=$item["ID"]?>">
                            <?=bitrix_sessid_post()?>

                            <div class="s7spb-row quantity-counter row-line"
                                 data-moq="<?=$item["PROPERTIES"]["MOQ"]["VALUE"]?>"
                                <?if($item["PRODUCT"]["QUANTITY_TRACE"] == "Y"):?>
                                    data-quantity="<?=$item["PRODUCT"]["QUANTITY"]?>"
                                <?endif;?>
                                 data-multiplicity="<?=$item["PROPERTIES"]["MULTIPLICITY"]["VALUE"]?>">
                                <div class="s7spb-col quantity-counter-minus"
                                     onclick="quantityCounter.down(this)">&ndash;</div>
                                <input type="text"
                                       name="<?=$arParams["PRODUCT_QUANTITY_VARIABLE"]?>"
                                       value="<?
                                       $value = 1;
                                       if($item["PROPERTIES"]["MOQ"]["VALUE"] > $value)
                                           $value = $item["PROPERTIES"]["MOQ"]["VALUE"];
                                       if($item["PROPERTIES"]["MULTIPLICITY"]["VALUE"] > $value)
                                           $value = $item["PROPERTIES"]["MULTIPLICITY"]["VALUE"];
                                       echo $value;
                                       ?>"
                                       class="s7spb-col" />
                                <div class="s7spb-col quantity-counter-plus"
                                     onclick="quantityCounter.up(this)">+</div>
                            </div>
                            <input type="submit" value="Купить" />
                        </form>
                    </div>
                </div>

            <?endif;?>




        </div>
    </div>


    <!-------------->

<?endif;?>