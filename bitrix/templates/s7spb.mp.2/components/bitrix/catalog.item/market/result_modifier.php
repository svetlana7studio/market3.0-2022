<?

/**
 * Resize pictures
 */
$arParams["RESIZE"] = 400;

// "ADD_PICT_PROP" => "", // H_PICTURES

if (!empty($arResult["ITEM"]["MORE_PHOTO"]))
{
    //AddMessage2Log($arResult["ITEM"]["MORE_PHOTO"], "MORE_PHOTO");

    foreach ($arResult["ITEM"]["MORE_PHOTO"] as $key=>$file){

        $file = CFile::ResizeImageGet(
            $file["ID"],
            [
                "width" => $arParams["RESIZE"],
                "height" => $arParams["RESIZE"]
            ],
            BX_RESIZE_IMAGE_EXACT,
            false
        );

        $arResult["ITEM"]["MORE_PHOTO"][$key] = [
            "SRC" => $file["src"],
            "WIDTH" => $arParams["RESIZE"],
            "HEIGHT" => $arParams["RESIZE"]
        ];
    }

}

if($arResult["ITEM"]["~DETAIL_PICTURE"] > 0){
    if($arResult["ITEM"]["~PREVIEW_PICTURE"] <= 0){
        $arResult["ITEM"]["~PREVIEW_PICTURE"] = $arResult["ITEM"]["~DETAIL_PICTURE"];
    }
}

if($arResult["ITEM"]["~PREVIEW_PICTURE"] > 0){

    $arResult["ITEM"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
        $arResult["ITEM"]["~PREVIEW_PICTURE"],
        [
            "width" => $arParams["RESIZE"],
            "height" => $arParams["RESIZE"]
        ],
        BX_RESIZE_IMAGE_EXACT,
        false
    );
    $arResult["ITEM"]["PREVIEW_PICTURE"] = [
        "SRC" => $arResult["ITEM"]["PREVIEW_PICTURE"]["src"],
        "WIDTH" => $arParams["RESIZE"],
        "HEIGHT" => $arParams["RESIZE"],
    ];


    if($arResult["ITEM"]["PREVIEW_PICTURE_SECOND"]["ID"] > 0){
        $arResult["ITEM"]["PREVIEW_PICTURE_SECOND"] = CFile::ResizeImageGet(
            $arResult["ITEM"]["~PREVIEW_PICTURE"],
            [
                "width" => $arParams["RESIZE"],
                "height" => $arParams["RESIZE"]
            ],
            BX_RESIZE_IMAGE_EXACT,
            false
        );
        $arResult["ITEM"]["PREVIEW_PICTURE_SECOND"] = [
            "SRC" => $arResult["ITEM"]["PREVIEW_PICTURE_SECOND"]["src"],
            "WIDTH" => $arParams["RESIZE"],
            "HEIGHT" => $arParams["RESIZE"],
        ];
    }




}