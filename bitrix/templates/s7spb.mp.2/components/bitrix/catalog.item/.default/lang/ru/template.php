<?
$MESS["CATALOG_IZB"] = "Добавить в избранное";
$MESS["CATALOG_IZB_IN"] = "В избранном";
$MESS["CATALOG_IZB_OUT"] = "Удалить из избранного";
$MESS["CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE"] = "от #PRICE# за #VALUE# #UNIT#";
$MESS["CATALOG_WARNING"] = "Внимание!";
$MESS["CATALOG_MEASURE_PACK"] = "В 1 коробе PCS шт. / Общее <span class='text-nowrap'>кол-во</span> <span class='measure-pack-nn'>NN</span> шт.";
$MESS["CATALOG_MEASURE_PACK_NAME"] = "кор.";
$MESS["CT_BCS_CATALOG_IN_QUANTITY"] = "Остаток";
$MESS["CT_CATALOG_QUANTITY_MEASURE"] = "N шт.";
$MESS["CATALOG_BUY"] = "Купить";
$MESS["CATALOG_ADD"] = "В корзину";