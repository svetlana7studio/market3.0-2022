<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

$this->setFrameMode(true);
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/catalog/bitrix.product.item.css");

if (isset($arResult['ITEM'])):
    $item = $arResult['ITEM'];
    $areaId = $arResult['AREA_ID'];
    $itemIds = array(
        'ID' => $areaId,
        'PICT' => $areaId.'_pict',
        'SECOND_PICT' => $areaId.'_secondpict',
        'PICT_SLIDER' => $areaId.'_pict_slider',
        'STICKER_ID' => $areaId.'_sticker',
        'SECOND_STICKER_ID' => $areaId.'_secondsticker',
        'QUANTITY' => $areaId.'_quantity',
        'QUANTITY_DOWN' => $areaId.'_quant_down',
        'QUANTITY_UP' => $areaId.'_quant_up',
        'QUANTITY_MEASURE' => $areaId.'_quant_measure',
        'QUANTITY_LIMIT' => $areaId.'_quant_limit',
        'BUY_LINK' => $areaId.'_buy_link',
        'BASKET_ACTIONS' => $areaId.'_basket_actions',
        'NOT_AVAILABLE_MESS' => $areaId.'_not_avail',
        'SUBSCRIBE_LINK' => $areaId.'_subscribe',
        'COMPARE_LINK' => $areaId.'_compare_link',
        'FAVORITE_LINK' => $areaId.'_favorite_link',
        'PRICE' => $areaId.'_price',
        'PRICE_OLD' => $areaId.'_price_old',
        'PRICE_TOTAL' => $areaId.'_price_total',
        'DSC_PERC' => $areaId.'_dsc_perc',
        'SECOND_DSC_PERC' => $areaId.'_second_dsc_perc',
        'PROP_DIV' => $areaId.'_sku_tree',
        'PROP' => $areaId.'_prop_',
        'DISPLAY_PROP_DIV' => $areaId.'_sku_prop',
        'BASKET_PROP_DIV' => $areaId.'_basket_prop',
    );

?>
    <div class="product-item-container product-item-container-min mb1">
        <div class="product-item" id="<?=$this->GetEditAreaId($item['ID']);?>">

            <a href="<?=$item["DETAIL_PAGE_URL"]?>" class="product-item-img mb3 d-block">
                <?if(is_array($item["PREVIEW_PICTURE"])):?>
                    <img src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>"
                         height="<?=$item["PREVIEW_PICTURE"]["HEIGHT"]?>"
                         width="<?=$item["PREVIEW_PICTURE"]["WIDTH"]?>"
                         class="img-responsive" />
                <?else:?>
                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/logo.png" src="img-responsive">
                <?endif;?>
            </a>

            <a href="<?=$item["DETAIL_PAGE_URL"]?>" class="product-item-title mb3 d-block"><?=$item["NAME"]?></a>

            <?if (is_array($item["MIN_PRICE"])):?>
                <div class="d-block ">
                    <?if($item["MIN_PRICE"]["DISCOUNT_DIFF"] > 0):?>
                        <div class="product-item-price-current d-inline-block"><?=$item["MIN_PRICE"]["PRINT_DISCOUNT_VALUE"]?></div>
                        <div class="product-item-price-old d-inline-block"><?=$item["MIN_PRICE"]["PRINT_VALUE"]?></div>
                        <div class="product-item-label bg-danger">-<?=$item["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]?>%</div>
                    <?else:?>
                        <div class="product-item-price-current"><?=$item["MIN_PRICE"]["PRINT_VALUE"]?></div>
                    <?endif;?>
                </div>
            <?endif;?>


            <!-- Start sale actions -->
            <form active="<?=$item["DETAIL_PAGE_URL"]?>" method="post" onsubmit="return s7market.actionBuy(this)">

                <input type="hidden" name="action" value="ADD2BASKET">
                <input type="hidden" name="id" value="<?=$item["ID"]?>">
                <?=bitrix_sessid_post()?>

                <div class="s7spb-row quantity-counter mx5"
                     data-moq="<?=$item["PROPERTIES"]["MOQ"]["VALUE"]?>"
                    <?if($item["PRODUCT"]["QUANTITY_TRACE"] == "Y"):?>
                        data-quantity="<?=$item["PRODUCT"]["QUANTITY"]?>"
                    <?endif;?>
                     data-multiplicity="<?=$item["PROPERTIES"]["MULTIPLICITY"]["VALUE"]?>">
                    <div class="s7spb-col quantity-counter-minus"
                         onclick="quantityCounter.down(this)">&ndash;</div>
                    <input type="text"
                           name="<?=$arParams["PRODUCT_QUANTITY_VARIABLE"]?>"
                           value="<?
                           $value = 1;
                           if($item["PROPERTIES"]["MOQ"]["VALUE"] > $value)
                               $value = $item["PROPERTIES"]["MOQ"]["VALUE"];
                           if($item["PROPERTIES"]["MULTIPLICITY"]["VALUE"] > $value)
                               $value = $item["PROPERTIES"]["MULTIPLICITY"]["VALUE"];
                           echo $value;
                           ?>"
                           class="s7spb-col" />
                    <div class="s7spb-col quantity-counter-plus"
                         onclick="quantityCounter.up(this)">+</div>
                </div>
                <input type="submit"
                       class="btn btn-primary"
                       value="<?=Loc::getMessage("CATALOG_BUY")?>" />
            </form>
            <!-- End sale actions -->

        </div>
    </div>
<?endif;?>