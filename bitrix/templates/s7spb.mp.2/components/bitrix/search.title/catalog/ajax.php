<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult["CATEGORIES"])):?>
	<table class="title-search-result">
		<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
			<?foreach($arCategory["ITEMS"] as $i => $arItem):?>
			<tr>
				<?if($category_id === "all"):?>
					<td class="title-search-all" colspan="<?=($arParams["SHOW_PREVIEW"]=="Y") ? '3' : '2'?>" >
						<a href="<?=$arItem["URL"]?>"><span class="text"><?=$arItem["NAME"]?></span><span class="icon"><i></i></span></a>
					</td>
				<?elseif(isset($arResult["ELEMENTS"][$arItem["ITEM_ID"]])):
					$arElement = $arResult["ELEMENTS"][$arItem["ITEM_ID"]];
				?>
					<?if ($arParams["SHOW_PREVIEW"]=="Y"):?>
						<td class="picture">
							<?if (is_array($arElement["PICTURE"])):?>
								<img class="item_preview" align="left" src="<?=$arElement["PICTURE"]["src"]?>" width="38">
							<?else:?>
								<img align="left" src="<?=SITE_TEMPLATE_PATH?>/images/no_image200.jpg" width="38" height="38">
							<?endif;?>
						</td>
					<?endif;?>
					<td class="main">
						<div class="item-text">
							<a href="<?=$arItem["URL"]?>"><?=$arItem["NAME"]?></a>
							<?if ($arParams["SHOW_ANOUNCE"]=="Y"):?><p class="title-search-preview"><?=$arElement["PREVIEW_TEXT"];?></p><?endif;?>
						</div>
					</td>
                    <td>
                        <div class="price cost prices">
                            <div class="title-search-price">
                                <?if(!empty($arElement["PRICES"])):?>
                                    <?foreach($arElement["PRICES"] as $code=>$arPrice):?>
                                        <?if($arPrice["CAN_ACCESS"]):
                                            $arPrice["STYLE"] = "text-price_green text-14 text-white-space";
                                            switch ($code){
                                                case "price_red":
                                                    $arPrice["STYLE"] = "text-price_red text-14 text-white-space";
                                                    break;
                                                case "price_yellow":
                                                    $arPrice["STYLE"] = "text-price_yellow text-20 text-white-space";
                                                    break;
                                                case "price_green":
                                                    $arPrice["STYLE"] = "text-price_green text-14 text-white-space";
                                                    break;
                                            }
                                            ?>
                                            <?if($arPrice["CAN_ACCESS"]):?>
                                                <div class="<?=$arPrice["STYLE"]?>" title="<?=$arResult["PRICES"][$code]["TITLE"];?>"><?=$arPrice["PRINT_VALUE"]?></div>
                                            <?endif;?>
                                        <?endif;?>
                                    <?endforeach;?>
                                <?endif;?>
                            </div>
                        </div>
                    </td>
				<?elseif(isset($arItem["ICON"])):?>
					<td class="main" colspan="<?=($arParams["SHOW_PREVIEW"]=="Y") ? '3' : '2'?>">
						<div class="item-text">
							<a href="<?=$arItem["URL"]?>"><?=$arItem["NAME"]?></a>
						</div>
					</td>
				<?endif;?>
			</tr>
			<?endforeach;?>
		<?endforeach;?>
	</table>
<?endif;?>
