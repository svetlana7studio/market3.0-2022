<?
/**
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

if(empty($arResult))
    return;


?>




<div class="wrapper-left__block">
<h3 class="text-primary text-18 px4"><?=Loc::getMessage("CATALOG_MENU_TITLE")?></h3>
<!--- MUNU LEVEL 1 --->
<ul id="menu-vertical-dropdown">
<?
$previousLevel = 0;
foreach($arResult as $arItem):

    $DEPTH_LEVEL = $arItem["DEPTH_LEVEL"] - $previousLevel;
    if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel)
    {
        echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));

    }
    ?>

    <?if ($arItem["IS_PARENT"]):?>
        <li class="has-childs"><a href="<?=$arItem["LINK"]?>"><?
                if($arItem["DEPTH_LEVEL"] == 1){
                    echo '<span class="menu-vertical-dropdown-item ' . $arItem["PARAMS"]["XML_ID"] . '">';
                    echo '</span>';
                }
                echo $arItem["TEXT"];
			?></a>
            <ul class="depth-level-<?=$arItem["DEPTH_LEVEL"]?>">
    <?else:?>
        <li><a href="<?=$arItem["LINK"]?>"><?
                if($arItem["DEPTH_LEVEL"] == 1){
                    echo '<span class="menu-vertical-dropdown-item ' . $arItem["PARAMS"]["XML_ID"] . '">';
                    echo '</span>';
                }
                echo $arItem["TEXT"];
                ?></a></li>
    <?endif;?>

<?
    $previousLevel = $arItem["DEPTH_LEVEL"];
endforeach;
if ($previousLevel > 1)
    echo str_repeat("</ul></li>", ($previousLevel - 1));
?>
</ul>
	</div>