<?
/**
 * @var array $arParams
 * @var array $arResult
 */
if(empty($arResult))
    return;
?>
<ul class="empty text-16 px1">
    <?foreach ($arResult as $arItem):?>
        <li class="mb1"><a href="<?=$arItem["LINK"]?>" class="text-decoration-none text-white"><?=$arItem["TEXT"]?></a></li>
    <?endforeach;?>
</ul>
