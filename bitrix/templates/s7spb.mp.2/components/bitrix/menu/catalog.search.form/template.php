<?
/**
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

if(empty($arResult))
    return;

$this->SetViewTarget('MENU_SEARCH_SELECT');
?>
<select name="section">
    <option value="<?=SITE_DIR?>catalog/"><?=Loc::getMessage("CATALOG_ALL")?></option>
    <?
    foreach ($arResult as $i=>$arItem):
        if($arItem["DEPTH_LEVEL"] > 1)
            continue;
        ?>
        <option value="<?=$arItem["LINK"]?>"
            <?if($arParams["SEARCH_CURRENT_OPTION"] == $arItem["LINK"]):?>
                selected
            <?endif;?>><?=$arItem["TEXT"]?></option>

    <?
    endforeach;
    ?>
</select>
<?
$this->EndViewTarget();
?>