<?
/**
 * @var array $arParams
 * @var array $arResult
 * Список ссылок с отступами
 * Отступы для первого и последнего свои
 */

if(empty($arResult))
    return;

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

$i = 0;
foreach($arResult as $arItem){

    $i++;

    $arItem["CSS"] = "text-decoration-none";

    switch ($i){
        case count($arResult):
            $arItem["CSS"] .= " pl4";
            break;
        case 1:
            $arItem["CSS"] .= " pr4";
            break;
        default:
            $arItem["CSS"] .= " px4";
    }

    if($arItem["SELECTED"])
        $arItem["CSS"] .= " text-info";
    else
        $arItem["CSS"] .= " text-white";

    echo Loc::getMessage("TOP_MENU_LINK", [
        "CSS" => $arItem["CSS"],
        "LINK" => $arItem["LINK"],
        "TEXT" => $arItem["TEXT"]
    ]);

}