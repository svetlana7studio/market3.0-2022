<?
/**
 * @var array $arResult
 * @var array $arParams
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["VISIBLE_COUNT"] = 15;
?>

<?if (is_array($arResult) && !empty($arResult)):?>
    <div class="s7sbp--marketplace--index--catalog--menu">
        <?if($arParams["TITLE"]):?>
            <div class="s7sbp--marketplace--index--catalog--menu--title"><?=$arParams["TITLE"]?></div>
        <?endif;?>
        <ul class="s7sbp--marketplace--index--catalog--menu--list">
            <?foreach( $arResult as $i=>$arItem ):?>
                <li class="full <?=$i>$arParams["VISIBLE_COUNT"] ? " d-none toggle-vis " : ""?>s7sbp--marketplace--index--catalog--menu--list--item caption has-child  m_line v_hover<?if($arParams["CURREN_PAGE"]==$arItem["LINK"]){echo " active";}?>">
                    <a class="s7sbp--marketplace--index--catalog--menu--list--item--link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                </li>
            <?endforeach;?>
        </ul>

        <?if(count($arResult) > $arParams["VISIBLE_COUNT"]):?>
        <div class="text-center">
            <button class="btn"
                    data-show="<?=GetMessage("SHOW_ALL_ITEMS")?>"
                    data-hide="<?=GetMessage("HIDE_ALL_ITEMS")?>"
                    onclick="menuOneLevel.toggle(this)"><?=GetMessage("SHOW_ALL_ITEMS")?></button>
        </div>
        <?endif;?>

    </div>


<?endif;?>