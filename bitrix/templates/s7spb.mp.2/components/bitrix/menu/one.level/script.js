var menuOneLevel = {
    visible: false,
    toggle: function (t) {

        if(this.visible === true){
            this.hide(t);
            this.visible = false;
            $(t).text($(t).data("show"));
        }else{
            this.show(t);
            this.visible = true;
            $(t).text($(t).data("hide"));
        }


    },
    show: function (t) {
        var block = $(t).parent().parent();
        block.find(".toggle-vis").removeClass("d-none");
    },
    hide: function (t) {
        var block = $(t).parent().parent();
        block.find(".toggle-vis").addClass("d-none");
    }
};