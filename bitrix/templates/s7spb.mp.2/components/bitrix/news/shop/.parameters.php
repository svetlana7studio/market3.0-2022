<?
// SORT_BUTTONS

$arViewModeList = array(
    'POPULARITY' => GetMessage('COMPANY_SORT_POPULARITY'),
    'NAME' => GetMessage('COMPANY_SORT_NAME'),
    'PRICE' => GetMessage('COMPANY_SORT_PRICE')
);



$arTemplateParameters = array(
    "SECTIONS_VIEW_MODE" => array(
        "PARENT" => "SECTIONS_SETTINGS",
        "NAME" => GetMessage('COMPANY_SORT_TITLE'),
        "TYPE" => "LIST",
        "VALUES" => $arViewModeList,
        "MULTIPLE" => "N",
    ),
    "PRICE_CODE" => array(
        "PARENT" => "SECTIONS_SETTINGS",
        "NAME" => GetMessage('COMPANY_PRICE_CODE'),
        "TYPE" => "LIST",
        "VALUES" => [],
        "MULTIPLE" => "N",
    ),
);