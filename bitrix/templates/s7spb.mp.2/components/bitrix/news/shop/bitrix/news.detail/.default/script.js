var tradeMark = {
    mark: null,
    scroll: {
        id: "#shop_navigation",
        class: "stiky-nav",
        docTop: 0,
        navTop: 0
    },

    // wish feature
    wish: function (t) {
        var link = $(t);
        if(link.hasClass("active")){
            link.removeClass("active");
            link.html('<i class="icon icon-favorite square-16"></i> ' + link.data("title"));

        }else{
            link.addClass("active");
            link.html('<i class="icon icon-favorite square-16"></i> ' + link.data("title-in"));
        }

        BX.ajax.runAction('studio7spb:marketplace.api.tools.addToWish', {
            data: {
                id: link.data("item-id"),
            }
        });

    },

    // top panel
    setScrollTop: function () {
        this.scroll.docTop = window.pageYOffset || document.documentElement.scrollTop;
    },

    getNavTop: function(){
        if(this.scroll.navTop == 0){
            this.scroll.navTop = $(this.scroll.id).offset().top;
        }
        return this.scroll.navTop;
    },

    toggleNavTop: function(top){
        if(top > this.getNavTop()){
            $(this.scroll.id).addClass(this.scroll.class);
        }else{
            $(this.scroll.id).removeClass(this.scroll.class);
        }
    },

    scroollToElement: function(element){

        var href = $(element).attr("href");

        if(href == "#"){
            top = 0;
        }else{
            var top = $(href).offset().top,
                height = $(this.scroll.id).height();
            top -= height;
        }

        $([document.documentElement, document.body]).animate({
            scrollTop: top
        }, 1000);

        return false;

    },

    // dev tools
    d: function (value) {
        console.info(value);
    }
};


window.addEventListener("scroll", function(event) {
    //tradeMark.scroll.docTop = this.scrollY;
    tradeMark.toggleNavTop(this.scrollY);
}, false);
