function moveToBrand(t) {

    var brand = $(t).attr("href"),
        offset = 50;

    brand = $(brand).offset().top;
    brand = parseInt(brand);
    brand = brand - offset;

    $('body,html').animate({
        scrollTop: brand
    }, 800);

    return false;
}