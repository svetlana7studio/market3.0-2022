<?
/**
 *
 */
$arResult["ALPHAVITE"] = array();

foreach ($arParams["ALPHAVITE"] as $code=>$alphavite)
{

    foreach ($alphavite as $i=>$letter){

        $arResult["ALPHAVITE"][$code][$i]["LETTER"] = $letter;

        foreach($arResult["ITEMS"] as $key=>$arItem){
            if(mb_substr($arItem["NAME"], 0, 1) === $letter){
                $arResult["ALPHAVITE"][$code][$i]["ITEMS"][] = array(
                    "NAME" => $arItem["NAME"],
                    "DETAIL_PAGE_URL" => $arItem["DETAIL_PAGE_URL"]
                );
                unset($arResult["ITEMS"][$key]);
            }
        }

    }

}
