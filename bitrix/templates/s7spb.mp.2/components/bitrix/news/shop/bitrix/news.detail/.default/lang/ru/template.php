<?
$MESS = array(
    "BREND_TITLE" => "Товары NAME",
    "BREND_SEARCH" => "Поиск товара NAME",
    "COMPANY_SECTIONS" => "КАТЕГОРИИ",
    "COMPANY_WISH_IN" => "В избранное",
    "COMPANY_WISH_OUT" => "В избранном",
    "COMPANY_NAV_MAIN" => "Главная",
    "COMPANY_NAV_CATALOG" => "Товары",
    "COMPANY_NAV_LEADER" => "Лидеры продаж",
    "COMPANY_NAV_DISCOUNT" => "Скидки",
    "COMPANY_NAV_REVIEWS" => "Отзывы",
);