<?

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["DISPLAY_TYPE"] = $arParams["DISPLAY_TYPE"] ? $arParams["DISPLAY_TYPE"] : "card";
$this->SetViewTarget('company_left_area');
?>
<div id="company-left-area">
    <?if(is_array($arResult["PREVIEW_PICTURE"])):?>
        <div class="bordered">
            <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>"
                 width="<?=$arResult["PREVIEW_PICTURE"]["WIDTH"]?>"
                 height="<?=$arResult["PREVIEW_PICTURE"]["HEIGHT"]?>"
                 title="<?=$arResult["PREVIEW_PICTURE"]["TITLE"]?>"
                 alt="..." />
        </div>
    <?endif;?>

    <span class="p3">
            <?=$arResult["NAME"]?>
        <?if(is_array($arResult["DISPLAY_PROPERTIES"]["COMP_NAME_FULL"])):?>
            <p class="text-12 text-gray mt-1"><?=$arResult["DISPLAY_PROPERTIES"]["COMP_NAME_FULL"]["DISPLAY_VALUE"]?></p>
        <?endif;?>
    </span>

    <div class="d-inline-block text-16
            <?if(in_array($arResult["ID"], $arParams["USER_FAVORITES"])):?>
                active
            <?endif;?>
            cursor"
         data-title="<?=Loc::getMessage("COMPANY_WISH_IN")?>"
         data-title-in="<?=Loc::getMessage("COMPANY_WISH_OUT")?>"
         onclick="tradeMark.wish(this)"
         data-item-id="<?=$arResult["ID"]?>">
        <i class="icon icon-favorite square-16"></i>

        <?
        if(in_array($arResult["ID"], $arParams["USER_FAVORITES"])){
            echo Loc::getMessage("COMPANY_WISH_OUT");
        }else{
            echo Loc::getMessage("COMPANY_WISH_IN");
        }
        ?>
    </div>

    <div id="company_search">
        <div class="s7sbp--marketplace--header--search-line--search">
            <form class="s7sbp--marketplace--header--search-line--search--input stitle_form"
                  action="<?=$arResult["DETAIL_PAGE_URL"]?>">
                <input type="text"
                       name="q"
                       value="<?=$arParams["SEARCH_QUERY"]?>"
                       size="40"
                       class="text small_block"
                       maxlength="100"
                       autocomplete="off"
                       placeholder="<?=Loc::getMessage("BREND_SEARCH", array("NAME" => $arResult["NAME"]))?>">
                <div class="s7sbp--marketplace--header--search-line--search--input--category"></div>
                <button type="submit" class="s7sbp--marketplace--header--search-line--search--input--button"></button>
            </form>

        </div>
    </div>
</div>

<div class="elecity-nav__wrapper" id="shop_navigation">
    <ul class="elecity-nav__list site-list">
        <li class="site-list__item current">
            <a href="#" onclick="return tradeMark.scroollToElement(this)" class="site-list__link"><?=Loc::getMessage("COMPANY_NAV_MAIN")?></a>
        </li>
        <li class="site-list__item">
            <a href="#shop_catalog" onclick="return tradeMark.scroollToElement(this)" class="site-list__link"><?=Loc::getMessage("COMPANY_NAV_CATALOG")?></a>
        </li>
        <li class="site-list__item">
            <a href="#shop-top-sale" onclick="return tradeMark.scroollToElement(this)" class="site-list__link"><?=Loc::getMessage("COMPANY_NAV_LEADER")?></a>
        </li>
        <li class="site-list__item">
            <a href="#shop_discont" onclick="return tradeMark.scroollToElement(this)" class="site-list__link"><?=Loc::getMessage("COMPANY_NAV_DISCOUNT")?></a>
        </li>
        <li class="site-list__item">
            <a href="#shop-reviews" onclick="return tradeMark.scroollToElement(this)" class="site-list__link"><?=Loc::getMessage("COMPANY_NAV_REVIEWS")?></a>
        </li>
    </ul>
</div>

<?$this->EndViewTarget(); ?>





<div class="s7sbp--marketplace--section-title">
    <span><?=Loc::getMessage("BREND_TITLE", array("NAME" => $arResult["NAME"]))?></span>
    <span class="s7sbp--marketplace--section-item-type">
	<!--noindex-->
    	<a class="s7sbp--marketplace--section-item-type--icon card <?=$arParams["DISPLAY_TYPE"] == "card" ? " active" : ""?>" href="<?=$arResult["DETAIL_PAGE_URL"]?>?display=card"></a>
		<a class="s7sbp--marketplace--section-item-type--icon list <?=$arParams["DISPLAY_TYPE"] == "line" ? " active" : ""?>" href="<?=$arResult["DETAIL_PAGE_URL"]?>?display=line"></a>
    <!--/noindex-->
	</span>
</div>

