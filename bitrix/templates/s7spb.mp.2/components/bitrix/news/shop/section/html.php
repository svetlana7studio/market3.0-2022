<?
/**
 * Povered by artem@koorochka.com
 * Created 27.08.2020
 * Тут происходит набор параметров для списка и определение режима вывода
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
 */

use Bitrix\Main\Localization\Loc;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->addExternalCss( SITE_TEMPLATE_PATH . "/assets/css/marketplace/catalog.filter.css");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/catalog.section.js");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/catalog.section.css");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/catalog/bitrix.product.item.css");
# search on checkbox
$this->addExternalCss( SITE_TEMPLATE_PATH . "/assets/css/search.checkbox.css");

$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/fancybox/jquery.fancybox.min.css");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.fancybox.min.js");


$arSections = [];

CJSCore::Init(["jquery"]);
?>


<div class="s7spb-container pb6">

<div class="py3">
    <?$APPLICATION->ShowViewContent("company_left_area");?>
</div>

<div class="py3">
    <?$APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        "catalog",
        Array(
            "PATH" => "",
            "SITE_ID" => SITE_ID,
            "START_FROM" => "0",
            "SEF_CATALOG_CODE_POINT" => "catalog",
            "IBLOCK_TYPE" => "marketplace",
            "IBLOCK_ID" => "2",
        ),
        false
    );?>
</div>
<div class="s7spb-row s7spb-row12 mb5">

    <div class="s7spb-col s7spb-col-auto aside-none">
        <div id="s7sbp-sidebur">

            <?$APPLICATION->ShowViewContent("sections.menu");?>

            <?
            $vendorMenu = [
                "VENDOR_ID" => $arResult['VARIABLES']['ELEMENT_ID'],
                "IBLOCK_COMPANY" => $arParams["IBLOCK_ID"],
                //"IBLOCK_CATALOG" => CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id"),
                "CURRENT_USER" => $USER->GetID(),
                "FORUM_MESSAGE_CNT" => 0,
                "SEF" => $arResult,
                "CURRENT_PAGE" => $APPLICATION->GetCurPage()
            ];
            $APPLICATION->IncludeComponent("studio7sbp:vendor.menu","aside", $vendorMenu, false, ["HIDE_ICONS" =>"Y"]);
            ?>


            <?
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.smart.filter",
                "search.checkbox",
                array(
                    //"PROPERTY_UNSET" => ["TRADE_MARK"],
                    "COMPONENT_TEMPLATE" => ".default",
                    "PICKUP_SEARCH_COUNT" => 2,
                    "IBLOCK_TYPE" => $componentSectionParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $componentSectionParams["IBLOCK_ID"],
                    "SECTION_ID" => 0,
                    "SECTION_CODE" => "",
                    "PREFILTER_NAME" => "companyFilter",
                    "FILTER_NAME" => "companyFilter",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "TEMPLATE_THEME" => "blue",
                    "FILTER_VIEW_MODE" => "vertical",
                    'VIEW_MODE' => 'vertical',
                    "POPUP_POSITION" => "left",
                    "DISPLAY_ELEMENT_COUNT" => "Y",
                    "SEF_MODE" => "N",
                    "CACHE_TYPE" => $componentSectionParams["CACHE_TYPE"],
                    "CACHE_TIME" => $componentSectionParams["CACHE_TIME"],
                    "CACHE_GROUPS" => $componentSectionParams["CACHE_GROUPS"],
                    "SAVE_IN_SESSION" => "N",
                    "PAGER_PARAMS_NAME" => "arrPager",
                    "PRICE_CODE" => $componentSectionParams["PRICE_CODE"],
                    "CONVERT_CURRENCY" => "N",
                    "XML_EXPORT" => "N",
                    "SECTION_TITLE" => "-",
                    "SECTION_DESCRIPTION" => "-"
                ),
                false,
                array('HIDE_ICONS' => 'Y')
            );?>

        </div>

    </div>
    <div class="s7spb-col catalog-adept">

        <div class="bg-white border-radius-10 py2 px1 mb3" id="shop_catalog">

            <div class="text-20 text-uppercase p3">
                <span><?=Loc::getMessage("COMPANY_TITLE", array("NAME" => $arResult["BRAND"]["NAME"]))?></span>
            </div>
            <?
            $this->SetViewTarget('resultSorting');
            include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/sort.php");
            $this->EndViewTarget();

            $APPLICATION->ShowViewContent("resultSorting");

            // <editor-fold defaultstate="collapsed" desc=" # catalog.section params">
            $componentSectionParams["sectionItemType"] = $_SESSION['sectionItemType'];
            $componentSectionParams["DISPLAY_TYPE"] = $sectionItemType;
            $componentSectionParams["ELEMENT_SORT_FIELD"] = $sort;
            $componentSectionParams["ELEMENT_SORT_ORDER"] = $sort_order;
            $componentSectionParams["REDIRECT_IF_EMPTY_TO"] = $arResult["FOLDER"];
            // </editor-fold>

            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "",
                $componentSectionParams,
                false,
                array("HIDE_ICONS" => "Y")
            );

            $APPLICATION->ShowViewContent("resultSorting");
            ?>
            <div class="module-pagination-wrapper">
                <?
                $APPLICATION->ShowViewContent("PAGER_SHOW_MORE");
                $APPLICATION->ShowViewContent("DISPLAY_BOTTOM_PAGER");
                ?>
            </div>
        </div>

        <div class="bg-white border-radius-10 py2 px1 mb3" id="shop-top-sale">
            <?
            global $USER_FAVORITES;
            if(!empty($GLOBALS)){
                //$GLOBALS["arrFilterBestseller"]["!PROPERTY_IS_BESTSELLER"] = false;
            }
            ?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.top",
                "best.seller.slider",
                Array(
                    "USER_ID" => $USER->GetID(),
                    "CART" => $arParams["CART"],
                    "USER_FAVORITES" => $USER_FAVORITES,
                    "ACTION_VARIABLE" => "action",
                    "ADD_PROPERTIES_TO_BASKET" => "N",
                    "ADD_TO_BASKET_ACTION" => "ADD",
                    "BASKET_URL" => "/basket/",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
                    "COMPATIBLE_MODE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "DETAIL_URL" => "",
                    "DISPLAY_COMPARE" => "N",
                    "ELEMENT_COUNT" => "10",
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_FIELD2" => "id",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "ELEMENT_SORT_ORDER2" => "desc",
                    "ENLARGE_PRODUCT" => "STRICT",
                    "FILTER_NAME" => "arrFilterBestseller",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                    "IBLOCK_ID" => "2",
                    "IBLOCK_TYPE" => "marketplace",
                    "LINE_ELEMENT_COUNT" => "5",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_COMPARE" => "Сравнить",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_NOT_AVAILABLE" => "Зарегистрируйтесь, чтобы увидеть цены",
                    "MESS_BLOCK_LABEL" => "Бестселлеры",
                    "BLOCK_LABEL_TYPE" => "BESTSELLERS",
                    "OFFERS_LIMIT" => "5",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRICE_CODE" => ["BASE"],
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "PRODUCT_PROPERTIES" => array(""),
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'5','BIG_DATA':false},{'VARIANT':'5','BIG_DATA':false},{'VARIANT':'5','BIG_DATA':false}]",
                    "PRODUCT_SUBSCRIPTION" => "Y",
                    "PROPERTY_CODE" => array("MOQ",""),
                    "ROTATE_TIMER" => "30",
                    "SECTION_URL" => "",
                    "SEF_MODE" => "N",
                    "SHOW_CLOSE_POPUP" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_MAX_QUANTITY" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "SHOW_PAGINATION" => "Y",
                    "SHOW_PRICE_COUNT" => "1",
                    "SHOW_SLIDER" => "N",
                    "SLIDER_INTERVAL" => "3000",
                    "SLIDER_PROGRESS" => "N",
                    "TEMPLATE_THEME" => "blue",
                    "USE_ENHANCED_ECOMMERCE" => "N",
                    "USE_PRICE_COUNT" => "N",
                    "USE_PRODUCT_QUANTITY" => "Y",
                    "VIEW_MODE" => "SECTION"
                )
            );?>
        </div>

        <div class="bg-white border-radius-10 py2 px1 mb3" id="shop-reviews">
            <?$APPLICATION->IncludeComponent(
                "bitrix:forum.topic.reviews",
                "main",
                [
                    "MESSAGE_TITLE" => "Ваш отзыв",
                    "SHOW_AVATAR" => "N",
                    "SHOW_RATING" => "N",
                    "SHOW_SUBSCRIBE" => "N",
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
                    "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
                    "PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
                    "FORUM_ID" => $arParams["FORUM_ID"],
                    "URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
                    "SHOW_LINK_TO_FORUM" => "N", //$arParams["SHOW_LINK_TO_FORUM"],
                    "DATE_TIME_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
                    "ELEMENT_ID" => $arResult['VARIABLES']['ELEMENT_ID'],
                    "AJAX_POST" => "N", // $arParams["REVIEW_AJAX_POST"]
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "URL_TEMPLATES_DETAIL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
                ],
                false,
                ['HIDE_ICONS' => 'Y']
            );?>
        </div>


        <div class="bg-white position-relative bg-warning" id="shop_discont">
            <div class="spb7-title bg-primary text-white ml5">Рекомендуем</div>
            <div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.top",
                    "recomend.slider",
                    array(
                        "IBLOCK_ID" => "2",
                        "COMPONENT_TEMPLATE" => "recomend.slider",
                        "IBLOCK_TYPE" => "marketplace",
                        "FILTER_NAME" => "arrFilterNast",
                        "CUSTOM_FILTER" => "",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                        "ELEMENT_SORT_FIELD" => "sort",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER2" => "desc",
                        "ELEMENT_COUNT" => "10",
                        "LINE_ELEMENT_COUNT" => "5",
                        "PROPERTY_CODE" => array(
                            0 => "ARTICLE",
                            1 => "MOQ",
                            2 => 'TRADE_MARK',
                            3 => 'ANYTOS_VENDOR'
                        ),
                        "PROPERTY_CODE_MOBILE" => array(
                        ),
                        "OFFERS_LIMIT" => "0",
                        "USER_FAVORITES" => "",
                        "TEMPLATE_THEME" => "blue",
                        "ADD_PICT_PROP" => "-",
                        "LABEL_PROP" => "",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_OLD_PRICE" => "N",
                        "SHOW_MAX_QUANTITY" => "N",
                        "SHOW_CLOSE_POPUP" => "N",
                        "ROTATE_TIMER" => "30",
                        "SHOW_PAGINATION" => "N",
                        "PRODUCT_SUBSCRIPTION" => "Y",
                        "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                        "ENLARGE_PRODUCT" => "STRICT",
                        "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                        "SHOW_SLIDER" => "Y",
                        "SLIDER_INTERVAL" => "3000",
                        "SLIDER_PROGRESS" => "N",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "MESS_BLOCK_LABEL" => "",
                        "BLOCK_LABEL_TYPE" => "RECCOMEND",
                        "SECTION_URL" => "",
                        "DETAIL_URL" => "",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "SEF_MODE" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_FILTER" => "N",
                        "ACTION_VARIABLE" => "action",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRICE_CODE" => array(
                            0 => "BASE"
                        ),
                        "USE_PRICE_COUNT" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "PRICE_VAT_INCLUDE" => "Y",
                        "CONVERT_CURRENCY" => "N",
                        "BASKET_URL" => "/basket/",
                        "USE_PRODUCT_QUANTITY" => "Y",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRODUCT_PROPERTIES" => array(
                        ),
                        "ADD_TO_BASKET_ACTION" => "ADD",
                        "DISPLAY_COMPARE" => "N",
                        "MESS_BTN_COMPARE" => "Сравнить",
                        "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
                        "USE_ENHANCED_ECOMMERCE" => "N",
                        "COMPATIBLE_MODE" => "Y"
                    ),
                    false
                );?>
            </div>
        </div>

    </div>

</div>
<!--- End --->