<?
/**
 * Povered by artem@koorochka.com
 * Created 27.08.2020
 * Тут происходит набор параметров для списка и определение режима вывода
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->RestartBuffer();

$this->SetViewTarget('resultSorting');
include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/sort.php");
$this->EndViewTarget();

// <editor-fold defaultstate="collapsed" desc=" # catalog.section params">
$componentSectionParams["sectionItemType"] = $_SESSION['sectionItemType'];
$componentSectionParams["DISPLAY_TYPE"] = $sectionItemType;
$componentSectionParams["ELEMENT_SORT_FIELD"] = $sort;
$componentSectionParams["ELEMENT_SORT_ORDER"] = $sort_order;
$componentSectionParams["AJAX_MORE_ITEMS"] = "Y";
// </editor-fold>

$intSectionID = $APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "",
    $componentSectionParams,
    false,
    ['HIDE_ICONS' => 'Y']
);