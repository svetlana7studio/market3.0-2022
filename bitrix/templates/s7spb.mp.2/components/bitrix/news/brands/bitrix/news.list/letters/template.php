<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?><?foreach ($arResult["ALPHAVITE"] as $alphavite):?>
    <?foreach ($alphavite as $letter):?>
        <?if(!empty($letter["ITEMS"])):?>

            <div class="s7sbp--marketplace--section-title" id="brand_<?=$letter["LETTER"]?>">
                <span><?=$letter["LETTER"]?></span>
            </div>


            <?
            $count = 4;
            $rowItems = count($letter["ITEMS"]) / $count;
            $rowItems = ceil($rowItems);

            $letter["ITEMS"] = array_chunk($letter["ITEMS"], $rowItems);



            for($row=0; $row < $rowItems; $row++){
                ?><div class="s7spb-row column-brand-mob"><?
                foreach($letter["ITEMS"] as $key=>$arItems):
                    $arItem = $arItems[$row];
                    if(empty($arItem)):
                        ?>
                        <div class="s7spb-col m1"></div>
                    <?else:?>

                        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"
                           class="brend-item text-18 s7spb-col m1">
                            <b><?echo $arItem["NAME"]?></b>
                        </a>
                    <?
                    endif;
                endforeach;
                ?></div><?
            }
            ?>


        <?endif;?>
    <?endforeach;?>
<?endforeach;?>

<?if(!empty($arResult["ALPHAVITE"]["ATHER"]["ITEMS"])):?>
    <div class="s7sbp--marketplace--section-title" id="brand_<?=$arParams["ALPHAVITE"]["ATHER"][0]?>">
        <span><?=$arParams["ALPHAVITE"]["ATHER"][0]?></span>
    </div>

    <div class="s7spb-row">
        <?foreach ($arResult["ALPHAVITE"]["ATHER"]["ITEMS"] as $arItem):?>
            <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"
               class="brend-item text-18 s7spb-col m1">
                <b><?echo $arItem["NAME"]?></b>
            </a>
        <?endforeach;?>
    </div>
<?endif;?>