var shopReviews = {
    toggle: function (t) {
        var link = $(t),
            item = link.parent(),
            detail = item.find(".detail-text");

        if(detail.hasClass("d-none")){
            detail.removeClass("d-none");
            link.text(link.data("hide"));
        }else{
            detail.addClass("d-none");
            link.text(link.data("show"));
        }

        return false;
    },

    d: function (value) {
        console.info(value);
    }
};
