<?
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(empty($arResult["ITEMS"]))
    return;
?>
<div class="s7sbp--marketplace--index--catalog-top mb-3" id="shop-reviews">
    <div class="s7sbp--marketplace--index--catalog-top--title s7sbp--marketplace--index--catalog-top--title_reviews">Отзывы</div>

    <div id="shop-reviews">

        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="ad-grid">
                    <div class="ad-grid-cell">
                        <div class="mb-3"><?$APPLICATION->IncludeComponent(
                                "bitrix:iblock.vote",
                                "product_rating",
                                Array(
                                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                    "ELEMENT_ID" => $arItem["ID"],
                                    "MAX_VOTE" => 5,
                                    "VOTE_NAMES" => array(),
                                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                                    "DISPLAY_AS_RATING" => 'vote_avg'
                                ),
                                $component, array("HIDE_ICONS" =>"Y")
                            );?></div>
                        <div class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
                    </div>
                    <div class="ad-grid-cell">
                        <h3 class="mb-3"><?=$arItem["NAME"]?></h3>
                        <div class="preview-text"><?=$arItem["PREVIEW_TEXT"]?></div>
                        <div class="detail-text d-none alert alert-info"><?=$arItem["DETAIL_TEXT"]?></div>
                        <div class="read-more-btn"
                             data-show="<?=Loc::getMessage("ELEMENT_READ_MORE")?>"
                             data-hide="<?=Loc::getMessage("ELEMENT_HIDE_MORE")?>"
                             onclick="return shopReviews.toggle(this)"><?=Loc::getMessage("ELEMENT_READ_MORE")?></div>
                    </div>
                </div>
            </div>
        <?endforeach;?>

        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <div class="mt-3"><?=$arResult["NAV_STRING"]?></div>
        <?endif;?>
    </div>

</div>