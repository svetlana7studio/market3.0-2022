var tradeMark = {
    mark: null,

    wish: function (t) {
        var link = $(t);
        if(link.hasClass("active")){
            link.removeClass("active");
            link.text(link.data("title"));

        }else{
            link.addClass("active");
            link.text(link.data("title-in"));
        }

        BX.ajax.runAction('studio7spb:marketplace.api.tools.addToWish', {
            data: {
                id: link.data("item-id"),
            }
        });

    }
};