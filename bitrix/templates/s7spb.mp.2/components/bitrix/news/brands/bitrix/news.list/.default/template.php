<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <div class="mb-3"><?=$arResult["NAV_STRING"]?></div>
<?endif;?>


    <div class="s7sbp--marketplace--index--right-block block-one-column">

        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>

            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"
               class="product-item-container position-relative"
               data-entity="item">
                <div class="product-item alert alert-primary" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

                    <!--<div class="item-favorite-fill"></div>-->

                    <?if(is_array($arItem["PREVIEW_PICTURE"])):?>
                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                             width="150">
                    <?else:?>
                        <img src="<?=$this->GetFolder()?>/images/no_photo.png"
                             width="150">
                    <?endif;?>







                    <div class="h5">
                        <?=$arItem["NAME"]?>
                    </div>

                </div>
            </a>
        <?endforeach;?>

    </div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <div class="mt-3"><?=$arResult["NAV_STRING"]?></div>
<?endif;?>