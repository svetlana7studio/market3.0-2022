<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

/*TAGS*/
$arParams["RESIZE"] = [
    "SIZE" => [
        "width" => 400,
        "height" => 400
    ]
];
if (!empty($arResult["ITEMS"]))
{
	foreach ($arResult["ITEMS"] as &$arItem)
	{
        if(is_array($arItem["PREVIEW_PICTURE"]) && $arItem["PREVIEW_PICTURE"]["ID"] > 0){
            $file = CFile::ResizeImageGet(
                $arItem["PREVIEW_PICTURE"]["ID"],
                $arParams["RESIZE"]["SIZE"],
                BX_RESIZE_IMAGE_EXACT,
                true
            );

            $arItem["PREVIEW_PICTURE"]["SRC"] = $file["src"];
            $arItem["PREVIEW_PICTURE"]["WIDTH"] = $file["width"];
            $arItem["PREVIEW_PICTURE"]["HEIGHT"] = $file["height"];
        }else{
            $arItem["PREVIEW_PICTURE"]["SRC"] = SITE_TEMPLATE_PATH . "/images/no_image400.jpg";
        }

	}
	unset($arItem);
}



/* THEME */
$arParams["TEMPLATE_THEME"] = trim($arParams["TEMPLATE_THEME"]);
$arResult["NAV_PARAM"]["TEMPLATE_THEME"] = $arParams["TEMPLATE_THEME"];

$arResult["NAV_STRING"] = $arResult["NAV_RESULT"]->GetPageNavStringEx(
	$navComponentObject,
	$arParams["PAGER_TITLE"],
	$arParams["PAGER_TEMPLATE"],
	$arParams["PAGER_SHOW_ALWAYS"],
	$this->__component,
	$arResult["NAV_PARAM"]
);
