<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Application;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$request = Application::getInstance()->getContext()->getRequest();

$this->setFrameMode(true);

$arResult["ACTSY"] = $APPLICATION->IncludeComponent("studio7sbp:anytos.aktsy.detail", "", [
    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
    "CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
    "PROPERTY_PRODUCTS" => "410", // Идентификатор свойства в котором указаны Товары
    "PROPERTY_SECTION" => "385", // Идентификатор свойства в котором указан Раздел
    "PROPERTY_VENDOR_ARTICLE" => "411" // Идентификатор свойства в котором указан Артикул поставщика
]);

if(is_array($arResult["ACTSY"]["ELEMENT"]) && $arResult["ACTSY"]["ELEMENT"]["ID"] > 0){
    // set title
    $APPLICATION->SetTitle($arResult["ACTSY"]["ELEMENT"]["NAME"]);
    $APPLICATION->SetPageProperty("title", $arResult["ACTSY"]["ELEMENT"]["NAME"]);
    // set chain to breadcrumbs
    $APPLICATION->AddChainItem($arResult["ACTSY"]["ELEMENT"]["NAME"]);
}


# Prepare filter
global $aktsyFilter;



$aktsyFilter["LOGIC"] = "OR";

if((!empty($arResult["ACTSY"]["PRODUCTS"]) && count($arResult["ACTSY"]["PRODUCTS"]) > 0)){
    $aktsyFilter[] = [
        "ID" => $arResult["ACTSY"]["PRODUCTS"]
    ];
}

if((!empty($arResult["ACTSY"]["PRODUCTS_SECTION"]) && count($arResult["ACTSY"]["PRODUCTS_SECTION"]) > 0)){
    $aktsyFilter[] = [
        "SECTION_ID" => $arResult["ACTSY"]["PRODUCTS_SECTION"],
        "INCLUDE_SUBSECTIONS" => "Y"
    ];
}

if((!empty($arResult["ACTSY"]["VENDOR"]) && count($arResult["ACTSY"]["VENDOR"]) > 0)){
    $aktsyFilter[] = [
        "PROPERTY_ANYTOS_VENDOR" => $arResult["ACTSY"]["VENDOR"]
        //"PROPERTY_ANYTOS_VENDOR" => 232
    ];
}

$aktsyFilter = [
    "ACTIVE" => "Y",
    [$aktsyFilter]
];

# User favorites
\CBitrixComponent::includeComponentClass("studio7sbp:favorite");
$favorite = new \studio7sbpFavorite();
if($USER->IsAuthorized()){
    $favorite->onPrepareComponentParams(array("USER_ID" => $USER->GetID()));
}

# component params
$componentSectionParams = [
    "DISPLAY_TYPE" => "card",
    "sectionItemType" => "card",
    "USER_ID" => $USER->GetID(),
    "USER_FAVORITES" => $favorite->getUserFavorite(),
    "EMPTY_PHRASE" => GetMessage("EMPTY_PHRASE"),
    "IBLOCK_TYPE" => "marketplace",
    "IBLOCK_ID" => "2",
    "SECTION_ID" => "",
    "SECTION_CODE" => "",
    "SECTION_USER_FIELDS" => array(
        0 => "",
        1 => "",
    ),
    "FILTER_NAME" => "aktsyFilter",
    "INCLUDE_SUBSECTIONS" => "Y",
    "SHOW_ALL_WO_SECTION" => "Y",
    "CUSTOM_FILTER" => "",
    "HIDE_NOT_AVAILABLE" => "N",
    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
    "ELEMENT_SORT_FIELD" => "id",
    "ELEMENT_SORT_ORDER" => "desc",
    "ELEMENT_SORT_FIELD2" => "id",
    "ELEMENT_SORT_ORDER2" => "desc",
    "OFFERS_SORT_FIELD" => "sort",
    "OFFERS_SORT_ORDER" => "asc",
    "OFFERS_SORT_FIELD2" => "id",
    "OFFERS_SORT_ORDER2" => "desc",
    "PAGE_ELEMENT_COUNT" => "20",
    "LINE_ELEMENT_COUNT" => "5",
    "PROPERTY_CODE" => array(
        0 => "ARTICLE",
        1 => "MOQ",
        2 => 'TRADE_MARK',
        3 => 'ANYTOS_VENDOR'
    ),
    "OFFERS_FIELD_CODE" => array(
        0 => "",
        1 => "",
    ),
    "OFFERS_PROPERTY_CODE" => array(
        0 => "",
        1 => "",
    ),
    "OFFERS_LIMIT" => "5",
    "BACKGROUND_IMAGE" => "-",
    "SECTION_URL" => "",
    "DETAIL_URL" => "",
    "SECTION_ID_VARIABLE" => "SECTION_ID",
    "SEF_MODE" => "N",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_GROUPS" => "N",
    "SET_TITLE" => "Y",
    "SET_BROWSER_TITLE" => "Y",
    "BROWSER_TITLE" => "-",
    "SET_META_KEYWORDS" => "Y",
    "META_KEYWORDS" => "-",
    "SET_META_DESCRIPTION" => "Y",
    "META_DESCRIPTION" => "-",
    "SET_LAST_MODIFIED" => "N",
    "USE_MAIN_ELEMENT_SECTION" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "CACHE_FILTER" => "Y",
    "ACTION_VARIABLE" => "action",
    "PRODUCT_ID_VARIABLE" => "id",
    "PRICE_CODE" => array(
        0 => "price_green",
        1 => "price_yellow",
        2 => "price_red",
    ),
    "USE_PRICE_COUNT" => "N",
    "SHOW_PRICE_COUNT" => "1",
    "PRICE_VAT_INCLUDE" => "Y",
    "CONVERT_CURRENCY" => "N",
    "BASKET_URL" => "/basket/",
    "USE_PRODUCT_QUANTITY" => "Y",
    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
    "ADD_PROPERTIES_TO_BASKET" => "Y",
    "PRODUCT_PROPS_VARIABLE" => "prop",
    "PARTIAL_PRODUCT_PROPERTIES" => "N",
    "PRODUCT_PROPERTIES" => array(
    ),
    "OFFERS_CART_PROPERTIES" => array(
    ),
    "DISPLAY_COMPARE" => "N",
    "PAGER_TEMPLATE" => "anitos",
    "DISPLAY_TOP_PAGER" => "Y",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Товары",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "PAGER_BASE_LINK_ENABLE" => "N",
    "SET_STATUS_404" => "N",
    "SHOW_404" => "N",
    "MESSAGE_404" => "",
    "COMPATIBLE_MODE" => "Y",
    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
    "PROPERTY_CODE_MOBILE" => array(
    ),
    "TEMPLATE_THEME" => "blue",
    //"TEMPLATE_THEME_ITEM_MODE" => "favorite",
    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
    "ENLARGE_PRODUCT" => "STRICT",
    "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
    "SHOW_SLIDER" => "Y",
    "PRODUCT_DISPLAY_MODE" => "N",
    "ADD_PICT_PROP" => "-",
    "LABEL_PROP" => array(
    ),
    "PRODUCT_SUBSCRIPTION" => "Y",
    "SHOW_DISCOUNT_PERCENT" => "N",
    "SHOW_OLD_PRICE" => "N",
    "SHOW_MAX_QUANTITY" => "N",
    "SHOW_CLOSE_POPUP" => "N",
    "MESS_BTN_BUY" => "Купить",
    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
    "MESS_BTN_SUBSCRIBE" => "Подписаться",
    "MESS_BTN_DETAIL" => "Подробнее",
    "MESS_NOT_AVAILABLE" => "Зарегистрируйтесь, чтобы увидеть цены",
    "RCM_TYPE" => "personal",
    "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
    "SHOW_FROM_SECTION" => "N",
    "ADD_TO_BASKET_ACTION" => "ADD",
    "USE_ENHANCED_ECOMMERCE" => "N",
    "LAZY_LOAD" => "N",
    "LOAD_ON_SCROLL" => "N"
];

# Количество на странице
$show_by = intval($request->get("show_by"));
if(!$show_by){
    $show_by = 24;
}
$componentSectionParams["PAGE_ELEMENT_COUNT"] = $show_by;
$componentSectionParams["~PAGE_ELEMENT_COUNT"] = $show_by;
$componentSectionParams["LANDING_PAGE_ELEMENT_COUNT"] = $show_by;
$componentSectionParams["~LANDING_PAGE_ELEMENT_COUNT"] = $show_by;

# mode=ex
# ajax_get=Y

if($request->get("ajax_get") == "Y")
{
    include "section/ajax.php";
}
elseif($request->get("mode") == "ex")
{
    include "section/excel.php";
}
else{
    include "section/html.php";
}