<?
/**
 * Povered by artem@koorochka.com
 * Created 27.08.2020
 * Тут происходит набор параметров для списка и определение режима вывода
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
 */

use Bitrix\Main\Localization\Loc;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
$this->addExternalCss( SITE_TEMPLATE_PATH . "/assets/css/marketplace/catalog.filter.css");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/catalog.section.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/components/bitrix/catalog/main/script.js");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/catalog.section.css");
# search on checkbox
$this->addExternalCss( SITE_TEMPLATE_PATH . "/assets/css/search.checkbox.css");
$arSections = [];
?>


<div class="s7spb-container pb6">

    <div class="py3">
        <?$APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "catalog",
            Array(
                "PATH" => "",
                "SITE_ID" => SITE_ID,
                "START_FROM" => "0",
                "SEF_CATALOG_CODE_POINT" => "catalog",
                "IBLOCK_TYPE" => "marketplace",
                "IBLOCK_ID" => "2",
            ),
            false
        );?>
    </div>
    <div class="s7spb-row s7spb-row12 mb5">

        <div class="s7spb-col s7spb-col-auto aside-none">
            <div id="s7sbp-sidebur">

                <?
                /*
                $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "s7sbp.catalog.sections",
                    array(
                        "PARENT_SECTIONS" => $arSections,
                        "COMPONENT_TEMPLATE" => "s7sbp.catalog.sections",
                        "ROOT_MENU_TYPE" => "catalog",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "360000000",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "3",
                        "CHILD_MENU_TYPE" => "catalog",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false,
                    array('HIDE_ICONS' => 'Y')
                );
                */

                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.smart.filter",
                    "search.checkbox",
                    array(
                        //"PROPERTY_UNSET" => ["TRADE_MARK"],
                        "COMPONENT_TEMPLATE" => ".default",
                        "PICKUP_SEARCH_COUNT" => 20,
                        "IBLOCK_TYPE" => $componentSectionParams["IBLOCK_TYPE"],
                        "IBLOCK_ID" => $componentSectionParams["IBLOCK_ID"],
                        "SECTION_ID" => 0,
                        "SECTION_CODE" => "",
                        //"PRICE_CODE" => $componentSectionParams["PRICE_CODE"],
                        "PRICE_CODE" => ["price_green"],
                        "PREFILTER_NAME" => "aktsyFilter",
                        "FILTER_NAME" => "aktsyFilter",

                        "HIDE_NOT_AVAILABLE" => "N",
                        "TEMPLATE_THEME" => "blue",
                        "FILTER_VIEW_MODE" => "vertical",
                        "POPUP_POSITION" => "left",
                        "DISPLAY_ELEMENT_COUNT" => "Y",
                        "SEF_MODE" => "N",
                        "CACHE_TYPE" => $componentSectionParams["CACHE_TYPE"],
                        "CACHE_TIME" => $componentSectionParams["CACHE_TIME"],
                        "CACHE_GROUPS" => $componentSectionParams["CACHE_GROUPS"],
                        "SAVE_IN_SESSION" => "N",
                        "PAGER_PARAMS_NAME" => "arrPager",
                        "CONVERT_CURRENCY" => "N",
                        "XML_EXPORT" => "N",
                        "SECTION_TITLE" => "-",
                        "SECTION_DESCRIPTION" => "-",
                        'VIEW_MODE' => 'vertical',
                    ),
                    false,
                    array('HIDE_ICONS' => 'Y')
                );?>

            </div>

        </div>
        <div class="s7spb-col catalog-adept">

            <div class="bg-white border-radius-10 py2 px1">

                <div class="text-20 text-uppercase p3">
                    <span><?=Loc::getMessage("COMPANY_TITLE", array("NAME" => $arResult["ACTSY"]["ELEMENT"]["NAME"]))?></span>
                </div>

                <?if(!empty($arResult["ACTSY"]["ELEMENT"]["PREVIEW_TEXT"])):?>
                    <div class="p3"><?=$arResult["ACTSY"]["ELEMENT"]["PREVIEW_TEXT"]?></div>
                <?endif;?>

                <?if(!empty($arResult["ACTSY"]["ELEMENT"]["DETAIL_TEXT"])):?>
                    <div class="p3"><?=$arResult["ACTSY"]["ELEMENT"]["DETAIL_TEXT"]?></div>
                <?endif;?>

                <?
                $this->SetViewTarget('resultSorting');
                include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/sort.php");
                $this->EndViewTarget();

                $APPLICATION->ShowViewContent("resultSorting");

                // <editor-fold defaultstate="collapsed" desc=" # catalog.section params">
                $componentSectionParams["sectionItemType"] = $_SESSION['sectionItemType'];
                $componentSectionParams["DISPLAY_TYPE"] = $sectionItemType;
                $componentSectionParams["ELEMENT_SORT_FIELD"] = $sort;
                $componentSectionParams["ELEMENT_SORT_ORDER"] = $sort_order;
                $componentSectionParams["REDIRECT_IF_EMPTY_TO"] = $arResult["FOLDER"];
                // </editor-fold>

                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section",
                    "",
                    $componentSectionParams,
                    false,
                    array("HIDE_ICONS" => "Y")
                );

                $APPLICATION->ShowViewContent("resultSorting");
                ?>
                <div class="module-pagination-wrapper">
                    <?
                    $APPLICATION->ShowViewContent("PAGER_SHOW_MORE");
                    $APPLICATION->ShowViewContent("DISPLAY_BOTTOM_PAGER");
                    ?>
                </div>
            </div>

        </div>

    </div>
</div>