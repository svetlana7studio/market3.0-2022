<?
/**
 * Povered by artem@koorochka.com
 * Created 27.08.2020
 * Тут происходит набор параметров для списка и определение режима вывода
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDatabase $DB
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arSections = [];
$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "s7sbp.catalog.sections",
    array(
        //"PARENT_SECTIONS" => $arSections,
        "COMPONENT_TEMPLATE" => "s7sbp.catalog.sections",
        "ROOT_MENU_TYPE" => "catalog",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "360000000",
        "MENU_CACHE_USE_GROUPS" => "N",
        "MENU_CACHE_GET_VARS" => array(
        ),
        "MAX_LEVEL" => "3",
        "CHILD_MENU_TYPE" => "catalog",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N"
    ),
    false,
    array('HIDE_ICONS' => 'Y')
);
$APPLICATION->RestartBuffer();

$this->SetViewTarget('resultSorting');
include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/sort.php");
$this->EndViewTarget();

// <editor-fold defaultstate="collapsed" desc=" # catalog.section params">
$componentSectionParams["sectionItemType"] = $_SESSION['sectionItemType'];
$componentSectionParams["DISPLAY_TYPE"] = $sectionItemType;
$componentSectionParams["ELEMENT_SORT_FIELD"] = $sort;
$componentSectionParams["ELEMENT_SORT_ORDER"] = $sort_order;
$componentSectionParams["AJAX_MORE_ITEMS"] = "Y";
// </editor-fold>

$intSectionID = $APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "",
    $componentSectionParams,
    false,
    ['HIDE_ICONS' => 'Y']
);