<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult['ITEMS']))
    return;

$generalParams = array(
    'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
    'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE']
);

if(is_set($arParams["USER_FAVORITES"])){
    $generalParams["USER_FAVORITES"] = $arParams["USER_FAVORITES"];
}

?>

<div class="position-relative">

    <div class="hm306 s7spb-row s7spb-row-nowrap s7spb-row-between" id="news-items">

        <?
        foreach ($arResult['ITEMS'] as $key=>$arItem):

            $generalParams["KEY"] = $key;
            $arItem["JS_ID_BASIS"] = "news-item";

            $APPLICATION->IncludeComponent(
                'bitrix:catalog.item',
                'catalog.top',
                array(
                    'RESULT' => array(
                        'ITEM' => $arItem,
                        'PRICES' => $arResult["PRICES"],
                        'PRODUCT_PRICES' => $arResult["PRODUCT_PRICES"],
                        'TYPE' => 'card',
                        'BIG_LABEL' => 'N',
                        'BIG_DISCOUNT_PERCENT' => 'N',
                        'BIG_BUTTONS' => 'N',
                        'SCALABLE' => 'N'
                    ),
                    'PARAMS' => $generalParams
                        + array('SKU_PROPS' => $arResult['SKU_PROPS'][$arItem['IBLOCK_ID']])
                ),
                $component,
                array('HIDE_ICONS' => 'Y')
            );

            unset($arItem);

        endforeach;
        ?>

    </div>

    <div class="carousel-controls">
        <div onclick="koorochkaNewsdSlider.slidePrev();" class="carousel-control-prev"></div>
        <div onclick="koorochkaNewsdSlider.slideNext();" class="carousel-control-next"></div>
    </div>
    <!-- End Partners -->

</div>