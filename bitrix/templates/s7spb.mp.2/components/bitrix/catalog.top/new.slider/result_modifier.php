<?
/**
 *
 */

$arParams["RESIZE"] = [
    "height" => 200,
    "width" => 200
];
$arParams["RESIZE_TYPE"] = BX_RESIZE_IMAGE_PROPORTIONAL; // BX_RESIZE_IMAGE_EXACT

if(!empty($arResult['ITEMS'])){

    foreach ($arResult['ITEMS'] as $i=>$arItem){

        if(is_array($arItem["DETAIL_PICTURE"]) && empty($arItem["PREVIEW_PICTURE"])){
            $arItem["PREVIEW_PICTURE"] = $arItem["DETAIL_PICTURE"];
        }

        // preview image
        if(is_array($arItem["PREVIEW_PICTURE"])){
            $arItem["~PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                $arItem["PREVIEW_PICTURE"]["ID"],
                $arParams["RESIZE"],
                $arParams["RESIZE_TYPE"],
                true
            );
            $arItem["PREVIEW_PICTURE"]["SRC"] = $arItem["~PREVIEW_PICTURE"]["src"];
            $arItem["PREVIEW_PICTURE"]["WIDTH"] = $arItem["~PREVIEW_PICTURE"]["width"];
            $arItem["PREVIEW_PICTURE"]["HEIGHT"] = $arItem["~PREVIEW_PICTURE"]["height"];
        }else{
            $arItem["PREVIEW_PICTURE"]["SRC"] = SITE_TEMPLATE_PATH . "/images/no_image200.jpg";
        }
        $arResult['ITEMS'][$i]["PREVIEW_PICTURE"] = $arItem["PREVIEW_PICTURE"];

        $arResult['ITEMS'][$i]["STIKER"][] = [
            "CODE" => $arItem["PROPERTIES"]["DISCOUNT"]["VALUE_XML_ID"][$num],
            "TITLE" => $arItem["PROPERTIES"]["DISCOUNT"]["VALUE"][$num],
            "CLASS" => $arItem["PROPERTIES"]["DISCOUNT"]["CLASS"]
        ];

    }

}
