<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult['ITEMS']))
    return;

$generalParams = array(
    'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
    'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE']
);

if(is_set($arParams["USER_FAVORITES"])){
    $generalParams["USER_FAVORITES"] = $arParams["USER_FAVORITES"];
}

?>


<div class="s7spb-row">
    <?
    foreach ($arResult['ITEMS'] as $key=>$arItem):
        $generalParams["KEY"] = $key;
    ?>

        <div class="s7spb-col-3">
            <?
            $APPLICATION->IncludeComponent(
                'bitrix:catalog.item',
                '',
                [
                    'RESULT' => [
                        'ITEM' => $arItem,
                        'PRICES' => $arResult["PRICES"],
                        'PRODUCT_PRICES' => $arResult["PRODUCT_PRICES"],
                        'TYPE' => 'card',
                        'BIG_LABEL' => 'N',
                        'BIG_DISCOUNT_PERCENT' => 'N',
                        'BIG_BUTTONS' => 'N',
                        'SCALABLE' => 'N'
                    ],
                    'PARAMS' => $generalParams
                ],
                $component,
                ['HIDE_ICONS' => 'Y']
            );
            ?>
        </div>

    <?
        unset($arItem);
    endforeach;
    ?>
</div>