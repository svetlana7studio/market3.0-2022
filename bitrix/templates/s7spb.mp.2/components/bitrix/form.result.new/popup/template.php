<?
/**
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//b_dump($arResult);
?><?
if ($arResult["isFormErrors"] == "Y"){
    $arResult["FORM_ERRORS_TEXT"] = str_replace(array("\"", "'", "&quot;", "*"), "", $arResult["FORM_ERRORS_TEXT"]);
    $arResult["FORM_ERRORS_TEXT"] = str_replace(GetMessage("FORM_ERROR_AGREE_REPLACE"), GetMessage("FORM_ERROR_AGREE"), $arResult["FORM_ERRORS_TEXT"]);
    echo $arResult["FORM_ERRORS_TEXT"];
}

echo $arResult["FORM_NOTE"];

if ($arResult["isFormNote"] != "Y") {
    // onsubmit="return wPopup.submit()"
    $arResult["FORM_HEADER"] = str_replace('enctype="multipart/form-data"', 'onsubmit="return ' . $arParams["JS_OBJ"] . '.submit(' . $arParams["JS_OBJ"] . ')"', $arResult["FORM_HEADER"]);
    echo $arResult["FORM_HEADER"];
}

foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
{
    $class = "form-control";

    switch ($FIELD_SID){
        case "PHONE":
            $class .= " widget-popup--phone-mask";
            break;
        case "AGREE":
            //$arQuestion["HTML_CODE"] = str_replace($arQuestion["STRUCTURE"][0]["MESSAGE"], $arQuestion["CAPTION"], $arQuestion["HTML_CODE"]);
            //$arQuestion["HTML_CODE"] = str_replace('<label for="' . $arQuestion["STRUCTURE"][0]["ID"] . '">', '', $arQuestion["HTML_CODE"]);
            //$arQuestion["HTML_CODE"] = str_replace('</label>', '', $arQuestion["HTML_CODE"]);
            //$arQuestion["CAPTION"] = null;
            break;
    }

    $arQuestion["HTML_CODE"] = str_replace(array("inputtext", "form-controlarea", "inputselect"), $class, $arQuestion["HTML_CODE"]);

    if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
    {
        echo $arQuestion["HTML_CODE"];
    }
    else if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == "text")
    {
        ?>
        <div class="row">

            <?if(!empty($arQuestion["CAPTION"])):?>
                <div class="caption">
                    <?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
                        <span class="error-fld" title="<?=htmlspecialcharsbx($arResult["FORM_ERRORS"][$FIELD_SID])?>"></span>
                    <?endif;?>
                    <?//=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?//=$arResult["REQUIRED_SIGN"];?><?endif;?>
                    <?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>
                </div>
            <?endif;?>

            <div class="field in-f">
                <input type="text"
                       class="inputtext form-control<? if($arQuestion['STRUCTURE'][0]['ID'] == 1): ?> widget-popup--phone-mask<? endif; ?>"
                       name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID'];?>"
                       value=""
                       placeholder="<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?>*<?endif;?>"
                       size="0" required>
            </div>
        </div>

        <?
    }
    else if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == "checkbox")
    {
        ?>
        <div class="row">

            <?if(!empty($arQuestion["CAPTION"])):?>
                <div class="caption">
                    <?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
                        <span class="error-fld" title="<?=htmlspecialcharsbx($arResult["FORM_ERRORS"][$FIELD_SID])?>"></span>
                    <?endif;?>
                    <?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>
                </div>
            <?endif;?>

            <div class="field in-f agree_row">
                <input  class="agree_style_checkbox"
                        type="checkbox"
                        id="<?=$arQuestion['STRUCTURE'][0]['ID']?>"
                        name="form_checkbox_<?=$FIELD_SID?>[]"
                        value="3" required>
                <label for="<?=$arQuestion['STRUCTURE'][0]['ID']?>"> <?=$arQuestion["CAPTION"]?></label>
                <?//=$arQuestion["HTML_CODE"]?>
            </div>
        </div>

        <?
    }
} //endwhile
?>

<?
if($arResult["isUseCaptcha"] == "Y")
{
    ?>


    <div class="row">
        
        <div class="field in-f">
            <input type="hidden"
                   name="captcha_sid"
                   value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>"
                 width="180"
                 class="img-thumbnail img-cap"
                 height="40" />

            <input type="text"
                   name="captcha_word"
                   size="30"
                   maxlength="50"
                   value=""
                   class="form-control form-te" required/>
            <i><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></i>
        </div>
    </div>

    <?
} // isUseCaptcha
?>

    <input type="hidden"
           name="mode"
           value="<?=$arParams["JS_OBJ"]?>" />

    <input type="hidden"
           name="note"
           value="<?=$arParams["FORM_NOTE_ADDOK"]?>" />

    <input type="hidden"
           name="web_form_apply"
           value="Y" />

    <div class="widget-popup--body--form--field">
        <button type="submit" class="widget-popup--body--form--field--btn text-semibold btn--mm"><?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?></button>
    </div>

<?=$arResult["FORM_FOOTER"]?>