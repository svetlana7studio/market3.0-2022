<?
$MESS["CATALOG_PERSONAL_RECOM"] = "Персональные рекомендации";
$MESS["SECT_TITLE"] = "Новинки";
$MESS["SECT_SORT_TITLE"] = "Сортировать:";
$MESS["SECT_SORT_SHOWS"] = "По популярности";
$MESS["SECT_SORT_NAME"] = "По алфавиту";
$MESS["SECT_SORT_PRICE"] = "По цене";
$MESS["SECT_SORT_QUANTITY"] = "По наличию";
$MESS["SECT_SORT_CATALOG_AVAILABLE"] = "По наличию";
