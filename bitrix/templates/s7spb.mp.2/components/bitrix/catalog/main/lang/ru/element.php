<?
$MESS = [
    "ELEMENT_REATING" => "Рейтинг",
    "ELEMENT_FAVORITE_WONT" => "В избранное",
    "ELEMENT_FAVORITE_IN" => "В избранном",
    "ELEMENT_SHARE" => "Поделиться",
    "ELEMENT_RECOMEND_TITLE" => "C этим товаром покупают",
    "ELEMENT_VIEWED_TITLE" => "Вы смотрели",
    "ELEMENT_SAME_TITLE" => "Похожие товары",

    "PROPERTIES_FORUM_MESSAGE_CNT" => "NUM отзыв",
    "TSB1_WORD_OBNOVL_END1" => "ов",
    "TSB1_WORD_OBNOVL_END2" => "ов",
    "TSB1_WORD_OBNOVL_END3" => "",
    "TSB1_WORD_OBNOVL_END4" => "а",

    "ELEMENT_H_COMPANY_TITLE" => "Магазин",
    "ELEMENT_H_COMPANY_ABOUT" => "о магазине",
    "ELEMENT_H_COMPANY_PRODUCTS" => "Другие товары этого продавца"
];