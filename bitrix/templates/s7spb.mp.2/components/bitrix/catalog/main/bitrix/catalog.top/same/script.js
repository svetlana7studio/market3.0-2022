(function(window){
    'use strict';

    if (window.koorochkaSamedSlider)
        return;

    window.koorochkaSamedSlider = {
        id: "same-items",
        obj: null,
        itemClass: "same-item",
        items: [],
        count: 0,
        sliderWidth: 0, // ширина видимой области
        itemWidth: 0, // ширина одной ячейки слайдера
        sliderWidthCount: 0, // количество видимых ячеек
        wait: null,
        currentItem: 0,
        interval: 4000,
        position: {
            left: 0,
            top: 0
        },

        init: function(){
            setTimeout(function () {
                window.koorochkaSamedSlider.create();
                //window.koorochkaSamedSlider.automatic();
            }, this.preloader);
        },
        create: function(){
            this.obj = BX(this.id);
            this.items = BX.findChildren(BX(this.id), {className: this.itemClass}, false);
            this.count = this.items.length;
            if(this.count > 0){
                this.sliderWidth = BX.width(this.obj);
                this.itemWidth = BX.width(BX(this.items[0]));
                this.sliderWidthCount = this.sliderWidth / this.itemWidth;
            }
            this.sliderWidthCount = Math.ceil(this.sliderWidthCount);
            //this.sliderWidthCount = Math.floor(this.sliderWidthCount);
        },
        slideNext: function(){

            // step
            if(this.currentItem > (this.items.length - this.sliderWidthCount)){
                this.currentItem = 0;
            }else{
                this.currentItem++;
                if(this.currentItem > this.items.length)
                    this.currentItem = 0;
            }

            this.currentContext(this.items[this.currentItem]);

        },

        slidePrev: function(){
            // step
            this.currentItem--;
            if(this.currentItem < 0){
                this.currentItem = 0;
            }

            this.currentContext(this.items[this.currentItem]);
        },
        currentContext: function(element) {
            if(element == undefined)
            {
                this.currentItem = 0;
                element = this.items[this.currentItem];
            }

            element = this.currentItem;
            element = document.getElementById('same-item-' + element);
            element = element.offsetLeft;
            BX(this.id).scrollLeft = element;
            //this.automatic();
        },

        automatic: function() {
            this.stopAutomatic();
            this.wait = setTimeout(function () {
                koorochkaSamedSlider.slideNext();
                koorochkaSamedSlider.automatic();
            }, this.interval);
        },

        stopAutomatic: function() {
            clearTimeout(this.wait);
        },
        setItemPosition: function (item) {
            var position = $(item).position();
            // var offset = $(item).position();
            //this.position.top = offset.top;
            this.position.left = parseInt(position.left);


        },
        setItemStylePosition: function (item) {
            //item.style.top = this.position.top;
            //item.style.left = this.position.left;
            $(item).find(".s7spb-product-cell").attr("style", "left:" + this.position.left + "px;");
        }

    };
})(window);

BX.ready(function () {
    window.koorochkaSamedSlider.init();
    $("." + koorochkaSamedSlider.itemClass).hover(function () {
        koorochkaSamedSlider.setItemPosition(this);
        koorochkaSamedSlider.setItemStylePosition(this);
    }, function(){

    });
});
