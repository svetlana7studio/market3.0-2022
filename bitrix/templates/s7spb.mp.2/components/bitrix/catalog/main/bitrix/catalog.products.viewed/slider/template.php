<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult['ITEMS']))
    return;

$arParams["LINE_ELEMENT_COUNT"] = 3;

Loc::loadLanguageFile(__FILE__);


$generalParams = array(

);

if(is_set($arParams["USER_FAVORITES"])){
    $generalParams["USER_FAVORITES"] = $arParams["USER_FAVORITES"];
}
?>


<div class="position-relative">


    <div class="hm306 s7spb-row s7spb-row-justify-start d-overflow-hidden" id="products-viewed-items">

        <?
        foreach ($arResult['ITEMS'] as $key=>$arItem):

            $generalParams["KEY"] = $key;

            $APPLICATION->IncludeComponent(
                'bitrix:catalog.item',
                'products.viewed',
                array(
                    'RESULT' => array(
                        'ITEM' => $arItem,
                        "BUY_URL" => $arResult["BUY_URL_TEMPLATE"] . "/basket/?action_cpv=BUY&id=#ID#7777777",
                        "BUY_URL" => str_replace("#ID#", $arItem["ID"], $arResult["BUY_URL_TEMPLATE"]),
                        'PRICES' => $arResult["PRICES"],
                        'PRODUCT_PRICES' => $arResult["PRODUCT_PRICES"],
                        'TYPE' => 'card',
                        'BIG_LABEL' => 'N',
                        'BIG_DISCOUNT_PERCENT' => 'N',
                        'BIG_BUTTONS' => 'N',
                        'SCALABLE' => 'N'
                    ),
                    'PARAMS' => $generalParams
                        + array('SKU_PROPS' => $arResult['SKU_PROPS'][$arItem['IBLOCK_ID']])
                ),
                $component,
                array('HIDE_ICONS' => 'Y')
            );

            unset($generalParams, $arItem);

        endforeach;
        ?>

    </div>

    <?if(count($arResult['ITEMS']) > $arParams["LINE_ELEMENT_COUNT"]):?>
        <div class="carousel-controls">
            <div onclick="koorochkaProductsViewedSlider.slidePrev();" class="carousel-control-prev"></div>
            <div onclick="koorochkaProductsViewedSlider.slideNext();" class="carousel-control-next"></div>
        </div>
    <?endif;?>


</div>