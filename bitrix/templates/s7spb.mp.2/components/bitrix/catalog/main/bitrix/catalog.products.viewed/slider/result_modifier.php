<?
/**
 *
 */

use Bitrix\Catalog\PriceTable;

$arParams["RESIZE"] = [
    "height" => 200,
    "width" => 200
];
if(!empty($arResult['ITEMS'])){
    $ID = [];
    foreach ($arResult['ITEMS'] as $i=>$arItem){
        // collecting ids
        $ID[] = $arItem["ID"];
        // preview image
        if(is_array($arItem["PREVIEW_PICTURE"])){
            $arItem["~PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                $arItem["PREVIEW_PICTURE"]["ID"],
                $arParams["RESIZE"],
                BX_RESIZE_IMAGE_EXACT,
                true
            );
            $arItem["PREVIEW_PICTURE"]["SRC"] = $arItem["~PREVIEW_PICTURE"]["src"];
            $arItem["PREVIEW_PICTURE"]["WIDTH"] = $arItem["~PREVIEW_PICTURE"]["width"];
            $arItem["PREVIEW_PICTURE"]["HEIGHT"] = $arItem["~PREVIEW_PICTURE"]["height"];
        }else{
            $arItem["PREVIEW_PICTURE"]["SRC"] = SITE_TEMPLATE_PATH . "/images/no_image200.jpg";
        }
        $arResult['ITEMS'][$i]["PREVIEW_PICTURE"] = $arItem["PREVIEW_PICTURE"];

        // stiker
        if(!empty($arItem["PROPERTIES"]["DISCOUNT"]["VALUE_ENUM_ID"])){
            // stiker
            $arItem["PROPERTIES"]["DISCOUNT"]["CLASS"] = "stiker";
            foreach ($arItem["PROPERTIES"]["DISCOUNT"]["VALUE_ENUM_ID"] as $num=>$id){

                // stiker
                switch ($id){
                    case "13":
                        $arItem["PROPERTIES"]["DISCOUNT"]["CLASS"] .= " stiker-primary";
                        break;
                    case "14":
                        $arItem["PROPERTIES"]["DISCOUNT"]["CLASS"] .= " stiker-warning";
                        break;
                }

                $arResult['ITEMS'][$i]["STIKER"][] = [
                    "CODE" => $arItem["PROPERTIES"]["DISCOUNT"]["VALUE_XML_ID"][$num],
                    "TITLE" => $arItem["PROPERTIES"]["DISCOUNT"]["VALUE"][$num],
                    "CLASS" => $arItem["PROPERTIES"]["DISCOUNT"]["CLASS"]
                ];

            }

        }
        ///

    }

    // colected prices
    if(!empty($ID)){
        $prices = PriceTable::getList([
            "filter" => [
                "PRODUCT_ID" => $ID,
                "CATALOG_GROUP_ID" => [4,6,8]
            ],
            "select" => [
                "PRODUCT_ID",
                "CATALOG_GROUP_ID",
                "PRICE",
                "CURRENCY"
            ]
        ]);
        while ($price = $prices->fetch()){
            $price["PRINT_VALUE"] = $price["PRICE"];
            $price["PRINT_VALUE"] = \CCurrencyLang::CurrencyFormat($price["PRICE"], $price['CURRENCY']);;
            $arResult['PRODUCT_PRICES'][$price["PRODUCT_ID"]][$price["CATALOG_GROUP_ID"]] = $price;

        }
    }

}

