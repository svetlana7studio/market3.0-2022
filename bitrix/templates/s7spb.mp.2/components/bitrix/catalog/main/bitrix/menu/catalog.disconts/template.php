<?
/**
 * @var array $arParams
 * @var array $arResult
 */

use Bitrix\Main\Localization\Loc;

if(empty($arResult))
    return;

Loc::loadLanguageFile(__FILE__);
?>

<div class="position-relative">

    <!-- Start Partners -->
    <div class="s7spb-row row-center" id="discont-menu-items">

        <?
        foreach ($arResult as $i=>$arItem):
            $arItem["STIKER"] = "stick ";
            if($arItem["PARAMS"]["PERCENT"] > 25){
                $arItem["STIKER"] .= "stick_danger";
            }
            elseif($arItem["PARAMS"]["PERCENT"] > 15 && $arItem["PARAMS"]["PERCENT"] < 25){
                $arItem["STIKER"] .= "stick_primary";
            }
        ?>
            <div id="discont-menu-item-<?=$i?>" class="discont-menu-item">

                <a href="<?=$arItem["LINK"]?>" class="position-relative d-block border-radius-10">

                    <?if(empty($arItem["PARAMS"]["IMG"])):?>
                        <img src="<?=SITE_TEMPLATE_PATH?>/images/no_image200.jpg"
                             class="d-block border-radius-10"
                             title="<?=$arItem["TEXT"]?>" />
                    <?else:?>
                        <img src="<?=$arItem["PARAMS"]["IMG"]?>"
                             class="d-block border-radius-10"
                             title="<?=$arItem["TEXT"]?>" />
                    <?endif;?>

                    <div class="<?=$arItem["STIKER"]?>">
                        <?=Loc::getMessage("CATALOG_MENU_PERCENT", ["PERCENT" => $arItem["PARAMS"]["PERCENT"]])?>
                    </div>

                </a>

            </div>
        <?endforeach;?>

    </div>

    <div class="carousel-controls">
        <div onclick="koorochkaDiscontMenudSlider.slidePrev();" class="carousel-control-prev"></div>
        <div onclick="koorochkaDiscontMenudSlider.slideNext();" class="carousel-control-next"></div>
    </div>
    <!-- End Partners -->

</div>
