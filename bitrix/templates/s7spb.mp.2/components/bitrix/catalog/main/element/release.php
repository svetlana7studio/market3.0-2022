<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;

CJSCore::Init(["jquery"]);

Loc::loadLanguageFile(__FILE__);

$arEnds = [
    Loc::getMessage("TSB1_WORD_OBNOVL_END1"),
    Loc::getMessage("TSB1_WORD_OBNOVL_END2"),
    Loc::getMessage("TSB1_WORD_OBNOVL_END3"),
    Loc::getMessage("TSB1_WORD_OBNOVL_END4")
];

global $brandInfo;

$request = Application::getInstance()->getContext()->getRequest();

//$this->addExternalCss("/bitrix/templates/s7spb.anitos/assets/css/marketplace/catalog.element.detail.css");
$this->setFrameMode(true);

if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y') {
    $basketAction = (isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? array($arParams['COMMON_ADD_TO_BASKET_ACTION']) : array());
} else {
    $basketAction = (isset($arParams['DETAIL_ADD_TO_BASKET_ACTION']) ? $arParams['DETAIL_ADD_TO_BASKET_ACTION'] : array());
}
?><div class="s7spb-container">
    <?
    # start breadcrumb
    echo '<div class="p3">';

    $APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        "catalog",
        [
            "PATH" => "",
            "SITE_ID" => SITE_ID,
            "START_FROM" => "0"
        ],
        false,
        ["HIDE_ICONS" => "Y"]
    );

    echo '</div>';

    $APPLICATION->IncludeComponent(
        "bitrix:menu",
        "catalog.search.form",
        [
            "COMPONENT_TEMPLATE" => "cataloglevel",
            "ROOT_MENU_TYPE" => "catalog.level_1",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "36000000",
            "MENU_CACHE_USE_GROUPS" => "N",
            "MENU_CACHE_GET_VARS" => [],
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "catalog.level_1",
            "USE_EXT" => "Y",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "N"
        ],
        false,
        ["HIDE_ICONS" => "Y"]
    );

    $componentElementParams = [
        'PROPERTIES_SHORT_COUNT' => 4,
        'DETAIL_PAGE_URL' => $APPLICATION->GetCurPage(),
        'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'PROPERTY_CODE' => (isset($arParams['DETAIL_PROPERTY_CODE']) ? $arParams['DETAIL_PROPERTY_CODE'] : []),
        'META_KEYWORDS' => $arParams['DETAIL_META_KEYWORDS'],
        'META_DESCRIPTION' => $arParams['DETAIL_META_DESCRIPTION'],
        'BROWSER_TITLE' => $arParams['DETAIL_BROWSER_TITLE'],
        'SET_CANONICAL_URL' => $arParams['DETAIL_SET_CANONICAL_URL'],
        'BASKET_URL' => $arParams['BASKET_URL'],
        'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
        'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
        'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
        'CHECK_SECTION_ID_VARIABLE' => (isset($arParams['DETAIL_CHECK_SECTION_ID_VARIABLE']) ? $arParams['DETAIL_CHECK_SECTION_ID_VARIABLE'] : ''),
        'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
        'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
        'CACHE_TYPE' => $arParams['CACHE_TYPE'],
        'CACHE_TIME' => $arParams['CACHE_TIME'],
        'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
        'SET_TITLE' => $arParams['SET_TITLE'],
        'SET_LAST_MODIFIED' => $arParams['SET_LAST_MODIFIED'],
        'MESSAGE_404' => $arParams['~MESSAGE_404'],
        'SET_STATUS_404' => $arParams['SET_STATUS_404'],
        'SHOW_404' => $arParams['SHOW_404'],
        'FILE_404' => $arParams['FILE_404'],
        'PRICE_CODE' => $arParams['~PRICE_CODE'],

        'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],

        'PRODUCT_PROPERTIES' => (isset($arParams['PRODUCT_PROPERTIES']) ? $arParams['PRODUCT_PROPERTIES'] : []),
        'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
        'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
        'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
        //'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
        //'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
        //'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
        'SECTION_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['section'],
        'DETAIL_URL' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['element'],
        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
        'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
        'HIDE_NOT_AVAILABLE_OFFERS' => $arParams['HIDE_NOT_AVAILABLE_OFFERS'],
        'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
        'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
        'USE_MAIN_ELEMENT_SECTION' => $arParams['USE_MAIN_ELEMENT_SECTION'],
        'STRICT_SECTION_CHECK' => (isset($arParams['DETAIL_STRICT_SECTION_CHECK']) ? $arParams['DETAIL_STRICT_SECTION_CHECK'] : ''),
        'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
        'LABEL_PROP' => $arParams['LABEL_PROP'],
        'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
        'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
        'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
        'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
        'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
        'DISCOUNT_PERCENT_POSITION' => (isset($arParams['DISCOUNT_PERCENT_POSITION']) ? $arParams['DISCOUNT_PERCENT_POSITION'] : ''),
        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
        'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
        'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
        'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
        'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
        'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
        'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
        'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
        'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
        'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
        'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
        'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),
        'MESS_PRICE_RANGES_TITLE' => (isset($arParams['~MESS_PRICE_RANGES_TITLE']) ? $arParams['~MESS_PRICE_RANGES_TITLE'] : ''),
        'MESS_DESCRIPTION_TAB' => (isset($arParams['~MESS_DESCRIPTION_TAB']) ? $arParams['~MESS_DESCRIPTION_TAB'] : ''),
        'MESS_PROPERTIES_TAB' => (isset($arParams['~MESS_PROPERTIES_TAB']) ? $arParams['~MESS_PROPERTIES_TAB'] : ''),
        'MESS_COMMENTS_TAB' => (isset($arParams['~MESS_COMMENTS_TAB']) ? $arParams['~MESS_COMMENTS_TAB'] : ''),
        'MAIN_BLOCK_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE']) ? $arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE'] : ''),
        'MAIN_BLOCK_OFFERS_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE']) ? $arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE'] : ''),
        'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
        'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
        'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
        'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
        'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
        'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
        'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
        'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
        'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
        'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
        'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
        'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
        'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
        'IMAGE_RESOLUTION' => (isset($arParams['DETAIL_IMAGE_RESOLUTION']) ? $arParams['DETAIL_IMAGE_RESOLUTION'] : ''),
        'PRODUCT_INFO_BLOCK_ORDER' => (isset($arParams['DETAIL_PRODUCT_INFO_BLOCK_ORDER']) ? $arParams['DETAIL_PRODUCT_INFO_BLOCK_ORDER'] : ''),
        'PRODUCT_PAY_BLOCK_ORDER' => (isset($arParams['DETAIL_PRODUCT_PAY_BLOCK_ORDER']) ? $arParams['DETAIL_PRODUCT_PAY_BLOCK_ORDER'] : ''),
        'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
        'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
        'ADD_SECTIONS_CHAIN' => (isset($arParams['ADD_SECTIONS_CHAIN']) ? $arParams['ADD_SECTIONS_CHAIN'] : ''),
        'ADD_ELEMENT_CHAIN' => (isset($arParams['ADD_ELEMENT_CHAIN']) ? $arParams['ADD_ELEMENT_CHAIN'] : ''),
        //'ADD_ELEMENT_CHAIN' => "N"
    ];

    # Trase point

    // User favorites
    \CBitrixComponent::includeComponentClass("studio7sbp:favorite");
    $favorite = new \studio7sbpFavorite();
    if($USER->IsAuthorized()){
        $favorite->onPrepareComponentParams(array("USER_ID" => $USER->GetID()));
        $componentElementParams["USER_ID"] = $USER->GetID();
    }



    ?>

    <div class="s7spb-row bg-white p3">
        <div class="s7spb-col-md-7 s7spb-col-lg-6">
            <?
            // <editor-fold defaultstate="s7spb-collapsed" desc="# bitrix:catalog.element">
            $arElement = $APPLICATION->IncludeComponent(
                'studio7sbp:catalog.element',
                'market2',
                $componentElementParams,
                false
            );
            // </editor-fold>
            ?>
        </div>
        <div class="s7spb-col-md-5 s7spb-col-lg-6 position-relative">
            <div class="float-right mt3">
                <?$APPLICATION->ShowViewContent('ELEMENT_TRADE_MARK');?>
            </div>
            <h1><?$APPLICATION->ShowViewContent('ELEMENT_NAME');?></h1>

            <div class="s7spb-row s7spb-row12 s7spb-row-start s7spb-row-center">

                <div class="s7spb-col s7spb-col-auto">
                    <div class="mr1 vertical-align-middle d-inline-block"><?=Loc::getMessage("ELEMENT_REATING")?>:</div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:iblock.vote",
                        "product_rating",
                        Array(
                            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "ELEMENT_ID" => $arElement["ID"],
                            "MAX_VOTE" => 5,
                            "VOTE_NAMES" => [],
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                            "DISPLAY_AS_RATING" => 'vote_avg'
                        ),
                        false,
                        ["HIDE_ICONS" =>"Y"]
                    );?>
                </div>

                <div class="s7spb-col s7spb-col-auto">
                    <a href="<?=$APPLICATION->GetCurPageParam("tab=reviews", ["tab"])?>#studio7sbp-bx-tabs" class="text-decoration-none text-16">
                        <?
                        echo Loc::getMessage("PROPERTIES_FORUM_MESSAGE_CNT", [
                            "NUM" => $arElement["FORUM_MESSAGE_CNT"] ? $arElement["FORUM_MESSAGE_CNT"] : 0
                        ]);

                        if (
                            $arElement["FORUM_MESSAGE_CNT"]>1 &&
                            substr($arElement["FORUM_MESSAGE_CNT"], strlen($arElement["FORUM_MESSAGE_CNT"])-2, 1)=="1")
                        {
                            echo $arEnds[0];
                        }
                        else
                        {
                            $c = IntVal(substr($arElement["FORUM_MESSAGE_CNT"], strlen($arElement["FORUM_MESSAGE_CNT"])-1, 1));
                            if ($c==0 || ($c>=5 && $c<=9))
                                echo $arEnds[1];
                            elseif ($c==1)
                                echo $arEnds[2];
                            else
                                echo $arEnds[3];
                        }
                        ?>
                    </a>
                </div>

                <?if(in_array($arElement["ID"], $favorite->getUserFavorite())):?>
                    <div class="s7spb-col s7spb-col-auto text-14 cursor-pointer py1 active favorites-desct"
                         data-title="<?=Loc::getMessage("ELEMENT_FAVORITE_WONT")?>"
                         data-titlein="<?=Loc::getMessage("ELEMENT_FAVORITE_IN")?>"
                         data-id="<?=$arElement["ID"]?>"
                         id="market2-element-favorite"
                         onclick="market2CatalogElement.toggleFavorite(this)">
                        <i class="icon icon-favorite-info square-16 vertical-align-middle"></i>
                        <?=Loc::getMessage("ELEMENT_FAVORITE_IN")?>
                    </div>
                <?else:?>
                    <div class="s7spb-col s7spb-col-auto text-14 cursor-pointer py1 favorites-desct"
                         data-title="<?=Loc::getMessage("ELEMENT_FAVORITE_WONT")?>"
                         data-titlein="<?=Loc::getMessage("ELEMENT_FAVORITE_IN")?>"
                         data-id="<?=$arElement["ID"]?>"
                         id="market2-element-favorite"
                         onclick="market2CatalogElement.toggleFavorite(this)">
                        <i class="icon icon-favorite-info square-16 vertical-align-middle"></i>
                        <?=Loc::getMessage("ELEMENT_FAVORITE_WONT")?>
                    </div>
                <?endif;?>

                <div class="s7spb-col s7spb-col-auto text-14 text-primary py1 cursor-pointer position-relative">
                    <div onclick="market2CatalogElement.toggleShare()">
                        <i class="icon icon-share vertical-align-middle"></i> <?=Loc::getMessage("ELEMENT_SHARE")?>
                    </div>

                    <div id="element-share" class="d-none">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.share",
                            "universal",
                            [
                                "HANDLERS" => ["vk", "facebook", "twitter", "pinterest", "gplus"],
                                "HIDE" => "N",
                                "PAGE_TITLE" => "",
                                "PAGE_URL" => "",
                                "SHORTEN_URL_KEY" => "",
                                "SHORTEN_URL_LOGIN" => ""
                            ],
                            false,
                            ["HIDE_ICONS" =>"Y"]
                        );?>
                    </div>

                </div>

            </div>

            <div class="s7spb-row s7spb-row12">
                <div class="s7spb-col s7spb-col-md-12 s7spb-col-lg pb3">
                    <?$APPLICATION->ShowViewContent('PROPERTIES_SHORT');?>
                </div>
                <div class="s7spb-col s7spb-col-12 s7spb-col-sm-auto s7spb-col-md-12 s7spb-col-lg-auto pb3">
                    <?if(!empty($arElement["H_COMPANY"])):?>
                        <h3 class="text-14"><?=Loc::getMessage("ELEMENT_H_COMPANY_TITLE")?>:</h3>

                        <a href="<?=$arElement["H_COMPANY"]["DETAIL_PAGE_URL"]?>" class="mb3 text-center d-block text-decoration-none">
                            <?if(is_array($arElement["H_COMPANY"]["PREVIEW_PICTURE"])):?>
                                <img src="<?=$arElement["H_COMPANY"]["PREVIEW_PICTURE"]["SRC"]?>"
                                     width="<?=$arElement["H_COMPANY"]["PREVIEW_PICTURE"]["WIDTH"]?>"
                                     height="<?=$arElement["H_COMPANY"]["PREVIEW_PICTURE"]["HEIGHT"]?>"
                                     class="img-responsive" />
                            <?else:?>
                                <div class="bg-primary text-white d-inline-block px5 py2 text-20 text-uppercase"><?=$arElement["H_COMPANY"]["NAME"]?></div>
                            <?endif;?>
                        </a>
    
                        <div class="mb3">
                            <?
                            $APPLICATION->IncludeComponent(
                                "bitrix:iblock.vote",
                                "product_rating",
                                Array(
                                    "IBLOCK_TYPE" => $arElement["H_COMPANY"]["IBLOCK_TYPE_ID"],
                                    "IBLOCK_ID" => $arElement["H_COMPANY"]["IBLOCK_ID"],
                                    "ELEMENT_ID" => $arElement["H_COMPANY"]["ID"],
                                    "MAX_VOTE" => 5,
                                    "VOTE_NAMES" => [],
                                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                                    "DISPLAY_AS_RATING" => 'vote_avg'
                                ),
                                false,
                                ["HIDE_ICONS" =>"Y"]
                            );?>
                        </div>
                    <?endif;?>
                    <a href="<?=$arElement["H_COMPANY"]["DETAIL_PAGE_URL"]?>#shop-reviews"><?
                        echo Loc::getMessage("PROPERTIES_FORUM_MESSAGE_CNT", [
                            "NUM" => $arElement["H_COMPANY"]["PROPERTY_FORUM_MESSAGE_CNT_VALUE"] ? $arElement["H_COMPANY"]["PROPERTY_FORUM_MESSAGE_CNT_VALUE"] : 0
                        ]);

                        if (
                            $arElement["H_COMPANY"]["PROPERTY_FORUM_MESSAGE_CNT_VALUE"]>1 &&
                            substr($arElement["FORUM_MESSAGE_CNT"], strlen($arElement["H_COMPANY"]["PROPERTY_FORUM_MESSAGE_CNT_VALUE"])-2, 1)=="1")
                        {
                            echo $arEnds[0];
                        }
                        else
                        {
                            $c = IntVal(substr($arElement["H_COMPANY"]["PROPERTY_FORUM_MESSAGE_CNT_VALUE"], strlen($arElement["H_COMPANY"]["PROPERTY_FORUM_MESSAGE_CNT_VALUE"])-1, 1));
                            if ($c==0 || ($c>=5 && $c<=9))
                                echo $arEnds[1];
                            elseif ($c==1)
                                echo $arEnds[2];
                            else
                                echo $arEnds[3];
                        }
                        ?></a> <span class="text-gray"><?
                        echo Loc::getMessage("ELEMENT_H_COMPANY_ABOUT");
                        ?></span>
                </div>
            </div>

        </div>
    </div>

    <div id="studio7sbp-bx-tabs">
        <?$APPLICATION->IncludeComponent(
            'studio7sbp:bx.tabs',
            'market2',
            [
                "REQUEST_PARAM" => "tab",
                "AJAX_MODE" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "Y",
                "TABS" => [
                    [
                        "TITLE" => "Описание товара",
                        "CODE" => "about"
                    ],
                    [
                        "TITLE" => "Отзывы",
                        "CODE" => "reviews"
                    ],
                    [
                        "TITLE" => "Доставка и оплата",
                        "CODE" => "delivery"
                    ],
                    [
                        "TITLE" => "Гарантии продавца",
                        "CODE" => "garanty"
                    ]
                ],
                "REVIEWS" => [
                    "MESSAGE_TITLE" => "Ваш отзыв",
                    "SHOW_AVATAR" => "N",
                    "SHOW_RATING" => "N",
                    "SHOW_SUBSCRIBE" => "N",
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
                    "USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
                    "PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
                    "FORUM_ID" => $arParams["FORUM_ID"],
                    "URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
                    "SHOW_LINK_TO_FORUM" => "N", //$arParams["SHOW_LINK_TO_FORUM"],
                    "DATE_TIME_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
                    "ELEMENT_ID" => $arResult['VARIABLES']['ELEMENT_ID'],
                    "AJAX_POST" => "N", // $arParams["REVIEW_AJAX_POST"]
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "URL_TEMPLATES_DETAIL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
                ]
            ],
            false,
            ["HIDE_ICONS" => "Y"]
        );?>
    </div>


    <?
    if(!empty($GLOBALS)){
        $GLOBALS["arrFilterNew"]["PROPERTY_H_COMPANY"] = $arElement["H_COMPANY"]["ID"];
    }
    ?>
    <div class="bg-white position-relative bg-warning">
        <div class="spb7-title bg-info ml5"><?=Loc::getMessage("ELEMENT_H_COMPANY_PRODUCTS")?></div>
        <div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.top",
                "new.slider",
                Array(
                    "USER_ID" => $USER->GetID(),
                    "CART" => $arParams["CART"],
                    "ACTION_VARIABLE" => "action",
                    "ADD_PICT_PROP" => "-",
                    "ADD_PROPERTIES_TO_BASKET" => "N",
                    "ADD_TO_BASKET_ACTION" => "ADD",
                    "BASKET_URL" => "/basket/",
                    "BLOCK_LABEL_TYPE" => "NEW",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
                    "COMPATIBLE_MODE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "CUSTOM_FILTER" => "",
                    "DETAIL_URL" => "",
                    "DISCOUNT_PERCENT_POSITION" => "bottom-right",
                    "DISPLAY_COMPARE" => "N",
                    "ELEMENT_COUNT" => "10",
                    "ELEMENT_SORT_FIELD" => "timestamp_x",
                    "ELEMENT_SORT_FIELD2" => "id",
                    "ELEMENT_SORT_ORDER" => "desc",
                    "ELEMENT_SORT_ORDER2" => "desc",
                    "ENLARGE_PRODUCT" => "STRICT",
                    "FILTER_NAME" => "arrFilterNew",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                    "IBLOCK_ID" => "2",
                    "IBLOCK_TYPE" => "marketplace",
                    "LABEL_PROP" => array(),
                    "LINE_ELEMENT_COUNT" => "5",
                    "MESS_BLOCK_LABEL" => "Новинки",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_COMPARE" => "Сравнить",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_NOT_AVAILABLE" => "Зарегистрируйтесь, чтобы увидеть цены",
                    "OFFERS_CART_PROPERTIES" => array(),
                    "OFFERS_FIELD_CODE" => array("",""),
                    "OFFERS_LIMIT" => "5",
                    "OFFERS_PROPERTY_CODE" => array("",""),
                    "OFFERS_SORT_FIELD" => "sort",
                    "OFFERS_SORT_FIELD2" => "id",
                    "OFFERS_SORT_ORDER" => "asc",
                    "OFFERS_SORT_ORDER2" => "desc",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRICE_CODE" => ["BASE"],
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                    "PRODUCT_DISPLAY_MODE" => "N",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "PRODUCT_PROPERTIES" => array(""),
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'5','BIG_DATA':false},{'VARIANT':'5','BIG_DATA':false},{'VARIANT':'5','BIG_DATA':false}]",
                    "PRODUCT_SUBSCRIPTION" => "Y",
                    "PROPERTY_CODE" => array("MOQ",""),
                    "PROPERTY_CODE_MOBILE" => array(),
                    "ROTATE_TIMER" => "30",
                    "SECTION_URL" => "",
                    "SEF_MODE" => "N",
                    "SHOW_CLOSE_POPUP" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_MAX_QUANTITY" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "SHOW_PAGINATION" => "Y",
                    "SHOW_PRICE_COUNT" => "1",
                    "SHOW_SLIDER" => "N",
                    "SLIDER_INTERVAL" => "3000",
                    "SLIDER_PROGRESS" => "N",
                    "TEMPLATE_THEME" => "blue",
                    "USER_FAVORITES" => $favorite->getUserFavorite(),
                    "USE_ENHANCED_ECOMMERCE" => "N",
                    "USE_PRICE_COUNT" => "N",
                    "USE_PRODUCT_QUANTITY" => "Y",
                    "VIEW_MODE" => "SECTION"
                )
            );?>
        </div>
    </div>

</div>