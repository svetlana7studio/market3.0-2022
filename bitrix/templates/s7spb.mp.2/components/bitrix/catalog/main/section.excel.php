<?
/**
 * @var CMain $APPLICATION
 */

// количество элементов для страницы
use Bitrix\Main\Application;

/*
$request = Application::getInstance()->getContext()->getRequest();
$show_by = intval($request->get("show_by"));
if($show_by <= 1){
    $show_by = 24;
}
*/
$show_by = 50000;

$componentSectionParams["PAGE_ELEMENT_COUNT"] = $show_by;
$componentSectionParams["~PAGE_ELEMENT_COUNT"] = $show_by;
$componentSectionParams["LANDING_PAGE_ELEMENT_COUNT"] = $show_by;
$componentSectionParams["~LANDING_PAGE_ELEMENT_COUNT"] = $show_by;
$componentSectionParams["ELEMENT_SORT_FIELD"] = $sort;
$componentSectionParams["ELEMENT_SORT_ORDER"] = $sort_order;

$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "excel",
    $componentSectionParams,
    false,
    array("HIDE_ICONS" => "Y")
);
