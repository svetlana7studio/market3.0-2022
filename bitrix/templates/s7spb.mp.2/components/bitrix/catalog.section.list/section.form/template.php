<?
/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$arParams["LINE_ELEMENT_COUNT"] = 4;

if(empty($arResult["SECTIONS"]))
    return;
?>
<div class="wrap-toggle tog-minimal">
    <form action="<?=$arParams["PAGE"]?>"
          method="get"
          class="bg-white p2 pt5 border-radius-10 mb4 getHightForm">
        <div id="sections-form">
            <?$APPLICATION->ShowViewContent('B_SECTION');?>
        </div>
    </form>
    <div class="toggle-bottom-background"></div>
    <div class="toggle-button"><a href="#" id="togglerButton" onclick="return false;">Развернуть</a></div>
</div>
