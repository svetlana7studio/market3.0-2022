$(document).ready(function(){

    var height = $(".getHightForm").height();
    // если выота меньше 290пкс - скроем кнопку - "Развернуть"
    if (height < 290) {
        $('#togglerButton').css("display", "none");
        $('.toggle-bottom-background').css("display", "none");
        $('.wrap-toggle').css("height", height + 47 + "px"); //не изменяем выссоту контейнера
    }
    else {
        /* ==== СВОРАЧИВАЕТ / РАЗВОРАЧИВАЕТ список фильтра Каталога по Разделам ===== */
        $("#togglerButton").on('click', function() {

            var height_wrap = height + 65;
            // проверим свернут ли список
            if ( $('.wrap-toggle').hasClass('tog-minimal') ) {
                $( ".wrap-toggle" ).animate({
                    height: height_wrap + "px", // высота элемента
                }, 500, "linear", function(){
                    // Удалим класс tog-minimal
                    $(".toggle-bottom-background").addClass('g-minimize');
                    $('.wrap-toggle').removeClass('tog-minimal');
                    $('#togglerButton').text('Свернуть');
                });
            }
            else {
                $( ".wrap-toggle" ).animate({
                    height: "290px", // высота элемента
                }, 500, "linear", function(){
                    // Удалим класс tog-minimal
                    $('.toggle-bottom-background').removeClass('g-minimize');
                    $('.wrap-toggle').addClass('tog-minimal');
                    $('#togglerButton').text('Развернуть');
                });
            }
        });
    }



    $(".ch_on").on('change', function() {
        var checked = false;
        $('.ch_on').each(function(i,elem) {
            if ($(this).is(":checked")) {
                checked = true;
                return false;
            }
        });
        if (checked === true) {
            $("#showFilterButton").removeClass('hide-button');
            $(".reset-button").removeClass('hide-b');
            $("#showFilterButton").prop('disabled', false);
            $("#showFilterButton").val("Показать");
        }
        else {
            $("#showFilterButton").addClass('hide-button');
            //$("#showFilterButton").addClass('hide-b');
            $("#showFilterButton").prop('disabled', true);
            $("#showFilterButton").val("Выберите разделы");
        }
    });
});
