<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"COUNT_ELEMENTS_SHOW" => array(
		"PARENT" => "VISUAL",
		"NAME" => GetMessage("MARKET_MAIN_SECTION_COUNT_PAR"),
		"TYPE" => "STRING",
		"DEFAULT" => "6",
	),
);
?>