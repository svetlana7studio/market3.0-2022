<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if(empty($arResult["SECTIONS"]))
    return;
?>

<div class="s7spb-row mb3 mx-neg-px-2 product-scroll">
    <?
    $i = 0;
    foreach ($arResult['SECTIONS'] as $arSection):
        if(++$i > $arParams["COUNT_ELEMENTS_SHOW"]) continue;
    ?>

        <div class="s7spb-col-2 bg-white" onClick="location.href='<?=$arSection["SECTION_PAGE_URL"]?>'">

            <div class="mx-px-2">

                <div class="text-white text-center bg-primary h42 item-elem">
                    <?=$arSection["NAME"]?>
                </div>

                <div class="py3">
                    <?if(is_array($arSection["PICTURE"])):?>
                        <img src="<?=$arSection["PICTURE"]["SRC"]?>"
                             height="<?=$arSection["PICTURE"]["HEIGHT"]?>"
                             width="<?=$arSection["PICTURE"]["WIDTH"]?>"
                             class="img-responsive" />
                    <?endif;?>
                </div>

            </div>

        </div>

    <?endforeach;?>
</div>
