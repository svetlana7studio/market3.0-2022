<?
/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$arParams["LINE_ELEMENT_COUNT"] = 4;

if(empty($arResult["SECTIONS"]))
    return;

?>
<? // b_dump($arResult); ?>

<div class="s7spb-row s7spb-row-wrap s7spb-row12 s7spb-col600">
    <?
    foreach ($arResult["SECTIONS"] as $i=>$arItem):

        if(empty($arItem["PICTURE"])){
            $arItem["PICTURE"] = $this->GetFolder() . "/images/default.jpg";
        }

        if($i > 0){
            if($i % $arParams["LINE_ELEMENT_COUNT"] === 0){
                echo '</div><div class="s7spb-row s7spb-row-wrap s7spb-row12 s7spb-col600">';
            }
        }
    ?>
        <div
           class="s7spb-col s7spb-col1200-50  mb4 hm200 border-radius-10 py4 pad px6 text-decoration-none text-black mr-rig"
             style=" background-size: cover; background-image: url(<?=$arItem["PICTURE"]?>)">
                 <a class="text-20 text-semibold ti-cat" href="<?=$arItem["URL"]?>"><?=$arItem["NAME"]?></a>
                    <? if ( !empty($arItem['SUBSECTIONS']) ) : ?>
                        <ul class="s7spb-catalog-list">
                            <? foreach ($arItem['SUBSECTIONS'] as $arSubsection) : ?>
                              <li class="item__catalog-card">
                                <a class="item__catalog-link" href="<?=$arSubsection['SECTION_PAGE_URL']?>"><?=$arSubsection['NAME']?></a>
                              </li>
                            <? endforeach; ?>
                        </ul>
                    <? endif; ?>
         </div>

    <?
    endforeach;
    $i++;
    $i = $i % $arParams["LINE_ELEMENT_COUNT"];

    if($i){
        for($i; $i < $arParams["LINE_ELEMENT_COUNT"]; $i++){
            echo '<div class="s7spb-col s7spb-col1200-50 py4 px6"></div>';
        }
    }
    ?>
</div>