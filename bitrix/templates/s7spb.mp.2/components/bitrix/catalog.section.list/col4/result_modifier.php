<?php

/**
 *  сформируем дерево из разделов и подраазделов.
 */

$arrTree = [];
$lev = 0;
foreach ($arResult["SECTIONS"] as $key => $arSection) {

    if ($arSection['DEPTH_LEVEL'] == 1) {
        $arrTree[$lev]['NAME'] = $arSection['NAME'];
        $arrTree[$lev]['URL'] = $arSection['SECTION_PAGE_URL'];
        $arrTree[$lev]['PICTURE'] = $arSection['PICTURE']['SRC'];
        foreach ($arResult["SECTIONS"] as $i => $arDepthSection) {
            if ($arDepthSection['IBLOCK_SECTION_ID'] == $arSection['ID']) {
                $arrTree[$lev]['SUBSECTIONS'][] = $arDepthSection;
            }

        }
        $lev++;
    }

}
if ( !empty($arrTree) ) {
    $arResult["SECTIONS"] = $arrTree;
}
