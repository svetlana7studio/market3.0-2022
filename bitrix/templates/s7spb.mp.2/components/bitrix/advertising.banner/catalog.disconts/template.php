<?
/**
 * @var array $arParams
 * @var array $arResult
 */

if(empty($arResult))
    return;
?>

<div class="position-relative mb3">

    <div class="s7spb-row row-center" id="discont-menu-items">

        <?foreach ($arResult["BANNERS"] as $i=>$banner):?>
            <div id="discont-menu-item-<?=$i?>" class="discont-menu-item">
                <?=$banner?>
            </div>
        <?endforeach;?>

    </div>

    <?if(count($arResult["BANNERS"]) > 1):?>
        <div class="carousel-controls">
            <div onclick="koorochkaDiscontMenudSlider.slidePrev();" class="carousel-control-prev"></div>
            <div onclick="koorochkaDiscontMenudSlider.slideNext();" class="carousel-control-next"></div>
        </div>
    <?endif;?>

</div>
