<?
/**
 * @var array $arCurrentValues
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = [
    "COLUMN_COUNT" => [
        "NAME" => GetMessage('COLUMN_COUNT'),
        "TYPE" => "LIST",
        "VALUES" => [
            "1" => "1",
            "2" => "2",
            "3" => "3",
            "4" => "4",
            "5" => "5",
            "6" => "6"
        ],
        "DEFAULT" => "2"
    ]
];