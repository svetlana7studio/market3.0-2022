<?
/**
 * @var array $arResult
 * @var array $arParams
 * @var CBitrixComponentTemplate $this
 */
$this->setFrameMode(true);
$frame = $this->createFrame("two-banner-block", false)->begin();
echo '<div id="two-banner-block" class="s7spb-row s7spb-row s7spb-row12 mb3">';
foreach ($arResult["BANNERS"] as $i=>$banner){
    echo '<div class="s7spb-col position-relative ">';

    echo '<div class="spb7-title bg-white ml3 position-absolute">' . $arResult["BANNERS_PROPERTIES"][$i]["NAME"] . '</div>';

    $banner = preg_replace('!(<a .*?href=")([^"]+)!', '$1' . $arResult["BANNERS_PROPERTIES"][$i]["URL"], $banner);

    echo $banner;
    echo '</div>';
}
echo '</div>';
$frame->end();