<?
/**
 * @var array $arResult
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(empty($arResult["BANNERS"]))
    return;
?>

<?foreach ($arResult["BANNERS"] as $i=>$banner):?>
    <div class="mb5">
        <?=$banner?>
    </div>
<?endforeach;?>