<?
/**
 * @var array $arResult
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(empty($arResult["BANNERS"]))
    return;
?>

<div class="position-relative">


    <div id="slider-two-items">
        <?
        foreach ($arResult["BANNERS"] as $i=>$banner):

            ?>
            <div id="slider-two-item-<?=$i?>" class="slider-two-item">
                <?=$banner?>
            </div>
        <?endforeach;?>
    </div>

    <?if(count($arResult["BANNERS"]) > 2):?>
        <div class="carousel-controls">
            <div onclick="koorochkaSliderTwoSlider.slidePrev();" class="carousel-control-prev"></div>
            <div onclick="koorochkaSliderTwoSlider.slideNext();" class="carousel-control-next"></div>
        </div>
    <?endif;?>
</div>