(function(window){
    'use strict';

    if (window.koorochkaTopBunnerdSlider)
        return;

    window.koorochkaTopBunnerdSlider = {
        id: "top-bunner-unique1",
        itemClass: "top-bunner-item",
        itemId: "top-bunner-item-",
        items: [],
        count: 0,
        wait: null,
        currentItem: 0,
        interval: 4000,

        init: function(){
            setTimeout(function () {
                window.koorochkaTopBunnerdSlider.create();
                //window.koorochkaTopBunnerdSlider.automatic();
            }, this.preloader);
        },
        create: function(){
            this.items = BX.findChildren(BX(this.id), {className: this.itemClass}, false);
            this.count = this.items.length;
        },
        slideNext: function(){
            // step
            this.currentItem++;
            if(this.currentItem > this.items.length)
                this.currentItem = 0;
            this.currentContext(this.items[this.currentItem]);

        },

        slidePrev: function(){
            // step
            this.currentItem--;
            if(this.currentItem < 0){
                this.currentItem = this.items.length;
                this.currentItem--;
            }
            this.currentContext(this.items[this.currentItem]);
        },

        currentContext: function(element) {
            if(element == undefined)
            {
                this.currentItem = 0;
                element = this.items[this.currentItem];
            }

            element = this.currentItem;
            element = document.getElementById(this.itemId + element);
            element = element.offsetLeft;
            BX(this.id).scrollLeft = element;
            this.automatic();
        },

        automatic: function() {
            this.stopAutomatic();
            this.wait = setTimeout(function () {
                koorochkaTopBunnerdSlider.slideNext();
                koorochkaTopBunnerdSlider.automatic();
            }, this.interval);
        },

        stopAutomatic: function() {
            clearTimeout(this.wait);
        }

    };
})(window);

BX.ready(function () {
    window.koorochkaTopBunnerdSlider.init();
    koorochkaTopBunnerdSlider.automatic();
});
