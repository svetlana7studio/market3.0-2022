<?
/**
 * @var array $arResult
 * @var array $arParams
 * @var CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
if(empty($arResult["BANNERS"]))
    return;
$arParams["IDENT"] = "top-bunner-unique1";
$frame = $this->createFrame($arParams["IDENT"] . "-frame", false)->begin();
?>
<div class="position-relative mb3 m">

    <div id="<?=$arParams["IDENT"]?>">
        <?
        foreach ($arResult["BANNERS"] as $i=>$banner):

            ?>
            <div id="top-bunner-item-<?=$i?>" class="top-bunner-item">
                <?=$banner?>
            </div>
        <?endforeach;?>
        <script>
            BX.ready(function () {
                window.koorochkaTopBunnerdSlider.init();
                koorochkaTopBunnerdSlider.automatic();
            });

        </script>
    </div>
    <?if(count($arResult["BANNERS"]) > 1):?>
        <div id="top-bunner-unique1-controls">
            <div onclick="koorochkaTopBunnerdSlider.slidePrev();" class="top-bunner-prev"></div>
            <div onclick="koorochkaTopBunnerdSlider.slideNext();" class="top-bunner-next"></div>
        </div>
    <?endif;?>
</div>

<?
$frame->end();
?>