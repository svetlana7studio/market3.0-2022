<?
/**
 * @var array $arResult
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(empty($arResult["BANNERS"]))
    return;
?>

<div class="mb4 mt3 text-center">
    <?=$arResult["BANNERS"][0]?>
</div>