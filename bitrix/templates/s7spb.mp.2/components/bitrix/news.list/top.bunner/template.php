<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(empty($arResult["ITEMS"]))
    return;
?>
<div class="position-relative">
    <div id="top-bunner-items">
        <?foreach($arResult["ITEMS"] as $i=>$arItem):?>
            <div id="top-bunner-item-<?=$i?>" class="top-bunner-item">
                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                         width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                         height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                         alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                         title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>" />
                </div>
            </div>
        <?endforeach;?>
    </div>

    <?if(count($arResult["ITEMS"]) > 1):?>
        <div id="top-bunner-controls">
            <div onclick="koorochkaTopBunnerdSlider.slidePrev();" class="top-bunner-prev"></div>
            <div onclick="koorochkaTopBunnerdSlider.slideNext();" class="top-bunner-next"></div>
        </div>
    <?endif;?>
</div>