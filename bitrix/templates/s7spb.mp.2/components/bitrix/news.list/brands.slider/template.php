<?
/**
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode( true );
if(empty($arResult["ITEMS"]))
    return;

$arResult["ITEMS"] = array_merge($arResult["ITEMS"], $arResult["ITEMS"], $arResult["ITEMS"]);

if(count($arResult['ITEMS']) > $arParams["NEWS_COUNT"]){
    $this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/catalog/brands.slider.js");
}

?>

<div class="position-relative">

    <div class="s7spb-row s7spb-row-nowrap s7spb-row-justify-start d-overflow-hidden" id="brands-slider-items">

        <?
        foreach($arResult["ITEMS"] as $i=>$arItem){
            #$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            #$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

            <?if( is_array($arItem["PREVIEW_PICTURE"]) ){?>

                <div id="brands-slider-item-<?=$i?>" class="brands-slider-item"
                    <?/*$this->GetEditAreaId($arItem['ID']);*/?>>
                    <a href="#">
                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                             alt="<?=$arItem["NAME"]?>"
                             title="<?=$arItem["NAME"]?>" />
                    </a>
                </div>

            <?}?>

        <?}?>

    </div>

    <?if(count($arResult['ITEMS']) > $arParams["NEWS_COUNT"]):?>
        <div class="carousel-controls">
            <div onclick="koorochkaBrandSlider.slidePrev();" class="carousel-control-prev"></div>
            <div onclick="koorochkaBrandSlider.slideNext();" class="carousel-control-next"></div>
        </div>
    <?endif;?>

</div>