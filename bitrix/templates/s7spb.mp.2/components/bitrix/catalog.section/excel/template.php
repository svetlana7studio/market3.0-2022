<?
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

$arParams["ALFAVITE"] = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q");

$arParams["MATRIX"] = array(
    "START" => array(
        "X" => "B",
        "Y" => 3,
        "ADRESS" => "B3"
    ),
    "CELL" => array(
        "X" => "B",
        "Y" => 3,
        "ADRESS" => "B3"
    )
);

$objPHPExcel = new PHPExcel();

$arParams["COLUMNS_HEADER"] = [
    Loc::getMessage("CATALOG_SECTION_NAME"),
    Loc::getMessage("CATALOG_SECTION_DETAIL_TEXT"),
    Loc::getMessage("CATALOG_SECTION_SECTION_NAME"),
];

if(!empty($arResult["PRICES"])){
    foreach($arResult["PRICES"] as $value){
        $arParams["COLUMNS_HEADER"][] = $value["TITLE"];
    }
}

if(!empty($arResult['ITEMS'][0]["DISPLAY_PROPERTIES"])){
    foreach($arResult['ITEMS'][0]["DISPLAY_PROPERTIES"] as $value){
        $arParams["COLUMNS_HEADER"][] = $value["NAME"];
    }
}

unset($value);
include "style.php";
include "head.php";



foreach ($arResult['ITEMS'] as $arItem){
    include "item.php";
}

// <editor-fold defaultstate="collapsed" desc=" # Output">
header("Content-Disposition: filename=" . $arResult["NAME"] . ".xlsx");
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
// </editor-fold>
