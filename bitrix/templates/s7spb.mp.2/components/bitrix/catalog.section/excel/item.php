<?php
/**
 * @var PHPExcel $objPHPExcel
 * @var array $arParams
 * @var array $arItem
 */
$i = 0;
use Bitrix\Main\Localization\Loc;

# Stylesheet
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setIndent(2);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getRowDimension($arParams["MATRIX"]["CELL"]["Y"])->setRowHeight(40);

# Data filling

if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])){


    if (strpos($arItem["PREVIEW_PICTURE"]["SRC"], 'http') === false) {
        $arItem["PREVIEW_PICTURE"]["SRC"] = "http://anytos.ru/" . $arItem["PREVIEW_PICTURE"]["SRC"];
    }
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arItem["PREVIEW_PICTURE"]["SRC"], PHPExcel_Cell_DataType::TYPE_STRING);
}

$i++;


$arItem["NAME"] = preg_replace("#[^0-9а-яА-ЯA-Za-z;:_.,? -]+#u", ' ', $arItem["NAME"]);

$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arItem["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arItem["DETAIL_TEXT"], PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arResult["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

// <editor-fold defaultstate="collapsed" desc=" # Prices">
if(!empty($arItem["PRICES"])){
    foreach($arItem["PRICES"] as $code=>$arPrice){
        $objPHPExcel->getActiveSheet()->getStyle($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        //AddMessage2Log($code);
        /*
        switch ($code){
            case "price_red":
                $objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('663af2');
                break;
            case "price_yellow":
                $objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f4b901');
                break;
            case "price_green":
                $objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('4ece02');
                break;
        }
        */

        $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arPrice["PRINT_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
        $i++;
    }
    unset($arPrice);
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Display properties">

if(!empty($arItem["DISPLAY_PROPERTIES"])){
    foreach($arItem["DISPLAY_PROPERTIES"] as $arProperties){
        $objPHPExcel->getActiveSheet()->getStyle($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arProperties["DISPLAY_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
        $i++;
    }
    unset($arProperties);
}
// </editor-fold>



# send matrix position to new row for busket items
$arParams["MATRIX"]["CELL"]["Y"]++;
$arParams["MATRIX"]["CELL"]["X"] = $arParams["MATRIX"]["START"]["X"];
$arParams["MATRIX"]["CELL"]["ADRESS"] = $arParams["MATRIX"]["CELL"]["X"] . $arParams["MATRIX"]["CELL"]["Y"] ;
