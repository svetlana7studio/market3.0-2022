<?php
/**
 * @var PHPExcel $objPHPExcel
 * @var array $arParams
 * @var array $arItem
 */
$i = 1;
use Bitrix\Main\Localization\Loc;

# Stylesheet
$objPHPExcel->getActiveSheet()->freezePane('C4');
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setIndent(2);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getRowDimension($arParams["MATRIX"]["CELL"]["Y"])->setRowHeight(18);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getFont()->setSize(14);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('22cfc7');
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

foreach ($arParams["COLUMNS_HEADER"] as $value){
    $objPHPExcel->getActiveSheet()->getColumnDimension($arParams["ALFAVITE"][$i])->setWidth(24);
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $value, PHPExcel_Cell_DataType::TYPE_STRING);
    $i++;
}


# send matrix position to new row for busket items
$arParams["MATRIX"]["CELL"]["Y"]++;
$arParams["MATRIX"]["CELL"]["X"] = $arParams["MATRIX"]["START"]["X"];
$arParams["MATRIX"]["CELL"]["ADRESS"] = $arParams["MATRIX"]["CELL"]["X"] . $arParams["MATRIX"]["CELL"]["Y"] ;
?>