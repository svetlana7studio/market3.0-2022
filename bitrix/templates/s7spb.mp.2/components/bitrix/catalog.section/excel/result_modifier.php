<?
/**
 * @var array $arParams
 * @var array $arResult
 */

if(!empty($arResult["ITEMS"])){
    foreach ($arResult["ITEMS"] as $key=>$arElement){

        // bitrix/templates/s7spb.anitos/images/no_image200.jpg
        // preview image
        if(is_array($arElement["PREVIEW_PICTURE"])){
            if($arElement["DETAIL_PICTURE"] > 0){
                $arElement["PREVIEW_PICTURE"] = $arElement["DETAIL_PICTURE"];
            }
        }
        else{
            if(is_array($arElement["DETAIL_PICTURE"])){
                $arElement["PREVIEW_PICTURE"] = $arElement["DETAIL_PICTURE"];
            }
        }


        if(is_array($arElement["PREVIEW_PICTURE"])){
            //AddMessage2Log($arElement["PREVIEW_PICTURE"]);
        }else{
            $arElement["PREVIEW_PICTURE"]["SRC"] = SITE_TEMPLATE_PATH . "/images/no_image200.jpg";
        }

        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arElement["PREVIEW_PICTURE"];



    }
}
