<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?
foreach ($arResult['ITEMS'] as $key=>$aItem) { ?>
    <div class="line-item">
        <?
        $generalParams["KEY"] = $key;

        $APPLICATION->IncludeComponent(
            'bitrix:catalog.item',
            'line',
            array(
                'RESULT' => array(
                    'ITEM' => $aItem,
                    'AREA_ID' => $areaIds[$aItem['ID']],
                    'TYPE' => 'card',
                    'BIG_LABEL' => 'N',
                    'BIG_DISCOUNT_PERCENT' => 'N',
                    'BIG_BUTTONS' => 'N',
                    'SCALABLE' => 'N'
                ),
                'PARAMS' => $generalParams
                    + array('SKU_PROPS' => $arResult['SKU_PROPS'][$aItem['IBLOCK_ID']])
            ),
            $component,
            array('HIDE_ICONS' => 'Y')
        );

        unset($aItem);
        ?>
    </div>
<? } ?>

