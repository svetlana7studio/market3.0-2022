<?
use Bitrix\Main\Localization\Loc;
/**
 * @var array $arParams
 * @var array $arResult
 * @var CMain $APPLICATION
 */

$APPLICATION->IncludeComponent("studio7sbp:section.list", "view.target", [
    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "ID" => $arResult["ITEMS_SECTIONS"],
    "ORDER_SORT_1" => "name",
    "ORDER_BY_1" => "asc",
    "CNT" => "N",
    "TITLE" => Loc::getMessage("CATALOG_SECTIONS_TITLE"),
    "CURRENT_BASE_PAGE" => $arParams["CURRENT_BASE_PAGE"],
    "CORE" => "D7"
], false, ["HIDE_ICONS" => "Y"]);
