<?
/**
 * @var array $areaIds
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


foreach ($arResult['ITEMS'] as $key=>$aItem):

    $APPLICATION->IncludeComponent(
        'bitrix:catalog.item',
        '',
        array(
            'RESULT' => array(
                'ITEM' => $aItem,
                'AREA_ID' => $areaIds[$aItem['ID']],
                'TYPE' => 'card',
                'BIG_LABEL' => 'N',
                'BIG_DISCOUNT_PERCENT' => 'N',
                'BIG_BUTTONS' => 'N',
                'SCALABLE' => 'N'
            ),
            'PARAMS' => $generalParams
        ),
        false,
        array('HIDE_ICONS' => 'Y')
    );

    unset($aItem);
endforeach;
