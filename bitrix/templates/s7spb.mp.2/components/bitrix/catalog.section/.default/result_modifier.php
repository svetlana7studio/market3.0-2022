<?
/**
 * @var array $arParams
 * @var array $arResult
 */

$arParams["RESIZE"] = [
    "height" => 200,
    "width" => 200
];

$arParams["RESIZE_TYPE"] = BX_RESIZE_IMAGE_PROPORTIONAL; // BX_RESIZE_IMAGE_EXACT

if(!empty($arResult["ITEMS"])){

    $arResult["ITEMS_SECTIONS"] = [];

    foreach ($arResult["ITEMS"] as $key=>$arElement){

        // bitrix/templates/s7spb.anitos/images/no_image200.jpg
        // preview image
        if(is_array($arElement["PREVIEW_PICTURE"])){
            if($arElement["DETAIL_PICTURE"] > 0){
                $arElement["PREVIEW_PICTURE"] = $arElement["DETAIL_PICTURE"];
            }
        }
        else{
            if(is_array($arElement["DETAIL_PICTURE"])){
                $arElement["PREVIEW_PICTURE"] = $arElement["DETAIL_PICTURE"];
            }
        }


        if(is_array($arElement["PREVIEW_PICTURE"])){
            // Resize
            $arElement["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                $arElement["PREVIEW_PICTURE"]["ID"],
                $arParams["RESIZE"],
                $arParams["RESIZE_TYPE"],
                false
            );
            $arElement["PREVIEW_PICTURE"] = [
                "SRC" => $arElement["PREVIEW_PICTURE"]["src"],
                "HEIGHT" => $arParams["RESIZE"]["height"],
                "WIDTH" => $arParams["RESIZE"]["width"]
            ];
        }else{
            $arElement["PREVIEW_PICTURE"]["SRC"] = SITE_TEMPLATE_PATH . "/images/no_image200.jpg";
        }

        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = $arElement["PREVIEW_PICTURE"];

        // stiker
        $arElement["PROPERTIES"]["DISCOUNT"]["CLASS"] = "stiker";

        if(!empty($arElement["PROPERTIES"]["DISCOUNT"]["VALUE_ENUM_ID"])){
            foreach ($arElement["PROPERTIES"]["DISCOUNT"]["VALUE_ENUM_ID"] as $i=>$id){
                switch ($id){
                    case "13":
                        $arElement["PROPERTIES"]["DISCOUNT"]["CLASS"] .= " stiker-primary";
                        break;
                    case "14":
                        $arElement["PROPERTIES"]["DISCOUNT"]["CLASS"] .= " stiker-primary stiker-warning";
                        break;
                }
                $arResult['ITEMS'][$key]["STIKER"][] = [
                    "CODE" => $arElement["PROPERTIES"]["DISCOUNT"]["VALUE_XML_ID"][$i],
                    "TITLE" => $arElement["PROPERTIES"]["DISCOUNT"]["VALUE"][$i],
                    "CLASS" => $arElement["PROPERTIES"]["DISCOUNT"]["CLASS"]
                ];
            }
        }

        // sections collect
        $arResult["ITEMS_SECTIONS"][] = $arElement["IBLOCK_SECTION_ID"];
    }
}

/**
 * Send out brand property
 */
$cp = $this->__component;
if (is_object($cp))
{
    $cp->arResult["ITEMS_SECTIONS"] = $arResult["ITEMS_SECTIONS"];
    $cp->SetResultCacheKeys(["ITEMS_SECTIONS"]);
}