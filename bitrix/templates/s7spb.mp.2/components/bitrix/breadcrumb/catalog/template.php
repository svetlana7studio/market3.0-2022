<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

$APPLICATION->AddChainItem($APPLICATION->arPageProperties['TITLE']);

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
$css = $APPLICATION->GetCSSArray();
if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
{
	$strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
}

$strReturn .= '<div class="bx-breadcrumb" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$arrow = ($index > 0? '<i class="fa fa-angle-right"></i>' : '');
	$arroww = ($index > 0? '<i class="fa fa-angle-ri"></i>' : '');
	$link = $arResult[$index]["LINK"];

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
	    // Если НЕ главная
	    if ($arResult[$index]["LINK"] != "/") {

            // строка в массив
            $explodeUrl = explode("/", $arResult[$index]["LINK"]);
            // уберем пустые элементы массива
            $explodeUrl = array_diff($explodeUrl, array(''));
            // берем последний элемент (это и будет символьный код раздела)
            $section_code = array_pop($explodeUrl);
            $sectionID = (int) $section_code;
            // запросим подразделы
            $arSections = getSectionList(
                Array(
                    "IBLOCK_ID" => 2,
                    "IBLOCK_TYPE" => 'marketplace',
                    "SECTION_ID" => ($sectionID > 0) ? $sectionID : 0
                ),
                Array(
                    'NAME',
                    'SECTION_PAGE_URL'
                )
            );
        }
            $strReturn .= '
        <div class="bx-breadcrumb-item" id="bx_breadcrumb_'.$index.'" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            '.$arrow.'
            <a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url">
                <span itemprop="name">'.$title.'</span>
            </a>
            <meta itemprop="position" content="'.($index + 1).'" />';
            if (!empty($arSections)) {
                $strReturn .= '<ul class="breadcrumb-dropdown">';
                foreach ($arSections['CHILDS'] as $key => $item) {
                    $strReturn .= ' <li class="breadcrumb-dropdown-item">
                        <a class="breadcrumb-dropdown-link" href="' . $item['SECTION_PAGE_URL'] . '">' . $item['NAME'] . '</a>
                    </li>';
                }
                $strReturn .= '</ul>';
            }
            $strReturn .= '</div>';
	}
	else
	{

		$strReturn .= '
			<div class="bx-breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				'.$arroww.'
				<span itemprop="name">'.$title.'</span>
				<meta itemprop="position" content="'.($index + 1).'" />
			</div>';
	}
}

$strReturn .= '<div style="clear:both"></div></div>';

return $strReturn;
