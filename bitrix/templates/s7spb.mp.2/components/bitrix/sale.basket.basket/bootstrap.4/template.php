<?
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
$this->addExternalJs($this->GetFolder() . "/js/modal.js");
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>

<div id="basket-empty" class="<?=empty($arResult["GRID"]["ROWS"]) ? "p-5 text-center" : "p-5 text-center d-none"?>">
    <img src="<?=$this->GetFolder()?>/images/empty_cart.svg" class="img-responsive">
    <div class="alert alert-danger mt-3"><?=Loc::getMessage("BASKET_EMPTY")?></div>
</div>

<div id="basket"
     data-page="<?=$arParams["PATH_TO_BASKET"]?>"
     data-emptyimg="<?=$this->GetFolder()?>/images/empty_cart.svg"
     data-empty="<?=Loc::getMessage("BASKET_EMPTY")?>"
     data-warning="<?=Loc::getMessage("BASKET_WARNING")?>">

    <?if(count($arResult["GRID"]["ROWS"]) > 0):?>

        <div class="container pt-1 pb-4">

            <div class="pt4 px2 text-right" id="market2-basket-actions">

                <a href="<?=$APPLICATION->GetCurPageParam("mode=ex", array("mode"))?>"
                   class="pl3 text-black-hole text-decoration-none d-inline-block"><i class="icon icon-excel-primary vertical-align-middle square-16"></i> Выгрузить корзину в Excel</a>
                <a href="#"
                   onclick="return multiplebasketPopup.open(this)"
                   class="pl3 text-black-hole text-decoration-none d-inline-block"><i class="icon icon-save vertical-align-middle square-14"></i> Сохранить корзину</a>
            </div>

            <div class="row border-bottom border-secondary">
                <h1 class="h1 text-uppercase">
                    <?
                    echo Loc::getMessage("BASKET_TITLE");
                    echo " ";
                    echo declension($arResult["ORDERABLE_BASKET_ITEMS_COUNT"], [
                        Loc::getMessage("BASKET_TOTAL_PRICE1"),
                        Loc::getMessage("BASKET_TOTAL_PRICE2"),
                        Loc::getMessage("BASKET_TOTAL_PRICE3")
                    ]);
                    ?>
                </h1>

                <?require "basket.head.php";?>

            </div>

            <?foreach ($arResult["MARKET_LIST"] as $market):?>
                <div class="market-item">
                    <!--- Market head --->
                    <div class="row mt-4 bg-light p-1 mb-xl-3 justify-content-between align-items-center">
                        <div class="store-secton col-12 col-lg-6 py-3">
                            <span class="pr-3 pt-2 pb-2 mr-2 border-right border-dark"><?=Loc::getMessage("BASKET_MARKET_TITLE")?></span><span class="text-primary"><?=$market["NAME"]?></span><?=$market["PREVIEW_TEXT"]?>
                        </div>
                    </div>

                    <!--- Basket items head --->
                    <div class="row align-items-center border border-secondary mt-3 py-2 d-none d-sm-flex">

                        <div class="col-6">
                            <div class="row d-none d-lg-flex">
                                <div class="col pl-md-5"><?=Loc::getMessage("BASKET_ITEMS_HEAD_NAME")?></div>
                                <div class="col col-lg-4 col-xl-3 text-center"><?=Loc::getMessage("BASKET_ITEMS_HEAD_QUANTITY")?></div>
                            </div>
                        </div>
                        <div class="col-6 text-center">
                            <div class="row">
                                <div class="col"><?=Loc::getMessage("BASKET_ITEMS_HEAD_PRICE")?></div>
                                <div class="col"><?=Loc::getMessage("BASKET_ITEMS_HEAD_SUM")?></div>
                                <div class="col"><?=Loc::getMessage("BASKET_ITEMS_HEAD_ACTION")?></div>
                            </div>
                        </div>
                    </div>

                    <!--- Basket items list --->
                    <?
                    foreach ($arResult["GRID"]["ROWS"] as $key => $arItem):

                        if(in_array($arItem["PRODUCT_ID"], $market["ITEMS"]) && $arItem["NOT_AVAILABLE"] < 1):
                    ?>
                            <div class="row align-items-center basket-item border border-top-0 border-secondary"
                                 data-product="<?=$arItem["ID"]?>">
                                <div class="col-5 col-sm-6 py-2">
                                    <div class="row align-items-center">
                                        <div class="col-12 col-lg">

                                            <div class="row align-items-center basket-item-info">
                                                <div class="col col-auto actio-col">
                                                    <input type="checkbox"
                                                           name="product[]"
                                                           value="<?=$arItem["ID"]?>"
                                                           data-id="<?=$arItem["ID"]?>"
                                                           data-productid="<?=$arItem["PRODUCT_ID"]?>"
                                                           class="cursor-pointer product-selector">
                                                    <br>
                                                    <i <?if(in_array($arItem["PRODUCT_ID"], $arResult["USER_FAVORITE"])):?>
                                                        class="item-favorite-btn item-favorite-fill"
                                                    <?else:?>
                                                        class="item-favorite-btn item-favorite-icon"
                                                    <?endif;?>
                                                            title="<?=Loc::getMessage("BASKET_HEAD_CHECKED_FAVORITE_WONT")?>"
                                                            onclick="saleBasket.toggleFavorite(this, <?=$arItem["PRODUCT_ID"]?>)"></i>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-5 col-lg-4 col-xl-3">



                                                    <?if(!empty($arItem["PREVIEW_PICTURE_SRC"])):?>
                                                        <img src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>"
                                                             width="100%"
                                                             class="img-responsive">
                                                    <?else:?>
                                                        <img src="<?=$this->GetFolder()?>/images/no_photo.png"
                                                             width="100%"
                                                             class="img-responsive">
                                                    <?endif;?>
                                                </div>
                                                <div class="col">
                                                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-12 col-lg-4 col-xl-3 mt-3 mt-lg-0 text-lg-center">

                                            <div class="input-group"
                                                 data-currency="<?=$arResult["CURRENCIES_FORMAT"][$arItem["CURRENCY"]]?>"
                                                 data-sum="<?=$arItem["SUM_VALUE"]?>"
                                                 data-price="<?=$arItem["PRICE"]?>"
                                                 data-discont="<?=$arItem["DISCOUNT_PRICE"]?>">
                                                <a class="input-group-prepend" onclick="saleBasket.count(this, false)">
                                                    <span class="input-group-text">-</span>
                                                </a>
                                                <input type="text"
                                                       class="form-control text-center"
                                                       value="<?=$arItem["QUANTITY"]?>"
                                                       name="count">
                                                <a class="input-group-append" onclick="saleBasket.count(this, true)">
                                                    <span class="input-group-text">+</span>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-7 col-sm-6 text-center">
                                    <div class="row align-items-center">
                                        <div class="col-12 col-sm-4 item-price py-2">
                                            <?if($arItem["DISCOUNT_PRICE"] > 0):?>
                                                <?=$arItem["PRICE_FORMATED"]?>
                                                <s><?=$arItem["FULL_PRICE_FORMATED"]?></s>
                                                <div class="text-danger"><?=Loc::getMessage("DISCOUNT_PRICE", array("DISCOUNT" => $arItem["DISCOUNT_PRICE_FORMATED"]))?></div>
                                            <?else:?>
                                                <?=$arItem["PRICE_FORMATED"]?>
                                            <?endif;?>
                                        </div>
                                        <div class="col-12 col-sm-4 item-sum py-2">
                                            <?=$arItem["SUM"]?>
                                        </div>
                                        <div class="col-12 col-sm-4 py-2">
                                            <a onclick="saleBasket.delete(this)"
                                               data-title="<?=Loc::getMessage("BASKET_ITEM_DELETE_CONFIRM_DELETE")?>"
                                               data-delete="<?=Loc::getMessage("BASKET_ITEM_DELETE_CONFIRM")?>"
                                               data-cancel="<?=Loc::getMessage("BASKET_ITEM_DELETE_CANCEL")?>"
                                               class="trash-link text-primary"><?=Loc::getMessage("BASKET_ITEM_DELETE")?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?
                        unset($arResult["GRID"]["ROWS"][$key]);
                        endif;
                    endforeach;
                    ?>
                </div>
            <?endforeach;?>

            <?if(count($arResult["GRID"]["ROWS"]) > 0):?>
                <!--- Basket items head --->
                <div class="row align-items-center border border-secondary mt-3 py-2 d-none d-sm-flex">

                    <div class="col-6">
                        <div class="row d-none d-lg-flex">
                            <div class="col pl-md-5"><?=Loc::getMessage("BASKET_ITEMS_HEAD_NAME")?></div>
                            <div class="col col-lg-4 col-xl-3 text-center"><?=Loc::getMessage("BASKET_ITEMS_HEAD_QUANTITY")?></div>
                        </div>
                    </div>
                    <div class="col-6 text-center">
                        <div class="row">
                            <div class="col"><?=Loc::getMessage("BASKET_ITEMS_HEAD_PRICE")?></div>
                            <div class="col"><?=Loc::getMessage("BASKET_ITEMS_HEAD_SUM")?></div>
                            <div class="col"><?=Loc::getMessage("BASKET_ITEMS_HEAD_ACTION")?></div>
                        </div>
                    </div>
                </div>

                <!--- Basket market items list --->
                <?
                if(!empty($arResult["GRID"]["ROWS"])):
                    foreach ($arResult["GRID"]["ROWS"] as $key => $arItem):
                        if($arItem["NOT_AVAILABLE"] < 1):
                        ?>
                        <div class="row align-items-center basket-item border border-top-0 border-secondary"
                             data-product="<?=$arItem["ID"]?>">
                            <div class="col-5 col-sm-6 py-2">
                                <div class="row align-items-center">
                                    <div class="col-12 col-lg">

                                        <div class="row align-items-center basket-item-info">

                                            <div class="col col-auto actio-col">
                                                <input type="checkbox"
                                                       name="product[]"
                                                       value="<?=$arItem["ID"]?>"
                                                       data-id="<?=$arItem["ID"]?>"
                                                       data-productid="<?=$arItem["PRODUCT_ID"]?>"
                                                       class="cursor-pointer product-selector">
                                                <br>
                                                <i <?if(in_array($arItem["PRODUCT_ID"], $arResult["USER_FAVORITE"])):?>
                                                    class="item-favorite-btn item-favorite-fill"
                                                <?else:?>
                                                    class="item-favorite-btn item-favorite-icon"
                                                <?endif;?>
                                                        title="<?=Loc::getMessage("BASKET_HEAD_CHECKED_FAVORITE_WONT")?>"
                                                        onclick="saleBasket.toggleFavorite(this, <?=$arItem["PRODUCT_ID"]?>)"></i>
                                            </div>

                                            <div class="col-12 col-sm-6 col-md-5 col-lg-4 col-xl-3">
                                                <?if(!empty($arItem["PREVIEW_PICTURE_SRC"])):?>
                                                    <img src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>"
                                                         width="100%"
                                                         class="img-responsive">
                                                <?else:?>
                                                    <img src="<?=$this->GetFolder()?>/images/no_photo.png"
                                                         width="100%"
                                                         class="img-responsive">
                                                <?endif;?>
                                            </div>
                                            <div class="col">
                                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-12 col-lg-4 col-xl-3 mt-3 mt-lg-0 text-lg-center">

                                        <div class="input-group"
                                             data-currency="<?=$arResult["CURRENCIES_FORMAT"][$arItem["CURRENCY"]]?>"
                                             data-sum="<?=$arItem["SUM_VALUE"]?>"
                                             data-price="<?=$arItem["PRICE"]?>"
                                             data-discont="<?=$arItem["DISCOUNT_PRICE"]?>">
                                            <a class="input-group-prepend" onclick="saleBasket.count(this, false)">
                                                <span class="input-group-text">-</span>
                                            </a>
                                            <input type="text"
                                                   class="form-control text-center"
                                                   value="<?=$arItem["QUANTITY"]?>"
                                                   name="count">
                                            <a class="input-group-append" onclick="saleBasket.count(this, true)">
                                                <span class="input-group-text">+</span>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-7 col-sm-6 text-center">
                                <div class="row align-items-center">
                                    <div class="col-12 col-sm-4 item-price py-2">
                                        <?if($arItem["DISCOUNT_PRICE"] > 0):?>
                                            <?=$arItem["PRICE_FORMATED"]?>
                                            <s><?=$arItem["FULL_PRICE_FORMATED"]?></s>
                                            <div class="text-danger"><?=Loc::getMessage("DISCOUNT_PRICE", array("DISCOUNT" => $arItem["DISCOUNT_PRICE_FORMATED"]))?></div>
                                        <?else:?>
                                            <?=$arItem["PRICE_FORMATED"]?>
                                        <?endif;?>
                                    </div>
                                    <div class="col-12 col-sm-4 item-sum py-2">
                                        <?=$arItem["SUM"]?>
                                    </div>
                                    <div class="col-12 col-sm-4 py-2">
                                        <a onclick="saleBasket.delete(this)"
                                           data-title="<?=Loc::getMessage("BASKET_ITEM_DELETE_CONFIRM_DELETE")?>"
                                           data-delete="<?=Loc::getMessage("BASKET_ITEM_DELETE_CONFIRM")?>"
                                           data-cancel="<?=Loc::getMessage("BASKET_ITEM_DELETE_CANCEL")?>"
                                           class="trash-link text-primary"><?=Loc::getMessage("BASKET_ITEM_DELETE")?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?
                        unset($arResult["GRID"]["ROWS"][$key]);
                        endif;
                    endforeach;
                endif;
                ?>
            <?endif;?>

            <!--- Basket not avaliable items list --->
            <?if(!empty($arResult["GRID"]["ROWS"])):?>
                <div class="row mt-4 bg-light p-1 mb-xl-3 justify-content-between align-items-center">
                    <div class="store-secton col-12 col-lg-6 py-3"><?=Loc::getMessage("BASKET_NOTAVALIABLE_TITLE")?></div>
                </div>

                <div class="row align-items-center border border-secondary mt-3 py-2 d-none d-sm-flex">

                    <div class="col-6">
                        <div class="row d-none d-lg-flex">
                            <div class="col pl-md-5"><?=Loc::getMessage("BASKET_ITEMS_HEAD_NAME")?></div>
                            <div class="col col-lg-4 col-xl-3 text-center"><?=Loc::getMessage("BASKET_ITEMS_HEAD_QUANTITY")?></div>
                        </div>
                    </div>
                    <div class="col-6 text-center">
                        <div class="row">
                            <div class="col"><?=Loc::getMessage("BASKET_ITEMS_HEAD_PRICE")?></div>
                            <div class="col"><?=Loc::getMessage("BASKET_ITEMS_HEAD_SUM")?></div>
                            <div class="col"><?=Loc::getMessage("BASKET_ITEMS_HEAD_ACTION")?></div>
                        </div>
                    </div>
                </div>
                <?foreach ($arResult["GRID"]["ROWS"] as $key => $arItem):?>
                    <div class="row align-items-center basket-item border border-top-0 border-secondary"
                         data-product="<?=$arItem["ID"]?>">
                        <div class="col-5 col-sm-6 py-2">
                            <div class="row align-items-center">
                                <div class="col-12 col-lg">

                                    <div class="row align-items-center basket-item-info">

                                        <div class="col col-auto actio-col">
                                            <input type="checkbox"
                                                   name="product[]"
                                                   value="<?=$arItem["ID"]?>"
                                                   data-id="<?=$arItem["ID"]?>"
                                                   data-productid="<?=$arItem["PRODUCT_ID"]?>"
                                                   class="cursor-pointer product-selector">
                                            <br>
                                            <i <?if(in_array($arItem["PRODUCT_ID"], $arResult["USER_FAVORITE"])):?>
                                                class="item-favorite-btn item-favorite-fill"
                                            <?else:?>
                                                class="item-favorite-btn item-favorite-icon"
                                            <?endif;?>
                                                    title="<?=Loc::getMessage("BASKET_HEAD_CHECKED_FAVORITE_WONT")?>"
                                                    onclick="saleBasket.toggleFavorite(this, <?=$arItem["PRODUCT_ID"]?>)"></i>
                                        </div>

                                        <div class="col-12 col-sm-6 col-md-5 col-lg-4 col-xl-3">
                                            <?if(!empty($arItem["PREVIEW_PICTURE_SRC"])):?>
                                                <img src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>"
                                                     width="100%"
                                                     class="img-responsive">
                                            <?else:?>
                                                <img src="<?=$this->GetFolder()?>/images/no_photo.png"
                                                     width="100%"
                                                     class="img-responsive">
                                            <?endif;?>
                                        </div>
                                        <div class="col">
                                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-12 col-lg-4 col-xl-3 mt-3 mt-lg-0 text-lg-center text-danger"><?=Loc::getMessage("BASKET_NOTAVALIABLE")?></div>
                            </div>
                        </div>
                        <div class="col-7 col-sm-6 text-center">
                            <div class="row align-items-center">
                                <div class="col-12 col-sm-4 item-price py-2">
                                    <?if($arItem["DISCOUNT_PRICE"] > 0):?>
                                        <?=$arItem["PRICE_FORMATED"]?>
                                        <s><?=$arItem["FULL_PRICE_FORMATED"]?></s>
                                        <div class="text-danger"><?=Loc::getMessage("DISCOUNT_PRICE", array("DISCOUNT" => $arItem["DISCOUNT_PRICE_FORMATED"]))?></div>
                                    <?else:?>
                                        <?=$arItem["PRICE_FORMATED"]?>
                                    <?endif;?>
                                </div>
                                <div class="col-12 col-sm-4 item-sum py-2">
                                    <?=$arItem["SUM"]?>
                                </div>
                                <div class="col-12 col-sm-4 py-2">
                                    <a onclick="saleBasket.delete(this)"
                                       data-title="<?=Loc::getMessage("BASKET_ITEM_DELETE_CONFIRM_DELETE")?>"
                                       data-delete="<?=Loc::getMessage("BASKET_ITEM_DELETE_CONFIRM")?>"
                                       data-cancel="<?=Loc::getMessage("BASKET_ITEM_DELETE_CANCEL")?>"
                                       class="trash-link text-primary"><?=Loc::getMessage("BASKET_ITEM_DELETE")?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?endforeach;?>
            <?endif;?>

            <div class="border-top border-primary row mt-3">
                <div class="bg-light col-12 col-lg-8 mt-3 py-2">

                    <?if ($arParams["HIDE_COUPON"] != "Y"):?>
                        <div class="row m-2">
                            <div class="cupon-secton col-12">
                                <form class="form-inline"
                                      method="post"
                                      onsubmit="return saleBasket.cuponUse(this)"
                                      action="<?=$arParams["PATH_TO_BASKET"]?>">
                                    <?=bitrix_sessid_post()?>
                                    <input type="hidden" name="site_id" value="<?=SITE_ID?>">
                                    <input type="hidden" name="<?=htmlspecialcharsbx($arParams["ACTION_VARIABLE"])?>" value="recalculateAjax">
                                    <input type="hidden" name="via_ajax" value="Y">

                                    <div class="form-group">
                                        <label for="input-cupon" class="mr-md-2"><?=Loc::getMessage("BASKET_CUPON_TITLE")?></label>
                                        <input type="text"
                                               class="form-control m-0"
                                               name="basket[coupon]" />
                                    </div>
                                    <button type="submit" class="btn btn-primary ml-md-2"><?=Loc::getMessage("BASKET_CUPON_SUBMIT")?></button>
                                </form>
                            </div>
                        </div>

                        <?if(!empty($arResult["COUPON_LIST"])):?>
                            <div class="row m-2 coupon">
                                <?foreach ($arResult["COUPON_LIST"] as $coupon):?>
                                    <div class="col-12 col-xl-8">
                                        <div class="input-group">

                                            <input type="text" class="form-control" value="<?=$coupon["COUPON"]?>" disabled />
                                            <a class="input-group-append" onclick="saleBasket.deleteCoupon('<?=$coupon["COUPON"]?>');">
                                                <span class="input-group-text btn"><?=Loc::getMessage("BASKET_ITEM_DEACTIVATE")?></span>
                                            </a>
                                        </div>
                                    </div>
                                <?endforeach;?>
                            </div>
                            <?
                            unset($coupon);
                        endif;
                        ?>

                    <?endif;?>

                    <div class="row m-2">
                        <div class="col-7"><b><?
                                echo Loc::getMessage("BASKET_TOTAL_PRICE") . " (";
                                echo declension($arResult["ORDERABLE_BASKET_ITEMS_COUNT"], array(
                                    Loc::getMessage("BASKET_TOTAL_PRICE1"),
                                    Loc::getMessage("BASKET_TOTAL_PRICE2"),
                                    Loc::getMessage("BASKET_TOTAL_PRICE3")
                                ));
                                echo ")";
                                ?>:</b></div>
                        <div class="col"><b id="basket-without-discont"><?=$arResult["PRICE_WITHOUT_DISCOUNT"]?></b></div>
                    </div>

                    <div class="row m-2 text-danger">
                        <div class="col-7"><b><?=Loc::getMessage("BASKET_TOTAL_DISCOUNT")?>:</b></div>
                        <div class="col"><b id="basket-discount"><?=$arResult["DISCOUNT_PRICE_FORMATED"]?></b></div>
                    </div>

                    <div class="row m-2 h3">
                        <div class="col-7"><?=Loc::getMessage("BASKET_TOTAL")?>:</div>
                        <div class="col"><b id="basket-all-sum"><?=$arResult["allSum_FORMATED"]?></b></div>
                    </div>

                </div>
                <div class="bg-light col-12 col-lg-4 mt-3 text-right pt-3">

                    <a onclick="saleBasket.deleteAll(this)"
                       data-pic="<?=$this->GetFolder()?>/images/empty_cart.svg"
                       data-title="<?=Loc::getMessage("BASKET_ITEM_CONFIRM_DELETE_ALL")?>"
                       data-delete="<?=Loc::getMessage("BASKET_ITEM_DELETE_CONFIRM")?>"
                       data-cancel="<?=Loc::getMessage("BASKET_ITEM_DELETE_CANCEL")?>"
                       class="trash-link text-primary"><?=Loc::getMessage("BASKET_DELETE_ALL")?></a>


                    <div class="mt-2">
                        <a href="<?=$arParams["PATH_TO_ORDER"]?>" class="btn btn-primary h4"><?=Loc::getMessage("BASKET_SUBMIT")?></a>
                    </div>
                </div>
            </div>

        </div>

        <div class="modal fade d-none" id="modal-basket">
            <div class="modal-dialog m-0" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close" onclick="saleModal.hide();">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body text-center"></div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>

        <div class="modal-backdrop fade d-none"
             id="modal-basket-backdrop"
             onclick="saleModal.hide();"></div>

    <?endif;?>
</div>
