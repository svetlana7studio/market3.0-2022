<?
/**
 * All basket functions
 */
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;
Loc::loadLanguageFile(__FILE__);
$request = Application::getInstance()->getContext()->getRequest();

// <editor-fold defaultstate="collapsed" desc=" # arParams Sort">

/**
 * Sort function
 * @param array $arr
 * @param null $order
 * @return array
 */
function arrayOrderBy(array &$arr, $order = null) {
    if (is_null($order)) {
        return $arr;
    }
    $orders = explode(',', $order);
    usort($arr, function($a, $b) use($orders) {
        $result = array();
        foreach ($orders as $value) {
            list($field, $sort) = array_map('trim', explode(' ', trim($value)));
            if (!(isset($a[$field]) && isset($b[$field]))) {
                continue;
            }
            if (strcasecmp($sort, 'desc') === 0) {
                $tmp = $a;
                $a = $b;
                $b = $tmp;
            }
            if (is_numeric($a[$field]) && is_numeric($b[$field]) ) {
                $result[] = $a[$field] - $b[$field];
            } else {
                $result[] = strcmp($a[$field], $b[$field]);
            }
        }
        return implode('', $result);
    });
    return $arr;
}

$arParams["SORT"]["PARAMS"] = [
    "sort_date",
    "sort_new",
    "sort_price",
    "sort_sum",
    "sort_name",
    "sort_article"
];
$arParams["SORT"]["ITEMS"] = [
    "DATE" => [
        "TEXT" => Loc::getMessage("BASKET_HEAD_SORT_DATE"),
        "HREF" => $APPLICATION->GetCurPageParam("sort_date=asc", $arParams["SORT"]["PARAMS"])
    ],
    "NEW" => [
        "TEXT" => Loc::getMessage("BASKET_HEAD_SORT_NEW"),
        "HREF" => $APPLICATION->GetCurPageParam("sort_new=asc", $arParams["SORT"]["PARAMS"])
    ],
    "PRICE" => [
        "TEXT" => Loc::getMessage("BASKET_HEAD_SORT_PRICE"),
        "HREF" => $APPLICATION->GetCurPageParam("sort_price=asc", $arParams["SORT"]["PARAMS"])
    ],
    "SUM" => [
        "TEXT" => Loc::getMessage("BASKET_HEAD_SORT_SUM"),
        "HREF" => $APPLICATION->GetCurPageParam("sort_sum=asc", $arParams["SORT"]["PARAMS"])
    ],
    "NAME" => [
        "TEXT" => Loc::getMessage("BASKET_HEAD_SORT_NAME"),
        "HREF" => $APPLICATION->GetCurPageParam("sort_name=asc", $arParams["SORT"]["PARAMS"])
    ],
    "ARTICLE" => [
        "TEXT" => Loc::getMessage("BASKET_HEAD_SORT_ARTICLE"),
        "HREF" => $APPLICATION->GetCurPageParam("sort_article=asc", $arParams["SORT"]["PARAMS"])
    ]
];

# sorting by date
$sort = "sort_date";
if($request->get($sort) == "asc"){
    $arParams["SORT"]["ITEMS"]["DATE"]["ARROW"] = "&darr;";
    $arParams["SORT"]["ITEMS"]["DATE"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=desc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'DATE_INSERT asc');
}elseif($request->get($sort) == "desc"){
    $arParams["SORT"]["ITEMS"]["DATE"]["ARROW"] = "&uarr;";
    $arParams["SORT"]["ITEMS"]["DATE"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=asc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'DATE_INSERT desc');
}
# sorting by new
$sort = "sort_new";
if($request->get($sort) == "asc"){
    $arParams["SORT"]["ITEMS"]["NEW"]["ARROW"] = "&darr;";
    $arParams["SORT"]["ITEMS"]["NEW"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=desc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'PRODUCT_ID asc');
}elseif($request->get($sort) == "desc"){
    $arParams["SORT"]["ITEMS"]["NEW"]["ARROW"] = "&uarr;";
    $arParams["SORT"]["ITEMS"]["NEW"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=asc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'PRODUCT_ID desc');
}

# sorting by price
$sort = "sort_price";
if($request->get($sort) == "asc"){
    $arParams["SORT"]["ITEMS"]["PRICE"]["ARROW"] = "&darr;";
    $arParams["SORT"]["ITEMS"]["PRICE"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=desc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'PRICE asc');
}elseif($request->get($sort) == "desc"){
    $arParams["SORT"]["ITEMS"]["PRICE"]["ARROW"] = "&uarr;";
    $arParams["SORT"]["ITEMS"]["PRICE"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=asc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'PRICE desc');
}

# sorting by sum
$sort = "sort_sum";
if($request->get($sort) == "asc"){
    $arParams["SORT"]["ITEMS"]["SUM"]["ARROW"] = "&darr;";
    $arParams["SORT"]["ITEMS"]["SUM"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=desc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'SUM_VALUE asc');
}elseif($request->get($sort) == "desc"){
    $arParams["SORT"]["ITEMS"]["SUM"]["ARROW"] = "&uarr;";
    $arParams["SORT"]["ITEMS"]["SUM"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=asc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'SUM_VALUE desc');
}

# sorting by name
$sort = "sort_name";
if($request->get($sort) == "asc"){
    $arParams["SORT"]["ITEMS"]["NAME"]["ARROW"] = "&darr;";
    $arParams["SORT"]["ITEMS"]["NAME"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=desc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'NAME asc');

}elseif($request->get($sort) == "desc"){
    $arParams["SORT"]["ITEMS"]["NAME"]["ARROW"] = "&uarr;";
    $arParams["SORT"]["ITEMS"]["NAME"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=asc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'NAME desc');
}

# sorting by article
$sort = "sort_article";
if($request->get($sort) == "asc"){
    $arParams["SORT"]["ITEMS"]["ARTICLE"]["ARROW"] = "&darr;";
    $arParams["SORT"]["ITEMS"]["ARTICLE"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=desc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'PROPERTY_ARTICLE_VALUE asc');
}elseif($request->get($sort) == "desc"){
    $arParams["SORT"]["ITEMS"]["ARTICLE"]["ARROW"] = "&uarr;";
    $arParams["SORT"]["ITEMS"]["ARTICLE"]["HREF"] = $APPLICATION->GetCurPageParam($sort . "=asc", $arParams["SORT"]["PARAMS"]);
    $arResult["GRID"]["ROWS"] = arrayOrderBy($arResult["GRID"]["ROWS"], 'PROPERTY_ARTICLE_VALUE desc');
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # arParams Actions">
$arParams["ACTIONS"] = [
    "ITEMS" => [
        [
            "TEXT" => Loc::getMessage("BASKET_HEAD_ACTIONS_ADD_ARTICLE"),
            "CLICK" => 'return saleBasket.fileShowForm(this)',
            "DATA" => [
                "title" => Loc::getMessage("BASKET_HEAD_ACTIONS_ADD_ARTICLE"),
                "btn-yes" => Loc::getMessage("BASKET_ARTICLE_BTN_YES"),
                "btn-more" => Loc::getMessage("BASKET_ARTICLE_BTN_MORE"),
                "btn-desc" => Loc::getMessage("BASKET_ARTICLE_DESC"),
                "btn-no" => Loc::getMessage("BASKET_ARTICLE_BTN_NO"),
                "input" => Loc::getMessage("BASKET_ARTICLE_INPUT")
            ]
        ],
        [
            "TEXT" => Loc::getMessage("BASKET_HEAD_ACTIONS_ADD_FILE"),
            "CLICK" => 'return saleBasket.excelShowForm(this)',
            "DATA" => [
                "title" => Loc::getMessage("BASKET_HEAD_ACTIONS_ADD_FILE"),
                "btn-yes" => Loc::getMessage("BASKET_ARTICLE_BTN_YES"),
                "btn-no" => Loc::getMessage("BASKET_ARTICLE_BTN_NO"),
                "btn-desc" => Loc::getMessage("BASKET_ARTICLE_DESC"),
                "input" => Loc::getMessage("BASKET_FILE_INPUT")
            ]
        ],
        [
            "TEXT" => Loc::getMessage("BASKET_HEAD_ACTIONS_ADD_SAVE"),
            "CLICK" => 'return saleBasket.saveCsv(this)',
            "DATA" => [
                "url" => "/basket/save.csv/"
            ]
        ],
        [
            "TEXT" => Loc::getMessage("BASKET_HEAD_ACTIONS_ADD_SHARE"),
            "CLICK" => 'return saleBasket.shareBasket(this)',
            "DATA" => [
                "title" => Loc::getMessage("BASKET_HEAD_SHARE_TITLE"),
                "products" => implode(",", $arResult["PRODUCT_IDs"]),
                "host" => SITE_SERVER_NAME
            ]
        ],
    ]
];

// </editor-fold>

?>

<div class="s7spb-row s7spb-row-center s7spb-content-between mb2">
    <div class="s7spb-col">
        <?=Loc::getMessage("BASKET_HEAD_SORT_TITLE")?>:

        <?foreach ($arParams["SORT"]["ITEMS"] as $i=>$arItem):?>
            <a href="<?=$arItem["HREF"]?>"
               class="text-decoration-none pl3 pr2 text-13 <?=empty($arItem["ARROW"]) ? "text-gray" : "text-primary"?>">
                <?
                echo $arItem["TEXT"];
                if(!empty($arItem["ARROW"]))
                    echo " " . $arItem["ARROW"];
                ?>
            </a>
        <?endforeach;?>
    </div>
    <div class="s7spb-col s7spb-col-auto text-right">

        <a href="<?=$_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : "/catalog/"?>" class="text-primary px3 text-decoration-none"><?=Loc::getMessage("BACK_TOCATALOG")?></a>

    </div>
</div>

<div class="s7spb-row s7spb-row-center line-s7 text-semibold">
    <div class="s7spb-col s7spb-col-auto colum-na">
        <input type="checkbox" onclick="saleBasket.selector(this)" class="cursor-pointer">
        <?=Loc::getMessage("BASKET_HEAD_CHECKED_TITLE")?>
        <span class="px1 cursor-pointer"
              data-title="<?=Loc::getMessage("BASKET_CNT_DELETES_TITLE")?>"
              data-desc="<?=Loc::getMessage("BASKET_CNT_DELETES_DESC")?>"
              data-delete="<?=Loc::getMessage("BASKET_CNT_DELETE")?>"
              data-cancel="<?=Loc::getMessage("BASKET_CNT_CANCELS")?>"
              title="<?=Loc::getMessage("BASKET_CNT_REMOVE")?>"
              onclick="saleBasket.deleteSelectedConfirm(this)">
            <i class="icon icon-delete square-14"></i> <?=Loc::getMessage("BASKET_HEAD_CHECKED_DELETE")?>
        </span>
        <span class="px1 cursor-pointer"
              title="<?=Loc::getMessage("BASKET_ITEM_FAVORITE")?>"
              onclick="saleBasket.favoriteSelected()">
            <i class="item-favorite-fill"></i>
            <?=Loc::getMessage("BASKET_HEAD_CHECKED_FAVORITE_WONT")?>
        </span>
    </div>
    <div class="s7spb-col text-right tex-cen">
        <?foreach ($arParams["ACTIONS"]["ITEMS"] as $arItem):?>
            <span class="position-relative">

                <a href="#"
                   <?if(isset($arItem["CLICK"])):?>
                       onclick='<?=$arItem["CLICK"]?>'
                   <?endif;?>
                    <?if(is_array($arItem["DATA"])):?>
                        <?foreach ($arItem["DATA"] as $key=>$value):?>
                            data-<?=$key?>="<?=$value?>"
                        <?endforeach;?>
                    <?endif;?>
                   class="p3 text-black-hole text-decoration-none d-inline-block"><?=$arItem["TEXT"]?></a>

            </span>
        <?endforeach;?>
    </div>
</div>