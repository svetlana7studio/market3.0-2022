;(function() {
    'use strict';

    window.saleBasket = {

        basket: {
            id: "basket",
            jqId: "#basket",
        },
        market: {
            class: ".market-item"
        },
        item: {
            class: ".basket-item",
            current: null
        },
        share: {
            id: "element-share",
            active: "d-none"
        },
        article: {
            alert: null,
            action: "/basket/article.fill/",
            form: null,
            data: [],
        },
        excel: {
            action: "/basket/excel/",
            form: null,
            alert: null
        },
        file: {
            action: "/basket/file.fill/",
            form: null,
            alert: null
        },
        obPopupWin: null,

        deleteAll: function (t) {
            this.item.current = t;

            // header title
            saleModal.title.content = $(t).data("title");
            // body
            saleModal.body.content = '<img class="img-thumbnail" src="'+  $(t).data("pic") + '">';
            // footer
            saleModal.footer.content = '<button class="btn btn-danger" onclick="saleBasket.removeAll();">' + $(t).data("delete") + '</button>';
            saleModal.footer.content += '<button class="btn btn-primary" onclick="saleModal.hide();">' + $(t).data("cancel") + '</button>';

            saleModal.show();
        },
        delete: function (t) {
            this.item.current = t;

            // header title
            saleModal.title.content = $(t).data("title");
            saleModal.body.content = $(this.item.current).closest(this.item.class).find(".basket-item-info");
            saleModal.body.content.find(".actio-col").remove();
            saleModal.body.content = saleModal.body.content.clone();
            $(saleModal.body.content).find("img")
                .addClass("img-thumbnail")
                .addClass("mb-3")
                .parent()
                .removeAttr("class")
                .addClass("col-12");
            saleModal.body.content = saleModal.body.content.html();
            // footer
            saleModal.footer.content = '<button class="btn btn-danger" onclick="saleBasket.remove();">' + $(t).data("delete") + '</button>';
            saleModal.footer.content += '<button class="btn btn-primary" onclick="saleModal.hide();">' + $(t).data("cancel") + '</button>';

            saleModal.show();
        },
        remove: function() {

            var market = $(this.item.current).closest(this.market.class),
                item = $(this.item.current).closest(this.item.class),
                request = BX.ajax.runAction('studio7spb:marketplace.api.tools.deleteBasket', {
                    data: {
                        id: item.data("product")
                    }
                });

            request.then(function(response){
                s7market.updateBasket();
            });

            item.remove();

            if(market.find(this.item.class).length <= 0){
                market.remove();
            }

            this.item.current = null;
            saleModal.hide();
            this.calculate();
        },
        removeAll: function() {
            var request = BX.ajax.runAction('studio7spb:marketplace.api.tools.deleteBasket');
            request.then(function(response){
                location.reload();
            });
        },
        count: function(t, type){

            var counter = $(t).parent(),
                input = counter.find("input"),
                value = parseInt(input.val()),
                price = counter.data("price"),
                //sum = counter.data("sum"),
                item = $(t).closest(this.item.class),
                itemSum = item.find(".item-sum"),
                currency = counter.data("currency");

            if(type){
                value++;
            }else{
                value--;
            }

            if(value < 1){
                value = 1;
            }

            input.val(value);
            price = parseFloat(price);
            price = price * value;

            this.itemSumAnimate(itemSum, price, currency);

            this.calculate();

            BX.ajax.runAction('studio7spb:marketplace.api.tools.counterBasket', {
                data: {
                    id: item.data("product"),
                    quantity: value
                }
            }).then(function(response){
                window.saleBasket.recalcBasketAjax();
            });

        },

        isInteger: function (value){

            return Math.ceil(parseFloat(value)) === value;
        },

        itemSumAnimate: function (itemSum, price, currency){
            var sum = price;

            $({numberValue: 1}).animate({numberValue: sum}, {
                duration: 200,
                easing: 'linear',
                step: function() {
                    this.numberValue = Math.ceil(this.numberValue);
                    itemSum.text(currency.replace('#', this.numberValue));
                },
                complete: function () {
                    window.saleBasket.itemSumFormate(itemSum, price, currency);
                }
            });
        },
        itemSumFormate: function (itemSum, price, currency){
            var priceRound = 2;
            if(this.isInteger(price))
                priceRound = 0;
            price = price.toLocaleString('ru-RU',{
                minimumFractionDigits: priceRound,
                maximumFractionDigits: priceRound
            });
            price = price.replace(",", ".");
            price = currency.replace('#', price);
            itemSum.text(price);
        },

        recalcBasketAjax: function(){
            var action,
                postData = {
                    sessid: BX.bitrix_sessid(),
                    mode: "json"
                };

            action = window.saleBasket.basket.jqId;
            action = $(action).data("page");

            BX.ajax({
                url: action,
                method: 'POST',
                data: postData,
                dataType: 'json',
                onsuccess: function(result)
                {
                    $("#basket-without-discont").html(result.RESULT.PRICE_WITHOUT_DISCOUNT);
                    $("#basket-discount").html(result.RESULT.DISCOUNT_PRICE_ALL_FORMATED);
                    $("#basket-all-sum").html(result.RESULT.allSum_FORMATED);
                }
            });
        },

        deleteCoupon: function(coupon){

            var action,
                postData = {
                    sessid: BX.bitrix_sessid(),
                    mode: "json",
                    activity: "delete_coupon",
                    coupon: coupon
                };

            action = window.saleBasket.basket.jqId;
            action = $(action).data("page");

            BX.ajax({
                url: action,
                method: 'POST',
                data: postData,
                dataType: 'json',
                onsuccess: function(result)
                {
                    location.reload();
                }
            });

        },

        calculate: function(){

            var basket = $("#basket"),
                sum = 0,
                price = 0,
                discont = 0,
                count = 0,
                currency = "";

            if(basket.find(".input-group").length > 0){
                basket.find(".input-group").each(function () {
                    count = $(this).find("input").val();
                    count = parseInt(count);
                    // calculate
                    price += parseInt($(this).data("price")) * count;
                    discont += parseInt($(this).data("discont")) * count;
                    // get currency
                    currency = $(this).data("currency");
                });

                sum = price + discont;

                $("#basket-without-discont").text(currency.replace('#', sum));
                $("#basket-discount").text(currency.replace('#', discont));
                $("#basket-all-sum").text(currency.replace('#', price));
            }
            else{
                basket.html(null);
                $("#basket-empty").removeClass("d-none");
            }



        },
        closeBasket: function(){

        },
        // basket
        cuponUse: function(t){
            $(t).find(".btn").addClass("disabled");
            $(t).find(".form-control").addClass("disabled");
            $.post($(t).attr("action"), $(t).serializeArray(), (result) => {
                location.reload();
            });

            return false;
        },

        fileShowForm: function(t){

            var popupContent = '<form onsubmit="return saleBasket.fileShowFormSubmit(this)" action="' + this.file.action + '" method="post" enctype="multipart/form-data">';
            popupContent += '<div class="alert py2 mb2 d-none"></div>';
            popupContent += '<div class="form-group pb2">';
            popupContent += '<lable class="pb1">' + $(t).data("btn-desc") + ':</lable>';
            popupContent += '<a href="/basket/sample.csv/" class="pb1 d-block">' + $(t).data("input") + ':</a>';
            popupContent += "<textarea name='data'></textarea>";
            popupContent += '</div>';
            popupContent += '<input type="submit" class="btn btn-danger" value="' + $(t).data("btn-yes") + '" />';
            popupContent += '</form>';


            this.obPopupWin = BX.PopupWindowManager.create('file_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                content: popupContent,
                closeIcon: true,
                contentColor: 'white',
                className: "filePopup"
            });

            this.obPopupWin.setTitleBar($(t).data("title"));
            //this.obPopupWin.setContent(popupContent);
            this.obPopupWin.show();

            return false;
        },

        fileShowFormSubmit: function(t){
            this.file.form = $(t);

            this.file.alert = this.file.form.find(".alert");
            this.file.alert
                .addClass("alert-danger")
                .removeClass("d-none")
                .html("Идёт процесс добавления ...");

            disableElement(this.file.form.find('input[type="submit"]'));

            return true;
        },

        excelShowForm: function(t){

            var popupContent = '<form onsubmit="return saleBasket.excelShowFormSubmit(this)" action="' + this.excel.action + '" method="post" enctype="multipart/form-data">';
            popupContent += '<div class="alert py2 mb2 d-none"></div>';
            popupContent += '<div class="form-group pb2">';
            popupContent += '<lable class="pb1">' + $(t).data("btn-desc") + ':</lable>';
            popupContent += '<a href="/basket/sample.csv" class="pb1 d-block">' + $(t).data("input") + ':</a>';
            popupContent += '<input type="file" name="file">';
            popupContent += '</div>';
            popupContent += '<input type="submit" class="btn btn-danger" value="' + $(t).data("btn-yes") + '" />';
            popupContent += '</form>';

            this.obPopupWin = BX.PopupWindowManager.create('excel_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                content: popupContent,
                closeIcon: true,
                contentColor: 'white',
                className: "excelPopup",
            });

            this.obPopupWin.setTitleBar($(t).data("title"));
            //this.obPopupWin.setContent(popupContent);
            this.obPopupWin.show();

            return false;

        },

        excelShowFormSubmit: function(t){
            this.excel.form = $(t);

            this.excel.alert = this.excel.form.find(".alert");
            this.excel.alert
                .addClass("alert-danger")
                .removeClass("d-none")
                .html("Идёт процесс добавления ...");

            disableElement(this.excel.form.find('input[type="submit"]'));

            return true;
        },

        saveCsv: function(t){
            var url = $(t).data("url"),
                items = $(this.basket.jqId).find(this.item.class);

            if(!!items && items.length > 0){
                url += "?products="
                $.each(items, function () {
                    url += $(this).find(":checkbox").data("productid");
                    url += ":";
                    url += $(this).find(":text").val();
                    url += ",";

                });
            }

            location.href = url;
            return false;
        },

        shareBasket: function(t){

            this.obPopupWin = BX.PopupWindowManager.create('article_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                //content: popupContent,
                closeIcon: true,
                contentColor: 'white',
                className: "sharePopup",
            });

            this.obPopupWin.setTitleBar($(t).data("title"));


            var content = '<table>',
                items = $(t).data("products"),
                value = "http://" + $(t).data("host");


            value += "/basket/share/";
            value += "?products=" + items;

            content += '<tr>';
            content += '<td>';
            content += '<div class="element-share">';
            content += '<input type="text" readonly value="' + value + '" />';
            content += '</div>';
            content += '</td>';
            content += '<td>';
            content += '<button onclick="saleBasket.shareBasketCopy(this)" class="cursor-pointer">';
            content += '<svg class="octicon octicon-clippy" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M5.75 1a.75.75 0 00-.75.75v3c0 .414.336.75.75.75h4.5a.75.75 0 00.75-.75v-3a.75.75 0 00-.75-.75h-4.5zm.75 3V2.5h3V4h-3zm-2.874-.467a.75.75 0 00-.752-1.298A1.75 1.75 0 002 3.75v9.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 13.25v-9.5a1.75 1.75 0 00-.874-1.515.75.75 0 10-.752 1.298.25.25 0 01.126.217v9.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-9.5a.25.25 0 01.126-.217z"></path></svg>';
            content += '</button>';
            content += '</td>';
            content += '</tr>';
            content += '</table>';
            this.obPopupWin.setContent(content);
            this.obPopupWin.show();



            return false;
        },

        shareBasketCopy: function(t){

            var input = $(t).parent().parent().find("input");

            /* Select the text field */
            input.select();

            /* Copy the text inside the text field */
            document.execCommand("copy");

        },

        /**
         * Article
         */
        articleShowForm: function(t){

            var popupContent = '<form onsubmit="return saleBasket.articleSubmit(this)" style="max-width: 400px;">',
                i;

            popupContent += '<div class="alert py2 mb2 d-none"></div>';
            popupContent += '<div class="form-group">';
            popupContent += '<lable class="pb1">' + $(t).data("input") + ':</lable>';
            for(i=0;i<3;i++) {
                popupContent += '<input type="text" class="form-control my3" name="article[]">';
            }
            popupContent += '</div>';
            popupContent += '<input type="button" onclick="return saleBasket.articleMoreInputs(this)" class="btn btn-danger mr2" value="' + $(t).data("btn-more") + '" />';
            popupContent += '<input type="submit" class="btn btn-danger" value="' + $(t).data("btn-yes") + '" />';
            popupContent += '</form>';

            this.obPopupWin = BX.PopupWindowManager.create('article_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                content: popupContent,
                closeIcon: true,
                contentColor: 'white',
                className: "articlePopup",
            });

            this.obPopupWin.setTitleBar($(t).data("title"));
            //this.obPopupWin.setContent(popupContent);
            this.obPopupWin.show();

            return false;
        },

        articleMoreInputs: function(t){
            var form = $(t).closest("form"),
                form_group = form.find(".form-group"),
                input = '<input type="text" class="form-control my3" name="article[]">',
                inputCount = form_group.find(".form-control").length,
                maxInputCount = 6;

            if(inputCount < maxInputCount){
                form_group.append(input);
            }else{
                $(t).remove();
            }

        },

        articleSubmit: function (t) {

            this.article.form = $(t);
            this.article.data = this.article.form.serializeArray();
            this.article.alert = this.article.form.find(".alert");
            this.article.alert.removeClass("alert-success")
                .removeClass("alert-danger")
                .removeClass("d-none")
                .html("Идёт процесс добавления ...");

            disableElement(this.article.form.find('input[type="submit"]'));


            $.getJSON(this.article.action, this.article.data, function (data) {
                var message = " товар ",
                    count = parseInt(data.COUNT);

                if(count > 0) {
                    if(count > 1 && count < 5){
                        message = " товара ";
                    }
                    else if(count > 5 && count < 20){
                        message += " товаров ";
                    }
                    saleBasket.article.alert.addClass("alert-success").html("В корзину добавлено " + data.COUNT + message);
                }else{
                    saleBasket.article.alert.addClass("alert-danger").html("Товары заданным артиклам не найдены!");
                }

                enableElement(saleBasket.article.form.find('input[type="submit"]'));

                $.get(saleBasket.basket.url, [], function (data) {
                    $(saleBasket.basket.id).html($(data).find(saleBasket.basket.id).html());
                });

            });

            return false;
        },

        /**
         * Selector functions
         */
        selector: function(t){
            if($(t).is(':checked')){
                this.selectAllItems();
            }else{
                this.unSelectAllItems();
            }
        },

        selectAllItems: function(){

            var basket = $(this.basket.jqId),
                products = basket.find(".product-selector");

            if(products.length > 0){
                products.each(function () {
                    $(this).attr('checked','checked');
                });

            }


        },

        unSelectAllItems: function(){

            var basket = $(this.basket.jqId),
                products = basket.find(".product-selector");

            if(products.length > 0){
                products.each(function () {
                    $(this).attr('checked',null);
                });

            }

        },

        deleteSelectedConfirm: function(t){

            this.obPopupWin = BX.PopupWindowManager.create('multiplebasket_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                //content: $(t).data("desc"),
                closeIcon: true,
                contentColor: 'white',
                className: "delectedPopup"
            });

            this.obPopupWin.setTitleBar($(t).data("title"));
            this.obPopupWin.setContent($(t).data("desc"));
            this.obPopupWin.setButtons([
                new BX.PopupWindowButton({
                    'text': $(t).data("delete"),
                    'events': {
                        'click': function ()
                        {
                            saleBasket.deleteSelected();
                        }
                    }
                }),
                new BX.PopupWindowButton({
                    'text': $(t).data("cancel"),
                    'events': {
                        'click': function ()
                        {
                            saleBasket.obPopupWin.close();
                        }
                    }
                })
            ]);
            this.obPopupWin.show();
        },

        deleteSelected: function(){

            var basket = $(this.basket.jqId),
                products = basket.find(".product-selector"),
                i = 0,
                time = 1000;

            if(products.length > 0){
                saleBasket.obPopupWin.setContent("Идёт процесс удаления. Дождитесь окончания.");


                $("#" + saleBasket.obPopupWin.getId()).find(".popup-window-buttons").remove();

                products.each(function () {
                    if($(this).is(':checked')){
                        i++;
                        var id = this.value,
                            item = $(this).closest(".basket-item");
                        setTimeout( function(){
                            item.remove();
                            // send data
                            BX.ajax.runAction('studio7spb:marketplace.api.tools.deleteBasket', {
                                data: {
                                    id: id
                                }
                            });
                        }, time);
                        time += 1000;
                    }
                });

                setTimeout( function(){
                    s7market.updateBasket();
                    // anitos-basket-title


                    saleBasket.obPopupWin.close();
                }, time);

            }

            if(i >= products.length)
            {
                this.basketEmpty();
            }

        },

        favoriteSelected: function(){
            // toggleFavorite: function (t, id)
            var basket = $(this.basket.jqId),
                products = basket.find(".product-selector");

            if(products.length > 0){
                products.each(function () {
                    if($(this).is(':checked')){
                        $(this).parent().find(".item-favorite-icon").click();
                    }
                });
                // item-favorite-btn item-favorite-icon
                // item-favorite-btn item-favorite-fill
            }
        },

        toggleFavorite: function (t, id) {

            // lighting
            if($(t).hasClass("item-favorite-icon")){
                $(t).removeClass("item-favorite-icon");
                $(t).addClass("item-favorite-fill");
            }else{
                $(t).removeClass("item-favorite-fill");
                $(t).addClass("item-favorite-icon");
            }

            this.item.current = t;
            //this.remove();

            // sent data
            var request = BX.ajax.runAction('studio7spb:marketplace.api.tools.addToWish', {
                data: {
                    id: id,
                }
            });
            request.then(function(response){
                if(response.status == 'success') {
                    //s7market.updateBasket();
                }
            });


        },

        /**
         * Clear basket and show empty
         */
        basketEmpty: function(){
            var empty = this.basketEmptyHtml($(this.basket.id).data("empty"));
            $(this.basket.jqId).html(empty);
            //$("#market2-basket-actions").remove();
        },

        basketEmptyHtml: function(html){
            var empty = BX.create('div', {
                props: { className: "alert alert-info text-center mb5" },
                children: [
                    BX.create('img', {
                        props: {
                            className: "mb3",
                            src: $(this.basket.jqId).data("emptyimg")
                        }
                    }),
                    BX.create('div', {
                        html: html
                    })
                ]
            });
            return empty;
        },

        /**
         * developer tools
         */

        d: function (value) {
            console.log(value);
        }
    };

})();

/**
 * multiplebasket Popup
 */

multiplebasketPopup = {

    obPopupWin: null,

    initPopupWindow: function()
    {
        if (this.obPopupWin)
            return;

        this.obPopupWin = BX.PopupWindowManager.create('multiplebasket_popup', null, {
            autoHide: true,
            offsetLeft: 0,
            offsetTop: 0,
            overlay : true,
            closeByEsc: true,
            titleBar: true,
            closeIcon: true,
            contentColor: 'white',
            className: "multiplebasketPopup",
        });
    },

    open: function (t) {

        this.initPopupWindow();

        var popupContent = '<form onsubmit="return multiplebasketPopup.submit(this)">';
        popupContent += '<div class="alert d-none"></div>';
        popupContent += '<div class="form-group">';
        popupContent += '<lable class="pb3">Введите название корзины</lable>';
        popupContent += '<input type="text" class="form-control my3" name="name">';
        //popupContent += '<small id="emailHelp" class="form-text text-muted">Имя корзины поможет легко найти её в списке</small>';
        popupContent += '</div>';
        popupContent += '<input type="submit" class="btn btn-danger" value="Сохранить" />';
        popupContent += '</form>';

        this.obPopupWin.setTitleBar("Сохранение корзины");
        this.obPopupWin.setContent(popupContent);
        this.obPopupWin.show();

        return false;
    },

    submit: function (t) {
        var form = $(t),
            data = form.serializeArray(),
            action = "/system/7studio/sale/multiplebasket/procedures/add.php";

        form.find(".alert").addClass("d-none");
        $.getJSON(action, data, function (result) {

            if(result.type == "success"){
                //location.href = "/basket/";
                $("#basket").html(null);
                $("#basket-empty").removeClass("d-none");
                multiplebasketPopup.obPopupWin.setTitleBar(result.title);
                multiplebasketPopup.obPopupWin.setContent(saleBasket.basketEmptyHtml(result.text));

            }else{
                form.find(".alert")
                    .removeClass("d-none")
                    .removeClass("alert-danger")
                    .removeClass("alert-success")
                    .addClass("alert-" + result.type)
                    .html(result.text);
            }

        });

        return false;
    }
};
