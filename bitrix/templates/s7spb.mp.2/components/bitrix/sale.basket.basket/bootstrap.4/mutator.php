<?
use Bitrix\Main\Loader,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Web\Json;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loader::includeModule("iblock");

/**
 *
 * This file modifies result for every request (including AJAX).
 * Use it to edit output result for "{{ mustache }}" templates.
 *
 * @var array $result
 */

global $APPLICATION;

if($_POST["mode"] == "json"){
    $APPLICATION->RestartBuffer();

    if($_POST["activity"] == "delete_coupon" && $_POST["coupon"]){
        DiscountCouponsManager::delete(htmlspecialcharsbx($_POST["coupon"]));
    }
    else{
        echo Json::encode(array(
            "ITEMS" => $this->basketItems,
            "RESULT" => $result
        ));
    }

    die;
}

$result["MARKET_LIST"] = array();
$result["PRODUCT_PRICES"] = [];


/**
 * Favorite feature
 */
/// Add favorite feature
\CBitrixComponent::includeComponentClass("studio7sbp:favorite");
$favorite = new \studio7sbpFavorite();
global $USER;
$favorite->onPrepareComponentParams(array("USER_ID" => $USER->GetID()));
$result["USER_FAVORITE"] = $favorite->getUserFavorite();

/**
 * Basket items
 */
foreach ($this->basketItems as $arItem)
{

    # collect id
    $result["PRODUCT_IDs"][] = $arItem["PRODUCT_ID"] . ":" . $arItem["QUANTITY"];

    $element = CIBlockElement::GetList(array(), array("ID" => $arItem["PRODUCT_ID"]), false, false, array(
        "ID",
        "IBLOCK_ID",
        "PROPERTY_H_COMPANY.ID",
        "PROPERTY_H_COMPANY.NAME",
        "PROPERTY_H_COMPANY.PREVIEW_TEXT"
    ));
    if($element = $element->GetNext()){

        if($element["PROPERTY_H_COMPANY_ID"] > 0){
            if(empty($result["MARKET_LIST"][$element["PROPERTY_H_COMPANY_ID"]])){
                $result["MARKET_LIST"][$element["PROPERTY_H_COMPANY_ID"]] = array(
                    "NAME" => $element["PROPERTY_H_COMPANY_NAME"],
                    "ITEMS" => array()
                );
            }

            $result["MARKET_LIST"][$element["PROPERTY_H_COMPANY_ID"]]["ITEMS"][] = $element["ID"];

        }

    }
}

// CURRENCIES FORMAT
$result["CURRENCIES_FORMAT"] = array();
foreach ($result["CURRENCIES"] as $currency){
    $result["CURRENCIES_FORMAT"][$currency["CURRENCY"]] = $currency["FORMAT"]["FORMAT_STRING"];
}

/**
 * @param $num
 * @param $vars
 * @param string $before
 * @param string $after
 * @return string|void
 */
function declension( $num, $vars, $before = '', $after = '' )
{
    if( $num == 0 ) // если число равно нулю
        return; // ничего не возвращаем
    $normal_num = $num; // сохраняем число в исходном виде
    $num = $num % 10; // определяем цифру, стоящую после десятка
    if( $num == 1 ) // если это единица
    {
        $num = $normal_num . ' ' . $vars[0];
    }else if( $num > 1 && $num < 5 ) // если это 2, 3, 4
    {
        $num = $normal_num . ' ' . $vars[1];
    }else
    {
        $num = $normal_num . ' ' . $vars[2];
    }
    return $before . $num . $after; // возвращаем строку
}

