<?
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

$arParams["ALFAVITE"] = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q");

$arParams["MATRIX"] = array(
    "START" => array(
        "X" => "B",
        "Y" => 3,
        "ADRESS" => "B3"
    ),
    "CELL" => array(
        "X" => "B",
        "Y" => 3,
        "ADRESS" => "B3"
    )
);


$objPHPExcel = new PHPExcel();

include "style.php";
include "basket.total.php";
include "basket.head.php";

foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arItem){
    include "basket.item.php";
}

include "basket.total.php";

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');