<?php
/**
 * @var PHPExcel $objPHPExcel
 * @var array $arParams
 * @var array $arItem
 */
$i = 0;
use Bitrix\Main\Localization\Loc;

# Stylesheet
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setIndent(2);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getRowDimension($arParams["MATRIX"]["CELL"]["Y"])->setRowHeight(40);

# Data filling

if(!empty($arItem["PREVIEW_PICTURE_SRC"])){
    $objDrawing = new PHPExcel_Worksheet_Drawing();
    $objDrawing->setName($arItem["NAME"]);
    $objDrawing->setDescription($arItem["NAME"]);
    $objDrawing->setPath($_SERVER["DOCUMENT_ROOT"] . $arItem["PREVIEW_PICTURE_SRC"]);
    $objDrawing->setCoordinates($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"]);
    //setOffsetX works properly
    $objDrawing->setOffsetX(5);
    $objDrawing->setOffsetY(5);
    //set width, height
    $objDrawing->setWidth(100);
    $objDrawing->setHeight(40);
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
}

$i++;

//$arItem["NAME"] = preg_replace("#[^0-9а-яА-ЯA-Za-z;:_.,? -]+#u", ' ', $arItem["NAME"]);
$arItem["NAME"] = str_replace(array("\"", "'", "&quot;", "*"), "", $arItem["NAME"]);

$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arItem["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->getStyle($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arItem["PRICE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
$i++;

$objPHPExcel->getActiveSheet()->getStyle($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arItem["QUANTITY"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
$i++;

$objPHPExcel->getActiveSheet()->getStyle($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], number_format($arItem["SUM_VALUE"], 0, '.', ' '), PHPExcel_Cell_DataType::TYPE_NUMERIC);
$i++;


# send matrix position to new row for busket items
$arParams["MATRIX"]["CELL"]["Y"]++;
$arParams["MATRIX"]["CELL"]["X"] = $arParams["MATRIX"]["START"]["X"];
$arParams["MATRIX"]["CELL"]["ADRESS"] = $arParams["MATRIX"]["CELL"]["X"] . $arParams["MATRIX"]["CELL"]["Y"] ;
