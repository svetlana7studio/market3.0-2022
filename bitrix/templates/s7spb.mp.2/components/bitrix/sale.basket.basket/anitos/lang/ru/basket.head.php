<?
$MESS = [
    "BASKET_HEAD_SORT_TITLE" => "Сортировать",
    "BASKET_HEAD_SORT_DATE" => "по дате добавления",
    "BASKET_HEAD_SORT_NEW" => "по новизне",
    "BASKET_HEAD_SORT_PRICE" => "по цене",
    "BASKET_HEAD_SORT_SUM" => "по сумме",
    "BASKET_HEAD_SORT_NAME" => "по алфавиту",
    "BASKET_HEAD_SORT_ARTICLE" => "по артикулу",

    "BASKET_HEAD_CHECKED_TITLE" => "С выделенными:",
    "BASKET_HEAD_CHECKED_DELETE" => "Удалить",
    "BASKET_HEAD_CHECKED_FAVORITE_WONT" => "В избранное",
    "BASKET_HEAD_CHECKED_FAVORITE_IN" => "В избранном",

    "BASKET_HEAD_ACTIONS_ADD_FILE" => "Добавить из файла",
    "BASKET_HEAD_ACTIONS_ADD_ARTICLE" => "Добавить по артикулу",
    "BASKET_HEAD_ACTIONS_ADD_SAVE" => "Сохранить список товаров",
    "BASKET_HEAD_ACTIONS_ADD_SHARE" => "Поделиться корзиной",

    "BASKET_ARTICLE_INPUT" => "Введите артикулы товара",
    "BASKET_FILE_INPUT" => "Пример файла",
    "BASKET_ARTICLE_BTN_YES" => "Добавить товары в корзину",
    "BASKET_ARTICLE_BTN_MORE" => "Ещё артикул",
    "BASKET_ARTICLE_BTN_NO" => "Отмена",
    //"BASKET_ARTICLE_DESC" => "Поддерживаются файлы формата CSV и не превышающие 5МБ",
    "BASKET_ARTICLE_DESC" => "Введите в поле товары каждый строки в формате CSV - Артикул;Количество",
    "BASKET_HEAD_SHARE_TITLE" => "Ссылка на корзину",
];