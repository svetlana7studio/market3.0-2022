<?
/**
 * @var array $arParams
 * @var array $arResult
 */
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

?>
<div class="s7spb-row py3 line-s7">
    <div class="s7spb-col text-dark text-18 py3 text-semibold line-sum">
        Общая сумма покупки:
        <span class="text-24"
              rel="<?=$arResult["allSum"]?>"
              id="allSum"><?=$arResult["allSum_FORMATED"]?></span>
    </div>
    <?if($arParams["MINIMUM_ORDER_PRICE"] > 0):?>
        <div class="s7spb-col text-info text-center py3 bg-white line-ss7"
             id="MINIMUM_ORDER_PRICE"
             data-price="<?=$arParams["MINIMUM_ORDER_PRICE"]?>">
            <?
            echo Loc::getMessage("BASKET_TOTAL_MINIMUM_ORDER_PRICE", [
                "PRICE" => $arParams["MINIMUM_ORDER_PRICE"],
                "CURRENCY" => $arParams["MINIMUM_ORDER_CURRENCY"]
            ]);
            ?>
            <div class="text-bold text-nowrap">
                <i class="icon icon-reload text-vertical-middle"></i>
                <a href="<?=$_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : "/catalog/"?>" class="text-info"><?=Loc::getMessage("BASKET_GOTO_CATALOG")?></a>
            </div>
        </div>
    <?endif;?>

    <div class="BASKET_GOTO_ORDER s7spb-col s7spb-col-auto px6 py3<?if($arResult["allSum"] < $arParams["MINIMUM_ORDER_PRICE"]):?> d-none<?endif;?>">
        <a href="<?=$arParams["PATH_TO_ORDER"]?>"
           class="btn btn-info px5 text-bold text-uppercase text-roboto text-16 text-white border-radius-0 mb1 cursor-pointer">
            <?=Loc::getMessage("BASKET_GOTO_ORDER")?>
        </a>
    </div>

</div>

<div class="text-center text-18 text-dark py4 mb1 mx2 bg-white line-sp text-semibold">
    <div class="py4"><i class="icon icon-car pr3"></i> БЕСПЛАТНАЯ ДОСТАВКА</div>
</div>