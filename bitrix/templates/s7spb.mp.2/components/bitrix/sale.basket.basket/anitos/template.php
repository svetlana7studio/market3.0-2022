<?
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);


$arParams["ACTIONS"] = [
    "ITEMS" => [
        [
            "TEXT" => Loc::getMessage("BASKET_HEAD_ACTIONS_ADD_ARTICLE"),
            "CLICK" => 'return saleBasket.fileShowForm(this)',
            "DATA" => [
                "title" => Loc::getMessage("BASKET_HEAD_ACTIONS_ADD_ARTICLE"),
                "btn-yes" => Loc::getMessage("BASKET_ARTICLE_BTN_YES"),
                "btn-more" => Loc::getMessage("BASKET_ARTICLE_BTN_MORE"),
                "btn-desc" => Loc::getMessage("BASKET_ARTICLE_DESC"),
                "btn-no" => Loc::getMessage("BASKET_ARTICLE_BTN_NO"),
                "input" => Loc::getMessage("BASKET_ARTICLE_INPUT")
            ]
        ],
        [
            "TEXT" => Loc::getMessage("BASKET_HEAD_ACTIONS_ADD_FILE"),
            "CLICK" => 'return saleBasket.excelShowForm(this)',
            "DATA" => [
                "title" => Loc::getMessage("BASKET_HEAD_ACTIONS_ADD_FILE"),
                "btn-yes" => Loc::getMessage("BASKET_ARTICLE_BTN_YES"),
                "btn-no" => Loc::getMessage("BASKET_ARTICLE_BTN_NO"),
                "btn-desc" => Loc::getMessage("BASKET_ARTICLE_DESC"),
                "input" => Loc::getMessage("BASKET_FILE_INPUT")
            ]
        ]
    ]
];

$countItems = count($arResult["ITEMS"]["AnDelCanBuy"]);

if($countItems > 0):

    $this->SetViewTarget('basketActionsPanel');
    ?>    <div class="pt4 px2 text-right" id="anitos-basket-actions">

        <a href="<?=$APPLICATION->GetCurPageParam("mode=ex", array("mode"))?>"
           class="pl3 text-black-hole text-decoration-none d-inline-block"><i class="icon-excel"></i> Выгрузить корзину в Excel</a>
        <a href="#"
           onclick="return multiplebasketPopup.open(this)"
           class="pl3 text-black-hole text-decoration-none d-inline-block"><i class="icon-save"></i> Сохранить корзину</a>
    </div>
    <?
    $this->EndViewTarget();

    foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arItem){
        $arResult["ITEMS"]["AnDelCanBuy"][$key]["DATE_INSERT"] = $arItem["DATE_INSERT"]->getTimestamp();
    }

?>
    <div id="anitos-basket"
         class="py2 mb7"
         data-currency="<?=$arResult["CURRENCIES_FORMAT"][$arResult["CURRENCY"]]?>"
         data-emptyimg="<?=$this->GetFolder()?>/images/empty_cart.svg"
         data-empty="<?=Loc::getMessage("BASKET_EMPTY")?>"
         data-warning="<?=Loc::getMessage("BASKET_WARNING")?>">

        <div class="bg-white pb4 px2">


        <div class="text-30 text-black" id="anitos-basket-title">
            <?
            echo Loc::getMessage("BASKET_CNT", ["NUM" => $countItems]);
            $arEnds = [
                Loc::getMessage("BASKET_CNT_END1"),
                Loc::getMessage("BASKET_CNT_END2"),
                Loc::getMessage("BASKET_CNT_END3"),
                Loc::getMessage("BASKET_CNT_END4")
            ];

            if ($countItems>1 && substr($countItems, strlen($countItems)-2, 1)=="1")
            {
                echo $arEnds[0];
            }
            else
            {
                $c = IntVal(substr($countItems, strlen($countItems)-1, 1));
                if ($c==0 || ($c>=5 && $c<=9))
                    echo $arEnds[1];
                elseif ($c==1)
                    echo $arEnds[2];
                else
                    echo $arEnds[3];
            }
            ?>
        </div>

        <?if($countItems > 0):?>


            <?
            if(count($arResult["GRID"]["ROWS"]) > 0):
                ?>
                <?
                // <editor-fold defaultstate="collapsed" desc=" # Basket head">
                include "basket.head.php";
                // </editor-fold>
                ?>

                <table class="table">
                    <thead class="text-center">
                    <tr>
                        <td class="border-bottom border-gray">
                            <input type="checkbox"
                                   onclick="saleBasket.selector(this)"
                                   class="cursor-pointer" />
                        </td>
                        <td class="border-bottom border-gray">
                            Наименование товара
                        </td>
                        <td class="border-bottom border-left border-gray position-relative title-price title-price8">
                            Цена мелкого опта
                            <?if($arResult["allSum"] <= 10000):?>
                                <div class="user-price-bajic user-price-bajic-success"> Ваша цена</div>
                            <?endif;?>
                        </td>
                        <td class="border-bottom border-left border-gray position-relative title-price title-price6">
                            Цена среднего опта
                            <?if($arResult["allSum"] > 10000 && $arResult["allSum"] < 50000):?>
                                <div class="user-price-bajic"> Ваша цена</div>
                            <?endif;?>
                        </td>
                        <td class="border-bottom border-left border-gray position-relative title-price title-price4">
                            Цена крупного опта
                            <?if($arResult["allSum"] > 50000):?>
                                <div class="user-price-bajic user-price-bajic-danger"> Ваша цена</div>
                            <?endif;?>
                        </td>
                        <td class="border-bottom border-left border-gray">
                            Кол-во
                        </td>
                        <td class="border-bottom border-left border-gray">
                            Итого
                        </td>
                        <td class="border-bottom border-gray"> </td>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    // <editor-fold defaultstate="collapsed" desc=" # Basket items list">
                    foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arItem){
                        include "basket.item.php";
                    }
                    // </editor-fold>
                    ?>
                    </tbody>
                </table>
            <?endif;?>

        </div>

            <?include "basket.total.php";?>

        <?endif;?>
    </div>


<?else:?>
    <div class="mb3 bacg">
        <?foreach ($arParams["ACTIONS"]["ITEMS"] as $arItem):?>
            <span class="position-relative">

                <a href="#"
                   <?if(isset($arItem["CLICK"])):?>
                       onclick='<?=$arItem["CLICK"]?>'
                   <?endif;?>
                    <?if(is_array($arItem["DATA"])):?>
                        <?foreach ($arItem["DATA"] as $key=>$value):?>
                            data-<?=$key?>="<?=$value?>"
                        <?endforeach;?>
                    <?endif;?>
                   class="p3 text-black-hole text-decoration-none d-inline-block"><?=$arItem["TEXT"]?></a>

            </span>
        <?endforeach;?>
    </div>
    <div class="alert alert-info text-center my5 mt-none">
        <img class="mb3"
             src="<?=$this->GetFolder()?>/images/empty_cart.svg">
        <div><?=Loc::getMessage("BASKET_EMPTY")?></div>
    </div>
<?endif;?>