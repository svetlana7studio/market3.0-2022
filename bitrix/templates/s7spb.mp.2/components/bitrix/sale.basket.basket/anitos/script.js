;(function() {
    'use strict';

    window.saleBasket = {

        basket: {
            id: "#anitos-basket",
            counterClass: ".quantity-counter",
            url: "/basket/",
            totalPrice: 0
        },
        item: {
            class: ".basket-item",
            current: null
        },
        format: {
            ceil: 0,
            duration: 500
        },
        share: {
            id: "element-share",
            active: "d-none"
        },
        article: {
            alert: null,
            action: "/basket/article.fill/",
            form: null,
            data: [],
        },
        excel: {
            action: "/basket/excel/",
            form: null,
            alert: null
        },
        file: {
            action: "/basket/file.fill/",
            form: null,
            alert: null
        },
        obPopupWin: null,

        fileShowForm: function(t){

            var popupContent = '<form onsubmit="return saleBasket.fileShowFormSubmit(this)" action="' + this.file.action + '" method="post" enctype="multipart/form-data">';
            popupContent += '<div class="alert py2 mb2 d-none"></div>';
            popupContent += '<div class="form-group pb2">';
            popupContent += '<lable class="pb1">' + $(t).data("btn-desc") + ':</lable>';
            popupContent += '<a href="/basket/sample.csv/" class="pb1 d-block">' + $(t).data("input") + ':</a>';
            popupContent += "<textarea name='data'></textarea>";
            popupContent += '</div>';
            popupContent += '<input type="submit" class="btn btn-danger" value="' + $(t).data("btn-yes") + '" />';
            popupContent += '</form>';


            this.obPopupWin = BX.PopupWindowManager.create('file_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                content: popupContent,
                closeIcon: true,
                contentColor: 'white',
                className: "filePopup"
            });

            this.obPopupWin.setTitleBar($(t).data("title"));
            //this.obPopupWin.setContent(popupContent);
            this.obPopupWin.show();

            return false;
        },

        fileShowFormSubmit: function(t){
            this.file.form = $(t);

            this.file.alert = this.file.form.find(".alert");
            this.file.alert
                .addClass("alert-danger")
                .removeClass("d-none")
                .html("Идёт процесс добавления ...");

            disableElement(this.file.form.find('input[type="submit"]'));

            return true;
        },

        excelShowForm: function(t){

            var popupContent = '<form onsubmit="return saleBasket.excelShowFormSubmit(this)" action="' + this.excel.action + '" method="post" enctype="multipart/form-data">';
            popupContent += '<div class="alert py2 mb2 d-none"></div>';
            popupContent += '<div class="form-group pb2">';
            popupContent += '<lable class="pb1">' + $(t).data("btn-desc") + ':</lable>';
            popupContent += '<a href="/system/cart/sample.csv" class="pb1 d-block">' + $(t).data("input") + ':</a>';
            popupContent += '<input type="file" name="file">';
            popupContent += '</div>';
            popupContent += '<input type="submit" class="btn btn-danger" value="' + $(t).data("btn-yes") + '" />';
            popupContent += '</form>';

            this.obPopupWin = BX.PopupWindowManager.create('excel_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                content: popupContent,
                closeIcon: true,
                contentColor: 'white',
                className: "excelPopup",
            });

            this.obPopupWin.setTitleBar($(t).data("title"));
            //this.obPopupWin.setContent(popupContent);
            this.obPopupWin.show();

            return false;

        },

        excelShowFormSubmit: function(t){
            this.excel.form = $(t);

            this.excel.alert = this.excel.form.find(".alert");
            this.excel.alert
                .addClass("alert-danger")
                .removeClass("d-none")
                .html("Идёт процесс добавления ...");

            disableElement(this.excel.form.find('input[type="submit"]'));

            return true;
        },

        saveCsv: function(t){
            var url = $(t).data("url"),
                items = $(this.basket.id).find(this.item.class);

            if(!!items && items.length > 0){
                url += "?products="
                $.each(items, function () {
                    url += $(this).find(":checkbox").data("productid");
                    url += ":";
                    url += $(this).find(":text").val();
                    url += ",";

                });
            }

            location.href = url;
            return false;
        },

            shareBasket: function(t){

            this.obPopupWin = BX.PopupWindowManager.create('article_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                //content: popupContent,
                closeIcon: true,
                contentColor: 'white',
                className: "sharePopup",
            });

            this.obPopupWin.setTitleBar($(t).data("title"));


            var content = '<table>',
                items = $(t).data("products"),
                value = "//" + $(t).data("host");


            value += "/basket/share/";
            value += "?products=" + items;

            content += '<tr>';
            content += '<td>';
            content += '<div class="element-share">';
            content += '<input type="text" readonly value="' + value + '" />';
            content += '</div>';
            content += '</td>';
            content += '<td>';
            content += '<button onclick="saleBasket.shareBasketCopy(this)" class="cursor-pointer">';
            content += '<svg class="octicon octicon-clippy" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M5.75 1a.75.75 0 00-.75.75v3c0 .414.336.75.75.75h4.5a.75.75 0 00.75-.75v-3a.75.75 0 00-.75-.75h-4.5zm.75 3V2.5h3V4h-3zm-2.874-.467a.75.75 0 00-.752-1.298A1.75 1.75 0 002 3.75v9.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 13.25v-9.5a1.75 1.75 0 00-.874-1.515.75.75 0 10-.752 1.298.25.25 0 01.126.217v9.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-9.5a.25.25 0 01.126-.217z"></path></svg>';
            content += '</button>';
            content += '</td>';
            content += '</tr>';
            content += '</table>';
            this.obPopupWin.setContent(content);
            this.obPopupWin.show();



            return false;
        },

        shareBasketCopy: function(t){

            var input = $(t).parent().parent().find("input");

            /* Select the text field */
            input.select();

            /* Copy the text inside the text field */
            document.execCommand("copy");

        },

        /**
         * Article
         */
        articleShowForm: function(t){

            var popupContent = '<form onsubmit="return saleBasket.articleSubmit(this)" style="max-width: 400px;">',
                i;

            popupContent += '<div class="alert py2 mb2 d-none"></div>';
            popupContent += '<div class="form-group">';
            popupContent += '<lable class="pb1">' + $(t).data("input") + ':</lable>';
            for(i=0;i<3;i++) {
                popupContent += '<input type="text" class="form-control my3" name="article[]">';
            }
            popupContent += '</div>';
            popupContent += '<input type="button" onclick="return saleBasket.articleMoreInputs(this)" class="btn btn-danger mr2" value="' + $(t).data("btn-more") + '" />';
            popupContent += '<input type="submit" class="btn btn-danger" value="' + $(t).data("btn-yes") + '" />';
            popupContent += '</form>';

            this.obPopupWin = BX.PopupWindowManager.create('article_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                content: popupContent,
                closeIcon: true,
                contentColor: 'white',
                className: "articlePopup",
            });

            this.obPopupWin.setTitleBar($(t).data("title"));
            //this.obPopupWin.setContent(popupContent);
            this.obPopupWin.show();

            return false;
        },

        articleMoreInputs: function(t){
            var form = $(t).closest("form"),
                form_group = form.find(".form-group"),
                input = '<input type="text" class="form-control my3" name="article[]">',
                inputCount = form_group.find(".form-control").length,
                maxInputCount = 6;

            if(inputCount < maxInputCount){
                form_group.append(input);
            }else{
                $(t).remove();
            }

        },

        articleSubmit: function (t) {

            this.article.form = $(t);
            this.article.data = this.article.form.serializeArray();
            this.article.alert = this.article.form.find(".alert");
            this.article.alert.removeClass("alert-success")
                              .removeClass("alert-danger")
                              .removeClass("d-none")
                              .html("Идёт процесс добавления ...");

            disableElement(this.article.form.find('input[type="submit"]'));


            $.getJSON(this.article.action, this.article.data, function (data) {
                var message = " товар ",
                    count = parseInt(data.COUNT);

                if(count > 0) {
                    if(count > 1 && count < 5){
                        message = " товара ";
                    }
                    else if(count > 5 && count < 20){
                        message += " товаров ";
                    }
                    saleBasket.article.alert.addClass("alert-success").html("В корзину добавлено " + data.COUNT + message);
                }else{
                    saleBasket.article.alert.addClass("alert-danger").html("Товары заданным артиклам не найдены!");
                }

                enableElement(saleBasket.article.form.find('input[type="submit"]'));

                $.get(saleBasket.basket.url, [], function (data) {
                    $(saleBasket.basket.id).html($(data).find(saleBasket.basket.id).html());
                });

            });

            return false;
        },

        /**
         * Toggle share panel
         */
        toggleShare: function () {
            if(BX.hasClass(BX(this.share.id), this.share.active)){
                BX.removeClass(BX(this.share.id), this.share.active);
            }else{
                BX.addClass(BX(this.share.id), this.share.active);
            }
            return false;
        },

        /**
         * Counter
         * @param t
         * @param type
         */
        count: function(t, type){

            if(type === true){
                quantityCounter.up(t);
            }
            if(type === false){
                quantityCounter.down(t);
            }


            var counter = $(t).parent(),
                input = counter.find("input"),
                value = parseInt(input.val()),
                price = 0,
                price4 = counter.data("price4"),
                price6 = counter.data("price6"),
                price8 = counter.data("price8"),
                item = $(t).closest(this.item.class);
                //itemSum = item.find(".item-sum");

            ////// В зависимости от общей стоимости корзины решаем какую цену брать
            //price = price.toFixed(saleBasket.format.ceil);

            this.total({
                price: 0,
                currency: counter.data("currency")
            });

            price = price8;

            if(this.basket.totalPrice > 5000){
                price = price8;
            }
            if(this.basket.totalPrice > 10000){
                price = price6;
            }
            if(this.basket.totalPrice > 50000){
                price = price4;
            }

            price = parseFloat(price);
            price = price * value;

            this.updateRow(t, {
                price: price,
                currency: counter.data("currency")
            });

            //this.animateInput(input, value);

            // save data
            BX.ajax.runAction('studio7spb:marketplace.api.tools.counterBasket', {
                data: {
                    id: item.data("product"),
                    quantity: value
                }
            });

        },

        /**
         * Update row price
         * @param t
         * @param data
         */
        updateRow: function(t, data){
            $(t).closest(".basket-item").find(".basket-sum").html(this.priceFormat(data));
        },

        /**
         * Calculate total
         * @param data
         */
        total: function(data){

            var basket = $(this.basket.id),
                priceType = 8,
                price4 = "price4",
                price6 = "price6",
                price8 = "price8",
                price = 0;

            if(basket.find(this.basket.counterClass).length > 0){

                // считаем по самой дорогой цене
                //if(data.price > 5000)
                basket.find(this.basket.counterClass).each(function () {
                    price = parseFloat($(this).data("price8"))  * parseInt($(this).find("input").val());
                    data.price += price;
                });

                // Подсветить
                // title-price title-price8
                $(".user-price-bajic").remove();
                $(".title-price8").append('<div class="user-price-bajic user-price-bajic-success"> Ваша цена</div>');

                // ксли итог позволяет - то обнуляем тотал и считаем по среднему опту
                if(data.price > 10000){
                    priceType = 6;
                    data.price = 0;
                    basket.find(this.basket.counterClass).each(function () {
                        price = parseFloat($(this).data("price6"))  * parseInt($(this).find("input").val());
                        data.price += price;
                    });

                    // Подсветить
                    // title-price title-price8
                    $(".user-price-bajic").remove();
                    $(".title-price6").append('<div class="user-price-bajic"> Ваша цена</div>');

                }
                // ксли итог позволяет - то обнуляем тотал и считаем по крупному опту
                if(data.price > 50000){
                    priceType = 4;
                    data.price = 0;
                    basket.find(this.basket.counterClass).each(function () {
                        price = parseFloat($(this).data("price4"))  * parseInt($(this).find("input").val());
                        data.price += price;
                    });

                    // Подсветить
                    // title-price title-price8
                    $(".user-price-bajic").remove();
                    $(".title-price4").append('<div class="user-price-bajic user-price-bajic-danger"> Ваша цена</div>');

                }

                $(".basket-item").find(".price-string")
                    .removeClass("text-warning")
                    .removeClass("text-success-ultras")
                    .removeClass("text-danger");

                switch (priceType) {
                    case 4:
                        $(".price-string-" + priceType).addClass("text-danger");
                        break;
                    case 6:
                        $(".price-string-" + priceType).addClass("text-warning");
                        break;
                    case 8:
                        $(".price-string-" + priceType).addClass("text-success-ultras");
                        break;
                    default:
                        $(".price-string-" + priceType).addClass("text-success-ultras");
                }

                // Сохраняем тотал
                this.basket.totalPrice = data.price;
                $("#allSum").attr("rel", data.price)
                    .text(this.priceFormat(data));

                // проверка суммы корзины на минимальную цену заказа
                this.minimumOrderPrice(data.price);

            }
            else{
                this.basketEmpty();
            }

        },

        minimumOrderPrice: function(sum){
            var minimumPrice = $("#MINIMUM_ORDER_PRICE");
            if(!!minimumPrice && minimumPrice.length > 0){
                minimumPrice = minimumPrice.data("price");

                if(sum > minimumPrice){
                    $(".BASKET_GOTO_ORDER").removeClass("d-none");
                }else{
                    $(".BASKET_GOTO_ORDER").addClass("d-none");
                }
            }
        },

        priceFormat: function(data){
            var priceFormat = data.price,
                currency = data.currency;
            priceFormat = priceFormat.toFixed(2);
            priceFormat = currency.replace("#", priceFormat);

            return priceFormat;
        },

        toggleFavorite: function (t, id) {

            // lighting
            if($(t).hasClass("item-favorite-icon")){
                $(t).removeClass("item-favorite-icon");
                $(t).addClass("item-favorite-fill");
            }else{
                $(t).removeClass("item-favorite-fill");
                $(t).addClass("item-favorite-icon");
            }

            this.item.current = t;
            //this.remove();

            // sent data
            var request = BX.ajax.runAction('studio7spb:marketplace.api.tools.addToWish', {
                data: {
                    id: id,
                }
            });
            request.then(function(response){
                if(response.status == 'success') {
                    //s7market.updateBasket();
                }
            });


        },

        /**
         * <td class="cursor-pointer border-bottom border-gray"
             onclick="saleBasket.delete(this)"
             data-title="Вы уверены что хотите удалить товар из корзины?"
             data-delete="Да, удалить" data-cancel="Нет, оставить товар в корзине" title="Удалить">
             <i class="icon-delete"></i>
             </td>
         *
         * Delete dialog
         * @param t
         *
         */
        delete: function (t) {
            s7market.updateBasket();
            //if (this.obPopupWin)
            //    return;
            this.obPopupWin = null;

            this.item.current = $(t).closest(this.item.class);

            var popupContent = this.item.current.find(".basket-item-info");
            popupContent = popupContent.clone();
            popupContent.find("a")
                .addClass("d-block")
                .addClass("mb3")
                .addClass("text-center");
            popupContent = popupContent.html();

            this.obPopupWin = BX.PopupWindowManager.create('multiplebasket_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                //content: popupContent,
                closeIcon: true,
                contentColor: 'white',
                className: "deletePopup"
            });

            this.obPopupWin.setTitleBar($(t).data("title"));
            this.obPopupWin.setContent(popupContent);
            this.obPopupWin.setButtons([
                new BX.PopupWindowButton({
                    'text': $(t).data("delete"),
                    'events': {
                        'click': function ()
                        {
                            saleBasket.removeBasketItem();
                            saleBasket.obPopupWin.close();
                        }
                    }
                }),
                new BX.PopupWindowButton({
                    'text': $(t).data("cancel"),
                    'events': {
                        'click': function ()
                        {
                            saleBasket.obPopupWin.close();
                        }
                    }
                })
            ]);
            this.obPopupWin.show();
        },

        /**
         * Confirm remove basket
         * @param basketId
         */
        removeBasketItem: function(){
            this.item.current.remove();
            var count = $(this.basket.id).find(this.item.class).length,

            basketId = $(this.item.current).data("product");

            // send data
            var request = BX.ajax.runAction('studio7spb:marketplace.api.tools.deleteBasket', {
                    data: {
                        id: basketId
                    }
                });

            request.then(function(response){
                s7market.updateBasket();
            });

            // check basket items if it empty - close basket and show empty
            // if not -recalculate
            if(count > 0) {
                this.recalculate();
            }
            else{
                this.basketEmpty();
            }

        },

        /**
         * Clear basket and show empty
         */
        basketEmpty: function(){
            var empty = this.basketEmptyHtml($(this.basket.id).data("empty"));
            $(this.basket.id).html(empty);
            $("#anitos-basket-actions").remove();
        },

        basketEmptyHtml: function(html){
            var empty = BX.create('div', {
                props: { className: "alert alert-info text-center mb5" },
                children: [
                    BX.create('img', {
                        props: {
                            className: "mb3",
                            src: $(this.basket.id).data("emptyimg")
                        }
                    }),
                    BX.create('div', {
                        html: html
                    })
                ]
            });
            return empty;
        },

        /**
         * Recalculate shell
         */
        recalculate: function(){
            this.total({
                price: 0,
                currency: $(this.basket.id).data("currency")
            });
        },

        /**
         * Selector functions
         */
        selector: function(t){
            if($(t).is(':checked')){
                this.selectAllItems();
            }else{
                this.unSelectAllItems();
            }
        },

        selectAllItems: function(){

            var basket = $(this.basket.id),
                products = basket.find(".product-selector");

            if(products.length > 0){
                products.each(function () {
                    $(this).attr('checked','checked');
                });

            }


        },

        unSelectAllItems: function(){

            var basket = $(this.basket.id),
                products = basket.find(".product-selector");

            if(products.length > 0){
                products.each(function () {
                    $(this).attr('checked',null);
                });

            }

        },

        deleteSelectedConfirm: function(t){

            this.obPopupWin = BX.PopupWindowManager.create('multiplebasket_popup', null, {
                autoHide: true,
                offsetLeft: 0,
                offsetTop: 0,
                overlay : true,
                closeByEsc: true,
                titleBar: true,
                //content: $(t).data("desc"),
                closeIcon: true,
                contentColor: 'white',
                className: "delectedPopup"
            });

            this.obPopupWin.setTitleBar($(t).data("title"));
            this.obPopupWin.setContent($(t).data("desc"));
            this.obPopupWin.setButtons([
                new BX.PopupWindowButton({
                    'text': $(t).data("delete"),
                    'events': {
                        'click': function ()
                        {
                            saleBasket.deleteSelected();
                        }
                    }
                }),
                new BX.PopupWindowButton({
                    'text': $(t).data("cancel"),
                    'events': {
                        'click': function ()
                        {
                            saleBasket.obPopupWin.close();
                        }
                    }
                })
            ]);
            this.obPopupWin.show();
        },

        deleteSelected: function(){

            var basket = $(this.basket.id),
                products = basket.find(".product-selector"),
                i = 0,
                time = 1000;

            if(products.length > 0){
                saleBasket.obPopupWin.setContent("Идёт процесс удаления. Дождитесь окончания.");


                $("#" + saleBasket.obPopupWin.getId()).find(".popup-window-buttons").remove();

                products.each(function () {
                    if($(this).is(':checked')){
                        i++;
                        var id = this.value,
                            item = $(this).parent().parent();
                        setTimeout( function(){
                            item.remove();
                            // send data
                            BX.ajax.runAction('studio7spb:marketplace.api.tools.deleteBasket', {
                                data: {
                                    id: id
                                }
                            });
                        }, time);
                        time += 1000;
                    }
                });

                setTimeout( function(){
                    s7market.updateBasket();
                    // anitos-basket-title


                    saleBasket.obPopupWin.close();
                }, time);

            }

            if(i >= products.length)
            {
                this.basketEmpty();
            }

        },

        favoriteSelected: function(){
            // toggleFavorite: function (t, id)
            var basket = $(this.basket.id),
                products = basket.find(".product-selector");

            if(products.length > 0){
                products.each(function () {
                    if($(this).is(':checked')){
                        $(this).parent().parent().find(".item-favorite-icon").click();
                    }
                });
                // item-favorite-btn item-favorite-icon
                // item-favorite-btn item-favorite-fill
            }
        },

        /**
         * developer tools
         */
        onkeyup: function (t) {
            if (event.keyCode === 13) {
                // Cancel the default action, if needed
                event.preventDefault();
                // Trigger the button element with a click
                t.value--;
                $(t).parent().find(".quantity-counter-plus").click();
            }else{
                //quantityCounter.inputIntValidator(t);
            }
        },

        onblur: function(t) {
            //t.value--;
            //$(t).parent().find(".quantity-counter-plus").click();


            saleBasket.count(t, "input");
            // эта проверка была в onkeyup
            quantityCounter.inputIntValidator(t);
        },

        onfocus: function(t) {
            $(t).select();
        },

        /**
         * developer tools
         */
        d: function (value) {
            console.log(value);
        }
    };

})();


/**
 * multiplebasket Popup
 */

multiplebasketPopup = {

    obPopupWin: null,

    initPopupWindow: function()
    {
        if (this.obPopupWin)
            return;

        this.obPopupWin = BX.PopupWindowManager.create('multiplebasket_popup', null, {
            autoHide: true,
            offsetLeft: 0,
            offsetTop: 0,
            overlay : true,
            closeByEsc: true,
            titleBar: true,
            closeIcon: true,
            contentColor: 'white',
            className: "multiplebasketPopup",
        });
    },

    open: function (t) {

        this.initPopupWindow();

        var popupContent = '<form onsubmit="return multiplebasketPopup.submit(this)">';
        popupContent += '<div class="alert d-none"></div>';
        popupContent += '<div class="form-group">';
        popupContent += '<lable class="pb3">Введите название корзины</lable>';
        popupContent += '<input type="text" class="form-control my3" name="name">';
        //popupContent += '<small id="emailHelp" class="form-text text-muted">Имя корзины поможет легко найти её в списке</small>';
        popupContent += '</div>';
        popupContent += '<input type="submit" class="btn btn-danger" value="Сохранить" />';
        popupContent += '</form>';

        this.obPopupWin.setTitleBar("Сохранение корзины");
        this.obPopupWin.setContent(popupContent);
        this.obPopupWin.show();

        return false;
    },

    submit: function (t) {
        var form = $(t),
            data = form.serializeArray(),
            action = "/system/7studio/sale/multiplebasket/procedures/add.php";

        form.find(".alert").addClass("d-none");
        $.getJSON(action, data, function (result) {

            if(result.type == "success"){
                //location.href = "/basket/";
                saleBasket.basketEmpty();
                multiplebasketPopup.obPopupWin.setTitleBar(result.title);
                multiplebasketPopup.obPopupWin.setContent(saleBasket.basketEmptyHtml(result.text));

            }else{
                form.find(".alert")
                    .removeClass("d-none")
                    .removeClass("alert-danger")
                    .removeClass("alert-success")
                    .addClass("alert-" + result.type)
                    .html(result.text);
            }

        });

        return false;
    }
};

$(document).ready(function () {
    s7market.updateBasket();
});