<?
/**
 * @var array $arItem
 * $var int $key
 */

//$arItem["SUM"] = $arItem["PRICE"] * $arItem["QUANTITY"];
use Bitrix\Main\Localization\Loc;

$arItem["NAME"] = str_replace(",", ", ", $arItem["NAME"]);
?>

<tr class="basket-item"
    data-product="<?=$arItem["ID"]?>">

    <td class="border-bottom border-gray">
        <input type="checkbox"
               name="product[]"
               value="<?=$arItem["ID"]?>"
               data-id="<?=$arItem["ID"]?>"
               data-productid="<?=$arItem["PRODUCT_ID"]?>"
               class="cursor-pointer product-selector" />
    </td>

    <td class="border-bottom border-gray">
        <div class="s7spb-row s7spb-row-start s7spb-row-center basket-item-info">

            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="s7spb-col s7spb-col-auto">
                <?
                if(empty($arItem["PREVIEW_PICTURE_SRC"])):
                    if(empty($arResult["PREVIEW_PICTURE"][$arItem["PRODUCT_ID"]])):
                        ?><img src="<?=SITE_TEMPLATE_PATH?>/images/no_image200.png" width="100"><?
                    else:
                        ?><img src="<?=$arResult["PREVIEW_PICTURE"][$arItem["PRODUCT_ID"]]?>" width="100"><?
                    endif;
                else:
                    ?><img src="<?=$arItem["PREVIEW_PICTURE_SRC"]?>" width="100"><?
                endif;
                ?>
            </a>

            <div class="s7spb-col pl1 text-wrap-normal w-max-300">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="text-decoration-none mb1 text-black d-block"><?=$arItem["NAME"]?></a>
                <div class="text-12 text-nowrap">Артикул <?=$arItem["PROPERTY_ARTICLE_VALUE"]?></div>
            </div>

            <div class="s7spb-col s7spb-col-auto">
                <i <?if(in_array($arItem["PRODUCT_ID"], $arResult["USER_FAVORITE"])):?>
                        class="item-favorite-btn item-favorite-fill"
                    <?else:?>
                        class="item-favorite-btn item-favorite-icon"
                    <?endif;?>
                       title="<?=Loc::getMessage("BASKET_ITEM_FAVORITE")?>"
                       onclick="saleBasket.toggleFavorite(this, <?=$arItem["PRODUCT_ID"]?>)"></i>
            </div>

        </div>
    </td>

    <td class="text-18 text-center text-nowrap border-bottom border-gray">
        <?
        if($arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["PRICE"] == $arItem["PRICE"]){
            echo '<div class="text-success-ultras price-string price-string-8">';
            if($arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["PRICE"] > $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["DISCOUNT_PRICE"] && $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["DISCOUNT_PRICE"] > 0)
            {
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["DISCOUNT_PRINT_VALUE"];
                echo ' <s class="text-gray">';
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["PRICE_FORMATED"];
                echo '</s>';
            }else{
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["PRICE_FORMATED"];
            }
            echo '</div>';
        }else{
            echo '<div class="price-string price-string-8">';
            if($arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["PRICE"] > $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["DISCOUNT_PRICE"] && $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["DISCOUNT_PRICE"] > 0)
            {
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["DISCOUNT_PRINT_VALUE"];
                echo ' <s class="text-gray">';
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["PRICE_FORMATED"];
                echo '</s>';
            }else{
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["PRICE_FORMATED"];
            }
            echo '</div>';
        }?>
    </td>

    <td class="text-18 text-center text-nowrap border-bottom border-gray">
        <?
        if($arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["PRICE"] == $arItem["PRICE"]){
            echo '<div class="text-warning price-string price-string-6">';
            if($arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["PRICE"] > $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["DISCOUNT_PRICE"] && $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["DISCOUNT_PRICE"] > 0)
            {
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["DISCOUNT_PRINT_VALUE"];
                echo ' <s class="text-gray">';
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["PRICE_FORMATED"];
                echo '</s>';
            }else{
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["PRICE_FORMATED"];
            }
            echo '</div>';
        }else{
            echo '<div class="price-string price-string-6">';
            if($arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["PRICE"] > $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["DISCOUNT_PRICE"] && $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["DISCOUNT_PRICE"] > 0)
            {
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["DISCOUNT_PRINT_VALUE"];
                echo ' <s class="text-gray">';
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["PRICE_FORMATED"];
                echo '</s>';
            }else{
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["PRICE_FORMATED"];
            }
            echo '</div>';
        }?>
    </td>

    <td class="text-18 text-center text-nowrap border-bottom border-gray">
        <?
        if($arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["PRICE"] == $arItem["PRICE"]){
            echo '<div class="text-danger price-string price-string-4">';
            if($arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["PRICE"] > $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["DISCOUNT_PRICE"] && $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["DISCOUNT_PRICE"] > 0)
            {
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["DISCOUNT_PRINT_VALUE"];
                echo ' <s class="text-gray">';
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["PRICE_FORMATED"];
                echo '</s>';
            }else{
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["PRICE_FORMATED"];
            }
            echo '</div>';
        }else{
            echo '<div class="price-string price-string-4">';
            if($arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["PRICE"] > $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["DISCOUNT_PRICE"] && $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["DISCOUNT_PRICE"] > 0)
            {
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["DISCOUNT_PRINT_VALUE"];
                echo ' <s class="text-gray">';
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["PRICE_FORMATED"];
                echo '</s>';
            }else{
                echo $arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["PRICE_FORMATED"];
            }
            echo '</div>';
        }?>
    </td>

    <td class="text-center border-bottom border-gray">
        <div class="s7spb-row quantity-counter"
             data-multiplicity="<?=$arItem["PROPERTY_MULTIPLICITY_VALUE"]?>"
             data-moq="<?=$arItem["PROPERTY_MOQ_VALUE"]?>"
             data-currency="<?=$arResult["CURRENCIES_FORMAT"][$arItem["CURRENCY"]]?>"
             data-sum="<?=$arItem["SUM_VALUE"]?>"
             data-price4="<?=$arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][4]["PRICE"]?>"
             data-price6="<?=$arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][6]["PRICE"]?>"
             data-price8="<?=$arResult["PRODUCT_PRICES"][$arItem["PRODUCT_ID"]][8]["PRICE"]?>"
             data-price="<?=$arItem["PRICE"]?>">
            <div class="s7spb-col quantity-counter-minus"
                 onclick="saleBasket.count(this, false)">&ndash;</div>
            <input type="text"
                   onkeyup="saleBasket.onkeyup(this)"
                   onblur="saleBasket.onblur(this)"
                   onfocus="saleBasket.onfocus(this)"
                   name="<?=$arResult["ORIGINAL_PARAMETERS"]["PRODUCT_QUANTITY_VARIABLE"]?>"
                   value="<?=$arItem["QUANTITY"]?>"

                   class="s7spb-col" />
            <div class="s7spb-col quantity-counter-plus"
                 onclick="saleBasket.count(this, true)">+</div>
        </div>
    </td>

    <td class="text-18 text-center text-nowrap border-bottom border-gray basket-sum">
        <?=$arItem["SUM"]?>
    </td>

    <td class="cursor-pointer border-bottom border-gray"
        onclick="saleBasket.delete(this)"
        data-id="<?=$arItem["ID"]?>"
        data-productid="<?=$arItem["PRODUCT_ID"]?>"
        data-title="<?=Loc::getMessage("BASKET_CNT_DELETE_TITLE")?>"
        data-delete="<?=Loc::getMessage("BASKET_CNT_DELETE")?>"
        data-cancel="<?=Loc::getMessage("BASKET_CNT_CANCEL")?>"
        title="<?=Loc::getMessage("BASKET_CNT_REMOVE")?>">
        <i class="icon-delete"></i>
    </td>

</tr>

