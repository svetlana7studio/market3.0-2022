<?
/**
 * @var array $arParams
 * @var array $arResult
 */
$arParams["Q"] = htmlspecialcharsbx($arParams["Q"]);
?>
<form class="title-search"
      action="/catalog/">

    <?$APPLICATION->ShowViewContent('MENU_SEARCH_SELECT');?>

    <input id="title-searchs-input"
           type="text"
           name="q"
           value="<?=$arParams["Q"]?>"
           size="40"
           class="text small_block"
           maxlength="100"
           autocomplete="off"
           placeholder="Я хочу купить...">

    <button type="submit" class="s7sbp--marketplace--header--search-line--search--input--button"></button>

</form>