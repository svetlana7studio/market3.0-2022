<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
?>

<div class="s7spb-row py3 line-s7">
    <div class="s7spb-col text-dark text-18 py3 text-semibold line-sum">
        <?=Loc::getMessage("ORDER_TOTAL_PRICE")?>:
        <span class="text-24"><?=$arResult["JS_DATA"]["TOTAL"]["ORDER_PRICE_FORMATED"]?></span>
    </div>
    <div class="s7spb-col text-info text-center py3 bg-white line-ss7" id="MINIMUM_ORDER_PRICE">
        <?
        echo Loc::getMessage("BASKET_TOTAL_MINIMUM_ORDER_PRICE", [
            "PRICE" => $arParams["MINIMUM_ORDER_PRICE"],
            "CURRENCY" => $arParams["MINIMUM_ORDER_CURRENCY"]
        ]);
        ?>
        <div class="text-bold text-nowrap">
            <i class="icon icon-reload text-vertical-middle"></i>
            <a href="/catalog/" class="text-info"><?=Loc::getMessage("BASKET_GOTO_CATALOG")?></a>
        </div>
    </div>
</div>