<?
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 * @var array $arResult
 * @var array $arParams
 */

use Bitrix\Main\Localization\Loc;
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
Loc::loadLanguageFile(__FILE__);

$this->SetViewTarget('BASKET_BASKET_LINE_INFORMER');
?>
<b><?=$arResult["NUM_PRODUCTS"]?></b> <?=$arResult["PRODUCT(S)"]?> <?=Loc::getMessage("TSB1_ON")?> <b><?=$arResult["TOTAL_PRICE"]?></b>
<?
$this->EndViewTarget();
?>
<a href="<?=$arParams["PATH_TO_BASKET"]?>"
   class="text-13 text-decoration-none text-black">
    <div id="cart-informer-count"><?=$arResult["NUM_PRODUCTS"]?></div>
    <i class="icon icon-cart"></i>
    <div><?=Loc::getMessage("TSB1_CART")?></div>
</a>