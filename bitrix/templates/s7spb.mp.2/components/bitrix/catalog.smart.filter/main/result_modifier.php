<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";

if(!empty($arParams["PROPERTY_UNSET"]))
{
    foreach($arResult["ITEMS"] as $key => $arItem)
    {
        if(in_array($arItem["CODE"], $arParams["PROPERTY_UNSET"])){
            unset($arResult["ITEMS"][$key]);
        }
    }
}


/*
if(!empty($_REQUEST["asf"])){
    $asf = [];
    foreach ($_REQUEST["asf"] as $val){
        $asf[] = "asf[]=" . $val;
    }
    $asf = implode("&", $asf);

}
*/

//$arResult["FORM_ACTION"] .= "?ID";
//$arResult["FILTER_URL"] = $arParams["PAGE"];