<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters["PROPERTY_UNSET"] = array(
    "NAME" => GetMessage('PROPERTY_UNSET'),
    "TYPE" => "LIST",
    "VALUES" => [

    ],
    "MULTIPLE" => "Y",
);