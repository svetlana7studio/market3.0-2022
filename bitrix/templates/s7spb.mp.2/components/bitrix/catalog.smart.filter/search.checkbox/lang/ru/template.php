<?
$MESS ['CT_BCSF_FILTER_TITLE'] = "Подбор параметров";
$MESS ['CT_BCSF_PICKUP_TITLE'] = "Подобрать";
$MESS ['CT_BCSF_FILTER_FROM'] = "От";
$MESS ['CT_BCSF_FILTER_TO'] = "До";
$MESS ['CT_BCSF_SET_FILTER'] = "Показать";
$MESS ['CT_BCSF_DEL_FILTER'] = "Сбросить";
$MESS ['CT_BCSF_FILTER_COUNT'] = "Найдено #ELEMENT_COUNT#";
$MESS ['CT_BCSF_FILTER_SHOW'] = "Показать";
$MESS ['CT_BCSF_FILTER_ALL'] = "Все";
$MESS ['PRICE'] = "Цена";
$MESS ['CT_BCSF_FILTER_TITLE'] = "Фильтры";
$MESS ['CT_BCSF_SHOW_ALL'] = "Показать все";
?>