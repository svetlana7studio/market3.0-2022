<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arDescription = Array(
	"NAME"=>GetMessage("SPB7_SIGNATURE_NAME"),
	"DESCRIPTION"=>GetMessage("SPB7_SIGNATURE_DESC"),
	"ICON" =>"",
	"TITLE_ICON_CLASS" => "bx-gadgets-spb7",
	"GROUP" => Array("ID"=>"admin_settings"),
	"NOPARAMS" => "Y",
	"LANGUAGE_ONLY_RU" => true,
	"AI_ONLY" => true,
	"COLOURFUL" => true
);
?>
