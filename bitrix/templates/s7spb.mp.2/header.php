<?
/**
 * @var global $APPLICATION
 */
use Bitrix\Main\Page\Asset,
    Bitrix\Main\Localization\Loc;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();
Loc::loadLanguageFile(__FILE__);

# Use global styleshit
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/styles.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/style-update.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/grids/grids.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/seperatizm.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/geometry.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/fonts.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/text.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/animations.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/icons.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/slider.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/colors.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/buttons.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/border.css");
# Use global catalog products styles
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/catalog/percent.css");
# Use global styleshit media-modes
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/media/text.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/nice.select.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/media/geometry.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/media/grid.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/media/seperatizm.css");

# Use global scripts
CJSCore::Init(["popup", "ajax", "fx"]);

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/quantity.counter.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/counters/yandex.metrika.js");
#Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/jquery.mask.js");
#Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/jquery.mask.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/app.js");
#Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/widget.popup.js");
#Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/script.js");

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/jquery-1.8.3.min.js");
#Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/jquery.flexslider.min.js");
#Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/jquery.actual.min.js");
#Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/jquery.ext.js");
#Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/jquery.fancybox.min.js");
#Asset::getInstance()->addString('<script>BX.message('.\CUtil::PhpToJSObject($MESS, false).')</script>', true);

# Use global meta information strings

global $curPage;
        $curPage = $APPLICATION->GetCurPage(true);
        if($curPage =="/index.php"){
            $notNeedBurger = true;
        }else{
            $notNeedBurger = preg_match("~^".SITE_DIR."(catalog)/~", $curPage);
        }


echo '<!DOCTYPE html>';
echo '<html lang="' . LANGUAGE_ID . '">';
echo '<head>';
echo '<meta charset="' . SITE_CHARSET . '" />';
echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
echo '<meta name="format-detection" content="telephone=no">';
$APPLICATION->ShowHead();
echo "<title>";
$APPLICATION->ShowTitle("title");
echo "</title>";
/*
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    [
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/template/header.counters.html"
    ],false, ["HIDE_ICONS" => "Y"]);
*/
echo '</head>';
echo '<body id="s7sbp-market2">';
$APPLICATION->ShowPanel();
?>

<header id="s7sbp-header" class="bg-white">

    <div class="mb3 bg-primary text-white px4 line-block-phone">
        <div class="s7spb-container">
            <div class="s7spb-row s7spb-row-center">
                <div class="s7spb-col-sm-12 s7spb-col-md-6 py2 line-head">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/include/template/hot.line.html"
                        )
                    );?>
                </div>
                <div class="s7spb-col-sm-12 s7spb-col-md-6 text-center text-md-right py2 line-head">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "top",
                        array(
                            "COMPONENT_TEMPLATE" => "top",
                            "ROOT_MENU_TYPE" => "main",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "main",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N"
                        ),
                        false
                    );?>
                </div>
            </div>
        </div>
    </div>
 <div class="header__inner">
    <div class="s7spb-container ">
        <div class="s7spb-row s7spb-row12 mob-fix">

            <div class="s7spb-col s7spb-col-12 s7spb-col-lg-auto text-center logo-wrapper">

                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "logo", [
                    "LINK" => [
                        "CLASS" => "d-block mb3",
                        "HREF" => SITE_DIR
                    ],
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/template/logo.php"
                ],
                    false,
                    ["ACTIVE_COMPONENT" => "Y"]);?>

			<div class="s7sbp--marketplace--header--search-line--hamburger hamburger hamburger--squeeze <?=$notNeedBurger ? "" : "d-block"?>">
								<div class="hamburger-box">
									<div class="hamburger-inner"></div>
								</div>
							</div>

                            <?$APPLICATION->ShowViewContent('hamburger');?>

            </div>


            <div class="s7spb-col s7spb-col-12 s7spb-col-lg">

                <div class="s7spb-row s7spb-row12 mb1 mob-search">

                    <div class="s7spb-col pt3 mb3 mob-fix">
                        <div class="px3">
                            <form id="title-search-desctop" class="title-search" action="<?=SITE_DIR?>catalog/">
                                <div class="select-box">  <?$APPLICATION->ShowViewContent('MENU_SEARCH_SELECT');?></div>

                                <?$APPLICATION->IncludeComponent("bitrix:search.title", "catalog",
                                    array(
                                        "NUM_CATEGORIES" => "1",
                                        "TOP_COUNT" => "5",
                                        "ORDER" => "date",
                                        "USE_LANGUAGE_GUESS" => "Y",
                                        "CHECK_DATES" => "Y",
                                        "SHOW_OTHERS" => "N",
                                        "QUERRY_VALUE" => trim(htmlspecialcharsbx($_REQUEST["q"])),
                                        "PAGE" => SITE_DIR."catalog/",
                                        "CATEGORY_0_TITLE" => GetMessage("CATEGORY_PRODUСTCS_SEARCH_NAME"),
                                        "CATEGORY_0" => array("iblock_marketplace"),
                                        "CATEGORY_0_iblock_marketplace" => array(
                                            0 => "2",
                                        ),
                                        "SHOW_INPUT" => "Y",
                                        "INPUT_ID" => "title-search-input-desctop",
                                        "CONTAINER_ID" => "title-search-desctop",
                                        "PRICE_CODE" => array(
                                            0 => "price_red",
                                            1 => "price_yellow",
                                            2 => "price_green",
                                        ),
                                        "PRICE_VAT_INCLUDE" => "Y",
                                        "SHOW_ANOUNCE" => "N",
                                        "PREVIEW_TRUNCATE_LEN" => "50",
                                        "SHOW_PREVIEW" => "Y",
                                        "PREVIEW_WIDTH" => "38",
                                        "PREVIEW_HEIGHT" => "38",
                                        "CONVERT_CURRENCY" => "N"
                                    ),
                                    false,
                                    array(
                                        "ACTIVE_COMPONENT" => "Y"
                                    )
                                );?>
                                <div class="s7sbp--marketplace--header--search-line--search--input--category"></div>
                                <button type="submit" class="s7sbp--marketplace--header--search-line--search--input--button"></button>
                            </form>
                        </div>
                    </div>

                    <div class="s7spb-col s7spb-col-sm-12 s7spb-col-md-auto text-center pt3">

                        <div class="s7spb-row s7spb-row12 text-center">

                            <div class="s7spb-col">
                                <?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "top", array(
                                        "REGISTER_URL" => SITE_DIR."auth/registration/",
                                        "FORGOT_PASSWORD_URL" => SITE_DIR."auth/forgot-password/",
                                        "PROFILE_URL" => SITE_DIR."personal/",
                                        "SHOW_ERRORS" => "Y"
                                    )
                                );?>
                            </div>

                            <div class="s7spb-col">
                                <a class="px3 text-decoration-none d-block text-black" href="<?=SITE_DIR?>personal/favorite/">
                                    <i class="icon icon-favorite"></i>
                                    <div class="text-13"><?=Loc::getMessage("FAVORITE")?></div>
                                </a>
                            </div>

                            <div class="s7spb-col position-relative">
                                <div class="pl3">
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:sale.basket.basket.line",
                                        "anitos",
                                        array(
                                            "COMPONENT_TEMPLATE" => "anitos",
                                            "PATH_TO_BASKET" => SITE_DIR."basket/",
                                            "PATH_TO_ORDER" => SITE_DIR."order/",
                                            "SHOW_NUM_PRODUCTS" => "Y",
                                            "SHOW_TOTAL_PRICE" => "Y",
                                            "SHOW_EMPTY_VALUES" => "Y",
                                            "SHOW_PERSONAL_LINK" => "Y",
                                            "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                                            "SHOW_AUTHOR" => "N",
                                            "PATH_TO_AUTHORIZE" => "",
                                            "SHOW_REGISTRATION" => "Y",
                                            "PATH_TO_REGISTER" => SITE_DIR."login/",
                                            "PATH_TO_PROFILE" => SITE_DIR."personal/",
                                            "SHOW_PRODUCTS" => "N",
                                            "POSITION_FIXED" => "N",
                                            "HIDE_ON_BASKET_PAGES" => "Y"
                                        ),
                                        false
                                    );?>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
<div class="s7spb-col s7spb-col-auto left-block mob-menu-burger">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "menu-mob",
                    array(
                        "COMPONENT_TEMPLATE" => "cataloglevel",
                        "ROOT_MENU_TYPE" => "cataloglevel",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "4",
                        "CHILD_MENU_TYPE" => "cataloglevel",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                );?>
            </div>	





        </div>
    </div>
</div>
</header>

<main id="s7sbp-main">