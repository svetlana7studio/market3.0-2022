<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
echo "</main>";
?>
    <footer id="s7sbp-footer">

    <div class="s7spb-container">

        <div class="s7spb-row">

            <div class="s7spb-col s7spb-col-12 s7spb-col-md-4 s7spb-col-lg-12 text-lg-center s7spb-col-xl text-xl-left info-marcet">

                <div class="p3">

                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "logo", [
                        "LINK" => [
                            "CLASS" => "d-block mb3",
                            "HREF" => SITE_DIR
                        ],
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/template/logo-footer.php"
                    ],
                        false,
                        ["ACTIVE_COMPONENT" => "Y"]);?>

                    <div class="mb1">© <?=date("Y")?></div>
                    <div class="mb1">Email: info@market.7sait.spb.ru</div>
                    <div class="mb1">Пользовательское соглашение</div>

                </div>

            </div>

            <div class="s7spb-col s7spb-col-12 s7spb-col-md-4 s7spb-col-lg">

                <div class="p3">

                    <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
                        "ROOT_MENU_TYPE" => "bottom_company",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "3600000",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "1",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),false
                    );?>

                </div>

            </div>

            <div class="s7spb-col s7spb-col-12 s7spb-col-md-4 s7spb-col-lg">

                <div class="p3">

                    <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
                        "ROOT_MENU_TYPE" => "bottom_services",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "3600000",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "1",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),false
                    );?>

                </div>

            </div>

            <div class="s7spb-col s7spb-col-12 s7spb-col-md-6 s7spb-col-lg">

                <div class="p3">

                    <?$APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
                        "ROOT_MENU_TYPE" => "bottom_partners",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "3600000",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "1",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),false
                    );?>

                </div>

            </div>

            <div class="s7spb-col s7spb-col-12 s7spb-col-md-6 s7spb-col-lg social-footer">

                <div class="p3">

                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/include/social.php"
                        )
                    );?>
                    <div class="bx-composite-banner"></div>
                </div>

            </div>

        </div>
    </div>

</footer>
    <div id="scroll-to-top"
         class="d-none"
         onclick="s7market.scrollToTop()"
         title="<?=Loc::getMessage("T_S7SPB_MARKETPLACE_SCROLL_TOP")?>"><div></div></div>
<?
if(
        $APPLICATION->GetCurDir() !== "/basket/" &&
        $APPLICATION->GetCurDir() !== "/order/"
):?>
    <a href="<?=SITE_DIR?>basket/" id="stiky-bascet" class="d-none">
        <span class="icon-basket"></span>
        <span id="stiky-bascet-counter"
              title="<?=Loc::getMessage("T_S7SPB_MARKETPLACE_BASKET_LOAD")?>"
              class="s7sbp--marketplace--full--basket--counter"><?$APPLICATION->ShowViewContent('BASKET_BASKET_LINE_INFORMER');?></span>
    </a>
<?endif;?>



<?
if(!in_array($USER->GetID(), [13])){
    $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
            "CART" => $componentElementParams["CART"],
            "PATH" => SITE_DIR."include/counters.html",
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "",
            "AREA_FILE_RECURSIVE" => "Y",
            "EDIT_TEMPLATE" => "standard.php"
        ),
        false,
        array('HIDE_ICONS' => 'Y')
    );
}
?>
<script>
document.querySelector('.hamburger').addEventListener('click', () => {
  document.querySelector('.hamburger').classList.toggle('active');
});
</script>
<script>
document.querySelector('.hamburger').addEventListener('click', () => {
  document.querySelector('.mob-menu-burger').classList.toggle('active');
});
</script>
<script> 

function ready() {
    var panels = document.getElementsByClassName("has-childs");
    var i;
    for (i = 0; panels.length > i; i++) {
        panels[i].onclick = function() {
            for (i = 0; panels.length > i; i++) {
               if (panels[i] != this)
    			panels[i].classList.remove("active");
            }
            if (this.classList.contains("active")) {
                this.classList.remove("active")
            } else {
                this.classList.add("active")
            }
        };
    }
}

document.addEventListener("DOMContentLoaded", ready);


</script>

<script> 
(function($) {
$(document).ready(function() {
    $('select').niceSelect();
});
}(jQuery));
</script>

<!--<script>
(function($) {

  $.fn.niceSelect = function(method) {
    
    // Methods
    if (typeof method == 'string') {      
      if (method == 'update') {
        this.each(function() {
          var $select = $(this);
          var $dropdown = $(this).next('.nice-select');
          var open = $dropdown.hasClass('open');
          
          if ($dropdown.length) {
            $dropdown.remove();
            create_nice_select($select);
            
            if (open) {
              $select.next().trigger('click');
            }
          }
        });
      } else if (method == 'destroy') {
        this.each(function() {
          var $select = $(this);
          var $dropdown = $(this).next('.nice-select');
          
          if ($dropdown.length) {
            $dropdown.remove();
            $select.css('display', '');
          }
        });
        if ($('.nice-select').length == 0) {
          $(document).off('.nice_select');
        }
      } else {
        console.log('Method "' + method + '" does not exist.')
      }
      return this;
    }
      
    // Hide native select
    this.hide();
    
    // Create custom markup
    this.each(function() {
      var $select = $(this);
      
      if (!$select.next().hasClass('nice-select')) {
        create_nice_select($select);
      }
    });
    
    function create_nice_select($select) {
      $select.after($('<div></div>')
        .addClass('nice-select')
        .addClass($select.attr('class') || '')
        .addClass($select.attr('disabled') ? 'disabled' : '')
        .attr('tabindex', $select.attr('disabled') ? null : '0')
        .html('<span class="current"></span><ul class="list"></ul>')
      );
        
      var $dropdown = $select.next();
      var $options = $select.find('option');
      var $selected = $select.find('option:selected');
      
      $dropdown.find('.current').html($selected.data('display') || $selected.text());
      
      $options.each(function(i) {
        var $option = $(this);
        var display = $option.data('display');

        $dropdown.find('ul').append($('<li></li>')
          .attr('data-value', $option.val())
          .attr('data-display', (display || null))
          .addClass('option' +
            ($option.is(':selected') ? ' selected' : '') +
            ($option.is(':disabled') ? ' disabled' : ''))
          .html($option.text())
        );
      });
    }
    
    /* Event listeners */
    
    // Unbind existing events in case that the plugin has been initialized before
    $(document).off('.nice_select');
    
    // Open/close
    $(document).on('click.nice_select', '.nice-select', function(event) {
      var $dropdown = $(this);
      
      $('.nice-select').not($dropdown).removeClass('open');
      $dropdown.toggleClass('open');
      
      if ($dropdown.hasClass('open')) {
        $dropdown.find('.option');  
        $dropdown.find('.focus').removeClass('focus');
        $dropdown.find('.selected').addClass('focus');
      } else {
        $dropdown.focus();
      }
    });
    
    // Close when clicking outside
    $(document).on('click.nice_select', function(event) {
      if ($(event.target).closest('.nice-select').length === 0) {
        $('.nice-select').removeClass('open').find('.option');  
      }
    });
    
    // Option click
    $(document).on('click.nice_select', '.nice-select .option:not(.disabled)', function(event) {
      var $option = $(this);
      var $dropdown = $option.closest('.nice-select');
      
      $dropdown.find('.selected').removeClass('selected');
      $option.addClass('selected');
      
      var text = $option.data('display') || $option.text();
      $dropdown.find('.current').text(text);
      
      $dropdown.prev('select').val($option.data('value')).trigger('change');
    });

    // Keyboard events
    $(document).on('keydown.nice_select', '.nice-select', function(event) {    
      var $dropdown = $(this);
      var $focused_option = $($dropdown.find('.focus') || $dropdown.find('.list .option.selected'));
      
      // Space or Enter
      if (event.keyCode == 32 || event.keyCode == 13) {
        if ($dropdown.hasClass('open')) {
          $focused_option.trigger('click');
        } else {
          $dropdown.trigger('click');
        }
        return false;
      // Down
      } else if (event.keyCode == 40) {
        if (!$dropdown.hasClass('open')) {
          $dropdown.trigger('click');
        } else {
          var $next = $focused_option.nextAll('.option:not(.disabled)').first();
          if ($next.length > 0) {
            $dropdown.find('.focus').removeClass('focus');
            $next.addClass('focus');
          }
        }
        return false;
      // Up
      } else if (event.keyCode == 38) {
        if (!$dropdown.hasClass('open')) {
          $dropdown.trigger('click');
        } else {
          var $prev = $focused_option.prevAll('.option:not(.disabled)').first();
          if ($prev.length > 0) {
            $dropdown.find('.focus').removeClass('focus');
            $prev.addClass('focus');
          }
        }
        return false;
      // Esc
      } else if (event.keyCode == 27) {
        if ($dropdown.hasClass('open')) {
          $dropdown.trigger('click');
        }
      // Tab
      } else if (event.keyCode == 9) {
        if ($dropdown.hasClass('open')) {
          return false;
        }
      }
    });

    // Detect CSS pointer-events support, for IE <= 10. From Modernizr.
    var style = document.createElement('a').style;
    style.cssText = 'pointer-events:auto';
    if (style.pointerEvents !== 'auto') {
      $('html').addClass('no-csspointerevents');
    }
    
    return this;

  };

}(jQuery));
</script> -->



</body>
</html>