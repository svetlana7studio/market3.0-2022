;(function (window)
{
    BX.namespace('BX.Sender');
    if (BX.Sender.Page)
    {
        return;
    }

    function Page()
    {
    }
    
    Page.prototype.choseProduct = function (num, id, name, img)
    {
        BX.html(BX("snippet-product-" + num), name);
        BX("snippet-product-id-" + num).value = id;
    };

    Page.prototype.sendToSnippet = function (form, code)
    {
        top.BX.Sender.Page.slider.close();

        var snippetHtml = "",
            count = 0,
            product1 = BX.findChild(form, {'attribute': {id:"snippet-product-id-1"}}, true),
            product2 = BX.findChild(form, {'attribute': {id:"snippet-product-id-2"}}, true),
            product3 = BX.findChild(form, {'attribute': {id:"snippet-product-id-3"}}, true);

        if(parseInt(product1.value) > 0){
            count++;
        }
        if(parseInt(product2.value) > 0){
            count++;
        }
        if(parseInt(product3.value) > 0){
            count++;
        }

        if(count === 1){
            snippetHtml = "<div class='products-snippet-block' data-id='" + product1.value +"'>###Вставка с одним продуктом###</div>";
        }
        if(count === 3){
            snippetHtml = "<div class='products-snippet-block' data-id='" + product1.value +"," + product2.value + "," + product3.value + "'>###Вставка с тремя продуктами###</div>";
        }


        top.BX(code).value = snippetHtml;

        return false;
    };

    Page.prototype.open = function (uri, callback, parameters)
    {
        this.slider.open(uri, callback, parameters);
    };

    Page.prototype.reloadGrid = function (id)
    {

    };

    Page.prototype.slider = {

        init: function (params)
        {
            if (!this.isSupported())
            {
                return;
            }
            if (
                typeof BX.Bitrix24 !== "undefined" &&
                typeof BX.Bitrix24.PageSlider !== "undefined"
            )
            {
                BX.Bitrix24.PageSlider.bindAnchors({
                    rules: [
                        {
                            condition: params.condition,
                            loader: params.loader,
                            stopParameters: [],
                            options: params.options
                        }
                    ],
                });
            }
        },
        getSlider: function ()
        {
            if (!this.isSupported())
            {
                return null;
            }

            return BX.SidePanel.Instance;
        },
        isInSlider: function ()
        {
            return (top !== window && top.BX && this.isSupported());
        },
        isSupported: function ()
        {
            return ((BX.SidePanel && BX.SidePanel.Instance));
        },
        bindOpen: function (element)
        {
            if (!this.isSupported())
            {
                return;
            }

            BX.bind(element, 'click', this.openHref.bind(this, element));
        },
        openHref: function (a, e)
        {
            if (!this.isSupported())
            {
                return;
            }

            e.preventDefault();
            e.stopPropagation();

            var href = a.getAttribute('href');
            if (!href)
            {
                return;
            }

            this.open(href);
        },
        open: function (uri, reloadAfterClosing, parameters)
        {
            if (!this.isSupported())
            {
                window.location.href = uri;
                return;
            }

            parameters = parameters || {};
            if (!BX.type.isBoolean(parameters.cacheable))
            {
                parameters.cacheable = false;
            }

            this.getSlider().open(uri, parameters);
            if (reloadAfterClosing)
            {
                if (!this.getSlider().iframe)
                {
                    return;
                }

                BX.addCustomEvent(
                    this.getSlider().iframe.contentWindow,
                    "BX.Bitrix24.PageSlider:onClose",
                    function ()
                    {
                        if (BX.type.isBoolean(reloadAfterClosing))
                        {
                            window.location.reload();
                        }
                        else if (BX.type.isFunction(reloadAfterClosing))
                        {
                            reloadAfterClosing();
                        }
                    }
                );
            }
            else
            {
                /*
                BX.addCustomEvent(
                    BX.SidePanel.Instance.getTopSlider(),
                    "SidePanel.Slider:onReload",
                    function () {
                        BX.Sender.Page.reloadGrid();
                    }
                );
                */
            }
        },
        close: function ()
        {
            if (!this.isSupported())
            {
                return null;
            }

            this.getSlider().close();
        }
    };

    BX.Sender.Page = new Page();
    BX.Sender.Page.slider.init({
        condition: ["/marketing/config/role/"],
        options: {
            cacheable: false,
            events: {
                onOpen: function () {
                    var manager = BX.Main.interfaceButtonsManager;
                    for (var menuId in manager.data)
                    {
                        manager.data[menuId].closeSubmenu();
                    }
                }
            }
        }
    });


})(window);