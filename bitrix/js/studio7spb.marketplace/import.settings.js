var kdaIEModuleName = 'studio7spb.marketplace';
var kdaIEModuleFilePrefix = 'studio7spb_import_excel';
var kdaIEModuleAddPath = '';

var marketplaceTransfer = {

    worksheet: {
        catalog: "#worksheet-catalog",
        preview: "#worksheet-preview"
    },

    preview: function(catalog)
    {


        var i = 0,
            n = 0,
            key,
            table = [],
            tr;

        for(i = 0; i < catalog.length; i++){
            n++;
            // set headders
            if(i === 0){

                // $(marketplaceTransfer.worksheet.preview)
                if($(marketplaceTransfer.worksheet.preview).find(".preview-header").length > 0)
                {
                    tr = "<tr class='preview-header'>";
                    tr += $(marketplaceTransfer.worksheet.preview).find(".preview-header").html();
                    tr += "</tr>";
                }else{
                    tr = "<tr class='preview-header'>";
                    tr += "<td>№</td>";
                    for(key in catalog[i]){
                        tr += this.th(key);
                    }
                    tr += "</tr>";
                }

                table.push(tr);
            }




            // set rows
            tr = "<tr>";
            tr += "<td>" + n +  "</td>";
            for(key in catalog[i]){
                tr += this.td(n, catalog[i][key], key);
            }
            tr += "</tr>";
            table.push(tr);

        }

        //table.append(tr);

        $(marketplaceTransfer.worksheet.preview)
            .fadeIn(300)
            .find("table")
            .html(table.join(''));

        $('.kda-ie-tbl').attr('data-init', 1);
        EList.Init();


        var position = $(marketplaceTransfer.worksheet.preview);
            position = position.offset();
            position = position.top;
            position = position - 150;

        $('body,html').animate({
            scrollTop: position
        }, 800);

        $(".fieldval").each(function () {
            EList.OnFieldFocus($(this));
        });
    },

    td: function(n, val, key){
        var td = "";
        if(n > 4){
            td += "<td><div class='value'>" + val + "</div></td>";
        }else{
            td += "<td><div class='value'>" + val + "</div>";
            td += '<input type="hidden" name="SIMPLE_ITEMS[' + key + '][' + n + ']" value="' + val + '" />';
            td += "</td>";
        }

        return td;
    },
    th: function(key){
        var th = '<td class="kda-ie-field-select">';
            // set name
            th += "<b>";
                th += key;
            th += "</b>";
            // set div control
            th += "<div>";
                th += '<input type="hidden" name="SETTINGS[FIELDS_LIST][' + key + '][0]" value="">';
                th += '<span class="fieldval_wrap"><span class="fieldval fieldval_empty" id="SETTINGS[FIELDS_LIST][' + key + '][0]" placeholder="- выбрать поле -" title="">- выбрать поле -</span></span>';

                th += '<a href="javascript:void(0)" class="field_settings inactive" id="SETTINGS[FIELDS_LIST][' + key + '][0]" title="Настройки поля" onclick="EList.ShowFieldSettings(this);">';
                th += '<input type="hidden" name="EXTRASETTINGS[0][0]" value="">';
                th += '</a>';

                th += '<a href="javascript:void(0)" class="field_delete" title="Удалить поле" onclick="EList.DeleteUploadField(this);"></a>';
            th += "</div>";
            // set kde buttons
            th += '<div class="kda-ie-field-select-btns">';
                th += '<div class="kda-ie-field-select-btns-inner">';
                    th += '<a href="javascript:void(0)" class="kda-ie-move-fields">';
                        th += '<span title="Переместить колонку и все последующие влево" onclick="return EList.ColumnsMoveLeft(this);"></span>';
                        th += '<span title="Переместить колонку и все последующие вправо" onclick="return EList.ColumnsMoveRight(this);"></span>';
                    th += '</a>';
                    th += '<a href="javascript:void(0)" class="kda-ie-add-load-field" title="Добавить поле" onclick="EList.AddUploadField(this);"></a>';
                th += '</div>';
            th += '</div>';

        th += "</td>";

        return th;
    },

    submit: function () {
        ShowWaitWindow();

        var form = $("#post_form"),
            action = form.attr("action"),
            data = form.serializeArray(),
            i;

        if(!!data && data.length > 0){
            for(i=0; i < data.length; i++){
                if(data[i].name == "vendor"){
                    value = parseInt(data[i].value);
                }
            }
        }

        $.post(action, data, function (result) {
            // fill catalog
            if(!!result.WORKSHEET && result.WORKSHEET.length > 0){
                $(marketplaceTransfer.worksheet.catalog).empty();
                for(i = 0; i < result.WORKSHEET.length; i++){


                    if(result.WORKSHEET[i].CURRENT > 0){
                        $(marketplaceTransfer.worksheet.catalog).append($("<option>", {
                            text: result.WORKSHEET[i].TITLE,
                            value: result.WORKSHEET[i].TITLE,
                            selected: "selected"
                        }));
                    }else{
                        $(marketplaceTransfer.worksheet.catalog).append($("<option>", {
                            text: result.WORKSHEET[i].TITLE,
                            value: result.WORKSHEET[i].TITLE,
                        }));
                    }


                }
            }
            // fill preview
            if(!!result.CATALOG && result.CATALOG.length > 0){
                marketplaceTransfer.preview(result.CATALOG);
            }

            if(!!result.ERRORS)
            {
                console.info(result);
            }


            CloseWaitWindow();
        }, "json");

        return false;
    },

    saveSettings: function () {
        ShowWaitWindow();
    },

    selectIblock: function (t){

        var type = $("#iblock-type"),
            iblock = $("#iblock-id"),
            section = $("#iblock-section"),
            type_value = type.val(),
            iblock_value = iblock.val(),
            section_value = section.val();

        if(type_value.length > 1){
            iblock.find("option")
                  .hide()
                  .each(function (){
                      if($(this).data("type") == type_value){
                          $(this).show();
                      }
                  });
        }

        if(iblock_value > 0)
        {
            section.find("option")
                   .hide()
                   .each(function (){
                        if($(this).data("iblock") == iblock_value){
                            $(this).show();
                        }
                   });
        }

    }
};

var EList = {
    cfields: {},

    Init: function()
    {
        var obj = this;
        this.InitLines();

        $('.kda-ie-tbl input[type=checkbox][name^="SETTINGS[CHECK_ALL]"]').bind('change', function(){
            var inputs = $(this).closest('tbody').find('input[type=checkbox]').not(this);
            if(this.checked)
            {
                inputs.prop('checked', true);
            }
            else
            {
                inputs.prop('checked', false);
            }
        });

        /*Bug fix with excess jquery*/
        var anySelect = $('select:eq(0)');
        if(typeof anySelect.chosen != 'function')
        {
            var jQuerySrc = $('script[src^="/bitrix/js/main/jquery/"]').attr('src');
            if(jQuerySrc)
            {
                $.getScript(jQuerySrc, function(){
                    $.getScript('/bitrix/js/'+kdaIEModuleName+'/chosen/chosen.jquery.min.js');
                });
            }
        }
        /*/Bug fix with excess jquery*/
        //this.SetFieldValues();

        /*$('.kda-ie-tbl select[name^="SETTINGS[FIELDS_LIST]"]').bind('change', function(){
			EList.OnChangeFieldHandler(this);
		}).trigger('change');*/

        $('.kda-ie-tbl input[type=checkbox][name^="SETTINGS[LIST_ACTIVE]"]').bind('click', function(e){
            if(e.shiftKey && (typeof obj.lastSheetChb == 'object') && obj.lastSheetChb.checked==this.checked)
            {
                var sheetKey1 = $(obj.lastSheetChb).closest('.kda-ie-tbl').attr('data-list-index');
                var sheetKey2 = $(this).closest('.kda-ie-tbl').attr('data-list-index');
                var kFrom = Math.min(sheetKey1, sheetKey2);
                var kTo = Math.max(sheetKey1, sheetKey2);
                for(var i=kFrom+1; i<kTo; i++)
                {
                    $('#list_active_'+i).prop('checked', this.checked);
                }
                obj.lastSheetChb = this;
                return;
            }
            obj.lastSheetChb = this;

            var showBtn = $(this).closest('td').find('a.showlist');
            if((this.checked && !showBtn.hasClass('open')) || (!this.checked && showBtn.hasClass('open'))) showBtn.trigger('click');
        });

        $('.kda-ie-tbl:not(.empty) tr.heading input[name^="SETTINGS[LIST_ACTIVE]"]:checked:eq(0)').closest('tr.heading').find('.showlist').trigger('click');
        //$('.kda-ie-tbl:not(.empty) tr.heading .showlist:eq(0)').trigger('click');


        $('.kda-ie-tbl:not(.empty) div.set').bind('scroll', function(){
            $('#kda_select_chosen').remove();
            $(this).prev('.set_scroll').scrollLeft($(this).scrollLeft());
        });
        $('.kda-ie-tbl:not(.empty) div.set_scroll').bind('scroll', function(){
            $('#kda_select_chosen').remove();
            $(this).next('.set').scrollLeft($(this).scrollLeft());
        });
        $(window).bind('resize', function(){
            EList.SetWidthList();
            $('.kda-ie-tbl tr.settings .set_scroll').removeClass('fixed');
            $('.kda-ie-tbl tr.settings .set_scroll_static').remove();
        });
        BX.addCustomEvent("onAdminMenuResize", function(json){
            $(window).trigger('resize');
        });
        $(window).trigger('resize');

        $(window).bind('scroll', function(){
            var windowHeight = $(window).height();
            var scrollTop = $(window).scrollTop();
            $('.kda-ie-tbl tr.settings:visible').each(function(){
                var height = $(this).height();
                if(height <= windowHeight) return;
                var top = $(this).offset().top;
                var scrollDiv = $('.set_scroll', this);
                if(scrollTop > top && scrollTop < top + height - windowHeight)
                {
                    if(!scrollDiv.hasClass('fixed'))
                    {
                        scrollDiv.before('<div class="set_scroll_static" style="height: '+(scrollDiv.height())+'px"></div>');
                        scrollDiv.addClass('fixed');
                    }
                }
                else if(scrollDiv.hasClass('fixed'))
                {
                    $('.set_scroll_static', this).remove();
                    scrollDiv.removeClass('fixed');
                }
            });
        });

        var sectionSelect = $('.kda-ie-tbl table.additional select');
        if(typeof sectionSelect.chosen == 'function') sectionSelect.chosen({search_contains: true, placeholder_text: BX.message("KDA_IE_SELECT_NOT_CHOSEN")});
    },

    InitLines: function(list)
    {
        var obj = this;
        $('.kda-ie-tbl .list input[name^="SETTINGS[IMPORT_LINE]"]').click(function(e){
            if(typeof obj.lastChb != 'object') obj.lastChb = {};
            var arKeys = this.name.substr(0, this.name.length - 1).split('][');
            var chbKey = arKeys.pop();
            var sheetKey = arKeys.pop();
            if(e.shiftKey && obj.lastChb[sheetKey] && obj.lastChb[sheetKey].checked==this.checked)
            {
                var arKeys2 = obj.lastChb[sheetKey].name.substr(0, obj.lastChb[sheetKey].name.length - 1).split('][');
                var chbKey2 = arKeys2.pop();
                var kFrom = Math.min(chbKey, chbKey2);
                var kTo = Math.max(chbKey, chbKey2);
                for(var i=kFrom+1; i<kTo; i++)
                {
                    $('.kda-ie-tbl .list input[name="SETTINGS[IMPORT_LINE]['+sheetKey+']['+i+']"]').prop('checked', this.checked);
                }
            }
            obj.lastChb[sheetKey] = this;
        });

        if(typeof admKDAMessages=='object' && typeof admKDAMessages.lineActions=='object')
        {
            var i = 0;
            for(var k in admKDAMessages.lineActions){i++};

            if(false /* || (i == 0)*/)
            {
                $('.kda-ie-tbl .list .sandwich').hide();
            }
            else
            {
                var sandwichSelector = '.kda-ie-tbl .list .sandwich';
                if(typeof list!='undefined') sandwichSelector = '.kda-ie-tbl[data-list-index='+list+'] .list .sandwich';

                $(sandwichSelector).unbind('click').bind('click', function(){
                    if(this.getAttribute('data-type')=='titles')
                    {
                        var list = $(this).closest('.kda-ie-tbl').attr('data-list-index');
                        var menuItems = [];
                        menuItems.push({
                            TEXT: BX.message("KDA_IE_INSERT_ALL_FIND_VALUES"),
                            ONCLICK: 'EList.InsertAllFindValues("'+list+'")'
                        });

                        var tbl = $(this).closest('.kda-ie-tbl');
                        var stInput = $('input[name="SETTINGS[LIST_SETTINGS]['+tbl.attr('data-list-index')+'][SET_TITLES]"]', tbl);
                        if(stInput.length > 0 && stInput.val().length > 0)
                        {
                            menuItems.push({
                                TEXT: BX.message("KDA_IE_CREATE_NEW_PROPERTIES"),
                                ONCLICK: 'EList.CreateNewProperties("'+list+'")'
                            });
                            var bfInput = $('input[name="SETTINGS[LIST_SETTINGS]['+tbl.attr('data-list-index')+'][BIND_FIELDS_TO_HEADERS]"]', tbl);
                            if(bfInput.length == 0 || bfInput.val()!='1')
                            {
                                menuItems.push({
                                    TEXT: BX.message("KDA_IE_BIND_FIELDS_TO_HEADERS"),
                                    ONCLICK: 'EList.BindFieldsToHeaders("'+list+'")'
                                });
                            }
                            else
                            {
                                menuItems.push({
                                    TEXT: BX.message("KDA_IE_UNBIND_FIELDS_TO_HEADERS"),
                                    ONCLICK: 'EList.UnbindFieldsToHeaders("'+list+'")'
                                });
                            }
                        }

                        var hecInput = $('input[name="SETTINGS[LIST_SETTINGS]['+tbl.attr('data-list-index')+'][HIDE_EMPTY_COLUMNS]"]', tbl);
                        if(hecInput.length == 0 || hecInput.val()!='1')
                        {
                            menuItems.push({
                                TEXT: BX.message("KDA_IE_HIDE_EMPTY_COLUMNS"),
                                ONCLICK: 'EList.HideEmptyColumns("'+list+'")'
                            });
                        }
                        else
                        {
                            menuItems.push({
                                TEXT: BX.message("KDA_IE_SHOW_EMPTY_COLUMNS"),
                                ONCLICK: 'EList.ShowEmptyColumns("'+list+'")'
                            });
                        }

                        if(this.OPENER) this.OPENER.SetMenu(menuItems);
                        BX.adminShowMenu(this, menuItems, {active_class: "bx-adm-scale-menu-butt-active"});
                    }
                    else
                    {
                        var inputName = $(this).closest('tr').find('input[name^="SETTINGS[IMPORT_LINE]"]:eq(0)').attr('name');
                        var arKeys = inputName.substr(22, inputName.length - 23).split('][');

                        var menuItems = [];
                        menuItems.push({
                            TEXT: BX.message("KDA_IE_ROW_SET_TITLES"),
                            ONCLICK: 'EList.SetLineAction("SET_TITLES", "'+arKeys[0]+'", "'+arKeys[1]+'")'
                        });
                        menuItems.push({
                            TEXT: BX.message("KDA_IE_ROW_SET_HINTS"),
                            ONCLICK: 'EList.SetLineAction("SET_HINTS", "'+arKeys[0]+'", "'+arKeys[1]+'")'
                        });

                        if(typeof admKDAMessages.lineActions.SECTION=='object')
                        {
                            var sectionItems = admKDAMessages.lineActions.SECTION;
                            var menuSectionItems = [];
                            for(var k in sectionItems)
                            {
                                menuSectionItems.push({
                                    TEXT: sectionItems[k].TEXT,
                                    ONCLICK: 'EList.SetLineAction("'+k+'", "'+arKeys[0]+'", "'+arKeys[1]+'")'
                                });
                            }
                            if(menuSectionItems.length > 0)
                            {
                                menuItems.push({
                                    TEXT: BX.message("KDA_IE_ROW_SECTION"),
                                    MENU: menuSectionItems
                                });
                            }
                        }
                        if(typeof admKDAMessages.lineActions.PROPERTY=='object')
                        {
                            var propertyItems = admKDAMessages.lineActions.PROPERTY;
                            var menuPropertyItems = [];
                            for(var k in propertyItems)
                            {
                                menuPropertyItems.push({
                                    TEXT: propertyItems[k].TEXT,
                                    ONCLICK: 'EList.SetLineAction("'+k+'", "'+arKeys[0]+'", "'+arKeys[1]+'")'
                                });
                            }
                            if(menuPropertyItems.length > 0)
                            {
                                menuItems.push({
                                    TEXT: BX.message("KDA_IE_ROW_PROPERTY"),
                                    MENU: menuPropertyItems
                                });
                            }
                        }
                        menuItems.push({
                            TEXT: BX.message("KDA_IE_ROW_REMOVE_ACTION"),
                            ONCLICK: 'EList.SetLineAction("REMOVE_ACTION", "'+arKeys[0]+'", "'+arKeys[1]+'")'
                        });
                        BX.adminShowMenu(this, menuItems, {active_class: "bx-adm-scale-menu-butt-active"});
                    }
                });
            }

            var inputsSelector = '.kda-ie-tbl input[name^="SETTINGS[LIST_SETTINGS]"]';
            if(typeof list!='undefined') inputsSelector = '.kda-ie-tbl[data-list-index='+list+'] input[name^="SETTINGS[LIST_SETTINGS]"]';

            $(inputsSelector).each(function(){
                EList.ShowLineActions(this, true);
            });
        }
    },

    InsertAllFindValues: function(list)
    {
        $('.kda-ie-tbl[data-list-index='+list+'] .list tr:first a.field_insert').trigger('click');
    },

    SetFieldValues: function(gParent, rewriteAutoinsert)
    {
        if(!gParent) gParent = $('.kda-ie-tbl');
        $('#IBLOCK_FIELDS_LIST', gParent).each(function(){
            var pSelect = this;
            var parent = $(pSelect).closest('tr');
            var arVals = [];
            var arValParents = [];
            for(var i=0; i<pSelect.options.length; i++)
            {
                arVals[pSelect.options.item(i).value] = pSelect.options.item(i).text;
                arValParents[pSelect.options.item(i).value] = pSelect.options.item(i).parentNode.getAttribute('label');
            }
            $('.kda-ie-field-select', parent).unbind('mousedown').bind('mousedown', function(event){
                var td = this;
                var timer = setTimeout(function(){
                    EList.ChooseColumnForMove(td, event);
                }, 200);
                $(window).one('mouseup', function(){
                    clearTimeout(timer);
                });
            }).unbind('selectstart').bind('selectstart', function(){return false;});
            EList.SetFieldAutoInsert(parent, rewriteAutoinsert);

            $('input[name^="SETTINGS[FIELDS_LIST]"]', parent).each(function(index){
                var input = this;
                /*var inputShow = $('input[name="'+input.name.replace('SETTINGS[FIELDS_LIST]', 'FIELDS_LIST_SHOW')+'"]', parent)[0];
				inputShow.setAttribute('placeholder', arVals['']);*/
                var inputShow = EList.GetShowInputFromValInput(input);
                inputShow.setAttribute('placeholder', arVals['']);

                if(!input.value || !arVals[input.value])
                {
                    //input.value = '';
                    //inputShow.value = '';
                    EList.SetHiddenFieldVal(input, '');
                    EList.SetShowFieldVal(inputShow, '');
                    return;
                }
                /*inputShow.value = arVals[input.value];
				inputShow.title = arVals[input.value];*/
                EList.SetHiddenFieldVal(input, input.value);
                EList.SetShowFieldVal(inputShow, arVals[input.value], arValParents[input.value]);
            });

            EList.OnFieldFocus($('span.fieldval', parent));
        });
    },

    GetShowInputNameFromValInputName: function(name)
    {
        return name.replace(/SETTINGS\[FIELDS_LIST\]\[([\d_]*)\]\[([\d_]*)\]/, 'field-list-show-$1-$2');
    },

    GetShowInputFromValInput: function(input)
    {
        return $('#'+this.GetShowInputNameFromValInputName(input.name))[0];
    },

    GetValInputFromShowInput: function(input)
    {
        return  $(input).closest('td').find('input[name="'+input.id.replace(/field-list-show-([\d_]*)-([\d_]*)$/, 'SETTINGS[FIELDS_LIST][$1][$2]')+'"]')[0];
    },

    InArray: function(val, arr)
    {
        for(var i=0; i<arr.length; i++)
        {
            if(arr[i]==val) return true;
        }
        return false;
    },

    SetHiddenFieldVal: function(input, val)
    {
        input = $(input);
        var arKeys = input.attr('name').replace(/SETTINGS\[FIELDS_LIST\]\[([\d_]*)\]\[([\d_]*)\]/, '$1|$2').split('|');
        if(!this.cfields[arKeys[0]]) this.cfields[arKeys[0]] = {};

        var oldVal = input.val();
        if(oldVal!=val && oldVal.length > 0 && this.cfields[arKeys[0]][oldVal] && this.cfields[arKeys[0]][oldVal].length > 1)
        {
            var fkeys = this.cfields[arKeys[0]][oldVal];
            this.cfields[arKeys[0]][oldVal] = [];
            for(var i=0; i<fkeys.length; i++)
            {
                if(arKeys[1]==fkeys[i])
                {
                    $('#field-list-show-'+arKeys[0]+'-'+fkeys[i]).removeClass('fieldval_duplicated');
                    continue;
                }
                this.cfields[arKeys[0]][oldVal].push(fkeys[i]);
            }
            var fkeys = this.cfields[arKeys[0]][oldVal];
            if(fkeys.length == 1)
            {
                for(var i=0; i<fkeys.length; i++)
                {
                    $('#field-list-show-'+arKeys[0]+'-'+fkeys[i]).removeClass('fieldval_duplicated');
                }
            }
        }

        if(val.length > 0)
        {
            if(!this.cfields[arKeys[0]][val]) this.cfields[arKeys[0]][val] = [];
            if(!this.InArray(arKeys[1], this.cfields[arKeys[0]][val])) this.cfields[arKeys[0]][val].push(arKeys[1]);
            var fkeys = this.cfields[arKeys[0]][val];
            if(fkeys.length > 1)
            {
                for(var i=0; i<fkeys.length; i++)
                {
                    $('#field-list-show-'+arKeys[0]+'-'+fkeys[i]).addClass('fieldval_duplicated');
                }
            }
        }

        input.val(val);
    },

    SetShowFieldVal: function(input, val, group)
    {
        input = $(input);
        var jsInput = input[0];
        var placeholder = jsInput.getAttribute('placeholder');
        if(val.length > 0 && val!=placeholder)
        {
            jsInput.innerHTML = val;
            input.removeClass('fieldval_empty');
        }
        else
        {
            jsInput.innerHTML = placeholder;
            input.addClass('fieldval_empty');
        }
        jsInput.title = (group ? group+' - ' : '')+val;
    },

    SetFieldAutoInsert: function(tr, rewriteAutoinsert)
    {
        var pSelect = $('#IBLOCK_FIELDS_LIST');
        if(pSelect.length==0) return;
        pSelect = pSelect[0];
        var arVals = [], arValsLowered = [], arValsCorrected = [], arValParents = [], key = '';
        for(var i=0; i<pSelect.options.length; i++)
        {
            key = pSelect.options.item(i).value;
            arVals[key] = pSelect.options.item(i).text;
            arValsLowered[key] = $.trim(pSelect.options.item(i).text.toLowerCase()).replace(/(&nbsp;|\s)+/g, ' ');
            arValsCorrected[key] = arValsLowered[key].replace(/^[^"]*"/, '').replace(/"[^"]*$/, '').replace(/\s+\[[\d\w_]*\]$/, '');
            arValParents[key] = pSelect.options.item(i).parentNode.getAttribute('label');
            if(arValParents[key]) arValParents[key] = arValParents[key].replace(/^.*(\d+\D{5}).*$/, '$1');
        }

        var table = $(tr).closest('table');
        var ctr = $('.slevel[data-level-type="headers"]', table).closest('tr');
        var fselect = $('.kda-ie-field-select', tr);
        var cntCheckCells = (fselect.length > 1000 ? 5 : 10);
        var timeBegin = (new Date()).getTime();

        fselect.each(function(index){
            $('.field_insert', this).remove();
            if((new Date()).getTime() - timeBegin > 3000) return;
            var input = $('input[name^="SETTINGS[FIELDS_LIST]"]:eq(0)', this);
            //var inputShow = $('input[name^="FIELDS_LIST_SHOW"]:eq(0)', this);
            var inputShow = $('span.fieldval:eq(0)', this);
            //var firstVal = $(this).closest('table').find('tr:gt(0) > td:nth-child('+(index+2)+') .cell_inner:not(:empty):eq(0)').text().toLowerCase();

            if(!$(this).attr('data-autoinsert-init') || rewriteAutoinsert)
            {
                var ind = false;

                if(ctr.length > 0 && ctr[0].cells[index+1])
                {
                    var inputVals = $(ctr[0].cells[index+1]).find('.cell_inner:not(:empty)');
                }
                else
                {
                    var inputVals = table.find('tr:gt(0) > td:nth-child('+(index+2)+') .cell_inner:not(:empty):lt('+cntCheckCells+')');
                }

                var length = -1;
                var bFind = false;
                if(inputVals && inputVals.length > 0)
                {
                    for(var j=0; j<inputVals.length; j++)
                    {
                        var firstValOrig = inputVals[j].innerHTML;
                        var firstVal = $.trim(firstValOrig.toLowerCase()).replace(/(&nbsp;|\s)+/g, ' ');
                        var firstValCode = firstVal.replace(/^.*\[(.*)\].*$/, '$1');
                        for(var i in arValsLowered)
                        {
                            if(firstValOrig.indexOf('{'+i+'}')!=-1)
                            {
                                ind = i;
                                bFind = true;
                                break;
                            }
                            var lowerVal = arValsLowered[i];
                            var lowerValCorrected = arValsCorrected[i];
                            if(i.indexOf('ISECT')==0 && !i.match(/ISECT\d+_NAME/) && lowerVal.indexOf('seo')==-1) continue;
                            if(
                                (firstVal.indexOf(lowerVal)!=-1)
                                || (lowerValCorrected.length > 0 && firstVal.indexOf(lowerValCorrected)!=-1)
                                || (firstValCode.length > 0 && lowerVal.indexOf('['+firstValCode+']'))!=-1
                                || (i.indexOf('ISECT')==0 && firstVal.indexOf(arValParents[i])!=-1))
                            {
                                if(length < 0)
                                {
                                    length = firstVal.replace(lowerVal, '').replace(lowerValCorrected, '').length;
                                    ind = i;
                                }
                                else if(firstVal.replace(lowerVal, '').replace(lowerValCorrected, '').length < length)
                                {
                                    length = firstVal.replace(lowerVal, '').replace(lowerValCorrected, '').length;
                                    ind = i;
                                }
                            }
                        }
                        if(bFind) break;
                    }
                }
            }
            else
            {
                var ind = $(this).attr('data-autoinsert-index');
                if(ind == 'false') ind = false;
            }

            if(ind)
            {
                $('a.field_settings:eq(0)', this).before('<a href="javascript:void(0)" class="field_insert" title=""></a>');
                $('a.field_insert', this).attr('title', arVals[ind]+' ('+BX.message("KDA_IE_INSERT_FIND_FIELD")+')')
                    .attr('data-value', ind)
                    .attr('data-show-value', arVals[ind])
                    .attr('data-show-group', arValParents[ind])
                    .bind('click', function(){
                        //input.val(this.getAttribute('data-value'));
                        //inputShow.val(this.getAttribute('data-show-value'));
                        EList.SetHiddenFieldVal(input, this.getAttribute('data-value'));
                        EList.SetShowFieldVal(inputShow, this.getAttribute('data-show-value'), this.getAttribute('data-show-group'));
                    });
            }

            $(this).attr('data-autoinsert-init', 1);
            $(this).attr('data-autoinsert-index', ind);
        });
    },

    OnFieldFocus: function(objInput)
    {
        $(objInput).unbind('click').bind('click', function(){
            var input = this;
            /*var parentTd = $(input).closest('td');
			parentTd.css('width', parentTd[0].offsetWidth - 6);*/

            var pSelect = $('#IBLOCK_FIELDS_LIST');
            //var inputVal = $('input[name="'+input.name.replace('FIELDS_LIST_SHOW', 'SETTINGS[FIELDS_LIST]')+'"]', parent)[0];
            var inputVal = EList.GetValInputFromShowInput(input);
            //$(input).css('visibility', 'hidden');
            var select = $(pSelect).clone();
            var options = select[0].options;

            for(var i=0; i<options.length; i++)
            {
                if(inputVal.value==options.item(i).value) options.item(i).selected = true;
            }

            var chosenId = 'kda_select_chosen';
            $('#'+chosenId).remove();
            var offset = $(input).offset();
            var div = $('<div></div>');
            div.attr('id', chosenId);
            div.css({
                position: 'absolute',
                left: offset.left,
                top: offset.top,
                //width: $(input).width() + 27
                width: $(input).width() + 1
            });
            div.append(select);
            $('body').append(div);

            //select.insertBefore($(input));
            if(typeof select.chosen == 'function') select.chosen({search_contains: true});
            select.bind('change', function(){
                if(this.value=='new_prop')
                {
                    var ind = this.name.replace(/^.*\[(\d+)\]$/, '$1');
                    var ptable = $('.kda-ie-tbl:eq('+ind+')');
                    var stInput = $('input[name="SETTINGS[LIST_SETTINGS]['+ind+'][SET_TITLES]"]', ptable);
                    var propName = '';
                    if(stInput.length > 0 && stInput.val().length > 0)
                    {
                        var rowIndex = parseInt(stInput.val()) + 1;
                        var cellIndex = parseInt(inputVal.name.replace(/^.*\[(\d+)(_\d+)?\]$/, '$1')) + 1;
                        propName = $('table.list tr:eq('+rowIndex+') td:eq('+cellIndex+') .cell_inner', ptable).html();
                    }


                    var option = options.item(0);
                    var dialog = new BX.CAdminDialog({
                        'title':BX.message("KDA_IE_POPUP_NEW_PROPERTY_TITLE"),
                        'content_url':'/bitrix/admin/'+kdaIEModuleFilePrefix+'_new_property.php?FIELD_NAME='+encodeURIComponent(/*input.name*/input.id)+'&IBLOCK_ID='+ptable.attr('data-iblock-id')+'&PROP_NAME='+encodeURIComponent(propName),
                        'width':'600',
                        'height':'400',
                        'resizable':true});
                    EList.newPropDialog = dialog;
                    EList.NewPropDialogButtonsSet();

                    BX.addCustomEvent(dialog, 'onWindowRegister', function(){
                        $('input[type=checkbox]', this.DIV).each(function(){
                            BX.adminFormTools.modifyCheckbox(this);
                        });
                    });

                    dialog.Show();
                }
                else
                {
                    var option = options.item(select[0].selectedIndex);
                }
                if(option.value)
                {
                    /*input.value = option.text;
					input.title = option.text;
					inputVal.value = option.value;*/
                    EList.SetHiddenFieldVal(inputVal, option.value);
                    EList.SetShowFieldVal(input, option.text, option.parentNode.getAttribute('label'));
                }
                else
                {
                    /*input.value = '';
					input.title = '';
					inputVal.value = '';*/
                    EList.SetHiddenFieldVal(inputVal, '');
                    EList.SetShowFieldVal(input, '');
                }
                if(typeof select.chosen == 'function') select.chosen('destroy');
                //select.remove();
                $('#'+chosenId).remove();
                //$(input).css('visibility', 'visible');
                //parentTd.css('width', 'auto');
            });

            $('body').one('click', function(e){
                e.stopPropagation();
                return false;
            });
            var chosenDiv = select.next('.chosen-container')[0];
            $('a:eq(0)', chosenDiv).trigger('mousedown');

            var lastClassName = chosenDiv.className;
            var interval = setInterval( function() {
                var className = chosenDiv.className;
                if (className !== lastClassName) {
                    select.trigger('change');
                    lastClassName = className;
                    clearInterval(interval);
                }
            },30);
        });
    },

    NewPropDialogButtonsSet: function(fireEvents)
    {
        var dialog = this.newPropDialog;
        dialog.SetButtons([
            dialog.btnCancel,
            new BX.CWindowButton(
                {
                    title: BX.message('JS_CORE_WINDOW_SAVE'),
                    id: 'savebtn',
                    name: 'savebtn',
                    className: BX.browser.IsIE() && BX.browser.IsDoctype() && !BX.browser.IsIE10() ? '' : 'adm-btn-save',
                    action: function () {
                        var form = document.getElementById('newPropertyForm');
                        if($.trim(form['FIELD[NAME]'].value).length==0)
                        {
                            form['FIELD[NAME]'].style.border = '1px solid red';
                            return;
                        }
                        else
                        {
                            form['FIELD[NAME]'].style.border = '';
                        }

                        this.disableUntilError();
                        this.parentWindow.PostParameters();
                        //this.parentWindow.Close();
                    }
                })
        ]);

        if(fireEvents)
        {
            BX.onCustomEvent(dialog, 'onWindowRegister');
        }
    },

    CreateNewProperties: function(listIndex)
    {
        var ptable = $('.kda-ie-tbl[data-list-index='+listIndex+']');
        var table = $('table.list', ptable);
        var tds = $('td.kda-ie-field-select', table);
        var l = $('.slevel[data-level-type="headers"]', table);
        if(l.length==0) return;

        var items = [];
        var tds2 = l.closest('td').nextAll('td');
        for(var i=0; i<tds2.length; i++)
        {
            var input = tds.eq(i).find('input[name^="SETTINGS[FIELDS_LIST]['+listIndex+']["]:first');
            var text = $(tds2[i]).text();
            if(input.val().length==0 && text.length > 0)
            {
                items.push({index: i, text: text});
            }
        }
        if(items.length > 0)
        {
            var dialogParams = {
                'title':BX.message("KDA_IE_NEW_PROPS_TITLE"),
                'content_url':'/bitrix/admin/'+kdaIEModuleFilePrefix+'_new_properties.php?list_index='+listIndex+'&IBLOCK_ID='+ptable.attr('data-iblock-id'),
                'content_post': {items: items},
                'width':'700',
                'height':'430',
                'resizable':true
            };
            var dialog = new BX.CAdminDialog(dialogParams);

            dialog.SetButtons([
                dialog.btnCancel,
                new BX.CWindowButton(
                    {
                        title: BX.message('JS_CORE_WINDOW_SAVE'),
                        id: 'savebtn',
                        name: 'savebtn',
                        className: BX.browser.IsIE() && BX.browser.IsDoctype() && !BX.browser.IsIE10() ? '' : 'adm-btn-save',
                        action: function () {
                            this.disableUntilError();
                            this.parentWindow.PostParameters();
                        }
                    })
            ]);

            BX.addCustomEvent(dialog, 'onWindowRegister', function(){
                $('input[type=checkbox]', this.DIV).each(function(){
                    BX.adminFormTools.modifyCheckbox(this);
                });
                if(typeof $('.kda-ie-newprops-mchosen select').chosen == 'function')
                {
                    $('.kda-ie-newprops-mchosen select').chosen({'placeholder_text':BX.message("KDA_IE_NP_FIELDS_FOR_UPDATE")});
                }
            });

            dialog.Show();
        }
    },

    OnAfterAddNewProperties: function(listIndex, iblockId, result)
    {
        var ptable = $('.kda-ie-tbl[data-list-index='+listIndex+']');
        var form = ptable.closest('form')[0];
        var post = {
            'MODE': 'AJAX',
            'ACTION': 'GET_SECTION_LIST',
            'IBLOCK_ID': iblockId,
            'PROFILE_ID': form.PROFILE_ID.value
        }
        $.post(window.location.href, post, function(data){
            ptable.find('select[name^="FIELDS_LIST["]').each(function(){
                var fields = $(data).find('select[name=fields]');
                fields.attr('name', this.name);
                $(this).replaceWith(fields);
            });
        });

        if(typeof result == 'object')
        {
            $('table.list td.kda-ie-field-select', ptable).each(function(index){
                if(!result[index]) return;
                var td = $(this);
                //$('input[name="FIELDS_LIST_SHOW['+listIndex+']['+index+']"]', td).val(result[index]['NAME']);
                //$('input[name="SETTINGS[FIELDS_LIST]['+listIndex+']['+index+']"]', td).val(result[index]['ID']);
                EList.SetHiddenFieldVal($('input[name="SETTINGS[FIELDS_LIST]['+listIndex+']['+index+']"]', td), result[index]['ID']);
                EList.SetShowFieldVal($('#field-list-show-'+listIndex+'-'+index, td), result[index]['NAME']);
                td.addClass('kda-ie-field-select-highligth');
                setTimeout(function(){
                    td.removeClass('kda-ie-field-select-highligth');
                }, 2000);
            });
        }

        BX.WindowManager.Get().Close();
    },

    SetOldTitles: function(listIndex, titles)
    {
        if(!this.oldTitles) this.oldTitles = {};
        this.oldTitles[listIndex] = titles;
    },

    BindFieldsToHeaders: function(listIndex)
    {
        var settingsBlock = $('.kda-ie-tbl[data-list-index='+listIndex+']').find('.list-settings');

        var inputName = 'SETTINGS[LIST_SETTINGS]['+listIndex+'][BIND_FIELDS_TO_HEADERS]';
        var input = $('input[name="'+inputName+'"]', settingsBlock);
        if(input.length == 0)
        {
            settingsBlock.append('<input type="hidden" name="'+inputName+'" value="">');
            input = $('input[name="'+inputName+'"]', settingsBlock);
        }
        input.val(1);
    },

    UnbindFieldsToHeaders: function(listIndex)
    {
        var settingsBlock = $('.kda-ie-tbl[data-list-index='+listIndex+']').find('.list-settings');
        var inputName = 'SETTINGS[LIST_SETTINGS]['+listIndex+'][BIND_FIELDS_TO_HEADERS]';
        $('input[name="'+inputName+'"]', settingsBlock).remove();
    },

    HideEmptyColumns: function(listIndex)
    {
        var settingsBlock = $('.kda-ie-tbl[data-list-index='+listIndex+']').find('.list-settings');

        var inputName = 'SETTINGS[LIST_SETTINGS]['+listIndex+'][HIDE_EMPTY_COLUMNS]';
        var input = $('input[name="'+inputName+'"]', settingsBlock);
        if(input.length == 0)
        {
            settingsBlock.append('<input type="hidden" name="'+inputName+'" value="">');
            input = $('input[name="'+inputName+'"]', settingsBlock);
        }
        input.val(1);

        var tbl = $('.kda-ie-tbl[data-list-index='+listIndex+'] table.list');
        $('.kda-ie-field-select', tbl).each(function(index){
            var inputs = $('input[name^="SETTINGS[FIELDS_LIST]"]', this);
            var empty = true;
            for(var i=0; i<inputs.length; i++)
            {
                if($.trim(inputs[i].value).length > 0) empty = false;
            }
            if(empty)
            {
                $(this).addClass('kda-ie-field-select-empty');
                $('>td:eq('+(index+1)+')', $('>tr', tbl)).hide();
                $('>td:eq('+(index+1)+')', $('>tbody >tr', tbl)).hide();
            }
        });
        $(window).trigger('resize');
    },

    ShowEmptyColumns: function(listIndex)
    {
        var settingsBlock = $('.kda-ie-tbl[data-list-index='+listIndex+']').find('.list-settings');
        var inputName = 'SETTINGS[LIST_SETTINGS]['+listIndex+'][HIDE_EMPTY_COLUMNS]';
        $('input[name="'+inputName+'"]', settingsBlock).remove();
        var tbl = $('.kda-ie-tbl[data-list-index='+listIndex+'] table.list');
        $('.kda-ie-field-select', tbl).each(function(index){
            if($(this).hasClass('kda-ie-field-select-empty'))
            {
                $(this).removeClass('kda-ie-field-select-empty');
                $('>td:eq('+(index+1)+')', $('>tr', tbl)).show();
                $('>td:eq('+(index+1)+')', $('>tbody >tr', tbl)).show();
            }
        });
        $(window).trigger('resize');
    },

    ShowLineActions: function(input, firstInit)
    {
        var arKeys = input.name.substr(0, input.name.length - 1).split('][');
        var action = arKeys[arKeys.length - 1];
        var tblIndex = arKeys[arKeys.length - 2];
        var title = '';
        var fieldGroup = '';
        if(action.indexOf('SET_SECTION_')==0) fieldGroup = 'SECTION';
        if(action.indexOf('SET_PROPERTY_')==0) fieldGroup = 'PROPERTY';
        if(fieldGroup.length > 0 && admKDAMessages.lineActions[fieldGroup])
        {
            if(admKDAMessages.lineActions[fieldGroup][action]) title = admKDAMessages.lineActions[fieldGroup][action].TITLE;
            var style = input.value;
            if(!style) return;
            var level = this.GetActionLevel(action);
            var label = title.substr(0, 1);
            if(fieldGroup=='SECTION') label = 'P'+(level > 0 ? level : '');
            var tbl = $(input).closest('.kda-ie-tbl');
            var cellNameIndex = 0;
            var inputCellName = 'SETTINGS[LIST_SETTINGS]['+tblIndex+'][SECTION_NAME_CELL]';
            var inputCellNameObj = $('input[name="'+inputCellName+'"]', tbl);
            if(inputCellNameObj.length > 0)
            {
                cellNameIndex = inputCellNameObj.val();
            }
            tbl.find('td.line-settings').each(function(){
                var td = $(this);
                var tr = td.closest('tr');
                if($('.cell_inner:not(:empty):eq(0)', tr).attr('data-style') == style)
                {
                    var html = '<span class="slevel" data-level="'+level+'" title="'+title+'">'+label+'</span>';
                    if(td.find('.slevel').length > 0)
                    {
                        td.find('.slevel').replaceWith(html);
                    }
                    else
                    {
                        td.append(html);
                    }
                    if(cellNameIndex > 0)
                    {
                        $('td:eq('+cellNameIndex+')', tr).addClass('section_name_cell');
                    }

                    $('td:gt(0)', tr).bind('contextmenu', function(e){
                        EList.SetCurrentCell(this);
                        if(this.OPENER) this.OPENER.Toggle();
                        else
                        {
                            var menuItems = [
                                {
                                    TEXT: BX.message("KDA_IE_IS_SECTION_NAME"),
                                    ONCLICK: 'EList.ChooseSectionNameCell();'
                                },
                                {
                                    TEXT: BX.message("KDA_IE_SECTION_FIELD_SETTINGS"),
                                    ONCLICK: 'EList.ShowSectionFieldSettings("'+level+'");'
                                },
                                {
                                    TEXT: BX.message("KDA_IE_RESET_ACTION"),
                                    ONCLICK: 'EList.UnsetSectionNameCell();'
                                }
                            ];
                            BX.adminShowMenu(this, menuItems, {active_class: "bx-adm-scale-menu-butt-active"});
                        }

                        e.stopPropagation();
                        return false;
                    });
                }
                else
                {
                    if(td.find('.slevel[data-level='+level+']').length > 0)
                    {
                        $('td.section_name_cell', tr).removeClass('section_name_cell');
                        $('td:gt(0)', tr).unbind('contextmenu');
                        td.find('.slevel').remove();
                    }
                }
            });
        }
        else if(action.indexOf('SECTION_NAME_CELL')==0)
        {
            var tdIndex = parseInt(input.value);
            var td = $('.kda-ie-tbl[data-list-index='+tblIndex+'] input[name^="SETTINGS[IMPORT_LINE]"]:eq(0)').closest('tr').find('td:eq('+tdIndex+')');
            if(td.length > 0)
            {
                td = td[0];
                EList.SetCurrentCell(td);
                EList.ChooseSectionNameCell();
            }
        }
        else if(action.indexOf('SET_TITLES')==0 || action.indexOf('SET_HINTS')==0)
        {
            var levelType = (action.indexOf('SET_TITLES')==0 ? 'headers' : 'hints');
            var lineIndex = input.value;
            var tbl = $(input).closest('.kda-ie-tbl');
            $('.slevel[data-level-type="'+levelType+'"]', tbl).remove();
            var td = $('input[name="SETTINGS[IMPORT_LINE]['+tblIndex+']['+lineIndex+']"]', tbl).closest('td.line-settings');
            if(td.length > 0)
            {
                var html = '<span class="slevel" data-level-type="'+levelType+'"></span>';
                if(td.find('.slevel').length > 0)
                {
                    td.find('.slevel').replaceWith(html);
                }
                else
                {
                    td.append(html);
                }
                var tr = td.closest('tr');
                tr.addClass('kda-ie-tbl-'+levelType);
                if(levelType=='headers')
                {
                    tr.find('>td:gt(0)').each(function(tdIndex){
                        var tdInput = $('<input type="hidden" name="SETTINGS[TITLES_LIST]['+tblIndex+']['+tdIndex+']" value="">');
                        tdInput.val($.trim($('.cell_inner', this).text()).toLowerCase().replace(/[\r\n\s]+/g, ' '));
                        $(this).prepend(tdInput);
                    });
                    var obj = this;
                    if(firstInit && obj.oldTitles && obj.oldTitles[tblIndex])
                    {
                        /*var bfInput = $('input[name="SETTINGS[LIST_SETTINGS]['+tblIndex+'][BIND_FIELDS_TO_HEADERS]"]', tbl);
						if(bfInput.length > 0)
						{
							var tds = $('table.list td.kda-ie-field-select', tbl);
							var oldTds = tds.clone();
							$('table.list td.kda-ie-field-select', tbl).each(function(tdIndex){
								var cutTitle = $('input[name="SETTINGS[TITLES_LIST]['+tblIndex+']['+tdIndex+']"]', tr).val();
								if(!obj.oldTitles[tblIndex][tdIndex] || obj.oldTitles[tblIndex][tdIndex]!=cutTitle)
								{
									var oldIndex = -1;
									for(var i in obj.oldTitles[tblIndex])
									{
										if(obj.oldTitles[tblIndex][i]==cutTitle) oldIndex = i;
									}
									if(oldIndex>=0)
									{
										$('>div', this).remove();
										$('>div', oldTds[oldIndex]).clone().appendTo(this);
									}
									else
									{
										$('>div:gt(0)', this).remove();
										EList.SetHiddenFieldVal($('>div input', this), '');
										EList.SetShowFieldVal($('>div span.fieldval', this), '');
									}
								}
							});
							this.OnAfterColumnsMove(tds.eq(0).closest('tr'));
						}*/
                    }
                }
            }
        }
    },

    GetActionLevel: function(action)
    {
        var level = 0;
        if(action.indexOf('SET_SECTION_')==0)
        {
            level = parseInt(action.substr(12));
            if(action.substr(12)=='PATH') level = 0;
        }
        else if(action.indexOf('SET_PROPERTY_')==0)
        {
            level = 'P'+parseInt(action.substr(13));
        }
        return level;
    },

    RemoveLineActions: function(input)
    {
        var arKeys = input.name.substr(0, input.name.length - 1).split('][');
        var action = arKeys[arKeys.length - 1];
        var k2 = arKeys[arKeys.length - 2];
        if(action.indexOf('SET_SECTION_')==0 || action.indexOf('SET_PROPERTY_')==0)
        {
            var level = this.GetActionLevel(action);
            var labels = $(input).closest('.kda-ie-tbl').find('.slevel[data-level='+level+']');
            var trs = labels.closest('tr');
            $('td.section_name_cell:eq(0)', trs).removeClass('section_name_cell');
            /*var td = $('td.section_name_cell:eq(0)', trs);
			if(td.length > 0)
			{
				this.SetCurrentCell(td[0]);
				this.UnsetSectionNameCell();
			}*/

            $('td:gt(0)', trs).unbind('contextmenu');
            labels.remove();
        }
        else if(action.indexOf('SET_TITLES')==0 || action.indexOf('SET_HINTS')==0)
        {
            var levelType = (action.indexOf('SET_TITLES')==0 ? 'headers' : 'hints');
            var labels = $(input).closest('.kda-ie-tbl').find('.slevel[data-level-type="'+levelType+'"]');
            var trs = labels.closest('tr');
            trs.removeClass('kda-ie-tbl-'+levelType);
            trs.find('>td input[name^="SETTINGS[TITLES_LIST]["]').remove();
            labels.remove();
        }
        $(input).remove();
    },

    SetLineAction: function(action, k1, k2)
    {
        var inputLine = $('.kda-ie-tbl input[name="SETTINGS[IMPORT_LINE]['+k1+']['+k2+']"]:eq(0)');

        if(action == 'REMOVE_ACTION')
        {
            var levelObj = $(inputLine).closest('td').find('.slevel');
            if(levelObj.attr('data-level-type')=='headers')
            {
                var input = $(inputLine).closest('.kda-ie-tbl').find('input[name="SETTINGS[LIST_SETTINGS]['+k1+'][SET_TITLES]"]');
                if(input.length > 0) this.RemoveLineActions(input[0]);
            }
            else if(levelObj.attr('data-level-type')=='hints')
            {
                var input = $(inputLine).closest('.kda-ie-tbl').find('input[name="SETTINGS[LIST_SETTINGS]['+k1+'][SET_HINTS]"]');
                if(input.length > 0) this.RemoveLineActions(input[0]);
            }
            else
            {
                var level = $(inputLine).closest('td').find('.slevel').attr('data-level');
                if(level.length > 0 && level=='0') level = 'PATH';
                var paramName = 'SET_SECTION_'+level;
                if(level!='PATH' && level.substr(0, 1)=='P') paramName = 'SET_PROPERTY_'+level.substr(1);
                if(level)
                {
                    var input = $(inputLine).closest('.kda-ie-tbl').find('input[name="SETTINGS[LIST_SETTINGS]['+k1+']['+paramName+']"]');
                    if(input.length > 0) this.RemoveLineActions(input[0]);
                }
            }
            return;
        }

        if(action.indexOf('SET_SECTION_')==0 || action.indexOf('SET_PROPERTY_')==0)
        {
            var tr = inputLine.closest('tr');
            var style = $('.cell_inner:not(:empty):eq(0)', tr).attr('data-style');

            var settingsBlock = inputLine.closest('.kda-ie-tbl').find('.list-settings');
            $('input[name^="SETTINGS[LIST_SETTINGS]['+k1+'][SET_SECTION_"], input[name^="SETTINGS[LIST_SETTINGS]['+k1+'][SET_PROPERTY_"]', settingsBlock).each(function(){
                if(this.value == style)
                {
                    EList.RemoveLineActions(this);
                }
            });

            var inputName = 'SETTINGS[LIST_SETTINGS]['+k1+']['+action+']';
            var input = $('input[name="'+inputName+'"]', settingsBlock);
            if(input.length == 0)
            {
                settingsBlock.append('<input type="hidden" name="'+inputName+'" value="">');
                input = $('input[name="'+inputName+'"]', settingsBlock);
            }
            input.val(style);
            this.ShowLineActions(input[0]);
        }

        if(action.indexOf('SET_TITLES')==0 || action.indexOf('SET_HINTS')==0)
        {
            var input = $(inputLine).closest('.kda-ie-tbl').find('input[name="SETTINGS[LIST_SETTINGS]['+k1+']['+action+']"]');
            if(input.length > 0) this.RemoveLineActions(input[0]);

            var settingsBlock = inputLine.closest('.kda-ie-tbl').find('.list-settings');
            var inputName = 'SETTINGS[LIST_SETTINGS]['+k1+']['+action+']';
            var input = $('input[name="'+inputName+'"]', settingsBlock);
            if(input.length == 0)
            {
                settingsBlock.append('<input type="hidden" name="'+inputName+'" value="">');
                input = $('input[name="'+inputName+'"]', settingsBlock);
            }
            input.val(k2);
            this.ShowLineActions(input[0]);
            this.SetFieldAutoInsert(inputLine.closest('table').find('tr:first'), true)
        }
    },

    SetCurrentCell: function(td)
    {
        this.currentCell = td;
    },

    GetCurrentCell: function(td)
    {
        return this.currentCell;
    },

    ChooseSectionNameCell: function()
    {
        var td = this.GetCurrentCell();
        var tr = $(td).closest('tr')[0];
        var table = $(td).closest('table');
        var tdIndex = 0;
        for(var i=0; i<tr.cells.length; i++)
        {
            if(tr.cells[i]==td) tdIndex = i;
        }
        table.find('.slevel').each(function(){
            var tr = $(this).closest('tr');
            $('td.section_name_cell', tr).removeClass('section_name_cell');
            if(tdIndex > 0) $('td:eq('+tdIndex+')', tr).addClass('section_name_cell');
        });


        var inputName2 = $(tr).find('input[name^="SETTINGS[IMPORT_LINE]"]:eq(0)').attr('name');
        var arKeys = inputName2.substr(22, inputName2.length - 23).split('][');
        var k1 = arKeys[0];

        var settingsBlock = $(td).closest('.kda-ie-tbl').find('.list-settings');
        var inputName = 'SETTINGS[LIST_SETTINGS]['+k1+'][SECTION_NAME_CELL]';

        var input = $('input[name="'+inputName+'"]', settingsBlock);
        if(input.length == 0)
        {
            settingsBlock.append('<input type="hidden" name="'+inputName+'" value="">');
            input = $('input[name="'+inputName+'"]', settingsBlock);
        }
        input.val(tdIndex);
    },

    UnsetSectionNameCell: function()
    {
        var td = this.GetCurrentCell();
        var tr = $(td).closest('tr')[0];
        var table = $(td).closest('table');
        var tdIndex = 0;
        for(var i=0; i<tr.cells.length; i++)
        {
            if(tr.cells[i]==td) tdIndex = i;
        }

        var inputName2 = $(tr).find('input[name^="SETTINGS[IMPORT_LINE]"]:eq(0)').attr('name');
        var arKeys = inputName2.substr(22, inputName2.length - 23).split('][');
        var k1 = arKeys[0];
        var settingsBlock = $(td).closest('.kda-ie-tbl').find('.list-settings');
        var inputName = 'SETTINGS[LIST_SETTINGS]['+k1+'][SECTION_NAME_CELL]';
        var input = $('input[name="'+inputName+'"]', settingsBlock);
        if(input.length == 0 || input.val()!=tdIndex) return;

        table.find('.slevel').each(function(){
            var tr = $(this).closest('tr');
            $('td.section_name_cell', tr).removeClass('section_name_cell');
        });
        input.remove();
    },

    ShowSectionFieldSettings: function(level)
    {
        var td = this.GetCurrentCell();
        var listIndex = $(td).closest('.kda-ie-tbl').attr('data-list-index');
        var tr = $(td).closest('tr')[0];
        var table = $(td).closest('table')[0];

        var linkId = 'field_settings_'+listIndex+'___'+level;
        if($('#'+linkId).length==0)
        {
            var settingsBlock = $(td).closest('.kda-ie-tbl').find('.list-settings');
            settingsBlock.append('<a href="javascript:void(0)" id="'+linkId+'" onclick="EList.ShowFieldSettings(this);"><input name="EXTRASETTINGS['+listIndex+'][__'+level+']" value="" type="hidden"></a>');
        }
        var field = (level > 0 ? 'SECTION_SEP_NAME' : 'SECTION_SEP_NAME_PATH');
        if(level.substr(0, 1)=='P') field = 'IP_PROP'+level.substr(1);
        this.ShowFieldSettings($('#'+linkId)[0], 'SETTINGS[FIELDS_LIST]['+listIndex+'][__'+level+']', field);

        /*var td = this.GetCurrentCell();
		var tr = $(td).closest('tr')[0];
		var table = $(td).closest('table')[0];
		var tdIndex = 0;
		for(var i=0; i<tr.cells.length; i++)
		{
			if(tr.cells[i]==td) tdIndex = i;
		}
		var listIndex = $(td).closest('.kda-ie-tbl').attr('data-list-index');
		var btn = $('tr:first td:eq('+tdIndex+') .field_settings:eq(0)', table);
		if(btn.length > 0)
		{
			this.ShowFieldSettings(btn[0], 'SETTINGS[FIELDS_LIST]['+listIndex+'][SECTION_'+(tdIndex-1)+']', 'SECTION_SEP_NAME');
		}*/
    },

    SetWidthList: function()
    {
        $('.kda-ie-tbl:not(.empty) div.set').each(function(){
            var div = $(this);
            div.css('width', 0);
            div.prev('.set_scroll').css('width', 0);
            var timer = setInterval(function(){
                var width = div.parent().width();
                if(width > 0)
                {
                    div.css('width', width);
                    div.prev('.set_scroll').css('width', width).find('>div').css('width', div.find('>table.list').width());
                    clearInterval(timer);
                }
            }, 100);
            setTimeout(function(){clearInterval(timer);}, 3000);
        });
    },

    ToggleSettings: function(btn)
    {
        var table = $(btn).closest('.kda-ie-tbl');
        var tr = table.find('tr.settings');
        if(tr.is(':visible'))
        {
            tr.hide();
            $(btn).removeClass('open');
        }
        else
        {
            if(!table.attr('data-fields-init'))
            {
                this.SetFieldValues(table);
                table.attr('data-fields-init', 1);

                var list = table.attr('data-list-index');
                var hecInput = $('input[name="SETTINGS[LIST_SETTINGS]['+list+'][HIDE_EMPTY_COLUMNS]"]', table);
                if(hecInput.val()=='1')
                {
                    this.HideEmptyColumns(list);
                }
            }

            tr.show();
            $(btn).addClass('open');
        }
        $(window).trigger('resize');
    },

    ShowFull: function(btn)
    {
        var tbl = $(btn).closest('.kda-ie-tbl');
        var list = tbl.attr('data-list-index');
        var colCount = Math.max(1, $('table.list tr:eq(0) > td', tbl).length - 1);
        var post = $(btn).closest('form').serialize() + '&ACTION=SHOW_FULL_LIST&LIST_NUMBER=' + list + '&COUNT_COLUMNS=' + colCount;
        var wait = BX.showWait();
        $.post(window.location.href, post, function(data){
            data = $(data);
            var chb = $('input[type=checkbox][name^="SETTINGS[CHECK_ALL]"]', tbl);
            /*if(chb.length > 0)
			{
				if(chb[0].checked)
				{
					data.find('input[type=checkbox]').prop('checked', true);
				}
				else
				{
					data.find('input[type=checkbox]').prop('checked', false);
				}
			}*/
            $('table.list', tbl).append(data);
            /*$('table.list input[type=checkbox]', tbl).each(function(){
				BX.adminFormTools.modifyCheckbox(this);
			});*/
            EList.InitLines(list);
            $(window).trigger('resize');
            BX.closeWait(null, wait);
        });
        $(btn).hide();
    },

    ApplyToAllLists: function(link)
    {
        var tbl = $(link).closest('.kda-ie-tbl');
        var tbls = tbl.parent().find('.kda-ie-tbl').not(tbl);
        var form = tbl.closest('form')[0];

        /*var post = {
			'MODE': 'AJAX',
			'ACTION': 'APPLY_TO_LISTS',
			'PROFILE_ID': form.PROFILE_ID.value,
			'LIST_FROM': tbl.attr('data-list-index')
		}
		post.LIST_TO = [];
		for(var i=0; i<tbls.length; i++)
		{
			post.LIST_TO.push($(tbls[i]).attr('data-list-index'));
		}
		$.post(window.location.href, post, function(data){});*/

        var ts = tbl.find('.kda-ie-field-select');
        var cha = tbl.find('input[type="checkbox"][name^="SETTINGS[CHECK_ALL]"]');
        var chl = tbl.find('input[type="checkbox"][name^="SETTINGS[IMPORT_LINE]"]');
        for(var i=0; i<tbls.length; i++)
        {
            var tss = $('.kda-ie-field-select', tbls[i]);
            for(var j=0; j<ts.length; j++)
            {
                if(!tss[j]) continue;
                /*var c1 = $('input.fieldval', ts[j]).length;
				var c2 = $('input.fieldval', tss[j]).length;*/
                var c1 = $('span.fieldval', ts[j]).length;
                var c2 = $('span.fieldval', tss[j]).length;
                if(c2 < c1)
                {
                    for(var k=0; k<c1-c2; k++)
                    {
                        $('.kda-ie-add-load-field', tss[j]).trigger('click');
                    }
                }
                else if(c2 > c1)
                {
                    for(var k=0; k<c2-c1; k++)
                    {
                        $('.field_delete:last', tss[j]).trigger('click');
                    }
                }

                var fts = $('input[name^="SETTINGS[FIELDS_LIST]"]', ts[j]);
                //var fts2 = $('input[name^="FIELDS_LIST_SHOW"]', ts[j]);
                var fts2 = $('span.fieldval', ts[j]);
                var fts2s = $('a.field_settings', ts[j]);
                var fts2si = $('input[name^="EXTRASETTINGS"]', fts2s);
                var ftss = $('input[name^="SETTINGS[FIELDS_LIST]"]', tss[j]);
                //var ftss2 = $('input[name^="FIELDS_LIST_SHOW"]', tss[j]);
                var ftss2 = $('span.fieldval', tss[j]);
                var ftss2s = $('a.field_settings', tss[j]);
                var ftss2si = $('input[name^="EXTRASETTINGS"]', ftss2s);
                for(var k=0; k<ftss.length; k++)
                {
                    if(fts[k])
                    {
                        //ftss[k].value = fts[k].value;
                        //ftss2[k].value = fts2[k].value;
                        EList.SetHiddenFieldVal(ftss[k], fts[k].value);
                        EList.SetShowFieldVal(ftss2[k], fts2[k].innerHTML);
                        ftss2si[k].value = fts2si[k].value;
                        if($(fts2s[k]).hasClass('inactive')) $(ftss2s[k]).addClass('inactive');
                        else $(ftss2s[k]).removeClass('inactive');
                    }
                }
            }

            var chas = $('input[type="checkbox"][name^="SETTINGS[CHECK_ALL]"]', tbls[i]);
            if(cha[0] && chas[0])
            {
                chas[0].checked = cha[0].checked;
            }
            var chls = $('input[type="checkbox"][name^="SETTINGS[IMPORT_LINE]"]', tbls[i]);
            for(var j=0; j<chl.length; j++)
            {
                if(!chls[j]) continue;
                chls[j].checked = chl[j].checked;
            }
        }
    },

    OnAfterAddNewProperty: function(fieldName, propId, propName, iblockId)
    {
        //var field = $('input[name="'+fieldName+'"]');
        var field = $('#'+fieldName);
        var form = field.closest('form')[0];
        var post = {
            'MODE': 'AJAX',
            'ACTION': 'GET_SECTION_LIST',
            'IBLOCK_ID': iblockId,
            'PROFILE_ID': form.PROFILE_ID.value
        }
        var ptable = $(field).closest('.kda-ie-tbl');
        $.post(window.location.href, post, function(data){
            ptable.find('select[name^="FIELDS_LIST["]').each(function(){
                var fields = $(data).find('select[name=fields]');
                fields.attr('name', this.name);
                $(this).replaceWith(fields);
            });
        });
        //field.val(propName);
        EList.SetHiddenFieldVal(EList.GetValInputFromShowInput(field[0]), propId);
        EList.SetShowFieldVal(field, propName);
        /*$('input[name="'+fieldName.replace('FIELDS_LIST_SHOW', 'SETTINGS[FIELDS_LIST]')+'"]', ptable).val(propId);
		var input = EList.GetValInputFromShowInput(field[0]);
		$(input).val(propId);*/

        BX.WindowManager.Get().Close();

        var td = field.closest('td');
        td.addClass('kda-ie-field-select-highligth');
        setTimeout(function(){
            td.removeClass('kda-ie-field-select-highligth');
        }, 2000);
    },

    ChooseIblock: function(select)
    {
        var form = $(select).closest('form')[0];
        var ptable = $(select).closest('.kda-ie-tbl');
        var post = {
            'MODE': 'AJAX',
            'ACTION': 'GET_SECTION_LIST',
            'IBLOCK_ID': select.value,
            'PROFILE_ID': form.PROFILE_ID.value,
            'LIST_INDEX': ptable.attr('data-list-index')
        }
        $.post(window.location.href, post, function(data){
            var sections = $(data).find('select[name=sections]');
            var sectSelect = $(select).closest('table').find('select[name="'+select.name.replace('[IBLOCK_ID]', '[SECTION_ID]')+'"]');
            sections.attr('name', sectSelect.attr('name'));
            if(typeof sectSelect.chosen == 'function') sectSelect.chosen('destroy');
            sectSelect.replaceWith(sections);
            if(typeof sections.chosen == 'function') sections.chosen({search_contains: true});

            ptable.find('select[name^="FIELDS_LIST["]').each(function(){
                var fields = $(data).find('select[name=fields]');
                fields.attr('name', this.name);
                $(this).replaceWith(fields);
                EList.SetFieldValues(ptable, true);
            });

            var setDiv = $('.addsettings', ptable);
            var searchSection = $(data).find('select[name=search_sections]');
            var searchSectionSelect = setDiv.find('select[name^="SETTINGS[SEARCH_SECTIONS]["]');
            searchSection.attr('name', searchSectionSelect.attr('name'));
            if(typeof searchSectionSelect.chosen == 'function') searchSectionSelect.chosen('destroy');
            searchSectionSelect.replaceWith(searchSection);
            if(typeof searchSection.chosen == 'function') searchSection.chosen({search_contains: true, placeholder_text: BX.message("KDA_IE_SELECT_NOT_CHOSEN")});

            var uid = $(data).find('select[name=element_uid]');
            var uidSelect = setDiv.find('select[name^="SETTINGS[LIST_ELEMENT_UID]["]');
            uid.attr('name', uidSelect.attr('name'));
            if(typeof uidSelect.chosen == 'function') uidSelect.chosen('destroy');
            uidSelect.replaceWith(uid);
            if(typeof uid.chosen == 'function') uid.chosen({search_contains: true, placeholder_text: BX.message("KDA_IE_SELECT_NOT_CHOSEN")});

            var needShow = (setDiv.find('input[name^="SETTINGS[CHANGE_ELEMENT_UID]["]:checked').length > 0);
            var uidSku = $(data).find('select[name=element_uid_sku]');
            var uidSkuSelect = setDiv.find('select[name^="SETTINGS[LIST_ELEMENT_UID_SKU]["]');
            uidSku.attr('name', uidSkuSelect.attr('name'));
            if(typeof uidSkuSelect.chosen == 'function') uidSkuSelect.chosen('destroy');
            uidSkuSelect.replaceWith(uidSku);
            if(typeof uidSku.chosen == 'function') uidSku.chosen({search_contains: true, placeholder_text: BX.message("KDA_IE_SELECT_NOT_CHOSEN")});
            uidSku.closest('tr').css('display', ((needShow && $('option', uidSku).length > 0) ? '' : 'none'));

            ptable.find('table.list tbody, table.list tfoot').show();
            ptable.attr('data-iblock-id', select.value);
        });
    },

    OnChangeFieldHandler: function(select)
    {
        var val = select.value;
        var link = $(select).next('a.field_settings');
        /*if(val.indexOf("ICAT_PRICE")===0 || val=="ICAT_PURCHASING_PRICE")
		{
			link.removeClass('inactive');
		}
		else
		{
			link.addClass('inactive');
		}*/
    },

    AddUploadField: function(link)
    {
        var parent = $(link).closest('.kda-ie-field-select-btns');
        var div = parent.prev('div').clone();
        var input = $('input[name^="SETTINGS[FIELDS_LIST]"]', div)[0];
        //var inputShow = $('input[name^="FIELDS_LIST_SHOW"]', div)[0];
        var inputShow = $('span.fieldval', div)[0];
        var a = $('a.field_settings', div)[0];
        var inputExtra = $('input', a)[0];
        $('.field_insert', div).remove();

        var sname = input.name;
        var index = sname.substr(0, sname.length-1).split('][').pop();
        var arIndex = index.split('_');
        if(arIndex.length==1) arIndex[1] = 1;
        else arIndex[1] = parseInt(arIndex[1]) + 1;

        input.name = input.name.replace(/\[[\d_]+\]$/, '['+arIndex.join('_')+']');
        //inputShow.name = input.name.replace('SETTINGS[FIELDS_LIST]', 'FIELDS_LIST_SHOW');
        inputShow.id = this.GetShowInputNameFromValInputName(input.name);
        if(arIndex[1] > 1) a.id = a.id.replace(/\_\d+_\d+$/, '_'+arIndex.join('_'));
        else a.id = a.id.replace(/\_\d+$/, '_'+arIndex.join('_'));
        $(a).addClass('inactive');
        inputExtra.name = inputExtra.name.replace(/\[[\d_]+\]$/, '['+arIndex.join('_')+']');
        inputExtra.value = '';

        div.insertBefore(parent);
        EList.OnFieldFocus(inputShow);
    },

    DeleteUploadField: function(link)
    {
        var parent = $(link).closest('div');
        EList.SetHiddenFieldVal($('input[name^="SETTINGS[FIELDS_LIST]"]', parent), '');
        parent.remove();
    },

    ShowFieldSettings: function(btn, name, val)
    {
        //if($(btn).hasClass('inactive')) return;
        if(!name || !val)
        {
            var input = $(btn).prevAll('input[name^="SETTINGS[FIELDS_LIST]"]');
            val = input.val();
            name = input[0].name;
        }
        //var input2 = $(btn).prevAll('input[name^="FIELDS_LIST_SHOW["]');
        var input2 = $(btn).closest('div').find('span.fieldval');
        var input2Val = input2.html();
        if(input2Val==input2.attr('placeholder')) input2Val = '';
        var ptable = $(btn).closest('.kda-ie-tbl');
        var form = $(btn).closest('form')[0];
        var countCols = $(btn).closest('tr').find('.kda-ie-field-select').length;

        var dialogParams = {
            //'title':BX.message("KDA_IE_POPUP_FIELD_SETTINGS_TITLE") + (input2.val() ? ' "'+input2.val()+'"' : ''),
            'title':BX.message("KDA_IE_POPUP_FIELD_SETTINGS_TITLE") + (input2Val ? ' "'+input2Val+'"' : ''),
            'content_url':'/bitrix/admin/'+kdaIEModuleFilePrefix+'_field_settings.php?field='+val+'&field_name='+name+'&IBLOCK_ID='+ptable.attr('data-iblock-id')+'&PROFILE_ID='+form.PROFILE_ID.value+'&count_cols='+countCols,
            'width':'900',
            'height':'400',
            'resizable':true
        };
        if($('input', btn).length > 0)
        {
            dialogParams['content_url'] += '&return_data=1';
            dialogParams['content_post'] = {'POSTEXTRA': $('input', btn).val()};
        }
        var dialog = new BX.CAdminDialog(dialogParams);

        dialog.SetButtons([
            dialog.btnCancel,
            new BX.CWindowButton(
                {
                    title: BX.message('JS_CORE_WINDOW_SAVE'),
                    id: 'savebtn',
                    name: 'savebtn',
                    className: BX.browser.IsIE() && BX.browser.IsDoctype() && !BX.browser.IsIE10() ? '' : 'adm-btn-save',
                    action: function () {
                        this.disableUntilError();
                        this.parentWindow.PostParameters();
                        //this.parentWindow.Close();
                    }
                })/*,
			dialog.btnSave*/
        ]);

        BX.addCustomEvent(dialog, 'onWindowRegister', function(){
            $('input[type=checkbox]', this.DIV).each(function(){
                BX.adminFormTools.modifyCheckbox(this);
            });
            ESettings.BindConversionEvents();
        });

        dialog.Show();
    },

    ShowListSettings: function(btn)
    {
        var tbl = $(btn).closest('.kda-ie-tbl');
        var post = 'list_index='+tbl.attr('data-list-index');
        var inputs = tbl.find('input[name^="SETTINGS[FIELDS_LIST]"], select[name^="SETTINGS[IBLOCK_ID]"], select[name^="SETTINGS[SECTION_ID]"], input[name^="SETTINGS[ADDITIONAL_SETTINGS]"]');
        for(var i in inputs)
        {
            post += '&'+inputs[i].name+'='+inputs[i].value;
        }

        var abtns = tbl.find('a.field_insert');
        var findFields = [];
        for(var i=0; i<abtns.length; i++)
        {
            findFields.push('FIND_FIELDS[]='+$(abtns[i]).attr('data-value'));
        }
        if(findFields.length > 0)
        {
            post += '&'+findFields.join('&');
        }

        var dialog = new BX.CAdminDialog({
            'title':BX.message("KDA_IE_POPUP_LIST_SETTINGS_TITLE"),
            'content_url':'/bitrix/admin/'+kdaIEModuleFilePrefix+'_list_settings.php',
            'content_post': post,
            'width':'900',
            'height':'400',
            'resizable':true});

        dialog.SetButtons([
            dialog.btnCancel,
            new BX.CWindowButton(
                {
                    title: BX.message('JS_CORE_WINDOW_SAVE'),
                    id: 'savebtn',
                    name: 'savebtn',
                    className: BX.browser.IsIE() && BX.browser.IsDoctype() && !BX.browser.IsIE10() ? '' : 'adm-btn-save',
                    action: function () {
                        this.disableUntilError();
                        this.parentWindow.PostParameters();
                        //this.parentWindow.Close();
                    }
                })/*,
			dialog.btnSave*/
        ]);

        BX.addCustomEvent(dialog, 'onWindowRegister', function(){
            $('input[type=checkbox]', this.DIV).each(function(){
                BX.adminFormTools.modifyCheckbox(this);
            });
            if(typeof $('select.kda-chosen-multi').chosen == 'function')
            {
                $('select.kda-chosen-multi').chosen();
            }
        });

        dialog.Show();
    },

    ChooseColumnForMove: function(btn, e)
    {
        var obj = this;
        var parent = $(btn).closest('.kda-ie-field-select');
        var offset = parent.offset();
        var btnsInnerParent = parent.find('.kda-ie-field-select-btns-inner');
        var width = parent.width() + 4;
        $('body').append('<div id="kda-ie-move-column-abs"></div>');
        $('#kda-ie-move-column-abs').css({
            width: width,
            height: parent.height() + btnsInnerParent.height() + 8,
            top: offset.top,
            left: offset.left
        });

        var tr = parent.closest('tr');
        var tds = tr.find('.kda-ie-field-select');
        this.moveColumnsX = [];
        for(var i=0; i<tds.length; i++)
        {
            this.moveColumnsX.push(Math.round($(tds[i]).offset().left));
            if(parent[0]==tds[i])
            {
                this.moveCurrentIndex = i;
            }
        }

        this.moveCorrectX = e.pageX - offset.left;
        this.moveCorrectY = e.pageY - offset.top;
        this.moveWidth = width;
        this.moveTds = tds;
        this.movebeginX = offset.left;
        this.moveNewIndex = -1;

        $(window).bind('mousemove', function(e){
            return obj.ColumnProccessMove(e);
        });

        $(window).bind('mouseup', function(){
            obj.ColumnMoveEnd();
        });

        e.stopPropagation();
        return false;
    },

    ColumnProccessMove: function(e)
    {
        if(!document.getElementById('kda-ie-move-column-abs')) return;
        var moveObj = $('#kda-ie-move-column-abs');
        var left = e.pageX - this.moveCorrectX;
        var right = left + this.moveWidth;
        moveObj.css({
            top: e.pageY - this.moveCorrectY,
            left: left
        });

        var currentPosition = -1;
        for(var i=0; i<this.moveColumnsX.length; i++)
        {
            if(left < this.moveColumnsX[i])
            {
                if(i == this.moveCurrentIndex) break;

                var curTd = this.moveTds.eq(i);
                if(curTd.hasClass('kda-ie-moved-to')) return;
                var otherTds = this.moveTds.not(':eq('+i+')');
                otherTds.removeClass('kda-ie-moved-to');
                curTd.addClass('kda-ie-moved-to');
                currentPosition = i;
                break;
            }
        }

        if(currentPosition < 0)
        {
            this.moveNewIndex = -1;
            this.moveTds.removeClass('kda-ie-moved-to');
        }
        else
        {
            this.moveNewIndex = currentPosition;
        }

        e.stopPropagation();
        return false;
    },

    ColumnMoveEnd: function()
    {
        if(!document.getElementById('kda-ie-move-column-abs')) return;
        $('#kda-ie-move-column-abs').remove();
        if(this.moveNewIndex >= 0)
        {
            this.moveTds.removeClass('kda-ie-moved-to');

            var td1 = this.moveTds.eq(this.moveCurrentIndex);
            var td2 = this.moveTds.eq(this.moveNewIndex);
            var content1 = $('>div', td1);
            var content2 = $('>div', td2);
            content1.appendTo(td2);
            content2.appendTo(td1);

            var tr = td1.closest('tr');
            this.OnAfterColumnsMove(tr);
        }
    },

    ColumnsMoveLeft: function(btn)
    {
        var obj = this;
        var parent = $(btn).closest('.kda-ie-field-select');
        var tr = parent.closest('tr');
        var tds = tr.find('.kda-ie-field-select');
        var find = false;
        var content1 = false;
        for(var i=0; i<tds.length; i++)
        {
            if(parent[0]==tds[i])
            {
                find = true;
                var content1 = $('>div', tds[(i > 0 ? i-1 : i)]);
            }
            if(find && i > 0)
            {
                $('>div', tds[i]).appendTo(tds[i-1]);
            }
        }
        if(content1) content1.appendTo(tds[tds.length - 1]);
        this.ClearColumnField(tds[tds.length - 1]);
        this.OnAfterColumnsMove(tr);
    },

    ColumnsMoveRight: function(btn)
    {
        var obj = this;
        var parent = $(btn).closest('.kda-ie-field-select');
        var tr = parent.closest('tr');
        var tds = tr.find('.kda-ie-field-select');
        var content1 = $('>div', tds[tds.length-1]);
        for(var i=tds.length-1; i>=0; i--)
        {
            if(i < tds.length-1)
            {
                $('>div', tds[i]).appendTo(tds[i+1]);
            }
            if(parent[0]==tds[i])
            {
                break;
            }
        }
        content1.appendTo(tds[i]);
        this.ClearColumnField(tds[i]);
        this.OnAfterColumnsMove(tr);
    },

    ClearColumnField: function(td)
    {
        $('>div:not(.kda-ie-field-select-btns):gt(0)', td).remove();
        $('>div:not(.kda-ie-field-select-btns)', td).each(function(){
            //$('input[name^="SETTINGS[FIELDS_LIST]"]', this).val('');
            //$('input[name^="FIELDS_LIST_SHOW"]', this).val('').attr('title', '');
            EList.SetHiddenFieldVal($('input[name^="SETTINGS[FIELDS_LIST]"]', this), '');
            EList.SetShowFieldVal($('span.fieldval', this), '');
            $('.field_settings', this).addClass('inactive');
            $('.field_settings input[name^="EXTRASETTINGS"]', this).val('');
        });
    },

    OnAfterColumnsMove: function(tr)
    {
        tr.find('.kda-ie-field-select').each(function(index){
            $(this).find('input').each(function(){
                if(this.name.substr(this.name.length - 1)==']')
                {
                    this.name = this.name.replace(/\[\d+((_\d+)?)\]$/, '['+index+'$1]');
                }
            });
            $(this).find('span.fieldval').each(function(){
                this.id = this.id.replace(/(field\-list\-show\-\d+\-)\d+((_\d+)?)/, '$1'+index+'$2');
            });
            $(this).find('.field_settings').each(function(){
                this.id = this.id.replace(/(field_settings_\d+_)\d+((_\d+)?)/, '$1'+index+'$2');
            });
        });
        this.SetFieldAutoInsert(tr);
    },

    SetExtraParams: function(oid, returnJson)
    {
        if(typeof returnJson == 'object') returnJson = JSON.stringify(returnJson);
        if(returnJson.length > 0) $('#'+oid).removeClass("inactive");
        else $('#'+oid).addClass("inactive");
        $('#'+oid+' input').val(returnJson);
        if(BX.WindowManager.Get()) BX.WindowManager.Get().Close();
    },

    ToggleAddSettings: function(input)
    {
        var display = (input.checked ? '' : 'none');
        var tr = $(input).closest('tr');
        var next;
        while((next = tr.next('tr.subfield')) && next.length > 0)
        {
            tr = next;
            if(display=='' && $('select', tr).length > 0 && $('select option', tr).length==0) continue;
            tr.css('display', display);
        }
    },

    ToggleAddSettingsBlock: function(link)
    {
        if($(link).hasClass('open'))
        {
            $(link).removeClass('open');
            $(link).next('div').slideUp();
        }
        else
        {
            $(link).addClass('open');
            $(link).next('div').slideDown();
        }
    }
}
