<?
/**
 * Delivery services for СДЭК
 * https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&LESSON_ID=8415
 * https://bxapi.ru/src/?module_id=sale&name=Configurable::getConfigStructure
 * https://mrcappuccino.ru/blog/post/delivery-handler-for-new-bitrix-sale-module
 */

namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Delivery\CalculationResult,
    Bitrix\Sale\Delivery\Services\Base,
    Bitrix\Main\Localization\Loc;


class CDeliveryOrmandoSdek extends Base
{
    public static function getClassTitle()
    {
        return Loc::getMessage("DELIVERY_SDEK_TITLE");
    }

    public static function getClassDescription()
    {
        return Loc::getMessage("DELIVERY_SDEK_DESCRIPTION");
    }

    protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment)
    {
        $result = new CalculationResult();
        $price = floatval($this->config["MAIN"]["PRICE"]);


        #if($shipment->getWeight() <= 0)
        #    $shipment->setWeight(1000);

        #$weight = floatval($shipment->getWeight()) / 1000;

        $result->setDeliveryPrice(roundEx($price, 2));
        $result->setPeriodDescription('1 день');

        return $result;
    }


    /**
     * @return array
     * More types here bitrix/modules/sale/handlers/delivery/spsr/handler.php
     */
    protected function getConfigStructure()
    {
        return [
            "MAIN" => [
                "TITLE" => Loc::getMessage("DELIVERY_SDEK_MAIN_TITLE"),
                "DESCRIPTION" => Loc::getMessage("DELIVERY_SDEK_MAIN_DESCRIPTION"),
                "ITEMS" => [
                    "PRICE" => [
                        "TYPE" => "NUMBER",
                        "MIN" => 0,
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_PRICE")
                    ],
                    "EMAIL" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_EMAIL")
                    ],
                    "NAME" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_NAME")
                    ],
                    "NAME_2" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_NAME_2")
                    ],
                    "NAME_3" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_NAME_3")
                    ],
                    "PHONE" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_PHONE")
                    ],
                    "PHONE_COUNTRY_CODE" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_PHONE_COUNTRY_CODE")
                    ],
                    "COUNTRY_code1" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_COUNTRY_code1")
                    ],
                    "COUNTRY_code2" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_COUNTRY_code2")
                    ],
                    "COUNTRY_customsCode" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_COUNTRY_customsCode")
                    ],
                    "COUNTRY_name" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_COUNTRY_name")
                    ],
                    "COUNTRY_name2" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_COUNTRY_name2")
                    ],
                    "COUNTRY_phoneCode" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_COUNTRY_phoneCode")
                    ],
                    "SDEK_LOGIN" => [
                        "TYPE" => "STRING",
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_LOGIN")
                    ],
                    "SDEK_SIGNATURE" => [
                        "TYPE" => "STRING",
                        //"READONLY" => true,
                        "NAME" => Loc::getMessage("DELIVERY_SDEK_SIGNATURE")
                    ]
                ]
            ]
        ];
    }

    public function isCalculatePriceImmediately()
    {
        return true;
    }

    public static function whetherAdminExtraServicesShow()
    {
        return true;
    }
}