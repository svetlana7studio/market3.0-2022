<?php
return array(
	"controllers" => array(
		"value" => array(
			"namespaces" => array(
				"\\Studio7spb\\Marketplace\\Controller" => "api",
			),
		),
		"readonly" => true,
	)
);
?>