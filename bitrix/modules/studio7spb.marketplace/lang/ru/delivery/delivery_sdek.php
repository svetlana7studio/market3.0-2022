<?
$MESS = [
    "DELIVERY_SDEK_TITLE" => "Доставка Ormando СДЭК",
    "DELIVERY_SDEK_MAIN_TITLE" => "Настройка данных отправителя СДЭК",
    "DELIVERY_SDEK_MAIN_DESCRIPTION" => "<a href='settings.php?mid=sale&lang=ru&back_url_settings=&tabControl_active_tab=edit1' target='_blank'>Адрес магазина</a> используется в качестве адреса отправителя. Данные с пометкой должны быть заполнены <a href='/bitrix/modules/studio7spb.marketplace/delivery/Countries.xlsx''>согласно таблице country.xlsx</a>", // Сопоставление идентификаторов местоположений интернет-магазина с идентификаторами местоположений служб доставок производится на основе
    "DELIVERY_SDEK_DESCRIPTION" => "Поступающие на Ормандо заказы отправляются в СДЭК. В форме оформления заказа получаем данные по расчету стоимости доставки при вводе адреса покупателем.",
    "DELIVERY_SDEK_PRICE" => "Примерная трансграничная цена доставки",
    "DELIVERY_SDEK_EMAIL" => "Адрес электронной почты отправителя",
    "DELIVERY_SDEK_NAME" => "Имя и фамилия отправителя",
    "DELIVERY_SDEK_NAME_2" => "Имя и фамилия отправителя на разных алфавитах (необязательно)",
    "DELIVERY_SDEK_NAME_3" => "Дополнительное поле для имени и фамилии отправителя",
    "DELIVERY_SDEK_PHONE" => "Номер телефона отправителя",
    "DELIVERY_SDEK_PHONE_COUNTRY_CODE" => "Код страны телефона (Заполненяется согласно таблице country.xlsx)",
    "DELIVERY_SDEK_COUNTRY_code1" => "Страна отправителя code1 (Заполненяется согласно таблице country.xlsx)",
    "DELIVERY_SDEK_COUNTRY_code2" => "Страна отправителя code2 (Заполненяется согласно таблице country.xlsx)",
    "DELIVERY_SDEK_COUNTRY_customsCode" => "Страна отправителя customsCode (Заполненяется согласно таблице country.xlsx)",
    "DELIVERY_SDEK_COUNTRY_name" => "Страна отправителя name (Заполненяется согласно таблице country.xlsx)",
    "DELIVERY_SDEK_COUNTRY_name2" => "Страна отправителя name2 (Заполненяется согласно таблице country.xlsx)",
    "DELIVERY_SDEK_COUNTRY_phoneCode" => "Страна отправителя phoneCode (Заполненяется согласно таблице country.xlsx)",
    "DELIVERY_SDEK_LOGIN" => "Логин СДЭК (userName)",
    "DELIVERY_SDEK_SIGNATURE" => "Цифровая подпись СДЭК (signature)",
];