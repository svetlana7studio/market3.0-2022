<?
$MESS = [
    "INDEXING_ACCESS_DENIED" => "Доступ запрещен",
    "INDEXING_TITLE" => "Индексирование Брендов каталога",
    "TRANSFER_PRODUCT" => "Проиндексирован бренд [#ID#] #NAME#",
    "TRANSFER_CURRENT_ACTION" => "Текущее действие",
    "TRANSFER_PLAYER" => "Проигрыватель",
    "TRANSFER_PLAYER_DESCRIPTION" => "Данный Проигрыватель позволяет проиндексировать все бренды для оптимальной работы базы данных публичной части. Эта индескация фактически актулизирует двнные, чем чаще будет проигрываться данный плеер тем актульнее и оптимальнее будет ситуация в разделе Брендов",
    "TRANSFER_PLAYER_START" => "Play",
    "TRANSFER_PLAYER_STOP" => "Stop"
];