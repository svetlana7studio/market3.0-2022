<?
$MESS = [
    "IMPORT_PRICE_SETTINGS_TITLE" => "Настройки импорта цен",
    "IMPORT_PRICE_SETTINGS_FILE" => "Файл для загрузки",
    "IMPORT_PRICE_SETTINGS_FILE_UPLOAD" => "Файл загружен",
    "IMPORT_PRICE_SETTINGS_FILE_UPLOAD_UNPAKING_TITLE" => "Шаг 1. Файл загружен",
    "IMPORT_PRICE_SETTINGS_FILE_UPLOAD_UNPAKING" => "Процесса распаковки и подготовка данных.",
    "IMPORT_PRICE_SETTINGS_FILE_UPLOAD_PREPARE_TITLE" => "Шаг 2. Файл распакован",
    "IMPORT_PRICE_SETTINGS_FILE_UPLOAD_PREPARE" => "И подготовлен к импорту можно переходить к загрузке цен на следующих шагах этого раздела",
    "ACCESS_DENIED" => "Доступ запрещён",
    "IMPORT_PRICE_INFO" => "Настройки с этой страницы применимы к импорту:",
    "IMPORT_PRICE_INFO_DONE" => "Можно переходить к импорту:",
    "IMPORT_PRICE_INFO_GRAY" => "Цены закупки",
    "IMPORT_PRICE_INFO_GREEN" => "Цены на сумму от 5 000 ₽",
    "IMPORT_PRICE_INFO_YELLOW" => "Цены на сумму от 10 000 ₽",
    "IMPORT_PRICE_INFO_RED" => "Цены на сумму от 50 000 ₽",
    "IMPORT_PRICE_DB_DATA_ERROR" => "На этапе загрузки файла с данными произошла ошибка. Скачайте образец файла импорта и сравните его с файлом, который вы загружаете.",
];