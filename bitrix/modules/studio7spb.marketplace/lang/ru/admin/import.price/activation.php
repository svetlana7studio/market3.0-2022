<?
$MESS = [
    "TRANSFER_TITLE" => "Актуализация наличия всего каталога",
    "TRANSFER_TAB1" => "Актуализация наличия товара",
    "TRANSFER_TITLE_VENDOR" => "Актуализация наличия товара вендора: VENDOR",
    "TRANSFER_ACCESS_DENIED" => "Доступ запрещен",
    "TRANSFER_FIELD_EMPTY" => "Не заполнено обязательное поле",
    "TRANSFER_CATALOG_ELEMENT_UPDATE" => "В каталоге обработано строк до №: ID, деактивированы товары: PRODUCTS",
    "TRANSFER_CATALOG_ELEMENT_NON_UPDATE" => "В каталоге обработано строк до №: ID, деактивированы товары: PRODUCTS",
    "TRANSFER_CATALOG_ELEMENT_DEACTIVATE" => "<span style='color: #ff5752;'>В каталоге деактивирован товар №: ID<span>",
    "TRANSFER_CATALOG_ELEMENT_CHECKED" => "<span style='color: darkolivegreen;'>В каталоге проверен товар №: ID</span>",
    "TRANSFER_ACTIVE" => "Активен",
    "TRANSFER_VENDOR_EMPTY" => "На странице настроек не выбран вендор",
    "TRANSFER_VENDOR_NOTE1" => "На этой странице происходит процесс сравнения и актуализация наличия товара вендора: VENDOR.",
    "TRANSFER_VENDOR_NOTE2" => "По окончанию процесса актуализации можно посмотреть :",
    "TRANSFER_VENDOR_NOTE3" => "Список актуальных товаров вендора <b>VENDOR</b>, которые присутствуют в загрухенном файде импорта",
    "TRANSFER_VENDOR_NOTE4" => "Список деактивированных товаров вендора <b>VENDOR</b>, которые отсутсвую в загрухенном файде импорта, но присутствуют в торновом каталоге сайта",
    "TRANSFER_VENDOR_NOTE5" => "Список актуальных товаров вендора <b>VENDOR</b> можно посмотреть на <a href='iblock_element_admin.php?IBLOCK_ID=2&type=marketplace'>странице каталога</a>, воспользовавшись фильтром",
];