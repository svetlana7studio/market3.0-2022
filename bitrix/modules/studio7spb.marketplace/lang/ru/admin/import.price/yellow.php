<?
$MESS = [
    "TRANSFER_TITLE" => "Обновление жёлтой цены",
    "TRANSFER_TITLE_VENDOR" => "Обновление жёлтой цены вендора: VENDOR",
    "TRANSFER_TAB1" => "Обновление жёлтой цены",
    "TRANSFER_FIELD_EMPTY" => "Не заполнено обязательное поле",
    "TRANSFER_CATALOG_ELEMENT_UPDATE" => "В каталоге обработано строк до №: ID, обновлёны товары: PRODUCTS",
    "TRANSFER_CATALOG_ELEMENT_NON_UPDATE" => "В каталоге обработано строк до №: ID, товаров в каталоге не найдено",
    "TRANSFER_CATALOG_ELEMENT_ADD" => "В каталог добавлен товар №: ID",
    "TRANSFER_ACTIVE" => "Активен",
    "TRANSFER_VENDOR_EMPTY" => "На странице настроек не выбран вендор",
    "TRANSFER_PRODUCT_NOT_FOUND" => "Товар не найден в каталоге",
];