<?
$MESS = [
    "TRANSFER_TITLE" => "Обновление закупочной цены",
    "TRANSFER_TITLE_VENDOR" => "Обновление закупочной цены вендора: VENDOR",
    "TRANSFER_TAB1" => "Обновление закупочной цены",
    "TRANSFER_ACCESS_DENIED" => "Доступ запрещен",
    "TRANSFER_FIELD_EMPTY" => "Не заполнено обязательное поле",
    "TRANSFER_CATALOG_ELEMENT_UPDATE" => "В каталоге обработано строк до №: ID, обновлёны товары: PRODUCTS",
    "TRANSFER_CATALOG_ELEMENT_NON_UPDATE" => "В каталоге обработано строк до №: ID, товаров в каталоге не найдено",
    "TRANSFER_CATALOG_ELEMENT_ADD" => "В каталог добавлен товар №: ID",
    "TRANSFER_ACTIVE" => "Активен",
    "TRANSFER_VENDOR_EMPTY" => "На странице настроек не выбран вендор",
    "TRANSFER_PRODUCT_NOT_FOUND" => "Товар не найден в каталоге",
];