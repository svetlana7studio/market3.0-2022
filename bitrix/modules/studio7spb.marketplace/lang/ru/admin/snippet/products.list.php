<?
$MESS = [
    "SNIPPET_TITLE" => "Наполнение сниппета товарами",
    "SNIPPET_PRODUCT_ID" => "ID",
    "SNIPPET_PRODUCT_SORT" => "Вес сортировки",
    "SNIPPET_PRODUCT_NAME" => "Название",
    "SNIPPET_PRODUCT_PREVIEW_PICTURE" => "Картинка",
    "SNIPPET_PRODUCT_SET_TO_SNIPPET" => "Добавить товары в сниппет",
    "SNIPPET_DESCRIPTION" => "В сниппет можно добавить только один или три товара",
    "SNIPPET_PRODUCT_1" => "Добавить первым продуктом в сниппет",
    "SNIPPET_PRODUCT_2" => "Добавить вторым продуктом в сниппет",
    "SNIPPET_PRODUCT_3" => "Добавить третьим продуктом в сниппет",
];