<?
$MESS = [
    "TITLE" => "Загрузка скидок",
    "TITLE_TAB" => "Загрузка файла скидок",
    "STEP_FILE_SAVE" => "Файл сохранён",
    "STEP_FILE_PREPARE" => "Подготовлено товаров: <b>COUNT</b>",
    "TITLE_FILE" => "Файл загрузки",
    "TITLE_FILE_INFO" => "Данные должны иметь формат .csv",
    "TITLE_ARTICLE_UPDATE" => "Обработан артикул ARTICLE",
    "TITLE_ID_UPDATE" => "Обработана позиция ID",
    "TITLE_ARTICLE_START" => "Анализ файла с данными",
    "TITLE_ARTICLE_DEACTIVATE" => "Операция деактивации товаров с артиклом AS-суфикс",
    "TITLE_ARTICLE_DELETE" => "Удалены отсутствующие в файле товары с артиклом AS-суфикс",
    "TITLE_ARTICLE_DELETE_MESSAGE" => "Работа мастера успешно окончена",
    "STEP_TIME_IMPORT" => "Шаг импорта",
    "STEP_TIME_DEACTIVATE" => "Шаг деактивации",
    "STEP_TIME_ARTICLE" => "Шаг обработки артикла - ",
    "STEP_TIME_SUCCESS" => "Обработка окончена",

    // delimiter
    "DELIMITER_TITLE" => "Разделитель полей",
    "DELIMITER_SEMICOLON" => "точка с запятой",
    "DELIMITER_COMMA" => "запятая",
    "DELIMITER_TAB" => "табуляция",
    "DELIMITER_SPACE" => "пробел",
];