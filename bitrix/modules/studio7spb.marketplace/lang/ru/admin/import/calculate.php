<?
$MESS = array(
    "TITLE" => "Таблица промежуточных расчетных значений",
    "LIST_HEADER_ID" => "ID",
    "LIST_HEADER_NAME" => "Название",
    "LIST_HEADER_FOB_RMB" => "FOB RMB",
    "LIST_HEADER_EXWORK_RMB" => "EXWORK RMB",
    "FORMULA" => "Формула",
    "CONDITION" => "Расчёт",
    "RESULT" => "Значение",
    "GOTO_ELEMENT" => "Перейти к єлементу",
)
?>