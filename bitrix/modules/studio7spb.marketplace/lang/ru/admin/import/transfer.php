<?
$MESS = [
    "TRANSFER_ACCESS_DENIED" => "Доступ запрещен",
    "TRANSFER_FIELD_EMPTY" => "Не заполнено обязательное поле",
    "TRANSFER_CATALOG_ELEMENT_UPDATE" => "Импортирована позиция №: ID, обработаны товары: PRODUCTS",
    "TRANSFER_CATALOG_ELEMENT_ADD" => "В каталог добавлен товар №: ID",
    "TRANSFER_CATALOG_ELEMENT_NON_UPDATE" => "В каталоге обработано строк до №: ID, товаров в каталоге не найдено",
    "TRANSFER_ACTIVE" => "Активен"
];