<?
$MESS = array(
    "ACCESS_DENIED" => "Доступ запрещен",
    "RECOUNT_TITLE" => "Пересчет цен в каталоге",
    "RECOUNT_START" => "Начать",
    "RECOUNT_STOP" => "Остановить",
    "RECOUNT_WORK_STATUS" => "Текущий статус",
    "RECOUNT_WORK_PROGRESS" => "Текущий прогресс процесса",
    "RECOUNT_WORK_TAB" => "Форма обработки",
    "RECOUNT_WORK_DATA_ERROR" => "Сбой в получении данных",
    "RECOUNT_VALIDATE_ERROR" => "Введите коректное значение поля \"новое значение курса доллара к рублю\"",
    "RECOUNT_WORK_NEW_VALUE" => "Новое значение курса доллара к рублю",
    "RECOUNT_PROCESS_WORK" => "Пересчёт цен товара ID #2634",
    "RECOUNT_PROCESS_END" => "Пересчёт цен каталога окончен",
);
