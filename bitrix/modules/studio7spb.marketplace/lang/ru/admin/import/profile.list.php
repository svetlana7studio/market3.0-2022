<?
$MESS = [
    "TITLE" => "Список профилей",
    "PROFILE_ADD" => "Добавить профиль",
    "PROFILE_EDIT" => "Редактировать профиль",
    "PROFILE_DELETE" => "Удалить профиль",
    "PROFILE_DELETE_CONFIRM" => "Подтвердить удаление профиля",
    "PROFILE_DOWNLOAD" => "Скачать",
];