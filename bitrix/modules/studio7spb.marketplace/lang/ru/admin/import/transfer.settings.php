<?
$MESS = [
    "TRANS_FILE_TITLE" => "Файл импорта (только *.csv):",
    "TRANS_FILE_DESCRIPTION" => "Данный формат (csv) используется для воизбежания различных неточностей Excel.",
    "TRANS_ERROR_FILE_FORMAT" => "Не верный формат файла", "Данный загрузчик поддерживает только файлы .csv",
    "TRANS_CHOOSE" => "Выбрать",
    "TRANS_NUMBER" => "№",
    "TRANS_CHOOSE_FIELD" => "Выбрать поле",
    "TRANS_DELETE_FIELD" => "Удалить поле",
    "TRANS_ADD_FIELD" => "Добавить поле",
    "TRANS_ADD_NEW" => "Создать новый профиль",
    "TRANS_DELETE" => "Удалить профиль",
    "TRANS_SETTINGS_FIELD" => "Настройки поля",
    "TRANS_LEFT_FIELD" => "Переместить колонку и все последующие влево",
    "TRANS_RIGHT_FIELD" => "Переместить колонку и все последующие вправо",
    "TRANS_CHOOSE_IBLOCK" => "Выбрать инфоблок",
    "TRANS_CHOOSE_IBLOCK_TYPE" => "Выбрать тип инфоблока",
    "TRANS_START_ROW" => "Задать строку, с которой начинать парсинг данных *:",
    "TRANS_START_ROW_DESC" => 'Для предпросмотра и для загрузки Листов нажмите кнопку "Загрузить настройки"',
    "TRANS_START_ROW_NULL" => 'Задать номер строки',
    "TRANS_IDENT_FIELD" => 'Выбрать поле (поля) для идентификации',
    "TRANS_IDENT_FIELD_DESC" => "(если не выбрать, товары будут просто добавлены в каталог,<br>для торговых предложений обязатально указать идентификацию)",
    "TRANS_IDENT_NAME" => "Наименование элемента",
    "TRANS_IDENT_XML_ID" => "Внешний код",
    "TRANS_IDENT_CODE" => "Символьный код",
    "TRANS_IDENT_ACTIVE" => "Активность",
    "TRANS_PROFILE_NAME" => "Название профиля",
    "TRANS_PROFILE_SORT" => "Вес профиля в списке",
    "TRANS_PROFILE_ACTIVE" => "Активен",
    "TRANS_PROFILE_PLACEHOLDER" => "Задать имя профиля",
    "TRANS_AVALIABLE_STRING" => "Строка, по которой будет выставлен флаг доступности товара",
    "TRANS_SEPAR" => "Разделитель полей",
    "TRANS_SEPAR_TZP" => "точка с запятой",
    "TRANS_SEPAR_ZPT" => "запятая",
    "TRANS_SEPAR_TAB" => "табуляция",
    "TRANS_SEPAR_SPS" => "пробел",
    "TRANS_VENDOR_USE" => "Использовать в качестве профиля партнёра",
    "TRANS_IE_NAME" => "Наименование элемента",
    "TRANS_IE_XML_ID" => "Внешний код",
    "TRANS_IE_CODE" => "Символьный код",
    "TRANS_IE_PREVIEW_PICTURE" => "Картинка для анонса (путь)",
    "TRANS_IE_PREVIEW_TEXT" => "Описание для анонса",
    "TRANS_IE_DETAIL_PICTURE" => "Детальная картинка (путь)",
    "TRANS_IE_DETAIL_TEXT" => "Детальное описание",
    "TRANS_IE_ACTIVE" => "Активность",
    "TRANS_IE_IBLOCK_SECTION_ID" => "Раздел (ID)",
    "TRANS_ICAT_IDENT" => "Обновлять товар по этому полю",
    "TRANS_ICAT_AVAILABLE" => "Доступен",
    "TRANS_ICAT_GROUP" => "Поле групировки торговых предложений",
    "TRANS_PROPERTIES" => "Свойства",
    "TRANS_PRICE" => "Цена",
    "TRANS_IB_ELEMENT" => "Элемент инфоблока",
    "TRANS_IB_CATALOG" => "Каталог",
    "TRANS_ACTION_LIST" => "Перейти к списку профилей",
    "TRANS_ACTION_IMPORT" => "Перейти к импорту",
    "TRANS_TABS_SETTINGS" => "Настройки импорта",
    "TRANS_TABS_ANALIZE" => "Проверка файла импорта",
    "TRANS_PLAY" => "Запустить",
    "TRANS_STOP" => "Остановить",
    "TRANS_PROGRESS" => "Текущий процесс",
    "TRANS_PROGRESS_ERRORS" => "Список ошибок в файле импорта",
    "TRANS_ANALIZE_ERROR" => "Товар № ID . Ошибка: ERROR",
    "TRANS_ANALIZE_ERROR_NAME" => "Ошибка заполнения имени товара",
    "TRANS_ANALIZE_ERROR_REQUIRED" => "Не заполнено обязательное поле",
    "TRANS_IMPORT_PRODUCTS" => "Импорт товаров",
];