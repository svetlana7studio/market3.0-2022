<?
$MESS = [
    "VENDOR_ID" => "ID вендора",
    "VENDOR_OLD_ID" => "Старый ID вендора",
    "VENDOR_NAME" => "Название вендора",
    "VENDOR_ACTIVE" => "Активность",
    "VENDOR_ACTIVE_YES" => "Активен",
    "VENDOR_ACTIVE_NO" => "Не активен",
    "VENDOR_ALL" => "Все",
    "VENDOR_SELECT" => "Выбрать вендора",
    "VENDOR_SELECT_CHOOSED" => "Выбрать отмеченных вендоров",
];