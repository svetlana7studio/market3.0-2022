<?php
$MESS = [
    "ACCESS_DENIED" => "Доступ запрещен",
    "VENDOR_TITLE" => "Наценка маркетплейса",
    "VENDOR_DETAIL_PAGE" => "Задать наценку маркетплейса",
    "VENDOR_ID" => "ID",
    "VENDOR_NAME" => "Название вендора",
    "VENDOR_COMP_COMMISSION" => "Наценка маркетплейса",
    "VENDOR_COMP_COMMISSION_NONE" => "Не задано",
];