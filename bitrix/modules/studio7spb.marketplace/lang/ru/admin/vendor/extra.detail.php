<?php
$MESS = [
    "ACCESS_DENIED" => "Доступ запрещен",
    "VENDOR_TITLE" => "Наценка маркетплейса",
    "VENDOR_TITLE_VALUE" => "Наценка маркетплейса",
    "VENDOR_DETAIL_PAGE" => "Задать вознаграждение вендора",
    "VENDOR_ID" => "ID",
    "VENDOR_NAME" => "Название вендора",
    "VENDOR_PROGRES_STATUS" => "Статус обработки",
    "VENDOR_PROGRES_STATUS_MESSAGE" => "Изменена цена товара ID",
    "VENDOR_PROGRES_STATUS_FINISH" => "Процес пересчёта цен вендора успешно окончен",
    "VENDOR_PROGRES_TAB" => "Пересчитать цены вендора: NAME",
    "VENDOR_PROGRES_START" => "Запустить процес пересчёта",
    "VENDOR_PROGRES_WORK" => "Процес исполняется...",
    "VENDOR_PROGRES_NOTE1" => "Задайте значение новой наценки маркетплейса вендору",
    "VENDOR_PROGRES_NOTE2" => "Запустите процес пересчёта",
    "VENDOR_PROGRES_NOTE3" => "Обязательно дождитесь окончания процеса пересчёта, не закрывайте страницу до тех пор пока процес не завершится",
];