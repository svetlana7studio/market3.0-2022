<?
$MESS = [
    "MASTER_PROCESS_STATUS" => "Подготовлено товаров COUNT до товара с № ID",
    "MASTER_PROCESS_CREATE_SUCCESS" => "Мастер-файл каталога успешно создан <a href='PATH'>Скачать Мастер-файл</a>",
    "MASTER_PROCESS_DOWNLOAD" => "Скачать Мастер-файл",
    "XML_HEADER_A" => "Картинка",
    "XML_HEADER_B" => "Наименование продукта",
    "XML_HEADER_C" => "Описание товара",
    "XML_HEADER_D" => "Категория",
    "XML_HEADER_E" => "от 50 000 ₽",
    "XML_HEADER_F" => "от 10 000 ₽",
    "XML_HEADER_G" => "от 5 000 ₽",
    "XML_HEADER_H" => "Торговая марка",
    "XML_HEADER_I" => "Идентификатор вендора",
    "XML_ACTIVE_Y" => "Активен",
    "XML_ACTIVE_N" => "Не активен",
];