<?
/**
 * # On hosting
 * Название: Все агенты на cron для MP 2
 * Исполнитель: php 7.4 самый самый, там уже даже 8,0 вышел
 * Путь до файла: /public_html/bitrix/modules/studio7spb.marketplace/tools/cron_events.php
 * Периодичность: Каждую минуту
 *
 * # On Bitrix
 * COption::SetOptionString("main", "agents_use_crontab", "N");
 * echo COption::GetOptionString("main", "agents_use_crontab", "N");
 * COption::SetOptionString("main", "check_agents", "N");
 * echo COption::GetOptionString("main", "check_agents", "Y");
 * В результате выполнения должно быть "NN".
 *
 * После этого убираем из файла /bitrix/php_interface/dbconn.php определение следующих констант:
 * define("BX_CRONTAB_SUPPORT", true);
 * define("BX_CRONTAB", true);
 *
 * И добавляем
 * # ALL AGENTS BY CRON
 * if(!(defined("CHK_EVENT") && CHK_EVENT===true))
 *      define("BX_CRONTAB_SUPPORT", true);
 *
 * После этого все агенты и отправка системных событий будут обрабатывается из под cron, раз в 5 минут. Чтобы не увеличивалась очередь отправки почтовых сообщений
 * COption::SetOptionString("main", "mail_event_bulk", "20");
 * echo COption::GetOptionString("main", "mail_event_bulk", "5");
 */
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../../../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

CAgent::CheckAgents();
define("BX_CRONTAB_SUPPORT", true);
define("BX_CRONTAB", true);
CEvent::CheckEvents();