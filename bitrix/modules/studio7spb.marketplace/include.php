<?
use Bitrix\Main\Loader;

Loader::registerAutoLoadClasses("studio7spb.marketplace", [
    "\\Studio7spb\\Marketplace\\CMarketplaceCache" => "lib/CMarketplaceCache.php",
    "\\Studio7spb\\Marketplace\\CMarketplaceEvents" => "lib/CMarketplaceEvents.php",
    "\\Studio7spb\\Marketplace\\CMarketplaceOptions" => "lib/CMarketplaceOptions.php",
    "\\Studio7spb\\Marketplace\\CMarketplaceSeller" => "lib/CMarketplaceSeller.php",
    "\\Studio7spb\\Marketplace\\CMarketplaceTools" => "lib/CMarketplaceTools.php",
    "\\Studio7spb\\Marketplace\\CMarketplaceGrupperGroups" => "lib/CMarketplaceGrupperGroups.php",
    "\\Studio7spb\\Marketplace\\CMarketplaceGrupperBinds" => "lib/CMarketplaceGrupperBinds.php",
    "\\Studio7spb\\Marketplace\\\Handler\\SaleActionDiscountFromProperty" => "lib/handler/saleactiondiscountfromproperty.php",
    "\\Studio7spb\\Marketplace\\\FavoriteTable" => "lib/favorite.php",
    "\\Studio7spb\\Marketplace\\\ElementsFavoriteTable" => "lib/elementsfavorite.php",
    "\\Studio7spb\\Marketplace\\\SaleAddressTable" => "lib/saleaddress.php",
    "\\Studio7spb\\Marketplace\\\RequisitsTable" => "lib/requisits.php",
    "\\Studio7spb\\Marketplace\\\MultipleBasketTable" => "lib/multiplebasket.php"
]);

global $APPLICATION, $USER;

$moduleId = 'studio7spb.marketplace';
$moduleJsId = str_replace('.', '_', $moduleId);
$pathJS = '/bitrix/js/'.$moduleId;
$pathCSS = '/bitrix/css/'.$moduleId;
$pathLang = BX_ROOT.'/modules/'.$moduleId.'/lang/'.LANGUAGE_ID;

if(!is_object($USER)) $USER = new CUser;
if($USER->IsAdmin())
{
    if(in_array($APPLICATION->GetCurPage(), [
        "/bitrix/admin/studio7spb.marketplace_transfer.settings.php",
        "/bitrix/admin/iblock_element_edit.php"
    ])){
        switch ($APPLICATION->GetCurPage()){
            case "/bitrix/admin/studio7spb.marketplace_transfer.settings.php":
                $arJSspb7Config = [
                    $moduleJsId . "_import_settings" => [
                        'js' => $pathJS.'/import.settings.js',
                        'css' => $pathCSS.'/import.settings.css',
                        'rel' => ['jquery'],
                        'lang' => $pathLang.'/admin/import/transfer.settings.php',
                    ],
                    $moduleJsId . "_chosen" => [
                        'js' => $pathJS.'/chosen/chosen.jquery.min.js',
                        'css' => $pathJS.'/chosen/chosen.min.css',
                        'rel' => ["jquery"]
                    ]
                ];
                break;
        }
    }

    /** Transfer to studio7spb.content_products
    $arJSspb7Config = [
        $moduleJsId . "_products_snippet" => [
            'js' => $pathJS.'/products.snippet.js',
            'css' => $pathCSS.'/products.snippet.css',
            //'rel' => ['sender_page']
        ],
    ];*/

    foreach ($arJSspb7Config as $ext => $arExt) {
        \CJSCore::RegisterExt($ext, $arExt);
    }

    \CJSCore::Init([
        $moduleJsId . "_products_snippet",
        "sidepanel"
    ]);
}

if (!function_exists("d") )
{
    /**
     * @param $value
     * @param string $type
     */
    function d($value, $type="pre")
    {
        if ( is_array( $value ) || is_object( $value ) )
        {
            echo "<" . $type . " class=\"prettyprint\">".htmlspecialcharsbx( print_r($value, true) )."</" . $type . ">";
        }
        else
        {
            echo "<" . $type . " class=\"prettyprint\">".htmlspecialcharsbx($value)."</" . $type . ">";
        }
    }
}

require_once dirname(__FILE__) . "/tools/catalog.php";