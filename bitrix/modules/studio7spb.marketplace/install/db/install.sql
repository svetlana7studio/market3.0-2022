CREATE TABLE IF NOT EXISTS b_studio7spb_grupper_groups
(
	ID int(11) NOT NULL AUTO_INCREMENT,
	XML_ID varchar(255) NULL,
	CODE varchar(255) NULL,
	NAME varchar(255) NOT NULL,
	SORT int(18) NOT NULL DEFAULT 500,
	ICON_PATH varchar(500) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS b_studio7spb_grupper_binds
(
	ID int(11) NOT NULL AUTO_INCREMENT,
	IBLOCK_PROPERTY_ID int(11) NOT NULL,
	GROUP_ID int(18) NOT NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS b_studio7spb_favorite
(
    ID INT NOT NULL AUTO_INCREMENT,
	IBLOCK_ELEMENT_ID INT,
	USER_ID INT,
	PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS b_studio7spb_sale_address
(
    ID INT NOT NULL AUTO_INCREMENT,
	PROFILE_ID INT,
	USER_ID INT,
	LOCATION varchar(255),
	LOCATION_VAL varchar(255),
	ORDER_PROPS varchar(255),
	PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS b_studio7spb_elements_favorite
(
    ID INT NOT NULL AUTO_INCREMENT,
	ELEMENT_ID INT,
	IBLOCK_ID INT,
	USER_ID INT,
	PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS b_studio7spb_requisits
(
    ID INT NOT NULL AUTO_INCREMENT,
    TIMESTAMP_X timestamp not null default current_timestamp on update current_timestamp,
    USER_ID INT,
    NAME varchar(255),
    OWNERSHIP varchar(255),
    INN varchar(255),
    KPP varchar(255),
    OKPO varchar(255),
    OGRN varchar(255),
    OKATO varchar(255),
    ACCOUNTANT varchar(255),
    GENERAL varchar(255),
    BAMK varchar(255),
    PAYMENT varchar(255),
    BIK varchar(255),
    ADRESS_JUR TEXT,
    ADRESS_FACT TEXT,
    ADRESS_POST TEXT,
    PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS b_studio7spb_import
(
    ID INT NOT NULL AUTO_INCREMENT,
    STATUS CHAR(1),
    DATA TEXT,
    NOTE VARCHAR(255),
    PRIMARY KEY (ID)
);
CREATE TABLE IF NOT EXISTS b_studio7spb_import_settings
(
    ID INT NOT NULL AUTO_INCREMENT,
    NAME varchar(255),
    CODE varchar(255),
    VALUE varchar(255),
    PRIMARY KEY (ID)
);
CREATE TABLE IF NOT EXISTS b_studio7spb_multiple_basket
(
    ID int not null AUTO_INCREMENT,
    LID char(2) not null,
    FUSER_ID int not null,
    NAME varchar(255),
    PRODUCTS varchar(255),
    PARAMS TEXT,
    PRIMARY KEY (ID)
);
CREATE TABLE IF NOT EXISTS b_studio7spb_static
(
    ID int not null AUTO_INCREMENT,
    LID char(2) not null,
    DATE datetime,
    NAME varchar(255),
    DATA TEXT,
    PRIMARY KEY (ID)
);
CREATE TABLE IF NOT EXISTS b_studio7spb_vendor_extra
(
    ID int not null AUTO_INCREMENT,
    DATE datetime,
    USER_ID int,
    VENDOR_ID int,
    VALUE int,
    PRIMARY KEY (ID)
);
CREATE TABLE IF NOT EXISTS b_anytos_section_property
(
    ID int not null AUTO_INCREMENT,
    SECTION_ID int(11) not null,
    DISCOUNT int(11),
    BREND_ID int(11),
    PRIMARY KEY (ID)
);
CREATE TABLE IF NOT EXISTS b_studio7spb_vendor_message
(
    ID INT NOT NULL AUTO_INCREMENT,
    NAME varchar(255),
    EMAIL varchar(255),
    TEXT varchar(255),
    VENDOR_ID int,
    CATEGORY varchar(32),
    SORT INT,
    ACTIVE char(1) not null DEFAULT 'Y',
    DATE_CREATE datetime,
    DATE_UPDATE datetime,
    USER_CREATE INT,
    USER_UPDATE INT,
    PRIMARY KEY (ID)
);
CREATE TABLE IF NOT EXISTS b_studio7spb_import_profile_list
(
    ID int not null AUTO_INCREMENT,
    ACTIVE char(1),
    NAME varchar(255),
    SORT int(11),
    IBLOCK_TYPE varchar(255),
    IBLOCK_ID int(11),
    IBLOCK_SECTION_ID int(11),
    FILE int,
    FIELDS text,
    SAMPLE text,
    START int(11),
    IDENT varchar(255),
    AVALIABLE_STRING varchar(255),
    SEPAR varchar(10),
    CUR char(1),
    PRIMARY KEY (ID)
);