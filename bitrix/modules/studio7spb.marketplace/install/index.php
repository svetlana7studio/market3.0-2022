<?
IncludeModuleLangFile(__FILE__);
Class studio7spb_marketplace extends CModule
{
	var $MODULE_ID = 'studio7spb.marketplace';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage('S7_MARKETPLACE_MODULE_NAME');
		$this->MODULE_DESCRIPTION = GetMessage('S7_MARKETPLACE_MODULE_DESCRIPTION');

		$this->PARTNER_NAME = GetMessage('S7_MARKETPLACE_PARTNER_NAME');
		$this->PARTNER_URI = 'https://7sait.spb.ru';
	}

	function DoInstall()
	{
		global $APPLICATION, $step;

		$this->InstallFiles();
		$this->InstallDB();
		$this->InstallEvents();
	}

	function DoUninstall()
	{
		global $APPLICATION, $step;

		$this->UnInstallDB();
		$this->UnInstallFiles();
		$this->UnInstallEvents();
	}

	function InstallFiles(){
		CopyDirFiles(__DIR__.'/admin/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin', true);
		CopyDirFiles(__DIR__.'/css/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/css/'.$this->MODULE_ID, true, true);
		// CopyDirFiles(__DIR__.'/js/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/js/'.$this->MODULE_ID, true, true);
		CopyDirFiles(__DIR__.'/images/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/images/'.$this->MODULE_ID, true, true);
		// CopyDirFiles(__DIR__.'/components/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/components', true, true);
		// CopyDirFiles(__DIR__.'/wizards/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/wizards', true, true);
		return true;
	}

	function UnInstallFiles(){
		DeleteDirFiles(__DIR__.'/admin/', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin');
		DeleteDirFilesEx('/bitrix/css/'.$this->MODULE_ID.'/');
		DeleteDirFilesEx('/bitrix/js/'.$this->MODULE_ID.'/');
		DeleteDirFilesEx('/bitrix/images/'.$this->MODULE_ID.'/');
		// DeleteDirFilesEx('/bitrix/wizards/'.self::partnerName.'/'.self::solutionName.'/');
		return true;
	}

	function InstallDB() {
		global $DB, $APPLICATION;
		
		\Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

		$DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/studio7spb.marketplace/install/db/install.sql");
		return true;
	}

	function UnInstallDB() {

		global $DB, $APPLICATION;
		$DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/studio7spb.marketplace/install/db/uninstall.sql");

		\Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);
		return true;
	}

	function InstallEvents() {
		$eventManager = \Bitrix\Main\EventManager::getInstance();
		$eventManager->registerEventHandlerCompatible('main', 'OnPageStart', 'studio7spb.marketplace', '\Studio7spb\Marketplace\CMarketplaceEvents', 'OnBeforeProlog');
		$eventManager->registerEventHandlerCompatible('main', 'OnBuildGlobalMenu', 'studio7spb.marketplace', '\Studio7spb\Marketplace\CMarketplaceEvents', 'onBuildGlobalMenu');
		$eventManager->registerEventHandlerCompatible('main', 'OnAdminListDisplay', 'studio7spb.marketplace', '\Studio7spb\Marketplace\CMarketplaceEvents', 'OnAdminListDisplay');
		$eventManager->registerEventHandlerCompatible('iblock', 'OnAfterIBlockElementUpdate', 'studio7spb.marketplace', '\Studio7spb\Marketplace\CMarketplaceEvents', 'OnAfterIBlockElementUpdate');
		$eventManager->registerEventHandlerCompatible('sale', 'OnCondSaleActionsControlBuildList', 'studio7spb.marketplace', '\Studio7spb\Marketplace\Handler\SaleActionDiscountFromProperty', 'GetControlDescr');

		return true;
	}

	function UnInstallEvents() {
		$eventManager = \Bitrix\Main\EventManager::getInstance();
		$eventManager->unRegisterEventHandler('main', 'OnPageStart', 'studio7spb.marketplace', '\Studio7spb\Marketplace\CMarketplaceEvents', 'OnBeforeProlog');
		$eventManager->unRegisterEventHandler('main', 'OnBuildGlobalMenu', 'studio7spb.marketplace', '\Studio7spb\Marketplace\CMarketplaceEvents', 'onBuildGlobalMenu');
		$eventManager->unRegisterEventHandler('main', 'OnAdminListDisplay', 'studio7spb.marketplace', '\Studio7spb\Marketplace\CMarketplaceEvents', 'OnAdminListDisplay');
		$eventManager->unRegisterEventHandler('iblock', 'OnAfterIBlockElementUpdate', 'studio7spb.marketplace', '\Studio7spb\Marketplace\CMarketplaceEvents', 'OnAfterIBlockElementUpdate');
		$eventManager->unRegisterEventHandler('sale', 'OnCondSaleActionsControlBuildList', 'studio7spb.marketplace', '\Studio7spb\Marketplace\Handler\SaleActionDiscountFromProperty', 'GetControlDescr');

		return true;
	}


}
?>