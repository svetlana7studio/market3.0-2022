<?
/**
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset,
    Bitrix\Iblock\ElementTable,
    Bitrix\Main\Grid\Options as GridOptions,
    Bitrix\Main\UI\PageNavigation;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_WITH_ON_AFTER_EPILOG', true);
define('BX_NO_ACCELERATOR_RESET', true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadLanguageFile(__FILE__);
Loader::includeModule("iblock");

$list_id = "products_list";
$grid_options = new GridOptions($list_id);
$filterOption = new Bitrix\Main\UI\Filter\Options($list_id);
$filterFields = $filterOption->getFilter();
$filter = [];

$sort = $grid_options->GetSorting(['sort' => ['ID' => 'DESC'], 'vars' => ['by' => 'by', 'order' => 'order']]);

$nav_params = $grid_options->GetNavParams();
$nav = new PageNavigation('request_list');
$nav->allowAllRecords(true)
    ->setPageSize($nav_params['nPageSize'])
    ->initFromUri();

$columns = [
    [
        "id" => "ID",
        "name" => Loc::getMessage("SNIPPET_PRODUCT_ID"),
        "sort" => "ID",
        "default" => true
    ],
    [
        "id" => "PREVIEW_PICTURE",
        "name" => Loc::getMessage("SNIPPET_PRODUCT_PREVIEW_PICTURE"),
        "default" => true
    ],
    [
        "id" => "NAME",
        "name" => Loc::getMessage("SNIPPET_PRODUCT_NAME"),
        "sort" => "NAME",
        "default" => true
    ],
    [
        "id" => "SORT",
        "name" => Loc::getMessage("SNIPPET_PRODUCT_SORT"),
        "sort" => "SORT",
        "default" => true
    ],
];

$filter = [
    "IBLOCK_ID" => 2,
    "ACTIVE" => "Y"
];


foreach ($filterFields as $key => $value)
{
    switch ($key){
        // integer
        case "ID_from":
            $filter[">=ID"] = $value;
            break;
        case "ID_to":
            $filter["<=ID"] = $value;
            break;
        case "SORT_from":
            $filter[">=SORT"] = $value;
            break;
        case "SORT_to":
            $filter["<=SORT"] = $value;
            break;
        case "NAME":
            $filter['%NAME'] = $value;
    }
}


$list = [];
$res = ElementTable::getList(['filter' => $filter, 'select' => ["ID"]]);
$nav_count = $res->getSelectedRowsCount();
$nav->setRecordCount($nav_count);

$res = ElementTable::getList([
    'filter' => $filter,
    'offset'      => $nav->getOffset(),
    'limit'       => $nav->getLimit(),
    'order'       => $sort['sort']
]);

foreach ($res->fetchAll() as $row) {
    if($row["PREVIEW_PICTURE"] > 0)
    {
        $row["PREVIEW_PICTURE"] = CFile::ResizeImageGet($row["PREVIEW_PICTURE"], [
            "width" => 75,
            "height" => 75
        ], BX_RESIZE_IMAGE_EXACT, false);
        $row["PREVIEW_PICTURE"] = '<img src="' . $row["PREVIEW_PICTURE"]["src"] . '" width="75" height="75">';
    }


    $list[] = [
        "data" => [
            "ID" => $row["ID"],
            "PREVIEW_PICTURE" => $row["PREVIEW_PICTURE"],
            "NAME" => $row["NAME"],
            "SORT" => $row["SORT"]
        ],
        "actions" => [
            [
                'text'    => Loc::getMessage("SNIPPET_PRODUCT_1"),
                'default' => true,
                'onclick' => 'BX.Sender.Page.choseProduct(1, ' . $row["ID"] . ', "' . htmlspecialcharsEx($row["NAME"]) . '", "' . $row["PREVIEW_PICTURE"]["src"] . '")'
            ],
[
                'text'    => Loc::getMessage("SNIPPET_PRODUCT_2"),
                'default' => true,
                'onclick' => 'BX.Sender.Page.choseProduct(2, ' . $row["ID"] . ', "' . htmlspecialcharsEx($row["NAME"]) . '", "' . $row["PREVIEW_PICTURE"]["src"] . '")'
            ],
[
                'text'    => Loc::getMessage("SNIPPET_PRODUCT_3"),
                'default' => true,
                'onclick' => 'BX.Sender.Page.choseProduct(3, ' . $row["ID"] . ', "' . htmlspecialcharsEx($row["NAME"]) . '", "' . $row["PREVIEW_PICTURE"]["src"] . '")'
            ],
        ]
    ];
}

$ui_filter = [
    [
        "id" => "ID",
        "name" => Loc::getMessage("SNIPPET_PRODUCT_ID"),
        "default" => true,
        "type" => "string"
    ],
    [
        "id" => "SORT",
        "name" => Loc::getMessage("SNIPPET_PRODUCT_SORT"),
        "default" => true,
        "type" => "string"
    ],
    [
        "id" => "NAME",
        "name" => Loc::getMessage("SNIPPET_PRODUCT_NAME"),
        "default" => true,
        "type" => "string"
    ],
];

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
Asset::getInstance()->addJs("/bitrix/js/studio7spb.marketplace/products.snippet.js");
$APPLICATION->SetTitle(Loc::getMessage("SNIPPET_TITLE"));

?>
<form action="<?=$APPLICATION->GetCurPage()?>"
      method="post"
      onsubmit="return BX.Sender.Page.sendToSnippet(this, '<?=$_REQUEST["code"]?>')">
    <?=bitrix_sessid_post()?>
    <table align="center">
        <tr align="center">
            <td colspan="3"><?=Loc::getMessage("SNIPPET_DESCRIPTION")?></td>
        </tr>
        <tr align="center">
            <?for($i=1; $i<=3;$i++):?>
            <td>
                <?=BeginNote();?>
                <div style="height: 74px; width: 74px; text-align: center; overflow: hidden;"
                     id="snippet-product-<?=$i?>">
                    <div style="line-height: 75px"><?=$i?></div>
                </div>
                <?=EndNote();?>
                <input type="hidden"
                       value=""
                       id="snippet-product-id-<?=$i?>"
                       name="snippet-product-id-<?=$i?>">
            </td>
            <?endfor;?>
        </tr>
        <tr align="center">
            <td colspan="3">
                <input type="submit" value="<?=Loc::getMessage("SNIPPET_PRODUCT_SET_TO_SNIPPET")?>">
            </td>
        </tr>
    </table>
</form>
<?

// <editor-fold defaultstate="collapsed" desc="Panel">
$APPLICATION->IncludeComponent('bitrix:main.ui.filter', '', [
    'FILTER_ID' => $list_id,
    'GRID_ID' => $list_id,
    'FILTER' => $ui_filter,
    'ENABLE_LIVE_SEARCH' => true,
    'ENABLE_LABEL' => true
]);

// </editor-fold>

$APPLICATION->IncludeComponent('bitrix:main.ui.grid', '', [
    'GRID_ID' => $list_id,
    'COLUMNS' => $columns,
    'ROWS' => $list,
    'SHOW_ROW_CHECKBOXES' => false,
    'NAV_OBJECT' => $nav,
    "TOTAL_ROWS_COUNT" => $nav_count,
    'AJAX_MODE' => 'Y',
    'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
    'PAGE_SIZES' =>  [
        ['NAME' => '5', 'VALUE' => '5'],
        ['NAME' => '10', 'VALUE' => '10'],
        ['NAME' => '20', 'VALUE' => '20'],
        ['NAME' => '50', 'VALUE' => '30'],
        ['NAME' => '40', 'VALUE' => '40'],
        ['NAME' => '50', 'VALUE' => '50']
    ],
    'AJAX_OPTION_JUMP'          => 'N',
    'SHOW_CHECK_ALL_CHECKBOXES' => false,
    'SHOW_ROW_ACTIONS_MENU'     => true,
    'SHOW_GRID_SETTINGS_MENU'   => true,
    'SHOW_NAVIGATION_PANEL'     => true,
    'SHOW_PAGINATION'           => true,
    'SHOW_SELECTED_COUNTER'     => true,
    'SHOW_TOTAL_COUNTER'        => true,
    'SHOW_PAGESIZE'             => true,
    'SHOW_ACTION_PANEL'         => true,
    'ALLOW_COLUMNS_SORT'        => true,
    'ALLOW_COLUMNS_RESIZE'      => true,
    'ALLOW_HORIZONTAL_SCROLL'   => true,
    'ALLOW_SORT'                => true,
    'ALLOW_PIN_HEADER'          => true,
    'AJAX_OPTION_HISTORY'       => 'N'
]);

