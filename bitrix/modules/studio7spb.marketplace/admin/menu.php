<?
use Studio7spb\Marketplace\Import\ImportProfileListTable,
    Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
$moduleId = 'studio7spb.marketplace';

// <editor-fold defaultstate="collapsed" desc=" # Import items group">
$items[] = [
    "text" => GetMessage("STUDIO7_MARKETPLACE_MENU_IMPORT_PROFILE_LIST"),
    "url" => $moduleId."_profile.list.php",
    "title" => GetMessage("STUDIO7_MARKETPLACE_MENU_IMPORT_PROFILE_LIST"),
    "icon" => "adm-menu-excel",
    "page_icon" => "adm-menu-excel"
];

Loader::registerAutoLoadClasses("studio7spb.marketplace", [
    "\\Studio7spb\\Marketplace\\Import\\ImportProfileListTable" => "lib/import/importprofilelisttable.php"
]);

$profiles = ImportProfileListTable::getList([]);
while ($profile = $profiles->fetch())
{
    $items[] = [
        "text" => $profile["NAME"],
        "url" => $moduleId."_transfer.settings.php?profile=" . $profile["ID"],
        "title" => $profile["NAME"],
        "icon" => "adm-menu-excel",
        "page_icon" => "adm-menu-excel",
        "more_url" => [$moduleId."_transfer.settings.php"]
    ];
}
/*
$items[] = [
    "text" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_IMPORT_PRODUCT_TEXT"),
    "url" => $moduleId."_transfer.php",
    "title" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_IMPORT_PRODUCT_TITLE"),
    "icon" => "adm-menu-excel",
    "page_icon" => "adm-menu-excel"
];
*/

$aMenu[] = array(
    "parent_menu" => "global_menu_studio7spb",
    "section" => "import_catalog",
    "sort" => 10,
    "text" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_IMPORT_CATALOG_TEXT"),
    "title" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_IMPORT_CATALOG_TITLE"),
    "icon" => "adm-menu-excel",
    "page_icon" => "adm-menu-excel",
    "items_id" => "import_catalog",
    "items" => $items,
);

unset($items);
// <editor-fold>

$aMenu[] = array(
    "parent_menu" => "global_menu_studio7spb",
    "section" => "salers",
    "sort" => 10,
    "text" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_SALERS_SECTION_TEXT"),
    "title" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_SALERS_SECTION_TITLE"),
    "icon" => "user_menu_icon",
    "page_icon" => "user_page_icon",
    "items_id" => "menu_selers",
    "items" => array(
        array(
            "text" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_SALERS_ITEM_COMPANY_LIST_TEXT"),
            "url" => $moduleId."_salers_company_list.php",
            "title" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_SALERS_ITEM_COMPANY_LIST_TITLE"),
        )
    ),
);

$aMenu[] = array(
    "parent_menu" => "global_menu_studio7spb",
    "sort" => 20,
    "text" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_DELIVERY_TEXT"),
    "title" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_DELIVERY_TITLE"),
    "url" => $moduleId."_settings_delivery.php",
    "more_url" => array($moduleId."_settings_delivery.php"),
    "icon" => "sale_menu_icon_marketplace",
    "page_icon" => "sale_page_icon_marketplace",
);

$aMenu[] = array(
    "parent_menu" => "global_menu_studio7spb",
    "sort" => 20,
    "text" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_GRUPPED_TEXT"),
    "title" => Loc::getMessage("STUDIO7_MARKETPLACE_MENU_GRUPPED_TITLE"),
    "url" => $moduleId."_grupper.php",
    "more_url" => array($moduleId."_grupper.php",$moduleId."_grupper_edit.php", ),
    "icon" => "iblock_menu_icon_sections",
    "page_icon" => "iblock_page_icon_sections",
);

return $aMenu;