<?

// set_time_limit(0);
set_time_limit(100000000000000000); /* --- увеличим время выполнения скрипта --- */

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Web\Json;

require $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';
# require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
Loc::loadLanguageFile(__FILE__);



/**
 * @param $path
 */
function clearImport($path){
    $files = array_diff(
        scandir($path),
        array('..', '.')
    );
    if(!empty($files))
    {
        foreach ($files as $file){
            unlink($path . $file);
        }
    }
}

// <editor-fold defaultstate="collapsed" desc=" # arParams">
$arParams = array(
    "UPLOAD_PATH" => $_SERVER["DOCUMENT_ROOT"] . "/upload/excel/download/catalog/",
    //"UPLOAD_FILE_NAME" => "anytos.catalog." . date("d_m_Y_H_i_s") . ".zip",
    "UPLOAD_FILE_NAME" => "anytos.catalog.zip",
    "ALPHAVITE" => [
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J"
    ]
);

$arParams["UPLOAD_PATH_TMP"] = $arParams["UPLOAD_PATH"] . "tmp/";
$arParams["UPLOAD_PATH_CACHE"] = $arParams["UPLOAD_PATH"] . "cache/";
$arParams["UPLOAD_PATH_IMPORT"] = $arParams["UPLOAD_PATH"] . "import/";
@mkdir($arParams["UPLOAD_PATH"], 0777);
@mkdir($arParams["UPLOAD_PATH_TMP"], 0777);
@mkdir($arParams["UPLOAD_PATH_CACHE"], 0777);
@mkdir($arParams["UPLOAD_PATH_IMPORT"], 0777);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # arResult">
$arResult = [
    "FILES" => []
];

$arResult["FILES"] = array_diff(
    scandir($arParams["UPLOAD_PATH_TMP"]),
    array('..', '.')
);


if(!empty($arResult["FILES"]))
{
    /*
    $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_discISAM;
    $cacheSettings = array(
        'dir' => $arParams["UPLOAD_PATH_CACHE"]
    );
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
*/

    $objPHPExcel = new PHPExcel();
    $row = 3;

    # Stylesheet
    $objPHPExcel->getActiveSheet()->freezePane('C4');
    $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setIndent(2);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(18);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getFont()->setSize(14);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('22cfc7');
    $objPHPExcel->getActiveSheet()->getStyle($row)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setWrapText(true);

    // <editor-fold defaultstate="collapsed" desc=" # Add header">
    foreach ($arParams["ALPHAVITE"] as $latter){
        $objPHPExcel->getActiveSheet()->SetCellValue($latter.$row, Loc::getMessage("XML_HEADER_" . $latter));
        switch ($latter){
            case "B":
            case "C":
                $objPHPExcel->getActiveSheet()->getColumnDimension($latter)->setWidth(50);
                break;
            default:
                $objPHPExcel->getActiveSheet()->getColumnDimension($latter)->setWidth(24);
        }
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Parse products from files">
    foreach ($arResult["FILES"] as $file){
        $jsonObjects = file($arParams["UPLOAD_PATH_TMP"] . $file);
        if(!empty($jsonObjects)){
            foreach ($jsonObjects as $arElements){
                $arElements = Json::decode($arElements);
                if(!empty($arElements)){
                    foreach ($arElements as $arEl){
                        $row++;

                        if(!empty($arEl["PROPERTY_LHW_CTN_VALUE"])){
                            $PROPERTY_LHW_CTN_VALUE = explode("х", $arEl["PROPERTY_LHW_CTN_VALUE"]);
                            foreach ($PROPERTY_LHW_CTN_VALUE as $key=>$value){
                                $value = str_replace(Loc::getMessage("XML_MEASURE_SM"), "", $value);
                                $value = trim($value);
                                $PROPERTY_LHW_CTN_VALUE[$key] = $value;
                            }
                        }

                        foreach ($arParams["ALPHAVITE"] as $latter){

                            // row styles
                            $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setWrapText(true);
                            $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(40);

                            // cells
                            switch ($latter){
                                case "A":
                                    if(is_array($arEl["PREVIEW_PICTURE"])){
                                        $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PREVIEW_PICTURE"]["SRC"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    }
                                    break;
                                case "B":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "C":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PREVIEW_TEXT"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "D":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["SECTION"]["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "E":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["CATALOG_PRICE_4"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "F":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["CATALOG_PRICE_6"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "G":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["CATALOG_PRICE_8"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "H":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_TRADE_MARK_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "I":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_ANYTOS_VENDOR_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                            }
                        }
                    }
                }

            }
        }

        unlink($arParams["UPLOAD_PATH_TMP"] . $file);

        ############# Start Write to excel file ###############
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($arParams["UPLOAD_PATH_IMPORT"]. $file . ".xlsx");
        unset($objPHPExcel);
        $objPHPExcel = new PHPExcel();
        $row = 1;
        ############# End Write to excel file ###############

    }
    // </editor-fold>

    $PATH = $arParams["UPLOAD_PATH"] . $arParams["UPLOAD_FILE_NAME"];

    //exec('zip -r "' . $PATH .  '" "' . $arParams["UPLOAD_PATH_IMPORT"] . '"');

    // exec("tar -cvf " . $PATH . " import/");


    // Архивирование в zip
    $packarc = CBXArchive::GetArchive($PATH);
    $packarc->SetOptions(Array(
        "REMOVE_PATH" => $_SERVER["DOCUMENT_ROOT"]."/upload/excel/download/catalog/import/",
    ));

    $pRes = $packarc->Pack($arParams["UPLOAD_PATH_IMPORT"]);


}

// </editor-fold>

# Output
#$PATH = $arParams["UPLOAD_PATH"] . $arParams["UPLOAD_FILE_NAME"];
$PATH = str_replace($DOCUMENT_ROOT, "", $PATH);
echo 'CurrentStatus = Array(100,"","' . Loc::getMessage("MASTER_PROCESS_CREATE_SUCCESS", ["PATH" => $PATH]) .'");';
unset($objPHPExcel);
clearImport($arParams["UPLOAD_PATH_CACHE"]);
clearImport($arParams["UPLOAD_PATH_TMP"]);
clearImport($arParams["UPLOAD_PATH_IMPORT"]);

die;