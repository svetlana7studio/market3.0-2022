<?
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\IO\File,
    Bitrix\Iblock\SectionTable,
    Bitrix\Main\Web\Json,
    Studio7spb\Marketplace\CMarketplaceOptions;

set_time_limit(0);

if (ini_get('mbstring.func_overload') & 2) {
    ini_set("mbstring.func_overload", 0);
}

if (isset($_REQUEST['work_start']))
{
    define("NO_AGENT_STATISTIC", true);
    define("NO_KEEP_STATISTIC", true);
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

CModule::IncludeModule("iblock");
Loc::loadLanguageFile(__FILE__);

// <editor-fold defaultstate="collapsed" desc="# Params and arResult init">
$arParams = [
    "ACTION" => "studio7spb.m2_master_file.php",
    "UPLOAD_PATH" => $_SERVER["DOCUMENT_ROOT"] . "/upload/excel/download/catalog/",
    "TMP_FILE_NAME" => 0,
    "IBLOCK_ID" => CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id"),
    "LIMIT" => 10,
    "FILE_LIMIT" => 50
];

$arParams["UPLOAD_PATH_TMP"] = $arParams["UPLOAD_PATH"] . "tmp/";

$arResult = [];
// </editor-fold>

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::getMessage("MAIN_ACCESS_DANIED"));

// <editor-fold defaultstate="collapsed" desc="# System execute">

switch ($_REQUEST["action"]){
    case "create_mater_file":

        if(check_bitrix_sessid()){
            # echo shell_exec('php ' . $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/studio7spb.marketplace/admin/download/master_file.create.php ' . $_SERVER["DOCUMENT_ROOT"]);

            require 'master_file.create.php';
        }
        die();
        break;
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="# Iblock sections">
$arResult["SECTIONS"] = [];
$sections = SectionTable::getList([
    "order" => ["NAME" => "ASC"],
    "filter" => [
        "IBLOCK_ID" => 2,
        "=DEPTH_LEVEL" => 3
    ],
    "select" => ["ID", "NAME", "DEPTH_LEVEL"]
]);
while ($section = $sections->fetch()){

    //$section["NAME"] = $section["NAME"] . " " . $section["DEPTH_LEVEL"];

    $arResult["SECTIONS"][$section["ID"]] = $section;
}
unset($sections, $section);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="# Work start">
if($_REQUEST['work_start'] && check_bitrix_sessid())
{
    $vendors = $_REQUEST['vendors'];
    $sectionID = intval($_REQUEST['sectionID']);
    $vendors = explode(",", $vendors);
    foreach ($vendors as $key=>$vendor){
        if(intval($vendor) <= 0){
            unset($vendors[$key]);
        }
    }

    if(empty($vendors) && $sectionID <= 0){
        //echo 'CurrentStatus = Array(10,"","' . Loc::getMessage("VENDORS_DONT_CHOOSE") .'");';
        die;
    }

    $arFilter = [
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        ">ID" => $_REQUEST["lastid"],
        //"PROPERTY_H_COMPANY" => 45431
        //"PROPERTY_H_COMPANY" => $vendors
    ];

    if(!empty($vendors)){
        $arFilter["PROPERTY_ANYTOS_VENDOR"] = $vendors;
    }

    if($sectionID > 0){
        $arFilter["SECTION_ID"] = $sectionID;
        $arFilter["INCLUDE_SUBSECTIONS"] = "Y";
    }

    $rsEl = CIBlockElement::GetList(
        ["ID" => "ASC"],
        $arFilter,
        false,
        ["nTopCount" => $arParams["LIMIT"]],
        [
            "ID",
            "IBLOCK_ID",
            "NAME",
            "PREVIEW_PICTURE",
            "PREVIEW_TEXT",
            "IBLOCK_SECTION_ID",
            "PROPERTY_ANYTOS_DETAIL_PICTURE",
            "PROPERTY_TRADE_MARK",
            "PROPERTY_ANYTOS_VENDOR",
            "CATALOG_GROUP_4",
            "CATALOG_GROUP_8",
            "CATALOG_GROUP_8"
        ]
    );

    $jsonExcel = [];
    while ($arEl = $rsEl->Fetch())
    {
        // formate name
        $arEl["NAME"] = preg_replace("#[^0-9а-яА-ЯA-Za-z;:_.,? -]+#u", ' ', $arEl["NAME"]);

        // get Section
        $arEl["SECTION"] = $arResult["SECTIONS"][$arEl["IBLOCK_SECTION_ID"]];

        // get picture
        if($_REQUEST["usePictures"] == "Y"){
            if($arEl["PREVIEW_PICTURE"] > 0){
                $arEl["PREVIEW_PICTURE"] = CFile::GetFileArray($arEl["PREVIEW_PICTURE"]);
            }else{
                $arEl["PREVIEW_PICTURE"] = [
                    "SRC" => m2PreviewPictureSrc($arEl["PROPERTY_ANYTOS_DETAIL_PICTURE_VALUE"]),
                ];
                if(empty($arElement["PREVIEW_PICTURE"])){
                    $arEl["PREVIEW_PICTURE"]["SRC"] = "/bitrix/templates/s7spb.m2/images/no_image200.jpg";
                }
            }

            if(!empty($arEl["PREVIEW_PICTURE"]["SRC"])){
                if (strpos($arEl["PREVIEW_PICTURE"]["SRC"], 'http') === false) {
                    $arEl["PREVIEW_PICTURE"]["SRC"] = "http://anytos.ru/" . $arEl["PREVIEW_PICTURE"]["SRC"];
                }
            }
        }


        $jsonExcel[] = $arEl;
        $lastID = intval($arEl["ID"]);
    }
    $jsonExcel = Json::encode($jsonExcel);


    // calculate process params
    $rsLeftBorder = CIBlockElement::GetList(
        ["ID" => "ASC"],
        [
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "<=ID" => $lastID,
            //"PROPERTY_H_COMPANY" => 45431
            //"PROPERTY_H_COMPANY" => $vendors
            "PROPERTY_ANYTOS_VENDOR" => $vendors
        ],
        false,
        false,
         ["ID"]
    );
    $leftBorderCnt = $rsLeftBorder->SelectedRowsCount();
    $rsAll = CIBlockElement::GetList(
            ["ID" => "ASC"],
            [
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                //"PROPERTY_H_COMPANY" => 45431
                //"PROPERTY_H_COMPANY" => $vendors
                "PROPERTY_ANYTOS_VENDOR" => $vendors
            ],
        false,
        false,
        ["ID"]
    );

    $allCnt = $rsAll->SelectedRowsCount();
    $p = round(100*$leftBorderCnt/$allCnt, 2);

    // save tmp file
    $arParams["TMP_FILE_NAME"] = $leftBorderCnt / $arParams["LIMIT"] / $arParams["FILE_LIMIT"];
    $arParams["TMP_FILE_NAME"] = round($arParams["TMP_FILE_NAME"]);

    if (File::isFileExists($arParams["UPLOAD_PATH_TMP"] . $arParams["TMP_FILE_NAME"])) {
        File::putFileContents($arParams["UPLOAD_PATH_TMP"] . $arParams["TMP_FILE_NAME"], $jsonExcel . "\n" , File::APPEND);
    }
    else{
        @mkdir($arParams["UPLOAD_PATH"], 0777);
        @mkdir($arParams["UPLOAD_PATH_TMP"], 0777);
        $fp = fopen($arParams["UPLOAD_PATH_TMP"] . $arParams["TMP_FILE_NAME"], "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту),мы создаем файл
        fwrite($fp, $jsonExcel . "\n");
        fclose($fp);
    }

    // Send responce
    // echo shell_exec('php ' . $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/studio7spb.marketplace/admin/download/master_file.create.php ' . $_SERVER["DOCUMENT_ROOT"] . " " . $p . " " . $lastID . " " . $leftBorderCnt);
    echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("MASTER_PROCESS_STATUS", ["ID" => $lastID, "COUNT" => $leftBorderCnt]) .'");';
    die();
}
// </editor-fold>



$clean_test_table = '<table id="result_table" cellpadding="0" cellspacing="0" border="0" width="100%" class="internal">'.
    '<tr class="heading">'.
    '<td>Текущее действие</td>'.
    '<td width="1%">&nbsp;</td>'.
    '</tr>'.
    '</table>';

$aTabs = [
    ["DIV" => "preparatizm", "TAB" => Loc::getMessage("MASTER_FILE_PREPARATIZM")],
];
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle(Loc::getMessage("MASTER_FILE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>


    <form method="post"
          action="<?echo $APPLICATION->GetCurPage()?>"
          name="master_file"
          id="master_file">
        <?
        echo bitrix_sessid_post();

        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>
        <tr>
            <td width="30%">
                <?=Loc::getMessage("VENDORS_LIST")?>
            </td>
            <td width="10">&nbsp;</td>
            <td>

                <table id="vendor_list">
                    <tr>
                        <td><input name="vendors"
                                   id="vendor[0]" value=""
                                   size="5"
                                   type="text"></td>
                        <td><input type="button" value="..."
                                   onClick="jsUtils.OpenWindow('studio7spb.m2_vendor_search.php?n=0', 900, 700);"></td>
                        <td id="sp_vendor[0]"></td>
                    </tr>
                </table>


            </td>
        </tr>

        <?if(!empty($arResult["SECTIONS"])):?>
        <tr>
            <td width="30%">
                <?=Loc::getMessage("MASTER_FILE_SECTION")?>
            </td>
            <td width="10">&nbsp;</td>
            <td>
                <select id="product-section">
                    <option value="0"><?=Loc::getMessage("VENDORS_LIST_ALL")?></option>
                    <?foreach ($arResult["SECTIONS"] as $section):?>
                        <option value="<?=$section["ID"]?>"><?=$section["NAME"]?></option>
                    <?
                    endforeach;
                    unset($arResult["SECTIONS"], $section);
                    ?>
                </select>
            </td>
        </tr>
        <?endif;?>

        <tr>
            <td width="30%">
                <?=Loc::getMessage("MASTER_FILE_PICTURES")?>
            </td>
            <td width="10">&nbsp;</td>
            <td>
                <p>
                    <lable>
                        <input type="radio"
                               name="pictures"
                               checked
                               value="N">
                        <?=Loc::getMessage("MASTER_FILE_PICTURES_N")?>
                    </lable>
                </p>

                <p>
                    <lable>
                        <input type="radio"
                               name="pictures"
                               value="Y">
                        <?=Loc::getMessage("MASTER_FILE_PICTURES_Y")?>
                    </lable>
                </p>
            </td>
        </tr>

        <tr>
            <td colspan="3">

                <input type=button class="adm-btn-save" value="<?=Loc::getMessage("MASTER_PROCESS_START")?>" id="work_start" onclick="set_start(1)" />
                <input type=button value="<?=Loc::getMessage("MASTER_PROCESS_END")?>" disabled id="work_stop" onclick="bSubmit=false;set_start(0)" />
                <div id="progress" style="display:none;" width="100%">
                    <br />
                    <div id="status"></div>
                    <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td height="10">
                                <div style="border:1px solid #B9CBDF">
                                    <div id="indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
                                </div>
                            </td>
                            <td width=30>&nbsp;<span id="percent">0%</span></td>
                        </tr>
                    </table>
                </div>
                <div id="result" style="padding-top:10px"></div>

            </td>
        </tr>


        <?
        $tabControl->End();
        ?>
    </form>

    <script type="text/javascript">

        function getCheckedBoxesIds() {
            var vendors = getCheckedBoxes("vendors"),
                values = [],
                i;

            if(!!vendors && vendors.length > 0){
                for(i=0; i < vendors.length; i++){
                    values[i] = vendors[i].value;
                }
            }

            return values;
        }

        // Pass the checkbox name to the function
        function getCheckedBoxes(chkboxName) {
            var checkboxes = document.getElementsByName(chkboxName);
            var checkboxesChecked = [];
            // loop over them all
            for (var i=0; i<checkboxes.length; i++) {
                // And stick the checked ones onto an array...
                if (parseInt(checkboxes[i].value) > 0) {
                    checkboxesChecked.push(checkboxes[i]);
                }
            }
            // Return the array if it is non-empty, or null
            return checkboxesChecked.length > 0 ? checkboxesChecked : null;
        }




        // tabControl_buttons_div
        // disable creativizm
        //tabControl.DisableTab("creativizm");

        var bWorkFinished = false;
        var bSubmit;

        function set_start(val)
        {
            document.getElementById('work_start').disabled = val ? 'disabled' : '';
            document.getElementById('work_stop').disabled = val ? '' : 'disabled';
            document.getElementById('progress').style.display = val ? 'block' : 'none';

            if (val)
            {
                var vendors = getCheckedBoxesIds(),
                    sectionID = 0,
                    noParams = true;

                if(!!vendors && vendors.length > 0){
                    vendors = vendors.join();
                    vendors = "&vendors=" + vendors
                    noParams = false;
                }else{
                    vendors = "";
                }

                sectionID = document.getElementById("product-section");
                sectionID = sectionID.options[sectionID.selectedIndex].value;

                if(sectionID > 0){
                    noParams = false;
                }


                if(noParams == true){
                    // Не выбраны параметры выгрузки
                    CloseWaitWindow();
                    alert('<?=Loc::getMessage("MASTER_NO_PARAMS_ERROR")?>');
                }else{
                    ShowWaitWindow();
                    document.getElementById('result').innerHTML = '<?=$clean_test_table?>';
                    document.getElementById('status').innerHTML = '<?=Loc::getMessage("MASTER_PROCESS_ACTIVE")?>';

                    document.getElementById('percent').innerHTML = '0%';
                    document.getElementById('indicator').style.width = '0%';

                    var usePictures =document.querySelector('input[name="pictures"]:checked').value;

                    CHttpRequest.Action = work_onload;
                    CHttpRequest.Send('<?=$arParams["ACTION"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>' + vendors + '&usePictures=' + usePictures + '&sectionID=' + sectionID);
                }
            }
            else
                CloseWaitWindow();
        }

        function work_onload(result)
        {
            try
            {
                eval(result);

                iPercent = CurrentStatus[0];
                strNextRequest = CurrentStatus[1];
                strCurrentAction = CurrentStatus[2];

                var vendors = getCheckedBoxesIds();
                if(!!vendors && vendors.length > 0){
                    vendors = vendors.join();
                    vendors = "&vendors=" + vendors
                }else{
                    vendors = "";
                }

                var sectionID = document.getElementById("product-section");
                sectionID = sectionID.options[sectionID.selectedIndex].value;

                if(sectionID > 0){
                    noParams = false;
                }

                document.getElementById('percent').innerHTML = iPercent + '%';
                document.getElementById('indicator').style.width = iPercent + '%';

                document.getElementById('status').innerHTML = '<?=Loc::getMessage("MASTER_PROCESS_ACTIVE")?>';

                if (strCurrentAction != 'null')
                {
                    oTable = document.getElementById('result_table');
                    if(oTable.rows.length > 10){
                        oTable.deleteRow(10);
                    }


                    oRow = oTable.insertRow(1);
                    oCell = oRow.insertCell(-1);
                    oCell.innerHTML = strCurrentAction;
                    oCell = oRow.insertCell(-1);
                    oCell.innerHTML = '';
                }

                if (strNextRequest && document.getElementById('work_start').disabled){
                    var usePictures =document.querySelector('input[name="pictures"]:checked').value;
                    CHttpRequest.Send('<?=$arParams["ACTION"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>' + strNextRequest + vendors + '&usePictures=' + usePictures + '&sectionID=' + sectionID);
                }
                else
                {
                    //set_start(0);
                    // Enable creativizm
                    //tabControl.EnableTab("creativizm");

                    creativizm();
                    bWorkFinished = true;
                }

            }
            catch(e)
            {
                CloseWaitWindow();
                document.getElementById('work_start').disabled = '';
                alert('<?=Loc::getMessage("MASTER_DATA_ERROR")?>');
            }
        }


        function creativizm() {
            //CloseWaitWindow();

            if (document.getElementById('work_start').disabled){
                //CHttpRequest.Send('<?=$arParams["ACTION"]?>?lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>&action=create_mater_file');


                BX.ajax.post(
                    '<?=$arParams["ACTION"]?>',
                    {
                        action: 'create_mater_file',
                        sessid: BX.bitrix_sessid(),
                        lang: '<?=LANGUAGE_ID?>'
                    },
                    function (result) {
                        set_start(0);
                        eval(result);

                        iPercent = CurrentStatus[0];
                        strNextRequest = CurrentStatus[1];
                        strCurrentAction = CurrentStatus[2];

                        oTable = document.getElementById('result_table');
                        if(oTable.rows.length > 10){
                            oTable.deleteRow(10);
                        }


                        oRow = oTable.insertRow(1);
                        oCell = oRow.insertCell(-1);
                        oCell.innerHTML = strCurrentAction;
                        oCell = oRow.insertCell(-1);
                        oCell.innerHTML = '';

                    }
                );

            }

        }
    </script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>