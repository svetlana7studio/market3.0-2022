<?
/**
 * Import settings page
 * @var CMain $APPLICATION
 */
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application,
    Bitrix\Main\IO\File;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadLanguageFile(__FILE__);

$request = Application::getInstance()->getContext()->getRequest();

$arParams = [
    "PATH" => $_SERVER["DOCUMENT_ROOT"] . "/include/priceInfo.popup.php"
];

// <editor-fold defaultstate="collapsed" desc="Set content to file">


if($request->isPost() && is_set($request->getPost("PREVIEW_TEXT"))){
    /**
     * TODO post
     */
    $PREVIEW_TEXT = $request->getPost("PREVIEW_TEXT");
    $PREVIEW_TEXT = trim($PREVIEW_TEXT);
    File::putFileContents($arParams["PATH"], $PREVIEW_TEXT);

    LocalRedirect($APPLICATION->GetCurPageParam("result=success", ["action", "result"]));
}

$PREVIEW_TEXT = File::getFileContents($arParams["PATH"]);

// </editor-fold>


$APPLICATION->SetTitle( Loc::getMessage("TITLE") );
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// result=success
if($request->get("result") == "success"){
    echo CAdminMessage::ShowMessage([
        "MESSAGE" => Loc::getMessage("SUCCESS_MESSAGE"),
        "HTML"=>true,
        "TYPE"=>"OK",
    ]);
}
?>

<form method="POST" name="frm" id="frm">
<?
echo bitrix_sessid_post();
$aTabs = [
    [
        "DIV" => "edit1",
        "TAB" => GetMessage("TITLE"),
        "ICON" => "iblock",
        "TITLE" => GetMessage("TITLE"),
    ]
];

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();

$tabControl->BeginNextTab();

$APPLICATION->IncludeComponent(
    "studio7sbp:lhe",
    "",
    Array(
        "LHE_NAME" => "lhe_preview_text_form",
        "LHE_ID" => "lhe_preview_text_form",
        "INPUT_NAME" => "PREVIEW_TEXT",
        "INPUT_VALUE" => $PREVIEW_TEXT,
    ),
    $component,
    Array("HIDE_ICONS" => "Y")
);

$tabControl->Buttons(array());
$tabControl->End();
?>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>