<?
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @global CUserTypeManager $USER_FIELD_MANAGER */

use Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc,
    Studio7spb\Marketplace\VendorTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
$request = Application::getInstance()->getContext()->getRequest();

$tableId = 'vendor_search';
$sTableID = "vendor_search";
$n = $request->get("n");
$n = intval($n);

$entity_id = "VENDOR";

$oSort = new CAdminSorting($sTableID, "ID", "ASC");
if (!isset($by))
    $by = 'ID';
if (!isset($order))
    $order = 'ASC';
$arOrder = (mb_strtoupper($by) === "ID"? array($by => $order): array($by => $order, "ID" => "ASC"));
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
    "find_section_id",
    "find_section_old_id",
    "find_section_name",
    "find_section_active",
);

if($entity_id)
    $USER_FIELD_MANAGER->AdminListAddFilterFields($entity_id, $arFilterFields);

$lAdmin->InitFilter($arFilterFields);

if($request->isPost()){
    $find_vendor_id = htmlspecialcharsbx($request->get("find_vendor_id"));
    $find_vendor_old_id = htmlspecialcharsbx($request->get("find_vendor_old_id"));
    $find_vendor_name = htmlspecialcharsbx($request->get("find_vendor_name"));
    $find_vendor_active = $request->get("find_vendor_active");

    $arFilter = [
        "?title"     => $find_vendor_name,
        "id"	    => $find_vendor_id,
        "old_id"	    => $find_vendor_old_id,
        "active"	=> $find_vendor_active,
    ];

    // validate id
    if(intval($find_vendor_id) <= 0){
        unset($find_vendor_id);
        unset($arFilter["id"]);
    }
    // validate id
    if(intval($find_vendor_old_id) <= 0){
        unset($find_vendor_old_id);
        unset($arFilter["old_id"]);
    }
    // validate active
    switch ($find_vendor_active){
        case "1":
            $find_vendor_active = 1;
            $arFilter["active"] = 1;
            break;
        case "0":
            $find_vendor_active = 0;
            $arFilter["active"] = 0;
            break;
        case "all":
        case "":
        case null:
        case false:
            unset($find_vendor_active);
            unset($arFilter["active"]);
            break;
    }
}




if($entity_id)
    $USER_FIELD_MANAGER->AdminListAddFilter($entity_id, $arFilter);

CModule::AddAutoloadClasses(
    "studio7spb.marketplace",
    array(
        "\\Studio7spb\\Marketplace\\VendorTable" => "lib/anytos/vendor.php"
    )
);
if($request->isPost()){
    $rsData = VendorTable::getList([
        "filter" => $arFilter
    ]);
}else{
    $rsData = VendorTable::getList();
}


$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText( $rsData->GetNavPrint( GetMessage("GRUPPER_PAGE_NAVI") ) );

$headers = array(
    array("id" => "id", "content" => Loc::getMessage("VENDOR_ID"), "sort" => "id", "default" => true),
    array("id" => "old_id", "content" => Loc::getMessage("VENDOR_OLD_ID"), "sort" => "old_id", "default" => true),
    array("id" => "title", "content" => Loc::getMessage("VENDOR_NAME"), "sort" => "name", "default" => true),
    array("id" => "active", "content" => Loc::getMessage("VENDOR_ACTIVE"), "sort" => "active", "default" => true)
);

if($entity_id)
    $USER_FIELD_MANAGER->AdminListAddHeaders($entity_id, $headers);
$lAdmin->AddHeaders($headers);

while($vendor = $rsData->fetch()){
    $row =& $lAdmin->AddRow($vendor["id"], $vendor);
    $row->AddViewField("id", $vendor["id"]);
    $row->AddViewField("old_id", $vendor["old_id"]);
    $row->AddViewField("title", $vendor["title"] . ' <div style="display:none" id="name_'.$vendor["id"].'">'.$vendor["title"].'</div>');
    $vendor["active"] = $vendor["active"] == 1 ? Loc::getMessage("VENDOR_ACTIVE_YES") : Loc::getMessage("VENDOR_ACTIVE_NO");
    $row->AddViewField("active", $vendor["active"]);

    $row->AddActions(array(
        array(
            "DEFAULT" => "Y",
            "TEXT" => Loc::getMessage("VENDOR_SELECT"),
            "ACTION"=>"javascript:SelEl('" . $vendor["id"] . "', '" . htmlspecialcharsbx($vendor["title"]) . "')",
        ),
    ));
}

/*
$lAdmin->AddGroupActionTable(array(
    array(
        "action" => "SelAll()",
        "value" => "select",
        "type" => "button",
        "name" => Loc::getMessage("VENDOR_SELECT_CHOOSED")
    )
), array("disable_action_target"=>true));
*/

/***************************************************************************
HTML form
 ****************************************************************************/
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_popup_admin.php");

$chain = new CAdminChain("main_navchain");
$chain->AddItem(array(
    "TEXT" => "AddItem",
    "LINK" => "#",
    "ONCLICK" => $lAdmin->ActionRedirect('/').';return false;',
));
$chain->Show();

$oFilter = new CAdminFilter($sTableID."_filter", $arFindFields);
?>
<form method="post" name="find_vendor_form" action="<?echo $APPLICATION->GetCurPage()?>">
    <?
    $oFilter->Begin();
    ?>
    <tr>
        <td><b><?=Loc::getMessage("VENDOR_ID")?>:</b></td>
        <td><input type="text" name="find_vendor_id" value="<?echo htmlspecialcharsbx($find_vendor_id)?>" size="47"></td>
    </tr>
    <tr>
        <td><b><?=Loc::getMessage("VENDOR_OLD_ID")?>:</b></td>
        <td><input type="text" name="find_vendor_old_id" value="<?echo htmlspecialcharsbx($find_vendor_old_id)?>" size="47"></td>
    </tr>
    <tr>
        <td><?=Loc::getMessage("VENDOR_NAME")?>:</td>
        <td><input type="text" name="find_vendor_name" size="47" value="<?echo htmlspecialcharsbx($find_vendor_name)?>">&nbsp;<?=ShowFilterLogicHelp()?></td>
    </tr>
    <tr>
        <td><?echo GetMessage("VENDOR_ACTIVE")?>:</td>
        <td>
            <select name="find_vendor_active" >
                <option value="all"><?=Loc::getMessage("VENDOR_ALL")?></option>
                <option value="1"<?if($find_vendor_active=="1")echo " selected"?>><?=htmlspecialcharsbx(GetMessage("VENDOR_ACTIVE_YES"))?></option>
                <option value="0"<?if($find_vendor_active=="0")echo " selected"?>><?=htmlspecialcharsbx(GetMessage("VENDOR_ACTIVE_NO"))?></option>
            </select>
        </td>
    </tr>
    <?
    //$USER_FIELD_MANAGER->AdminListShowFilter($entity_id);
    $oFilter->Buttons();
    ?>
    <span class="adm-btn-wrap"><input type="submit"  class="adm-btn" name="set_filter" value="<? echo GetMessage("admin_lib_filter_set_butt"); ?>" title="<? echo GetMessage("admin_lib_filter_set_butt_title"); ?>" onclick="return applyFilter(this);"></span>
    <span class="adm-btn-wrap"><input type="submit"  class="adm-btn" name="del_filter" value="<? echo GetMessage("admin_lib_filter_clear_butt"); ?>" title="<? echo GetMessage("admin_lib_filter_clear_butt_title"); ?>" onclick="return deleteFilter(this);"></span>
    <?
    $oFilter->End();
    ?>
</form>
<?
$lAdmin->DisplayList();
?>
<script>

    // vendorChooserCount

    function SelAll()
    {
        var frm = BX('form_<?echo $sTableID?>'),
            e,
            v,
            n,
            i;

        if(frm)
        {
            e = frm.elements['ID[]'];
            if(e && e.nodeName)
            {
                v = e.value;
                n = BX('name_'+v).innerHTML;
                SelEl(v, n);
            }
            else if(e)
            {
                for(i=0;i<e.length;i++)
                {
                    if (e[i].checked)
                    {
                        v = e[i].value;
                        n = BX('name_'+v).innerHTML;
                        SelEl(v, n);
                    }
                }
            }
            window.close();
        }
    }

    function SelEl(id, name)
    {
        el = window.opener.document.getElementById('vendor[<?=$n?>]');
        el.value = id;

        el = window.opener.document.getElementById('sp_vendor[<?=$n?>]');
        if(el)
            el.innerHTML = name;

        // vendor_list

       var vendor_list = window.opener.document.getElementById('vendor_list'),
           hasEmptyInput = false,
           input,
           i;

       if(vendor_list && vendor_list.rows.length > 0){
           for (i=0; i < vendor_list.rows.length; i++){
               input = vendor_list.rows[i].getElementsByTagName('input');
               if(input[0].value <= 0){
                   hasEmptyInput = true;
               }
           }
       }

       if(hasEmptyInput == false)
       {
           addNewVendorChooser();
       }
       window.close();
    }

    function addNewVendorChooser()
    {
        var oTable = window.opener.document.getElementById('vendor_list'),
            i = oTable.rows.length,
            obInput;

        obInput = BX.create('INPUT', {
            props: {
                type: 'text',
                name: 'vendors',
                id: 'vendor[' + i + ']',
                size: 5,
                value: ''
            }
        });
        oRow = oTable.insertRow(i);
        oCell = oRow.insertCell(-1);
        oCell.appendChild(obInput);

        obInput = BX.create('INPUT', {
            props: {
                type: 'button',
                value: '...'
            },
            attrs: {onclick: "jsUtils.OpenWindow('studio7spb.m2_vendor_search.php?n=" + i + "', 900, 700);"}
        });
        oCell = oRow.insertCell(-1);
        oCell.appendChild(obInput);

        oCell = oRow.insertCell(-1);
        oCell.id = "sp_vendor[" + i + "]";

    }
</script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_popup_admin.php");