<?
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Studio7spb\Marketplace\CMarketplaceOptions,
    Bitrix\Main\Application;

if (isset($_REQUEST['work_start']))
{
    define("NO_AGENT_STATISTIC", true);
    define("NO_KEEP_STATISTIC", true);
}

$arParams = [
    "MODULES" => ["iblock"], // "studio7spb.marketplace",
    "VENDOR_IBLOCK_ID" => null,
    "VENDOR_ID" => null,
    "CATALOG_IBLOCK_ID" => null,
    "LIMIT" => 1,
    "PRICE_TYPE_ID" => 3
];

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
if(!empty($arParams["MODULES"])){
    foreach ($arParams["MODULES"] as $value) {
        Loader::includeModule($value);
    }
}
Loc::loadLanguageFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

$request = Application::getInstance()->getContext()->getRequest();
$arParams["VENDOR_ID"] = intval($request->get("vendor"));
if($arParams["VENDOR_ID"] <= 0){
    LocalRedirect("studio7spb.marketplace_vendor_extra_list.php");
}
$arParams["VENDOR_IBLOCK_ID"] = CMarketplaceOptions::getInstance()->getOption("company_iblock_id");
$arParams["CATALOG_IBLOCK_ID"] = CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id");

$arResult = [];

$arResult["VENDOR"] = CIBlockElement::GetList(
    [],
    [
        "IBLOCK_ID" => $arParams["VENDOR_IBLOCK_ID"],
        "ID" => $arParams["VENDOR_ID"],
    ],
    false,
    false,
    [
        "ID",
        "NAME",
        "IBLOCK_ID",
        "PROPERTY_COMP_COMMISSION"
    ]
);
$arResult["VENDOR"] = $arResult["VENDOR"]->Fetch();

if($request->get("work_start") && check_bitrix_sessid())
{
    $rsEl = CIBlockElement::GetList(
            ["ID" => "ASC"],
            [
                "IBLOCK_ID" => $arParams["CATALOG_IBLOCK_ID"],
                ">ID" => $request->get("lastid"),
                "PROPERTY_H_COMPANY" => $arParams["VENDOR_ID"]
            ],
            false,
            ["nTopCount" => $arParams["LIMIT"]],
            ["ID", "PROPERTY_FOB_RMB"]
    );
    while ($arEl = $rsEl->Fetch())
    {
        /*
         * change normal price
         */
        //AddMessage2Log($arEl);
        //AddMessage2Log($arResult["VENDOR"]["PROPERTY_COMP_COMMISSION_VALUE"]);
        $COMP_COMMISSION = $request->get("COMP_COMMISSION");
        if(is_numeric($COMP_COMMISSION) && $COMP_COMMISSION > 0){
            $res = CPrice::GetList(
                [],
                [
                    "PRODUCT_ID" => $arEl["ID"],
                    "CATALOG_GROUP_ID" => $arParams["PRICE_TYPE_ID"]
                ]
            );

            if ($arr = $res->Fetch())
            {
                //$arr["PRICE"] = $arr["PRICE"] * (1 - floatval($arResult["VENDOR"]["PROPERTY_COMP_COMMISSION_VALUE"]) * 0.01);
                //$arr["PRICE"] = $arr["PRICE"] / (1 - floatval($COMP_COMMISSION) * 0.01);
                $arr["PRICE"] = $arEl["PROPERTY_FOB_RMB_VALUE"] / (1 - floatval($COMP_COMMISSION) * 0.01);
                CPrice::Update($arr["ID"], ["PRICE" => $arr["PRICE"]]);
            }
        }



        $lastID = intval($arEl["ID"]);
    }

    $rsLeftBorder = CIBlockElement::GetList(
            ["ID" => "ASC"],
            [
                "IBLOCK_ID" => $arParams["CATALOG_IBLOCK_ID"],
                "PROPERTY_H_COMPANY" => $arParams["VENDOR_ID"],
                "<=ID" => $lastID
            ],
            false,
            false,
            ["ID"]
        );
    $leftBorderCnt = $rsLeftBorder->SelectedRowsCount();

    $rsAll = CIBlockElement::GetList(
            ["ID" => "ASC"],
            [
                "IBLOCK_ID" => $arParams["CATALOG_IBLOCK_ID"],
                "PROPERTY_H_COMPANY" => $arParams["VENDOR_ID"],
            ],
        false,
        false,
        ["ID"]
    );
    $allCnt = $rsAll->SelectedRowsCount();

    $p = round(100*$leftBorderCnt/$allCnt, 2);

    if($p == 100){
        echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("VENDOR_PROGRES_STATUS_FINISH") . '");';
        // COMP_COMMISSION
        $COMP_COMMISSION = $request->get("COMP_COMMISSION");
        CIBlockElement::SetPropertyValuesEx($arParams["VENDOR_ID"], $arParams["VENDOR_IBLOCK_ID"], ["COMP_COMMISSION" => $COMP_COMMISSION]);
    }else{
        echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("VENDOR_PROGRES_STATUS_MESSAGE", ["ID" => $lastID]) . '");';
    }

    die();
}

$clean_test_table = '<table id="result_table" cellpadding="0" cellspacing="0" border="0" width="100%" class="internal">'.
    '<tr class="heading">'.
    '<td>' . Loc::getMessage("VENDOR_PROGRES_STATUS") . '</td>'.
    '</tr>'.
    '<tr>'.
    '<td id="result_table_status"></td>'.
    '</tr>'.
    '</table>';

$aTabs = array(array("DIV" => "edit1", "TAB" => Loc::getMessage("VENDOR_PROGRES_TAB", ["NAME" => $arResult["VENDOR"]["NAME"]])));
$tabControl = new CAdminTabControl("tabControl", $aTabs);
$APPLICATION->SetTitle(Loc::getMessage("VENDOR_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
    <script type="text/javascript">

        var bWorkFinished = false;
        var bSubmit;

        function set_start(val)
        {
            document.getElementById('work_start').disabled = val ? 'disabled' : '';
            document.getElementById('progress').style.display = val ? 'block' : 'none';
            var COMP_COMMISSION = document.getElementById('COMP_COMMISSION').value;

            if (val)
            {
                ShowWaitWindow();
                document.getElementById('result').innerHTML = '<?=$clean_test_table?>';
                document.getElementById('status').innerHTML = '<?=Loc::getMessage("VENDOR_PROGRES_WORK")?>';

                document.getElementById('percent').innerHTML = '0%';
                document.getElementById('indicator').style.width = '0%';

                CHttpRequest.Action = work_onload;
                CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>&vendor=<?=$arParams["VENDOR_ID"]?>&COMP_COMMISSION=' + COMP_COMMISSION);
            }
            else
                CloseWaitWindow();
        }

        function work_onload(result)
        {
            try
            {
                eval(result);

                iPercent = CurrentStatus[0];
                strNextRequest = CurrentStatus[1];
                strCurrentAction = CurrentStatus[2];
                var COMP_COMMISSION = document.getElementById('COMP_COMMISSION').value;

                document.getElementById('percent').innerHTML = iPercent + '%';
                document.getElementById('indicator').style.width = iPercent + '%';

                document.getElementById('status').innerHTML = '<?=Loc::getMessage("VENDOR_PROGRES_WORK")?>';

                if (strCurrentAction != 'null')
                {
                    oStatus = document.getElementById('result_table_status');
                    oStatus.innerHTML = strCurrentAction;
                }

                if (strNextRequest && document.getElementById('work_start').disabled)
                    CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>&vendor=<?=$arParams["VENDOR_ID"]?>&COMP_COMMISSION=' + COMP_COMMISSION + strNextRequest);
                else
                {
                    set_start(0);
                    bWorkFinished = true;
                }

            }
            catch(e)
            {
                CloseWaitWindow();
                document.getElementById('work_start').disabled = '';
                alert('Сбой в получении данных');
            }
        }

    </script>

    <form method="post" action="<?echo $APPLICATION->GetCurPage()?>" enctype="multipart/form-data" name="post_form" id="post_form">
        <?
        echo bitrix_sessid_post();

        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>
        <tr>
            <td width="40%" align=""><?=Loc::getMessage("VENDOR_TITLE_VALUE")?></td>
            <td>
                <input type="number"
                       min="1"
                       max="100"
                       step="0.01"
                       id="COMP_COMMISSION"
                       name="COMP_COMMISSION"
                       value="<?=$arResult["VENDOR"]["PROPERTY_COMP_COMMISSION_VALUE"]?>">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type=button value="<?=Loc::getMessage("VENDOR_PROGRES_START")?>" id="work_start" onclick="set_start(1)" />
                <div id="progress" style="display:none;" width="100%">
                    <br />
                    <div id="status"></div>
                    <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td height="10">
                                <div style="border:1px solid #B9CBDF">
                                    <div id="indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
                                </div>
                            </td>
                            <td width=30>&nbsp;<span id="percent">0%</span></td>
                        </tr>
                    </table>
                </div>
                <div id="result" style="padding-top:10px"></div>

            </td>
        </tr>
        <?
        $tabControl->End();
        ?>
    </form>

    <?=BeginNote();?>
    <span class="adm-lamp adm-lamp-green"></span> <?=Loc::getMessage("VENDOR_PROGRES_NOTE1")?><br>
    <span class="adm-lamp adm-lamp-yellow"></span> <?=Loc::getMessage("VENDOR_PROGRES_NOTE2")?><br>
    <span class="adm-lamp adm-lamp-red"></span> <?=Loc::getMessage("VENDOR_PROGRES_NOTE3")?>
    <?=EndNote();?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>