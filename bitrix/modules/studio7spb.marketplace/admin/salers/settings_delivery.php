<?
use Bitrix\Main,
	Bitrix\Main\Loader;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/prolog.php");
IncludeModuleLangFile(__FILE__);

$moduleId = 'studio7spb.marketplace';
Loader::includeModule($moduleId);

if ($REQUEST_METHOD=="POST" && strlen($Update)>0 && check_bitrix_sessid()) {
	if(strlen($_POST["DESCRIPTION"]) > 0) {
		COption::SetOptionString($moduleId, "settings_product_delivery_text", $_POST["DESCRIPTION"]);
	}
}
$optionDescription = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOption("settings_product_delivery_text");


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

Loader::includeModule("fileman");
?>

<form method="POST" name="frm" id="frm">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="Update" value="Y">
	<?
	$aTabs = array(
		array(
			"DIV" => "edit1",
			"TAB" => GetMessage("MP_S_D_TAB1"),
			"ICON" => "iblock",
			"TITLE" => GetMessage("MP_S_D_TAB1_T"),
		)
	);

	$tabControl = new CAdminTabControl("tabControl", $aTabs);
	$tabControl->Begin();

	$tabControl->BeginNextTab();
	?>

		<tr class="heading">
			<td colspan="2"><?echo GetMessage("MP_S_D_DESCRIPTION")?></td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<?CFileMan::AddHTMLEditorFrame(
					"DESCRIPTION",
					$optionDescription,
					"DESCRIPTION_TYPE",
					"html",
					array(
						'height' => 450,
						'width' => '100%'
					)
				);?>
			</td>
		</tr>
	<?
	$tabControl->Buttons(array());
	$tabControl->End();
?>
</form>
<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>