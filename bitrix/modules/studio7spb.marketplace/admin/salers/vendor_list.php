<?
/**
 * Import settings page
 * @var CMain $APPLICATION
 */
use Bitrix\Main\Localization\Loc,
    \Studio7spb\Marketplace\VendorTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

// <editor-fold defaultstate="collapsed" desc=" # Load orm vendor">
CModule::AddAutoloadClasses(
    "studio7spb.marketplace",
    array(
        "\\Studio7spb\\Marketplace\\VendorTable" => "lib/anytos/vendor.php"
    )
);
// </editor-fold>

Loc::loadLanguageFile(__FILE__);

$arParams = [

];
// <editor-fold defaultstate="collapsed" desc="Get Table data">

$sTableID = "anytos_vendor";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);
$rsData = VendorTable::getList($arParams);
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText( $rsData->GetNavPrint( GetMessage("GRUPPER_PAGE_NAVI") ) );

$headers = [];
$headersSkip = ["LID"];
foreach (VendorTable::getMap() as $code=>$head){
    if(in_array($code, $headersSkip)){
        continue;
    }
    $headers[] = ["id" => $code, "content" => $head["title"], "sort" => "ID", "default" => true];
}

$lAdmin->AddHeaders($headers);
while ($element = $rsData->Fetch())
{
    $row =& $lAdmin->AddRow($element["ID"], $element);
    $row->AddViewField("title", $element["title"]);
}
// </editor-fold>


// ******************************************************************** //
//                ВЫВОД                                                 //
// ******************************************************************** //

// альтернативный вывод
$lAdmin->CheckListMode();

// установим заголовок страницы
$APPLICATION->SetTitle( Loc::getMessage("TITLE") );

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?
// выведем таблицу списка элементов
$lAdmin->DisplayList();
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>