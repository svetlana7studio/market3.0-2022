<?

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

use \Bitrix\Main\Loader;
use \Studio7spb\Marketplace\CMarketplaceTools;

$sAction = $_REQUEST["action"];

switch ($sAction) {
	case 'getSalesProductPage':
		$iSalersId = (int)$_REQUEST["salerId"];
		Loader::includeModule("studio7spb.marketplace");
		$sIblockType = "marketplace";
		$iIblockId = 2;
		$iIblockPropertyId = 5;

		$aProperty = array('id' => $iIblockPropertyId, 'value' => $iSalersId, 'name' => 'ACME Corp');

		CMarketplaceTools::setIblockUiFilter($sIblockType, $iIblockId, $aProperty);

		LocalRedirect('/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=marketplace&lang=ru&find_el_y=Y');

	break;
	
	default:
		die();
	break;
}
?>