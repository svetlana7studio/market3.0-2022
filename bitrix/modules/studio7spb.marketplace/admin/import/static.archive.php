<?
/**
 * Import settings page
 * @var CMain $APPLICATION
 */
use Bitrix\Main\Localization\Loc,
    Studio7spb\Marketplace\StaticTable;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadLanguageFile(__FILE__);

$arParams = [
    "TITLES" => [
        "AP" => "За куб срочная",
        "AO" => "За куб стнд",
        "AZ1" => "Юаней за доллар",
        "AK" => "Наценка для рассчета цены FOB $ в процентах",
        "AI" => "таможенные платежи",
        "AJ" => "НДС",
        "AM" => "Нац на DDP срочная",
        "AL" => "Нац на DDP стандарт",
    ]
];


CModule::AddAutoloadClasses(
    "studio7spb.marketplace",
    array(
        "\\Studio7spb\\Marketplace\\StaticTable" => "lib/lansy/static.php"
    )
);


// <editor-fold defaultstate="collapsed" desc="Get Table data">

$sTableID = "import_calculate";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);
$rsData = StaticTable::getList();
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText( $rsData->GetNavPrint( GetMessage("GRUPPER_PAGE_NAVI") ) );

$headers = [];
$headersSkip = ["LID", "NAME"];
foreach (StaticTable::getMap() as $code=>$head){
    if(in_array($code, $headersSkip)){
        continue;
    }
    $headers[] = ["id" => $code, "content" => $head["title"], "sort" => "ID", "default" => true];
}

$lAdmin->AddHeaders($headers);

$dl = null;
$showData = ["AZ1", "AO", "AP", "AK"];
while ($element = $rsData->Fetch())
{
    $row =& $lAdmin->AddRow($element["ID"], $element);
    $element["DATA"] = unserialize($element["DATA"]);

    $dl = '<table border="1" style="text-align: center;">';
    $dl .= "<thead>";
    $dl .= "<tr>";
    foreach ($element["DATA"] as $code=>$data){
        if(!in_array($code, $showData)){
            continue;
        }
        $title = $code;
        if(is_set($arParams["TITLES"][$code])){
            $title = $arParams["TITLES"][$code] . " (" . $code . ")";
        }

        $dl .= "<th width='10%'>" . $title . "</th>";


    }
    $dl .= "</tr>";
    $dl .= "</thead>";
    $dl .= "</tbody>";
    $dl .= "<tr>";
    foreach ($element["DATA"] as $code=>$data){
        if(!in_array($code, $showData)){
            continue;
        }
        $dl .= "<td>" . $data . "</td>";
    }
    $dl .= "</tr>";
    $dl .= "</tbody>";
    $dl .= '</table>';


    $row->AddViewField("DATA", $dl);
}
// </editor-fold>


// ******************************************************************** //
//                ВЫВОД                                                 //
// ******************************************************************** //

// альтернативный вывод
$lAdmin->CheckListMode();

// установим заголовок страницы
$APPLICATION->SetTitle( Loc::getMessage("TITLE") );

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?
// выведем таблицу списка элементов
$lAdmin->DisplayList();
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>