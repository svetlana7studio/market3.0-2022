<?
use Bitrix\Main\Localization\Loc,
    Studio7spb\Marketplace\ImportTable,
    Bitrix\Highloadblock,
    Bitrix\Catalog\Model\Product,
    Bitrix\Iblock\SectionTable,
    Studio7spb\Marketplace\ImportSettingsTable;

$arParams["SKU"] = [
    "CML2_LINK" => 196
];

$importSettings = [];
$db = ImportSettingsTable::getList([
    //"filter" => ["%CODE" => "IMPORT_"]
    "filter" => [
        "CODE" => [
            "IMPORT_FIELDS_LIST",
            "IMPORT_DETECT_FIELD",
            "IMPORT_FIELDS_LIST"
        ]
    ]
]);
while($importSetting = $db->fetch()){

    switch ($importSetting["CODE"]){
        case "IMPORT_FIELDS_LIST":
        case "IMPORT_DETECT_FIELD":
            $importSetting["VALUE"] = unserialize($importSetting["VALUE"]);
            break;
    }

    $importSettings[$importSetting["CODE"]] = $importSetting["VALUE"];
}

// parent section
$parentSection = 0;
$parentSection = SectionTable::getList(array(
    "filter" => array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ID" => $arResult["SETTINGS"]["IMPORT_DATA_VENDOR"]["VALUE"]
    ),
    "select" => array("ID")
));
if($parentSection = $parentSection->fetch()){
    $parentSection = $parentSection["ID"];
}

// импорт из временной таблицы в инфоблок
$elements = ImportTable::getList(array(
    "order" => array("ID" => "ASC"),
    "filter" => array(
        ">ID" => $_REQUEST["lastid"]
    ),
    "limit" => $arParams["LIMIT"]
));

$arProducts = [];
while ($element = $elements->fetch())
{

    // get tmp data
    $element["DATA"] = unserialize($element["DATA"]);
    //check iblock element add or update

    $arFilter = [
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    ];

    $arFields = [];
    $arProperties = [];
    $arPrices = [];
    $arCatalog = [];

    // не задана идентификация
    if(empty($importSettings["IMPORT_DETECT_FIELD"])){
        //$arFilter["XML_ID"] = "never sache this product";
    }else{
        foreach ($importSettings["IMPORT_DETECT_FIELD"] as $field)
        {
            $arFilter[$field] = "";
        }
    }

    foreach ($importSettings["IMPORT_FIELDS_LIST"] as $letter=>$fields)
    {
        foreach ($fields as $field){

            if(isset($arFilter[substr($field, 3)])){
                $arFilter[substr($field, 3)] = $element["DATA"][$letter];
            }

            if(isset($arFilter["PROPERTY_" . substr($field, 7)])){
                $arFilter["PROPERTY_" . substr($field, 7)] = $element["DATA"][$letter];
            }

            if(!empty($field)) {
                // Набор полей
                if(substr($field, 0, 3) === "IE_"){
                    $code = substr($field, 3);
                    $arFields[$code] = $element["DATA"][$letter];
                    // PREVIEW_PICTURE DETAIL_PICTURE
                    if($code == "PREVIEW_PICTURE" || $code == "DETAIL_PICTURE")
                    {
                        $arFields[$code] = explode(",", $arFields[$code]);
                        $arFields[$code] = CFile::MakeFileArray($arFields[$code][0]);
                    }

                }
                // properties
                if(substr( $field, 0, 7 ) === "IP_PROP"){
                    $arProperties[substr($field, 7)] = $element["DATA"][$letter];
                }
                // sale prices
                if($field === "ICAT_PRICE1_PRICE"){
                    $arPrices[1] = $element["DATA"][$letter];
                }
                if($field === "ICAT_PRICE2_PRICE"){
                    $arPrices[2] = $element["DATA"][$letter];
                }
                // catalog avaliable
                if(ToUpper($field) == "ICAT_AVAILABLE"){
                    if(ToUpper($element["DATA"][$letter]) == ToUpper($arResult["SETTINGS"]["IMPORT_DATA_CATALOG_AVAILABLE"]["VALUE"])){
                        $arCatalog["AVAILABLE"] = "Y";
                    }
                    else{
                        $arCatalog["AVAILABLE"] = "N";
                    }
                }
                // product tp groopen ICAT_GROUP
                if($field === "ICAT_GROUP"){
                    $arCatalog["TP_GROUP"] = $element["DATA"][$letter];
                }
            }

        }
    }


    /**
     * Все данные с временной таблицы собраны и провалидированы
     * Начинаем процес обновления продукта и его торговых предложений
     */
    $lastID = 0;
    //////////////////////////
    // Update product if exist by data
    $arFields["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
    $arFields["IBLOCK_SECTION_ID"] = $parentSection;
    // если используется поле группировки для торговых предложений - занимаем XML_ID
    if(!empty($arCatalog["TP_GROUP"])){
        $arFields["XML_ID"] = $arCatalog["TP_GROUP"];
    }

    //$arFilter["XML_ID"] = "never sache this product";
    if(empty($importSettings["IMPORT_DETECT_FIELD"])){
        // Add new product by data
        $lastID = setShopFields($arParams, $arFields);
    }else{
        // проверка в каталоге
        $products = CIBlockElement::GetList(
            [],
            $arFilter,
            false,
            false,
            ["ID", "NAME"]
        );

        if($product = $products->fetch())
        {
            // Update product by data
            $lastID = setShopFields($arParams, $arFields, $product["ID"]);
        }
        else {
            // Add new product by data
            $lastID = setShopFields($arParams, $arFields);
        }
    }


    // Добавление свойств
    setShopProperties($arParams, $arProperties, $lastID);
    // Накапливаем продукты для информации в прогрессе загрузки
    $arProducts[] = $lastID;

    // проверка в торговых предложениях того же набора свойств и полей
    if($lastID > 0){
        # Фильтр по товару Достаём все торговые предложения товара
        $arFilter["IBLOCK_ID"] = $arParams["IBLOCK_CML2_LINK"];
        $arFilter["PROPERTY_CML2_LINK"] = $lastID;

        # Фильтр по свойствам
        $arProps = getShopProperties(["IBLOCK_ID" => $arFilter["IBLOCK_ID"]], $arProperties);

        $arProperties = fillShopProperties($arProps, $arProperties);

        if(!empty($arProperties)){
            foreach ($arProperties as $id=>$value){
                $arFilter["PROPERTY_" . $id] = $value;
            }
        }

        # Фильтр по цене
        /*
        if(!empty($arPrices)){
            foreach ($arPrices as $id=>$value){
                $arFilter["CATALOG_GROUP_" . $id] = $value;
            }
        }
        */

        // если используется поле группировки для торговых предложений - занимаем XML_ID
        if(!empty($arCatalog["TP_GROUP"])){
            //$arFields["XML_ID"] = $arCatalog["TP_GROUP"];
            unset($arFilter["NAME"]);
        }

        $products = CIBlockElement::GetList(
            [],
            $arFilter,
            false,
            false,
            [
                "ID",
                "NAME"
            ]
        );

        //////////////////////////
        // Update product tp if exist by data
        if($product = $products->fetch())
        {
            $arFields = [
                "IBLOCK_ID" => $arParams["IBLOCK_CML2_LINK"],
                "NAME" => $arFields["NAME"],
                "XML_ID" => $arFields["XML_ID"]
            ];
            $lastID = setShopFields($arParams, $arFields, $product["ID"]);

            $arFields = $arParams;
            $arFields["IBLOCK_ID"] = $arParams["IBLOCK_CML2_LINK"];
            $arProperties[$arParams["SKU"]["CML2_LINK"]] = $arFilter["PROPERTY_CML2_LINK"]; // CML2_LINK
            addShopProperties($arFields, $arProperties, $lastID);

            # Check position in catalog
            // https://dev.1c-bitrix.ru/api_help/catalog/classes/ccatalogproduct/add.php
            $existProduct = Product::getCacheItem($lastID,true);


            if(empty($existProduct)){
                Product::add([
                    "ID" => $lastID,
                    "QUANTITY" => $arCatalog["AVAILABLE"] == "Y" ? 100000 : 0,
                    "VAT_INCLUDED" => "Y",
                    "AVAILABLE" => $arCatalog["AVAILABLE"]
                    //"TYPE" => 4,
                    // "WEIGHT" => $element["DATA"]["W"],
                    //"VAT_INCLUDED" => "Y",
                    //"VAT_ID" => ($element["DATA"]["V"] == 10 ? 1 : 2)
                ]);
            }else{
                Product::update($product["ID"], [
                    "QUANTITY" => $arCatalog["AVAILABLE"] == "Y" ? 100000 : 0,
                    "VAT_INCLUDED" => "Y",
                    "AVAILABLE" => $arCatalog["AVAILABLE"]
                    //"VAT_ID" => ($element["DATA"]["V"] == 10 ? 1 : 2)
                    //"WEIGHT" => $element["DATA"]["W"],
                ]);
            }

            foreach ($arPrices as $id=>$value){
                setShopPrice($lastID, $id, $value);
            }

        }
        // Add new product tp by data
        else{

            $arFields = [
                "IBLOCK_ID" => $arParams["IBLOCK_CML2_LINK"],
                "NAME" => $arFields["NAME"],
                "XML_ID" => $arFields["XML_ID"]
            ];
            $lastID = setShopFields($arParams, $arFields);

            $arFields = $arParams;
            $arFields["IBLOCK_ID"] = $arParams["IBLOCK_CML2_LINK"];
            $arProperties[$arParams["SKU"]["CML2_LINK"]] = $arFilter["PROPERTY_CML2_LINK"]; // CML2_LINK
            addShopProperties($arFields, $arProperties, $lastID);

            # data for catalog module
            // Add to caatalog module
            Product::add(array(
                "ID" => $lastID,
                "QUANTITY" => $arCatalog["AVAILABLE"] == "Y" ? 100000 : 0,
                "AVAILABLE" => $arCatalog["AVAILABLE"],
                "VAT_INCLUDED" => "Y"
                // "WEIGHT" => $element["DATA"]["W"],
                //"VAT_INCLUDED" => "Y",
                //"VAT_ID" => ($element["DATA"]["V"] == 10 ? 1 : 2)
            ));

            foreach ($arPrices as $id=>$value){
                setShopPrice($lastID, $id, $value);
            }

        }

    }


    //////////////////////////

    // проверка в торговых предложениях
    // IBLOCK_CML2_LINK

    //AddMessage2Log($arFields, "arFields");
    //AddMessage2Log($arFields, "arProperties");


    // GOTO nexst step
    // Всё равно как использовалось до этого $lastID
    // в конце концов присваиваем ему значение из временной таблицы
    $lastID = $element["ID"];
}

$rsLeftBorder = ImportTable::getList(array(
    "order" => array("ID" => "ASC"),
    "filter" => array(
        "<=ID" => $lastID
    ),
    "select" => array(
        "ID"
    )
));
$leftBorderCnt = $rsLeftBorder->getSelectedRowsCount();

$rsAll = ImportTable::getList(array(
    "order" => array("ID" => "ASC"),
    "select" => array(
        "ID"
    )
));
$allCnt = $rsAll->getSelectedRowsCount();

$p = round(100*$leftBorderCnt/$allCnt, 2);

if(empty($arProducts)){
    echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("TRANSFER_CATALOG_ELEMENT_NON_UPDATE", ["ID" => $lastID]) . '");';
}else{
    echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("TRANSFER_CATALOG_ELEMENT_UPDATE", ["ID" => $lastID, "PRODUCTS" => implode(", ", $arProducts)]) . '");';
}