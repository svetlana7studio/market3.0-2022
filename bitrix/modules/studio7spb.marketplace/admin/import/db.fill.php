<?
use Bitrix\Main\Config\Option,
    Bitrix\Main\Loader,
    Studio7spb\Marketplace\ImportSettingsTable,
    Studio7spb\Marketplace\ImportTable;


$_SERVER["DOCUMENT_ROOT"] = $argv[1];
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
require $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// <editor-fold defaultstate="collapsed" desc=" # Clear b_studio7spb_import">
$DB->Query("DROP TABLE IF EXISTS b_studio7spb_import;");
$DB->Query("CREATE TABLE IF NOT EXISTS b_studio7spb_import
(
    ID INT NOT NULL AUTO_INCREMENT,
    STATUS CHAR(1),
    DATA TEXT,
    NOTE VARCHAR(255),
    PRIMARY KEY (ID)
);");
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # arParams">
$arParams = array(
    "MODULE" => "studio7spb.marketplace",
    "FILE" => "IMPORT_DATA_FILE",
    "CATALOG" => "",
    "IMPORT_DATA_SEPARATOR" => ""
);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Load orm vendor">
CModule::AddAutoloadClasses(
    $arParams["MODULE"],
    array(
        "\\Studio7spb\\Marketplace\\ImportTable" => "lib/import.php"
    )
);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Get import settings">
$arParams["FILE"] = Option::get($arParams["MODULE"], $arParams["FILE"]);

$importSettings = ImportSettingsTable::getList(array(
    "filter" => array("%CODE" => "IMPORT_")
));
while($importSetting = $importSettings->fetch()){
    //IMPORT_DATA_CATALOG
    switch ($importSetting["CODE"]){
        case "IMPORT_DATA_CATALOG":
            $arParams["CATALOG"] = $importSetting["VALUE"];
            break;
        case "IMPORT_DATA_CATALOG_START":
            $arParams["CATALOG_START"] = $importSetting["VALUE"];
            break;
        case "IMPORT_DATA_FILE":
            $arParams["FILE"] = $importSetting["VALUE"];
            break;
        case "IMPORT_DATA_SEPARATOR":
            $arParams["IMPORT_DATA_SEPARATOR"] = $importSetting["VALUE"];
            break;

    }
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Load">
if(empty($arParams["FILE"])){
    $arParams["FILE"] = null;
}else{
    $arParams["FILE"] = $_SERVER["DOCUMENT_ROOT"] . "/upload/excel/import.csv";
}

/////////////////
// Try to detect file encoding
/*
$encoding = mb_detect_encoding(file_get_contents($arParams["FILE"]),
    // example of a manual detection order
    'ASCII,UTF-8,ISO-8859-15');
*/

/////////////////

// Первая строка содержит имена полей


$objReader = PHPExcel_IOFactory::createReaderForFile($arParams["FILE"]);

/*
$encoding = mb_detect_encoding(file_get_contents($arParams["FILE"]),
    // example of a manual detection order
    'ASCII,UTF-8,ISO-8859-15');



$objReader->setInputEncoding($encoding);
*/

// $objReader->setInputEncoding('UTF-8');
//$objReader->setInputEncoding('CP1252');
//$objReader->setInputEncoding('ANSI');

//$objReader->setEnclosure('');
//$objReader->setLineEnding("\r\n");
//$objReader->setSheetIndex(0);
switch ($arParams["IMPORT_DATA_SEPARATOR"]){
    case "TZP":
        $objReader->setDelimiter(';');
        break;
    case "ZPT":
        $objReader->setDelimiter(',');
        break;
    case "TAB":
        $objReader->setDelimiter('  ');
        break;
    case "SPS":
        $objReader->setDelimiter(' ');
        break;
}

$objPHPExcel = $objReader->load($arParams["FILE"]);
// </editor-fold>

$key = 0;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

    $key++;

    $rows = array();

    switch ($worksheet->getTitle()) {

        case $arParams["CATALOG"]:
            // get rows limit for workshit
            $maxCell = $worksheet->getHighestRowAndColumn();
            $data = $worksheet->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row']);
            $data = array_map('array_filter', $data);
            $rowsLimit = array_filter($data);
            $rowsLimit = count($rowsLimit);

            $value = null;
            $i = 0;
            $j = 0;



            foreach ($worksheet->getRowIterator() as $row) {



                if($rowsLimit >= $row->getRowIndex()) {
                    if ($row->getRowIndex() >= $arParams["CATALOG_START"]) {
                        $i++;
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
                        $j = 0;
                        foreach ($cellIterator as $cell) {
                            $j++;
                            if (!is_null($cell) && $j < 34) {

                                $value = $cell->getValue();
                                $value = str_replace(["'", '"'], " ", $value);
                                //$value = iconv("ISO-8859-1", "UTF-8", $value);

                                // check prices is numeric
                                // HELP http://pushorigin.ru/php/phpexcel-snippets
                                //if ($cell->getColumn() == "H" || $cell->getColumn() == "I") {
                                //    if (!is_numeric($cell->getValue())) {
                                //        $value = $cell->getCalculatedValue();
                                //    }
                                //}

                                // add to array
                                $rows[$row->getRowIndex()][$cell->getColumn()] = $value;

                            }
                        }
                    }
                }
            }

            // add to db
            foreach ($rows as $row){
                ImportTable::add(array(
                    "STATUS" => "Y",
                    "DATA" => serialize($row),
                    "NOTE" => ""
                ));
            }
            break;
    }
}







$p = 5;
//$p = round(100*$leftBorderCnt/$allCnt, 2);
$lastID = 0;

echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","Данные получены.");';