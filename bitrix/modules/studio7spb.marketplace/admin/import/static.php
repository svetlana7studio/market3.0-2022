<?
/**
 * Import settings page
 * @var CMain $APPLICATION
 */
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application,
    Studio7spb\Marketplace\ImportSettingsTable;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadLanguageFile(__FILE__);

$request = Application::getInstance()->getContext()->getRequest();

// <editor-fold defaultstate="collapsed" desc="Set CONSTANTS">

$arParams = array(
    "FIELDS" => array(),
    "CONSTANTS" => array(),
    "SELECT" => array(
        "ID",
        "NAME",
        "IBLOCK_ID"
    ),
    "FILTER" => array(
        "IBLOCK_ID" => 2
    ),
    "PAGE" => "/bitrix/admin/studio7spb.marketplace_import_static.php"
);

if($request->isPost() && is_set($request->getPost("CONSTANTS"))){
    $CONSTANTS = $request->getPost("CONSTANTS");
}

$constants = ImportSettingsTable::getList(array("filter" => array("!%CODE" => "IMPORT_")));
while ($constant = $constants->fetch())
{
    if(is_set($CONSTANTS[$constant["CODE"]]))
    {
        $constant["VALUE"] = $CONSTANTS[$constant["CODE"]];
        ImportSettingsTable::update($constant["ID"], array("VALUE" => $constant["VALUE"]));
    }

    $arParams["CONSTANTS"][$constant["CODE"]] = $constant["VALUE"];
    $arParams["FIELDS"][$constant["CODE"]] = $constant;
}

// </editor-fold>



if(is_set($request->getPost("CONSTANTS"))){
    LocalRedirect($arParams["PAGE"]);
}

$APPLICATION->SetTitle( Loc::getMessage("TITLE") );
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<form method="POST" name="frm" id="frm">
<?
echo bitrix_sessid_post();
$aTabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => GetMessage("TITLE"),
        "ICON" => "iblock",
        "TITLE" => GetMessage("TITLE"),
    )
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();

$tabControl->BeginNextTab();
?>
    <?foreach ($arParams["CONSTANTS"] as $key=>$value):?>
        <tr>
            <td width="240"><?=$arParams["FIELDS"][$key]["NAME"]?></td>
            <td>
                <input type="text"
                       name="CONSTANTS[<?=$key?>]"
                       value="<?=$value?>">
            </td>
        </tr>
    <?endforeach;?>
<?
$tabControl->Buttons(array());
$tabControl->End();
?>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>