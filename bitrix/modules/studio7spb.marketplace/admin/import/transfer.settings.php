<?
/**
 * Есть основной профиль с данными IMPORT_FIELDS_LIST
 * Эти данные как раз и имеют наибольшую ценность, поступают от юзера, остальные - это всяческие галочки
 * Есть клоны этих данных IMPORT_FIELDS_LIST_1 IMPORT_FIELDS_LIST_2 ...
 * Эти клоны могут подменить основные данные
 * @var CMain $APPLICATION
 */

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application,
    Studio7spb\Marketplace\Import\ImportTable,
    Studio7spb\Marketplace\Import\ImportProfileListTable,
    Bitrix\Iblock\IblockTable,
    Bitrix\Iblock\TypeTable,
    Bitrix\Iblock\SectionTable,
    Bitrix\Iblock\PropertyTable;

define("NO_AGENT_STATISTIC", true);
define("NO_KEEP_STATISTIC", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadLanguageFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::getMessage("TRANSFER_ACCESS_DENIED"));

$request = Application::getInstance()->getContext()->getRequest();

$arParams = [
    "MODULE" => "studio7spb.marketplace",
    "MODULE_JS" => "studio7spb.marketplace",
    "CURRENT_PROFILE" => (int) $request->get("profile"),
    "FILE" => "IMPORT_DATA_FILE",
    "IBLOCKS" => [],
    "FIELDS_IDENT" => [
        "NAME" => Loc::getMessage("TRANS_IDENT_NAME"),
        "XML_ID" => Loc::getMessage("TRANS_IDENT_XML_ID"),
        "CODE" => Loc::getMessage("TRANS_IDENT_CODE"),
        "ACTIVE" => Loc::getMessage("TRANS_IDENT_ACTIVE")
    ],
    "PROPERTIES_IDENT" => [
        "PRODUCER",
        "H_COMPANY",
    ],
    "FIELD_GROUPS" => [
        "ELEMENT" => Loc::getMessage("TRANS_IB_ELEMENT"),
        "PRODUCT" => Loc::getMessage("TRANS_IB_CATALOG")
    ],
    "PREFIX" => [
        "ELEMENT" => "IE_",
        "PRODUCT" => "ICAT_",
        "TP" => "",
    ],
    "PROPERTIES_EX" => [
        //"PRODUCER",
        //"H_COMPANY",
        "vote_count",
        "vote_sum",
        "rating",
        "FORUM_TOPIC_ID",
        "FORUM_MESSAGE_CNT",
    ],
    "FIELDS" => [
        "IE_NAME" => Loc::getMessage("TRANS_IE_NAME"),
        "IE_XML_ID" => Loc::getMessage("TRANS_IE_XML_ID"),
        "IE_CODE" => Loc::getMessage("TRANS_IE_CODE"),
        "IE_PREVIEW_PICTURE" => Loc::getMessage("TRANS_IE_PREVIEW_PICTURE"),
        "IE_PREVIEW_TEXT" => Loc::getMessage("TRANS_IE_PREVIEW_TEXT"),
        "IE_DETAIL_PICTURE" => Loc::getMessage("TRANS_IE_DETAIL_PICTURE"),
        "IE_DETAIL_TEXT" => Loc::getMessage("TRANS_IE_DETAIL_TEXT"),
        "IE_ACTIVE" => Loc::getMessage("TRANS_IE_ACTIVE"),
        "IE_IBLOCK_SECTION_ID" => Loc::getMessage("TRANS_IE_IBLOCK_SECTION_ID"),
        "ICAT_IDENT" => Loc::getMessage("TRANS_ICAT_IDENT"),
        "ICAT_AVAILABLE" => Loc::getMessage("TRANS_ICAT_AVAILABLE"),
        "ICAT_GROUP" => Loc::getMessage("TRANS_ICAT_GROUP")
    ],
    "LIMIT" => 10,
    "START_IMPORT" => "work_start",
    "STOP_IMPORT" => "work_stop",
    "PROGRESS_IMPORT" => "work_progress",
    "START_ANALIZE" => "analize_start",
    "STOP_ANALIZE" => "analize_stop",
    "PROGRESS_ANALIZE" => "analize_progress"
];

$arResult = array(
    "PROFILE" => [],
    "SETTINGS" => [],
    "FILE" => [],
    "ERROR" => [],
    "LIMIT" => 0
);

$arParams["MODULE_JS"] = str_replace('.', '_', $arParams["MODULE"]);
CJSCore::Init([
    "jquery",
    $arParams["MODULE_JS"] . "_import_settings",
    $arParams["MODULE_JS"] . "_chosen"
]);

Loader::includeModule("iblock");
Loader::includeModule("sale");
Loader::registerAutoLoadClasses($arParams["MODULE"], [
    "\\Studio7spb\\Marketplace\\Import\\ImportProfileListTable" => "lib/import/importprofilelisttable.php",
    "\\Studio7spb\\Marketplace\\Import\\ImportTable" => "lib/import/import.php"
]);

// <editor-fold defaultstate="collapsed" desc=" # Settings process">

if($request->isPost()){

    # Save file to profile
    $arResult["FILE"] = $request->getFile($arParams["FILE"]);

    if(pathinfo($arResult["FILE"]["name"], PATHINFO_EXTENSION) === "csv")
    {

        if($arParams["CURRENT_PROFILE"] > 0){
            if(strlen($request->get("settings_profile_name")) <= 0){
                ImportProfileListTable::update($arParams["CURRENT_PROFILE"], [
                    "NAME" => htmlspecialcharsbx($arResult["FILE"]["name"])
                ]);
            }
        }
        else{
            $profile = ImportProfileListTable::add([
                "NAME" => htmlspecialcharsbx($arResult["FILE"]["name"])
            ]);
            if($profile->isSuccess())
                $arParams["CURRENT_PROFILE"] = $profile->getId();
            else
                $arResult["ERROR"] = $profile->getErrorMessages();
        }

        if($arParams["CURRENT_PROFILE"] > 0){

            $arResult["FILE"] = CFile::SaveFile($arResult["FILE"], "excel");
            if($arResult["FILE"] > 0){
                # delete old fille
                $profile = ImportProfileListTable::getList([
                    "filter" => [
                        "ID" => $arParams["CURRENT_PROFILE"]
                    ]
                ]);
                if($profile = $profile->fetch())
                    if($profile["FILE"] > 0)
                        CFile::Delete($profile["FILE"]);
                # set new file to profile
                ImportProfileListTable::update($arParams["CURRENT_PROFILE"], [
                    "FILE" => $arResult["FILE"]
                ]);
            }

        }

    }
    else{
        $arResult["ERROR"] = [Loc::getMessage("TRANS_ERROR_FILE_FORMAT")];
    }

    if($arParams["CURRENT_PROFILE"] > 0){
        # Save settings to profile

        // SETTINGS
        $arFields =  [
            "IBLOCK_TYPE" => htmlspecialcharsbx($request->get("iblock-type")),
            "IBLOCK_ID" => (int) $request->get("iblock-id"),
            "IBLOCK_SECTION_ID" => (int) $request->get("iblock-section"),
            "START" => (int) $request->get("worksheet_catalog_start"),
            "IDENT" => serialize($request->get("DETECT_FIELD")),
            "AVALIABLE_STRING" => htmlspecialcharsbx($request->get("worksheet_catalog_avaliable")),
            "SEPAR" => htmlspecialcharsbx($request->get("worksheet_catalog_separator")),
            "SORT" => htmlspecialcharsbx($request->get("settings_profile_sort")),
            "ACTIVE" => htmlspecialcharsbx($request->get("settings_profile_active")),
            "CUR" => $request->get("settings_partner_use")
        ];

        if(strlen(htmlspecialcharsbx($request->get("settings_profile_name"))) > 0)
            $arFields["NAME"] = htmlspecialcharsbx($request->get("settings_profile_name"));
        $arFields["FIELDS"] = $request->get("SETTINGS");
        $arFields["FIELDS"] = serialize($arFields["FIELDS"]["FIELDS_LIST"]);
        $arFields["SAMPLE"] = serialize($request->get("SIMPLE_ITEMS"));
        ImportProfileListTable::update($arParams["CURRENT_PROFILE"], $arFields);

    }

    # Redirect after all operations
    if(empty($request->get("save")))
        LocalRedirect("studio7spb.marketplace_transfer.settings.php?" . http_build_query([
                'lang' => LANGUAGE_ID,
                'profile' => $arParams["CURRENT_PROFILE"]
            ]));
    else
        LocalRedirect("studio7spb.marketplace_profile.list.php?" . http_build_query([
                'lang' => LANGUAGE_ID
            ]));
    die;
}else{

    if($arParams["CURRENT_PROFILE"] > 0){
        $arResult["PROFILE"] = [
            "limit" => 1,
            "filter" => ["ID" => $arParams["CURRENT_PROFILE"]]
        ];
        $arResult["PROFILE"] = ImportProfileListTable::getList($arResult["PROFILE"]);
        if($arResult["PROFILE"] = $arResult["PROFILE"]->fetch()){

            $arResult["LIMIT"] = $arResult["PROFILE"]["START"] + $arParams["LIMIT"];

            if($arResult["PROFILE"]["FILE"] > 0){
                $arResult["PROFILE"]["SAMPLE"] = Application::getDocumentRoot() . "/" . CFile::GetPath($arResult["PROFILE"]["FILE"]);

                require Application::getDocumentRoot() . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';
                $objReader = PHPExcel_IOFactory::createReaderForFile($arResult["PROFILE"]["SAMPLE"]);
                switch ($arResult["PROFILE"]["SEPAR"]){
                    case "TZP":
                        $objReader->setDelimiter(';');
                        break;
                    case "ZPT":
                        $objReader->setDelimiter(',');
                        break;
                    case "TAB":
                        $objReader->setDelimiter('  ');
                        break;
                    case "SPS":
                        $objReader->setDelimiter(' ');
                        break;
                }

                $objPHPExcel = $objReader->load($arResult["PROFILE"]["SAMPLE"]);

                foreach ($objPHPExcel->getActiveSheet()->getRowIterator() as $row) {
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(false);
                    foreach ($cellIterator as $cell)
                        $rows[$row->getRowIndex()][$cell->getColumn()] = $cell->getValue();
                }

                $arResult["PROFILE"]["SAMPLE"] = $rows;

                // FIELDS
                $arResult["PROFILE"]["SETTINGS_FIELDS_LIST"] = unserialize($arResult["PROFILE"]["FIELDS"]);
                if(!empty($rows)){

                    $arResult["PROFILE"]["FIELDS"] = current($rows);
                    foreach ($arResult["PROFILE"]["FIELDS"] as $letter=>$row)
                        if(empty($arResult["PROFILE"]["SETTINGS_FIELDS_LIST"][$letter]))
                            $arResult["PROFILE"]["FIELDS"][$letter] = [1 => []];
                        else
                            $arResult["PROFILE"]["FIELDS"][$letter] = $arResult["PROFILE"]["SETTINGS_FIELDS_LIST"][$letter];

                    // <editor-fold defaultstate="collapsed" desc=" # Clear b_studio7spb_import">
                    global $DB;
                    $DB->Query("DROP TABLE IF EXISTS b_studio7spb_import;");
                    $DB->Query("CREATE TABLE IF NOT EXISTS b_studio7spb_import(
                        ID INT NOT NULL AUTO_INCREMENT,
                        DATA TEXT,
                        PRIMARY KEY (ID)
                    );");
                    foreach ($rows as $i=>$row)
                        if($arResult["PROFILE"]["START"] <= $i)
                            ImportTable::add(["DATA" => serialize($row)]);
                    // </editor-fold>
                }

                // IDENT DETECT_FIELD
                $arResult["PROFILE"]["IDENT"] = unserialize($arResult["PROFILE"]["IDENT"]);

            }

        }
    }

    if($request->get($arParams["START_ANALIZE"]) !== "Y" && empty($request->get($arParams["START_IMPORT"]))){
        # Iblock Types
        $iblockTypes = TypeTable::getList();
        while ($iblockType = $iblockTypes->fetch())
            $arParams["IBLOCK_TYPES"][] = $iblockType;

        # Iblocks
        $iblocks = IblockTable::getList([
            "order" => ["IBLOCK_TYPE_ID" => "ASC"],
            "select" => [
                "ID",
                "NAME",
                "IBLOCK_TYPE_ID"
            ]
        ]);
        while ($iblock = $iblocks->fetch())
            $arParams["IBLOCKS"][] = $iblock;

        # Iblock sections
        $sections = SectionTable::getList([
            "order" => ['LEFT_MARGIN' => 'ASC'],
            "select" => [
                "ID",
                "NAME",
                "DEPTH_LEVEL",
                "IBLOCK_ID"
            ]
        ]);
        while ($section = $sections->fetch())
            $arParams["IBLOCK_SECTIONS"][] = $section;

        if($arResult["PROFILE"]["IBLOCK_ID"] > 0){
            # properties
            $arResult["PROPERTIES"] = [];
            $sections = PropertyTable::getList([
                "order" => ["SORT" => "ASC"],
                "filter" => [
                    "IBLOCK_ID" => $arResult["PROFILE"]["IBLOCK_ID"]
                ],
                "select" => [
                    "ID",
                    "NAME",
                    "CODE"
                ]
            ]);
            while($section = $sections->fetch()){
                $arResult["PROPERTIES"][] = $section;
                $arParams["FIELDS"]["IP_PROP" . $section["ID"]] = $section["NAME"];
            }
            unset($sections, $section);
        }

        # prices
        $importSettings = CCatalogGroup::GetList(["SORT" => "ASC"], []);
        while ($importSetting = $importSettings->Fetch())
        {
            $arParams["FIELDS"]["ICAT_PRICE" . $importSetting["ID"] . "_PRICE"] = Loc::getMessage("TRANS_PRICE") . ' "' . $importSetting["NAME_LANG"] .'"';
        }

        unset($code, $importSetting, $importSettings);
    }

}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # One-sep Analize process">

if($request->get($arParams["START_ANALIZE"]) == "Y" && check_bitrix_sessid())
{

    $elements = ImportTable::getList(array(
        "order" => array("ID" => "ASC"),
        "filter" => array(
            ">ID" => $request->get("lastid")
        ),
        "select" => array(
            "ID",
            "DATA"
        ),
        "limit" => 1 // $arParams["LIMIT"] // bed for analize errors
    ));

    $error = [];
    if($element = $elements->fetch())
    {
        $arParams["CURRENT_PROFILE"] = (int) $request->get("PID");
        if($arParams["CURRENT_PROFILE"] > 0){
            $arParams["CURRENT_PROFILE"] = ImportProfileListTable::getList([
                "limit" => 1,
                "filter" => [
                    "ID" => $arParams["CURRENT_PROFILE"]
                ]
            ]);
            if($arParams["CURRENT_PROFILE"] = $arParams["CURRENT_PROFILE"]->fetch()){
                # iblock props for required checking
                if($arParams["CURRENT_PROFILE"]["IBLOCK_ID"] > 0){
                    # properties
                    $arParams["REQUIRED"] = [];
                    $values = PropertyTable::getList([
                        "order" => ["SORT" => "ASC"],
                        "filter" => [
                            "IBLOCK_ID" => $arParams["CURRENT_PROFILE"]["IBLOCK_ID"],
                            "IS_REQUIRED" => "Y"
                        ],
                        "select" => [
                            "ID",
                            "NAME",
                            "CODE"
                        ]
                    ]);
                    while($value = $values->fetch())
                        $arParams["REQUIRED"]["IP_PROP" . $value["ID"]] = $value["NAME"] . " (" . $value["CODE"] . ")";
                }
                # data
                $element["DATA"] = unserialize($element["DATA"]);
                // $arParams["CURRENT_PROFILE"]["IDENT"]
                $arParams["CURRENT_PROFILE"]["FIELDS"] = unserialize($arParams["CURRENT_PROFILE"]["FIELDS"]);
                foreach ($element["DATA"] as $key=>$value){
                    foreach ($arParams["CURRENT_PROFILE"]["FIELDS"][$key] as $field){
                        switch ($field){
                            case "IE_NAME":
                                if(empty($value))
                                    $error[] = Loc::getMessage("TRANS_ANALIZE_ERROR_NAME");
                                break;
                            default:
                                if(!empty($arParams["REQUIRED"][$field]))
                                    if(empty($value))
                                        $error[] = Loc::getMessage("TRANS_ANALIZE_ERROR_REQUIRED");
                        }

                    }
                }
            }
        }

        $lastID = $element["ID"];
    }

    $rsLeftBorder = ImportTable::getList(array(
        "order" => array("ID" => "ASC"),
        "filter" => array(
            "<=ID" => $lastID
        ),
        "select" => array(
            "ID"
        )
    ));
    $leftBorderCnt = $rsLeftBorder->getSelectedRowsCount();

    $rsAll = ImportTable::getList(array(
        "order" => array("ID" => "ASC"),
        "select" => array(
            "ID"
        )
    ));
    $allCnt = $rsAll->getSelectedRowsCount();

    $p = round(100*$leftBorderCnt/$allCnt, 2);

    if(empty($error)){
        echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","Статус проверки. Последний проверенный товар №: '.$lastID.'. Ошибок не обнаружено.", "success");';
    }else{
        $error = implode(", <br />", $error);
        # echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","Товар №: '.$lastID.'. Ошибка: ' . $error . '", "error");';
        echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("TRANS_ANALIZE_ERROR", [
                "ID" => $lastID,
                "ERROR" => $error
            ]) . '", "error");';
    }

    die();
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # One-sep Import process with checking iblock by SKU">
if(!empty($request->get($arParams["START_IMPORT"])) && check_bitrix_sessid())
{
    // CURRENT_PROFILE
    $arParams["CURRENT_PROFILE"] = (int) $request->get("PID");
    if($arParams["CURRENT_PROFILE"] > 0) {
        $arParams["CURRENT_PROFILE"] = ImportProfileListTable::getList([
            "limit" => 1,
            "filter" => [
                "ID" => $arParams["CURRENT_PROFILE"]
            ]
        ]);
        if ($arParams["CURRENT_PROFILE"] = $arParams["CURRENT_PROFILE"]->fetch()) {
            # iblock props for required checking
            if($arParams["CURRENT_PROFILE"]["IBLOCK_ID"] > 0){
                $arResult["IBLOCK"] = CCatalogSKU::GetInfoByProductIBlock($arParams["IBLOCK_ID"]);
                if (is_array($arResult["IBLOCK"]))
                {
                    $arParams["IBLOCK_CML2_LINK"] = $arResult["IBLOCK"]['IBLOCK_ID'];
                    #require_once "transfer.import.tp.php";
                }
                else
                {
                    unset($arParams["IBLOCK_CML2_LINK"]);
                    require_once "transfer.import.php";
                }
            }
        }
    }
    die();
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Formating Analize script">
$arResult["CLEAN_ANALIZE_TABLE"] = '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="internal" id="' . $arParams["START_ANALIZE"] . '-info-table">'.
    '<tr class="heading">'.
    '<td>' . Loc::getMessage("TRANS_PROGRESS") . '</td>'.
    '</tr>'.
    '<tr>'.
    '<td id="' . $arParams["START_ANALIZE"] . '-info">&nbsp;</td>'.
    '</tr>'.
    '<tr class="heading">'.
    '<td>' . Loc::getMessage("TRANS_PROGRESS_ERRORS") . '</td>'.
    '</tr>'.
    '</table>';
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Admin and tabs controls">

$aTabs = [
    ["DIV" => $arParams["FILE"] . "_SETTINGS", "TAB" => Loc::getMessage("TRANS_TABS_SETTINGS"), "ICON"=>"main_user_edit", "TITLE" => Loc::getMessage("TRANS_TABS_SETTINGS")],
    ["DIV" => $arParams["FILE"] . "_ANALIZE", "TAB" => Loc::getMessage("TRANS_TABS_ANALIZE"), "ICON"=>"main_user_edit", "TITLE" => Loc::getMessage("TRANS_TABS_ANALIZE")],
    ["DIV" => $arParams["FILE"] . "_IMPORT", "TAB" => Loc::getMessage("TRANS_IMPORT_PRODUCTS"), "ICON"=>"main_user_edit", "TITLE" => Loc::getMessage("TRANS_IMPORT_PRODUCTS")]
];

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$APPLICATION->SetTitle("Импорт товаров");

// </editor-fold>

$context = new CAdminContextMenu([
    [
        "TEXT" => Loc::getMessage("TRANS_ACTION_LIST"),
        "TITLE"=> Loc::getMessage("TRANS_ACTION_LIST"),
        "LINK"=>"/bitrix/admin/studio7spb.marketplace_profile.list.php?lang=".LANG,
        "ICON"=>"btn_list"
    ],
    [
        "TEXT" => Loc::getMessage("TRANS_ADD_NEW"),
        "TITLE"=> "Add",
        "LINK"=>"/studio7spb.marketplace_transfer.settings.php?lang=".LANG,
        "ICON"=>"btn_new"
    ],
    [
        "TEXT"=>Loc::getMessage("TRANS_DELETE"),
        "TITLE"=> Loc::getMessage("TRANS_DELETE"),
        "LINK"=>"javascript:if(confirm('".Loc::getMessage("rubric_mnu_del_conf")."'))window.location='studio7spb.marketplace_profile.list.php?ID=".$arParams["CURRENT_PROFILE"]."&action=delete&lang=".LANG."&".bitrix_sessid_get()."';",
        "ICON"=>"btn_delete"
    ]
]);

// вывод административного меню


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
$context->Show();
if(!empty($arResult["ERROR"])){
    CAdminMessage::ShowMessage(implode("<br>", $arResult["ERROR"]));
}

// создание экземпляра класса административного меню

?>
<form method="post"
      action="<?=$APPLICATION->GetCurPage()?>"
      enctype="multipart/form-data"
      onsubmit="ShowWaitWindow()"
      name="post_form">
<?
$tabControl->Begin();

$tabControl->BeginNextTab();
// <editor-fold defaultstate="collapsed" desc=" # Tab: Import settings">
?>
    <tr>
        <td width="40%" class="adm-detail-content-cell-l">
            <b><?=Loc::getMessage("TRANS_FILE_TITLE")?></b>
            <br>
            <i><?=Loc::getMessage("TRANS_FILE_DESCRIPTION")?></i>
        </td>
        <td class="adm-detail-content-cell-r">
            <?
            echo \CFileInput::Show(
                $arParams["FILE"], // name
                $arResult["PROFILE"]["FILE"], // value
                array(
                    "IMAGE" => "N",
                    "PATH" => "Y",
                    "FILE_SIZE" => "Y",
                    "DIMENSIONS" => "N"
                ),
                array(
                    'upload' => true,
                    'medialib' => false,
                    'file_dialog' => true,
                    'cloud' => true,
                    'email' => true,
                    'linkauth' => true,
                    'del' => false,
                    'description' => false
                )
            );
            ?>
        </td>
    </tr>
    <tr>
        <td colspan="2">

            <table class="kda-ie-tbl">
                <tbody>
                <tr>
                    <td colspan="2">

                        <select id="IBLOCK_FIELDS_LIST" style="display:none;">
                            <option value=""><?=Loc::getMessage("TRANS_CHOOSE_FIELD")?></option>
                            <optgroup label="<?=$arParams["FIELD_GROUPS"]["ELEMENT"]?>">
                                <?foreach ($arParams["FIELDS"] as $key=>$value):?>
                                    <?if (strncmp($key, $arParams["PREFIX"]["ELEMENT"], strlen($arParams["PREFIX"]["ELEMENT"])) === 0):?>
                                        <option value="<?=$key?>"><?=$value?></option>
                                    <?endif;?>
                                <?endforeach;?>
                            </optgroup>
                            <optgroup label="<?=$arParams["FIELD_GROUPS"]["PRODUCT"]?>">
                                <?foreach ($arParams["FIELDS"] as $key=>$value):?>
                                    <?if (strncmp($key, $arParams["PREFIX"]["PRODUCT"], strlen($arParams["PREFIX"]["PRODUCT"])) === 0):?>
                                        <option value="<?=$key?>"><?=$value?></option>
                                    <?endif;?>
                                <?endforeach;?>
                            </optgroup>
                            <optgroup label="<?=Loc::getMessage("TRANS_PROPERTIES")?>">

                                <?foreach ($arResult["PROPERTIES"] as $property):?>
                                    <?if(!in_array($property["CODE"], $arParams["PROPERTIES_EX"])):?>
                                        <option value="IP_PROP<?=$property["ID"]?>"><?=$property["NAME"]?> [<?=$property["CODE"]?>]</option>
                                    <?endif;?>
                                <?endforeach;?>

                            </optgroup>
                        </select>

                        <div class="set_scroll">
                            <div></div>
                        </div>
                        <div id="worksheet-preview">
                            <div class="set">
                                <table class="list">
                                    <?
                                    // AddMessage2Log($arResult["SETTINGS_FIELDS_LIST"]);
                                    include "transfer.settings.table.php";
                                    ?>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody></table>

        </td>
    </tr>

    <tr>
        <td><?=Loc::getMessage("TRANS_CHOOSE_IBLOCK_TYPE")?></td>
        <td>
            <select name="iblock-type" id="iblock-type" onchange="marketplaceTransfer.selectIblock(this)">
                <option value="0"><?=Loc::getMessage("TRANS_CHOOSE")?></option>
                <?foreach ($arParams["IBLOCK_TYPES"] as $value):?>
                    <option value="<?=$value["ID"]?>" <?
                    if($arResult["PROFILE"]["IBLOCK_TYPE"] == $value["ID"])
                        echo "selected";
                    ?>><?=$value["ID"]?></option>
                <?endforeach;?>
            </select>
        </td>
    </tr>

    <tr>
        <td><?=Loc::getMessage("TRANS_CHOOSE_IBLOCK")?></td>
        <td>
            <select name="iblock-id" id="iblock-id" onchange="marketplaceTransfer.selectIblock(this)">
                <option value="0"><?=Loc::getMessage("TRANS_CHOOSE")?></option>
                <?foreach ($arParams["IBLOCKS"] as $value):?>
                    <option value="<?=$value["ID"]?>"
                            <?
                            if($arResult["PROFILE"]["IBLOCK_ID"] == $value["ID"])
                                echo "selected";
                            ?>
                            data-type="<?=$value["IBLOCK_TYPE_ID"]?>"><?=$value["NAME"]?> (<?=$value["IBLOCK_TYPE_ID"]?>)</option>
                <?endforeach;?>
            </select>
        </td>
    </tr>

    <tr>
        <td width="40%" class="adm-detail-content-cell-l">
            Выбрать раздел загрузки :
        </td>
        <td class="adm-detail-content-cell-r">
            <select name="iblock-section" id="iblock-section" onchange="marketplaceTransfer.selectIblock(this)">
                <?foreach ($arParams["IBLOCK_SECTIONS"] as $section):?>
                    <option <?
                    if($arResult["PROFILE"]["IBLOCK_SECTION_ID"] == $section["ID"])
                        echo "selected";
                    ?>
                            data-iblock="<?=$section["IBLOCK_ID"]?>"
                            value="<?=$section["ID"]?>">
                        <?
                        for($i=1; $i < $section["DEPTH_LEVEL"]; $i++)
                            echo ". ";
                        echo $section["NAME"];
                        echo " [";
                        echo $section["ID"];
                        echo "]";
                        ?>
                    </option>
                <?endforeach;?>
            </select>
        </td>
    </tr>

    <tr>
        <td class="adm-detail-content-cell-l">
            <b><?=Loc::getMessage("TRANS_START_ROW")?></b>
            <br />
            <i><?=Loc::getMessage("TRANS_START_ROW_DESC")?></i>
        </td>
        <td class="adm-detail-content-cell-r">
            <select name="worksheet_catalog_start">
                <option value="0"><?=Loc::getMessage("TRANS_START_ROW_NULL")?></option>
                <?for($i = 1; $i <= 100; $i++):?>
                    <option <?
                    if($arResult["PROFILE"]["START"] == $i){
                        echo "selected";
                    }
                    ?> value="<?=$i?>"><?=$i?></option>
                <?endfor;?>
            </select>
        </td>
    </tr>

    <tr>
        <td class="adm-detail-content-cell-l">
            <b><?=Loc::getMessage("TRANS_IDENT_FIELD")?></b>
            <br>
            <i><?=Loc::getMessage("TRANS_IDENT_FIELD_DESC")?></i>
        </td>
        <td class="adm-detail-content-cell-r">
            <ol>
                <?foreach ($arParams["FIELDS_IDENT"] as $code=>$property):?>
                    <li class="mb-5">
                        <lable>
                            <input type="checkbox"
                                   name="DETECT_FIELD[]"
                                <?
                                if(in_array($code, $arResult["PROFILE"]["IDENT"]))
                                    echo "checked";
                                ?>
                                   value="<?=$code?>"> <?=$property?>
                        </lable>
                    </li>
                <?endforeach;?>
            </ol>

        </td>
    </tr>

    <tr>
        <td class="adm-detail-content-cell-l">
            <b><?=Loc::getMessage("TRANS_AVALIABLE_STRING")?>:</b>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="text"
                   name="worksheet_catalog_avaliable"
                   placeholder="1"
                   value="<?=$arResult["PROFILE"]["AVALIABLE_STRING"]?>">
        </td>
    </tr>

    <tr>
        <td class="adm-detail-content-cell-l">
            <b><?=Loc::getMessage("TRANS_SEPAR")?>:</b>
        </td>
        <td class="adm-detail-content-cell-r">
            <?
            if(empty($arResult["PROFILE"]["SEPAR"])){
                $arResult["PROFILE"]["SEPAR"] = "TZP";
            }
            foreach ([
                         "TZP" => Loc::getMessage("TRANS_SEPAR_TZP"),
                         "ZPT" => Loc::getMessage("TRANS_SEPAR_ZPT"),
                         "TAB" => Loc::getMessage("TRANS_SEPAR_TAB"),
                         "SPS" => Loc::getMessage("TRANS_SEPAR_SPS")
                     ] as $value => $title):
                ?>

                <input type="radio"
                       name="worksheet_catalog_separator"
                       id="worksheet_catalog_separator_<?=$value?>"
                       value="<?=$value?>"
                    <?
                    if($value == $arResult["PROFILE"]["SEPAR"])
                        echo "checked=\"\"";
                    ?>>
                <label for="worksheet_catalog_separator_<?=$value?>"><?=$title?></label><br>

            <?endforeach;?>

        </td>
    </tr>

    <tr>
        <td class="adm-detail-content-cell-l">
            <b><?=Loc::getMessage("TRANS_PROFILE_NAME")?>:</b>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="text"
                   name="settings_profile_name"
                   placeholder="<?=Loc::getMessage("TRANS_PROFILE_PLACEHOLDER")?>"
                   value="<?=$arResult["PROFILE"]["NAME"]?>">
        </td>
    </tr>

    <tr>
        <td class="adm-detail-content-cell-l">
            <b><?=Loc::getMessage("TRANS_PROFILE_SORT")?>:</b>
        </td>
        <td class="adm-detail-content-cell-r">
            <input type="text"
                   name="settings_profile_sort"
                   placeholder="500"
                   value="<?=$arResult["PROFILE"]["SORT"]?>">
        </td>
    </tr>

    <tr>
        <td class="adm-detail-content-cell-l">
            <b><?=Loc::getMessage("TRANS_PROFILE_ACTIVE")?>:</b>
        </td>
        <td class="adm-detail-content-cell-r">
            <?
            echo '<input type="checkbox" ';
            echo 'name="settings_profile_active" ';
            if($arResult["PROFILE"]["ACTIVE"] == "Y")
                echo 'checked ';
            echo 'value="Y">';
            ?>
        </td>
    </tr>

    <tr>
        <td class="adm-detail-content-cell-l">
            <b><?=Loc::getMessage("TRANS_VENDOR_USE")?>:</b>
        </td>
        <td class="adm-detail-content-cell-r">
            <?
            echo '<input type="checkbox" ';
            echo 'name="settings_partner_use" ';
            if($arResult["PROFILE"]["CUR"] == $arParams["CURRENT_PROFILE"])
                echo 'checked ';
            echo 'value="' . $arParams["CURRENT_PROFILE"] .'">';
            ?>
        </td>
    </tr>
<?
// </editor-fold>

$tabControl->BeginNextTab();
// <editor-fold defaultstate="collapsed" desc=" # Tab: Import settings">
?>
    <tr>
        <td colspan="2">
            <input type=button value="<?=Loc::getMessage("TRANS_PLAY")?>" id="<?=$arParams["START_ANALIZE"]?>" onclick="analizeStart(1)" />
            <input type=button value="<?=Loc::getMessage("TRANS_STOP")?>" disabled id="<?=$arParams["STOP_ANALIZE"]?>" onclick="bSubmit=false;analizeStart(0)" />
            <div id="<?=$arParams["PROGRESS_ANALIZE"]?>" style="display:none;" width="100%">
                <br />
                <div id="<?=$arParams["START_ANALIZE"]?>-status"></div>
                <table border="0" cellspacing="0" cellpadding="2" width="100%">
                    <tr>
                        <td height="10">
                            <div style="border:1px solid #B9CBDF">
                                <div id="<?=$arParams["START_ANALIZE"]?>-indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
                            </div>
                        </td>
                        <td width=30>&nbsp;<span id="<?=$arParams["START_ANALIZE"]?>-percent">0%</span></td>
                    </tr>
                </table>
            </div>
            <div id="<?=$arParams["START_ANALIZE"]?>-result" style="padding-top:10px"></div>
        </td>
    </tr>
<?
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Tab: Import-action">
$tabControl->BeginNextTab();
?>

    <tr>
        <td colspan="2">

            <input type=button value="<?=Loc::getMessage("TRANS_PLAY")?>" id="<?=$arParams["START_IMPORT"]?>" onclick="import_start(1)" />
            <input type=button value="<?=Loc::getMessage("TRANS_STOP")?>" disabled id="<?=$arParams["STOP_IMPORT"]?>" onclick="bSubmit=false;import_start(0)" />
            <div id="<?=$arParams["PROGRESS_IMPORT"]?>" style="display:none;" width="100%">
                <br />
                <div id="<?=$arParams["START_IMPORT"]?>-status"></div>
                <table border="0" cellspacing="0" cellpadding="2" width="100%">
                    <tr>
                        <td height="10">
                            <div style="border:1px solid #B9CBDF">
                                <div id="<?=$arParams["START_IMPORT"]?>-indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
                            </div>
                        </td>
                        <td width=30>&nbsp;<span id="<?=$arParams["START_IMPORT"]?>-percent">0%</span></td>
                    </tr>
                </table>
            </div>
            <div id="<?=$arParams["START_IMPORT"]?>-result"></div>

        </td>
    </tr>

<?
// </editor-fold>

$tabControl->Buttons(
    array(
        "disabled"=>($POST_RIGHT == "D"),
        "back_url"=>"/bitrix/admin/studio7spb.marketplace_profile.list.php?lang=".LANG,

    )
);

echo bitrix_sessid_post();
echo '<input type="hidden" name="lang" value="' . LANG . '">';
if($arParams["CURRENT_PROFILE"] > 0)
    echo '<input type="hidden" name="profile" value="' . $arParams["CURRENT_PROFILE"] . '">';

$tabControl->End();

?>
    <script type="text/javascript">

        var bWorkFinished = false;
        var bSubmit;

        /**
         * import start
         * @param val
         */
        function import_start(val)
        {
            document.getElementById('<?=$arParams["START_IMPORT"]?>').disabled = val ? 'disabled' : '';
            document.getElementById('<?=$arParams["STOP_IMPORT"]?>').disabled = val ? '' : 'disabled';
            document.getElementById('<?=$arParams["PROGRESS_IMPORT"]?>').style.display = val ? 'block' : 'none';

            if (val)
            {
                ShowWaitWindow();
                document.getElementById('<?=$arParams["START_IMPORT"]?>-result').innerHTML = '<?=$arResult["CLEAN_IMPORT_TABLE"]?>';
                document.getElementById('<?=$arParams["START_IMPORT"]?>-status').innerHTML = 'Идёт прогресс импорта...';

                document.getElementById('<?=$arParams["START_IMPORT"]?>-percent').innerHTML = '0%';
                document.getElementById('<?=$arParams["START_IMPORT"]?>-indicator').style.width = '0%';

                CHttpRequest.Action = import_onload;
                CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?<?=$arParams["START_IMPORT"]?>=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>&PID=<?=$arParams["CURRENT_PROFILE"]?>');
            }
            else
                CloseWaitWindow();
        }

        function import_onload(result)
        {
            try
            {
                eval(result);

                iPercent = CurrentStatus[0];
                strNextRequest = CurrentStatus[1];
                strCurrentAction = CurrentStatus[2];

                document.getElementById('<?=$arParams["START_IMPORT"]?>-percent').innerHTML = iPercent + '%';
                document.getElementById('<?=$arParams["START_IMPORT"]?>-indicator').style.width = iPercent + '%';

                document.getElementById('<?=$arParams["START_IMPORT"]?>-status').innerHTML = 'Идёт прогресс импорта...';

                if (strCurrentAction != 'null')
                {

                    var objInfo = BX('<?=$arParams["START_IMPORT"]?>-info'),
                        limit = 10,
                        count = BX.findChildren(BX(objInfo), {'className':'m10'}, true),
                        node = BX.create("div", {
                            props: {
                                className: "m10"
                            },
                            text: strCurrentAction
                        });

                    if (!!count) {
                        count = count.length;
                        if(count > limit){
                            objInfo.innerHTML = "";
                        }
                    }

                    BX.append(node, objInfo);

                }


                if (strNextRequest && document.getElementById('<?=$arParams["START_IMPORT"]?>').disabled)
                    CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?<?=$arParams["START_IMPORT"]?>=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>&PID=<?=$arParams["CURRENT_PROFILE"]?>' + strNextRequest);
                else
                {
                    import_start(0);
                    bWorkFinished = true;
                }

            }
            catch(e)
            {
                CloseWaitWindow();
                document.getElementById('<?=$arParams["START_IMPORT"]?>').disabled = '';
                alert('Сбой в получении данных');
            }
        }

        /**
         * import start
         * @param val
         */
        function analizeStart(val)
        {
            document.getElementById('<?=$arParams["START_ANALIZE"]?>').disabled = val ? 'disabled' : '';
            document.getElementById('<?=$arParams["STOP_ANALIZE"]?>').disabled = val ? '' : 'disabled';
            document.getElementById('<?=$arParams["PROGRESS_ANALIZE"]?>').style.display = val ? 'block' : 'none';

            if (val)
            {
                ShowWaitWindow();
                document.getElementById('<?=$arParams["START_ANALIZE"]?>-result').innerHTML = '<?=$arResult["CLEAN_ANALIZE_TABLE"]?>';
                document.getElementById('<?=$arParams["START_ANALIZE"]?>-status').innerHTML = 'Идёт процесс проверки...';

                document.getElementById('<?=$arParams["START_ANALIZE"]?>-percent').innerHTML = '0%';
                document.getElementById('<?=$arParams["START_ANALIZE"]?>-indicator').style.width = '0%';

                CHttpRequest.Action = analize_onload;
                CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?<?=$arParams["START_ANALIZE"]?>=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>&PID=<?=$arParams["CURRENT_PROFILE"]?>');
            }
            else
                CloseWaitWindow();
        }

        function analize_onload(result)
        {
            try
            {
                eval(result);

                iPercent = CurrentStatus[0];
                strNextRequest = CurrentStatus[1];
                strCurrentAction = CurrentStatus[2];

                document.getElementById('<?=$arParams["START_ANALIZE"]?>-percent').innerHTML = iPercent + '%';
                document.getElementById('<?=$arParams["START_ANALIZE"]?>-indicator').style.width = iPercent + '%';
                document.getElementById('<?=$arParams["START_ANALIZE"]?>-status').innerHTML = 'дождитесь окончания прогресса...';

                if (strCurrentAction != 'null')
                {
                    resultTableInfo = document.getElementById('<?=$arParams["START_ANALIZE"]?>-info');
                    resultTableInfo.innerHTML = strCurrentAction;

                    if(CurrentStatus[3] == "error"){
                        resultTableInfo = document.getElementById('<?=$arParams["START_ANALIZE"]?>-info-table');

                        // it to mach errors
                        if($(resultTableInfo).find("tr").length > 11){
                            analizeStart(0);
                            bWorkFinished = true;
                        }

                        oRow = resultTableInfo.insertRow(-1);
                        oCell = oRow.insertCell(-1);
                        oCell.innerHTML = strCurrentAction;
                    }

                }

                if (strNextRequest && document.getElementById('<?=$arParams["START_ANALIZE"]?>').disabled)
                    CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?<?=$arParams["START_ANALIZE"]?>=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>&PID=<?=$arParams["CURRENT_PROFILE"]?>' + strNextRequest);
                else
                {
                    analizeStart(0);
                    bWorkFinished = true;

                    if($("#<?=$arParams["START_ANALIZE"]?>-info-table").find("tr").length <= 6){
                        tabControl.EnableTab('<?=$arParams["FILE"] . "_IMPORT"?>');
                    }

                }

            }
            catch(e)
            {
                CloseWaitWindow();
                document.getElementById('<?=$arParams["START_ANALIZE"]?>').disabled = '';
                alert('Сбой в получении данных');
            }
        }

        tabControl.DisableTab('<?=$arParams["FILE"] . "_IMPORT"?>');

        $('.kda-ie-tbl').attr('data-init', 1);
        EList.Init();
        $(".fieldval").each(function () {
            EList.OnFieldFocus($(this));
        });
    </script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>