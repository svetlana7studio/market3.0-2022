<?
/**
 * @var array $arResult
 * @var array $arParams
 */
use Bitrix\Main\Localization\Loc;
#####################################################
$arResult["SETTINGS_FIELDS_LIST"] = array (
    'A' =>
        array (
            0 => '',
        ),
    'B' =>
        array (
            0 => 'IE_NAME',
        ),
    'C' =>
        array (
            0 => '',
        ),
    'D' =>
        array (
            0 => 'ICAT_AVAILABLE',
        ),
    'E' =>
        array (
            0 => '',
        ),
    'F' =>
        array (
            0 => 'IP_PROP15',
        ),
    'G' =>
        array (
            0 => '',
        ),
    'H' =>
        array (
            0 => 'IP_PROP17',
        ),
    'I' =>
        array (
            0 => 'ICAT_PRICE1_PRICE',
        ),
    'J' =>
        array (
            0 => 'IP_PROP18',
        ),
    'K' =>
        array (
            0 => 'IE_PREVIEW_TEXT',
        ),
    'L' =>
        array (
            0 => '',
        ),
    'M' =>
        array (
            0 => '',
        ),
    'N' =>
        array (
            0 => 'IE_PREVIEW_TEXT',
        ),
    'O' =>
        array (
            0 => 'IE_DETAIL_TEXT',
        ),
    'P' =>
        array (
            0 => '',
        ),
    'Q' =>
        array (
            0 => '',
        ),
    'R' =>
        array (
            0 => '',
        ),
    'S' =>
        array (
            0 => '',
        ),
    'T' =>
        array (
            0 => '',
        ),
    'U' =>
        array (
            0 => '',
        ),
    'V' =>
        array (
            0 => '',
        ),
    'W' =>
        array (
            0 => '',
        ),
    'X' =>
        array (
            0 => '',
        ),
    'Y' =>
        array (
            0 => '',
        ),
    'Z' =>
        array (
            0 => '',
        ),
    'AA' =>
        array (
            0 => '',
        ),
    'AB' =>
        array (
            0 => '',
        ),
    'AC' =>
        array (
            0 => '',
        ),
    'AD' =>
        array (
            0 => '',
        ),
    'AE' =>
        array (
            0 => '',
        ),
    'AF' =>
        array (
            0 => '',
        ),
    'AG' =>
        array (
            0 => '',
        ),
);

#####################################################

if(!empty($arResult["PROFILE"]["SAMPLE"])):
?>

<tr class='preview-header'>
    <td><?=Loc::getMessage("TRANS_NUMBER")?></td>
    <?
    $CELL = 0;
    foreach ($arResult["PROFILE"]["FIELDS"] as $letter=>$field):
        $CELL++;
    ?>
    <td class="kda-ie-field-select" title="#CELL<?=$CELL?>#">
        <b><?=$letter?></b>
        <?foreach ($field as $key=>$value):?>
            <div>
                <input type="hidden" name="SETTINGS[FIELDS_LIST][<?=$letter?>][<?=$key?>]" value="<?=$value?>">
                <span class="fieldval_wrap">
                        <?if(empty($value)):?>
                            <span class="fieldval fieldval_empty" id="SETTINGS[FIELDS_LIST][<?=$letter?>][<?=$key?>]" placeholder="<?=Loc::getMessage("TRANS_CHOOSE_FIELD")?>" title="">
                            <?=Loc::getMessage("TRANS_CHOOSE_FIELD")?>
                        </span>
                        <?else:?>
                            <span class="fieldval" id="SETTINGS[FIELDS_LIST][<?=$letter?>][<?=$key?>]" placeholder="<?=Loc::getMessage("TRANS_CHOOSE_FIELD")?>" title="">
                                <?=$arParams["FIELDS"][$value]?>
                            </span>
                        <?endif;?>
                    </span>
                <a href="javascript:void(0)" class="field_settings inactive" id="SETTINGS[FIELDS_LIST][<?=$letter?>][<?=$key?>]" title="<?=Loc::getMessage("TRANS_SETTINGS_FIELD")?>" onclick="EList.ShowFieldSettings(this);">
                    <input type="hidden" name="EXTRASETTINGS[<?=$letter?>][<?=$key?>]" value=""></a>
                <a href="javascript:void(0)" class="field_delete" title="<?=Loc::getMessage("TRANS_DELETE_FIELD")?>" onclick="EList.DeleteUploadField(this);"></a>
            </div>
        <?endforeach;?>

        <div class="kda-ie-field-select-btns">
            <div class="kda-ie-field-select-btns-inner">
                <a href="javascript:void(0)" class="kda-ie-move-fields">
                    <span title="<?=Loc::getMessage("TRANS_LEFT_FIELD")?>" onclick="return EList.ColumnsMoveLeft(this);"></span>
                    <span title="<?=Loc::getMessage("TRANS_RIGHT_FIELD")?>" onclick="return EList.ColumnsMoveRight(this);"></span>
                </a>
                <a href="javascript:void(0)" class="kda-ie-add-load-field" title="<?=Loc::getMessage("TRANS_ADD_FIELD")?>" onclick="EList.AddUploadField(this);"></a>
            </div>
        </div>
    </td>
    <?endforeach;?>

</tr>

<?
$CELL = 0;
foreach ($arResult["PROFILE"]["SAMPLE"] as $i=>$row):
    if($i > $arResult["LIMIT"])
        break;
    $CELL++;
?>
    <tr>
        <td><?=$CELL?></td>
        <?foreach ($row as $letter=>$value):?>
            <td>
                <div class="value"><?=$value?></div>
                <input type="hidden" name="SIMPLE_ITEMS[<?=$letter?>][<?=$i?>]" value="<?=$value?>" />
            </td>
        <?endforeach;?>
    </tr>
<?endforeach;?>

<?endif;?>