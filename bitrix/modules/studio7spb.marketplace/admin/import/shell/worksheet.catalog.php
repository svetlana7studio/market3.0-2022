<?
use Bitrix\Main\Config\Option,
    Studio7spb\Marketplace\ImportSettingsTable,
    Bitrix\Main\Web\Json;

$_SERVER["DOCUMENT_ROOT"] = $argv[1];
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
require $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

AddMessage2Log("worksheet.catalog");

die;

// <editor-fold defaultstate="collapsed" desc=" # arParams">
$arParams = array(
    "MODULE" => "studio7spb.marketplace",
    "FILE" => "IMPORT_DATA_FILE",
    "CATALOG" => ""
);

$importSettings = ImportSettingsTable::getList(array(
    "filter" => array("%CODE" => "IMPORT_")
));
while($importSetting = $importSettings->fetch()){
    //IMPORT_DATA_CATALOG
    switch ($importSetting["CODE"]){
        case "IMPORT_DATA_CATALOG":
            $arParams["CATALOG"] = $importSetting["VALUE"];
            break;
        case "IMPORT_DATA_FILE":
            $arParams["FILE"] = $importSetting["VALUE"];
            break;
        case "IMPORT_DATA_SEPARATOR":
            $arParams["IMPORT_DATA_SEPARATOR"] = $importSetting["VALUE"];
            break;
    }
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Load">
$arResult = array(
    "FILE" => null,
    "WORKSHEET" => array(),
    "CATALOG" => array(),
    "ERRORS" => array()
);

if(empty($arParams["FILE"])){
    $arParams["FILE"] = null;
}else{
    $arParams["FILE"] = $_SERVER["DOCUMENT_ROOT"] . "/upload/excel/import.csv";
}

$objReader = PHPExcel_IOFactory::createReaderForFile($arParams["FILE"]);
switch ($arParams["IMPORT_DATA_SEPARATOR"]){
    case "TZP":
        $objReader->setDelimiter(';');
        break;
    case "ZPT":
        $objReader->setDelimiter(',');
        break;
    case "TAB":
        $objReader->setDelimiter('  ');
        break;
    case "SPS":
        $objReader->setDelimiter(' ');
        break;
}

$objPHPExcel = $objReader->load($arParams["FILE"]);
// </editor-fold>

foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

    if($arParams["CATALOG"] === $worksheet->getTitle()){
        $arResult["WORKSHEET"][] = array(
            "TITLE" => $worksheet->getTitle(),
            "CURRENT" => 1
        );
    }else{
        $arResult["WORKSHEET"][] = array(
            "TITLE" => $worksheet->getTitle(),
            "CURRENT" => 0
        );
    }

    $rows = array();

    if($worksheet->getTitle() === $arParams["CATALOG"]){
        // get rows limit for workshit
        $maxCell = $worksheet->getHighestRowAndColumn();
        $data = $worksheet->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row']);
        $data = array_map('array_filter', $data);
        $rowsLimit = array_filter($data);
        $rowsLimit = count($rowsLimit);

        // read worksheet
        $counter = 0;
        foreach ($worksheet->getRowIterator() as $row) {

            if($rowsLimit >= $row->getRowIndex()){
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
                $i = 0;
                foreach ($cellIterator as $cell) {
                    $i++;
                    if (!is_null($cell) && $i < 34) {
                        //$rows[$row->getRowIndex()][$cell->getCoordinate()] = $cell->getValue();

                        $value = $cell->getValue();
                        if(is_numeric($value)){
                            $value = round($value, 2);
                        }

                        $rows[$row->getRowIndex()][$cell->getColumn()] = $value;
                    }
                }

                $counter++;
                if($counter > 15){
                    break;
                }
            }

        }

        // fill catalog

        $arResult["ERRORS"]["CATALOG"] = "count: " . count($rows);


        if(empty($rows)){
            $arResult["ERRORS"]["CATALOG"] = "В этом листе каталог не обнаружен";
        }else{
            foreach ($rows as $row) {
                $arResult["CATALOG"][] = $row;
            }
        }
    }

}

echo Json::encode($arResult);
