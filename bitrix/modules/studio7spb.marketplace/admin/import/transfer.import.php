<?
/**
 * @var array $arParams
 * @var array $arResult
 */
use Bitrix\Main\Localization\Loc,
    Studio7spb\Marketplace\Import\ImportTable,
    Studio7spb\Marketplace\ImportSettingsTable,
    Bitrix\Highloadblock,
    Bitrix\Iblock\PropertyTable,
    Bitrix\Catalog\Model\Product;

// <editor-fold defaultstate="collapsed" desc=" # Prepare">

/**
 * @param $PRODUCT_ID
 * @param $PRICE_TYPE_ID
 * @param $PRICE
 * @param string $CURRENCY
 */
function setShopPrice($PRODUCT_ID, $PRICE_TYPE_ID, $PRICE, $CURRENCY="RUB"){
    // умножить цену на курс
    $currency = ImportSettingsTable::getList([
        "filter" => ["CODE" => "CURRENCY_USD_RUB"]
    ]);
    if($currency = $currency->fetch()) {
        $currency["VALUE"] = intval($currency["VALUE"]);
        if($currency["VALUE"] > 0){
            $PRICE = $PRICE * $currency["VALUE"];
        }
    }

    $arFields = [
        "PRODUCT_ID" => $PRODUCT_ID,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
        "PRICE" => $PRICE,
        "CURRENCY" => $CURRENCY
    ];

    $res = CPrice::GetList(
        [],
        [
            "PRODUCT_ID" => $PRODUCT_ID,
            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
        ]
    );

    if ($arr = $res->Fetch())
    {
        CPrice::Update($arr["ID"], $arFields);
    }
    else
    {
        CPrice::Add($arFields);
    }

}

/**
 * @param $arParams
 * @param $arProperties
 * @return array
 * @throws \Bitrix\Main\ArgumentException
 * @throws \Bitrix\Main\ObjectPropertyException
 * @throws \Bitrix\Main\SystemException
 */
function getShopProperties($arParams, $arProperties){
    $arPropertiIDs = [];
    $arProps = [];
    foreach ($arProperties as $id=>$arProperty){
        $arPropertiIDs[] = $id;
    }
    if(!empty($arPropertiIDs)){
        $db = PropertyTable::getList([
            "filter" => [
                "IBLOCK_ID" => $arParams["CURRENT_PROFILE"]["IBLOCK_ID"],
                "ID" => $arPropertiIDs
            ]
        ]);
        while ($arProperty = $db->fetch()){
            $arProps[$arProperty["ID"]] = $arProperty;
        }
    }

    return $arProps;
}

/**
 * @param array $arProps
 * @param array $arProperties
 * @return array
 */
function fillShopProperties($arProps, $arProperties){
    $arResult = [];

    if(!empty($arProps)){
        foreach ($arProps as $arProp){

            switch ($arProp["PROPERTY_TYPE"]){
                case "S": // Строка
                case "N": // Число

                    if($arProp["MULTIPLE"] == "Y"){
                        $arResult[$arProp["ID"]][] = $arProperties[$arProp["ID"]];
                    }else{
                        $arResult[$arProp["ID"]] = $arProperties[$arProp["ID"]];
                    }

                    break;
                case 'F': // Файл

                    $files = $arProperties[$arProp["ID"]];
                    $files = explode(",", $files);

                    if(empty($files)){

                    }else{
                        $arProperties[$arProp["ID"]] = [];
                        foreach ($files as $value){
                            if(!empty($value)){
                                $value = trim($value);
                                $arResult[$arProp["ID"]][] = CFile::MakeFileArray($value);
                            }

                        }
                    }

                    break;
                case 'L': // Список

                    // Использую все свойства список
                    // В перспективе учитывать раздел свойства
                    // https://dev.1c-bitrix.ru/api_help/iblock/classes/ciblockpropertyenum/getlist.php


                    $sizes = CIBlockPropertyEnum::GetList([],[
                        "IBLOCK_ID" => $arProp["IBLOCK_ID"],
                        "CODE" => $arProp["CODE"],
                        "VALUE" => $arProperties[$arProp["ID"]],
                    ]);
                    while ($size = $sizes->Fetch()){

                        if($arProp["MULTIPLE"] == "Y"){

                            if(ToUpper($arProperties[$arProp["ID"]]) == ToUpper($size["VALUE"])){
                                $arResult[$size["PROPERTY_ID"]][] = $size["ID"];
                            }else{
                                $arResult[$size["PROPERTY_ID"]][] = 1; // Если цвет не задан то он будет попадать в плашку с прочерком
                            }


                        }else{

                            if(ToUpper($arProperties[$arProp["ID"]]) == ToUpper($size["VALUE"])){
                                $arResult[$size["PROPERTY_ID"]] = $size["ID"];
                            }else{
                                $arResult[$size["PROPERTY_ID"]] = 1; // Если цвет не задан то он будет попадать в плашку с прочерком
                            }


                        }



                    }

                    break;
                case 'E': // Привязка к элементу инфоблока
                    // Просто добавить значение
                    if($arProp['USER_TYPE'] == 'SKU'){
                        $arResult[$arProp["ID"]] = $arProperties[$arProp["ID"]];
                    }
                    // проверить по значению и добавить или удалить
                    else{
                        # Set trademark
                        $arProperties["TRADE_MARK"] = [
                            "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"],
                            "XML_ID" => $arProperties[$arProp["ID"]],
                        ];
                        $arProperties["TRADE_MARK"] = CIBlockElement::GetList(
                            [],
                            $arProperties["TRADE_MARK"],
                            false,
                            false,
                            [
                                "ID"
                            ]
                        );
                        if($arProperties["TRADE_MARK"] = $arProperties["TRADE_MARK"]->Fetch())
                        {
                            $arResult[$arProp["ID"]] = $arProperties["TRADE_MARK"]["ID"];
                        }
                        else{
                            $neo = new CIBlockElement();
                            $arResult[$arProp["ID"]] = $neo->Add([
                                "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"],
                                "XML_ID" => $arProperties[$arProp["ID"]],
                                "NAME" => $arProperties[$arProp["ID"]]
                            ]);

                        }
                    }

            }
        }
    }

    return $arResult;
}

/**
 * @param $arParams
 * @param $arProperties
 * @param $productID
 */
function addShopProperties($arParams, $arProperties, $productID)
{
    CIBlockElement::SetPropertyValuesEx($productID, $arParams["IBLOCK_ID"], $arProperties);
}

/**
 * Набираем тут arProperties и обновдяем его элементу
 * @param $files
 * @param $arParams
 * @param $element
 * @param $product
 */
function setShopProperties($arParams, $arProperties, $productID){
    $arProps = getShopProperties($arParams, $arProperties);
    $arProperties = fillShopProperties($arProps, $arProperties);
    addShopProperties($arParams, $arProperties, $productID);
}

/**
 * @param array $arParams
 * @param array $arFields
 * @param int $productID
 * @return int
 */
function setShopFields($arParams, $arFields, $productID=0){

    $neo = new CIBlockElement();
    if($arFields["ACTIVE"] == $arParams["CURRENT_PROFILE"]["AVALIABLE_STRING"])
        $arFields["ACTIVE"] = "Y";
    else
        $arFields["ACTIVE"] = "N";

    $arFields["DETAIL_TEXT_TYPE"] = "html";

    if($productID > 0){
        $lastID = $productID;
        $neo->Update($productID, $arFields);
    }else{
        $lastID = $neo->Add($arFields);
    }

    return $lastID;
}
// </editor-fold>

$importSettings = [];

// IDENT
// FIELDS
$importSettings["IMPORT_FIELDS_LIST"] = unserialize($arParams["CURRENT_PROFILE"]["FIELDS"]);
$importSettings["IMPORT_DETECT_FIELD"] = unserialize($arParams["CURRENT_PROFILE"]["IDENT"]);
// parent section
$parentSection = $arParams["CURRENT_PROFILE"]["IBLOCK_SECTION_ID"];

// import from tmp
$elements = ImportTable::getList(array(
    "order" => array("ID" => "ASC"),
    "filter" => array(
        ">ID" => $_REQUEST["lastid"]
    ),
    "limit" => $arParams["LIMIT"]
));

$arProducts = [];
while ($element = $elements->fetch())
{

    // get tmp data
    $element["DATA"] = unserialize($element["DATA"]);
    //check iblock element add or update

    $arFilter = [
        "IBLOCK_ID" => $arParams["CURRENT_PROFILE"]["IBLOCK_ID"],
    ];

    $arFields = [];
    $arProperties = [];
    $arPrices = [];
    $arCatalog = [];

    // не задана идентификация
    if(empty($importSettings["IMPORT_DETECT_FIELD"])){
        //$arFilter["XML_ID"] = "never sache this product";
    }else{
        foreach ($importSettings["IMPORT_DETECT_FIELD"] as $field)
        {
            $arFilter[$field] = "";
        }
    }

    foreach ($importSettings["IMPORT_FIELDS_LIST"] as $letter=>$fields)
    {
        foreach ($fields as $field){

            if(isset($arFilter[substr($field, 3)])){
                $arFilter[substr($field, 3)] = $element["DATA"][$letter];
            }

            if(isset($arFilter["PROPERTY_" . substr($field, 7)])){
                $arFilter["PROPERTY_" . substr($field, 7)] = $element["DATA"][$letter];
            }

            if(!empty($field)) {
                // Набор полей
                if(substr($field, 0, 3) === "IE_"){
                    $code = substr($field, 3);
                    $arFields[$code] = $element["DATA"][$letter];
                    switch ($code){
                        case "PREVIEW_PICTURE":
                        case "DETAIL_PICTURE":
                            $arFields[$code] = explode(",", $arFields[$code]);
                            $arFields[$code] = CFile::MakeFileArray($arFields[$code][0]);
                            break;
                    }

                }
                // properties
                if(substr( $field, 0, 7 ) === "IP_PROP"){
                    $arProperties[substr($field, 7)] = $element["DATA"][$letter];
                }
                // sale prices
                if($field === "ICAT_PRICE1_PRICE"){
                    $arPrices[1] = $element["DATA"][$letter];
                }
                if($field === "ICAT_PRICE2_PRICE"){
                    $arPrices[2] = $element["DATA"][$letter];
                }
                // catalog avaliable
                if(ToUpper($field) == "ICAT_AVAILABLE"){
                    if(ToUpper($element["DATA"][$letter]) == ToUpper($arResult["SETTINGS"]["IMPORT_DATA_CATALOG_AVAILABLE"]["VALUE"])){
                        $arCatalog["AVAILABLE"] = "Y";
                    }
                    else{
                        $arCatalog["AVAILABLE"] = "N";
                    }
                }
                // product tp groopen ICAT_GROUP
                if($field === "ICAT_GROUP"){
                    $arCatalog["TP_GROUP"] = $element["DATA"][$letter];
                }
            }

        }
    }

    /**
     * Все данные с временной таблицы собраны и провалидированы
     * Начинаем процес обновления продукта и его торговых предложений
     */
    $lastID = 0;
    //////////////////////////
    // Update product if exist by data
    $arFields["IBLOCK_ID"] = $arParams["CURRENT_PROFILE"]["IBLOCK_ID"];
    $arFields["IBLOCK_SECTION_ID"] = $parentSection;
    // если используется поле группировки для торговых предложений - занимаем XML_ID
    if(!empty($arCatalog["TP_GROUP"])){
        $arFields["XML_ID"] = $arCatalog["TP_GROUP"];
    }

    if(empty($importSettings["IMPORT_DETECT_FIELD"])){
        // Add new product by data
        $lastID = setShopFields($arParams, $arFields);
    }else{

        // проверка в каталоге
        $products = CIBlockElement::GetList(
            [],
            $arFilter,
            false,
            false,
            ["ID", "NAME"]
        );

        if($product = $products->fetch())
        {
            // Update product by data
            $lastID = setShopFields($arParams, $arFields, $product["ID"]);
        }
        else {
            // Add new product by data
            $lastID = setShopFields($arParams, $arFields);
        }
    }

    // Добавление свойств
    setShopProperties($arParams, $arProperties, $lastID);

    // Check position in catalog
    // https://dev.1c-bitrix.ru/api_help/catalog/classes/ccatalogproduct/add.php
    $existProduct = Product::getCacheItem($lastID,true);


    if(empty($existProduct)){
        Product::add([
            "ID" => $lastID,
            "QUANTITY" => $arCatalog["AVAILABLE"] == "Y" ? 100000 : 0,
            "VAT_INCLUDED" => "Y",
            "AVAILABLE" => $arCatalog["AVAILABLE"]
        ]);
    }else{
        Product::update($product["ID"], [
            "QUANTITY" => $arCatalog["AVAILABLE"] == "Y" ? 100000 : 0,
            "VAT_INCLUDED" => "Y",
            "AVAILABLE" => $arCatalog["AVAILABLE"]
        ]);
    }

    foreach ($arPrices as $id=>$value){
        setShopPrice($lastID, $id, $value);
    }

    // Накапливаем продукты для информации в прогрессе загрузки
    $arProducts[] = $lastID;

    // GOTO nexst step
    // Всё равно как использовалось до этого $lastID
    // в конце концов присваиваем ему значение из временной таблицы
    $lastID = $element["ID"];
}

$rsLeftBorder = ImportTable::getList(array(
    "order" => array("ID" => "ASC"),
    "filter" => array(
        "<=ID" => $lastID
    ),
    "select" => array(
        "ID"
    )
));
$leftBorderCnt = $rsLeftBorder->getSelectedRowsCount();

$rsAll = ImportTable::getList(array(
    "order" => array("ID" => "ASC"),
    "select" => array(
        "ID"
    )
));
$allCnt = $rsAll->getSelectedRowsCount();

$p = round(100*$leftBorderCnt/$allCnt, 2);

if(empty($arProducts)){
    echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("TRANSFER_CATALOG_ELEMENT_NON_UPDATE", ["ID" => $lastID]) . '");';
}else{
    echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("TRANSFER_CATALOG_ELEMENT_UPDATE", ["ID" => $lastID, "PRODUCTS" => implode(", ", $arProducts)]) . '");';
}