<?
use Bitrix\Main\Config\Option,
    Bitrix\Main\Web\Json;


$_SERVER["DOCUMENT_ROOT"] = $argv[1];
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
require $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



// <editor-fold defaultstate="collapsed" desc=" # arParams">
$arParams = array(
    "MODULE" => "studio7spb.marketplace",
    "FILE" => "IMPORT_DATA_FILE"
);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Load">
$arResult = array(
    "FILE" => $_SERVER["DOCUMENT_ROOT"] . "/upload/excel/import.xls",
    "WORKSHEET" => array()
);

$objReader = PHPExcel_IOFactory::createReaderForFile($arResult["FILE"]);
$objPHPExcel = $objReader->load($arResult["FILE"]);
// </editor-fold>

foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    $arResult["WORKSHEET"][] = array(
        "TITLE" => $worksheet->getTitle(),
        "CURRENT" => 0
    );
}

echo Json::encode($arResult);
