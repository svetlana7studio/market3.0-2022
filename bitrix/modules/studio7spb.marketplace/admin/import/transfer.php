<?
use Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Localization\Loc,
    Studio7spb\Marketplace\ImportTable,
    Bitrix\Main\IO\File,
    Bitrix\Highloadblock,
    Bitrix\Iblock\PropertyTable,
    Studio7spb\Marketplace\ImportSettingsTable;


// <editor-fold defaultstate="collapsed" desc=" # Prepare">

/**
 * @param $PRODUCT_ID
 * @param $PRICE_TYPE_ID
 * @param $PRICE
 * @param string $CURRENCY
 */
function setShopPrice($PRODUCT_ID, $PRICE_TYPE_ID, $PRICE, $CURRENCY="RUB"){
    // умножить цену на курс
    $currency = ImportSettingsTable::getList([
        "filter" => ["CODE" => "CURRENCY_USD_RUB"]
    ]);
    if($currency = $currency->fetch()) {
        $currency["VALUE"] = intval($currency["VALUE"]);
        if($currency["VALUE"] > 0){
            $PRICE = $PRICE * $currency["VALUE"];
        }
    }
    
    $arFields = [
        "PRODUCT_ID" => $PRODUCT_ID,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
        "PRICE" => $PRICE,
        "CURRENCY" => $CURRENCY
    ];

    $res = CPrice::GetList(
        [],
        [
            "PRODUCT_ID" => $PRODUCT_ID,
            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
        ]
    );

    if ($arr = $res->Fetch())
    {
        CPrice::Update($arr["ID"], $arFields);
    }
    else
    {
        CPrice::Add($arFields);
    }

}

/**
 * @param $arParams
 * @param $arProperties
 * @return array
 * @throws \Bitrix\Main\ArgumentException
 * @throws \Bitrix\Main\ObjectPropertyException
 * @throws \Bitrix\Main\SystemException
 */
function getShopProperties($arParams, $arProperties){
    $arPropertiIDs = [];
    $arProps = [];
    foreach ($arProperties as $id=>$arProperty){
        $arPropertiIDs[] = $id;
    }

    if(!empty($arPropertiIDs)){
        $db = PropertyTable::getList([
            "filter" => [
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ID" => $arPropertiIDs
            ]
        ]);
        while ($arProperty = $db->fetch()){
            $arProps[$arProperty["ID"]] = $arProperty;
        }
    }

    return $arProps;
}

/**
 * @param array $arProps
 * @param array $arProperties
 * @return array
 */
function fillShopProperties($arProps, $arProperties){
    $arResult = [];

    if(!empty($arProps)){
        foreach ($arProps as $arProp){

            switch ($arProp["PROPERTY_TYPE"]){
                case "S": // Строка
                case "N": // Число

                    if($arProp["MULTIPLE"] == "Y"){
                        $arResult[$arProp["ID"]][] = $arProperties[$arProp["ID"]];
                    }else{
                        $arResult[$arProp["ID"]] = $arProperties[$arProp["ID"]];
                    }

                    break;
                case 'F': // Файл

                    $files = $arProperties[$arProp["ID"]];
                    $files = explode(",", $files);

                    if(empty($files)){

                    }else{
                        $arProperties[$arProp["ID"]] = [];
                        foreach ($files as $value){
                            if(!empty($value)){
                                $value = trim($value);
                                $arResult[$arProp["ID"]][] = CFile::MakeFileArray($value);
                            }

                        }
                    }

                    break;
                case 'L': // Список

                    // Использую все свойства список
                    // В перспективе учитывать раздел свойства
                    // https://dev.1c-bitrix.ru/api_help/iblock/classes/ciblockpropertyenum/getlist.php


                    $sizes = CIBlockPropertyEnum::GetList([],[
                        "IBLOCK_ID" => $arProp["IBLOCK_ID"],
                        "CODE" => $arProp["CODE"],
                        "VALUE" => $arProperties[$arProp["ID"]],
                    ]);
                    while ($size = $sizes->Fetch()){

                        if($arProp["MULTIPLE"] == "Y"){

                            if(ToUpper($arProperties[$arProp["ID"]]) == ToUpper($size["VALUE"])){
                                $arResult[$size["PROPERTY_ID"]][] = $size["ID"];
                            }else{
                                $arResult[$size["PROPERTY_ID"]][] = 1; // Если цвет не задан то он будет попадать в плашку с прочерком
                            }


                        }else{

                            if(ToUpper($arProperties[$arProp["ID"]]) == ToUpper($size["VALUE"])){
                                $arResult[$size["PROPERTY_ID"]] = $size["ID"];
                            }else{
                                $arResult[$size["PROPERTY_ID"]] = 1; // Если цвет не задан то он будет попадать в плашку с прочерком
                            }


                        }



                    }

                    break;
                case 'E': // Привязка к элементу инфоблока
                    // Просто добавить значение
                    if($arProp['USER_TYPE'] == 'SKU'){
                        $arResult[$arProp["ID"]] = $arProperties[$arProp["ID"]];
                    }
                    // проверить по значению и добавить или удалить
                    else{
                        # Set trademark
                        $arProperties["TRADE_MARK"] = [
                            "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"],
                            "XML_ID" => $arProperties[$arProp["ID"]],
                        ];
                        $arProperties["TRADE_MARK"] = CIBlockElement::GetList(
                            [],
                            $arProperties["TRADE_MARK"],
                            false,
                            false,
                            [
                                "ID"
                            ]
                        );
                        if($arProperties["TRADE_MARK"] = $arProperties["TRADE_MARK"]->Fetch())
                        {
                            $arResult[$arProp["ID"]] = $arProperties["TRADE_MARK"]["ID"];
                        }
                        else{
                            $neo = new CIBlockElement();
                            $arResult[$arProp["ID"]] = $neo->Add([
                                "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"],
                                "XML_ID" => $arProperties[$arProp["ID"]],
                                "NAME" => $arProperties[$arProp["ID"]]
                            ]);

                        }
                    }

            }
        }
    }

    return $arResult;
}

/**
 * @param $arParams
 * @param $arProperties
 * @param $productID
 */
function addShopProperties($arParams, $arProperties, $productID)
{
    CIBlockElement::SetPropertyValuesEx($productID, $arParams["IBLOCK_ID"], $arProperties);
}

/**
 * Набираем тут arProperties и обновдяем его элементу
 * @param $files
 * @param $arParams
 * @param $element
 * @param $product
 */
function setShopProperties($arParams, $arProperties, $productID){
    $arProps = getShopProperties($arParams, $arProperties);
    $arProperties = fillShopProperties($arProps, $arProperties);
    addShopProperties($arParams, $arProperties, $productID);
}

/**
 * @param array $arParams
 * @param array $arFields
 * @param int $productID
 * @return int
 */
function setShopFields($arParams, $arFields, $productID=0){

    //AddMessage2Log($arFields, "setShopFields " . $productID);

    $neo = new CIBlockElement();
    // Дополняем массив полей
    $arFields["ACTIVE"] = "Y";
    $arFields["DETAIL_TEXT_TYPE"] = "html";

    if($productID > 0){
        $lastID = $productID;
        $neo->Update($productID, $arFields);
    }else{
        $lastID = $neo->Add($arFields);
    }

    return $lastID;
}

$arParams = [
    "MODULE" => "studio7spb.marketplace",
    "FILE" => "IMPORT_DATA_FILE",
    "START_IMPORT" => "work_start",
    "STOP_IMPORT" => "work_stop",
    "PROGRESS_IMPORT" => "work_progress",
    "START_ANALIZE" => "analize_start",
    "STOP_ANALIZE" => "analize_stop",
    "PROGRESS_ANALIZE" => "analize_progress",
    "IBLOCK_ID" => 2,
    "IBLOCK_CML2_LINK" => 3,
    "TRADE_MARK_IBLOCK_ID" => 7,
    "LIMIT" => 5
];

$arResult = [
    "FILE" => null,
    "SETTINGS" => [],
    "IBLOCK" => []
];

if (isset($_REQUEST[$arParams["START_IMPORT"]]))
{
    define("NO_AGENT_STATISTIC", true);
    define("NO_KEEP_STATISTIC", true);
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
CJSCore::Init(array("jquery"));
//Loader::includeModule("highloadblock");
Loader::includeModule("iblock");
Loader::includeModule("catalog");
Loc::loadLanguageFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::getMessage("TRANSFER_ACCESS_DENIED"));

// get import settings
$importSettings = ImportSettingsTable::getList(array(
    "filter" => array("%CODE" => "IMPORT_")
));
while($importSetting = $importSettings->fetch()){
    $arResult["SETTINGS"][$importSetting["CODE"]] = $importSetting;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Save import file">
// get file
$arResult["FILE"] = $arResult["SETTINGS"]["IMPORT_DATA_FILE"]["VALUE"];
// if send new file
if(!empty($_POST["import_file"]) && !empty($_FILES[$arParams["FILE"]])){
    // delete old if exist
    //if($arResult["FILE"] > 0){
    //    CFile::Delete($arResult["FILE"]);
    //}

    // $file["name"]
    $file = $_FILES[$arParams["FILE"]];
    // and save to settings

    if(empty($file["tmp_name"])) {
        Option::set($arParams["MODULE"], $arParams["FILE"], null);
    }else{
        ImportSettingsTable::update($arResult["SETTINGS"]["IMPORT_DATA_FILE"]["ID"], array("VALUE" => $file["name"]));
        Option::set($arParams["MODULE"], $arParams["FILE"], "IMPORT_DATA_FILE");
        $file = $file["tmp_name"];
        $file = Bitrix\Main\IO\File::getFileContents($file);
        File::putFileContents($_SERVER["DOCUMENT_ROOT"] . "/upload/excel/import.csv", $file);
    }

    ImportSettingsTable::update($arResult["SETTINGS"]["IMPORT_DATA_VENDOR"]["ID"], array("VALUE" => $_POST["vendor"]));
    ImportSettingsTable::update($arResult["SETTINGS"]["IMPORT_DATA_VENDOR_DEACTIVATE"]["ID"], array("VALUE" => $_POST["vendor_deactivate"]));

    LocalRedirect($APPLICATION->GetCurPageParam("file=upload", array("file")));
}

if(empty($arResult["FILE"])){
    $arResult["FILE"] = null;
}
else{
    $arResult["FILE"] = "/upload/excel/import.xls";
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # One-sep Analize process">

if($_REQUEST[$arParams["START_ANALIZE"]] == "Y" && check_bitrix_sessid())
{

    // Первый вызов - тут чтение файла
    // и запись во временную таблицу
    if(!isset($_REQUEST["lastid"])) {
        // записать во временную таблицу
        echo shell_exec('php ' . $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/studio7spb.marketplace/admin/import/db.fill.php ' . $_SERVER["DOCUMENT_ROOT"]);
    }
    else{
        // TODO работа со статусами во временной таблице
        // TODO проверка обязательных полей
        // TODO http://teatr-msk.ru/bitrix/admin/iblock_edit.php?type=marketplace&tabControl_active_tab=edit2&lang=ru&ID=2&admin=Y
        $elements = ImportTable::getList(array(
            "order" => array("ID" => "ASC"),
            "filter" => array(
                ">ID" => $_REQUEST["lastid"]
            ),
            "select" => array(
                "ID",
                "DATA"
            ),
            "limit" => $arParams["LIMIT"]
        ));
        $error = array();

        while ($element = $elements->fetch())
        {
            /*
             * TODO работа с полем статуса
             * TODO ImportTable::update($element["ID"], array("STATUS" => "Y"))
             */

            $element["DATA"] = unserialize($element["DATA"]);

            foreach ($element["DATA"] as $key=>$value)
            {
                if(in_array($key, $arParams["FILE_FIELDS"]["REQUIRED"])){

                    if(empty($value)){
                        $error[] = Loc::getMessage("TRANSFER_FIELD_EMPTY") . ": <b>" . $arParams["FILE_FIELDS"]["HEADERS"][$key] . " (" . $key . ")</b>";
                    }else{

                        // Свитчер более детальной валидации
                        /*
                        switch ($key){
                            // проверка любого из требуемых полей на чило
                            // цены
                            case "H":
                            case "I":
                                if(!is_numeric($value)){
                                    $error[] = "Значение цены не является числом: <b>" . $arParams["FILE_FIELDS"]["HEADERS"][$key] . " (" . $key . ")</b> Вместо такого значения " . $value . " должно быть число, так как на его основе проводится калькуляция цен в каталоге";
                                }
                                break;
                            // проверка на чило внешнего кода раздела
                            // Group code
                            case "D":
                                if(!is_numeric($value)){
                                    $error[] = "Значение внешнего кода раздела (Group code) не является числом: <b>aaaaaaaaa" . $arParams["FILE_FIELDS"]["HEADERS"][$key] . " (" . $key . ")</b> Вместо такого значения " . $value . " должно быть число, так как по нему идёт привязка товара к группе (к разделу сайта)";
                                }
                                break;


                        }
                        */

                    }

                }
            }

            $lastID = $element["ID"];
        }

        $rsLeftBorder = ImportTable::getList(array(
            "order" => array("ID" => "ASC"),
            "filter" => array(
                "<=ID" => $lastID
            ),
            "select" => array(
                "ID"
            )
        ));
        $leftBorderCnt = $rsLeftBorder->getSelectedRowsCount();

        $rsAll = ImportTable::getList(array(
            "order" => array("ID" => "ASC"),
            "select" => array(
                "ID"
            )
        ));
        $allCnt = $rsAll->getSelectedRowsCount();

        $p = round(100*$leftBorderCnt/$allCnt, 2);

        if(empty($error)){
            echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","Статус проверки. Последний проверенный товар №: '.$lastID.'. Ошибок не обнаружено.", "success");';
        }else{
            $error = implode(", <br />", $error);
            echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","Товар №: '.$lastID.'. Ошибка: ' . $error . '", "error");';
        }
    }

    die();
}
else{

    if($_REQUEST["transfer"] === "worksheet"){

        if($_POST["worksheet_catalog"] !== "0"){
            ImportSettingsTable::update($arResult["SETTINGS"]["IMPORT_DATA_CATALOG"]["ID"], array("VALUE" => trim($_POST["worksheet_catalog"])));
            ImportSettingsTable::update($arResult["SETTINGS"]["IMPORT_DATA_CATALOG_START"]["ID"], array("VALUE" => trim($_POST["worksheet_catalog_start"])));
            echo shell_exec('php ' . $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/studio7spb.marketplace/admin/import/worksheet.catalog.php ' . $_SERVER["DOCUMENT_ROOT"]);
        }
        else{
            echo shell_exec('php ' . $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/studio7spb.marketplace/admin/import/worksheet.php ' . $_SERVER["DOCUMENT_ROOT"]);
        }

        ImportSettingsTable::update($arResult["SETTINGS"]["IMPORT_DATA_VENDOR"]["ID"], array("VALUE" => $_POST["vendor"]));
        ImportSettingsTable::update($arResult["SETTINGS"]["IMPORT_DATA_VENDOR_DEACTIVATE"]["ID"], array("VALUE" => $_POST["vendor_deactivate"]));

        die;
    }
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # One-sep Import process with checking iblock by SKU">
if($_REQUEST[$arParams["START_IMPORT"]] && check_bitrix_sessid())
{
    $arResult["IBLOCK"] = CCatalogSKU::GetInfoByProductIBlock($arParams["IBLOCK_ID"]);
    if (is_array($arResult["IBLOCK"]))
    {
        // 'ID инфоблока торговых предложений = '.$mxResult['IBLOCK_ID'];
        $arParams["IBLOCK_CML2_LINK"] = $arResult["IBLOCK"]['IBLOCK_ID'];
        require_once "transfer.import.tp.php";
    }
    else
    {
        // ShowError('У этого инфоблока нет SKU');
        unset($arParams["IBLOCK_CML2_LINK"]);
        require_once "transfer.import.php";
    }

    die();
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Formating Analize script">
$arResult["CLEAN_ANALIZE_TABLE"] = '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="internal" id="' . $arParams["START_ANALIZE"] . '-info-table">'.
    '<tr class="heading">'.
    '<td>Текущий процесс</td>'.
    '</tr>'.
    '<tr>'.
    '<td id="' . $arParams["START_ANALIZE"] . '-info">&nbsp;</td>'.
    '</tr>'.
    '<tr class="heading">'.
    '<td>Список ошибок в файле импорта</td>'.
    '</tr>'.
    '</table>';
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Formating Import script">
$arResult["CLEAN_IMPORT_TABLE"] = '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="internal">'.
    '<tr class="heading">'.
    '<td>Текущий процесс</td>'.
    '</tr>'.
    '<tr>'.
    '<td id="' . $arParams["START_IMPORT"] . '-info">&nbsp;</td>'.
    '</tr>'.
    '</table>';
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Admin and tabs controls">

$aTabs = array(
    array("DIV" => $arParams["FILE"] . "_ANALIZE", "TAB" => "Проверка файла импорта", "ICON"=>"main_user_edit", "TITLE" => "Проверка файла импорта"),
    array("DIV" => $arParams["FILE"] . "_IMPORT", "TAB" => "Импорт товаров", "ICON"=>"main_user_edit", "TITLE" => "Импорт товаров")
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle("Импорт товаров");

// </editor-fold>

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$tabControl->Begin();

// <editor-fold defaultstate="collapsed" desc=" # Tab: Validate">
$tabControl->BeginNextTab();
?>
    <tr>
        <td colspan="2">

            <input type=button value="Запустить" id="<?=$arParams["START_ANALIZE"]?>" onclick="analizeStart(1)" />
            <input type=button value="Остановить" disabled id="<?=$arParams["STOP_ANALIZE"]?>" onclick="bSubmit=false;analizeStart(0)" />
            <div id="<?=$arParams["PROGRESS_ANALIZE"]?>" style="display:none;" width="100%">
                <br />
                <div id="<?=$arParams["START_ANALIZE"]?>-status"></div>
                <table border="0" cellspacing="0" cellpadding="2" width="100%">
                    <tr>
                        <td height="10">
                            <div style="border:1px solid #B9CBDF">
                                <div id="<?=$arParams["START_ANALIZE"]?>-indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
                            </div>
                        </td>
                        <td width=30>&nbsp;<span id="<?=$arParams["START_ANALIZE"]?>-percent">0%</span></td>
                    </tr>
                </table>
            </div>
            <div id="<?=$arParams["START_ANALIZE"]?>-result" style="padding-top:10px"></div>

        </td>
    </tr>

<?
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Tab: Import-action">
$tabControl->BeginNextTab();
?>

    <tr>
        <td colspan="2">

            <input type=button value="Запустить" id="<?=$arParams["START_IMPORT"]?>" onclick="import_start(1)" />
            <input type=button value="Остановить" disabled id="<?=$arParams["STOP_IMPORT"]?>" onclick="bSubmit=false;import_start(0)" />
            <div id="<?=$arParams["PROGRESS_IMPORT"]?>" style="display:none;" width="100%">
                <br />
                <div id="<?=$arParams["START_IMPORT"]?>-status"></div>
                <table border="0" cellspacing="0" cellpadding="2" width="100%">
                    <tr>
                        <td height="10">
                            <div style="border:1px solid #B9CBDF">
                                <div id="<?=$arParams["START_IMPORT"]?>-indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
                            </div>
                        </td>
                        <td width=30>&nbsp;<span id="<?=$arParams["START_IMPORT"]?>-percent">0%</span></td>
                    </tr>
                </table>
            </div>
            <div id="<?=$arParams["START_IMPORT"]?>-result"></div>

        </td>
    </tr>

<?
// </editor-fold>

$tabControl->End();

// <editor-fold defaultstate="collapsed" desc=" # Scripts">
?>
    <script type="text/javascript">

        var bWorkFinished = false;
        var bSubmit;

        /**
         * import start
         * @param val
         */
        function import_start(val)
        {
            document.getElementById('<?=$arParams["START_IMPORT"]?>').disabled = val ? 'disabled' : '';
            document.getElementById('<?=$arParams["STOP_IMPORT"]?>').disabled = val ? '' : 'disabled';
            document.getElementById('<?=$arParams["PROGRESS_IMPORT"]?>').style.display = val ? 'block' : 'none';

            if (val)
            {
                ShowWaitWindow();
                document.getElementById('<?=$arParams["START_IMPORT"]?>-result').innerHTML = '<?=$arResult["CLEAN_IMPORT_TABLE"]?>';
                document.getElementById('<?=$arParams["START_IMPORT"]?>-status').innerHTML = 'Идёт прогресс импорта...';

                document.getElementById('<?=$arParams["START_IMPORT"]?>-percent').innerHTML = '0%';
                document.getElementById('<?=$arParams["START_IMPORT"]?>-indicator').style.width = '0%';

                CHttpRequest.Action = import_onload;
                CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?<?=$arParams["START_IMPORT"]?>=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>');
            }
            else
                CloseWaitWindow();
        }

        function import_onload(result)
        {
            try
            {
                eval(result);

                iPercent = CurrentStatus[0];
                strNextRequest = CurrentStatus[1];
                strCurrentAction = CurrentStatus[2];

                document.getElementById('<?=$arParams["START_IMPORT"]?>-percent').innerHTML = iPercent + '%';
                document.getElementById('<?=$arParams["START_IMPORT"]?>-indicator').style.width = iPercent + '%';

                document.getElementById('<?=$arParams["START_IMPORT"]?>-status').innerHTML = 'Идёт прогресс импорта...';

                if (strCurrentAction != 'null')
                {

                    var objInfo = BX('<?=$arParams["START_IMPORT"]?>-info'),
                        limit = 10,
                        count = BX.findChildren(BX(objInfo), {'className':'m10'}, true),
                        node = BX.create("div", {
                            props: {
                                className: "m10"
                            },
                            text: strCurrentAction
                        });

                    if (!!count) {
                        count = count.length;
                        if(count > limit){
                            objInfo.innerHTML = "";
                        }
                    }

                    BX.append(node, objInfo);

                }


                if (strNextRequest && document.getElementById('<?=$arParams["START_IMPORT"]?>').disabled)
                    CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?<?=$arParams["START_IMPORT"]?>=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>' + strNextRequest);
                else
                {
                    import_start(0);
                    bWorkFinished = true;
                }

            }
            catch(e)
            {
                CloseWaitWindow();
                document.getElementById('<?=$arParams["START_IMPORT"]?>').disabled = '';
                alert('Сбой в получении данных');
            }
        }

        /**
         * import start
         * @param val
         */
        function analizeStart(val)
        {
            document.getElementById('<?=$arParams["START_ANALIZE"]?>').disabled = val ? 'disabled' : '';
            document.getElementById('<?=$arParams["STOP_ANALIZE"]?>').disabled = val ? '' : 'disabled';
            document.getElementById('<?=$arParams["PROGRESS_ANALIZE"]?>').style.display = val ? 'block' : 'none';

            if (val)
            {
                ShowWaitWindow();
                document.getElementById('<?=$arParams["START_ANALIZE"]?>-result').innerHTML = '<?=$arResult["CLEAN_ANALIZE_TABLE"]?>';
                document.getElementById('<?=$arParams["START_ANALIZE"]?>-status').innerHTML = 'Идёт процесс проверки...';

                document.getElementById('<?=$arParams["START_ANALIZE"]?>-percent').innerHTML = '0%';
                document.getElementById('<?=$arParams["START_ANALIZE"]?>-indicator').style.width = '0%';

                CHttpRequest.Action = analize_onload;
                CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?<?=$arParams["START_ANALIZE"]?>=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>');
            }
            else
                CloseWaitWindow();
        }

        function analize_onload(result)
        {
            try
            {
                eval(result);

                iPercent = CurrentStatus[0];
                strNextRequest = CurrentStatus[1];
                strCurrentAction = CurrentStatus[2];

                document.getElementById('<?=$arParams["START_ANALIZE"]?>-percent').innerHTML = iPercent + '%';
                document.getElementById('<?=$arParams["START_ANALIZE"]?>-indicator').style.width = iPercent + '%';
                document.getElementById('<?=$arParams["START_ANALIZE"]?>-status').innerHTML = 'дождитесь окончания прогресса...';

                if (strCurrentAction != 'null')
                {
                    resultTableInfo = document.getElementById('<?=$arParams["START_ANALIZE"]?>-info');
                    resultTableInfo.innerHTML = strCurrentAction;

                    if(CurrentStatus[3] == "error"){
                        resultTableInfo = document.getElementById('<?=$arParams["START_ANALIZE"]?>-info-table');

                        // it to mach errors
                        if($(resultTableInfo).find("tr").length > 11){
                            analizeStart(0);
                            bWorkFinished = true;
                        }

                        oRow = resultTableInfo.insertRow(-1);
                        oCell = oRow.insertCell(-1);
                        oCell.innerHTML = strCurrentAction;
                    }




                }

                if (strNextRequest && document.getElementById('<?=$arParams["START_ANALIZE"]?>').disabled)
                    CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?<?=$arParams["START_ANALIZE"]?>=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>' + strNextRequest);
                else
                {
                    analizeStart(0);
                    bWorkFinished = true;

                    if($("#<?=$arParams["START_ANALIZE"]?>-info-table").find("tr").length <= 6){
                        tabControl.EnableTab('<?=$arParams["FILE"] . "_IMPORT"?>');
                    }

                }

            }
            catch(e)
            {
                CloseWaitWindow();
                document.getElementById('<?=$arParams["START_ANALIZE"]?>').disabled = '';
                alert('Сбой в получении данных');
            }
        }

        var marketplaceTransfer = {

            worksheet: {
                catalog: "#worksheet-catalog",
                preview: "#worksheet-preview"
            },

            preview: function(catalog)
            {


                var i = 0,
                    n = 0,
                    key,
                    table = [],
                    tr;

                for(i = 0; i < catalog.length; i++){
                    n++;
                    // set headders
                    if(i === 0){
                        tr = "<tr>";
                        tr += "<th>№</th>";
                        for(key in catalog[i]){
                            tr += "<th>" + key + "</th>";
                        }
                        tr += "</tr>";
                        table.push(tr);
                    }

                    // set rows
                    tr = "<tr>";
                    tr += "<th>" + n +  "</th>";
                    for(key in catalog[i]){
                        tr += "<td>" + catalog[i][key] + "</td>";
                    }
                    tr += "</tr>";
                    table.push(tr);

                }

                //table.append(tr);

                $(marketplaceTransfer.worksheet.preview)
                    .fadeIn(300)
                    .find("table")
                    .html(table.join(''));

            },

            submit: function () {
                ShowWaitWindow();
                tabControl.DisableTab('<?=$arParams["FILE"] . "_IMPORT"?>');

                var form = $("#post_form"),
                    action = form.attr("action"),
                    data = form.serializeArray(),
                    i;

                if(!!data && data.length > 0){
                    for(i=0; i < data.length; i++){
                        if(data[i].name == "vendor"){
                            value = parseInt(data[i].value);
                            if(value > 0)
                            {
                                tabControl.EnableTab('<?=$arParams["FILE"] . "_ANALIZE"?>');
                            }else{
                                tabControl.DisableTab('<?=$arParams["FILE"] . "_ANALIZE"?>');
                            }
                        }
                    }
                }

                $.post(action, data, function (result) {
                    // fill catalog
                    if(!!result.WORKSHEET && result.WORKSHEET.length > 0){
                        $(marketplaceTransfer.worksheet.catalog).empty();
                        for(i = 0; i < result.WORKSHEET.length; i++){


                            if(result.WORKSHEET[i].CURRENT > 0){
                                $(marketplaceTransfer.worksheet.catalog).append($("<option>", {
                                    text: result.WORKSHEET[i].TITLE,
                                    value: result.WORKSHEET[i].TITLE,
                                    selected: "selected"
                                }));
                            }else{
                                $(marketplaceTransfer.worksheet.catalog).append($("<option>", {
                                    text: result.WORKSHEET[i].TITLE,
                                    value: result.WORKSHEET[i].TITLE,
                                }));
                            }


                        }
                    }
                    // fill preview
                    if(!!result.CATALOG && result.CATALOG.length > 0){
                        marketplaceTransfer.preview(result.CATALOG);
                    }


                    if(!!result.ERRORS)
                    {
                        console.info(result);
                    }



                    CloseWaitWindow();
                }, "json");

                return false;
            }

        };

        tabControl.DisableTab('<?=$arParams["FILE"] . "_IMPORT"?>');

    </script>
<?
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Styles">
?>
    <style>
        #worksheet-preview{
            width: 100%;
            max-width: 1200px;
            overflow-x: scroll;
        }
        #worksheet-preview th,
        #worksheet-preview td{
            border: 1px solid #333;
            text-align: center;
            max-width: 200px;
            overflow: hidden;
        }
        #worksheet-preview th{
            background-color: #cecece;
        }
        .m10{margin: 10px;}
    </style>
<?
// </editor-fold>

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>