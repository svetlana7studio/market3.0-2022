<?
/**
 * @var CMain $APPLICATION
 */
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application,
    Bitrix\Main\Loader,
    Studio7spb\Marketplace\Import\ImportProfileListTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadLanguageFile(__FILE__);
$request = Application::getInstance()->getContext()->getRequest();

$sTableID = ImportProfileListTable::getTableName();

$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminUiList($sTableID, $oSort);

Loader::includeModule("iblock");
Loader::registerAutoLoadClasses("studio7spb.marketplace", [
    "\\Studio7spb\\Marketplace\\Import\\ImportProfileListTable" => "lib/import/importprofilelisttable.php"
]);

// FIELDS UPDATE
if($request->isPost() && $lAdmin->EditAction())
{
    foreach($FIELDS as $ID=>$arFields) {
        if (!$lAdmin->IsUpdated($ID))
            continue;

        $ID = (int) $ID;
        if($ID > 0){
            ImportProfileListTable::update($ID, $arFields);
        }
    }
}

if(($arID = $lAdmin->GroupAction()))
{
    foreach($arID as $ID)
    {
        switch ($_REQUEST['action'])
        {
            case "activate":
                ImportProfileListTable::update($ID, ["ACTIVE" => "Y"]);
                break;
            case "deactivate":
                ImportProfileListTable::update($ID, ["ACTIVE" => "N"]);
                break;
            case "delete":
                $propfile = ImportProfileListTable::getList([
                    "filter" => [
                        "ID" => $ID
                    ]
                ]);
                if($propfile = $propfile->fetch())
                {
                    CFile::Delete($propfile["FILE"]);
                    ImportProfileListTable::delete($ID);
                }


                break;
        }

    }
}

$headers = [];
foreach (ImportProfileListTable::getMap() as $element){
    /**
     * @var Bitrix\Main\ORM\Fields\Field $element
     */
    if(in_array($element->getName(), [
        "FIELDS",
        "SAMPLE",
        "START",
        "IDENT",
        "SEPAR",
        "AVALIABLE_STRING"
    ]))
        continue;
    $headers[] = [
        "id"    => $element->getName(),
        "content"  => $element->getTitle(),
        "sort"    => $element->getName(),
        "default"  =>true
    ];
}
$lAdmin->AddHeaders($headers);

$elements = $elements = ImportProfileListTable::getList([
    "select" => [
        "ID",
        "ACTIVE",
        "NAME",
        "SORT",
        "IBLOCK_TYPE",
        "IBLOCK_ID",
        "IBLOCK_SECTION_ID",
        "SECTION_NAME" => "SECTION.NAME",
        "FILE",
        "CUR"
    ],
    "runtime" => [
        new \Bitrix\Main\Entity\ReferenceField(
            "SECTION",
            "\Bitrix\Iblock\SectionTable",
            ["this.IBLOCK_SECTION_ID" => "ref.ID"],
            ["join_type" => "left"]
        )
    ]
]);


while($element = $elements->fetch()){
    $row =& $lAdmin->AddRow($element["ID"], $element);
    $row->AddCheckField("ACTIVE", $element["ACTIVE"]);
    $row->AddViewField("NAME", $element["NAME"]);
    $row->AddInputField("NAME", ["size"=>20]);
    $row->AddViewField("SORT", $element["SORT"]);
    $row->AddInputField("SORT", ["size"=>5]);
    $row->AddViewField("IBLOCK_TYPE", $element["IBLOCK_TYPE"]);
    $row->AddViewField("IBLOCK_ID", $element["IBLOCK_ID"]);
    $row->AddViewField(
        "IBLOCK_SECTION_ID",
        '<a href="iblock_element_admin.php?' . http_build_query([
            'lang' => LANGUAGE_ID,
            'type' => $element["IBLOCK_TYPE"],
            'IBLOCK_ID' => $element["IBLOCK_ID"],
            'SECTION_ID' => $element["IBLOCK_SECTION_ID"]
        ]) . '" target="_blank">' . $element["SECTION_NAME"] . '</a>'
    );
    $row->AddViewField("FILE", $element["FILE"]);
    $row->AddViewField("FILE", '<a href="' . CFile::GetPath($element["FILE"]) . '" download="download">' . Loc::getMessage("PROFILE_DOWNLOAD") . '</a>');
    $row->AddCheckField("CUR", $element["CUR"]);

    $row->AddActions([
        [
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => Loc::getMessage("PROFILE_EDIT"),
            "ACTION" => $lAdmin->ActionRedirect("studio7spb.marketplace_transfer.settings.php?profile=".$element["ID"])
        ],
        [
            "ICON" => "delete",
            "TEXT" => Loc::getMessage("PROFILE_DELETE"),
            "ACTION" => "if(confirm('" . Loc::getMessage("PROFILE_DELETE_CONFIRM") . "')) " . $lAdmin->ActionDoGroup($element["ID"], "delete")
        ]
    ]);
}

// резюме таблицы
$lAdmin->AddFooter([
    ["title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$elements->getSelectedRowsCount()],
    ["counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"]
]);

// групповые действия
$lAdmin->AddGroupActionTable([
    'edit' => true,
    'delete' => true,
    "activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"),
    "deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"),
]);

// ******************************************************************** //
//                АДМИНИСТРАТИВНОЕ МЕНЮ                                 //
// ******************************************************************** //

$aContext = [
    [
        "TEXT" => Loc::getMessage("PROFILE_ADD"),
        "LINK" => "/bitrix/admin/studio7spb.marketplace_transfer.settings.php?lang=".LANG,
        "TITLE" => Loc::getMessage("PROFILE_ADD"),
        "ICON" => "btn_new"
    ]
];
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                ВЫВОД                                                 //
// ******************************************************************** //
// альтернативный вывод
$lAdmin->CheckListMode();

$APPLICATION->SetTitle( Loc::getMessage("TITLE") );
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
$lAdmin->DisplayList();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");