<?
/**
 * Based on Версия 1,5
 */

use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc,
    Studio7spb\Marketplace\SectionPropertyTable;
use Studio7spb\Marketplace\SectionElementTable;

if (isset($_REQUEST['work_start']))
{
    define("NO_AGENT_STATISTIC", true);
    define("NO_KEEP_STATISTIC", true);
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

Loader::includeModule("iblock");
Loader::registerAutoLoadClasses("studio7spb.marketplace", [
    "\\Studio7spb\\Marketplace\\SectionPropertyTable" => "lib/anytos/sectionpropertytable.php",
    "\\Studio7spb\\Marketplace\\SectionElementTable" => "lib/anytos/sectionelementtable.php",
    "\\Studio7spb\\Marketplace\\ElementPropS2Table" => "lib/anytos/elementprops2table.php"
]);

Loc::loadLanguageFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::getMessage("INDEXING_ACCESS_DENIED"));

$arParams = [
    "FILTER" => [
        "IBLOCK_ID" => 9,
        //"!PROPERTY_TRADE_MARK" => false
    ],
    "SELECT" => [
        "ID",
        "NAME",
        "IBLOCK_ID"
    ],
    "LIMIT" => 1
];

if($_REQUEST['work_start'] && check_bitrix_sessid())
{

    $arParams["FILTER"][">ID"] = $_REQUEST["lastid"];
    $brands = CIBlockElement::GetList(["ID" => "ASC"], $arParams["FILTER"], false, ["nTopCount" => $arParams["LIMIT"]], $arParams["SELECT"]);

    while ($brand = $brands->Fetch())
    {
        /**
         * start do something
         */

        $brendSections = SectionElementTable::getList([
            "filter" => [
                "PARENT_SECTION.ACTIVE" => "Y",
                "PARENT_SECTION.DEPTH_LEVEL" => 1,
                "PROPERTY.PROPERTY_267" => $brand["ID"]
            ],
            "group" => ["PARENT_SECTION.ID"],
            "select" => [
                "PARENT_SECTION_ID" => "PARENT_SECTION.ID",
            ]
        ]);

        $parents = [];

        while ($brendSection = $brendSections->fetch())
        {
            $parents[] = $brendSection["PARENT_SECTION_ID"];
        }

        if(!empty($parents)) {
            foreach ($parents as $parent) {
                $parentSection = SectionPropertyTable::getList([
                    "limit" => 1,
                    "filter" => [
                        "SECTION_ID" => $parent,
                        "BREND_ID" => $brand["ID"]
                    ]
                ]);
                if($parentSection->getSelectedRowsCount() > 0)
                {
                    // Meby it need updatin later
                }
                else{
                    SectionPropertyTable::add([
                        "SECTION_ID" => $parent,
                        "BREND_ID" => $brand["ID"]
                    ]);
                }
            }
        }

        /**
         * end do something
         */
        $lastID = intval($brand["ID"]);
    }

    unset($arParams["FILTER"][">ID"]);
    $arParams["FILTER"]["<=ID"] = $lastID;

    $rsLeftBorder = CIBlockElement::GetList(["ID" => "ASC"], $arParams["FILTER"], false, false, ["ID"]);
    $leftBorderCnt = $rsLeftBorder->SelectedRowsCount();
    unset($arParams["FILTER"]["<=ID"]);

    $rsAll = CIBlockElement::GetList(["ID" => "ASC"], $arParams["FILTER"], false, false, ["ID"]);
    $allCnt = $rsAll->SelectedRowsCount();

    $p = round(100*$leftBorderCnt/$allCnt, 2);

    echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("TRANSFER_PRODUCT", ["#ID#" => $lastID, "#NAME#" => ""]) . '");';

    die();
}

$clean_test_table = '<table id="result_table" cellpadding="0" cellspacing="0" border="0" width="100%" class="internal">'.
    '<tr class="heading">'.
    '<td>' . Loc::getMessage("TRANSFER_CURRENT_ACTION") . '</td>'.
    '<td width="1%">&nbsp;</td>'.
    '</tr>'.
    '</table>';

$aTabs = array(array("DIV" => "edit1", "TAB" => Loc::getMessage("TRANSFER_PLAYER")));
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle(Loc::getMessage("INDEXING_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");





?>


    <form method="post" action="<?echo $APPLICATION->GetCurPage()?>" enctype="multipart/form-data" name="post_form" id="post_form">
        <?
        echo bitrix_sessid_post();

        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>
        <tr>
            <td>
                <audio id="audio" controls loop>
                    <source src="/bitrix/css/studio7spb.marketplace/sounds/tunel.mp3" type="audio/mpeg">
                    Your browser does not support the audio element.
                </audio>
            </td>
            <td>
                <?
                $moduleId = 'studio7spb.marketplace';
                $moduleJsId = str_replace('.', '_', $moduleId);
                CJSCore::Init([

                    $moduleJsId . "_player"
                ]);

                echo BeginNote();
                echo Loc::getMessage("TRANSFER_PLAYER_DESCRIPTION");
                echo EndNote();
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">

                <div id="work_start"
                     onclick="set_start(1)"
                     title="<?=Loc::getMessage("TRANSFER_PLAYER_START")?>"></div>
                <div disabled
                     id="work_stop"
                     onclick="bSubmit=false;set_start(0)"
                     title="<?=Loc::getMessage("TRANSFER_PLAYER_STOP")?>"></div>
                <div id="progress" style="display:none;" width="100%">
                    <br />
                    <div id="status"></div>
                    <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td height="10">
                                <div style="border:1px solid #B9CBDF">
                                    <div id="indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
                                </div>
                            </td>
                            <td width=30>&nbsp;<span id="percent">0%</span></td>
                        </tr>
                    </table>
                </div>
                <div id="result" style="padding-top:10px"></div>

            </td>
        </tr>
        <?
        $tabControl->End();
        ?>
    </form>

    <script type="text/javascript">

        var bWorkFinished = false;
        var bSubmit;
        var audio = document.getElementById('audio');


        function set_start(val)
        {

            document.getElementById('work_start').disabled = val ? 'disabled' : '';
            document.getElementById('work_stop').disabled = val ? '' : 'disabled';
            document.getElementById('progress').style.display = val ? 'block' : 'none';

            if (val)
            {
                ShowWaitWindow();
                audio.play();
                document.getElementById('result').innerHTML = '<?=$clean_test_table?>';
                document.getElementById('status').innerHTML = 'Работаю...';

                document.getElementById('percent').innerHTML = '0%';
                document.getElementById('indicator').style.width = '0%';

                CHttpRequest.Action = work_onload;
                CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>');
            }
            else {
                CloseWaitWindow();
                audio.pause();
                audio.currentTime = 0;
            }
        }

        function work_onload(result)
        {
            try
            {
                eval(result);

                iPercent = CurrentStatus[0];
                strNextRequest = CurrentStatus[1];
                strCurrentAction = CurrentStatus[2];

                document.getElementById('percent').innerHTML = iPercent + '%';
                document.getElementById('indicator').style.width = iPercent + '%';

                document.getElementById('status').innerHTML = 'Работаю...';

                if (strCurrentAction != 'null')
                {
                    oTable = document.getElementById('result_table');

                    if(oTable.rows.length > 5)
                    {
                        oTable.innerHTML = '';
                    }

                    oRow = oTable.insertRow(-1);
                    oCell = oRow.insertCell(-1);
                    oCell.innerHTML = strCurrentAction;
                    oCell = oRow.insertCell(-1);
                    oCell.innerHTML = '';
                }

                if (strNextRequest && document.getElementById('work_start').disabled)
                    CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>' + strNextRequest);
                else
                {
                    set_start(0);
                    bWorkFinished = true;
                }

            }
            catch(e)
            {
                CloseWaitWindow();
                audio.pause();
                audio.currentTime = 0;
                document.getElementById('work_start').disabled = '';
                alert('Сбой в получении данных');
            }
        }

    </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>