<?
/**
 * Версия 1,5
 * Loging after do something
 */
if (isset($_REQUEST['work_start']))
{
    define("NO_AGENT_STATISTIC", true);
    define("NO_KEEP_STATISTIC", true);
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
CModule::IncludeModule("iblock");
IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm("Доступ запрещен");

$BID = 2;
$limit = 1;

if($_REQUEST["lastid"] <= 0){
    $arFilter = [
        "ACTIVE" => "Y",
        "IBLOCK_ID" => 2,
        "!PROPERTY_ANYTOS_DETAIL_PICTURE" => false,
        "PREVIEW_PICTURE" => false,
        "!SORT" => 10
    ];
    $elements = CIBlockElement::GetList(["ID" => "ASC"], $arFilter, false, false, ["ID"]);
    $elementsCount = $elements->SelectedRowsCount();
    /*
    if($elements = $elements->Fetch())
    {
        $_REQUEST["lastid"] = $elements["ID"];
    }
    */
}




if($_REQUEST['work_start'] && check_bitrix_sessid())
{
    $arFilter = [
        "ACTIVE" => "Y",
        "IBLOCK_ID" => $BID,
        "!PROPERTY_ANYTOS_DETAIL_PICTURE" => false,
        "PREVIEW_PICTURE" => false,
        "!SORT" => 10
    ];

    $arSelect = [
        "ID",
        "IBLOCK_ID",
        "NAME",
        "ACTIVE",
        "DETAIL_PICTURE",
        "PREVIEW_PICTURE",
        "PROPERTY_ANYTOS_DETAIL_PICTURE"
    ];

    $arFilter[">ID"] = $_REQUEST["lastid"];
    $rsEl = CIBlockElement::GetList(["ID" => "ASC"], $arFilter, false, ["nTopCount" => $limit], $arSelect);
    $neo = new CIBlockElement();
    while ($arEl = $rsEl->Fetch())
    {
        /**
         * start do something
         */
        $arEl["PREVIEW_PICTURE"] = $_SERVER["DOCUMENT_ROOT"];
        $arEl["PREVIEW_PICTURE"] .= $arEl["PROPERTY_ANYTOS_DETAIL_PICTURE_VALUE"];
        $arEl["PREVIEW_PICTURE"] = CFile::MakeFileArray($arEl["PREVIEW_PICTURE"]);
        $neo->Update($arEl["ID"], ["PREVIEW_PICTURE" => $arEl["PREVIEW_PICTURE"], "SORT" => 10]);

        /**
         * end do something
         */
        $lastID = intval($arEl["ID"]);
    }

    unset($arFilter[">ID"]);
    $arFilter["<=ID"] = $lastID;

    $rsLeftBorder = CIBlockElement::GetList(["ID" => "ASC"], $arFilter, false, false, ["ID"]);
    $leftBorderCnt = $rsLeftBorder->SelectedRowsCount();
    unset($arFilter["<=ID"]);

    $rsAll = CIBlockElement::GetList(["ID" => "ASC"], $arFilter, false, false, ["ID"]);
    $allCnt = $rsAll->SelectedRowsCount();

    //AddMessage2Log($arFilter, "Measure time");

    /**
     * log after do something

    $arFilter = [
    "ID" => $lastID,
    "IBLOCK_ID" => $arFilter["IBLOCK_ID"]
    ];
    $arEl = CIBlockElement::GetList(["ID" => "ASC"], $arFilter, false, false, ["ID", "PREVIEW_PICTURE"]);
    if($arEl = $arEl->Fetch()){
    $arEl["PREVIEW_PICTURE"] = CFile::GetFileArray($arEl["PREVIEW_PICTURE"]);
    AddMessage2Log($arEl);
    }
     */

    $p = round(100*$leftBorderCnt/$allCnt, 2);

    echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","Обрабатываю запись с ID #'.$lastID.'");';

    die();
}

$clean_test_table = '<table id="result_table" cellpadding="0" cellspacing="0" border="0" width="100%" class="internal">'.
    '<tr class="heading">'.
    '<td>Текущее действие</td>'.
    '<td width="1%">&nbsp;</td>'.
    '</tr>'.
    '</table>';

$aTabs = array(array("DIV" => "edit1", "TAB" => "Обработка"));
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle("Обработка элементов инфоблока");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>


    <form method="post" action="<?echo $APPLICATION->GetCurPage()?>" enctype="multipart/form-data" name="post_form" id="post_form">
        <?
        echo bitrix_sessid_post();

        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>
        <tr>
            <td>На данный момент осталось эмигрировать</td>
            <td>
                <?
                echo $elementsCount;
                ?> картинок.
            </td>
        </tr>
        <tr>
            <td>
                <audio id="audio" controls loop>
                    <source src="/bitrix/css/studio7spb.marketplace/sounds/pres.mp3" type="audio/mpeg">
                    Your browser does not support the audio element.
                </audio>
            </td>
            <td>
                <?=BeginNote()?>
                Для того что бы интегрировать данные со старого проекта необходимо произвести миграция всех файлов старого проекта.
                <br>
                После этой миграции раздел в котором сейчас находятся картинки старого сайта можно будет удалить.
                <br>
                Те картинки которые перенесены уже используются в битрикс. Старые картинки - это промежуточное решение, используется системой так же, но без эфективности и возможностей, которые даёт система.
                <?=EndNote()?>
            </td>
        </tr>
        <tr>
            <td colspan="2">

                <input type=button value="Play" id="work_start" onclick="set_start(1)" />
                <input type=button value="Stop" disabled id="work_stop" onclick="bSubmit=false;set_start(0)" />
                <div id="progress" style="display:none;" width="100%">
                    <br />
                    <div id="status"></div>
                    <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td height="10">
                                <div style="border:1px solid #B9CBDF">
                                    <div id="indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
                                </div>
                            </td>
                            <td width=30>&nbsp;<span id="percent">0%</span></td>
                        </tr>
                    </table>
                </div>
                <div id="result" style="padding-top:10px"></div>

            </td>
        </tr>
        <?
        $tabControl->End();
        ?>
    </form>

    <script type="text/javascript">

        var bWorkFinished = false;
        var bSubmit;
        var audio = document.getElementById('audio');

        function set_start(val)
        {
            document.getElementById('work_start').disabled = val ? 'disabled' : '';
            document.getElementById('work_stop').disabled = val ? '' : 'disabled';
            document.getElementById('progress').style.display = val ? 'block' : 'none';

            if (val)
            {
                ShowWaitWindow();
                audio.play();
                document.getElementById('result').innerHTML = '<?=$clean_test_table?>';
                document.getElementById('status').innerHTML = 'Работаю...';

                document.getElementById('percent').innerHTML = '0%';
                document.getElementById('indicator').style.width = '0%';

                CHttpRequest.Action = work_onload;
                CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>');
            }
            else {
                CloseWaitWindow();
                audio.pause();
                audio.currentTime = 0;
            }
        }

        function work_onload(result)
        {
            try
            {
                eval(result);

                iPercent = CurrentStatus[0];
                strNextRequest = CurrentStatus[1];
                strCurrentAction = CurrentStatus[2];

                document.getElementById('percent').innerHTML = iPercent + '%';
                document.getElementById('indicator').style.width = iPercent + '%';

                document.getElementById('status').innerHTML = 'Работаю...';

                if (strCurrentAction != 'null')
                {
                    oTable = document.getElementById('result_table');

                    if(oTable.rows.length > 5)
                    {
                        oTable.innerHTML = '';
                    }

                    oRow = oTable.insertRow(-1);
                    oCell = oRow.insertCell(-1);
                    oCell.innerHTML = strCurrentAction;
                    oCell = oRow.insertCell(-1);
                    oCell.innerHTML = '';
                }

                if (strNextRequest && document.getElementById('work_start').disabled)
                    CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>' + strNextRequest);
                else
                {
                    set_start(0);
                    bWorkFinished = true;
                }

            }
            catch(e)
            {
                CloseWaitWindow();
                document.getElementById('work_start').disabled = '';
                alert('Сбой в получении данных');
            }
        }

    </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>