<?
/**
 * @var CMain $APPLICATION
 */

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Web\Json,
    Bitrix\Catalog\Model\Product,
    Studio7spb\Marketplace\M2\SyncPiTable;

// <editor-fold defaultstate="collapsed" desc=" # Prepare">

/**
 * @param $arParams
 * @param string $title
 * @param int $percent
 * @param string $messageProgress
 * @param string $messageTime
 */
function progressBar($arParams, $title='', $percent=0, $messageProgress="", $messageTime="")
{
    echo '<div class="adm-info-message-wrap adm-info-message-gray">';
    echo '<div class="adm-info-message">';
    echo '<div class="adm-info-message-title" id="sync-title">' . $title . '</div>';
    echo '<div id="sync-message">' . $messageProgress . '</div>';
    echo '<div class="adm-progress-bar-outer" style="width: ' . $arParams["PROGRESS"]["WIDTH"] . 'px;">';
    echo '<div class="adm-progress-bar-inner" id="sync-bar" style="width: ' . $percent . '%">';
    echo '<div class="adm-progress-bar-inner-text" id="sync-percent-1" style="width: ' . $arParams["PROGRESS"]["WIDTH"] . 'px;">' . $percent . '%</div>';
    echo '</div><span id="sync-percent-2">' . $percent . '%</span>';
    echo '</div>';
    echo '<div id="sync-time">' . $messageTime . '</div>';
    echo '</div>';
    echo '</div>';
}

/**
 * @param $PRODUCT_ID
 * @param $PRICE_TYPE_ID
 * @param $PRICE
 * @param string $CURRENCY
 */
function setShopPrice($PRODUCT_ID, $PRICE_TYPE_ID, $PRICE, $CURRENCY="RUB"){

    $arFields = [
        "PRODUCT_ID" => $PRODUCT_ID,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
        "PRICE" => $PRICE,
        "CURRENCY" => $CURRENCY
    ];

    $res = CPrice::GetList(
        [],
        [
            "PRODUCT_ID" => $PRODUCT_ID,
            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
        ]
    );

    if ($arr = $res->Fetch())
    {
        CPrice::Update($arr["ID"], $arFields);
    }
    else
    {
        CPrice::Add($arFields);
    }

}

// <editor-fold defaultstate="Catalog methods">
/**
 * Using https://www.php.net/manual/ru/function.abs.php
 * @param $arFields
 */
function SaleDiscount($arFields)
{
    if($arFields["ID"] > 0){
        CModule::IncludeModule('sale');

        $discounts = [];

        foreach ($arFields["PRICES"] as $code=>$price) {
            $discounts[$code] = abs($arFields["ELEMENT_PRICE"][$code]);
        }

        // Set discont
        if(!empty($discounts)){
            foreach ($discounts as $priceCode=>$discount){
                // colect fields

                if($discount > 0){

                    $discountFields = [
                        "LID" => "s1",
                        "ACTIVE" => "Y",
                        //'XML_ID' => $arFields["ID"] . "_" . $code,
                        'XML_ID' => $arFields["ID"] . "_" . $priceCode,
                        'NAME' => "[" . $arFields["ID"] . "] Price id - " . $priceCode . " Discont - " . $discount . "%",
                        'PRIORITY' => '1',
                        'LAST_DISCOUNT' => 'N',
                        'LAST_LEVEL_DISCOUNT' => 'N',
                        'USER_GROUPS' => [2],
                        'ACTIONS' => [
                            'CLASS_ID' => 'CondGroup',
                            'DATA' => ['All' => 'AND'],
                            'CHILDREN' => [
                                [
                                    'CLASS_ID' => 'ActSaleBsktGrp',
                                    'DATA' => [
                                        'Type' => 'Discount',
                                        'Value' => $discount,
                                        'Unit' => 'Perc', // CurEach
                                        'Max' => 0,
                                        'All' => 'AND',
                                        'True' => 'True'
                                    ],
                                    'CHILDREN' => [
                                        [
                                            'CLASS_ID' => 'CondIBElement',
                                            'DATA' => [
                                                'logic' => 'Equal',
                                                'value' => [$arFields["ID"]]
                                            ]
                                        ],
                                        [
                                            'CLASS_ID' => 'ActSaleSubGrp',
                                            'DATA' => [
                                                'All' => 'AND',
                                                'True' => 'True',
                                            ],
                                            'CHILDREN' => [
                                                [
                                                    'CLASS_ID' => 'CondCatalogPriceType',
                                                    'DATA' => [
                                                        'logic' => 'Equal',
                                                        'value' => [$priceCode]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'CONDITIONS' => ['CLASS_ID' => 'CondGroup', 'DATA' => ['All' => 'AND', 'True' => 'True'], 'CHILDREN' => []]
                    ];

                    // check
                    $discount = CSaleDiscount::GetList(
                        ["ID" => "DESC"],
                        ["XML_ID" => $arFields["ID"] . "_" . $priceCode],
                        false,
                        false,
                        ["ID", "XML_ID"]
                    );
                    if($discount = $discount->Fetch()){
                        $discount = $discount["ID"];
                    }

                    // save
                    if($discount > 0){
                        CSaleDiscount::Update($discount, $discountFields);
                    }else{
                        CSaleDiscount::Add($discountFields);
                    }

                }

            }




        }

    }
}
// </editor-fold>

if (isset($_REQUEST[$arParams["START_IMPORT"]]))
{
    define("NO_AGENT_STATISTIC", true);
    define("NO_KEEP_STATISTIC", true);
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadLanguageFile(__FILE__);
Loader::includeModule("iblock");
Loader::includeModule("catalog");
Loader::registerAutoLoadClasses("studio7spb.marketplace", ["\\Studio7spb\\Marketplace\\M2\\SyncPiTable" => "lib/anytos/syncpitable.php"]);

$arParams = [
    "IBLOCK_ID" => 2,
    "CODE" => "SINC_PI",
    "FILE" => "csv",
    "DELIMITER" => [
        "TITLE" => Loc::getMessage("DELIMITER_TITLE"),
        "NAME" => "delimiter",
        "VALUE" => "SEMICOLON",
        "VALUES" => [
            "SEMICOLON" => Loc::getMessage("DELIMITER_SEMICOLON"),
            "COMMA" => Loc::getMessage("DELIMITER_COMMA"),
            "TAB" => Loc::getMessage("DELIMITER_TAB"),
            "SPACE" => Loc::getMessage("DELIMITER_SPACE"),
        ],
    ],
    "PROGRESS" => [
        "WIDTH" => 500
    ],
    "PAGE" => "/bitrix/admin/studio7spb.m2_discont_import.php",
    "FILE_PATH" => [], //$_SERVER["DOCUMENT_ROOT"] . "/upload/studio7spb/pi.sync/file.csv"
];
$arResult = [
    "STEP" => "",
    "COUNT" => 0,
    "ID" => 0,
    "PERCENT" => 0
];

$request = Application::getInstance()->getContext()->getRequest();

if(empty($request->get("step"))){

}else{
    $arResult["STEP"] = $request->get("step");
}
if($request->get("id") > 0){
    $arResult["ID"] = $request->get("id");
}
if(!empty($request->get($arParams["DELIMITER"]["NAME"]))){
    $arParams["DELIMITER"]["VALUE"] = $request->get($arParams["DELIMITER"]["NAME"]);
}
if($request->get("count") > 0){
    $arResult["COUNT"] = $request->get("count");
}
if($arResult["COUNT"] > 0){
    $arResult["PERCENT"] = $arResult["ID"] * 100 / $arResult["COUNT"];
    $arResult["PERCENT"] = round($arResult["PERCENT"]);
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Шаг загрузки файла">
if($request->isPost())
{

    $arParams["FILE_PATH"] = $request->getFile($arParams["FILE"]);
    $arParams["FILE_PATH"] = $arParams["FILE_PATH"]["tmp_name"];

    if(empty($arParams["FILE_PATH"])){

    }
    else
    {

        # 1. Если загружен файл читаем из него данные
        require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
        $csvFile = new CCSVData('R', true);
        $csvFile->LoadFile($arParams["FILE_PATH"]);

        switch ($arParams["DELIMITER"]["VALUE"]){
            case "SEMICOLON":
                $csvFile->SetDelimiter(';');
                break;
            case "COMMA":
                $csvFile->SetDelimiter(',');
                break;
            case "TAB":
                $csvFile->SetDelimiter('    ');
                break;
            case "SPACE":
                $csvFile->SetDelimiter(' ');
                break;
            default:
                $csvFile->SetDelimiter(';');
        }

        # 2. Создаём временную таблицу
        SyncPiTable::clearTable();

        # Сохраняем данные с файла во временную таблицу
        $i = 0;
        while ($csvData = $csvFile->Fetch()) {
            $i++;

            /**
            0 => '68338', 1 стлб — наш артикул, перед которым на сайте добавляется AS и пробел. Это чтобы не задваивались.
            1 => '186',   2 стлб -остатки на складе, которые отражаются в карте товара и , больше которых корзина не дает по этой цене заказывать
            2 => '248,00', красная,
            3 => '279,00', желтая
            4 => '372,00', зеленая
            */

            SyncPiTable::add([
                "ARTICLE"      => trim(iconv('WINDOWS-1251', 'UTF-8', $csvData[0])),
                "QUANTITY"     => floatval(str_replace(",", ".", $csvData[1])),
                "PRICE_RED"    => floatval(str_replace(",", ".", $csvData[2])),
                "PRICE_YELLOW" => floatval(str_replace(",", ".", $csvData[3])),
                "PRICE_GREEN"  => floatval(str_replace(",", ".", $csvData[4]))
            ]);

        }

        # После этого редирект на первый шаг
        LocalRedirect($APPLICATION->GetCurPageParam("step=filesave&count=" . $i, ["step", "count"]));
    }

    die;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Шаг более или равно нуля если есть размер">

if($arResult["STEP"] !== "filesave"){

    switch ($arResult["STEP"]){
        case "delete":
            $elements = CIBlockElement::GetList(
                [],
                [
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "PROPERTY_ARTICLE" => "AS %",
                    "ACTIVE" => "N"
                ],
                false,
                false,
                ["ID"]
            );
            while ($element = $elements->Fetch()){
                // удалить скидку этого товара
                Loader::includeModule("sale");
                $discount = CSaleDiscount::GetList(
                    [],
                    ["%XML_ID" => $element["ID"] . "_"],
                    false,
                    false,
                    ["ID"]
                );
                if($discount = $discount->Fetch()){
                    CSaleDiscount::Delete($discount["ID"]);
                }
                CIBlockElement::Delete($element["ID"]);
            }

            echo Json::encode([
                "PERCENT" => 100
            ]);
            die;
            break;
        case "deactivate":

            $elements = CIBlockElement::GetList(
                [],
                [
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "PROPERTY_ARTICLE" => "AS %",
                    "ACTIVE" => "Y"
                ],
                false,
                false,
                ["ID"]
            );
            $neo = new CIBlockElement();
            while ($element = $elements->Fetch()){
                $neo->Update($element["ID"], ["ACTIVE" => "N"]);
            }

            echo Json::encode([
                "PERCENT" => 50
            ]);
            die;
            break;
        case "set":
            if($arResult["ID"] >= 0 && $arResult["COUNT"] > 0){

                $article = SyncPiTable::getList([
                    "order" => ["ID" => "ASC"],
                    "filter" => [
                        ">ID" => $arResult["ID"]
                    ],
                    "limit" => 1
                ]);
                if($article = $article->fetch())
                {
                    # ищем по артиклу с AS
                    $element = CIBlockElement::GetList(
                        [],
                        [
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "PROPERTY_ARTICLE" => "AS " . $article["ARTICLE"]
                        ],
                        false,
                        false,
                        ["ID"]
                    );
                    if($element = $element->Fetch()){
                        # Update UF
                        $neo = new CIBlockElement();
                        $neo->Update($element["ID"], ["ACTIVE" => "Y"]);
                        $element = CCatalogProduct::GetByIDEx($element["ID"]);

                        # Add catalog position
                        $existProduct = Product::getCacheItem($element["ID"],true);

                        if(empty($existProduct)){
                            Product::add([
                                "ID" => $element["ID"],
                                "QUANTITY" => $article["QUANTITY"],
                                "VAT_INCLUDED" => "Y",
                                "QUANTITY_TRACE" => "Y",
                                "CAN_BUY_ZERO" => "N"
                                //"AVAILABLE" => $arCatalog["AVAILABLE"]
                                //"TYPE" => 4,
                                // "WEIGHT" => $element["DATA"]["W"],
                                //"VAT_INCLUDED" => "Y",
                                //"VAT_ID" => ($element["DATA"]["V"] == 10 ? 1 : 2)
                            ]);
                        }else{

                            Product::update($element["ID"], [
                                "QUANTITY" => $article["QUANTITY"],
                                "VAT_INCLUDED" => "Y",
                                "QUANTITY_TRACE" => "Y",
                                "CAN_BUY_ZERO" => "N"
                                //"AVAILABLE" => $arCatalog["AVAILABLE"]
                                //"VAT_ID" => ($element["DATA"]["V"] == 10 ? 1 : 2)
                                //"WEIGHT" => $element["DATA"]["W"],
                            ]);
                        }


                        # Add price disconts

                        $element["ELEMENT_PRICE"] = [
                            4 => $article['PRICE_RED'],
                            6 => $article['PRICE_YELLOW'],
                            8 => $article['PRICE_GREEN'],
                        ];
                        SaleDiscount($element);

                    }
                    else{
                        # Add UF
                        # ищем по артиклу без AS
                        $element = CIBlockElement::GetList(
                            [],
                            [
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "PROPERTY_ARTICLE" => $article["ARTICLE"],
                                "ACTIVE" => "Y",
                            ],
                            false,
                            false,
                            ["ID"]
                        );

                        if($element = $element->Fetch()){
                            # На основе этого товра создаём копию с артиклом UF
                            $element = CCatalogProduct::GetByIDEx($element["ID"]);
                            $neo = new CIBlockElement();
                            $arFields = [
                                "IBLOCK_ID" => $element["IBLOCK_ID"],
                                "IBLOCK_SECTION_ID" => $element["IBLOCK_SECTION_ID"],
                                "NAME" => $element["NAME"],
                                "ACTIVE" => "Y",
                                "DETAIL_TEXT" => $element["DETAIL_TEXT"],
                                "DETAIL_TEXT_TYPE" => $element["DETAIL_TEXT_TYPE"],
                                "DETAIL_PICTURE" => $element["DETAIL_PICTURE"],
                            ];
                            if($arFields["DETAIL_PICTURE"] > 1)
                                $arFields["DETAIL_PICTURE"] = CFile::MakeFileArray($element["PREVIEW_PICTURE"]);
                            elseif($arFields["DETAIL_PICTURE"] < 1)
                                $arFields["DETAIL_PICTURE"] = CFile::MakeFileArray($element["PREVIEW_PICTURE"]);
                            if(empty($arFields["DETAIL_PICTURE"]))
                                $arFields["DETAIL_PICTURE"] = CFile::MakeFileArray($element["PROPERTIES"]["ANYTOS_DETAIL_PICTURE"]["VALUE"]);

                            unset($element["PROPERTIES"]["ANYTOS_DETAIL_PICTURE"]);

                            $arFields["PROPERTY_VALUES"] = [];

                            foreach ($element["PROPERTIES"] as $code=>$property){
                                if(!empty($property["VALUE"]))
                                {
                                    switch ($code){
                                        case "ARTICLE":
                                            $property["VALUE"] = "AS " . $property["VALUE"];
                                            break;
                                        case "DISCOUNT":
                                            $property["VALUE"] = null; // 14
                                            break;
                                        case "MOQ":
                                        case "MULTIPLICITY":
                                            $property["VALUE"] = 1;
                                            break;
                                    }
                                    $arFields["PROPERTY_VALUES"][$code] = $property["VALUE"];
                                }
                            }

                            $el = new CIBlockElement;
                            if($PRODUCT_ID = $el->Add($arFields)){
                                # Add catalog position
                                $existProduct = Product::getCacheItem($lastID,true);

                                if(empty($existProduct)){
                                    Product::add([
                                        "ID" => $PRODUCT_ID,
                                        "QUANTITY" => $article["QUANTITY"],
                                        "VAT_INCLUDED" => "Y",
                                        "QUANTITY_TRACE" => "Y",
                                        "CAN_BUY_ZERO" => "N"
                                        //"AVAILABLE" => $arCatalog["AVAILABLE"]
                                        //"TYPE" => 4,
                                        // "WEIGHT" => $element["DATA"]["W"],
                                        //"VAT_INCLUDED" => "Y",
                                        //"VAT_ID" => ($element["DATA"]["V"] == 10 ? 1 : 2)
                                    ]);
                                }else{

                                    Product::update($PRODUCT_ID, [
                                        "QUANTITY" => $article["QUANTITY"],
                                        "VAT_INCLUDED" => "Y",
                                        "QUANTITY_TRACE" => "Y",
                                        "CAN_BUY_ZERO" => "N"
                                        //"AVAILABLE" => $arCatalog["AVAILABLE"]
                                        //"VAT_ID" => ($element["DATA"]["V"] == 10 ? 1 : 2)
                                        //"WEIGHT" => $element["DATA"]["W"],
                                    ]);
                                }

                                # Add price
                                foreach ($element["PRICES"] as $PRICE_TYPE_ID=>$property) {
                                    setShopPrice($PRODUCT_ID, $PRICE_TYPE_ID, $property["PRICE"], $property["CURRENCY"]);
                                }

                                # Add discont
                                $element["ID"] = $PRODUCT_ID;
                                $element["ELEMENT_PRICE"] = [
                                    4 => $article['PRICE_RED'],
                                    6 => $article['PRICE_YELLOW'],
                                    8 => $article['PRICE_GREEN'],
                                ];
                                SaleDiscount($element);
                            }

                            // DISCOUNT
                            CIBlockElement::SetPropertyValues(
                                $PRODUCT_ID,
                                $element["IBLOCK_ID"],
                                [12, 14],
                                "DISCOUNT");

                        }
                    }

                    echo Json::encode([
                        "ID" => $article["ID"],
                        "ARTICLE" => Loc::getMessage("TITLE_ARTICLE_UPDATE", ["ARTICLE" => $article["ARTICLE"]]),
                        "PERCENT" => $arResult["PERCENT"]
                    ]);
                }
                die;
            }
            die;
            break;
    }


}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Admin and tabs controls">

$tabControl = new CAdminTabControl("tabControl", [
    [
        "DIV" => "SINC_PI",
        "TAB" => Loc::getMessage("TITLE_TAB"),
        "ICON"=>"main_user_edit",
        "TITLE" => Loc::getMessage("TITLE_TAB")
    ]
]);

$APPLICATION->SetTitle(Loc::getMessage("TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
CJSCore::Init("ajax");

// </editor-fold>

echo '<form method="post" ';
echo 'action="' . $APPLICATION->GetCurPage() . '" ';
echo 'enctype="multipart/form-data" ';
echo 'onsubmit="ShowWaitWindow()" ';
echo 'name="sinc_pi_form">';
echo bitrix_sessid_post();

$tabControl->Begin();

// <editor-fold defaultstate="collapsed" desc=" # Tab: SINC_PI">
$tabControl->BeginNextTab();
?>
<td width="40%" class="adm-detail-content-cell-l">
    <b><?=$arParams["DELIMITER"]["TITLE"]?>:</b>
</td>
<td class="adm-detail-content-cell-r">
    <?foreach ($arParams["DELIMITER"]["VALUES"] as $code=>$value):?>
        <input type="radio"
               <?if($arParams["DELIMITER"]["VALUE"] == $code) echo " checked ";?>
               name="<?=$arParams["DELIMITER"]["NAME"]?>"
               id="<?=$arParams["DELIMITER"]["NAME"]?>-<?=$code?>" value="<?=$code?>" />
        <label for="<?=$arParams["DELIMITER"]["NAME"]?>-<?=$code?>"><?=$value?></label>
        <br />
    <?endforeach;?>
</td>
<tr id="discont-progress-file">
    <td width="40%" class="adm-detail-content-cell-l">
        <b><?=Loc::getMessage("TITLE_FILE")?>:</b>
        <br>
        <i><?=Loc::getMessage("TITLE_FILE_INFO")?></i>
    </td>
    <td class="adm-detail-content-cell-r">
        <?
        echo \CFileInput::Show(
            $arParams["FILE"], // name
            "", // value
            [
                "IMAGE" => "N",
                "PATH" => "Y",
                "FILE_SIZE" => "Y",
                "DIMENSIONS" => "N"
            ],
            [
                'upload' => true,
                'medialib' => false,
                'file_dialog' => true,
                'cloud' => true,
                'email' => true,
                'linkauth' => true,
                'del' => false,
                'description' => false
            ]
        );
        ?>
    </td>
</tr>
<tr>
    <td id="discont-progress" colspan="2">
        <?
        //if($request->get("step") === "filesave"){
        if(!empty($arResult["STEP"])){
            # calculate
            $arResult["STEP"] = "import";
            $arResult["COUNT"] = $request->get("count");
            $arResult["PERCENT"] = 1;

            # print progress
            progressBar(
                $arParams,
                Loc::getMessage("STEP_FILE_SAVE"),
                $arResult["PERCENT"],
                Loc::getMessage("STEP_FILE_PREPARE", ["COUNT" => $arResult["COUNT"]])
            );
        }
        ?>
    </td>
</tr>
<tr>
    <td id="discont-progress-info" colspan="2">

    </td>
</tr>
<?
//$tabControl->Buttons(array());
$tabControl->Buttons([
    //"disabled" => !$editable,
	"btnSave" => true,
	"btnApply" => false,
    "btnCancel" => false,
    "btnSaveAndAdd" => false
]);
$tabControl->End();
// </editor-fold>
echo '</form>';

#require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>

<script>
    var discontImport = {
        url: '<?=$arParams["PAGE"]?>',
        block: 'discont-progress',
        blockInfo: 'discont-progress-info',
        params: {
            title: null,
            step: "import",
            id: 0,
            count: <?=$arResult["COUNT"]?>
        },

        send: function (title, id, step) {
            ShowWaitWindow();

            this.params.title = title;
            this.params.id = id;
            this.params.step = step;

            BX.ajax.loadJSON(
                this.url,
                this.params,
                BX.delegate(this.progress, this)
            );

            //BX.remove(BX("tabControl_buttons_div"));
            //BX.remove(BX("discont-progress-file"));
            BX.hide(BX("tabControl_buttons_div"));
            BX.hide(BX("discont-progress-file"));

        },

        progress: function (result) {
            //BX(discontImport.block).innerHTML = result;

            if(this.params.count > 0){

                switch (this.params.step) {
                    case "import":
                        discontImport.send("", 0, "deactivate");
                        BX("sync-title").innerHTML = "<?=Loc::getMessage("TITLE_ARTICLE_START")?>";
                        BX("sync-time").innerHTML = "<?=Loc::getMessage("STEP_TIME_IMPORT")?>";
                        break;
                    case "deactivate":
                        discontImport.send("", 0, "set");
                        BX("sync-title").innerHTML = "<?=Loc::getMessage("TITLE_ARTICLE_DEACTIVATE")?>";
                        BX("sync-time").innerHTML = "<?=Loc::getMessage("STEP_TIME_DEACTIVATE")?>";
                        break;
                    case "set":
                        if(this.params.count >  result.ID){
                            discontImport.send(result.ARTICLE, result.ID, "set");
                            BX("sync-title").innerHTML = result.ARTICLE;
                            BX("sync-message").innerHTML = result.ARTICLE;
                            BX("sync-time").innerHTML = "<?=Loc::getMessage("STEP_TIME_ARTICLE")?>" + result.ARTICLE;
                        }else{
                            discontImport.send("", 0, "delete");
                            BX("sync-title").innerHTML = "<?=Loc::getMessage("TITLE_ARTICLE_DELETE_MESSAGE")?>";
                            BX("sync-message").innerHTML = "<?=Loc::getMessage("TITLE_ARTICLE_DELETE")?>";
                            BX("sync-time").innerHTML = "<?=Loc::getMessage("STEP_TIME_SUCCESS")?>";
                            BX.show(BX("tabControl_buttons_div"));
                            BX.show(BX("discont-progress-file"));
                        }
                        break;
                }

                BX("sync-bar").style.width = result.PERCENT + "%";
                BX("sync-percent-1").innerHTML = result.PERCENT + " %";
                BX("sync-percent-2").innerHTML = result.PERCENT + " %";

            }

            CloseWaitWindow();
        }

    };
    <?
    if($arResult["COUNT"] > 0){
        switch ($arResult["STEP"]){
            case "import":
                ?>
                    discontImport.send("", 0, "deactivate");
                <?
                break;
            case "deactivate":
                // discontImport.params.step = "deactivate";
                break;
            case "delete":
                // discontImport.params.step = "delete";
                break;
        }
    }
    ?>

</script>
