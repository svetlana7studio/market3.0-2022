<?
use Bitrix\Main\Loader,
    Studio7spb\Marketplace\ImportSettingsTable,
    Studio7spb\Marketplace\VendorTable,
    Bitrix\Main\Localization\Loc;


if (isset($_REQUEST['work_start']))
{
    define("NO_AGENT_STATISTIC", true);
    define("NO_KEEP_STATISTIC", true);
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadLanguageFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm("Доступ запрещен");

$arParams = [
    "LIMIT" => 1
];

// <editor-fold defaultstate="collapsed" desc=" # Load orm">

// get import settings
Loader::includeModule("studio7spb.marketplace");
Loader::includeModule("iblock");
$importSettings = ImportSettingsTable::getList(array(
    "filter" => array("%CODE" => "IMPORT_")
));
while($importSetting = $importSettings->fetch()){
    $arResult["SETTINGS"][$importSetting["CODE"]] = $importSetting;
}

// </editor-fold>

if($_REQUEST['work_start'] && check_bitrix_sessid())
{
    $neo = new CIBlockElement();
    $lastID = intval($_REQUEST["lastid"]);
    $table = "b_iblock_element_prop_s2";
    $notExist = [];
    $rs = $DB->Query("SELECT IBLOCK_ELEMENT_ID, PROPERTY_334 FROM " . $table . " FORCE INDEX (anytos_import_v) WHERE IBLOCK_ELEMENT_ID>" . $lastID . " AND PROPERTY_388=" . $arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"]["VALUE"] . " order by IBLOCK_ELEMENT_ID asc limit " . $arParams["LIMIT"]);
    while ($ar = $rs->Fetch())
    {
        #$exsist = "select * from b_studio7spb_import_price where DATA like '%" . '"' . $ar["PROPERTY_334"] . '"' . "%'";
        $exsist = "select * from b_studio7spb_import_price where ARTICLE='" . $ar["PROPERTY_334"] . "'";

        $exsist = $DB->Query($exsist);
        if($exsist = $exsist->Fetch())
        {
            // is exist in file
            $neo->Update($ar["IBLOCK_ELEMENT_ID"], ["ACTIVE" => "Y"]);
        }else{
            // is not exist in file
            $notExist[] = $ar["IBLOCK_ELEMENT_ID"];
            $neo->Update($ar["IBLOCK_ELEMENT_ID"], ["ACTIVE" => "N"]);
        }
        /*
         * do something
         */
        $lastID = intval($ar["IBLOCK_ELEMENT_ID"]);
    }


    $rsLeftBorder = $DB->Query("SELECT IBLOCK_ELEMENT_ID FROM " . $table . " FORCE INDEX (anytos_import_v) WHERE IBLOCK_ELEMENT_ID <= " . $lastID . " AND PROPERTY_388=" . $arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"]["VALUE"] . " order by IBLOCK_ELEMENT_ID asc", false, "FILE: ".__FILE__."<br /> LINE: ".__LINE__);
    $leftBorderCnt = $rsLeftBorder->SelectedRowsCount();

    $rsAll = $DB->Query("SELECT IBLOCK_ELEMENT_ID FROM " . $table . " FORCE INDEX (anytos_import_v) WHERE PROPERTY_388=" . $arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"]["VALUE"], false, "FILE: ".__FILE__."<br /> LINE: ".__LINE__);
    $allCnt = $rsAll->SelectedRowsCount();

    $p = round(100*$leftBorderCnt/$allCnt, 2);

    if(empty($notExist)){
        echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","'.Loc::getMessage("TRANSFER_CATALOG_ELEMENT_CHECKED", ["ID" => $lastID]).'");';
    }else{
        echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","'.Loc::getMessage("TRANSFER_CATALOG_ELEMENT_DEACTIVATE", ["ID" => $lastID]).'");';
    }

    die();
}

$clean_test_table = '<table id="result_table" cellpadding="0" cellspacing="0" border="0" width="100%" class="internal">'.
    '<tr class="heading">'.
    '<td>Текущее действие</td>'.
    '<td width="1%">&nbsp;</td>'.
    '</tr>'.
    '</table>';

$aTabs = array(array("DIV" => "edit1", "TAB" => Loc::getMessage("TRANSFER_TITLE")));
CModule::AddAutoloadClasses(
    "studio7spb.marketplace",
    array(
        "\\Studio7spb\\Marketplace\\VendorTable" => "lib/anytos/vendor.php"
    )
);
$vendor = VendorTable::getList([
    "filter" => [
        "ID" => $arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"]["VALUE"]
    ]
]);
if($vendor = $vendor->fetch()){
    $aTabs = array(array("DIV" => "edit1", "TAB" => Loc::getMessage("TRANSFER_TITLE_VENDOR", ["VENDOR" => $vendor["title"]])));
}
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle(Loc::getMessage("TRANSFER_TAB1"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
    <script type="text/javascript">

        var bWorkFinished = false;
        var bSubmit;

        function set_start(val)
        {
            document.getElementById('work_start').disabled = val ? 'disabled' : '';
            document.getElementById('work_stop').disabled = val ? '' : 'disabled';
            document.getElementById('progress').style.display = val ? 'block' : 'none';

            if (val)
            {
                ShowWaitWindow();
                document.getElementById('result').innerHTML = '<?=$clean_test_table?>';
                document.getElementById('status').innerHTML = 'Работаю...';

                document.getElementById('percent').innerHTML = '0%';
                document.getElementById('indicator').style.width = '0%';

                CHttpRequest.Action = work_onload;
                CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>');
            }
            else
                CloseWaitWindow();
        }

        function work_onload(result)
        {
            try
            {
                eval(result);

                iPercent = CurrentStatus[0];
                strNextRequest = CurrentStatus[1];
                strCurrentAction = CurrentStatus[2];

                document.getElementById('percent').innerHTML = iPercent + '%';
                document.getElementById('indicator').style.width = iPercent + '%';

                document.getElementById('status').innerHTML = 'Работаю...';

                if (strCurrentAction != 'null')
                {
                    oTable = document.getElementById('result_table');
                    oRow = oTable.insertRow(-1);
                    oCell = oRow.insertCell(-1);
                    oCell.innerHTML = strCurrentAction;
                    oCell = oRow.insertCell(-1);
                    oCell.innerHTML = '';

                    var totalRowCount = oTable.rows.length; // 5
                    console.info(totalRowCount);


                    if(totalRowCount > 5){
                        oTable.deleteRow(0);
                    }
                }

                if (strNextRequest && document.getElementById('work_start').disabled)
                    CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>' + strNextRequest);
                else
                {
                    set_start(0);
                    bWorkFinished = true;
                }

            }
            catch(e)
            {
                CloseWaitWindow();
                document.getElementById('work_start').disabled = '';
                alert('Сбой в получении данных');
            }
        }

    </script>

    <form method="post" action="<?echo $APPLICATION->GetCurPage()?>" enctype="multipart/form-data" name="post_form" id="post_form">
        <?
        echo bitrix_sessid_post();

        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>
        <tr>
            <td colspan="2">

                <input type=button value="Старт" id="work_start" onclick="set_start(1)" />
                <input type=button value="Стоп" disabled id="work_stop" onclick="bSubmit=false;set_start(0)" />
                <div id="progress" style="display:none;" width="100%">
                    <br />
                    <div id="status"></div>
                    <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td height="10">
                                <div style="border:1px solid #B9CBDF">
                                    <div id="indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
                                </div>
                            </td>
                            <td width=30>&nbsp;<span id="percent">0%</span></td>
                        </tr>
                    </table>
                </div>
                <div id="result" style="padding-top:10px"></div>

            </td>
        </tr>
        <?
        $tabControl->End();
        ?>
    </form>


<?
echo BeginNote();
echo Loc::getMessage("TRANSFER_VENDOR_NOTE1");
echo "<br>";
echo Loc::getMessage("TRANSFER_VENDOR_NOTE2");
echo "<br>";
echo Loc::getMessage("TRANSFER_VENDOR_NOTE5", ["VENDOR" => $vendor["title"]]);
echo "<br>";
?>
<!--
    <span class="adm-lamp adm-lamp adm-lamp-green"></span> <a href="#"><?=Loc::getMessage("TRANSFER_VENDOR_NOTE3", ["VENDOR" => $vendor["title"]])?></a><br>
    <span class="adm-lamp adm-lamp adm-lamp-yellow"></span> <a href="#"><?=Loc::getMessage("TRANSFER_VENDOR_NOTE4", ["VENDOR" => $vendor["title"]])?></a>
    -->
<div style="text-align: center; padding: 20px 0;">
    <img src="/system/7studio/images/m2.import.png">
</div>
<?
echo EndNote();
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>