<?
/**
 * Povered by artem@koorochka.com
 */

use Studio7spb\Marketplace\ImportSettingsTable,
    Studio7spb\Marketplace\ImportPriceTable,
    Bitrix\Main\Web\Json;

$_SERVER["DOCUMENT_ROOT"] = $argv[1];
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
require $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// <editor-fold defaultstate="collapsed" desc=" # Clear b_studio7spb_import_price">
$DB->Query("DROP INDEX IF EXISTS art ON b_studio7spb_import_price;");
$DB->Query("DROP TABLE IF EXISTS b_studio7spb_import_price;");
//$DB->Query("DROP TABLE IF EXISTS b_studio7spb_import_price_log;");
$DB->Query("CREATE TABLE IF NOT EXISTS b_studio7spb_import_price
(
    ID INT NOT NULL AUTO_INCREMENT,
    STATUS char(1) not null default 'Y',
    ARTICLE VARCHAR(255),
    DATA TEXT,
    NOTE TEXT,
    PRIMARY KEY (ID)
);");
$DB->Query("CREATE INDEX art ON b_studio7spb_import_price(ARTICLE);");

/*
$DB->Query("CREATE TABLE IF NOT EXISTS b_studio7spb_import_price_log
(
    ID INT NOT NULL AUTO_INCREMENT,
    ELEMENT_ID INT,
    NOTE varchar(255),
    PRIMARY KEY (ID)
);");
*/
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Init arParams arResult">
$arParams = [
    "MODULE" => "studio7spb.marketplace",
    "FILE" => "IMPORT_DATA_FILE_PRICE",
    "FILE_NAME" => null,
    "FILE_PATH" => null,
    "CATALOG_START" => 1
];
$arResult = [];
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Load orm">
CModule::AddAutoloadClasses(
    $arParams["MODULE"],
    [
        "\\Studio7spb\\Marketplace\\ImportSettingsTable" => "lib/importsettings.php",
    ]
);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Load Settings">
$importSettings = ImportSettingsTable::getList(array(
    "filter" => array("CODE" => $arParams["FILE"])
));
while($importSetting = $importSettings->fetch()){
    //IMPORT_DATA_CATALOG
    switch ($importSetting["CODE"]){
        case $arParams["FILE"]:
            $arParams["FILE_NAME"] = $importSetting["VALUE"];
            break;
    }
}
unset($importSettings, $importSetting);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Load Excel">
if(empty($arParams["FILE_NAME"])){
    $arParams["FILE_PATH"] = null;
}else{
    $arParams["FILE_PATH"] = $_SERVER["DOCUMENT_ROOT"] . "/upload/excel/import.price.xls";
}

/**  Identify the type of $inputFileName  **/
$inputFileType = PHPExcel_IOFactory::identify($arParams["FILE_PATH"]);
if(!in_array($inputFileType, ['Excel2007', 'Excel5'])){
    echo Json::encode([
        "TYPE" => "ERROR"
    ]);
    die;
}

$objReader = PHPExcel_IOFactory::createReaderForFile($arParams["FILE_PATH"]);
$objPHPExcel = $objReader->load($arParams["FILE_PATH"]);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Iterate workshit">
$key = 0;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    $key++;
    // get shett data
    $rows = [];

    // get rows limit for workshit
    $maxCell = $worksheet->getHighestRowAndColumn();
    $data = $worksheet->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row']);
    $data = array_map('array_filter', $data);
    $rowsLimit = array_filter($data);
    $rowsLimit = count($rowsLimit);

    if($key === 1){
        foreach ($worksheet->getRowIterator() as $row) {
            if($rowsLimit >= $row->getRowIndex()) {
                if ($row->getRowIndex() >= $arParams["CATALOG_START"]) {
                    $i++;
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
                    $j = 0;
                    foreach ($cellIterator as $cell) {
                        $j++;
                        if (!is_null($cell) && $j < 6) {
                            $rows[$row->getRowIndex()][$cell->getColumn()] = $cell->getValue();
                        }
                    }
                }
            }
        }

        // add to db
        foreach ($rows as $row){
            ImportPriceTable::add(array(
                "ARTICLE" => $row["A"],
                "DATA" => serialize($row)
            ));
        }

    }
}
// </editor-fold>

echo Json::encode([
    "TYPE" => "OK"
]);