<?

use Bitrix\Main\IO\File;
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Studio7spb\Marketplace\ImportSettingsTable,
    Bitrix\Main\Web\Json;
use Studio7spb\Marketplace\VendorTable;


$arParams = [
    "MODULES" => ["iblock", "studio7spb.marketplace"],
    "IBLOCK_ID" => 2,
    "PRICES" => [
        "BUY" => [
            "CODE" => "GRAY",
            "PAGE" => "/bitrix/admin/studio7spb.m2_import_price_buy.php"
        ],
        "GREEN" => [
            "CODE" => "GREEN",
            "PAGE" => "/bitrix/admin/studio7spb.m2_import_price_green.php"
        ],
        "YELLOW" => [
            "CODE" => "YELLOW",
            "PAGE" => "/bitrix/admin/studio7spb.m2_import_price_yellow.php"
        ],
        "RED" => [
            "CODE" => "RED",
            "PAGE" => "/bitrix/admin/studio7spb.m2_import_price_red.php"
        ],
    ],
    "FILE_NAME" => "import",
    "VENDOR_NAME" => "vendor",
];



require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
if(!empty($arParams["MODULES"])){
    foreach ($arParams["MODULES"] as $value) {
        Loader::includeModule($value);
    }
}

Loc::loadLanguageFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

$request = Application::getInstance()->getContext()->getRequest();

$arResult = [
    "FILE" => [],
    "SETTINGS" => [],
    "DB" => []
];

// <editor-fold defaultstate="collapsed" desc=" # Get import settings">

// get import settings
$importSettings = ImportSettingsTable::getList(array(
    "filter" => array("%CODE" => "IMPORT_")
));
while($importSetting = $importSettings->fetch()){
    $arResult["SETTINGS"][$importSetting["CODE"]] = $importSetting;
}

// </editor-fold>

if($request->isPost()){
    $arResult["FILE"] = $request->getFile($arParams["FILE_NAME"]);

    if(empty($arResult["FILE"]["tmp_name"])) {

    }else{
        ImportSettingsTable::update($arResult["SETTINGS"]["IMPORT_DATA_FILE_PRICE"]["ID"], array("VALUE" => $arResult["FILE"]["name"]));
        $file = $arResult["FILE"]["tmp_name"];
        $file = Bitrix\Main\IO\File::getFileContents($file);
        File::putFileContents($_SERVER["DOCUMENT_ROOT"] . "/upload/excel/import.price.xls", $file);
        LocalRedirect($APPLICATION->GetCurPageParam("file=upload", array("file")));
    }

    $vendor = $request->get($arParams["VENDOR_NAME"]);
    $vendor = intval($vendor);
    if($vendor > 0){
        $arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"]["VALUE"] = $vendor;
        ImportSettingsTable::update($arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"]["ID"], array("VALUE" => $vendor));
    }


}

$aTabs = array(array("DIV" => "edit1", "TAB" => Loc::getMessage("IMPORT_PRICE_SETTINGS_TITLE")));
$tabControl = new CAdminTabControl("tabControl", $aTabs);
$APPLICATION->SetTitle(Loc::getMessage("IMPORT_PRICE_SETTINGS_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");


if($request->get("file") == "upload"){
    echo CAdminMessage::ShowMessage(array(
        "TYPE" => "OK",
        "MESSAGE" => Loc::getMessage("IMPORT_PRICE_SETTINGS_FILE_UPLOAD_UNPAKING_TITLE"),
        "DETAILS" => Loc::getMessage("IMPORT_PRICE_SETTINGS_FILE_UPLOAD_UNPAKING"),
        "HTML" => true,
    ));

    $arResult["DB"] = shell_exec('php ' . $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/studio7spb.marketplace/admin/import.price/db.fill.php ' . $_SERVER["DOCUMENT_ROOT"]);
    $arResult["DB"] = Json::decode($arResult["DB"]);

    if($arResult["DB"]["TYPE"] == "OK"){
        echo CAdminMessage::ShowMessage(array(
            "TYPE" => "OK",
            "MESSAGE" => Loc::getMessage("IMPORT_PRICE_SETTINGS_FILE_UPLOAD_PREPARE_TITLE"),
            "DETAILS" => Loc::getMessage("IMPORT_PRICE_SETTINGS_FILE_UPLOAD_PREPARE"),
            "HTML" => true,
        ));
    }else{
        echo CAdminMessage::ShowMessage(Loc::getMessage("IMPORT_PRICE_DB_DATA_ERROR"));
    }
}


?>

<?
echo BeginNote();

if($arResult["DB"]["TYPE"] == "OK"):
?>
    <span class="adm-lamp adm-lamp-<?=ToLower($arPrice["CODE"])?>"></span> <?=Loc::getMessage("IMPORT_PRICE_INFO_DONE")?><br>
<?
    foreach ($arParams["PRICES"] as $arPrice):
    ?>
        <span class="adm-lamp adm-lamp-<?=ToLower($arPrice["CODE"])?>"></span> <a href="<?=$arPrice["PAGE"]?>"><?=Loc::getMessage("IMPORT_PRICE_INFO_" . $arPrice["CODE"])?></a><br>
    <?
    endforeach;
else:
    ?>
    <span class="adm-lamp adm-lamp-<?=ToLower($arPrice["CODE"])?>"></span> <?=Loc::getMessage("IMPORT_PRICE_INFO")?><br>
<?
    foreach ($arParams["PRICES"] as $arPrice):
        ?>
        <span class="adm-lamp adm-lamp-<?=ToLower($arPrice["CODE"])?>"></span> <?=Loc::getMessage("IMPORT_PRICE_INFO_" . $arPrice["CODE"])?><br>
    <?
    endforeach;
endif;
echo EndNote();
?>



    <form method="post" action="<?echo $APPLICATION->GetCurPage()?>"
          enctype="multipart/form-data"
          name="post_form"
          id="post_form">
        <?
        echo bitrix_sessid_post();

        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>
        <tr>
            <td width="40%" class="adm-detail-content-cell-l">
                <b><?=Loc::getMessage("IMPORT_PRICE_SETTINGS_FILE")?>: </b>
                <br>
                <a href="/upload/excel/sample/m2_import_price.xls?v=2">Пример загружаемого на обновление файла (файл формата Excel)</a>
            </td>
            <td class="adm-detail-content-cell-r">
                <?
                echo \CFileInput::Show(
                    $arParams["FILE_NAME"], // name
                    $arResult["SETTINGS"]["IMPORT_DATA_FILE_PRICE"]["VALUE"], // value
                    array(
                        "IMAGE" => "N",
                        "PATH" => "Y",
                        "FILE_SIZE" => "Y",
                        "DIMENSIONS" => "N"
                    ),
                    array(
                        'upload' => true,
                        'medialib' => false,
                        'file_dialog' => true,
                        'cloud' => true,
                        'email' => true,
                        'linkauth' => true,
                        'del' => false,
                        'description' => false
                    )
                );
                ?>
            </td>
        </tr>
        <tr>
            <td class="adm-detail-content-cell-l">
                Выбирать поставщика;
            </td>
            <td class="adm-detail-content-cell-r">
                <select name="<?=$arParams["VENDOR_NAME"]?>">
                    <option value="0">Выбирать из списка</option>
                    <?
                    CModule::AddAutoloadClasses(
                        "studio7spb.marketplace",
                        array(
                            "\\Studio7spb\\Marketplace\\VendorTable" => "lib/anytos/vendor.php"
                        )
                    );
                    $vendors = VendorTable::getList();
                    while ($vendor = $vendors->fetch()):
                        ?>
                        <option<?
                        if($arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"]["VALUE"] == $vendor["id"])
                            echo ' selected="selected" ';
                        ?> value="<?=$vendor["id"]?>">[<?=$vendor["id"]?>] <?=$vendor["title"]?></option>
                    <?
                    endwhile;
                    unset($vendor, $vendors);
                    ?>
                </select>
            </td>
        </tr>
        <?
        // завершение формы - вывод кнопок сохранения изменений
        $tabControl->Buttons([
            "disabled"=>($POST_RIGHT<"W"),
        ]);

        $tabControl->End();
        ?>
    </form>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>