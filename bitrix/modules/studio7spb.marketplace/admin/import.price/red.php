<?

use Bitrix\Main\Localization\Loc,
    Studio7spb\Marketplace\ImportSettingsTable,
    Studio7spb\Marketplace\ImportPriceTable,
    Bitrix\Main\Loader;
use Studio7spb\Marketplace\VendorTable;

if (isset($_REQUEST['work_start']))
{
    define("NO_AGENT_STATISTIC", true);
    define("NO_KEEP_STATISTIC", true);
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadLanguageFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("main");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::getMessage("TRANSFER_ACCESS_DENIED"));

$arParams = [
    "MODULES" => ["iblock", "catalog", "studio7spb.marketplace"],
    "IBLOCK_ID" => 2,
    "LIMIT" => 1
];

// <editor-fold defaultstate="collapsed" desc=" # Load orm">
foreach ($arParams["MODULES"] as $module){
    Loader::includeModule($module);
}
unset($module);
// </editor-fold>

// get import settings
Loader::includeModule("studio7spb.marketplace");
$importSettings = ImportSettingsTable::getList(array(
    "filter" => array("%CODE" => "IMPORT_")
));
while($importSetting = $importSettings->fetch()){
    $arResult["SETTINGS"][$importSetting["CODE"]] = $importSetting;
}

// </editor-fold>

if($_REQUEST['work_start'] && check_bitrix_sessid())
{

    $elements = ImportPriceTable::getList(array(
        "order" => array("ID" => "ASC"),
        "filter" => array(
            ">ID" => $_REQUEST["lastid"]
        ),
        "limit" => $arParams["LIMIT"]
    ));

    $arProducts = [];
    while ($element = $elements->fetch())
    {
        $element["DATA"] = unserialize($element["DATA"]);

        /**
        'A' => 20544.0,            // VENDOR_ARTICLE
        'B' => 1.18,               // buy price
        'C' => 1.3923999999999999, // red price
        'D' => 1.503792,           // yellow price
        'E' => 1.7293607999999998, // green price
        'F' =>                     // ANYTOS_VENDOR
         */
        /*
        $products = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "PROPERTY_ANYTOS_VENDOR" => $arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"],
                "PROPERTY_VENDOR_ARTICLE" => $element["DATA"]["A"]
            ],
            false,
            false,
            [
                "ID"
            ]
        );
        */

        // Оновить или добавить и проверить на дули, если есть дубли - то первый обновляется а остальные удаляются
        $product = "select IBLOCK_ELEMENT_ID FROM b_iblock_element_prop_s2 WHERE PROPERTY_334='" . $element["DATA"]["A"] . "' and PROPERTY_388=" . $arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"]["VALUE"];
        $product = $DB->Query($product);
        if($product = $product->Fetch())
        {
            $product["ID"] = $product["IBLOCK_ELEMENT_ID"];
            $arProducts[] = $product["ID"];
            $PRICE_TYPE_ID = 8;
            $arFields = Array(
                "PRODUCT_ID" => $product["ID"],
                "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                "PRICE" => $element["DATA"]["С"],
                "CURRENCY" => "RUB"
            );
            $res = CPrice::GetList(
                array(),
                array(
                    "PRODUCT_ID" => $product["ID"],
                    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
                )
            );
            if ($arr = $res->Fetch())
            {
                CPrice::Update($arr["ID"], $arFields);
            }
            else
            {
                CPrice::Add($arFields);
            }
        }else{
            ImportPriceTable::update($element["ID"], ["NOTE" => Loc::getMessage("TRANSFER_PRODUCT_NOT_FOUND")]);
        }

        // GOTO nexst step
        $lastID = $element["ID"];
    }

    $rsLeftBorder = ImportPriceTable::getList(array(
        "order" => array("ID" => "ASC"),
        "filter" => array(
            "<=ID" => $lastID
        ),
        "select" => array(
            "ID"
        )
    ));
    $leftBorderCnt = $rsLeftBorder->getSelectedRowsCount();

    $rsAll = ImportPriceTable::getList(array(
        "order" => array("ID" => "ASC"),
        "select" => array(
            "ID"
        )
    ));
    $allCnt = $rsAll->getSelectedRowsCount();

    $p = round(100*$leftBorderCnt/$allCnt, 2);

    if(empty($arProducts)){
        echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("TRANSFER_CATALOG_ELEMENT_NON_UPDATE", ["ID" => $lastID]) . '");';
    }else{
        echo 'CurrentStatus = Array('.$p.',"'.($p < 100 ? '&lastid='.$lastID : '').'","' . Loc::getMessage("TRANSFER_CATALOG_ELEMENT_UPDATE", ["ID" => $lastID, "PRODUCTS" => implode(", ", $arProducts)]) . '");';
    }

    die();
}

$clean_test_table = '<table id="result_table" cellpadding="0" cellspacing="0" border="0" width="100%" class="internal">'.
    '<tr class="heading">'.
    '<td>Текущее действие</td>'.
    '<td width="1%">&nbsp;</td>'.
    '</tr>'.
    '</table>';

$aTabs = array(array("DIV" => "edit1", "TAB" => Loc::getMessage("TRANSFER_TAB1")));
CModule::AddAutoloadClasses(
    "studio7spb.marketplace",
    array(
        "\\Studio7spb\\Marketplace\\VendorTable" => "lib/anytos/vendor.php"
    )
);
$vendor = VendorTable::getList([
    "filter" => [
        "ID" => $arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"]["VALUE"]
    ]
]);
if($vendor = $vendor->fetch()){
    $aTabs = array(array("DIV" => "edit1", "TAB" => Loc::getMessage("TRANSFER_TITLE_VENDOR", ["VENDOR" => $vendor["title"]])));
}
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$APPLICATION->SetTitle(Loc::getMessage("TRANSFER_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

if($arResult["SETTINGS"]["IMPORT_DATA_VENDOR_PRICE"]["VALUE"] < 1)
{
    echo CAdminMessage::ShowMessage(Loc::getMessage("TRANSFER_VENDOR_EMPTY"));
    die;
}
?>
    <script type="text/javascript">

        var bWorkFinished = false;
        var bSubmit;

        function set_start(val)
        {
            document.getElementById('work_start').disabled = val ? 'disabled' : '';
            document.getElementById('work_stop').disabled = val ? '' : 'disabled';
            document.getElementById('progress').style.display = val ? 'block' : 'none';

            if (val)
            {
                ShowWaitWindow();
                document.getElementById('result').innerHTML = '<?=$clean_test_table?>';
                document.getElementById('status').innerHTML = 'Работаю...';

                document.getElementById('percent').innerHTML = '0%';
                document.getElementById('indicator').style.width = '0%';

                CHttpRequest.Action = work_onload;
                CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>');
            }
            else
                CloseWaitWindow();
        }

        function work_onload(result)
        {
            try
            {
                eval(result);

                iPercent = CurrentStatus[0];
                strNextRequest = CurrentStatus[1];
                strCurrentAction = CurrentStatus[2];

                document.getElementById('percent').innerHTML = iPercent + '%';
                document.getElementById('indicator').style.width = iPercent + '%';

                document.getElementById('status').innerHTML = 'Работаю...';

                if (strCurrentAction != 'null')
                {
                    oTable = document.getElementById('result_table');
                    oRow = oTable.insertRow(-1);
                    oCell = oRow.insertCell(-1);
                    oCell.innerHTML = strCurrentAction;
                    oCell = oRow.insertCell(-1);
                    oCell.innerHTML = '';

                    var totalRowCount = oTable.rows.length; // 5
                    console.info(totalRowCount);


                    if(totalRowCount > 5){
                        oTable.deleteRow(0);
                    }

                }

                if (strNextRequest && document.getElementById('work_start').disabled)
                    CHttpRequest.Send('<?= $_SERVER["PHP_SELF"]?>?work_start=Y&lang=<?=LANGUAGE_ID?>&<?=bitrix_sessid_get()?>' + strNextRequest);
                else
                {
                    set_start(0);
                    bWorkFinished = true;
                }

            }
            catch(e)
            {
                CloseWaitWindow();
                document.getElementById('work_start').disabled = '';
                alert('Сбой в получении данных');
            }
        }

    </script>

    <form method="post" action="<?echo $APPLICATION->GetCurPage()?>" enctype="multipart/form-data" name="post_form" id="post_form">
        <?
        echo bitrix_sessid_post();

        $tabControl->Begin();
        $tabControl->BeginNextTab();
        ?>
        <tr>
            <td colspan="2">

                <input type=button value="Старт" id="work_start" onclick="set_start(1)" />
                <input type=button value="Стоп" disabled id="work_stop" onclick="bSubmit=false;set_start(0)" />
                <div id="progress" style="display:none;" width="100%">
                    <br />
                    <div id="status"></div>
                    <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td height="10">
                                <div style="border:1px solid #B9CBDF">
                                    <div id="indicator" style="height:10px; width:0%; background-color:#B9CBDF"></div>
                                </div>
                            </td>
                            <td width=30>&nbsp;<span id="percent">0%</span></td>
                        </tr>
                    </table>
                </div>
                <div id="result" style="padding-top:10px"></div>

            </td>
        </tr>
        <?
        $tabControl->End();
        ?>
    </form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>