<?
/**
 * Import settings page
 * @var CMain $APPLICATION
 */

// <editor-fold defaultstate="collapsed" desc="# Подготовка, подключение библиотек, инициализация и объявление базовых переменных">
use Bitrix\Main\Localization\Loc,
    Studio7spb\Marketplace\ImportPriceTable;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
Loc::loadLanguageFile(__FILE__);

CModule::AddAutoloadClasses(
    "studio7spb.marketplace",
    [
        "\\Studio7spb\\Marketplace\\ReviewsTable" => "lib/importprice.php"
    ]
);

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="# Работа с таблицей данных">

$sTableID = "anytos_products";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);
$rsData = ImportPriceTable::getList([
    "filter" => [
        "!NOTE" => false
    ]
]);
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText( $rsData->GetNavPrint( GetMessage("GRUPPER_PAGE_NAVI") ) );

$headers = [];
foreach (ImportPriceTable::getMap() as $code=>$head){

    switch ($code){
        case "STATUS":
        case "DATA":
            $headers[] = ["id" => $code, "content" => $head["title"], "sort" => ToLower($code), "default" => false];
            break;
        default:
            $headers[] = ["id" => $code, "content" => $head["title"], "sort" => ToLower($code), "default" => true];
    }


}

$lAdmin->AddHeaders($headers);

while ($element = $rsData->Fetch())
{
    $row =& $lAdmin->AddRow($element["ID"], $element);
    $row->AddViewField("ARTICLE", $element["ARTICLE"]);
    $row->AddViewField("DATA", $element["DATA"]);
    $row->AddViewField("NOTE", $element["NOTE"]);
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="# ВЫВОД">

// альтернативный вывод
$lAdmin->CheckListMode();

// установим заголовок страницы
$APPLICATION->SetTitle( Loc::getMessage("IMPORT_CATALOG_EMPTY_TITLE") );

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// выведем таблицу списка элементов
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");

// </editor-fold>