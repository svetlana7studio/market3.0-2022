<?
$moduleId = 'studio7spb.marketplace';
use \Bitrix\Main\Localization\Loc;
if (!\Bitrix\Main\Loader::includeModule($moduleId)) {
	return;
}
global $APPLICATION, $USER;
if(!$USER->IsAdmin()){
	return;
}

$userGroups = array();
$oUserGrop = \Bitrix\Main\GroupTable::getList(array(
	"filter" => array(
		"STRING_ID" => array("mp_partners_admin", "mp_partners_moderate")
	), 
));
while ($aUserGrop = $oUserGrop->fetch()) {
	$userGroups[$aUserGrop["ID"]] = $aUserGrop["NAME"];
}

Loc::loadMessages(__FILE__);
$allOptions = array(
	array("company_iblock_id", "Идентификатор инфоблока \"Компании\"", "1", Array("text", 10)),
	array("shop_iblock_id", "Идентификатор инфоблока \"Магазины\"", "6", Array("text", 10)),
	array("catalog_iblock_id", "Идентификатор инфоблока \"Каталог\"", "2", Array("text", 10)),
	array("seller_admin_group", "Группа пользователей администраторов продавцов", "5", Array("selectbox"), $userGroups),
	array("seller_moderator_group", "Группа пользователей модераторов продавцов", "6", Array("selectbox"), $userGroups),
	array("hidden_prefix", "Префикс свойств инфоблока", "H_", Array("text", 20)),
	array("email_in_footer", "Email в подвале", "info@marketplace.ru", Array("text", 30)),
);

if ($REQUEST_METHOD=="POST" && strlen($Update)>0 && check_bitrix_sessid()) {

	foreach($allOptions as $option) {
		$optionName = $option[0];
		$optionVal = $$optionName;
		if ($option[3][0] == "checkbox" && $optionVal != "Y")
			$optionVal = "N";
		COption::SetOptionString($moduleId, $optionName, $optionVal, $option[1]);
	}
}

$arTabs[] = array(
	"DIV" => "systemSettings",
	"TAB" => "Основные настройки",
	"ICON" => "settings",
	"TITLE" => "Основные настройки",
);

$tabControl = new CAdminTabControl("tabControl", $arTabs);

CJSCore::Init(array("jquery"));
CAjax::Init();
?>
<?$tabControl->Begin();?>
	<form method="post" action="<?=$APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?=LANGUAGE_ID?>" ENCTYPE="multipart/form-data">
		<?
			echo bitrix_sessid_post();
			$tabControl->BeginNextTab();
			foreach($allOptions as $optionItem) {
				$val = COption::GetOptionString($moduleId, $optionItem[0], $optionItem[2]);
				$type = $optionItem[3];
			?>
				<tr>
					<td valign="top" width="50%"><?
						if ($type[0]=="checkbox")
							echo "<label for=\"".htmlspecialcharsbx($optionItem[0])."\">".$optionItem[1]."</label>";
						else
							echo $optionItem[1];
					?></td>
					<td valign="middle" width="50%">
						<?if($type[0]=="checkbox"):?>
							<input type="checkbox" name="<?echo htmlspecialcharsbx($optionItem[0])?>" id="<?echo htmlspecialcharsbx($optionItem[0])?>" value="Y"<?if($val=="Y")echo" checked";?>>
						<?elseif($type[0]=="text"):?>
							<input type="text" size="<?echo $type[1]?>" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($optionItem[0])?>">
						<?elseif($type[0]=="textarea"):?>
							<textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialcharsbx($optionItem[0])?>"><?echo htmlspecialcharsbx($val)?></textarea>
						<?elseif($type[0]=="selectbox"):?>
							<select name="<?echo htmlspecialcharsbx($optionItem[0])?>" id="<?echo htmlspecialcharsbx($optionItem[0])?>">
								<?foreach($optionItem[4] as $v => $k)
								{
									?><option value="<?=$v?>"<?if($val==$v)echo" selected";?>><?=$k?></option><?
								}
								?>
							</select>
						<?endif?>
					</td>
				</tr>
		<?
			}
			$tabControl->Buttons();
		?>
		<input type="submit" name="Update" value="<?echo GetMessage("MAIN_SAVE")?>">
	</form>
<?$tabControl->End();?>