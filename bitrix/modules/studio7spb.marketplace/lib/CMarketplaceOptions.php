<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

class CMarketplaceOptions {	

	private static $instance = null;
	private static $moduleId = "studio7spb.marketplace";
	private static $moduleOptions = array();

	private function __construct() {
		$this->moduleOptions = Option::getForModule(self::$moduleId);

	}

	public static function getInstance() {
		if (static::$instance === null) {
			static::$instance = new CMarketplaceOptions();
		}
		return static::$instance;
	}

	public function setOption($optionName, $optionValue) {
		$this->moduleOptions[$optionName] = $optionValue;
	}
	public function getOption($optionName) {
		return $this->moduleOptions[$optionName];
	}
	public function getOptionList() {
		return $this->moduleOptions;
	}


} 