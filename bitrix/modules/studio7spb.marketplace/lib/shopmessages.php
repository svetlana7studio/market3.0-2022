<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class ShopMessagesTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> DATETIME datetime optional
 * <li> SORT int optional default 500
 * <li> ACTIVE bool optional default 'Y'
 * <li> ELEMENT_ID int optional
 * <li> IBLOCK_ID int optional
 * <li> USER_ID int optional
 * <li> USER_MAIL string(255) optional
 * <li> USER_MESSAGE string optional
 * <li> PARAMS string(255) optional
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class ShopMessagesTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_shop_messages';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('SHOP_MESSAGES_ENTITY_ID_FIELD'),
            ),
            'DATETIME' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('SHOP_MESSAGES_ENTITY_DATETIME_FIELD'),
            ),
            'SORT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SHOP_MESSAGES_ENTITY_SORT_FIELD'),
            ),
            'ACTIVE' => array(
                'data_type' => 'boolean',
                'values' => array('N', 'Y'),
                'title' => Loc::getMessage('SHOP_MESSAGES_ENTITY_ACTIVE_FIELD'),
            ),
            'ELEMENT_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SHOP_MESSAGES_ENTITY_ELEMENT_ID_FIELD'),
            ),
            'IBLOCK_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SHOP_MESSAGES_ENTITY_IBLOCK_ID_FIELD'),
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SHOP_MESSAGES_ENTITY_USER_ID_FIELD'),
            ),
            'USER_MAIL' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateUserMail'),
                'title' => Loc::getMessage('SHOP_MESSAGES_ENTITY_USER_MAIL_FIELD'),
            ),
            'USER_MESSAGE' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('SHOP_MESSAGES_ENTITY_USER_MESSAGE_FIELD'),
            ),
            'PARAMS' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateParams'),
                'title' => Loc::getMessage('SHOP_MESSAGES_ENTITY_PARAMS_FIELD'),
            ),
        );
    }
    /**
     * Returns validators for USER_MAIL field.
     *
     * @return array
     */
    public static function validateUserMail()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for PARAMS field.
     *
     * @return array
     */
    public static function validateParams()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
}