<?
namespace Studio7spb\Marketplace\Handler;

use \Bitrix\Main\Loader;
 
class SaleActionDiscountFromProperty extends \CSaleActionCtrlBasketGroup
{
	public static function GetClassName()
	{
		return __CLASS__;
	}

	public static function GetControlID()
	{
		return "DiscountFromFroperty";
	}

	public static function GetControlDescr()
	{
		return parent::GetControlDescr();
	}

	public static function GetAtoms()
	{
		return static::GetAtomsEx(false, false);
	}

	public static function GetControlShow($arParams)
	{
		$arAtoms = static::GetAtomsEx(false, false);
		$arResult = [
			"controlId" => static::GetControlID(),
			"group" => false,
			"label" => "Применить скидку из свойства",
			"defaultText" => "",
			"showIn" => static::GetShowIn($arParams["SHOW_IN_GROUPS"]),
			"control" => [
				"Применить скидку из свойства",
				$arAtoms["IB_PROPERTY"]
			]
		];

		return $arResult;
	}

	public static function GetAtomsEx($strControlID = false, $boolEx = false)
	{
		$boolEx = (true === $boolEx ? true : false);
		$arAtomList = [
			"IB_PROPERTY" => [
				"JS" => [
					"id" => "IB_PROPERTY",
					"name" => "extra",
					"type" => "input",
				],
				"ATOM" => [
					"ID" => "IB_PROPERTY",
					"FIELD_TYPE" => "string",
					"FIELD_LENGTH" => 255,
					"MULTIPLE" => "N",
					"VALIDATE" => ""
				]
			],
		];
		if (!$boolEx) {
			foreach ($arAtomList as &$arOneAtom) {
				$arOneAtom = $arOneAtom["JS"];
			}
			if (isset($arOneAtom)) {
				unset($arOneAtom);
			}
		}
		return $arAtomList;
	}

	public static function Generate($arOneCondition, $arParams, $arControl, $arSubs = false)
	{
		$mxResult = __CLASS__ . "::applyProductDiscount(" . $arParams["ORDER"] . ", " . "\"" . $arOneCondition["IB_PROPERTY"] . "\"" . ");";

		return $mxResult;
	}

	/**
	 * Применяет скидку из свойства к товарам из корзины
	 * @param $arOrder
	 * @param $blProperty - Property code 
	 */
	public static function applyProductDiscount(&$arOrder, $blProperty)
	{
		if (\Bitrix\Main\Loader::includeModule('iblock')) {
			$productIds = array();
			foreach ($arOrder['BASKET_ITEMS'] as &$product)
			{
				$productIds[] = $product['PRODUCT_ID'];
			}
			unset($product);

			if(empty($productIds)) return;

			$discounts = array();
			$propertyCode = "PROPERTY_".$blProperty;

			$oElement = \CIBlockElement::GetList(array(), array("ID" => $productIds), false, false, array("ID", $propertyCode));
			while($rElement = $oElement->Fetch()) {
				$discounts[$rElement["ID"]] = $rElement[$propertyCode."_VALUE"];
			}
			//Применяем скидку
			foreach ($arOrder['BASKET_ITEMS'] as &$product) {
				$productId = $product['PRODUCT_ID'];
				if ((int)$discounts[$productId] > 0) {
					$product['DISCOUNT_PRICE'] = $product['BASE_PRICE'] - $discounts[$productId];
					$product['PRICE'] = $discounts[$productId];
				}
			}
			unset($product);
		}
	}
}

?>