<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main\Context\Site;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

Loc::loadMessages(__FILE__);

class CMarketplaceEvents
{
	const moduleId = 'studio7spb.marketplace';
	
	public static function onBuildGlobalMenu(&$arGlobalMenu, &$arModuleMenu)
	{
		$GLOBALS['APPLICATION']->SetAdditionalCss("/bitrix/css/".self::moduleId."/menu.css");
		$arRes = array(
			"global_menu_studio7spb" => array(
				"menu_id" => "studio7spb",
				"page_icon" => "services_title_icon",
				"index_icon" => "services_page_icon",
				"text" => Loc::getMessage('STUDIO7_EVENTS_MENU_PERFORMANCE'),
				"title" => Loc::getMessage('STUDIO7_EVENTS_MENU_TITLE'),
				"sort" => 450,
				"items_id" => "global_menu_studio7spb",
				"help_section" => "studio7spb",
				"items" => array()
			),
		);
		return $arRes;
	}


	public static function OnAdminListDisplay(&$oAdminList) {
		
		if($oAdminList->table_id == 'tbl_iblock_element_6b82575264c4a4f3ed9e05461d9bef2a') {
			foreach ($oAdminList->aRows as $aRowItem) {
				$aRowItem->aActions["salersProductList"]["ICON"] = "";
				$aRowItem->aActions["salersProductList"]["TEXT"] = "Товары продавца";
				$sUrl = "/bitrix/admin/studio7spb.marketplace_salers_tools.php?action=getSalesProductPage&salerId=".$aRowItem->id;
				$aRowItem->aActions["salersProductList"]["ACTION"] = "BX.adminPanel.Redirect([], '".\CUtil::AddSlashes($sUrl)."', event);";
			}
		}
	}

	public static function OnBeforeProlog() {
		
	}
	public static function OnAfterIBlockElementUpdate(&$aFields) {

	    $aFields["IBLOCK_ID"] == CMarketplaceOptions::getInstance()->getOption("company_iblock_id");

		if($aFields["IBLOCK_ID"] > 0) {

			if($aFields["ACTIVE"] == "Y") { // Если компания активна
				// get users
				$aUser = array();

				// COMP_STATUS
                // "CODE" => "COMP_USER"
                $COMP_STATUS = 0;
                $COMP_USER_INFORM = 0;
				$oCompanyProperty = \CIBlockElement::GetProperty($aFields["IBLOCK_ID"], $aFields["ID"], "sort", "asc", array());
				while ($rCompanyProperty = $oCompanyProperty->Fetch()) {
                    // Не уведомлять администраторов
                    // COMP_USER_INFORM
				    if($rCompanyProperty["CODE"] == "COMP_USER_INFORM")
                    {
                        $COMP_USER_INFORM = $rCompanyProperty["VALUE"];
                    }
				    if($rCompanyProperty["CODE"] == "COMP_USER")
                    {
                        // Получаем её администраторов
                        $aUser[] = $rCompanyProperty["VALUE"];
                    }
				    // Получаем статус
				    if($rCompanyProperty["CODE"] == "COMP_STATUS")
                    {
                        $COMP_STATUS = $rCompanyProperty["VALUE"];
                    }
				}
				unset($oCompanyProperty, $rCompanyProperty);

				// Если есть администраторы и статус верифицирован отсылаем приглашение администраторам
				if(
				    !empty($aUser) &&
                    $COMP_USER_INFORM <> 24 &&
                    $COMP_STATUS == 24
                ) {
					$oUser = new \CUser;
					foreach ($aUser as $aUserItem) {

					    // Вот тут раньше я генерировал новій пароль и отсілал его в письме
					    //$password = GetRandomCode(12);
                        // А тут происходило обновление в базе админа, оставил галочку активности на всякий случай
						$oUser->Update($aUserItem, array(
						    "ACTIVE" => $aFields["ACTIVE"],
                            //"PASSWORD" => $password,
                            //"CONFIRM_PASSWORD" => $password,
                        ));
						if($aFields["ACTIVE"] == "Y"){
                            // send invite
                            $aUserItem = UserTable::getList(array(
                                "filter" => array(
                                    "ID" => $aUserItem
                                ),
                                "select" => array(
                                    "ID",
                                    "NAME",
                                    "LAST_NAME",
                                    "LOGIN",
                                    "EMAIL"
                                )
                            ));
                            if($aUserItem = $aUserItem->fetch()){
                                //$aUserItem["PASSWORD"] = $password;
                                // Password: #PASSWORD#<br>

                                \CEvent::Send("MAS7SBP_MARKETPLACE_PARTNER_INVITE", "s1", $aUserItem, "N"); // SITE_ID
                                //\CEvent::Send("USER_INVITE", SITE_ID, $mailFields, "N");
                                // Не уведомлять администраторов
                                // COMP_USER_INFORM
                                \CIBlockElement::SetPropertyValuesEx($aFields["ID"], $aFields["IBLOCK_ID"], array("COMP_USER_INFORM" => 24));

                            }
                        }

					}
				}
			}

		}
		
	}
	

}
?>