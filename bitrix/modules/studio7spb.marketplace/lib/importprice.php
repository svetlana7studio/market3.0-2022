<?
namespace Studio7spb\Marketplace;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main;

/**
 * Class ImportTablePrice
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> STATUS string(1) optional
 * <li> DATA string optional
 * <li> NOTE string(255) optional
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class ImportPriceTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_import_price';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => "ID"
            ),
            'STATUS' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateStatus'),
                'title' => "STATUS",
            ),
            'ARTICLE' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateArticle'),
                'title' => "ARTICLE",
            ),
            'DATA' => array(
                'data_type' => 'text',
                'title' => "DATA",
            ),
            'NOTE' => array(
                'data_type' => 'text',
                'title' => "NOTE",
            )
        );
    }

    /**
     * Returns validators for STATUS field.
     *
     * @return array
     */
    public static function validateStatus()
    {
        return array(
            new Main\Entity\Validator\Length(null, 1),
        );
    }

    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateArticle()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
}