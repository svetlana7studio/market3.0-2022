<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main\Localization\Loc,
	\Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class CMarketplaceSeller
{

	private static $availableAdminOperation = array(
		"edit_product",
		"edit_company",
		"edit_shop",
		"edit_user",
	);
	private static $availableModeratorOperation = array(
		"edit_product",
	);

	public static function canDoOperation($operation) {
		global $USER;
		if(!$USER->isAuthorized()) return false;
		if($USER->isAdmin()) return true;

		$returnResult = false;
		if(in_array($operation, self::$availableAdminOperation)) {
			$returnResult = self::isSellerAdmin();
		}
		if(in_array($operation, self::$availableModeratorOperation) && !$returnResult) {
			$returnResult = self::isSellerModerator();
		}
		return $returnResult;
	}

	public static function isSellerAdmin() {
		global $USER;
		if(!$USER->isAuthorized()) return false;
		if($USER->isAdmin()) return true;

		$sellerAdminGropuId = CMarketplaceOptions::getInstance()->getOption("seller_admin_group");
		$userGroups = $USER->GetUserGroupArray();
		
		return in_array($sellerAdminGropuId, $userGroups);
	}

	public static function isSellerModerator() {
		global $USER;
		if(!$USER->isAuthorized()) return false;
		if($USER->isAdmin()) return true;

		$sellerAdminGropuId = CMarketplaceOptions::getInstance()->getOption("seller_moderator_group");
		$userGroups = $USER->GetUserGroupArray();
		return in_array($sellerAdminGropuId, $userGroups);
	}

	public static function getCompanyById($elementId = null) {

		if(!$elementId) return null;

		$companyIblockid = (int)CMarketplaceOptions::getInstance()->getOption("company_iblock_id");
		if($companyIblockid <= 0) {
			return false;
		}
		Loader::includeModule("iblock");
		$oElement = \CIBlockElement::GetList(array(), 
			array(
				"IBLOCK_ID" => $companyIblockid,
				"ID" => (int)$elementId,
			),
			false,
			array("nTopCount" => 1),
			array("ID", "NAME", "PREVIEW_TEXT", "PROPERTY_COMP_GARANTY", "PROPERTY_COMP_DELIVERY", "PROPERTY_COMP_NAME_SHORT")
		);
		if($aElement = $oElement->fetch()) {
			return $aElement;
		}
		return null;
	}

	public static function getCompanyByUser() {
        global $USER;
        if($USER->IsAuthorized()){
            $companyIblockid = (int)CMarketplaceOptions::getInstance()->getOption("company_iblock_id");
            if($companyIblockid <= 0) {
                return false;
            }
            Loader::includeModule("iblock");
            $oElement = \CIBlockElement::GetList(array(),
                array(
                    "IBLOCK_ID" => $companyIblockid,
                    "PROPERTY_COMP_USER" => $USER->GetID()
                ),
                false,
                array("nTopCount" => 1),
                array("ID", "NAME", "PREVIEW_TEXT", "PROPERTY_COMP_GARANTY", "PROPERTY_COMP_DELIVERY", "PROPERTY_COMP_NAME_SHORT", "PROPERTY_COMP_TYPE", "PROPERTY_COMP_COMMISSION")
            );
            if($aElement = $oElement->fetch()) {
                return $aElement;
            }
        }
		return false;
	}

	public static function getShopByUser() {

		$companyIblockid = self::getCompanyIdByUser();
		if($companyIblockid <= 0) return false;

		$shopIblockid = (int)CMarketplaceOptions::getInstance()->getOption("shop_iblock_id");
		if($shopIblockid <= 0) return false;

		Loader::includeModule("iblock");
		$oElement = \CIBlockElement::GetList(array(), 
			array(
				"IBLOCK_ID" => $shopIblockid,
				"PROPERTY_COMPANY" => $companyIblockid,
			),
			false,
			array("nTopCount" => 1),
			array("ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE")
		);
		if($aElement = $oElement->fetch()) return $aElement;
		
		return false;
	}

    public static function getCompanyUsers($companyID) {
        if(Loader::includeModule("iblock"))
        {
            $companyIblockid = (int)CMarketplaceOptions::getInstance()->getOption("company_iblock_id");
            if($companyIblockid <= 0) {
                return false;
            }
            $companyUsers = \CIBlockElement::GetList([],
                [
                    "IBLOCK_ID" => $companyIblockid,
                    "ID" => $companyID
                ],
                false,
                ["nTopCount" => 1],
                [
                    "ID",
                    "NAME",
                    "PROPERTY_COMP_USER"
                ]
            );
            if($companyUser = $companyUsers->Fetch())
                return $companyUser["PROPERTY_COMP_USER_VALUE"];
        }
    }

	public static function getCompanyIdByUser() {
		$aCompany = self::getCompanyByUser();
		if(!empty($aCompany)) {
			return $aCompany["ID"];
		}
		return false;
	}

} 