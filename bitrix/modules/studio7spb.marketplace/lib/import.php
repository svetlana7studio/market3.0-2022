<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class ImportTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> STATUS string(1) optional
 * <li> DATA string optional
 * <li> NOTE string(255) optional
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class ImportTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_import';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('IMPORT_ENTITY_ID_FIELD'),
            ),
            'STATUS' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateStatus'),
                'title' => Loc::getMessage('IMPORT_ENTITY_STATUS_FIELD'),
            ),
            'DATA' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('IMPORT_ENTITY_DATA_FIELD'),
            ),
            'NOTE' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateNote'),
                'title' => Loc::getMessage('IMPORT_ENTITY_NOTE_FIELD'),
            ),
        );
    }
    /**
     * Returns validators for STATUS field.
     *
     * @return array
     */
    public static function validateStatus()
    {
        return array(
            new Main\Entity\Validator\Length(null, 1),
        );
    }
    /**
     * Returns validators for NOTE field.
     *
     * @return array
     */
    public static function validateNote()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
}