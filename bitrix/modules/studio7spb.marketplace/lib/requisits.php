<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class RequisitsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> TIMESTAMP_X datetime mandatory default 'CURRENT_TIMESTAMP'
 * <li> USER_ID int optional
 * <li> NAME string(255) optional
 * <li> OWNERSHIP string(255) optional
 * <li> INN string(255) optional
 * <li> KPP string(255) optional
 * <li> OKPO string(255) optional
 * <li> OGRN string(255) optional
 * <li> OKATO string(255) optional
 * <li> ACCOUNTANT string(255) optional
 * <li> GENERAL string(255) optional
 * <li> BAMK string(255) optional
 * <li> PAYMENT string(255) optional
 * <li> BIK string(255) optional
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class RequisitsTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_requisits';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('REQUISITS_ENTITY_ID_FIELD'),
            ),
            'TIMESTAMP_X' => array(
                'data_type' => 'datetime',
                'required' => true,
                'title' => Loc::getMessage('REQUISITS_ENTITY_TIMESTAMP_X_FIELD'),
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('REQUISITS_ENTITY_USER_ID_FIELD'),
            ),
            'NAME' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateName'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_NAME_FIELD'),
            ),
            'OWNERSHIP' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateOwnership'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_OWNERSHIP_FIELD'),
            ),
            'INN' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateInn'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_INN_FIELD'),
            ),
            'KPP' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateKpp'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_KPP_FIELD'),
            ),
            'OKPO' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateOkpo'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_OKPO_FIELD'),
            ),
            'OGRN' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateOgrn'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_OGRN_FIELD'),
            ),
            'OKATO' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateOkato'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_OKATO_FIELD'),
            ),
            'ACCOUNTANT' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateAccountant'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_ACCOUNTANT_FIELD'),
            ),
            'GENERAL' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateGeneral'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_GENERAL_FIELD'),
            ),
            'BAMK' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateBamk'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_BAMK_FIELD'),
            ),
            'PAYMENT' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validatePayment'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_PAYMENT_FIELD'),
            ),
            'BIK' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateBik'),
                'title' => Loc::getMessage('REQUISITS_ENTITY_BIK_FIELD'),
            ),
            'ADRESS_JUR' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('REQUISITS_ENTITY_ADRESS_JUR_FIELD'),
            ),
            'ADRESS_FACT' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('REQUISITS_ENTITY_ADRESS_FACT_FIELD'),
            ),
            'ADRESS_POST' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('REQUISITS_ENTITY_ADRESS_POST_FIELD'),
            ),
        );
    }
    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for OWNERSHIP field.
     *
     * @return array
     */
    public static function validateOwnership()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for INN field.
     *
     * @return array
     */
    public static function validateInn()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for KPP field.
     *
     * @return array
     */
    public static function validateKpp()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for OKPO field.
     *
     * @return array
     */
    public static function validateOkpo()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for OGRN field.
     *
     * @return array
     */
    public static function validateOgrn()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for OKATO field.
     *
     * @return array
     */
    public static function validateOkato()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for ACCOUNTANT field.
     *
     * @return array
     */
    public static function validateAccountant()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for GENERAL field.
     *
     * @return array
     */
    public static function validateGeneral()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for BAMK field.
     *
     * @return array
     */
    public static function validateBamk()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for PAYMENT field.
     *
     * @return array
     */
    public static function validatePayment()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for BIK field.
     *
     * @return array
     */
    public static function validateBik()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
}