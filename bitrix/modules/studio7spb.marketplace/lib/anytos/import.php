<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class ImportTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> STATUS string(1) optional
 * <li> DATA string optional
 * <li> NOTE string(255) optional
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class ImportTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_import';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => "ID"
            ),
            'DATA' => array(
                'data_type' => 'text',
                'title' => "DATA"
            )
        );
    }

}