<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\DatetimeField,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\StringField,
    Bitrix\Main\ORM\Fields\Validators\LengthValidator,
    Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);

/**
 * Class VendorTable
 *
 * Fields:
 * <ul>
 * <li> id int mandatory
 * <li> old_id int optional default 0
 * <li> title string(255) optional
 * <li> prefix_art string(10) optional
 * <li> active int optional default 1
 * <li> ins_dt datetime optional default current datetime
 * <li> ins_user int optional default 0
 * <li> del int optional default 0
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class VendorTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'vendor';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('_ENTITY_ID_FIELD'),
            ),
            'old_id' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('_ENTITY_OLD_ID_FIELD'),
            ),
            'title' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateTitle'),
                'title' => Loc::getMessage('_ENTITY_TITLE_FIELD'),
            ),
            'prefix_art' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validatePrefixArt'),
                'title' => Loc::getMessage('_ENTITY_PREFIX_ART_FIELD'),
            ),
            'active' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('_ENTITY_ACTIVE_FIELD'),
            ),
            'ins_dt' => array(
                'data_type' => 'datetime',
                'required' => true,
                'title' => Loc::getMessage('_ENTITY_INS_DT_FIELD'),
            ),
            'ins_user' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('_ENTITY_INS_USER_FIELD'),
            ),
            'del' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('_ENTITY_DEL_FIELD'),
            ),
        );
    }
    /**
     * Returns validators for title field.
     *
     * @return array
     */
    public static function validateTitle()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for prefix_art field.
     *
     * @return array
     */
    public static function validatePrefixArt()
    {
        return array(
            new Main\Entity\Validator\Length(null, 10),
        );
    }
}