<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\IntegerField;

/**
 * Powered by Artem@koorochka.com
 * 5.11.20250
 * Class SectionPropertyTable
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> SECTION_ID int mandatory
 * <li> DISCOUNT int mandatory
 * <li> BREND_ID int mandatory
 * </ul>
 *
 * @package Studio7spb\Marketplace\Anytos
 **/

class SectionPropertyTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     * @return string
     */
    public static function getTableName()
    {
        return 'b_anytos_section_property';
    }

    /**
     * Returns entity map definition.
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField('ID', ['primary' => true, 'autocomplete' => true, 'title' => "ID"]),
            new IntegerField('SECTION_ID', ['required' => true, 'title' => "SECTION_ID"]),
            new IntegerField('DISCOUNT', ['title' => "DISCOUNT"]),
            new IntegerField('BREND_ID', ['title' => "BREND_ID"]),
        ];
    }
}