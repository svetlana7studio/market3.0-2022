<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\FloatField,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\TextField;

Loc::loadMessages(__FILE__);

/**
 * Class ElementPropS2Table
 *
 * Fields:
 * <ul>
 * <li> IBLOCK_ELEMENT_ID int mandatory
 * <li> PROPERTY_267 int optional
 * <li> PROPERTY_268 text optional
 * <li> PROPERTY_277 double optional
 * <li> PROPERTY_282 text optional
 * <li> PROPERTY_285 text optional
 * <li> PROPERTY_286 text optional
 * <li> PROPERTY_288 double optional
 * <li> PROPERTY_289 text optional
 * <li> PROPERTY_290 text optional
 * <li> PROPERTY_291 text optional
 * <li> PROPERTY_294 text optional
 * <li> PROPERTY_297 text optional
 * <li> PROPERTY_321 int optional
 * <li> PROPERTY_330 text optional
 * <li> PROPERTY_332 text optional
 * <li> PROPERTY_334 text optional
 * <li> PROPERTY_336 text optional
 * <li> PROPERTY_340 double optional
 * <li> PROPERTY_341 double optional
 * <li> PROPERTY_342 double optional
 * <li> PROPERTY_343 double optional
 * <li> PROPERTY_344 double optional
 * <li> PROPERTY_345 text optional
 * <li> PROPERTY_346 text optional
 * <li> PROPERTY_347 text optional
 * <li> PROPERTY_348 text optional
 * <li> PROPERTY_349 text optional
 * <li> PROPERTY_350 text optional
 * <li> PROPERTY_351 text optional
 * <li> PROPERTY_352 text optional
 * <li> PROPERTY_353 text optional
 * <li> PROPERTY_355 text optional
 * <li> PROPERTY_356 text optional
 * <li> PROPERTY_386 text optional
 * <li> PROPERTY_388 double optional
 * <li> PROPERTY_390 text optional
 * <li> PROPERTY_394 text optional
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class ElementPropS2Table extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock_element_prop_s2';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'IBLOCK_ELEMENT_ID',
                [
                    'primary' => true,
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_IBLOCK_ELEMENT_ID_FIELD')
                ]
            ),
            new IntegerField(
                'PROPERTY_267',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_267_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_268',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_268_FIELD')
                ]
            ),
            new FloatField(
                'PROPERTY_277',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_277_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_282',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_282_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_285',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_285_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_286',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_286_FIELD')
                ]
            ),
            new FloatField(
                'PROPERTY_288',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_288_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_289',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_289_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_290',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_290_FIELD')
                ]
            ),

            new TextField(
                'PROPERTY_291',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_291_FIELD'),
                    //'serialized' => true
                ]
            ),


            new TextField(
                'PROPERTY_294',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_294_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_297',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_297_FIELD')
                ]
            ),
            new IntegerField(
                'PROPERTY_321',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_321_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_330',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_330_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_332',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_332_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_334',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_334_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_336',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_336_FIELD')
                ]
            ),
            new FloatField(
                'PROPERTY_340',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_340_FIELD')
                ]
            ),
            new FloatField(
                'PROPERTY_341',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_341_FIELD')
                ]
            ),
            new FloatField(
                'PROPERTY_342',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_342_FIELD')
                ]
            ),
            new FloatField(
                'PROPERTY_343',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_343_FIELD')
                ]
            ),
            new FloatField(
                'PROPERTY_344',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_344_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_345',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_345_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_346',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_346_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_347',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_347_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_348',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_348_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_349',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_349_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_350',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_350_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_351',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_351_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_352',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_352_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_353',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_353_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_355',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_355_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_356',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_356_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_386',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_386_FIELD')
                ]
            ),
            new FloatField(
                'PROPERTY_388',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_388_FIELD')
                ]
            ),
            new TextField(
                'PROPERTY_390',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_390_FIELD')
                ]
            ),
             new TextField(
                'PROPERTY_394',
                [
                    'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_390_FIELD')
                ]
            ),
        ];
    }
}