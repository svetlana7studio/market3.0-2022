<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\Relations\Reference;

Loc::loadMessages(__FILE__);

/**
 * Class SectionElementTable
 *
 * Fields:
 * <ul>
 * <li> IBLOCK_SECTION_ID int mandatory
 * <li> IBLOCK_ELEMENT_ID int mandatory
 * <li> ADDITIONAL_PROPERTY_ID int optional
 * <li> IBLOCK_ELEMENT_ID reference to {@link \Bitrix\Iblock\IblockElementTable}
 * <li> ADDITIONAL_PROPERTY_ID reference to {@link \Bitrix\Iblock\IblockPropertyTable}
 * <li> IBLOCK_SECTION_ID reference to {@link \Bitrix\Iblock\IblockSectionTable}
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class SectionElementTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock_section_element';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'IBLOCK_SECTION_ID',
                [
                    'primary' => true,
                    'title' => Loc::getMessage('SECTION_ELEMENT_ENTITY_IBLOCK_SECTION_ID_FIELD')
                ]
            ),
            new IntegerField(
                'IBLOCK_ELEMENT_ID',
                [
                    'primary' => true,
                    'title' => Loc::getMessage('SECTION_ELEMENT_ENTITY_IBLOCK_ELEMENT_ID_FIELD')
                ]
            ),
            new Reference(
                'IBLOCK_ELEMENT',
                '\Bitrix\Iblock\ElementTable',
                ['=this.IBLOCK_ELEMENT_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'PROPERTY',
                '\Studio7spb\Marketplace\ElementPropS2Table',
                ['=this.IBLOCK_ELEMENT_ID' => 'ref.IBLOCK_ELEMENT_ID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'IBLOCK_SECTION',
                '\Bitrix\Iblock\SectionTable',
                [
                    '=this.IBLOCK_SECTION_ID' => 'ref.ID',
                    //'ref.DEPTH_LEVEL' => 3
                ],
                ['join_type' => 'INNER']
            ),
            new Reference(
                'PARENT_SECTION',
                '\Bitrix\Iblock\SectionTable',
                [

                    //'<this.IBLOCK_SECTION.LEFT_MARGIN' => 'ref.LEFT_MARGIN',
                    //'>this.IBLOCK_SECTION.RIGHT_MARGIN' => 'ref.RIGHT_MARGIN',

                    '<ref.LEFT_MARGIN' => 'this.IBLOCK_SECTION.LEFT_MARGIN',
                    '>ref.RIGHT_MARGIN' => 'this.IBLOCK_SECTION.RIGHT_MARGIN',
                ],
                ['join_type' => 'inner']
            )
        ];
    }
}