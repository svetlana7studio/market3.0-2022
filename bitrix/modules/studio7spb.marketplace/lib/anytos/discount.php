<?php
namespace Studio7spb\Marketplace\M2;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\TextField;

/**
 * Class DiscountTable
 *
 * Fields:
 * <ul>
 * <li> IBLOCK_ELEMENT_ID int mandatory
 * <li> DISCOUNT text optional
 * </ul>
 *
 * @package Studio7spb\Marketplace\M2
 **/

class DiscountTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock_element_prop_s2';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'IBLOCK_ELEMENT_ID',
                [
                    'primary' => true,
                    'title' => "IBLOCK_ELEMENT_ID"
                ]
            ),
            new TextField(
                'PROPERTY_291', // create index `anytos_discont` on b_iblock_element_prop_s2 (`PROPERTY_291`(10));
                [
                    'title' => "DISCOUNT",
                    'serialized' => true
                ]
            ),

        ];
    }
}