<?php
namespace Studio7spb\Marketplace\M2;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\StringField,
    Bitrix\Main\ORM\Fields\Validators\LengthValidator;



/**
 * Class SyncPiTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ARTICLE string(50) optional
 * <li> QUANTITY string(50) optional
 * <li> PRICE_RED string(50) optional
 * <li> PRICE_YELLOW string(50) optional
 * <li> PRICE_GREEN string(50) optional
 * </ul>
 *
 * @package Studio7spb\Marketplace\M2
 **/

class SyncPiTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_sync_pi';
    }

    public static function clearTable()
    {
        global $DB;

        $DB->Query("DROP TABLE IF EXISTS " . self::getTableName() . ";");
        $DB->Query("CREATE TABLE IF NOT EXISTS " . self::getTableName() . "(
            ID INT NOT NULL AUTO_INCREMENT,
            ARTICLE VARCHAR(50),
            QUANTITY VARCHAR(50),
            PRICE_RED VARCHAR(50),
            PRICE_YELLOW VARCHAR(50),
            PRICE_GREEN VARCHAR(50),
            PRIMARY KEY (ID)
        );");


    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => "ID"
                ]
            ),
            new StringField(
                'ARTICLE',
                [
                    'validation' => [__CLASS__, 'validateArticle'],
                    'title' => "ARTICLE"
                ]
            ),
            new StringField(
                'QUANTITY',
                [
                    'validation' => [__CLASS__, 'validateQuantity'],
                    'title' => "QUANTITY"
                ]
            ),
            new StringField(
                'PRICE_RED',
                [
                    'validation' => [__CLASS__, 'validatePriceRed'],
                    'title' => "PRICE_RED"
                ]
            ),
            new StringField(
                'PRICE_YELLOW',
                [
                    'validation' => [__CLASS__, 'validatePriceYellow'],
                    'title' => "PRICE_YELLOW"
                ]
            ),
            new StringField(
                'PRICE_GREEN',
                [
                    'validation' => [__CLASS__, 'validatePriceGreen'],
                    'title' => "PRICE_GREEN"
                ]
            ),
        ];
    }

    /**
     * Returns validators for ARTICLE field.
     *
     * @return array
     */
    public static function validateArticle()
    {
        return [
            new LengthValidator(null, 50),
        ];
    }

    /**
     * Returns validators for QUANTITY field.
     *
     * @return array
     */
    public static function validateQuantity()
    {
        return [
            new LengthValidator(null, 50),
        ];
    }

    /**
     * Returns validators for PRICE_RED field.
     *
     * @return array
     */
    public static function validatePriceRed()
    {
        return [
            new LengthValidator(null, 50),
        ];
    }

    /**
     * Returns validators for PRICE_YELLOW field.
     *
     * @return array
     */
    public static function validatePriceYellow()
    {
        return [
            new LengthValidator(null, 50),
        ];
    }

    /**
     * Returns validators for PRICE_GREEN field.
     *
     * @return array
     */
    public static function validatePriceGreen()
    {
        return [
            new LengthValidator(null, 50),
        ];
    }
}