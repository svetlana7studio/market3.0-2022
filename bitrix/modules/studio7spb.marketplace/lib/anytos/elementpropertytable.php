<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\FloatField,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\Relations\Reference,
    Bitrix\Main\ORM\Fields\StringField,
    Bitrix\Main\ORM\Fields\TextField;

/**
 * Class ElementPropertyTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> IBLOCK_PROPERTY_ID int mandatory
 * <li> IBLOCK_ELEMENT_ID int mandatory
 * <li> VALUE text mandatory
 * <li> VALUE_TYPE enum ('text', 'html') optional default 'text'
 * <li> VALUE_ENUM int optional
 * <li> VALUE_NUM double optional
 * <li> DESCRIPTION string(255) optional
 * <li> IBLOCK_ELEMENT_ID reference to {@link \Bitrix\Iblock\IblockElementTable}
 * <li> IBLOCK_PROPERTY_ID reference to {@link \Bitrix\Iblock\IblockPropertyTable}
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class ElementPropertyTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock_element_property';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => "ID"
                ]
            ),
            new IntegerField(
                'IBLOCK_PROPERTY_ID',
                [
                    'required' => true,
                    'title' => "IBLOCK_PROPERTY_ID"
                ]
            ),
            new IntegerField(
                'IBLOCK_ELEMENT_ID',
                [
                    'required' => true,
                    'title' => "IBLOCK_ELEMENT_ID"
                ]
            ),
            new TextField(
                'VALUE',
                [
                    'required' => true,
                    'title' => "VALUE"
                ]
            ),
            new StringField(
                'VALUE_TYPE',
                [
                    'values' => array('text', 'html'),
                    'default' => 'text',
                    'title' => "VALUE_TYPE"
                ]
            ),
            new IntegerField(
                'VALUE_ENUM',
                [
                    'title' => "VALUE_ENUM"
                ]
            ),
            new FloatField(
                'VALUE_NUM',
                [
                    'title' => "VALUE_NUM"
                ]
            ),
            new Reference(
                'IBLOCK_ELEMENT',
                '\Bitrix\Iblock\IblockElement',
                ['=this.IBLOCK_ELEMENT_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'IBLOCK_PROPERTY',
                '\Bitrix\Iblock\IblockProperty',
                ['=this.IBLOCK_PROPERTY_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        ];
    }
}