<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\BooleanField,
    Bitrix\Main\ORM\Fields\DatetimeField,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\StringField,
    Bitrix\Main\ORM\Fields\Validators\LengthValidator;

Loc::loadMessages(__FILE__);

/**
 * Class VendorMessageTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> NAME string(255) optional
 * <li> EMAIL string(255) optional
 * <li> TEXT string(255) optional
 * <li> VENDOR_ID int optional
 * <li> CATEGORY string(32) optional
 * <li> SORT int optional
 * <li> ACTIVE bool ('N', 'Y') optional default 'Y'
 * <li> DATE_CREATE datetime optional
 * <li> DATE_UPDATE datetime optional
 * <li> USER_CREATE int optional
 * <li> USER_UPDATE int optional
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class VendorMessageTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_vendor_message';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_ID_FIELD')
                ]
            ),
            new StringField(
                'NAME',
                [
                    'validation' => [__CLASS__, 'validateName'],
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_NAME_FIELD')
                ]
            ),
            new StringField(
                'EMAIL',
                [
                    'validation' => [__CLASS__, 'validateEmail'],
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_EMAIL_FIELD')
                ]
            ),
            new StringField(
                'TEXT',
                [
                    'validation' => [__CLASS__, 'validateText'],
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_TEXT_FIELD')
                ]
            ),
            new StringField(
                'CATEGORY',
                [
                    'validation' => [__CLASS__, 'validateCategory'],
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_CATEGORY_FIELD')
                ]
            ),
            new IntegerField(
                'VENDOR_ID',
                [
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_VENDOR_ID_FIELD')
                ]
            ),
            new IntegerField(
                'SORT',
                [
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_SORT_FIELD')
                ]
            ),
            new BooleanField(
                'ACTIVE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'Y',
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_ACTIVE_FIELD')
                ]
            ),
            new DatetimeField(
                'DATE_CREATE',
                [
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_DATE_CREATE_FIELD')
                ]
            ),
            new DatetimeField(
                'DATE_UPDATE',
                [
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_DATE_UPDATE_FIELD')
                ]
            ),
            new IntegerField(
                'USER_CREATE',
                [
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_USER_CREATE_FIELD')
                ]
            ),
            new IntegerField(
                'USER_UPDATE',
                [
                    'title' => Loc::getMessage('VENDOR_MESSAGE_ENTITY_USER_UPDATE_FIELD')
                ]
            ),
        ];
    }

    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for EMAIL field.
     *
     * @return array
     */
    public static function validateEmail()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for TEXT field.
     *
     * @return array
     */
    public static function validateText()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    public static function validateCategory()
    {
        return [
            new LengthValidator(null, 31),
        ];
    }
}