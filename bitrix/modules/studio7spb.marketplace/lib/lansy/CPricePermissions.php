<?php
namespace Studio7spb\Marketplace;

class CPricePermissions
{

    private static $_priceCodes = array(
        0 => "normal_price",
        1 => "quickly_price",
        2 => "FOB"
    );

    public static function getPriceCodes()
    {
        global $USER;

        $arResult = array();

        if($USER->IsAuthorized()){
            $arResult = self::$_priceCodes;
        }

        return $arResult;
    }

    public static function getMinimumPriceCodes()
    {
        global $USER;

        $arResult = array();

        if($USER->IsAuthorized()){
            $arResult = self::$_priceCodes;
        }

        if(!empty($arResult)){
            foreach ($arResult as $key=>$value){
                if (
                    $value == "FOB" ||
                    $value == "quickly_price"
                ){
                    unset($arResult[$key]);
                    unset($arResult[$key]);
                }
            }
        }

        return $arResult;
    }

}