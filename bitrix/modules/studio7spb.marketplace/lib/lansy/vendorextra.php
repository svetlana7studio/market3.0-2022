<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class VendorExtraTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> DATE datetime optional
 * <li> USER_ID int optional
 * <li> VENDOR_ID int optional
 * <li> VALUE int optional
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class VendorExtraTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_vendor_extra';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('VENDOR_EXTRA_ENTITY_ID_FIELD'),
            ),
            'DATE' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('VENDOR_EXTRA_ENTITY_DATE_FIELD'),
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('VENDOR_EXTRA_ENTITY_USER_ID_FIELD'),
            ),
            'VENDOR_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('VENDOR_EXTRA_ENTITY_VENDOR_ID_FIELD'),
            ),
            'VALUE' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('VENDOR_EXTRA_ENTITY_VALUE_FIELD'),
            ),
        );
    }
}