<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);


/**
 * Class ElementPropertyTable
 *
 * Fields:
 * <ul>
 * <li> IBLOCK_ELEMENT_ID int mandatory
 * <li> PROPERTY_262 string optional
 * <li> PROPERTY_267 int optional
 * <li> PROPERTY_268 string optional
 * <li> PROPERTY_270 string optional
 * <li> PROPERTY_277 double optional
 * <li> PROPERTY_280 double optional
 * <li> PROPERTY_281 string optional
 * <li> PROPERTY_282 string optional
 * <li> PROPERTY_283 string optional
 * <li> PROPERTY_284 string optional
 * <li> PROPERTY_285 string optional
 * <li> PROPERTY_286 string optional
 * <li> PROPERTY_287 string optional
 * <li> PROPERTY_288 double optional
 * <li> PROPERTY_289 string optional
 * <li> PROPERTY_290 string optional
 * <li> PROPERTY_291 string optional
 * <li> PROPERTY_294 string optional
 * <li> PROPERTY_297 string optional
 * <li> PROPERTY_321 int optional
 * <li> PROPERTY_330 string optional
 * <li> PROPERTY_332 string optional
 * <li> PROPERTY_334 string optional
 * <li> PROPERTY_336 string optional
 * <li> PROPERTY_340 double optional
 * <li> PROPERTY_341 double optional
 * <li> PROPERTY_342 double optional
 * <li> PROPERTY_343 double optional
 * <li> PROPERTY_344 double optional
 * <li> PROPERTY_345 string optional
 * <li> PROPERTY_346 string optional
 * <li> PROPERTY_347 string optional
 * <li> PROPERTY_348 string optional
 * <li> PROPERTY_349 string optional
 * <li> PROPERTY_350 string optional
 * <li> PROPERTY_351 string optional
 * <li> PROPERTY_352 string optional
 * <li> PROPERTY_353 string optional
 * <li> PROPERTY_355 string optional
 * <li> PROPERTY_356 string optional
 * <li> PROPERTY_357 string optional
 * <li> DESCRIPTION_357 string(255) optional
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class ElementPropertyTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock_element_prop_s2';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'IBLOCK_ELEMENT_ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_IBLOCK_ELEMENT_ID_FIELD'),
            ),
            'PROPERTY_262' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_262_FIELD'),
            ),
            'PROPERTY_267' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_267_FIELD'),
            ),
            'PROPERTY_268' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_268_FIELD'),
            ),
            'PROPERTY_270' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_270_FIELD'),
            ),
            'PROPERTY_277' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_277_FIELD'),
            ),
            'PROPERTY_280' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_280_FIELD'),
            ),
            'PROPERTY_281' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_281_FIELD'),
            ),
            'PROPERTY_282' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_282_FIELD'),
            ),
            'PROPERTY_283' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_283_FIELD'),
            ),
            'PROPERTY_284' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_284_FIELD'),
            ),
            'PROPERTY_285' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_285_FIELD'),
            ),
            'PROPERTY_286' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_286_FIELD'),
            ),
            'PROPERTY_287' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_287_FIELD'),
            ),
            'PROPERTY_288' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_288_FIELD'),
            ),
            'PROPERTY_289' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_289_FIELD'),
            ),
            'PROPERTY_290' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_290_FIELD'),
            ),
            'PROPERTY_291' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_291_FIELD'),
            ),
            'PROPERTY_294' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_294_FIELD'),
            ),
            'PROPERTY_297' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_297_FIELD'),
            ),
            'PROPERTY_321' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_321_FIELD'),
            ),
            'PROPERTY_330' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_330_FIELD'),
            ),
            'PROPERTY_332' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_332_FIELD'),
            ),
            'PROPERTY_334' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_334_FIELD'),
            ),
            'PROPERTY_336' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_336_FIELD'),
            ),
            'PROPERTY_340' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_340_FIELD'),
            ),
            'PROPERTY_341' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_341_FIELD'),
            ),
            'PROPERTY_342' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_342_FIELD'),
            ),
            'PROPERTY_343' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_343_FIELD'),
            ),
            'PROPERTY_344' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_344_FIELD'),
            ),
            'PROPERTY_345' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_345_FIELD'),
            ),
            'PROPERTY_346' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_346_FIELD'),
            ),
            'PROPERTY_347' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_347_FIELD'),
            ),
            'PROPERTY_348' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_348_FIELD'),
            ),
            'PROPERTY_349' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_349_FIELD'),
            ),
            'PROPERTY_350' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_350_FIELD'),
            ),
            'PROPERTY_351' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_351_FIELD'),
            ),
            'PROPERTY_352' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_352_FIELD'),
            ),
            'PROPERTY_353' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_353_FIELD'),
            ),
            'PROPERTY_355' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_355_FIELD'),
            ),
            'PROPERTY_356' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_356_FIELD'),
            ),
            'PROPERTY_357' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_PROPERTY_357_FIELD'),
            ),
            'DESCRIPTION_357' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateDescription357'),
                'title' => Loc::getMessage('ELEMENT_PROP_S2_ENTITY_DESCRIPTION_357_FIELD'),
            ),
        );
    }
    /**
     * Returns validators for DESCRIPTION_357 field.
     *
     * @return array
     */
    public static function validateDescription357()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
}