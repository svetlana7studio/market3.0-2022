<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class ElementsFavoriteTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ELEMENT_ID int optional
 * <li> IBLOCK_ID int optional
 * <li> USER_ID int optional
 * </ul>
 *
 * @package Bitrix\Studio7spb
 **/

class ElementsFavoriteTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_elements_favorite';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('ELEMENTS_FAVORITE_ENTITY_ID_FIELD'),
            ),
            'ELEMENT_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('ELEMENTS_FAVORITE_ENTITY_ELEMENT_ID_FIELD'),
            ),
            'IBLOCK_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('ELEMENTS_FAVORITE_ENTITY_IBLOCK_ID_FIELD'),
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('ELEMENTS_FAVORITE_ENTITY_USER_ID_FIELD'),
            ),
        );
    }
}