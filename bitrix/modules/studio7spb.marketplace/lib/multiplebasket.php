<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class MultipleBasketTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> LID string(2) mandatory
 * <li> FUSER_ID int mandatory
 * <li> NAME string(255) optional
 * <li> PRODUCTS string(255) optional
 * <li> PARAMS string optional
 * </ul>
 *
 * @package Bitrix\Studio7spb
 **/

class MultipleBasketTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_multiple_basket';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('MULTIPLE_BASKET_ENTITY_ID_FIELD'),
            ),
            'LID' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateLid'),
                'title' => Loc::getMessage('MULTIPLE_BASKET_ENTITY_LID_FIELD'),
            ),
            'FUSER_ID' => array(
                'data_type' => 'integer',
                'required' => true,
                'title' => Loc::getMessage('MULTIPLE_BASKET_ENTITY_FUSER_ID_FIELD'),
            ),
            'NAME' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateName'),
                'title' => Loc::getMessage('MULTIPLE_BASKET_ENTITY_NAME_FIELD'),
            ),
            'PRODUCTS' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateProducts'),
                'title' => Loc::getMessage('MULTIPLE_BASKET_ENTITY_PRODUCTS_FIELD'),
            ),
            'PARAMS' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('MULTIPLE_BASKET_ENTITY_PARAMS_FIELD'),
            ),
        );
    }
    /**
     * Returns validators for LID field.
     *
     * @return array
     */
    public static function validateLid()
    {
        return array(
            new Main\Entity\Validator\Length(null, 2),
        );
    }
    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for PRODUCTS field.
     *
     * @return array
     */
    public static function validateProducts()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
}