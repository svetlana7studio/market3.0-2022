<?php
namespace Studio7spb\Marketplace\Import;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\StringField,
    Bitrix\Main\ORM\Fields\TextField,
    Bitrix\Main\ORM\Fields\Validators\LengthValidator;

Loc::loadMessages(__FILE__);

/**
 * Class ImportProfileListTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ACTIVE string(1) optional
 * <li> NAME string(255) optional
 * <li> SORT int optional
 * <li> IBLOCK_TYPE string(255) optional
 * <li> IBLOCK_ID int optional
 * <li> IBLOCK_SECTION_ID int optional
 * <li> FILE int optional
 * <li> FIELDS text optional
 * <li> SAMPLE text optional
 * <li> START int optional
 * <li> IDENT string(255) optional
 * <li> AVALIABLE_STRING string(255) optional
 * <li> SEPAR string(10) optional
 * <li> CUR string(1) optional
 * </ul>
 *
 * @package Bitrix\Studio7spb
 **/

class ImportProfileListTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_import_profile_list';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_ID_FIELD')
                ]
            ),
            new StringField(
                'ACTIVE',
                [
                    'validation' => [__CLASS__, 'validateActive'],
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_ACTIVE_FIELD')
                ]
            ),
            new StringField(
                'NAME',
                [
                    'validation' => [__CLASS__, 'validateName'],
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_NAME_FIELD')
                ]
            ),
            new IntegerField(
                'SORT',
                [
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_SORT_FIELD')
                ]
            ),
            new StringField(
                'IBLOCK_TYPE',
                [
                    'validation' => [__CLASS__, 'validateIblockType'],
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_IBLOCK_TYPE_FIELD')
                ]
            ),
            new IntegerField(
                'IBLOCK_ID',
                [
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_IBLOCK_ID_FIELD')
                ]
            ),
            new IntegerField(
                'IBLOCK_SECTION_ID',
                [
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_IBLOCK_SECTION_ID_FIELD')
                ]
            ),
            new IntegerField(
                'FILE',
                [
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_FILE_FIELD')
                ]
            ),
            new TextField(
                'FIELDS',
                [
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_FIELDS_FIELD')
                ]
            ),
            new TextField(
                'SAMPLE',
                [
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_SAMPLE_FIELD')
                ]
            ),
            new IntegerField(
                'START',
                [
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_START_FIELD')
                ]
            ),
            new StringField(
                'IDENT',
                [
                    'validation' => [__CLASS__, 'validateIdent'],
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_IDENT_FIELD')
                ]
            ),
            new StringField(
                'AVALIABLE_STRING',
                [
                    'validation' => [__CLASS__, 'validateAvaliableString'],
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_AVALIABLE_STRING_FIELD')
                ]
            ),
            new StringField(
                'SEPAR',
                [
                    'validation' => [__CLASS__, 'validateSepar'],
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_SEPAR_FIELD')
                ]
            ),
            new StringField(
                'CUR',
                [
                    'validation' => [__CLASS__, 'validateCur'],
                    'title' => Loc::getMessage('IMPORT_PROFILE_LIST_ENTITY_CUR_FIELD')
                ]
            ),
        ];
    }

    /**
     * Returns validators for ACTIVE field.
     *
     * @return array
     */
    public static function validateActive()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }

    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for IBLOCK_TYPE field.
     *
     * @return array
     */
    public static function validateIblockType()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for IDENT field.
     *
     * @return array
     */
    public static function validateIdent()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for AVALIABLE_STRING field.
     *
     * @return array
     */
    public static function validateAvaliableString()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for SEPAR field.
     *
     * @return array
     */
    public static function validateSepar()
    {
        return [
            new LengthValidator(null, 10),
        ];
    }

    /**
     * Returns validators for CUR field.
     *
     * @return array
     */
    public static function validateCur()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }
}