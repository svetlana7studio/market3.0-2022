<?php
namespace Studio7spb\Marketplace\Import;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\StringField,
    Bitrix\Main\ORM\Fields\TextField,
    Bitrix\Main\ORM\Fields\Validators\LengthValidator;

Loc::loadMessages(__FILE__);

/**
 * Class ImportSettingsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> NAME string(255) optional
 * <li> CODE string(255) optional
 * <li> VALUE text optional
 * </ul>
 *
 * @package Bitrix\Studio7spb
 **/

class ImportSettingsTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_import_settings';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => Loc::getMessage('IMPORT_SETTINGS_ENTITY_ID_FIELD')
                ]
            ),
            new StringField(
                'NAME',
                [
                    'validation' => [__CLASS__, 'validateName'],
                    'title' => Loc::getMessage('IMPORT_SETTINGS_ENTITY_NAME_FIELD')
                ]
            ),
            new StringField(
                'CODE',
                [
                    'validation' => [__CLASS__, 'validateCode'],
                    'title' => Loc::getMessage('IMPORT_SETTINGS_ENTITY_CODE_FIELD')
                ]
            ),
            new TextField(
                'VALUE',
                [
                    'title' => Loc::getMessage('IMPORT_SETTINGS_ENTITY_VALUE_FIELD')
                ]
            ),
        ];
    }

    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for CODE field.
     *
     * @return array
     */
    public static function validateCode()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * @param $worksheet_catalog
     * @param $profile
     * @return void
     */
    public static function worksheet($worksheet_catalog, $profile)
    {
        if($worksheet_catalog !== "0")
            self::shellWorksheetCatalog($profile); # shell/worksheet.catalog.php
        else
            self::shellWorksheet($profile);

        die;
    }

    protected static function shellWorksheetCatalog($profile)
    {
        AddMessage2Log("shellWorksheetCatalog");
    }

    protected static function shellWorksheet($profile)
    {
        # echo shell_exec('php ' . $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/studio7spb.marketplace/admin/import/shell/worksheet.php ' . $_SERVER["DOCUMENT_ROOT"]);
    }

    public static function getExcelData($document_root)
    {

    }
}