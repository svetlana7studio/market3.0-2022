<?
namespace Studio7spb\Marketplace;

use Bitrix\Main,
    Bitrix\Main\ORM\Data\DataManager;

/**
 * Class ImportPriceLog
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> STATUS string(1) optional
 * <li> DATA string optional
 * <li> NOTE string(255) optional
 * </ul>
 *
 * @package Studio7spb\Marketplace
 **/

class ImportPriceLogTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_import_price_log';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => "ID"
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('REQUISITS_ENTITY_USER_ID_FIELD'),
            ),
            'NOTE' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateNote'),
                'title' => "NOTE",
            )
        );
    }

    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateNote()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
}