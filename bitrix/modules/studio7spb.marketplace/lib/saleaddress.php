<?php
namespace Studio7spb\Marketplace;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class SaleAddressTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> PROFILE_ID int optional
 * <li> USER_ID int optional
 * <li> LOCATION int optional
 * <li> LOCATION_VAL string(255) optional
 * <li> ORDER_PROPS string(255) optional
 * </ul>
 *
 * @package Bitrix\Studio7spb
 **/

class SaleAddressTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_studio7spb_sale_address';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('SALE_ADDRESS_ENTITY_ID_FIELD'),
            ),
            'PROFILE_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SALE_ADDRESS_ENTITY_PROFILE_ID_FIELD'),
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SALE_ADDRESS_ENTITY_USER_ID_FIELD'),
            ),
            'LOCATION' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateLocation'),
                'title' => Loc::getMessage('SALE_ADDRESS_ENTITY_LOCATION_FIELD'),
            ),
            'LOCATION_VAL' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateLocationVal'),
                'title' => Loc::getMessage('SALE_ADDRESS_ENTITY_LOCATION_VAL_FIELD'),
            ),
            'ORDER_PROPS' => array(
                'data_type' => 'string',
                'validation' => array(__CLASS__, 'validateOrderProps'),
                'title' => Loc::getMessage('SALE_ADDRESS_ENTITY_ORDER_PROPS_FIELD'),
            ),
        );
    }
    /**
     * Returns validators for LOCATION field.
     *
     * @return array
     */
    public static function validateLocation()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for LOCATION_VAL field.
     *
     * @return array
     */
    public static function validateLocationVal()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
    /**
     * Returns validators for ORDER_PROPS field.
     *
     * @return array
     */
    public static function validateOrderProps()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }
}