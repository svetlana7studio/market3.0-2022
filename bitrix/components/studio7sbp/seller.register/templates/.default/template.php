<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<div class="s7sbp--marketplace--partner">

    <?if($arResult["isSuccess"]):?>
        <div class="s7sbp--marketplace--partner--description-success">
            <?$APPLICATION->IncludeFile(SITE_DIR."include/partner.success.php", Array(), Array("MODE" => "html", "NAME" => GetMessage("MP_PARTNER_REGISTER_INCLUDE_AREA")));?>
        </div>
    <?else:?>

        <div class="text-center mb3">

            <a href="/partner/" class="btn btn-danger px4">Зарегистрироваться как продавец</a>
            или
            <a href="/auth/registration/?mode=register" class="btn btn-danger px4">Зарегистрироваться как покупатель</a>

        </div>

        <div class="s7sbp--marketplace--partner--title"><?=GetMessage("MP_PARTNER_REGISTER_TITLE")?></div>

        <div class="s7sbp--marketplace--partner--description-top">
            <?$APPLICATION->IncludeFile(SITE_DIR."include/partner.description.php", Array(), Array("MODE" => "html", "NAME" => GetMessage("MP_PARTNER_REGISTER_INCLUDE_AREA")));?>
        </div>

        <div class="s7sbp--marketplace--partner--title pt-30"><?=GetMessage("MP_PARTNER_REGISTER_ORDER_TITLE")?></div>



        <div class="s7sbp--marketplace--partner--form">

            <?/*if(!empty($arResult["error"])):
                ShowError(implode("<br />", $arResult["error"]));
                echo "<br>";
            endif;*/?>

            <form method="post">
                <?=bitrix_sessid_post()?>

                <!-- company name -->
                <div class="form-control<?
                if(!empty($arResult["error"]["company_name"])){
                    echo " form-control-error";
                }elseif(
                    !empty($arResult["postFileds"]["company"]["name"]) &&
                    empty($arResult["error"]["company_name"])
                ){
                    echo " form-control-success";
                }
                ?>">
                    <label><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMPANY_NAME")?></label>
                    <input type="text"
                           name="company[name]"
                           value="<?=$arResult["postFileds"]["company"]["name"]?>">
                    <?if(!empty($arResult["error"]["company_name"])):?>
                        <i class="text-danger"><?=$arResult["error"]["company_name"]?></i>
                    <?endif;?>
                </div>

                <!-- company site -->
                <div class="form-control<?
                if(!empty($arResult["error"]["company_site"])){
                    echo " form-control-error";
                }elseif(
                    !empty($arResult["postFileds"]["company"]["site"]) &&
                    empty($arResult["error"]["company_site"])
                ){
                    echo " form-control-success";
                }
                ?>">
                    <label><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMPANY_SITE")?></label>
                    <input type="text" name="company[site]" value="<?=$arResult["postFileds"]["company"]["site"]?>">
                    <?if(!empty($arResult["error"]["company_site"])):?>
                        <i class="text-danger"><?=$arResult["error"]["company_site"]?></i>
                    <?endif;?>
                </div>

                <!-- company present -->
                <div class="form-control <?
                if(!empty($arResult["error"]["company_present"])){
                    echo " form-control-error";
                }elseif(
                    $arResult["postFileds"]["company"]["present"] > 0 &&
                    empty($arResult["error"]["company_present"])
                ){
                    echo " form-control-success";
                }
                ?>">
                    <label><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMP_PRESENT")?></label>
                    <select name="company[present]">
                        <option value="0">
                            <?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_CHOOSE")?>
                        </option>
                        <option value="20" <?=$arResult["postFileds"]["company"]["present"] == 20 ? "selected" : ""?>>
                            <?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMP_PRESENT_20")?>
                        </option>
                        <option value="21" <?=$arResult["postFileds"]["company"]["present"] == 21 ? "selected" : ""?>>
                            <?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMP_PRESENT_21")?>
                        </option>
                    </select>
                    <?if(!empty($arResult["error"]["company_present"])):?>
                        <i class="text-danger"><?=$arResult["error"]["company_present"]?></i>
                    <?endif;?>
                </div>

                <!-- company email -->
                <div class="form-control <?
                if(!empty($arResult["error"]["company_email"])){
                    echo " form-control-error";
                }elseif(
                    !empty($arResult["postFileds"]["company"]["email"]) &&
                    empty($arResult["error"]["company_email"])
                ){
                    echo " form-control-success";
                }
                ?>">
                    <label><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMPANY_EMAIL")?></label>
                    <input type="text"
                           name="company[email]"
                           value="<?=$arResult["postFileds"]["company"]["email"]?>">
                    <?if(!empty($arResult["error"]["company_email"])):?>
                        <i class="text-danger"><?=$arResult["error"]["company_email"]?></i>
                    <?endif;?>
                </div>

                <!-- company phone -->
                <div class="form-control <?
                if(!empty($arResult["error"]["company_phone"])){
                    echo " form-control-error";
                }elseif(
                    !empty($arResult["postFileds"]["company"]["phone"]) &&
                    empty($arResult["error"]["company_phone"])
                ){
                    echo " form-control-success";
                }
                ?>">
                    <label><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMPANY_PHONE")?></label>
                    <input type="text"
                           name="company[phone]"
                           value="<?=$arResult["postFileds"]["company"]["phone"]?>">
                    <?if(!empty($arResult["error"]["company_phone"])):?>
                        <i class="text-danger"><?=$arResult["error"]["company_phone"]?></i>
                    <?endif;?>
                </div>

                <p><b><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_PERSONAL_INFO")?></b></p>
                <br>

                <!-- contact name -->
                <div class="form-control <?
                if(!empty($arResult["error"]["contact_name"])){
                    echo " form-control-error";
                }elseif(
                    !empty($arResult["postFileds"]["contact"]["name"]) &&
                    empty($arResult["error"]["contact_name"])
                ){
                    echo " form-control-success";
                }
                ?>">
                    <label><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMPANY_CONTACT_NAME")?></label>
                    <input type="text" name="contact[name]" value="<?=$arResult["postFileds"]["contact"]["name"]?>">
                    <?if(!empty($arResult["error"]["contact_name"])):?>
                        <i class="text-danger"><?=$arResult["error"]["contact_name"]?></i>
                    <?endif;?>
                </div>

                <!-- contact lastname -->
                <div class="form-control<?
                if(!empty($arResult["error"]["contact_lastname"])){
                    echo " form-control-error";
                }elseif(
                    !empty($arResult["postFileds"]["contact"]["lastname"]) &&
                    empty($arResult["error"]["contact_lastname"])
                ){
                    echo " form-control-success";
                }
                ?>">
                    <label><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMPANY_CONTACT_LASTNAME")?></label>
                    <input type="text" name="contact[lastname]" value="<?=$arResult["postFileds"]["contact"]["lastname"]?>">
                    <?if(!empty($arResult["error"]["contact_lastname"])):?>
                        <i class="text-danger"><?=$arResult["error"]["contact_lastname"]?></i>
                    <?endif;?>
                </div>

                <!-- contact email -->
                <div class="form-control<?
                if(
                        !empty($arResult["error"]["contact_email"]) ||
                        !empty($arResult["error"]["contact_system"])
                ){
                    echo " form-control-error";
                }elseif(
                    !empty($arResult["postFileds"]["contact"]["email"]) &&
                    empty($arResult["error"]["contact_email"])
                ){
                    echo " form-control-success";
                }
                ?>">
                    <label><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMPANY_CONTACT_EMAIL")?></label>
                    <input type="text" name="contact[email]" value="<?=$arResult["postFileds"]["contact"]["email"]?>">
                    <?if(!empty($arResult["error"]["contact_email"])):?>
                        <i class="text-danger"><?=$arResult["error"]["contact_email"]?></i>
                    <?endif;?>
                    <?if(!empty($arResult["error"]["contact_system"])):?>
                        <i class="text-danger"><?=$arResult["error"]["contact_system"]?></i>
                    <?endif;?>
                </div>

                <!-- contact password -->
                <div class="form-control<?
                if(!empty($arResult["error"]["contact_password"])){
                    echo " form-control-error";
                }elseif(
                    !empty($arResult["postFileds"]["contact"]["password"]) &&
                    empty($arResult["error"]["contact_password"])
                ){
                    echo " form-control-success";
                }
                ?>">
                    <label><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMPANY_CONTACT_PASSWORD")?></label>
                    <input type="password" name="contact[password]" value="">
                    <?if(!empty($arResult["error"]["contact_password"])):?>
                        <i class="text-danger"><?=$arResult["error"]["contact_password"]?></i>
                    <?endif;?>
                </div>

                <!-- contact confirm password -->
                <div class="form-control<?
                if(!empty($arResult["error"]["contact_confirm_password"])){
                    echo " form-control-error";
                }elseif(
                    !empty($arResult["postFileds"]["contact"]["confirm_password"]) &&
                    empty($arResult["error"]["contact_confirm_password"])
                ){
                    echo " form-control-success";
                }
                ?>">
                    <label><?=GetMessage("MP_PARTNER_REGISTER_FORM_FIELD_COMPANY_CONTACT_CONFIRM_PASSWORD")?></label>
                    <input type="password" name="contact[confirm_password]" value="">
                    <?if(!empty($arResult["error"]["contact_confirm_password"])):?>
                        <i class="text-danger"><?=$arResult["error"]["contact_confirm_password"]?></i>
                    <?endif;?>
                </div>

                <div class="buttons">
                    <button type="submit" class="btn btn-danger px4" name="submit" value="send"><?=GetMessage("MP_PARTNER_REGISTER_FORM_SUBMIT")?></button>
                </div>

            </form>
        </div>
    <?endif;?>
</div>