<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
	\Bitrix\Main\Loader,
    Bitrix\Highloadblock;

Loc::loadMessages(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/studio7spb.marketplace/options.php");

if (!\Bitrix\Main\Loader::includeModule('studio7spb.marketplace')) {
	ShowError(Loc::getMessage('studio7spb.marketplace_MODULE_NOT_INSTALLED'));
	die();
}

class SellerRegister extends CBitrixComponent
{

    private $_countries;
	protected $error = array();
	protected $moduleOptionList = array();

	function __construct($component = null) {
		$this->loadModuleOptions();
		parent::__construct($component);
	}

	protected function loadModuleOptions() {
		$this->moduleOptionList = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOptionList();
	}

	/**
	 * Process incoming request
	 * @return void
	 */
	protected function processRequest()
	{
        $notNededFields = array(
            "country_new",
            "country_adequate"
        );

		if($this->request->isPost() && isset($this->request['submit']) && $this->request['submit'] == "send" && check_bitrix_sessid())
		{
			
			$postFields = $this->request->getPostList();

			foreach ($postFields["company"] as $keyPost => $companyField) {
			    if(in_array($keyPost, $notNededFields)){
			        continue;
                }
                if($keyPost == "country"){
                    if($companyField == 0){
                        $companyField = $postFields["company"]["country_new"];
                    }
                    if($companyField == 0 && empty($postFields["company"]["country_new"])){
                        $companyField = $postFields["company"]["country_adequate"];
                        $postFields["company"]["country_new"] = $companyField;
                    }

                    $postFields["company"]["country"] = $companyField;
                }

				if(strlen($companyField) <= 0) {
					$this->error["company_" . $keyPost] = Loc::getMessage("MP_SR_FIELD_IMPORTANT_MESSAGE", array("#FIELD#" => Loc::getMessage("MP_SR_FIELD_COMPANY_".\ToUpper($keyPost))));
				}
				if($keyPost == "email" && !\check_email($companyField)) {
					$this->error["company_" . $keyPost] = Loc::getMessage("MP_SR_FIELD_EMAIL_ERROR");
				}
				if($keyPost == "type" && $companyField <= 0){
                    $this->error["company_" . $keyPost] = Loc::getMessage("MP_SR_FIELD_IMPORTANT_MESSAGE", array("#FIELD#" => Loc::getMessage("MP_SR_FIELD_COMPANY_".\ToUpper($keyPost))));
                }
				if($keyPost == "present" && $companyField <= 0){
                    $this->error["company_" . $keyPost] = Loc::getMessage("MP_SR_FIELD_IMPORTANT_MESSAGE", array("#FIELD#" => Loc::getMessage("MP_SR_FIELD_COMPANY_".\ToUpper($keyPost))));
                }
			}

			foreach ($postFields["contact"] as $keyPost => $companyField) {
				if(strlen($companyField) <= 0) {
					$this->error["contact_" . $keyPost] = Loc::getMessage("MP_SR_FIELD_IMPORTANT_MESSAGE", array("#FIELD#" => Loc::getMessage("MP_SR_FIELD_CONTACT_".\ToUpper($keyPost))));
				}
				if($keyPost == "email") {
				    if(!\check_email($companyField)){
                        $this->error["contact_" . $keyPost] = Loc::getMessage("MP_SR_FIELD_EMAIL_VALID_ERROR");
                    }
				    else{
                        $user = \Bitrix\Main\UserTable::getList(array(
                            "select" => array("ID"),
                            "filter" => array("EMAIL" => trim($companyField))
                        ));
                        if($user->getSelectedRowsCount() > 0){
                            $this->error["contact_" . $keyPost] = Loc::getMessage("MP_SR_FIELD_EMAIL_EXIST_ERROR");
                        }
                    }
				}
			}

            // check password & confirmation
            if(empty($this->error["contact_password"])){
                if($postFields["contact"]["confirm_password"] == $postFields["contact"]["password"]){
                    $errors = (new \CUser)->CheckPasswordAgainstPolicy($postFields["contact"]["password"], \CUser::GetGroupPolicy([1]));
                    if(!empty($errors)){
                        $errors = implode("<br>", $errors);
                        $this->error["contact_password"] = $errors;
                        $this->error["contact_confirm_password"] = $errors;
                    }
                }
                else{
                    $this->error["contact_password"] = Loc::getMessage("MP_SR_FIELD_PASSWORD_VALID_ERROR");
                    $this->error["contact_confirm_password"] = Loc::getMessage("MP_SR_FIELD_PASSWORD_VALID_ERROR");
                }
            }

			$this->arResult["postFileds"] = $postFields;

			if(!empty($this->error)) return null;

			$oUser = new \CUser;
			$userFields = array(
				"NAME"  => $postFields["contact"]["name"],
				"LAST_NAME" => $postFields["contact"]["lastname"],
				"EMAIL" => $postFields["contact"]["email"],
				"LOGIN" => $postFields["contact"]["email"],
				"ACTIVE" => "Y",
				"GROUP_ID" => array(2, 3, 4, $this->moduleOptionList["seller_admin_group"]),
				"PASSWORD" => $postFields["contact"]["password"],
				"CONFIRM_PASSWORD" => $postFields["contact"]["confirm_password"]
			);

			$createdUserId = $oUser->Add($userFields);
			if ( (int)$createdUserId <= 0) {
				$this->error["contact_system"] = $oUser->LAST_ERROR;
				return null;
			}


			$translitParams = array("replace_space"=>"-","replace_other"=>"-");
			$translitName = \Cutil::translit($postFields["company"]["name"], "ru", $translitParams);

			if(strlen($postFields["company"]["country_new"]) > 2){
                $postFields["company"]["country"] = $this->addNewCountries($postFields["company"]["country_new"]);
            }

			$addFields = array(
				"IBLOCK_ID" => $this->moduleOptionList["company_iblock_id"],
				"NAME" => $postFields["company"]["name"],
				"IBLOCK_SECTION_ID" => null,
				"ACTIVE" => "N",
				"CODE" => time(),
				"PROPERTY_VALUES" => array(
				    "COMP_STATUS" => 23,
					"COMP_NAME_SHORT" => $postFields["company"]["name"],
					"COMP_SITE" => $postFields["company"]["site"],
					"COMP_EMAIL" => $postFields["company"]["email"],
					"COMP_PHONE" => $postFields["company"]["phone"],
					"COMP_TYPE" => $postFields["company"]["type"],
					"COMP_PRESENT" => $postFields["company"]["present"],
					"COMP_COUNTRY" => $postFields["company"]["country"],
					"COMP_USER" => array(
						"n0" => array(
							"VALUE" => $createdUserId
						)
					)
				)
			);

			Loader::includeModule("iblock");
			
			$oElement = new \CIBlockElement();
			if($createdElementId = $oElement->Add($addFields, false, false, true)) {

				$oUser->Update($createdUserId, array("UF_MP_COMPANY" => $createdElementId));
				
				$protocol = \Bitrix\Main\Config\Option::get("main", "mail_link_protocol", 'https', SITE_ID);
				if(!$protocol) {
					$protocol = "http";
				}

				$mailFields = array(
					"COMPANY_NAME" => $addFields["NAME"],
					"COMPANY_SITE" => $addFields["PROPERTY_VALUES"]["COMP_SITE"],
					"COMPANY_EMAIL" => $addFields["PROPERTY_VALUES"]["COMP_EMAIL"],
					"CONTACT_NAME" => $userFields["NAME"],
					"CONTACT_LASTNAME" => $userFields["LAST_NAME"],
					"CONTACT_EMAIL" => $userFields["EMAIL"],
					"MODERATE_LINK" => $protocol."://".SITE_SERVER_NAME."/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=".$addFields["IBLOCK_ID"]."/&type=marketplace&ID=".$createdElementId."&lang=ru&find_section_section=-1&WF=Y",
				);
				\CEvent::Send("S7SBP_MARKETPLACE_NEW_PARTNER", SITE_ID, $mailFields, "N");

				\localRedirect("/partner/?success=y");
			} else {
				$this->error["company_system"] = $oElement->LAST_ERROR;
				return null;
			}
		}
	}

	public function setCountriesFromDB() {
        Loader::includeModule("highloadblock");
        $countries = array();

        $ENTITY_ID = 3;
        $hlblock = Highloadblock\HighloadBlockTable::getById($ENTITY_ID)->fetch();
        $hlEntity = Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entDataClass = $hlEntity->getDataClass();
        $sTableID = 'tbl_'.$hlblock['TABLE_NAME'];

        $rsData = $entDataClass::getList(array(
            "select" => array(
                "ID",
                "UF_NAME"
            )
        ));
        $rsData = new CDBResult($rsData, $sTableID);
        while($arRes = $rsData->Fetch()){
            $countries[$arRes["ID"]] = $arRes["UF_NAME"];
        }


        $this->setCountries($countries);
    }

    /**
     * @return mixed
     */
    public function getCountries()
    {
        return $this->_countries;
    }

    /**
     * @param mixed $countries
     */
    public function setCountries($countries)
    {
        $this->_countries = $countries;
    }

    /**
     * @param mixed $countries
     */
    public function addNewCountries($country)
    {
        // country_new
        Loader::includeModule("highloadblock");
        $ID = 0;

        $ENTITY_ID = 3;
        $hlblock = Highloadblock\HighloadBlockTable::getById($ENTITY_ID)->fetch();
        $hlEntity = Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entDataClass = $hlEntity->getDataClass();
        $sTableID = 'tbl_'.$hlblock['TABLE_NAME'];

        $rsData = $entDataClass::getList(array(
            "filter" => array(
                "UF_NAME" => $country
            ),
            "select" => array(
                "ID"
            )
        ));
        $rsData = new CDBResult($rsData, $sTableID);
        if($arRes = $rsData->Fetch()){
            $ID = $arRes["ID"];
        }else{
            // add new
            $ID = 1;
            //$translitParams = array("replace_space"=>"-","replace_other"=>"-");
            //$translitName = \Cutil::translit($country, "ru", $translitParams);
            $result = $entDataClass::add(array(
                'UF_NAME' => $country,
                'UF_XML_ID' => $country
            ));

            if($result->isSuccess()){
                $ID = $result->getId();
            }

        }

        return $ID;
    }

    public function getAdequateCountries(){
        $countries = array("Абхазия", "Австралия", "Австрия", "Азербайджан", "Албания", "Алжир", "Ангола", "Ангуилья", "Андорра", "Антигуа и Барбуда", "Антильские о-ва", "Аргентина", "Армения", "Арулько", "Афганистан", "Багамские о-ва", "Бангладеш", "Барбадос", "Бахрейн", "Беларусь", "Белиз", "Бельгия", "Бенин", "Бермуды", "Болгария", "Боливия", "Босния/Герцеговина", "Ботсвана", "Бразилия", "Британские Виргинские о-ва", "Бруней", "Буркина Фасо", "Бурунди", "Бутан", "Валлис и Футуна о-ва", "Вануату", "Великобритания", "Венгрия", "Венесуэла", "Восточный Тимор", "Вьетнам", "Габон", "Гаити", "Гайана", "Гамбия", "Гана", "Гваделупа", "Гватемала", "Гвинея", "Гвинея-Бисау", "Германия", "Гернси о-в", "Гибралтар", "Гондурас", "Гонконг", "Гренада", "Гренландия", "Греция", "Грузия", "Дания", "Джерси о-в", "Джибути", "Доминиканская республика", "Египет", "Замбия", "Западная Сахара", "Зимбабве", "Израиль", "Индия", "Индонезия", "Иордания", "Ирак", "Иран", "Ирландия", "Исландия", "Испания", "Италия", "Йемен", "Кабо-Верде", "Казахстан", "Камбоджа", "Камерун", "Канада", "Катар", "Кения", "Кипр", "Кирибати", "Китай", "Колумбия", "Коморские о-ва", "Конго (Brazzaville)", "Конго (Kinshasa)", "Коста-Рика", "Кот-д''Ивуар", "Куба", "Кувейт", "Кука о-ва", "Кыргызстан", "Лаос", "Латвия", "Лесото", "Либерия", "Ливан", "Ливия", "Литва", "Лихтенштейн", "Люксембург", "Маврикий", "Мавритания", "Мадагаскар", "Македония", "Малави", "Малайзия", "Мали", "Мальдивские о-ва", "Мальта", "Марокко", "Мартиника о-в", "Мексика", "Мозамбик", "Молдова", "Монако", "Монголия", "Мьянма (Бирма)", "Мэн о-в", "Намибия", "Науру", "Непал", "Нигер", "Нигерия", "Нидерланды (Голландия)", "Никарагуа", "Новая Зеландия", "Новая Каледония о-в", "Норвегия", "Норфолк о-в", "О.А.Э.", "Оман", "Пакистан", "Панама", "Папуа Новая Гвинея", "Парагвай", "Перу", "Питкэрн о-в", "Польша", "Португалия", "Пуэрто Рико", "Реюньон", "Россия", "Руанда", "Румыния", "Сальвадор", "Самоа", "Сан-Марино", "Сан-Томе и Принсипи", "Саудовская Аравия", "Свазиленд", "Святая Люсия", "Святой Елены о-в", "Северная Корея", "Сейшеллы", "Сен-Пьер и Микелон", "Сенегал", "Сент-Винсент и Гренадины", "Сент Китс и Невис", "Сербия", "Сингапур", "Сирия", "Словакия", "Словения", "Соломоновы о-ва", "Сомали", "Судан", "Суринам", "США", "Сьерра-Леоне", "Таджикистан", "Таиланд", "Тайвань", "Танзания", "Того", "Токелау о-ва", "Тонга", "Тринидад и Тобаго", "Тувалу", "Тунис", "Туркменистан", "Туркс и Кейкос", "Турция", "Уганда", "Узбекистан", "Украина", "Уругвай", "Фарерские о-ва", "Фиджи", "Филиппины", "Финляндия", "Франция", "Французская Гвинея", "Французская Полинезия", "Хорватия", "Чад", "Черногория", "Чехия", "Чили", "Швейцария", "Швеция", "Шри-Ланка", "Эквадор", "Экваториальная Гвинея", "Эритрея", "Эстония", "Эфиопия", "ЮАР", "Южная Корея", "Южная Осетия", "Ямайка", "Япония");
        return $countries;
    }

	public function executeComponent() {
		$this->setCountriesFromDB();
        $this->arResult["COMP_COUNTRY"]["ITEMS"] = $this->getCountries();
        $this->arResult["COMP_COUNTRY"] = array(
            "ITEMS" => $this->getCountries(),
            "ADEQUATE" => $this->getAdequateCountries()
        );
		$this->processRequest();

		if($this->request['success'] == "y") {
			$this->arResult["isSuccess"] = true;
		}

		if(!empty($this->error)) {
			$this->arResult["error"] = $this->error;
		}

		$this->includeComponentTemplate();
	}
}