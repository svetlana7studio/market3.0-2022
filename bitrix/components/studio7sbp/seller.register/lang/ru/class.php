<?
$MESS["SPS_CHAIN_MAIN"] = "Мой кабинет";
$MESS["MP_SR_FIELD_IMPORTANT_MESSAGE"] = "Поле \"#FIELD#\" обязательно для заполнения";
$MESS["MP_SR_FIELD_EMAIL_ERROR"] = "Не верно указан E-mail";
$MESS["MP_SR_FIELD_EMAIL_VALID_ERROR"] = "Указан не коректный E-mail адрес";
$MESS["MP_SR_FIELD_EMAIL_EXIST_ERROR"] = "Пользователь с таким E-mail адресом уже существует в системе";

$MESS["MP_SR_FIELD_COMPANY_NAME"] = "Название компании для покупателей";
$MESS["MP_SR_FIELD_COMPANY_SITE"] = "Сайт";
$MESS["MP_SR_FIELD_COMPANY_EMAIL"] = "E-mail вендора";
$MESS["MP_SR_FIELD_COMPANY_PHONE"] = "Телефон вендора";
$MESS["MP_SR_FIELD_COMPANY_TYPE"] = "Тип вендора";
$MESS["MP_SR_FIELD_COMPANY_PRESENT"] = "Кого вы представляете";
$MESS["MP_SR_FIELD_COMPANY_COUNTRY"] = "Страна происхождения товара";

$MESS["MP_SR_FIELD_CONTACT_NAME"] = "Имя";
$MESS["MP_SR_FIELD_CONTACT_LASTNAME"] = "Фамилия";
$MESS["MP_SR_FIELD_CONTACT_EMAIL"] = "E-mail";
$MESS["MP_SR_FIELD_CONTACT_PASSWORD"] = "Пароль";
$MESS["MP_SR_FIELD_CONTACT_CONFIRM_PASSWORD"] = "Подтверждение пароля";
$MESS["MP_SR_FIELD_PASSWORD_VALID_ERROR"] = "Пароль и подтверждение пароля должны совпадать";
?>