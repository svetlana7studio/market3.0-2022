<?
/**
 * seller_register
 * seller register
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "seller register",
    "DESCRIPTION" => "seller register",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "seller_register",
            "NAME" => "seller register"
        )
    ),
);
?>