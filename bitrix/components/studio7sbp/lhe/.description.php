
/**
 * lhe
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => "lhe",
    "DESCRIPTION" => "lhe",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "lhe",
            "NAME" => "lhe"
        )
    ),
);