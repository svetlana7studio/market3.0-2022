<?
/**
 * shop_user_message_list
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "shop_user_message_list",
	"DESCRIPTION" => "shop_user_message_list",
	"ICON" => "/images/icon.gif",
	"PATH" => array(
		"ID" => "studio7sbp",
		"CHILD" => array(
			"ID" => "shop_user_message_list",
			"NAME" => "shop_user_message_list"
		)
	),
);