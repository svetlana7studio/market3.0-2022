<?
/**
 * @var array $arParams
 * @var array $arResult
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
?>
<div class="s7sbp--marketplace--saler--lk--right--inner">
    <div class="s7sbp--marketplace--saler--lk--title ff--roboto"><?=Loc::getMessage("SHOP_USER_MESSAGES")?></div>

    <?if(empty($arResult["MESSAGES"])):?>
        <div class="alert alert-danger"><?=Loc::getMessage("SHOP_USER_MESSAGES_EMPTY")?></div>
    <?else:?>
        <table class="table">
            <tr>
                <?foreach ($arResult["FIELDS"] as $code=>$arField):?>
                    <td><?=$arField["title"]?></td>
                <?endforeach;?>
            </tr>
            <?foreach ($arResult["MESSAGES"] as $code=>$arMessage):?>
                <tr>
                    <?foreach ($arMessage as $key=>$message):?>
                        <td><?

                            if($key == "DATETIME"){
                                echo $message->toString();
                            }else{
                                echo $message;
                            }

                            ?></td>
                    <?endforeach;?>
                </tr>
            <?endforeach;?>
        </table>
    <?endif;?>

</div>
