<?
use Studio7spb\Marketplace\ShopMessagesTable;

class shopUserMessageList extends CBitrixComponent
{

    private $_shop;
    private $_messages;
    private $_fields;


    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->_messages;
    }

    /**
     * Set messages from db
     */
    public function setMessages()
    {
        $shop = $this->getShop();
        $shop = $shop["ID"];
        if($shop > 0){
            $messages = ShopMessagesTable::getList(array(
                "select" => $this->arParams["SELECT"],
                "filter" => array(
                    "ELEMENT_ID" => $shop
                )
            ));
            while ($message = $messages->fetch()){
                $this->_messages[] = $message;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getFields()
    {
        return $this->_fields;
    }

    /**
     * Set Fields from ORM
     */
    public function setFields()
    {
        $this->_fields = ShopMessagesTable::getMap();

        foreach ($this->_fields as $code=>$arField){
            if(!in_array($code, $this->arParams["SELECT"])){
                unset($this->_fields[$code]);
            }
        }

    }

    /**
     * set shop from iblock
     */
    public function setShop()
    {
        global $USER;
        $element = array("ID");
        $element = CIBlockElement::GetList(array(), array(
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
            "PROPERTY_COMP_USER" => $USER->GetID()
        ), false,
            false,
            $element
        );
        if($element = $element->Fetch()){
            $this->_shop = $element;
        }
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->_shop;
    }

    public function executeComponent()
    {
        if($this->startResultCache()){
            $this->setShop();
            $this->setFields();
            $this->setMessages();
            $this->endResultCache();
        }

        if(!empty($this->getShop())){
            $this->arResult["FIELDS"] = $this->getFields();
            $this->arResult["MESSAGES"] = $this->getMessages();
        }

        $this->includeComponentTemplate();
    }
}