<?
use Bitrix\Main\Loader;

class bestsellerList extends CBitrixComponent
{

    private $_superPrice;
    private $_dayDiscont;

    /**
     * @return mixed
     */
    public function getSuperPrice()
    {
        return $this->_superPrice;
    }

    /**
     * @param mixed $superPrice
     */
    public function setSuperPrice($superPrice)
    {
        $this->_superPrice = $superPrice;
    }

    public function setSuperPriceFromDb()
    {
        $superPrice = [];
        $select = [
            "ID",
            "NAME",
            "PREVIEW_PICTURE",
            "DETAIL_PAGE_URL"
        ];
        $element = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_TYPE" => $this->arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                "ACTIVE" => "Y",
                "!PROPERTY_SUPER_PRICE" => false
            ],
            false,
            ["nTopCount" => 1],
            $select
        );
        if($element = $element->GetNext())
        {
            // pictures
            $element["PREVIEW_PICTURE"] = $this->formatPicture($element["PREVIEW_PICTURE"]);
            // tp prices
            $select = [
                "ID",
                "IBLOCK_ID"
            ];
            if(!empty($this->arParams["PRICE_CODE"])){
                foreach ($this->arParams["PRICE_CODE"] as $code){
                    $select[] = "CATALOG_GROUP_" . $code;
                }
            }
            Loader::includeModule("sale");
            $products = CIBlockElement::GetList(
                [
                    "CATALOG_PRICE_1" => "asc"
                ],
                [
                    "IBLOCK_TYPE" => $this->arParams["TP_IBLOCK_TYPE"],
                    "IBLOCK_ID" => $this->arParams["TP_IBLOCK_ID"],
                    "ACTIVE" => "Y",
                    "PROPERTY_CML2_LINK" => $element["ID"]
                ],
                false,
                ["nTopCount" => 1],
                $select
            );
            if($product = $products->Fetch())
            {

                if(!empty($this->arParams["PRICE_CODE"])){
                    foreach ($this->arParams["PRICE_CODE"] as $code){
                        $product["CATALOG_PRICE_" . $code . "_DISPLAY"] = SaleFormatCurrency(
                            $product["CATALOG_PRICE_" . $code],
                            $product["CATALOG_CURRENCY_" . $code]
                        );
                    }
                }


                $element["PRODUCT"] = $product;
            }
            $superPrice = $element;
        }
        $this->setSuperPrice($superPrice);
    }

    /**
     * @return mixed
     */
    public function getDayDiscont()
    {
        return $this->_dayDiscont;
    }

    /**
     * @param mixed $dayDiscont
     */
    public function setDayDiscont($dayDiscont)
    {
        $this->_dayDiscont = $dayDiscont;
    }

    public function setDayDiscontFromDb()
    {
        $dayDiscont = [];
        $select = [
            "ID",
            "NAME",
            "PREVIEW_PICTURE",
            "DETAIL_PAGE_URL"
        ];
        $elements = CIBlockElement::GetList(
            ["ID" => "ASC"],
            [
                "IBLOCK_TYPE" => $this->arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                "ACTIVE" => "Y",
                "!PROPERTY_DISCONT_DAY" => false
            ],
            false,
            [
                "nTopCount" => $this->arParams["DAY_DISCONT_COUNT"]
            ],
            $select
        );
        while($element = $elements->GetNext())
        {
            // pictures
            $element["PREVIEW_PICTURE"] = $this->formatPicture($element["PREVIEW_PICTURE"]);

            // tp prices
            $select = [
                "ID",
                "NAME",
                "IBLOCK_ID"
            ];
            if(!empty($this->arParams["PRICE_CODE"])){
                foreach ($this->arParams["PRICE_CODE"] as $code){
                    $select[] = "CATALOG_GROUP_" . $code;
                }
            }
            Loader::includeModule("sale");
            
            $products = CIBlockElement::GetList(
                [
                    "CATALOG_PRICE_1" => "asc"
                ],
                [
                    "IBLOCK_TYPE" => $this->arParams["TP_IBLOCK_TYPE"],
                    "IBLOCK_ID" => $this->arParams["TP_IBLOCK_ID"],
                    "ACTIVE" => "Y",
                    "PROPERTY_CML2_LINK" => $element["ID"]
                ],
                false,
                ["nTopCount" => 1],
                $select
            );
            if($product = $products->Fetch())
            {

                if(!empty($this->arParams["PRICE_CODE"])){
                    foreach ($this->arParams["PRICE_CODE"] as $code){
                        $product["CATALOG_PRICE_" . $code . "_DISPLAY"] = SaleFormatCurrency(
                            $product["CATALOG_PRICE_" . $code],
                            $product["CATALOG_CURRENCY_" . $code]
                        );
                    }
                }


                $element["PRODUCT"] = $product;
            }
            $dayDiscont[] = $element;
        }
        $this->setDayDiscont($dayDiscont);
    }

    /**
     * @param $PREVIEW_PICTURE
     * @return array|bool|mixed
     */
    public function formatPicture($PREVIEW_PICTURE)
    {

        if($PREVIEW_PICTURE > 0){

            if(is_int($this->arParams["RESIZE"]) && $this->arParams["RESIZE"] > 0){
                $PREVIEW_PICTURE = CFile::ResizeImageGet(
                    $PREVIEW_PICTURE,
                    [
                        "width" => $this->arParams["RESIZE"]["width"],
                        "height" => $this->arParams["RESIZE"]["height"]
                    ],
                    BX_RESIZE_IMAGE_EXACT,
                    false
                );
                $PREVIEW_PICTURE = [
                    "SRC" => $PREVIEW_PICTURE["src"],
                    "WIDTH" => $this->arParams["RESIZE"],
                    "HEIGHT" => $this->arParams["RESIZE"],
                ];
            }elseif($this->arParams["RESIZE"]["width"] > 0 && $this->arParams["RESIZE"]["height"] > 0){
                $PREVIEW_PICTURE = CFile::ResizeImageGet(
                    $PREVIEW_PICTURE,
                    [
                        "width" => $this->arParams["RESIZE"]["width"],
                        "height" => $this->arParams["RESIZE"]["height"]
                    ],
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true
                );
                $PREVIEW_PICTURE = [
                    "SRC" => $PREVIEW_PICTURE["src"],
                    "WIDTH" => $PREVIEW_PICTURE["width"],
                    "HEIGHT" => $PREVIEW_PICTURE["height"],
                ];
            }else{
                $PREVIEW_PICTURE = CFile::GetFileArray($PREVIEW_PICTURE);
            }

        }

        return $PREVIEW_PICTURE;
    }

    public function executeComponent()
    {
        if($this->startResultCache()){
            if(Loader::includeModule("iblock")){
                $this->setSuperPriceFromDb();
                $this->setDayDiscontFromDb();
            }
            $this->endResultCache();
        }

        $this->arResult = [
            "SUPER_PRICE" => $this->getSuperPrice(),
            "DAY_DISCONT" => $this->getDayDiscont()
        ];

        $this->includeComponentTemplate();
    }
}