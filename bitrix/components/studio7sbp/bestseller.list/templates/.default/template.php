<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/catalog/top.item.css");
?>



<div class="s7spb-row s7spb-row-wrap s7spb-row12 s7spb-col600">

    <?if(!empty($arResult["SUPER_PRICE"])):?>
        <div class="s7spb-col">
            <div class="item item-superprice">
                <div class="item-header bg-warning text-black">
                    <?=Loc::getMessage("BESTSELLER_SUPER_PRICE_TITLE")?>
                </div>
                <div class="item-body">
                    <a href="<?=$arResult["SUPER_PRICE"]["DETAIL_PAGE_URL"]?>" class="item-img">
                        <?if(empty($arResult["SUPER_PRICE"]["PREVIEW_PICTURE"])):?>

                        <?else:?>
                            <img src="<?=$arResult["SUPER_PRICE"]["PREVIEW_PICTURE"]["SRC"]?>"
                                 width="<?=$arResult["SUPER_PRICE"]["PREVIEW_PICTURE"]["WIDTH"]?>"
                                 height="<?=$arResult["SUPER_PRICE"]["PREVIEW_PICTURE"]["HEIGHT"]?>"
                                 class="img-responsive" />
                        <?endif;?>
                    </a>
                    <a href="<?=$arResult["SUPER_PRICE"]["DETAIL_PAGE_URL"]?>" class="item-title">
                        <?=$arResult["SUPER_PRICE"]["NAME"]?>
                    </a>
                </div>
                <div class="item-footer pb3">
                    <div class="item-price-current">145 000 &#8381; </div>
                    <div class="item-price-old">
                        <div class="item-price-old-value">150 000 &#8381;</div>
                        <div class="item-price-old-percent">-3%</div>
                    </div>
                </div>
            </div>
        </div>
    <?endif;?>

    <?
    if(!empty($arResult["DAY_DISCONT"])):
        $i = 0;
        foreach ($arResult["DAY_DISCONT"] as $arItem):
            $i++;
            $arItem["CSS"] = "item";
    ?>
            <div class="s7spb-col">
                <div class="<?=$arItem["CSS"]?>">
                    <div class="item-header">
                        <?=Loc::getMessage("BESTSELLER_DAY_DISCONT_TITLE")?>
                    </div>
                    <div class="item-body text-center">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-img">
                            <?if(empty($arItem["PREVIEW_PICTURE"])):?>

                            <?else:?>
                                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                                     width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                                     height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                                     class="img-responsive" />
                            <?endif;?>
                        </a>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="item-title">
                            <?=$arItem["NAME"]?>
                        </a>
                    </div>
                    <div class="item-footer pb3">
                        <div class="item-price-current">145 000 &#8381; </div>
                        <div class="item-price-old">
                            <div class="item-price-old-value">150 000 &#8381;</div>
                            <div class="item-price-old-percent">-3%</div>
                        </div>
                    </div>
                </div>
            </div>
        <?
        endforeach;
    endif;
    ?>

</div>