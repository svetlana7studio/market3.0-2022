<?
/**
 * bestseller_list
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "bestseller_list",
    "DESCRIPTION" => "bestseller_list",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "bestseller_list",
            "NAME" => "bestseller_list"
        )
    ),
);