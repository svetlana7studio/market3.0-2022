<?
use Bitrix\Main\Loader,
    Bitrix\Iblock\ElementTable;

class anytosBrandDetail extends CBitrixComponent
{

    public function executeComponent()
    {
        $brend = $this->arParams["ID"];

        if($brend <= 0){
            if($this->startResultCache()) {
                Loader::includeModule("iblock");
                $brend = ElementTable::getList([
                    "filter" => [
                        "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                        "CODE" => $this->arParams["CODE"]
                    ],
                    "select" => ["ID", "NAME"]
                ]);
                $brend = $brend->fetch();
                //$this->endResultCache();
            }
        }

        return $brend;
    }
}