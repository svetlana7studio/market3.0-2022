<?
/**
 * anytos_brand_detail
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "anytos_brand_detail",
    "DESCRIPTION" => "anytos_brand_detail",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "anytos_brand_detail",
            "NAME" => "anytos_brand_detail"
        )
    ),
);