<?
/**
 * anytos_aktsy_detail
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "anytos_aktsy_detail",
    "DESCRIPTION" => "anytos_aktsy_detail",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "anytos_aktsy_detail",
            "NAME" => "anytos_aktsy_detail"
        )
    ),
);