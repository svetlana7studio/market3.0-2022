<?
use Bitrix\Main\Loader,
    Studio7spb\Marketplace\ElementPropertyTable,
    Bitrix\Iblock\ElementTable;

class anytosAktsyDetail extends CBitrixComponent
{

    /**
     * @var array
     */
    private $_aktsy;

    /**
     * @var array
     */
    private $_products;

    /**
     * @var array
     */
    private $_productsSection;

    /**
     * @var
     */
    private $_vendor;

    /**
     * @return mixed
     */
    public function getVendor()
    {
        return $this->_vendor;
    }

    /**
     * @param mixed $vendor
     */
    public function setVendor($vendor)
    {
        $this->_vendor = $vendor;
    }

    public function setVendorFromIB()
    {
        $vendors = [];

        if($this->_aktsy["ID"] > 0 && $this->arParams["PROPERTY_VENDOR_ARTICLE"] > 0){

            $db = ElementPropertyTable::getList([
                "select" => ["VALUE"],
                //"limit" => 1,
                "filter" => [
                    "IBLOCK_ELEMENT_ID" => $this->_aktsy["ID"],
                    "IBLOCK_PROPERTY_ID" => $this->arParams["PROPERTY_VENDOR_ARTICLE"]
                ]
            ]);
            while($section = $db->fetch())
            {
                $section["VALUE"] = CIBlockPropertyEnum::GetByID($section["VALUE"]);
                $section["VALUE"] = $section["VALUE"]["XML_ID"];
                $vendors[] = $section["VALUE"];
            }
        }

        $this->setVendor($vendors);
    }

    /**
     * @return int
     */
    public function getProductsSection()
    {
        return $this->_productsSection;
    }

    /**
     * @param int $productsSection
     */
    public function setProductsSection($productsSection)
    {
        $this->_productsSection = $productsSection;
    }

    /**
     * Set Products Section from DB
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function setProductsSectionFromDB()
    {
        $productsSection = [];

        if($this->_aktsy["ID"] > 0 && $this->arParams["PROPERTY_SECTION"] > 0){

            $db = ElementPropertyTable::getList([
                "select" => ["VALUE"],
                //"limit" => 1,
                "filter" => [
                    "IBLOCK_ELEMENT_ID" => $this->_aktsy["ID"],
                    "IBLOCK_PROPERTY_ID" => $this->arParams["PROPERTY_SECTION"]
                ]
            ]);
            while($section = $db->fetch())
            {
                $productsSection[] = $section["VALUE"];
            }
        }



        $this->setProductsSection($productsSection);
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->_products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->_products = $products;
    }

    /**
     * Set Products From DB
     */
    public function setAkstyProductsFromDB()
    {
        $products = [];

        if($this->_aktsy["ID"] > 0 && $this->arParams["PROPERTY_PRODUCTS"] > 0){

            $db = ElementPropertyTable::getList([
                "select" => ["VALUE"],
                "filter" => [
                    "IBLOCK_ELEMENT_ID" => $this->_aktsy["ID"],
                    "IBLOCK_PROPERTY_ID" => $this->arParams["PROPERTY_PRODUCTS"]
                ]
            ]);
            while ($product = $db->fetch())
            {
                $products[] = $product["VALUE"];
            }
        }


        $this->setProducts($products);
    }

    /**
     * @return array
     */
    public function getAktsy()
    {
        return $this->_aktsy;
    }

    /**
     * @param array $aktsy
     */
    public function setAktsy($aktsy)
    {
        $this->_aktsy = $aktsy;
    }

    /**
     * Set aktsy from DB
     */
    public function setAktsyFromDB()
    {
        $aktsy = ElementTable::getList([
            "filter" => [
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                "ID" => $this->arParams["ID"]
            ],
            "select" => [
                "ID",
                "NAME",
                "PREVIEW_TEXT",
                "DETAIL_TEXT",
            ],
        ]);
        if($aktsy = $aktsy->fetch())
        {
            $this->setAktsy($aktsy);
        }
    }

    /**
     * Load libs
     */
    public function Load()
    {
        Loader::includeModule("iblock");
        Loader::registerAutoLoadClasses("studio7spb.marketplace", [
            "\\Studio7spb\\Marketplace\\ElementPropertyTable" => "lib/anytos/elementpropertytable.php"
        ]);
    }

    /**
     * Execute Component
     * @return array|\Bitrix\Main\ORM\Query\Result|false|mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent()
    {

        if($this->arParams["ID"] > 0){
            if($this->startResultCache()) {

                $this->Load();

                $this->setAktsyFromDB();

                if($this->arParams["PROPERTY_PRODUCTS"] > 0){
                    $this->setAkstyProductsFromDB();
                }

                if($this->arParams["PROPERTY_SECTION"] > 0){
                    $this->setProductsSectionFromDB();
                }

                if($this->arParams["PROPERTY_VENDOR_ARTICLE"] > 0){
                    $this->setVendorFromIB();
                }

                //$this->endResultCache();

            }
        }

        return [
            "ELEMENT" => $this->getAktsy(),
            "PRODUCTS" => $this->getProducts(),
            "PRODUCTS_SECTION" => $this->getProductsSection(),
            "VENDOR" => $this->getVendor()
        ];
    }
}