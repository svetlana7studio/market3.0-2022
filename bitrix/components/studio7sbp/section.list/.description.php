<?
/**
 * section_list_element_prop_brand
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "section_list_element_prop_brand",
    "DESCRIPTION" => "section_list_element_prop_brand",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "section_list_element_prop_brand",
            "NAME" => "section_list_element_prop_brand"
        )
    ),
);
