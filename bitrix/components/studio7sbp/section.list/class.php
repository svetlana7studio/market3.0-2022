<?
use Bitrix\Main\Loader,
    Bitrix\Iblock\SectionTable;

class sectionList extends CBitrixComponent
{
    private $_sections;

    /**
     * @return mixed
     */
    public function getSections()
    {
        return $this->_sections;
    }

    /**
     * @param mixed $sections
     */
    public function setSections($sections)
    {
        $this->_sections = $sections;
    }

    public function setSectionsD7()
    {
        $arSections = [];
        if(Loader::includeModule("iblock")){
            $sections = SectionTable::getList([
                "filter" => [
                    "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                    "ID" => $this->arParams["ID"],
                    "ACTIVE" => "Y"
                ],
                "select" => [
                    "ID",
                    "NAME"
                ]
            ]);
            while ($section = $sections->fetch())
            {
                $arSections[] = $section;
            }
        }

        $this->setSections($arSections);
    }

    public function setSectionsOld()
    {
        $arSections = [];
        if(Loader::includeModule("iblock")){
            $sections = CIBlockSection::GetList(
                [$this->arParams["ORDER_SORT_1"] => $this->arParams["ORDER_BY_1"]],
                [
                    "IBLOCK_TYPE" => $this->arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                    "ID" => $this->arParams["ID"],
                    "ACTIVE" => "Y"
                ],
                ($this->arParams["CNT"] === "Y" ? true : false),
                [
                    "ID",
                    "IBLOCK_ID",
                    "NAME",
                    "SECTION_PAGE_URL",
                ]
            );
            while ($section = $sections->GetNext())
            {
                $arSections[] = $section;
            }
        }

        $this->setSections($arSections);
    }

    public function executeComponent()
    {
        if($this->startResultCache()) {

            switch ($this->arParams["CORE"]){
                case "D7":
                    $this->setSectionsD7();
                    break;
                default:
                    $this->setSectionsOld();
            }

            $this->endResultCache();
        }
        $this->arResult["SECTIONS"] = $this->getSections();
        $this->includeComponentTemplate();
    }
}