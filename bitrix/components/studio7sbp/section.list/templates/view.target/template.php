<?
/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */
$this->setFrameMode(true);
$this->addExternalCss(SITE_TEMPLATE_PATH . "/components/bitrix/menu/one.level/style.css");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/components/bitrix/menu/one.level/script.js");

$this->SetViewTarget('sections.menu');
?>
    <div class="s7sbp--marketplace--index--catalog--menu mb3">
        <?if($arParams["TITLE"]):?>
            <div class="s7sbp--marketplace--index--catalog--menu--title"><?=$arParams["TITLE"]?></div>
        <?endif;?>
        <ul class="s7sbp--marketplace--index--catalog--menu--list">
            <?
            foreach( $arResult["SECTIONS"] as $i=>$arItem ):
                $arItem["SECTION_PAGE_URL"] = $arParams["CURRENT_BASE_PAGE"] . $arItem["ID"] . "/";
                $arItem["CLASS"] = "full s7sbp--marketplace--index--catalog--menu--list--item";
                //$arItem["CLASS"] .= " active";
            ?>
                <li class="<?=$arItem["CLASS"]?>">
                    <a class="s7sbp--marketplace--index--catalog--menu--list--item--link" href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                </li>
            <?endforeach;?>
        </ul>
    </div>
<?
$this->EndViewTarget();

