<?
/**
 * section_list_element_prop_discont
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "section_list_element_prop_discont",
    "DESCRIPTION" => "section_list_element_prop_discont",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "section_list_element_prop_discont",
            "NAME" => "section_list_element_prop_discont"
        )
    ),
);
