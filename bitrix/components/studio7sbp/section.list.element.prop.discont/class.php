<?

use Studio7spb\Marketplace\SectionElementTable;
use Studio7spb\Marketplace\SectionPropertyTable;

class sectionListElementPropDiscont extends CBitrixComponent
{
    private $_parentSections;

    /**
     * @return mixed
     */
    public function getParentSections()
    {
        return $this->_parentSections;
    }

    /**
     * @param mixed $parentSections
     */
    public function setParentSections($parentSections)
    {
        $this->_parentSections = $parentSections;
    }

    public function setParentSectionsORM()
    {
        $parentSections = [];
        CModule::AddAutoloadClasses(
            "studio7spb.marketplace",
            array(
                "\\Studio7spb\\Marketplace\\SectionPropertyTable" => "lib/anytos/sectionpropertytable.php"
            )
        );

        $sections = SectionPropertyTable::getList([
            "filter" => [
                "DISCOUNT" => $this->arParams["DISCOUNT"]
            ],
            "select" => [
                "SECTION_ID"
            ]
        ]);

        while ($section = $sections->fetch())
        {
            $parentSections[] = $section["SECTION_ID"];
        }
        
        $this->setParentSections($parentSections);
    }


    public function executeComponent()
    {
        if($this->startResultCache()) {
            $this->setParentSectionsORM();
            //$this->endResultCache();
        }
            return $this->getParentSections();
    }
}