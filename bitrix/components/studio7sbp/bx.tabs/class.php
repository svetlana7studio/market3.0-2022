<?
/**
 *
 */

use Bitrix\Main\Application;

class studio7sbpBxTabs extends CBitrixComponent
{



    /**
     * En execute
     * @return bool|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {

        $request = Application::getInstance()->getContext()->getRequest();

        if($request->get($this->arParams["REQUEST_PARAM"])){
            $this->arResult["ACTIVE_TAB"] = $request->get($this->arParams["REQUEST_PARAM"]);
        }
        else{
            $this->arResult["ACTIVE_TAB"] = $this->arParams["TABS"][0]["CODE"];
        }

        $this->includeComponentTemplate();

    }
}