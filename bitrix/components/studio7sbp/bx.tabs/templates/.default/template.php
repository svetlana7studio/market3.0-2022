<div class="s7sbp--marketplace--catalog-element-detail-product--tabs">
    <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--header">
        <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--header--item active" data-tabname="about">Описание товара</div>
        <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--header--item" data-tabname="reviews">Отзывы</div>
        <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--header--item" data-tabname="delivery">Доставка и оплата</div>
        <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--header--item" data-tabname="garanty">Гарантии продавца</div>
    </div>
    <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body">

        <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body--item active" data-tabname="about">

        </div>

        <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body--item" data-tabname="video">
        </div>

        <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body--item" data-tabname="delivery">
            <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body--item--title">Доставка и оплата</div>
        </div>

        <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body--item" data-tabname="garanty">
            <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body--item--title">Гарантии продавца</div>
        </div>

    </div>
</div>