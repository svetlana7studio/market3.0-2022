<?
/**
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
?>

<div class="s7sbp--marketplace--catalog-element-detail-product--tabs mb3" id="catalog-element-detail-product--tabs">

    <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--header">
        <?foreach ($arParams["TABS"] as $arTab):?>
            <a href="<?=$APPLICATION->GetCurPageParam( $arParams["REQUEST_PARAM"] . "=" . $arTab["CODE"], [$arParams["REQUEST_PARAM"]])?>"
               class="s7sbp--marketplace--catalog-element-detail-product--tabs--header--item <?=$arTab["CODE"] == $arResult["ACTIVE_TAB"] ? "active" : ""?>"
               data-tabname="<?=$arTab["CODE"]?>">
                <?=$arTab["TITLE"]?>
            </a>
        <?endforeach;?>
    </div>

    <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body">

        <?foreach ($arParams["TABS"] as $arTab):?>
            <div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body--item <?=$arTab["CODE"] == $arResult["ACTIVE_TAB"] ? "active" : ""?>"
                 data-tabname="<?=$arTab["CODE"]?>">

                <?switch ($arTab["CODE"]){
                    case "about":
						echo '<div id="about-component">';
                        $APPLICATION->ShowViewContent('TABS_DETAIL_TEXT');
						echo '</div>';
                        break;
                    case "data":
                        $APPLICATION->ShowViewContent('TABS_CONVERT_DATA');
                        break;
                    case "reviews":
                        if($request->get($arParams["REQUEST_PARAM"]) == "reviews"){
                            echo '<div id="reviews-component">';
                            $APPLICATION->IncludeComponent(
                                "bitrix:forum.topic.reviews",
                                "main",
                                $arParams["REVIEWS"],
                                false,
                                ['HIDE_ICONS' => 'Y']
                            );
                            echo '</div>';
                        }
                        break;
                    case "delivery":
                        $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                            array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "CART" => $componentElementParams["CART"],
                                "PATH" => SITE_DIR."include/catalog/element.delivery.php",
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "",
                                "AREA_FILE_RECURSIVE" => "Y",
                                "EDIT_TEMPLATE" => "standard.php"
                            ),
                            false,
                            array('HIDE_ICONS' => 'Y')
                        );
                        break;
                    case "garanty":
                        $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                            array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "CART" => $componentElementParams["CART"],
                                "PATH" => SITE_DIR."include/catalog/element.garanty.php",
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "",
                                "AREA_FILE_RECURSIVE" => "Y",
                                "EDIT_TEMPLATE" => "standard.php"
                            ),
                            false,
                            array('HIDE_ICONS' => 'Y')
                        );
                        break;
                }?>

            </div>
        <?endforeach;?>

    </div>
</div>