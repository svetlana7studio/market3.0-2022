<?
/**
 * bx_tabs
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "bx_tabs",
    "DESCRIPTION" => "bx_tabs",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "bx_tabs",
            "NAME" => "bx_tabs"
        )
    ),
);