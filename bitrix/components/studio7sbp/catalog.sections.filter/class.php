<?

use Bitrix\Main\Loader,
    Bitrix\Iblock\SectionTable,
    Bitrix\Main\Application;

class catalogSectionsFilter extends CBitrixComponent
{

    private $_sections;
    private $_filter;

    /**
     * @return mixed
     */
    public function getSections()
    {
        return $this->_sections;
    }

    /**
     * @param mixed $sections
     */
    public function setSections($sections)
    {
        $this->_sections = $sections;
    }
    /**
     * @param mixed $sections
     */
    public function setSectionsFromIblock()
    {
        $arFilter = [
            //"IBLOCK_TYPE" => $this->arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
            "GLOBAL_ACTIVE"=>"Y",
            "ACTIVE"=>"Y",
            "UF_PARENT"=>$this->arParams["SECTION_ID"]
            //"IBLOCK_SECTION_ID" => $this->arParams["SECTION_ID"],
            //"IBLOCK_ACTIVE"=>"Y"
        ];

        if($this->arParams["DEPTH_LEVEL"] > 0){
            $arFilter["DEPTH_LEVEL"] = $this->arParams["DEPTH_LEVEL"];
        }

        $sections = [];
        $ibLockSections = CIBlockSection::GetList(
            [
                $this->arParams["SORT_BY1"] => $this->arParams["SORT_ORDER1"],
                $this->arParams["SORT_BY2"] => $this->arParams["SORT_ORDER2"]
            ],
            $arFilter,
            false,
            [
                "ID",
                "NAME",
                "UF_PARENT"
            ]
        );

        while ($section = $ibLockSections->Fetch())
        {
            $sections[$section["ID"]] = $section;
        }
        
        $this->setSections($sections);
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->_filter;
    }

    /**
     * @param mixed $filter
     */
    public function setFilter($filter)
    {
        $this->_filter = $filter;
    }

    public function prepareFilter()
    {
        $arrFilter = [
            "INCLUDE_SUBSECTIONS" => "Y"
        ];
        $request = Application::getInstance()->getContext()->getRequest();
        if(!empty($request->get("asf"))){
            # form filter
            $arrFilter["SECTION_ID"] = $request->get("asf");
            # activatec hecked sections
            if(!empty($this->getSections())){
                foreach ($this->getSections() as $arSections){
                    if(in_array($arSections["ID"], $arrFilter["SECTION_ID"])){
                        $this->_sections[$arSections["ID"]]["CHECKED"] = "Y";
                    }
                }
            }
        }
        $this->setFilter($arrFilter);
    }

    public function executeComponent()
    {
        if(Loader::includeModule("iblock")){
            $this->setSectionsFromIblock();
        }

        $this->prepareFilter();

        $this->arResult = [
            "SECTIONS" =>$this->getSections()
        ];

        $this->includeComponentTemplate();

        return $this->getFilter();
    }

}