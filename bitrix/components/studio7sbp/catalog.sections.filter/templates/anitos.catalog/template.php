<?
/**
 * @var array $arParams
 * @var array $arResult
 */

$this->SetViewTarget("catalogSectionsFilter");
?>

    <div class="wrap-toggle tog-minimal">
        <form action="<?=$arParams["PAGE"]?>"
              method="get"
              class="bg-white p2 pt5 border-radius-10 mb4 getHightForm">
            <div id="sections-form">
                <?
                if(!empty($arResult["SECTIONS"])):
                    $checked = false;
                    foreach($arResult["SECTIONS"] as $arSection){
                        if($arSection["CHECKED"]){
                            $checked = true;
                            break;
                        }
                    }
                    ?>

                    <div class="text-center marginB15">
                        <input type="submit"
                               id="showFilterButton"
                               name="set_filter"
                               class="btn btn-warning border-radius-0 text-14 text-height-14 py1 px4 cursor-pointer <? if($checked === false):?> hide-button<? endif;?>"
                               value="<? if($checked === false):?>Выберите разделы<? else: ?>Показать<? endif;?>" <? if($checked === false):?> disabled<? endif;?> >
                        <a href="?dfs=Y" class="btn btn-warning border-radius-0 text-14 text-height-14 py1 px4 cursor-pointer reset-button<? if($checked === false):?> hide-b<? endif;?>">

                            Сбросить</a>
                    </div>


                    <?foreach ($arResult["SECTIONS"] as $arSection):?>

                    <label>
                        <input type="checkbox"
                               name="asf[]"
                            <?if($arSection["CHECKED"] == "Y"):?>
                                checked
                            <?endif;?>
                               class="ch_on"
                               value="<?=$arSection["ID"]?>" />

                        <div class="el-checks-text">
                            <?=$arSection["NAME"]?>
                        </div>
                    </label>

                <?
                endforeach;
                    unset($arSection, $arResult);
                endif;
                ?>
            </div>
        </form>
        <div class="toggle-bottom-background"></div>
        <div class="toggle-button"><a href="#" id="togglerButton" onclick="return false;">Развернуть</a></div>
    </div>
<?
$this->EndViewTarget();
?>