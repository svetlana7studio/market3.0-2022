<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);


if (!Loader::includeModule('iblock'))
	return;

$catalogIncluded = Loader::includeModule('catalog');
$iblockExists = (!empty($arCurrentValues['IBLOCK_ID']) && (int)$arCurrentValues['IBLOCK_ID'] > 0);

$arIBlockType = CIBlockParameters::GetIBlockTypes();
$arIBlock = array();
$iblockFilter = (
	!empty($arCurrentValues['IBLOCK_TYPE'])
	? array('TYPE' => $arCurrentValues['IBLOCK_TYPE'], 'ACTIVE' => 'Y')
	: array('ACTIVE' => 'Y')
);
$rsIBlock = CIBlock::GetList(array('SORT' => 'ASC'), $iblockFilter);
while ($arr = $rsIBlock->Fetch())
	$arIBlock[$arr['ID']] = '['.$arr['ID'].'] '.$arr['NAME'];
unset($arr, $rsIBlock, $iblockFilter);

$arSorts = [
    "ASC"=>Loc::getMessage("S7_CSF_IBLOCK_DESC_ASC"),
    "DESC"=>Loc::getMessage("S7_CSF_IBLOCK_DESC_DESC")
];
$arSortFields = [
    "ID"=>Loc::getMessage("S7_CSF_IBLOCK_DESC_FID"),
    "NAME"=>Loc::getMessage("S7_CSF_IBLOCK_DESC_FNAME"),
    //"ACTIVE_FROM"=>Loc::getMessage("S7_CSF_IBLOCK_DESC_FACT"),
    "SORT"=>Loc::getMessage("S7_CSF_IBLOCK_DESC_FSORT"),
    "TIMESTAMP_X"=>Loc::getMessage("S7_CSF_IBLOCK_DESC_FTSAMP")
];


$arComponentParameters = array(
	"GROUPS" => [
	    "FILTER_GROUP" => ["NAME" => Loc::getMessage("S7_CSF_FILTER_GROUP")],
        "SORT_GROUP" => ["NAME" => Loc::getMessage("S7_CSF_SORT_GROUP")]
    ],

	"PARAMETERS" => [

	    # DATA_SOURCE GROUP

	    "IBLOCK_TYPE" => [
            "PARENT" => "DATA_SOURCE",
            "NAME" => Loc::getMessage("S7_CSF_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ],
        "IBLOCK_ID" => [
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("S7_CSF_IBLOCK_ID"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
        ],
        "SECTION_ID" => [
            "PARENT" => "DATA_SOURCE",
            "NAME" => Loc::getMessage("S7_CSF_SECTION_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => '={$_REQUEST["SECTION_ID"]}',
        ],
        "SECTION_CODE" => [
            "PARENT" => "DATA_SOURCE",
            "NAME" => Loc::getMessage("S7_CSF_SECTION_CODE"),
            "TYPE" => "STRING",
            "DEFAULT" => '',
        ],

        # FILTER GROUP

        "FILTER_NAME" => [
            "PARENT" => "FILTER_GROUP",
            "NAME" => Loc::getMessage("S7_CSF_FILTER_NAME"),
            "TYPE" => "STRING",
            "DEFAULT" => "arrFilter",
        ],

        "DEPTH_LEVEL" => [
            "PARENT" => "FILTER_GROUP",
            "NAME" => Loc::getMessage("S7_CSF_DEPTH_LEVEL"),
            "TYPE" => "LIST",
            "VALUES" => [
                0 => Loc::getMessage("S7_CSF_ALL"),
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5,
                6 => 6,
                7 => 7,
                8 => 8,
                9 => 9,
                10 => 10
            ],
            "DEFAULT" => '3',
        ],

        # CACHE PARAMS GROUP

        "CACHE_TIME" => array(
            "DEFAULT" => 36000000,
        ),
        "CACHE_GROUPS" => array(
            "PARENT" => "CACHE_SETTINGS",
            "NAME" => Loc::getMessage("S7_CSF_CACHE_GROUPS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),

        # SORT GROUP
        "SORT_BY1" => array(
            "PARENT" => "SORT_GROUP",
            "NAME" => Loc::getMessage("S7_CSF_IBLOCK_DESC_IBORD1"),
            "TYPE" => "LIST",
            "DEFAULT" => "ACTIVE_FROM",
            "VALUES" => $arSortFields,
            "ADDITIONAL_VALUES" => "Y",
        ),
        "SORT_ORDER1" => array(
            "PARENT" => "SORT_GROUP",
            "NAME" => Loc::getMessage("S7_CSF_IBLOCK_DESC_IBBY1"),
            "TYPE" => "LIST",
            "DEFAULT" => "DESC",
            "VALUES" => $arSorts,
            "ADDITIONAL_VALUES" => "Y",
        ),
        "SORT_BY2" => array(
            "PARENT" => "SORT_GROUP",
            "NAME" => Loc::getMessage("S7_CSF_IBLOCK_DESC_IBORD2"),
            "TYPE" => "LIST",
            "DEFAULT" => "SORT",
            "VALUES" => $arSortFields,
            "ADDITIONAL_VALUES" => "Y",
        ),
        "SORT_ORDER2" => array(
            "PARENT" => "SORT_GROUP",
            "NAME" => Loc::getMessage("S7_CSF_IBLOCK_DESC_IBBY2"),
            "TYPE" => "LIST",
            "DEFAULT" => "ASC",
            "VALUES" => $arSorts,
            "ADDITIONAL_VALUES" => "Y",
        )
    ]
);