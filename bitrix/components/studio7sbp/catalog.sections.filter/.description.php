<?
/**
 * catalog_sections_filter
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "catalog_sections_filter",
    "DESCRIPTION" => "catalog_sections_filter",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "catalog_sections_filter",
            "NAME" => "catalog_sections_filter"
        )
    ),
);