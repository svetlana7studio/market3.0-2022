<?
/**
 * @var array $arParams
 * @var array $arResult
 */
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>
<div id="favorite-store">
    <div class="h2"><?=Loc::getMessage("STORE_FAVORITE_TITLE")?></div>
    <?foreach ($arResult["STORES"] as $store):?>
        <?if($store["FAVORITE"] == "Y"):?>
            <div class="alert alert-success row">
                <div class="col"><?=$store["NAME"]?></div>
                <div class="col col-auto">
                    <a href="<?=$APPLICATION->GetCurPageParam("action=delete&id=" . $store["ID"], array("id", "action"))?>">
                        <?=Loc::getMessage("STORE_FAVORITE_DELETE")?>
                    </a>
                </div>
            </div>
        <?else:?>
            <div class="alert alert-secondary row">
                <div class="col"><?=$store["NAME"]?></div>
                <div class="col col-auto">
                    <a href="<?=$APPLICATION->GetCurPageParam("action=add&id=" . $store["ID"], array("id", "action"))?>">
                        <?=Loc::getMessage("STORE_FAVORITE_ADD")?>
                    </a>
                </div>
            </div>
        <?endif;?>
    <?endforeach;?>
</div>