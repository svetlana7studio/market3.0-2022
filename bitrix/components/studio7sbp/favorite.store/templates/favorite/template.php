<?
/**
 * @var array $arParams
 * @var array $arResult
 */
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>
<div id="favorite-store">
    <h1><?=Loc::getMessage("STORE_FAVORITE_TITLE")?></h1>

    <div class="s7sbp--marketplace--index--right-block block-one-column">

        <?if(empty($arResult["STORES"])):?>
            <div class="alert alert-danger text-center"><?=Loc::getMessage("STORE_FAVORITE_EMPTY")?></div>
        <?else:?>
            <?foreach ($arResult["STORES"] as $store):?>
                <a href="<?=SITE_DIR?>brands/<?=$store["ID"]?>/"
                   class="product-item-container position-relative"
                   data-entity="item">
                    <div class="product-item alert alert-primary" >
                        <div class="item-favorite-fill"></div>

                        <?if(is_array($store["PREVIEW_PICTURE"])):?>
                            <img src="<?=$store["PREVIEW_PICTURE"]["SRC"]?>"
                                 width="150">
                        <?else:?>
                            <img src="<?=$this->GetFolder()?>/images/no_photo.png"
                                 width="150">
                        <?endif;?>

                        <div class="h5">
                            <?=$store["NAME"]?>
                        </div>

                    </div>
                </a>
            <?endforeach;?>
        <?endif;?>
    </div>
</div>