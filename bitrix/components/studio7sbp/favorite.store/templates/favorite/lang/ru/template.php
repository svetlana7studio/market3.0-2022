<?
$MESS = array(
    "STORE_FAVORITE_TITLE" => "Избранные торговые марки",
    "STORE_FAVORITE_ADD" => "Добавить в избранные",
    "STORE_FAVORITE_DELETE" => "Удалить из избранных",
    "STORE_FAVORITE_EMPTY" => "У вас пока нет избранных торговых марок.",
);
?>