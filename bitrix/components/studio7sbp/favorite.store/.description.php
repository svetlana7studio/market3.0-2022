<?
/**
 * favorite_store
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "favorite_store",
    "DESCRIPTION" => "favorite_store",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "favorite_store",
            "NAME" => "favorite_store"
        )
    ),
);