<?
/**
 * @var array $arResult
 * @var array $arParams
 */
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>

<form action="<?=$arResult["PAGE"]?>">
    <?=bitrix_sessid_post()?>

    <?foreach ($arResult["FIELDS"] as $field):?>
        <div class="row mb-2 pb-2 border-bottom border-primary">
            <div class="col-auto"><?=$field["NAME"]?> (<?=$field["CODE"]?>):</div>
            <div class="col"><?=$arResult["MESSAGE"][$field["CODE"]]?></div>
        </div>
    <?endforeach;?>

    <input type="submit"
           class="btn btn-primary"
           value="<?=Loc::getMessage("VENDOR_MESSAGE_SAVE")?>">
</form>
