<?
/**
 * @var array $arResult
 * @var array $arParams
 */
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>

<form action="<?=$arResult["PAGE"]?>" method="post">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="IFRAME" value="<?=$arParams["IFRAME"]?>">

    <?foreach ($arResult["FIELDS"] as $field):?>
        <div class="row mb-2 pb-2 border-bottom border-primary align-items-center">
            <div class="col-auto"><?=$field["NAME"]?>:</div>
            <div class="col"><?
                $value = $arResult["MESSAGE"][$field["CODE"]];

                switch ($field["CODE"]){
                    case "EMAIL";
                        $value = '<a href="mailto:' . $value . '">' . $value . '</a>';
                        echo $value;
                    break;
                    case "CATEGORY";
                        $value = Loc::getMessage("SELLER_CATEGORY_" . ToUpper($value));
                        echo $value;
                    break;
                    case "ACTIVE";
                        ?>
                            <label>
                                <input type="radio"
                                       name="<?=$field["CODE"]?>"
                                       <?
                                       if(ToUpper($value) == "Y")
                                           echo 'checked';
                                       ?>
                                       value="Y"> <?=Loc::getMessage("SELLER_MESSAGES_Y")?>
                            </label>
                            <br>
                            <label>
                                <input type="radio"
                                       name="<?=$field["CODE"]?>"
                                       <?
                                       if(ToUpper($value) <> "Y")
                                           echo 'checked';
                                       ?>
                                       value="N"> <?=Loc::getMessage("SELLER_MESSAGES_N")?>
                            </label>

                        <?
                    break;
                    case "SORT";
                        ?>
                            <select name="<?=$field["CODE"]?>">
                                <?for($i=1; $i<=500;$i++):?>
                                    <option value="<?=$i?>" <?
                                    if($i == $value)
                                        echo "selected";
                                    ?>><?=$i?></option>
                                <?endfor;?>
                            </select>
                        <?
                    break;
                    case "USER_CREATE":
                        if(empty($value["COMPANY"])){
                            if($value["USER_ID"] > 0)
                                echo Loc::getMessage("SELLER_USER_TYPE_BUYER");
                            else
                                echo Loc::getMessage("SELLER_USER_TYPE_GUEST");
                        }else{
                            ?>
                            <a href="<?=$value["COMPANY"]["DETAIL_PAGE_URL"]?>" class="toast">
                                <div class="toast-header">
                                    <img src="<?=$value["COMPANY"]["PREVIEW_PICTURE"]?>" class="rounded mr-2" alt="...">
                                    <strong class="mr-auto"><?=$value["COMPANY"]["NAME"]?></strong>
                                </div>
                                <div class="toast-body">

                                </div>
                            </a>
                            <?
                        }
                        break;
                    default:
                        echo $value;
                }
            ?></div>
        </div>
    <?endforeach;?>

    <input type="submit"
           class="btn btn-primary"
           value="<?=Loc::getMessage("VENDOR_MESSAGE_SAVE")?>">
</form>
