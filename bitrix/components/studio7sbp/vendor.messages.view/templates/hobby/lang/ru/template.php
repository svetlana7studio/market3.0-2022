<?
$MESS = [
    "VENDOR_MESSAGE_SAVE" => "Сохранить",
    "SELLER_CATEGORY_M" => "Мастер-класс",
    "SELLER_CATEGORY_W" => "Дизайнер",
    "SELLER_CATEGORY_SHOP" => "Магазин",
    "SELLER_MESSAGES_Y" => "Да",
    "SELLER_MESSAGES_N" => "Нет",
    "SELLER_USER_TYPE_GUEST" => "Гость",
    "SELLER_USER_TYPE_BUYER" => "Покупатель",
];