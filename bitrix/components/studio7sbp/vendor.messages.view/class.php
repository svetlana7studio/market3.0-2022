<?
use Bitrix\Main\Loader,
    Bitrix\Main\Type\DateTime,
    Studio7spb\Marketplace\CMarketplaceSeller,
    Studio7spb\Marketplace\VendorMessageTable;

class vendorMessagesView extends CBitrixComponent
{
    /**
     * @var array
     */
    private $_fields;

    /**
     * @var array
     */
    private $_message;

    /**
     * set fields from map
     */
    public function setFields()
    {
        $fields = VendorMessageTable::getMap();
        /**
         * @var Bitrix\Main\ORM\Fields\IntegerField [] $fields
         */
        foreach ($fields as $field)
        {
            if(!in_array($field->getName(), ["VENDOR_ID", "USER_UPDATE"])){
                $this->_fields[] = [
                    "CODE" => $field->getName(),
                    "NAME" => $field->getTitle()
                ];
            }
        }
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->_fields;
    }


    public function setMessage()
    {
        $message = VendorMessageTable::getList([
            "filter" => [
                "VENDOR_ID" => $this->arParams["COMPANY"],
                "ID" => $this->arParams["ID"]
            ]
        ]);
        if($message = $message->fetch())
        {
            $message["USER_CREATE"] = $this->userCreateInfo($message["USER_CREATE"]);
            $this->_message = $message;
        }
    }

    /**
     * @return array
     */
    public function getMessage()
    {
        return $this->_message;
    }

    private function userCreateInfo($userID=0){
        $info = [];
        if($userID > 0){
            $info = [
                "COMPANY" => CMarketplaceSeller::getCompanyByUserID($userID),
                "USER_ID" => $userID
            ];
            if($info["COMPANY"]["PREVIEW_PICTURE"] > 0){
                $info["COMPANY"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                    $info["COMPANY"]["PREVIEW_PICTURE"],
                    ["width" => 60, "height" => 60],
                    BX_RESIZE_IMAGE_EXACT,
                    false
                );
                $info["COMPANY"]["PREVIEW_PICTURE"] = $info["COMPANY"]["PREVIEW_PICTURE"]["src"];
            }
        }else{
            $info = [
                "TEXT" => "GUEST"
            ];
        }
        return $info;
    }

    private function refreshMessage()
    {
        if($this->request->isPost()){
            if(check_bitrix_sessid()){
                $arFields = [];
                foreach ($this->getFields() as $field)
                    if(array_key_exists($field["CODE"], $this->request->getPostList()->toArray()))
                        $arFields[$field["CODE"]] = $this->request->getPost($field["CODE"]);

                if(!empty($arFields)){
                    $arFields["DATE_UPDATE"] = new DateTime();
                    VendorMessageTable::update($this->arParams["ID"], $arFields);
                }

            }
        }

    }

    public function setDate(){
        if(empty($this->_message["DATE_UPDATE"])){
            $arFields = [
                "DATE_UPDATE" => new DateTime(),
                "ACTIVE" => "Y"
            ];
            VendorMessageTable::update($this->arParams["ID"], $arFields);
            $this->_message["DATE_UPDATE"] = $arFields["DATE_UPDATE"];
            $this->_message["ACTIVE"] = "Y";
        }
    }

    public function executeComponent()
    {
        Loader::registerAutoLoadClasses(null, [
            "\Studio7spb\Marketplace\VendorMessageTable" => "/bitrix/modules/studio7spb.marketplace/lib/market2/vendormessagetable.php"
        ]);

        $this->setFields();
        $this->refreshMessage();
        $this->setMessage();
        $this->setDate();

        $this->arResult = [
            "PAGE" => str_replace("#ID#", $this->arParams["ID"], $this->arParams["PATH_TO_MESSAGES_VIEW"]),
            "COMPANY" => $this->arParams["COMPANY"],
            "FIELDS" => $this->getFields(),
            "MESSAGE" => $this->getMessage()
        ];

        $this->includeComponentTemplate();
    }
}