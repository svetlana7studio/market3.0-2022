<?
/**
 * iblock_property_edit
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "iblock_property_edit",
    "DESCRIPTION" => "iblock_property_edit",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "iblock_property_edit",
            "NAME" => "iblock_property_edit"
        )
    ),
);