<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
	\Bitrix\Iblock,
	\Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/studio7spb.marketplace/options.php");

if (!\Bitrix\Main\Loader::includeModule('studio7spb.marketplace')){
	ShowError(Loc::getMessage('studio7spb.marketplace_MODULE_NOT_INSTALLED'));
	die();
}

class IblockPropertyEdit extends CBitrixComponent
{

	protected $moduleOptionList = array();

	function __construct($component = null) {
		$this->loadModuleOptions();
		parent::__construct($component);
	}

	protected function loadModuleOptions() {
		$this->moduleOptionList = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOptionList();
	}

	public function onPrepareComponentParams($params) {
		if(strlen($params["HIDDEN_PREFIX"]) <= 0) {
			$params["HIDDEN_PREFIX"] = $this->moduleOptionList["hidden_prefix"];
		}

		return $params;
	}
	
	protected function getPropertyList() {
		if(empty($this->arResult["property_list"])) {

			Loader::includeModule("iblock");

			$aSkipProperty = \Studio7spb\Marketplace\CMarketplaceTools::getSkipCatalogProperty();

			$this->arResult["property_list"] = array();
			$hiddenPropertyPrefix = $this->arParams["HIDDEN_PREFIX"];
			$hiddenPropertyPrefixLen = strlen($hiddenPropertyPrefix);

			$propertyIterator = Iblock\PropertyTable::getList(array(
				'filter' => array('=IBLOCK_ID' => $this->arParams['IBLOCK_ID'], '=ACTIVE' => 'Y'),
				'order' => array('SORT' => 'ASC', 'ID' => 'ASC')
			));

			while ($property = $propertyIterator->fetch()) {
				//if((substr($property["CODE"], 0, $hiddenPropertyPrefixLen) == $hiddenPropertyPrefix) || in_array($property["CODE"], $aSkipProperty)) {
				if(in_array($property["CODE"], $aSkipProperty)) {
					continue;
				}
				if(!in_array($property["CODE"], $this->getPropertyMap())){
				    continue;
                }

				if($this->arParams["COMPANY"]["PROPERTY_COMP_TYPE_ENUM_ID"] === 18){
                    $property["NAME"] = $property["XML_ID"];
                }

				if ($property["PROPERTY_TYPE"] == "L") {
					$propertyEnumIterator = Iblock\PropertyEnumerationTable::getList(array(
						'filter' => array('=PROPERTY_ID' => $property['ID']),
						'order' => array('SORT' => 'ASC', 'ID' => 'ASC')
					));
					$property["ENUM"] = array();
					while ($propertyEnum = $propertyEnumIterator->fetch()){
						$property["ENUM"][$propertyEnum["ID"]] = $propertyEnum;
					}
				}

				if(strlen($property["USER_TYPE"]) > 0 ) {
					$userType = \CIBlockProperty::GetUserType($property["USER_TYPE"]);
					if(array_key_exists("GetPublicEditHTML", $userType)) {
						$property["GetPublicEditHTML"] = $userType["GetPublicEditHTML"];
					} else {
						$property["GetPublicEditHTML"] = false;
					}
				} else {
					$property["GetPublicEditHTML"] = false;
				}

				$this->arResult["property_list"][$property["ID"]] = $property;
			}
		}
	}

	public function getPropertyMap(){
        $arResult = array();
	    switch ($this->arParams["COMPANY"]["PROPERTY_COMP_TYPE_ENUM_ID"]){
            case 19:
                // Зарубежный вендор
                $arResult = array(
                    "H_COMPANY",

                );
                break;

            case 18:
                // Российский вендор
                $arResult = array(
                    "H_COMPANY",

                );
                break;
                // Outher
            default:
                // Российский вендор если не задан тип компании
                $arResult = array(
                    "H_COMPANY",
                    "ARTICLE",
                    "COLOR_REF",
                    "SIZE",
                    "PROCESSOR",

                );

        }
        return $arResult;
    }

	public function executeComponent() {
		$this->getPropertyList();
		$this->includeComponentTemplate();
	}
}