<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init(array('ajax', 'jquery'));
?>

<?if (is_array($arResult["property_list"]) && !empty($arResult["property_list"])):?>
	<?
    foreach ($arResult["property_list"] as $propertyItem):
        if($propertyItem["CODE"] == "H_COMPANY")
            continue;
    ?>
		<div class="s7sbp--marketplace--saler--lk--product-add--field" data-is-property="Y" data-entity-id="<?=$propertyItem["ID"]?>">
			<div class="s7sbp--marketplace--saler--lk--product-add--field--label">
				<label for="PROPERTY[<?=$propertyItem["ID"]?>]">
                    <?
                    if($arParams["COMPANY"]["PROPERTY_COMP_TYPE_ENUM_ID"] == 18 && strlen($propertyItem["XML_ID"]) > 2){
                        echo $propertyItem["XML_ID"];
                    }else{
                        echo $propertyItem["NAME"];
                    }
                    if($propertyItem["IS_REQUIRED"] == "Y"){
                        ?><span class="required">*</span><?
                    }
                    ?>
                </label>
			</div>
			<div class="s7sbp--marketplace--saler--lk--product-add--field--value">
				<?

				$propertyType = $propertyItem["PROPERTY_TYPE"];
				if($propertyItem["GetPublicEditHTML"]) {
					$propertyType = "USER_TYPE";
				}

				switch ($propertyType):
					case "USER_TYPE":
						$value = $arParams["ELEMENT_PROPERTIES"][$propertyItem["ID"]][0]["VALUE"];
						$description = "";
						
						$propertyItem["USER_TYPE_SETTINGS"] = unserialize($propertyItem["USER_TYPE_SETTINGS"]);

						echo call_user_func_array($propertyItem["GetPublicEditHTML"],
							array(
								$propertyItem,
								array(
									"VALUE" => $value,
									"DESCRIPTION" => $description,
								),
								array(
									"VALUE" => "PROPERTY[".$propertyItem["ID"]."][VALUE]",
									"DESCRIPTION" => "PROPERTY[".$propertyItem["ID"]."][DESCRIPTION]",
									"FORM_NAME"=>"form-roduct-add",
								),
							)
						);
					break;
					case "HTML":
						$LHE = new CHTMLEditor;
						$LHE->Show(array(
							'name' => "PROPERTY[".$propertyItem["ID"]."][0]",
							'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyItem["ID"]."][0]"),
							'inputName' => "PROPERTY[".$propertyItem["ID"]."][0]",
							'content' => $arParams["ELEMENT_PROPERTIES"][$propertyItem["ID"]][0]["VALUE"],
							'width' => '100%',
							'minBodyWidth' => 350,
							'normalBodyWidth' => 555,
							'height' => '200',
							'bAllowPhp' => false,
							'limitPhpAccess' => false,
							'autoResize' => true,
							'autoResizeOffset' => 40,
							'useFileDialogs' => false,
							'saveOnBlur' => true,
							'showTaskbars' => false,
							'showNodeNavi' => false,
							'askBeforeUnloadPage' => true,
							'bbCode' => false,
							'siteId' => SITE_ID,
							'controlsMap' => array(
								array('id' => 'Bold', 'compact' => true, 'sort' => 80),
								array('id' => 'Italic', 'compact' => true, 'sort' => 90),
								array('id' => 'Underline', 'compact' => true, 'sort' => 100),
								array('id' => 'Strikeout', 'compact' => true, 'sort' => 110),
								array('id' => 'RemoveFormat', 'compact' => true, 'sort' => 120),
								array('id' => 'Color', 'compact' => true, 'sort' => 130),
								array('id' => 'FontSelector', 'compact' => false, 'sort' => 135),
								array('id' => 'FontSize', 'compact' => false, 'sort' => 140),
								array('separator' => true, 'compact' => false, 'sort' => 145),
								array('id' => 'OrderedList', 'compact' => true, 'sort' => 150),
								array('id' => 'UnorderedList', 'compact' => true, 'sort' => 160),
								array('id' => 'AlignList', 'compact' => false, 'sort' => 190),
								array('separator' => true, 'compact' => false, 'sort' => 200),
								array('id' => 'InsertLink', 'compact' => true, 'sort' => 210),
								array('id' => 'InsertImage', 'compact' => false, 'sort' => 220),
								array('id' => 'InsertVideo', 'compact' => true, 'sort' => 230),
								array('id' => 'InsertTable', 'compact' => false, 'sort' => 250),
								array('separator' => true, 'compact' => false, 'sort' => 290),
								array('id' => 'Fullscreen', 'compact' => false, 'sort' => 310),
								array('id' => 'More', 'compact' => true, 'sort' => 400)
							),
						));
					break;
					case "T":
						?>
							<textarea name="PROPERTY[<?=$propertyItem["ID"]?>]" <?if($propertyItem["IS_REQUIRED"] == "Y"):?> required="required"<?endif;?>><?=$arParams["ELEMENT_PROPERTIES"][$propertyItem["ID"]][0]["VALUE"]?></textarea>
						<?
					break;

					case "S":
					    ?>
                        <input type="text"
                               name="PROPERTY[<?=$propertyItem["ID"]?>]"
                            <?if($propertyItem["IS_REQUIRED"] == "Y"):?>
                                required="required"
                            <?endif;?>
                               value="<?=$arParams["ELEMENT_PROPERTIES"][$propertyItem["ID"]][0]["VALUE"]?>"/>
                    <?
                        break;
					case "N":

					    if($propertyItem["CODE"] == "NDS"):
						?>
                            <select name="PROPERTY[<?=$propertyItem["ID"]?>]">
                                <option value="10" <?=$arParams["ELEMENT_PROPERTIES"][$propertyItem["ID"]][0]["VALUE"] == 10 ? " selected" : ""?>>10</option>
                                <option value="20" <?=$arParams["ELEMENT_PROPERTIES"][$propertyItem["ID"]][0]["VALUE"] == 20 ? " selected" : ""?>>20</option>
                            </select>
                        <?else:?>
						<input type="number"
                               name="PROPERTY[<?=$propertyItem["ID"]?>]"
                               <?if($propertyItem["IS_REQUIRED"] == "Y"):?>
                                   required="required"
                               <?endif;?>
                               placeholder="1.0"
                               step="0.0001"
                               value="<?=$arParams["ELEMENT_PROPERTIES"][$propertyItem["ID"]][0]["VALUE"]?>"/>
                    <?
                        endif;
					break;

					case "L":

						if ($propertyItem["LIST_TYPE"] == "C") {
							$propertyItemListInputType = $propertyItem["MULTIPLE"] == "Y" ? "checkbox" : "radio";
						} else {
							$propertyItemListInputType = $propertyItem["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";
						}

						switch ($propertyItemListInputType):
							case "checkbox":
							case "radio":
								foreach ($propertyItem["ENUM"] as $key => $enumItem) {
									$checked = false;
									if ($enumItem["DEF"] == "Y") $checked = true;
									if($key == $arParams["ELEMENT_PROPERTIES"][$propertyItem["ID"]][0]["VALUE"])  $checked = true;
									?>
										<input type="<?=$propertyItemListInputType?>" name="PROPERTY[<?=$propertyItem["ID"]?>]<?=$propertyItemListInputType == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$enumItem["VALUE"]?></label><br />
									<?
								}
							break;

							case "dropdown":
							case "multiselect":
							?>
								<select name="PROPERTY[<?=$propertyItem["ID"]?>]<?=$propertyItemListInputType=="multiselect" ? "[]\" multiple=\"multiple" : ""?>"  <?if($propertyItem["IS_REQUIRED"] == "Y"):?> required="required"><?endif;?>>
									<option value=""></option>
								<?
									if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
									else $sKey = "ELEMENT";

									foreach ($propertyItem["ENUM"] as $key => $enumItem) {
										$checked = false;
										if ($enumItem["DEF"] == "Y") $checked = true;
										if($key == $arParams["ELEMENT_PROPERTIES"][$propertyItem["ID"]][0]["VALUE"])  $checked = true;
										?>
											<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$enumItem["VALUE"]?></option>
										<?
									}
								?>
								</select>
							<?
							break;

						endswitch;
					break;
				endswitch;?>
			</div>
		</div>
	<?endforeach;?>
<?endif?>

<script>
	var studio7sbpIblockPropertyEditSettings = {
		iblockId: '<?=$arParams["IBLOCK_ID"]?>',
	}
</script>