function setPropertyBySection(items, openAll) {
	if(openAll == "Y") {
		$('.s7sbp--marketplace--saler--lk--product-add--field[data-is-property="Y"]').show();
		return false;
	}

	/*
	$('.s7sbp--marketplace--saler--lk--product-add--field[data-is-property="Y"]').each(function(index, el) {
		var propertyId = parseInt($(this).attr('data-entity-id'));
		if(!items.hasOwnProperty(propertyId)) {
			$(this).hide();
		} else {
			$(this).show();
		}
	});
	*/
}

function loadSectionBySelectedSection() {
	var sectionValue = parseInt($('[name="SECTION_ID"]').val());
	if(!sectionValue || sectionValue == undefined) {
		setPropertyBySection([], 'Y');
	}
	var request = BX.ajax.runComponentAction('studio7sbp:iblock.property.edit', 'getPropertyBySection', {
		mode:'ajax',
		data: {
			sectionId: sectionValue,
			iblockId: studio7sbpIblockPropertyEditSettings.iblockId,
		}
	});

	request.then(function(response){
		setPropertyBySection(response.data.items);
	});
}

$(document).ready(function() {
	$('body').on('change', '[name="SECTION_ID"]', function(e) {
		e.preventDefault();
		loadSectionBySelectedSection();
	});

	loadSectionBySelectedSection();		
});