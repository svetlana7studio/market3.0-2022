<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Engine\ActionFilter;

class IblockPropertyEditAjaxController extends \Bitrix\Main\Engine\Controller
{

	public function configureActions() {
		return array(
			'getPropertyBySection' => array(
				'prefilters' => array(
					new ActionFilter\Authentication(),
					new ActionFilter\HttpMethod(
						array(ActionFilter\HttpMethod::METHOD_POST)
					),
				),
				'postfilters' => array()
			)
		);
	}
 
	/**
	 * @return array
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function getPropertyBySectionAction()
	{
		$app = \Bitrix\Main\Application::getInstance();
		$request = $app->getContext()->getRequest();

		$sectionId = $request->getPost('sectionId');
		$iblockId = $request->getPost('iblockId');

		\Bitrix\Main\Loader::includeModule("iblock");
		$aProperty = \CIBlockSectionPropertyLink::GetArray($iblockId, $sectionId);

		return array(
			'items' => $aProperty
		);
	}
}