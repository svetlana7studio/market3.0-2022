<?
$MESS = [
    "PROPERTIES_FORUM_MESSAGE_CNT" => "NUM отзыв",
    "PRICE" => "Цена",
    "PRICE_INFO_DETAIL" => "Подробнее о цене",
    "PRODUCT_QUANTITY_TITLE" => "Доступно для покупки",
    "PRODUCT_QUANTITY_MEASURE" => "N шт.",
    "PROPERTIES_SHORT_TITLE" => "Коротко о товаре",
    "DISPLAY_PROPERTIES_TITLE" => "Общие характеристики",
    "DETAIL_TEXT_TITLE" => "Описание",
    "ADD_BASKET_BTN" => "Добавить в корзину",
];