var market2CatalogElement = {

    form: {
        id: "market2-catalog-element-sale",
        submitBtn: "actionADD2BASKET"
    },

    favorite: {
        id: "market2-element-favorite",
        active: "active"
    },
    share: {
        id: "element-share",
        active: "d-none"
    },
    obPopupWin: null,

    viewedCounter: {
        path: '/bitrix/components/bitrix/catalog.element/ajax.php',
        params: {
            AJAX: 'Y',
            SITE_ID: null,
            PRODUCT_ID: null,
            PARENT_ID: null
        }
    },

    toggleFavorite: function (t) {

        BX.ajax.runAction('studio7spb:marketplace.api.tools.addToWish', {
            data: {
                id: t.dataset.id,
            }
        });

        if(BX.hasClass(BX(market2CatalogElement.favorite.id), market2CatalogElement.favorite.active)){
            BX.removeClass(BX(market2CatalogElement.favorite.id), market2CatalogElement.favorite.active);
            BX(market2CatalogElement.favorite.id).innerHTML = '<i class="icon icon-favorite-info square-16 vertical-align-middle"></i> ' + BX(market2CatalogElement.favorite.id).dataset.title
        }else{
            BX.addClass(BX(market2CatalogElement.favorite.id), market2CatalogElement.favorite.active);
            BX(market2CatalogElement.favorite.id).innerHTML = '<i class="icon icon-favorite-info square-16 vertical-align-middle"></i> ' + BX(market2CatalogElement.favorite.id).dataset.titlein
        }

    },

    /**
     * Toggle share panel
     */
    toggleShare: function () {
        if(BX.hasClass(BX(this.share.id), this.share.active)){
            BX.removeClass(BX(this.share.id), this.share.active);
        }else{
            BX.addClass(BX(this.share.id), this.share.active);
        }
    },

    priceInfoPopup: function(t){

        var popupContent = $("#market2-price-info-popup").html();

        this.obPopupWin = BX.PopupWindowManager.create('price_info_popup', null, {
            autoHide: true,
            offsetLeft: 0,
            offsetTop: 0,
            overlay : true,
            closeByEsc: true,
            titleBar: false,
            content: popupContent,
            closeIcon: true,
            contentColor: 'white',
            className: "price_info_popup",
        });

        //this.obPopupWin.setTitleBar($(t).attr("title"));
        //this.obPopupWin.setContent(popupContent);
        this.obPopupWin.show();

        return false;

    },

    /**
     * Detail picture functions
     */
    detailPictureShow: function (t) {
        var link = $("#market2-catalog-element-photo");
        if(!!link && link.length > 0){
            link.click();
        }
    },

    /**
     * Doom events
     * @param t
     */
    onkeyup: function(t){

        if (event.keyCode === 13) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Trigger the button element with a click

            if($('#' + this.form.submitBtn).is('[disabled=disabled]')){
                BX(this.form.submitBtn).click();
            }

            disableElement(this.form.submitBtn);
        }else{
            //quantityCounter.inputIntValidator(t);
        }

    },

    onblur: function(t){
        event.preventDefault();
        // Trigger the button element with a click

        if($('#' + this.form.submitBtn).is('[disabled=disabled]')){
            BX(this.form.submitBtn).click();
        }

        disableElement(this.form.submitBtn);
        // эта проверка была в onkeyup
        quantityCounter.inputIntValidator(t);
    },

    actionBuy: function(t){

        BX.onCustomEvent('OnBasketPreload');

        var form = $(t).closest("form"),
            quantity = form.find("input[type=text]"),
            id = form.find("input[name=id]");

        quantity = quantity.val();
        id = id.val();


        var request = BX.ajax.runAction('studio7spb:marketplace.api.tools.addToBasket', {
            data: {
                id: id,
                quantity: quantity
            }
        });
        request.then(function(response){
            if(response.status == 'success') {
                BX.onCustomEvent('OnBasketChange');
            }
        });

        //s7market.showItemsInBasket();

        return false;
    },

    /**
     * dev tools
     * @param value
     */
    d: function (value) {
        console.info(value);
    }

};


/*
var viewedCounter = {
    path: '/bitrix/components/bitrix/catalog.element/ajax.php',
    params: {
        AJAX: 'Y',
        SITE_ID: "<?= SITE_ID ?>",
        PRODUCT_ID: "<?= $arResult['ID'] ?>",
        PARENT_ID: "<?= $arResult['ID'] ?>"
    }
};
*/

BX.ready(
    BX.defer(function(){

        // set to viewedCounter siteid
        // set to viewedCounter id

        var elementData = BX("market2-catalog-element-sale");

        market2CatalogElement.viewedCounter.params.SITE_ID = elementData.dataset.siteid;
        market2CatalogElement.viewedCounter.params.PARENT_ID = elementData.dataset.id;
        market2CatalogElement.viewedCounter.params.PRODUCT_ID= elementData.dataset.id;

        // send data to vieved
        BX.ajax.post(
            market2CatalogElement.viewedCounter.path,
            market2CatalogElement.viewedCounter.params
        );
    })
);

/**
 * Use jquery
 */
$(function() {
    /**
     * Init Jquery fancybob
     */
    $('.fancy').fancybox({
        openEffect  : 'fade',
        closeEffect : 'fade',
        nextEffect : 'fade',
        prevEffect : 'fade',
        index: 1
    });
});