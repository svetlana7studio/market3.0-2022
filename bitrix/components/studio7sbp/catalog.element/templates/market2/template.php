<?
/**
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */
use Bitrix\Main\Localization\Loc;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
Loc::loadLanguageFile(__FILE__);

$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/fancybox/jquery.fancybox.min.css");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/flexslider.css");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.fancybox.min.js");



// <editor-fold defaultstate="s7spb-collapsed" desc="# View target by element fields (ELEMENT)">

$this->SetViewTarget('ELEMENT_NAME');
echo $arResult["ELEMENT"]["NAME"];
$this->EndViewTarget();

$this->SetViewTarget('TABS_DETAIL_TEXT');

?><h3 class="mb3 text-primary text-16 text-uppercase"><?=Loc::getMessage("DETAIL_TEXT_TITLE")?></h3><?
echo "<p class='mb3'>";
echo $arResult["ELEMENT"]["DETAIL_TEXT"];
echo "</p>";


if(!empty($arResult["DISPLAY_PROPERTIES"])){

    ?><h3 class="mb3 text-primary text-16 text-uppercase"><?=Loc::getMessage("DISPLAY_PROPERTIES_TITLE")?></h3><?
    foreach($arResult["DISPLAY_PROPERTIES"] as $propertyID=>$propertyValue)
    {

        if(empty($propertyValue))
            continue;

        if(in_array($arResult["PROPERTIES"][$propertyID]["CODE"], $arResult["DISPLAY_PROPERTIES_EXEPT"]))
            continue;

        if(count($propertyValue) > 1)
            $propertyValue = implode(", ", $propertyValue);
        else{
            $propertyValue = current($propertyValue);
        }

        if(is_array($arResult["LINK_PROPERTIES"][$propertyID])){
            $propertyValue = '<a href="' . $arResult["LINK_PROPERTIES"][$propertyID]["DETAIL_PAGE_URL"]
                . '">'
                . $arResult["LINK_PROPERTIES"][$propertyID]["NAME"] . '</a>';
        }
        ?>
        <div class="s7spb-row s7spb-row-between border-dotted-bottom my2">
            <div class="s7spb-col s7spb-col-auto bg-white mb-neg-2 pr1"><?=$arResult["PROPERTIES"][$propertyID]["NAME"]?></div>
            <div class="s7spb-col s7spb-col-auto bg-white mb-neg-2 pl1"><?=$propertyValue?></div>
        </div>
        <?
    }
}
$this->EndViewTarget();

// </editor-fold>

// <editor-fold defaultstate="s7spb-collapsed" desc="# View target by element Link props (ELEMENT_TRADE_MARK &&LINK_PROPERTIES)">
$this->SetViewTarget('ELEMENT_TRADE_MARK');

$company = [];

foreach ($arResult["PROPERTIES"] as $property){
    if($property["CODE"] == "PRODUCER"){
        $company = $arResult["LINK_PROPERTIES"][$property["ID"]];
    }
}

//d($company);

if(is_array($company["PREVIEW_PICTURE"])):
    ?>
    <a href="<?=$company["DETAIL_PAGE_URL"]?>"
       class="text-decoration-none"><img src="<?=$company["PREVIEW_PICTURE"]["SRC"]?>"
                                         class="img-responsive"
                                         width="<?=$company["PREVIEW_PICTURE"]["WIDTH"]?>"
                                         height="<?=$company["PREVIEW_PICTURE"]["WIDTH"]?>" /></a>
<?
else:
    ?>
    <a href="<?=$company["DETAIL_PAGE_URL"]?>"
       class="text-decoration-none d-block p3 bg-primary text-white text-bold"><?=$company["NAME"]?> 777    </a>
<?
endif;
unset($company, $property);

$this->EndViewTarget();
// </editor-fold>

// <editor-fold defaultstate="s7spb-collapsed" desc="# View target by element props (DISPLAY_PROPERTIES)">


$this->SetViewTarget('PROPERTIES_SHORT');

if(!empty($arResult["DISPLAY_PROPERTIES"])) {
    $i = 0;

    if($arParams["PROPERTIES_SHORT_COUNT"] < 1)
        $arParams["PROPERTIES_SHORT_COUNT"] = 4;

    if(!empty($arResult["DISPLAY_PROPERTIES"])){

        ?><h3 class="mb3 text-primary text-16 text-uppercase"><?=Loc::getMessage("PROPERTIES_SHORT_TITLE")?></h3><?
        foreach($arResult["DISPLAY_PROPERTIES"] as $propertyID=>$propertyValue)
        {

            if($i < $arParams["PROPERTIES_SHORT_COUNT"]){
            if(empty($propertyValue))
                continue;

            if(in_array($arResult["PROPERTIES"][$propertyID]["CODE"], $arResult["DISPLAY_PROPERTIES_EXEPT"]))
                continue;

            if(count($propertyValue) > 1)
                $propertyValue = implode(", ", $propertyValue);
            else{
                $propertyValue = current($propertyValue);
            }

            if(is_array($arResult["LINK_PROPERTIES"][$propertyID])){
                $propertyValue = '<a href="' . $arResult["LINK_PROPERTIES"][$propertyID]["DETAIL_PAGE_URL"]
                . '">'
                . $arResult["LINK_PROPERTIES"][$propertyID]["NAME"] . '</a>';
            }
            ?>
            <div class="s7spb-row s7spb-row-between border-dotted-bottom my2">
                <div class="s7spb-col s7spb-col-auto bg-white mb-neg-2 pr1"><?=$arResult["PROPERTIES"][$propertyID]["NAME"]?></div>
                <div class="s7spb-col s7spb-col-auto bg-white mb-neg-2 pl1"><?=$propertyValue?></div>
            </div>
            <?
            }
            $i++;
        }
        ?>

        <div class="mb3">
            <a href="<?=$APPLICATION->GetCurPageParam("tab=about", ["tab"])?>#studio7sbp-bx-tabs"><?=Loc::getMessage("DISPLAY_PROPERTIES_TITLE")?></a>
        </div>

        <form id="market2-catalog-element-sale"
              data-siteid="<?=SITE_ID?>"
              data-id="<?=$arResult["ELEMENT"]["ID"]?>"
              action="<?=$APPLICATION->GetCurPage()?>"
              onsubmit="return market2CatalogElement.actionBuy(this)"
              method="post">


            <input type="hidden" name="action" value="ADD">
            <input type="hidden" name="id" value="<?=$arResult["ELEMENT"]["ID"]?>">

            <div class="mb3">
                <?
                echo bitrix_sessid_post();
                $arResult["PRICES"] = current($arResult["PRICES"]);

                if($arResult["PRICES"]["PRICE"] > $arResult["PRICES"]["DISCOUNT_PRICE"]):
                    $percent = (1 - $arResult["PRICES"]["DISCOUNT_PRICE"] / $arResult["PRICES"]["PRICE"]) * 100;
                    ?>
                    <div class="mb3">
                        <s class="text-24 text-gray mr3"><?=$arResult["PRICES"]["PRINT_VALUE"]?></s>
                        <div class="d-inline-block bg-danger px2 py1 text-white text-bold text-20">- <?=$percent?>%</div>
                    </div>
                    <div class="text-30"><?=$arResult["PRICES"]["PRINT_DISCOUNT_VALUE"]?></div>
                <?else:?>
                    <div class="text-30"><?=$arResult["PRICES"]["PRINT_VALUE"]?></div>
                <?endif;?>
            </div>

            <div class="s7spb-row s7spb-row12">
                <div class="s7spb-col s7spb-col-auto pb3">
                    <div class="s7spb-row quantity-counter"
                         data-moq="1"
                         data-multiplicity="1">
                        <div class="s7spb-col quantity-counter-minus"
                             onclick="quantityCounter.down(this)">&ndash;</div>
                        <input type="text"
                               onkeyup="market2CatalogElement.onkeyup(this)"
                               onblur="market2CatalogElement.onblur(this)"
                               name="<?=$arParams["PRODUCT_QUANTITY_VARIABLE"]?>"
                               value="<?
                               $value = 1;
                               echo $value;
                               ?>"
                               class="s7spb-col" />
                        <div class="s7spb-col quantity-counter-plus"
                             onclick="quantityCounter.up(this)">+</div>
                    </div>
                </div>
                <div class="s7spb-col pb3">
                    <input type="submit"
                           class="btn btn-primary"
                           value="<?=Loc::getMessage("ADD_BASKET_BTN")?>">
                </div>
            </div>

        </form>

        <?
    }

}
$this->EndViewTarget();

unset($propertyID, $propertyValue);
// </editor-fold>


?>

 <div id="market2-catalog-element-photos">

    <?if(!empty($arResult['MORE_PHOTO']) && count($arResult['MORE_PHOTO']) > 1):?>
        <ul id="market2-catalog-element-thumbs">
            <?foreach ($arResult['MORE_PHOTO'] as $photo):?>
                <li>
                    <a href="<?=$photo["BIG"]?>"
                       rel="element-gallery"
                       class="popup_link fancy">
                        <img src="<?=$photo["THUMB"]["src"]?>"
                              height="<?=$photo["THUMB"]["height"]?>"
                             width="<?=$photo["THUMB"]["width"]?>" />
                    </a>
                </li>
            <?endforeach;?>
        </ul>
    <?endif;?>

    <?if(is_array($arResult["ELEMENT"]["DETAIL_PICTURE"])):?>
        <a href="<?=$arResult["ELEMENT"]["DETAIL_PICTURE"]["SRC"]?>"
           rel="element-gallery"
           id="market2-catalog-element-photo"
           class="popup_link fancy"
           title="<?=$arResult["ELEMENT"]["DETAIL_PICTURE"]["TITLE"]?>">
            <img src="<?=$arResult["ELEMENT"]["DETAIL_PICTURE"]["SRC"]?>"
                 width="<?=$arResult["ELEMENT"]["DETAIL_PICTURE"]["WIDTH"]?>"
                 height="<?=$arResult["ELEMENT"]["DETAIL_PICTURE"]["HEIGHT"]?>"
                 class="d-inline-block img-responsive" /></a>
    <?endif;?>

</div>