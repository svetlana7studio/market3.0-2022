<?
/**
 * catalog_element
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "catalog_element",
    "DESCRIPTION" => "catalog_element",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "catalog_element",
            "NAME" => "catalog_element"
        )
    ),
);