<?
/**
 * Class catalogElement
 * Povered by artem@koorochka.com
 * created by 28.10.2020
 * Не поддерживаем версию хранения значений свойств в общей таблице на єтом проекте
 */

use Bitrix\Main\Loader,
    Bitrix\Iblock\InheritedProperty\ElementValues,
    Bitrix\Iblock\ElementTable,
    Bitrix\Iblock\PropertyTable,
    Bitrix\Catalog\PriceTable,
    Bitrix\Catalog\ProductTable,
    Studio7spb\Marketplace\ElementPropertyTable;

class catalogElement extends CBitrixComponent
{
    // <editor-fold defaultstate="collapsed" desc=" # Statemnt">

    /**
     * @var array
     */
    private $_element;

    /**
     * @var array
     */
    private $_section;

    /**
     * @var array
     */
    private $_propertis;

    /**
     * @var array
     */
    private $_propertyCodes;

    /**
     * @var array
     */
    private $_displayProperties;

    /**
     * @var array
     */
    private $_linkedProperties;

    /**
     * @var array
     */
    private $_prices;

    /**
     * @var array
     */
    private $_product;

    /**
     * @var array
     */
    private $_morePhoto;

    /**
     * @var array
     */
    private $_ipropValues;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Iblock Element">
    /**
     * @return array
     */
    public function getElement()
    {
        return $this->_element;
    }

    /**
     * @param array $element
     */
    public function setElement($element)
    {
        $this->_element = $element;
    }

    public function setElementFromDb()
    {
        $element = ElementTable::getList([
            "filter" => [
                "=IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                "=ID" => $this->arParams["ELEMENT_ID"]
            ],
            "limit" => 1,
            "select" => [
                "ID",
                "NAME",
                "DETAIL_TEXT",
                "IBLOCK_SECTION_ID",
                "PREVIEW_PICTURE",
                "DETAIL_PICTURE"
            ]
        ]);
        if($element = $element->fetch()){

            // picture
            if($element["PREVIEW_PICTURE"] > 0){
                if($element["DETAIL_PICTURE"] <= 0){
                    $element["DETAIL_PICTURE"] = $element["PREVIEW_PICTURE"];
                }
            }
            if($element["DETAIL_PICTURE"] > 0){
                $element["DETAIL_PICTURE"] = CFile::GetFileArray($element["DETAIL_PICTURE"]);
            }
            /*
            else{
                $element["DETAIL_PICTURE"] = [
                    "SRC" => SITE_TEMPLATE_PATH . "/images/no_image200.jpg",
                    "WIDTH" => 200,
                    "HEIGHT" => 200,
                    "ALT" => $element["NAME"],
                    "TITLE" => $element["NAME"]
                ];
            }
            */


            $this->setElement($element);
        }

    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Iblock properties">

    /**
     * @return array
     */
    public function getPropertis()
    {
        return $this->_propertis;
    }

    /**
     * @param string $key
     * @param array $property
     */
    public function setProperty($code, $property)
    {
        $this->_propertis[$code] = $property;
    }

    public function setPropertisByParams()
    {
        $propertis = PropertyTable::getList([
            "filter" => [
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
            ]
        ]);
        while ($property = $propertis->fetch()){
            $this->setProperty($property["ID"], [
                "ID" => $property["ID"],
                "CODE" => $property["CODE"],
                "NAME" => $property["NAME"],
                "PROPERTY_TYPE" => $property["PROPERTY_TYPE"],
                "USER_TYPE" => $property["USER_TYPE"],
                "MULTIPLE" => $property["MULTIPLE"],
                "LINK_IBLOCK_ID" => $property["LINK_IBLOCK_ID"]
            ]);
            $this->_propertyCodes[$property["ID"]] = $property["CODE"];
        }
    }

    /**
     * @param string $code
     * @return bool
     */
    public function isPropertyExist($code){
        $result = false;
        if(!empty($this->_propertis[$code])){
            $result = true;
        }
        return $result;
    }

    /**
     * @param string $code
     * @return array
     */
    public function getPropertyByCode($code){

        $property = [];

        if(empty($this->_propertis) && !empty($this->arResult["PROPERTIES"]))
            $this->_propertis = $this->arResult["PROPERTIES"];

        foreach ($this->_propertis as $prop){
            if($code === $prop["CODE"]){
                $property = $prop;
                break;
            }
        }
        unset($prop);
        return $property;
    }

    public function getPropertyCode($id){
        return $this->_propertyCodes[$id];
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Display properties">

    /**
     * @return array
     */
    public function getDisplayProperties()
    {
        return $this->_displayProperties;
    }

    /**
     * @param string $code
     * @param array $property
     */
    public function setDisplayProperty($code, $property)
    {
        $this->_displayProperties[$code][] = $property;
    }

    /**
     * @param array $displayProperties
     */
    public function setDisplayProperties($displayProperties)
    {
        $this->_displayProperties = $displayProperties;
    }

    /**
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * PROPERTY_CODE
     */
    public function fillDisplayPropertis($selectAll=true)
    {
        if($this->_element["ID"] <= 0)
            return false;

        $arParams = [
            "filter" => [
                "=IBLOCK_ELEMENT_ID" => $this->_element["ID"],
                "!VALUE" => false
            ]
        ];

        Loader::registerAutoLoadClasses(
            "studio7spb.marketplace",
            [
                "\\Studio7spb\\Marketplace\\ElementPropertyTable" => "lib/market2/elementpropertytable.php"
            ]
        );

        $values = ElementPropertyTable::getList($arParams);

        while ($value = $values->fetch())
        {
            $this->setDisplayProperty($value["IBLOCK_PROPERTY_ID"], $value["VALUE"]);
        }

    }

    /**
     * @return array
     */
    public function getLinkedProperties()
    {
        return $this->_linkedProperties;
    }

    public function fillLinkedProps()
    {

        foreach ($this->getPropertis() as $code=>$property){
            if($property["PROPERTY_TYPE"] == "E"){

                if($property["MULTIPLE"] == "N"){

                    $property["VALUE"] = $this->_displayProperties[$property["ID"]];
                    if(!empty($property["VALUE"])) {
                        $property["VALUE"] = current($property["VALUE"]);
                        $property = CIBlockElement::GetList(
                            [],
                            [
                                "IBLOCK_TYPE" => $this->arParams["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $property["LINK_IBLOCK_ID"],
                                "ACTIVE" => "Y",
                                "ID" => $property["VALUE"]
                            ],
                            false,
                            false,
                            [
                                "ID",
                                "NAME",
                                "PREVIEW_PICTURE",
                                "DETAIL_PICTURE",
                                "DETAIL_PAGE_URL",
                                "PROPERTY_FORUM_MESSAGE_CNT"
                            ]
                        );
                        if($property = $property->GetNext()){

                            if($property["PREVIEW_PICTURE"] <= 0 && $property["DETAIL_PICTURE"] > 0){
                                $property["PREVIEW_PICTURE"] = $property["DETAIL_PICTURE"];
                            }


                            if($property["PREVIEW_PICTURE"] > 0){

                                $property["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                                    $property["PREVIEW_PICTURE"],
                                    [
                                        "width" => 100,
                                        "height" => 100
                                    ],
                                    BX_RESIZE_IMAGE_PROPORTIONAL,
                                    true
                                );

                                $property["PREVIEW_PICTURE"] = [
                                    "SRC" => $property["PREVIEW_PICTURE"]["src"],
                                    "WIDTH" => $property["PREVIEW_PICTURE"]["width"],
                                    "HEIGHT" => $property["PREVIEW_PICTURE"]["height"]
                                ];

                            }
                            $this->_linkedProperties[$code] = $property;
                        }
                    }

                }

            }

        }

    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Product prices">

    /**
     * @return array
     */
    public function getPrices()
    {
        return $this->_prices;
    }

    /**
     * @param array $prices
     */
    public function setPrices($prices)
    {
        $this->_prices = $prices;
    }

    /**
     *
     */
    public function setElementPrices()
    {

        if($this->_element["ID"] <= 0)
            return false;

        Loader::includeModule("catalog");

        // get product prices
        $prices = [];
        $db  = PriceTable::getList([
            "filter" => [
                "=PRODUCT_ID" => $this->_element["ID"],
            ],
            "select" => [
                "ID",
                "CATALOG_GROUP_ID",
                "PRICE",
                "CURRENCY"
            ]
        ]);
        while($price = $db->fetch())
        {
            $discount = CCatalogDiscount::GetDiscountByPrice(
                $price["ID"],
                null, // $USER->GetUserGroupArray()
                "N",
                SITE_ID
            );

            $discount = CCatalogProduct::CountPriceWithDiscount($price["PRICE"], $price["CURRENCY"], $discount);
            $price["DISCOUNT_PRICE"] = $discount;

            #$price["PRINT_DISCOUNT_VALUE"] = number_format($price["DISCOUNT_PRICE"], 2, '.', ' ');
            $price["PRINT_DISCOUNT_VALUE"] = CurrencyFormat($price["DISCOUNT_PRICE"], $price["CURRENCY"]);;
            #$price["PRINT_VALUE"] = number_format($price["PRICE"], 2, '.', ' ');
            $price["PRINT_VALUE"] = CurrencyFormat($price["PRICE"], $price["CURRENCY"]);;

            $prices[$price["CATALOG_GROUP_ID"]] = $price;

        }

        $this->setPrices($prices);
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # More photo">
    /**
     * @return array
     */
    public function getMorePhoto()
    {
        return $this->_morePhoto;
    }

    /**
     * @param array $morePhoto
     * ["PROPERTIES"]["MORE_PHOTO"]["ID"]
     */
    public function setMorePhoto()
    {
        // get data
        $morePhoto = $this->arParams["ADD_PICT_PROP"];
        $morePhoto = $this->getPropertyByCode($morePhoto);
        $morePhoto = $morePhoto["ID"];
        $morePhoto = $this->_displayProperties[$morePhoto];

        // format data

        if(is_array($this->_element["DETAIL_PICTURE"]))
        {
            $this->_morePhoto = [
                [
                    "BIG" => $this->_element["DETAIL_PICTURE"]["SRC"],
                    "SMALL" => \CFile::ResizeImageGet($this->_element["DETAIL_PICTURE"]["ID"], array("width" => 340, "height" => 340), BX_RESIZE_IMAGE_PROPORTIONAL, true, array()),
                    "THUMB" => \CFile::ResizeImageGet($this->_element["DETAIL_PICTURE"]["ID"], array("width" => 66, "height" => 66), BX_RESIZE_IMAGE_EXACT, true, array())
                ]
            ];
        }

        if(!empty($morePhoto)){
            foreach ($morePhoto as $photo){

                $photo = [
                    "BIG" => \CFile::GetPath($photo),
                    "SMALL" => \CFile::ResizeImageGet($photo, array("width" => 340, "height" => 340), BX_RESIZE_IMAGE_PROPORTIONAL, true, array()),
                    "THUMB" => \CFile::ResizeImageGet($photo, array("width" => 66, "height" => 66), BX_RESIZE_IMAGE_EXACT, true, array())
                ];

                $this->_morePhoto[] = $photo;
            }
            unset($morePhoto, $photo);
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Meta Data">

    /**
     * @return array
     */
    public function getIpropValues()
    {
        return $this->_ipropValues;
    }

    /**
     * @param array $ipropValues
     */
    public function setIpropValues()
    {
        // fill chain data
        $ipropValues = [
            "SECTIONS" => [
                "PARENT" => [
                    "NAME" => $this->_section["PARENT"]["NAME"],
                    "SECTION_PAGE_URL" => $this->_section["PARENT"]["SECTION_PAGE_URL"]
                ],
                "CURRENT" => [
                    "NAME" => $this->_section["CURRENT"]["NAME"],
                    "SECTION_PAGE_URL" => $this->_section["CURRENT"]["SECTION_PAGE_URL"]
                ]
            ],
            "META_TAGS" => []
        ];

        // fill metadata
        $metadata = new ElementValues($this->arParams["IBLOCK_ID"], $this->_element["ID"]);
        $ipropValues["META_TAGS"] = $metadata->getValues();

        $this->_ipropValues = $ipropValues;
    }

    public function addMetaData(){
        global $APPLICATION;

        // print metadata
        $APPLICATION->AddChainItem($this->arResult["IPROPERTY_VALUES"]["SECTIONS"]["PARENT"]["NAME"], $this->arResult["IPROPERTY_VALUES"]["SECTIONS"]["PARENT"]["SECTION_PAGE_URL"]);
        $APPLICATION->AddChainItem($this->arResult["IPROPERTY_VALUES"]["SECTIONS"]["CURRENT"]["NAME"], $this->arResult["IPROPERTY_VALUES"]["SECTIONS"]["CURRENT"]["SECTION_PAGE_URL"]);
        if ($this->arParams["ADD_ELEMENT_CHAIN"] === "Y") {
            $APPLICATION->AddChainItem($this->arResult["ELEMENT"]["NAME"]);
        }


        // Print Meta title
        if ($this->arParams["SET_TITLE"] === "Y")
        {
            $arResult["META_TAGS"]["TITLE"] = (
            $this->arResult["IPROPERTY_VALUES"]["META_TAGS"]["ELEMENT_META_TITLE"] != ""
                ? $this->arResult["IPROPERTY_VALUES"]["META_TAGS"]["ELEMENT_META_TITLE"]
                : $this->arResult["ELEMENT"]["NAME"]
            );
            $APPLICATION->SetTitle($arResult["META_TAGS"]["TITLE"]);

            if ($arResult["META_TAGS"]["BROWSER_TITLE"] !== '')
                $APPLICATION->SetPageProperty("title", $arResult["META_TAGS"]["TITLE"]);
        }

        // Print Meta description
        if ($arResult["META_TAGS"]["DESCRIPTION"] !== '')
            $APPLICATION->SetPageProperty("description", $this->arResult["IPROPERTY_VALUES"]["META_TAGS"]["ELEMENT_META_DESCRIPTION"]);

        // Print Meta keywords
        //$arResult["META_TAGS"]["KEYWORDS"] = $this->arResult["IPROPERTY_VALUES"]["META_TAGS"]["ELEMENT_META_KEYWORDS"];

    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Section data">

    /**
     * @return array
     */
    public function getSection()
    {
        return $this->_section;
    }

    /**
     * @param array $section
     */
    public function setSection($section)
    {
        $this->_section = $section;
    }

    public function setSectionByElement()
    {
        if($this->_element["ID"] <= 0)
            return false;
        $arSection = [];
        $section = CIBlockSection::GetList(
            [],
            [
                'IBLOCK_TYPE' => $this->arParams['IBLOCK_TYPE'],
                'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                'ID' => $this->_element["IBLOCK_SECTION_ID"]
            ],
            null,
            [
                "ID",
                "NAME",
                "IBLOCK_SECTION_ID",
                "SECTION_PAGE_URL"
            ]
        );
        if($section = $section->GetNext()){
            $arSection["CURRENT"] = $section;
            if($section["IBLOCK_SECTION_ID"] > 0){
                $section = CIBlockSection::GetList(
                    [],
                    [
                        'IBLOCK_TYPE' => $this->arParams['IBLOCK_TYPE'],
                        'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
                        'ID' => $section["IBLOCK_SECTION_ID"]
                    ],
                    null,
                    [
                        "ID",
                        "NAME",
                        "IBLOCK_SECTION_ID",
                        "SECTION_PAGE_URL"
                    ]
                );
                if($section = $section->GetNext()){
                    $arSection["PARENT"] = $section;
                }
            }
        }

        $this->setSection($arSection);
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Catalog product">

    /**
     * @return array
     */
    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * @param array $product
     */
    public function setProduct($product)
    {
        $this->_product = $product;
    }

    /**
     * Fill state by bitrix api
     * @throws \Bitrix\Main\LoaderException
     * Product fields
    0 => 'ID',
    1 => 'QUANTITY',
    2 => 'QUANTITY_TRACE',
    3 => 'QUANTITY_TRACE_ORIG',
    4 => 'WEIGHT',
    5 => 'TIMESTAMP_X',
    6 => 'PRICE_TYPE',
    7 => 'RECUR_SCHEME_LENGTH',
    8 => 'RECUR_SCHEME_TYPE',
    9 => 'TRIAL_PRICE_ID',
    10 => 'WITHOUT_ORDER',
    11 => 'SELECT_BEST_PRICE',
    12 => 'VAT_ID',
    13 => 'VAT_INCLUDED',
    14 => 'CAN_BUY_ZERO',
    15 => 'CAN_BUY_ZERO_ORIG',
    16 => 'NEGATIVE_AMOUNT_TRACE',
    17 => 'NEGATIVE_AMOUNT_TRACE_ORIG',
    18 => 'TMP_ID',
    19 => 'PURCHASING_PRICE',
    20 => 'PURCHASING_CURRENCY',
    21 => 'BARCODE_MULTI',
    22 => 'QUANTITY_RESERVED',
    23 => 'SUBSCRIBE',
    24 => 'SUBSCRIBE_ORIG',
    25 => 'WIDTH',
    26 => 'LENGTH',
    27 => 'HEIGHT',
    28 => 'MEASURE',
    29 => 'TYPE',
    30 => 'AVAILABLE',
    31 => 'BUNDLE',
    32 => 'IBLOCK_ELEMENT',
    33 => 'TRIAL_IBLOCK_ELEMENT',
    34 => 'TRIAL_PRODUCT'
     */
    public function setProductFromDB()
    {

        if (Loader::includeModule("catalog")) {
            $product = ProductTable::getList([
                "limit" => 1,
                "filter" => [
                    "ID" => $this->arParams["ELEMENT_ID"],
                    'QUANTITY_TRACE' => 'Y'
                ],
                "select" => [
                    'ID',
                    'QUANTITY',
                    'QUANTITY_TRACE'
                ]
            ]);
            if($product = $product->fetch()){
                $this->setProduct($product);
            }

        }

    }
    // </editor-fold>


    // <editor-fold defaultstate="collapsed" desc=" # Execution">

    /**
     * Formating
     * @return int
     */
    public function getForumMessgeCnt()
    {
        $property = $this->getPropertyByCode("FORUM_MESSAGE_CNT");
        $property = $this->arResult["DISPLAY_PROPERTIES"][$property["ID"]];
        if(!empty($property))
            $property = current($property);
        if($property <=0)
            $property = 0;
        return $property;
    }

    /**
     * Formating
     * @return int
     */
    public function getCompany()
    {
        $property = $this->getPropertyByCode("TRADE_MARK");
        $property = $this->arResult["LINK_PROPERTIES"][$property["ID"]];

        return $property;
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent()
    {
        if($this->startResultCache()) {
            if (Loader::includeModule("iblock")) {

                // Filing data
                $this->setElementFromDb();
                $this->setProductFromDB();
                $this->setSectionByElement();
                $this->setPropertisByParams();
                $this->fillDisplayPropertis(false);
                $this->fillLinkedProps();
                $this->setElementPrices();
                $this->setMorePhoto();
                $this->setIpropValues();

                $this->arResult = [
                    "ELEMENT" => $this->getElement(),
                    "PROPERTIES" => $this->getPropertis(),
                    "DISPLAY_PROPERTIES" => $this->getDisplayProperties(),
                    "LINK_PROPERTIES" => $this->getLinkedProperties(),
                    "DISPLAY_PROPERTIES_EXEPT" => [
                        "MORE_PHOTO",
                        "H_COMPANY",
                        "TRADE_MARK",
                        "H_PICTURES",
                        "H_PRICE_DISCOUNT",
                        "vote_count",
                        "vote_sum",
                        "rating",
                        "FORUM_TOPIC_ID",
                        "FORUM_MESSAGE_CNT"
                    ],
                    "PRICES" => $this->getPrices(),
                    "MORE_PHOTO" => $this->getMorePhoto(),
                    "DETAIL_PAGE_URL" => $this->arParams["DETAIL_PAGE_URL"],
                    "IPROPERTY_VALUES" => $this->getIpropValues()
                ];

                // используем там где идёт количественный учёт
                if(!empty($this->getProduct())){
                    $this->arResult["PRODUCT"] = $this->getProduct();
                }

                $this->includeComponentTemplate();

            }
            else{
                $this->abortResultCache();
                ShowError("IBLOCK_MODULE_NOT_INSTALLED");
            }


        }

        // print metadata
        $this->addMetaData();

        global $USER, $APPLICATION;
        if($USER->IsAdmin()){

            $arReturnUrl = array(
                "add_element" => CIBlock::GetArrayByID($this->arParams["IBLOCK_ID"], "DETAIL_PAGE_URL"),
                "delete_element" => (
                empty($arResult["SECTION_URL"])?
                    $arResult["LIST_PAGE_URL"]:
                    $arResult["SECTION_URL"]
                ),
            );

            $arButtons = CIBlock::GetPanelButtons(
                $this->arParams["IBLOCK_ID"],
                $this->arResult["ELEMENT"]["ID"],
                $this->arResult["ELEMENT"]["IBLOCK_SECTION_ID"],
                Array(
                    "RETURN_URL" => $arReturnUrl,
                    "SECTION_BUTTONS" => false,
                )
            );

            if($APPLICATION->GetShowIncludeAreas())
                $this->addIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

        }

        return [
            "ID" => $this->arResult["ELEMENT"]["ID"],
            "IBLOCK_SECTION_ID" => $this->arResult["ELEMENT"]["IBLOCK_SECTION_ID"],
            "NAME" => $this->arResult["ELEMENT"]["NAME"],
            "FORUM_MESSAGE_CNT" => $this->getForumMessgeCnt(),
            "H_COMPANY" => $this->getCompany()
        ];
    }
    // </editor-fold>
}