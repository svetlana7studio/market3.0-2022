<?
/**
 * vendor_order_list
 * vendor order list
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "vendor order list",
    "DESCRIPTION" => "vendor order list",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "vendor_order_list",
            "NAME" => "vendor order list"
        )
    ),
);
?>