<?

use Bitrix\Main\Loader,
    Bitrix\Sale\Internals\OrderTable,
    Bitrix\Sale\Basket,
    Studio7spb\Marketplace\CMarketplaceSeller,
    \Studio7spb\Marketplace\CMarketplaceOptions;

class vendorOrderList extends CBitrixComponent
{



    public function executeComponent()
    {
        # Load classes
        Loader::includeModule("sale");
        Loader::includeModule("iblock");

        #1 get company
        $this->arResult["COMPANY"] = CMarketplaceSeller::getCompanyByUser();
        $this->arResult["ORDERS"] = [];
    
        #2 get company products
        if($this->arResult["COMPANY"]["ID"] > 0){
            $this->arResult["COMPANY"]["PRODUCTS"] = array();
            $products = CIBlockElement::GetList(
                array(),
                array(
                    "IBLOCK_ID" => CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id"),
                    "PROPERTY_H_COMPANY" => $this->arResult["COMPANY"]["ID"]
                ),
                false,
                false,
                array(
                    "ID"
                )
            );
            while ($product = $products->Fetch())
            {
                $this->arResult["COMPANY"]["PRODUCTS"][] = $product["ID"];
            }
        }

        #3 get company products orders
        if($this->arResult["COMPANY"]["ID"] > 0 && !empty($this->arResult["COMPANY"]["PRODUCTS"])){

            $basketItems = Basket::getList(array(
                "filter" => array(
                    "!ORDER_ID" => false,
                    "PRODUCT_ID" => $this->arResult["COMPANY"]["PRODUCTS"]
                ),
                "select" => array("ID", "PRODUCT_ID", "QUANTITY", "NAME", "PRICE", "ORDER_ID")
            ));
            $this->arResult["COMPANY"]["PRODUCTS"] = [];
            while ($basketItem = $basketItems->fetch()){
                $this->arResult["COMPANY"]["PRODUCTS"][] = $basketItem["PRODUCT_ID"];
                $this->arResult["COMPANY"]["ITEMS"][$basketItem["PRODUCT_ID"]] = $basketItem;
                $this->arResult["ORDERS"][] = $basketItem["ORDER_ID"];
            }
            unset($basketItem);
            unset($basketItems);
        }

        #4 get orders
        if(!empty($this->arResult["ORDERS"])){
            $orders = OrderTable::getList(array(
                "order" => ["ID" => "desc"],
                "filter" => array(
                    ">PRICE" => 0,
                    "ID" => $this->arResult["ORDERS"]
                    //"STATUS_ID" => ["OP", "F"] // OP => В пути, оплачен F => // Заказ доставлен и оплачен
                ),
                'runtime' => array(
                    new \Bitrix\Main\Entity\ReferenceField(
                        'STATUS',
                        '\Bitrix\Sale\StatusLangTable',
                        array(
                            ['=this.STATUS_ID' => 'ref.STATUS_ID'],
                            ['ref.LID' => new \Bitrix\Main\DB\SqlExpression('?', LANGUAGE_ID)]
                        )
                    ),
                ),
                "select" => [
                    "ID",
                    "ACCOUNT_NUMBER",
                    "DATE_INSERT",
                    "PRICE",
                    "CURRENCY",
                    "STATUS_ID",
                    "STATUS"
                ]
            ));
            $this->arResult["ORDERS"] = array();
            while ($order = $orders->fetch()){
                $order["DATE_INSERT"] = $order["DATE_INSERT"]->format("d.m.Y");
                $this->arResult["ORDERS"][] = $order;
            }
            unset($orders);
            unset($order);
        }

        #5 get iblock elements factories
        if(!empty($this->arResult["COMPANY"]["PRODUCTS"])){

            $products = CIBlockElement::GetList(
                array(),
                array(
                    "IBLOCK_ID" => CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id"),
                    "ID" => $this->arResult["COMPANY"]["PRODUCTS"]
                ),
                false,
                false,
                array(
                    "ID",
                    "NAME",
                    "DETAIL_PAGE_URL",
                    "PREVIEW_PICTURE",
                    "PROPERTY_FACTORY",
                    "PROPERTY_Production_time_days",
                )
            );
            while ($product = $products->GetNext())
            {
                // extending product data by iblock-element
                if($product["PREVIEW_PICTURE"] > 0){
                    $this->arResult["COMPANY"]["ITEMS"][$product["ID"]]["PREVIEW_PICTURE"] = CFile::GetFileArray($product["PREVIEW_PICTURE"]);
                }
                $this->arResult["COMPANY"]["ITEMS"][$product["ID"]]["DETAIL_PAGE_URL"] = $product["DETAIL_PAGE_URL"];
            }
        }

        $this->includeComponentTemplate();
    }
}