<?php
/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

if(empty($arResult["ORDERS"])):
?>
    <div class="alert alert-danger text-center"><?=Loc::getMessage("ORDERS_EMPTY")?></div>
<?else:?>

    <?
    $sum = 0;
    $sumComission = 0;
    $sumResult = 0;
    foreach ($arResult["ORDERS"] as $order):

        ?><div class="border border-success border-order-group bg-light mb-3 p-2"><?

        // company commission
        if(is_numeric($order["PRICE"]) && $order["PRICE"] > 0){
            $comission = $order["PRICE"] * $arResult["COMPANY"]["PROPERTY_COMP_COMMISSION_VALUE"] / 100;
        }else{
            $comission = 3;
        }

        ?>

        <div class="card mb-4">

            <div class="card-header">
                <div class="card-title h4">
                    Ваши товары в заказе №<?=$order["ACCOUNT_NUMBER"];?>
                </div>
            </div>

            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th colspan="2">Наименование товара</th>
                        <th>Цена</th>
                        <th>Колличество</th>
                        <th>Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    $order["PRICE"] = 0;
                    $order["COMISSION"] = 0;
                    $order["SUM"] = 0;
                    foreach ($arResult["COMPANY"]["ITEMS"] as $product_id=>$value):


                        // output info by product
                        if($value["ORDER_ID"] == $order["ID"]):
                            if($factory == $value["PROPERTY_FACTORY_VALUE"]):
                                $value["SUM"] = $value["PRICE"] * $value["QUANTITY"];
                                $order["PRICE"] += $value["SUM"];
                                ?>
                                <tr>
                                    <td>
                                        <a href="<?=$value["DETAIL_PAGE_URL"]?>">
                                            <?if(is_array($value["PREVIEW_PICTURE"])):?>
                                                <img src="<?=$value["PREVIEW_PICTURE"]["SRC"]?>" width="75">
                                            <?else:?>
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/logo.png" width="75">
                                            <?endif;?>
                                        </a>
                                    </td>
                                    <td><a href="<?=$value["DETAIL_PAGE_URL"]?>"><?=$value["NAME"]?></a></td>
                                    <td><?=CCurrencyLang::CurrencyFormat($value["PRICE"], "RUB", true);?></td>
                                    <td><?=round($value["QUANTITY"])?> шт.</td>
                                    <td><?=CCurrencyLang::CurrencyFormat($value["SUM"], "RUB", true);?></td>
                                </tr>
                                <?
                                unset($arResult["COMPANY"]["ITEMS"][$product_id]);
                            endif;
                        endif;

                        // calculate order total

                        $order["COMISSION"] = $order["PRICE"] * $arResult["COMPANY"]["PROPERTY_COMP_COMMISSION_VALUE"] / 100;
                        $order["SUM"] = $order["PRICE"] - $order["COMISSION"];

                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>

            <div class="card-footer">

                <table class="table">
                    <thead>
                    <tr class="text-center">
                        <th>№</th>
                        <th>Статус заказа</th>
                        <th>Сумма заказа, руб.</th>
                        <th>Вознаграждение маркетплейса, руб.</th>
                        <th>Сумма за товар к выплате, руб.</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr class="text-center">
                        <td class="text-success">
                            <?=$order["ACCOUNT_NUMBER"];?>
                        </td>
                        <th><?=$order["SALE_INTERNALS_ORDER_STATUS_NAME"]?></th>
                        <td>
                            <?=\CCurrencyLang::CurrencyFormat($order["PRICE"], "RUB", false); ?>
                        </td>
                        <td>
                            <?=\CCurrencyLang::CurrencyFormat($order["COMISSION"], "RUB", false);?>
                        </td>
                        <th><?

                            echo \CCurrencyLang::CurrencyFormat($order["SUM"], "RUB", false);
                            ?></th>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>

        </div>
    <?endforeach;?>


<?endif;?>