<?php
/**
 * @var array $arParams
 * @var array $arResult
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

if(empty($arResult["ORDERS"])):
?>
    <div class="alert alert-danger text-center"><?=Loc::getMessage("ORDERS_EMPTY")?></div>
<?else:?>
<table class="table mb-5">
    <thead class="bg-warning">
    <tr class="text-center">
        <th></th>
        <th>Статус заказа</th>
        <th>Сумма заказа, руб.</th>
        <th>Вознаграждение маркетплейса, руб.</th>
        <th>Сумма за товар к выплате, руб.</th>
    </tr>
    </thead>
    <tbody>

    <?
    $sum = 0;
    $sumComission = 0;
    $sumResult = 0;
    foreach ($arResult["ORDERS"] as $order):

        if(is_numeric($order["PRICE"]) && $order["PRICE"] > 0){
            $comission = $order["PRICE"] * $arResult["COMPANY"]["PROPERTY_COMP_COMMISSION_VALUE"] / 100;
        }else{
            $comission = 0;
        }

        $result = $order["PRICE"] - $comission;

        $sum += $order["PRICE"];
        $sumComission += $order["PRICE"] * $arResult["COMPANY"]["PROPERTY_COMP_COMMISSION_VALUE"] / 100;
        $sumResult += $order["PRICE"] - $comission;
        ?>
        <tr class="text-center">
            <td class="text-success">
                <?
                echo "Заказ №";
                echo $arResult["COMPANY"]["ID"];
                echo str_pad($order["ACCOUNT_NUMBER"], 4, "0", STR_PAD_LEFT);
                ?>
            </td>
            <th><?=$order["SALE_INTERNALS_ORDER_STATUS_NAME"]?></th>
            <td>
                <?
                echo \CCurrencyLang::CurrencyFormat($order["PRICE"], "RUB", false);
                ?>
            </td>
            <td>
                <?

                echo \CCurrencyLang::CurrencyFormat($comission, "RUB", false);
                ?>
            </td>
            <th><?

                echo \CCurrencyLang::CurrencyFormat($result, "RUB", false);
                ?></th>
        </tr>
    <?endforeach;?>

    </tbody>
    <tbody class="text-center bg-light text-bold">
    <th></th>
    <th>Итого</th>
    <th><?=\CCurrencyLang::CurrencyFormat($sum, "RUB", false)?></th>
    <th><?=\CCurrencyLang::CurrencyFormat($sumComission, "RUB", false)?></th>
    <th><?=\CCurrencyLang::CurrencyFormat($sumResult, "RUB", false)?></th>
    </tbody>
</table>
<?endif;?>