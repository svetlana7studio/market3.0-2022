<?
/**
 * @var array $arParams
 * @var array $arResult
 */
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
// define('ONLY_EMAIL','none');
?>

<form action="<?=$arParams["PAGE"]?>"
      method="post">
    <?=bitrix_sessid_post()?>
    <div>
        <label>
            <input type="checkbox"
                   name="unsubscrible"
                   <?if($arResult["UF_UNSUBSCRIBE"] > 0):?>
                    checked
                   <?endif;?>
                   value="Y">
            Я хочу отписаться от рассылки с этого сайта
        </label>
    </div>
    <input type="submit"
           class="btn"
           value="Подтвердить">
</form>