<?
/**
 * user_unsubscrible
 * user unsubscrible
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "user unsubscrible",
    "DESCRIPTION" => "user unsubscrible",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "user_unsubscrible",
            "NAME" => "user unsubscrible"
        )
    ),
);
?>