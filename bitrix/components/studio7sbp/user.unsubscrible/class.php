<?php

use Bitrix\Main\Application,
    Bitrix\Main\UserTable;

class userUnsubscrible extends CBitrixComponent
{
    private $_user;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     *
     */
    public function setUser()
    {
        $user = UserTable::getList(array(
            "filter" => array(
                "ID" => $this->arParams["USER_ID"]
            ),
            "select" => array(
                "ID",
                "UF_UNSUBSCRIBE"
            )
        ));
        if($user = $user->fetch())
        {
            $this->_user = $user;
        }
    }

    public function executeComponent()
    {
        global $USER;
        if($this->arParams["USER_ID"] > 0){
            $this->setUser();

            $request = Application::getInstance()->getContext()->getRequest();
            if(check_bitrix_sessid() && $request->isPost()){

                if($request->getPost("unsubscrible") == "Y"){
                    $USER->Update($this->arParams["USER_ID"], array("UF_UNSUBSCRIBE" => 1));
                }else{
                    $USER->Update($this->arParams["USER_ID"], array("UF_UNSUBSCRIBE" => 0));
                }
                $this->setUser();
            }

            $this->arResult = $this->getUser();

            $this->includeComponentTemplate();
        }
    }
}