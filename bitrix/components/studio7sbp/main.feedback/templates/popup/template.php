<?
/**
 * @var array $arParams
 * @var array $arResult
 */

use Bitrix\Main\Localization\Loc;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
Loc::loadLanguageFile(__FILE__);
?>

<form action="<?=POST_FORM_ACTION_URI?>" method="POST">
    <?
    if(!empty($arResult["ERROR_MESSAGE"]))
    {
        ?><div class="mf-ok-text text-center"><?=$arResult["OK_MESSAGE"]?><?
        foreach($arResult["ERROR_MESSAGE"] as $v)
            ShowError($v);
        ?></div><?
    }
    if(strlen($arResult["OK_MESSAGE"]) > 0)
    {
        ?><div class="mf-ok-text text-center"><?=$arResult["OK_MESSAGE"]?></div><?
    }
    echo bitrix_sessid_post()
    ?>
    <input type="hidden"
           name="submit"
           value="submit" />

    <div class="row">

        <div class="field in-f">
            <input type="text"
                   class="inputtext form-control widget-popup--phone-mask"
                   name="user_phone"
                   value="<?=$arResult["AUTHOR_PHONE"]?>"
                   placeholder="<?=Loc::getMessage("MFT_PHONE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])):?>*<?endif?>"
                   size="0"
                   <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])):?>
                        required=""
                   <?endif?>
                   maxlength="16">
        </div>
    </div>

    <div class="row">

        <div class="field in-f">
            <input type="text"
                   class="inputtext form-control"
                   name="user_name"
                   value="<?=$arResult["AUTHOR_NAME"]?>"
                   placeholder="<?=Loc::getMessage("MFT_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>*<?endif?>"
                   <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>
                       required=""
                   <?endif?>
                   size="0">
        </div>
    </div>

    <div class="row">

        <div class="field in-f agree_row">
            <input class="agree_style_checkbox" type="checkbox" id="3" name="form_checkbox_SIMPLE_QUESTION_745[]" value="3" required="">
            <label for="3"> Согласие на обработку персональных данных</label>
        </div>
    </div>




    <?if($arParams["USE_CAPTCHA"] == "Y"):?>
        <div class="row">

            <div class="field in-f">
                <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>"
                     class="img-thumbnail img-cap"
                     width="180"
                     height="40"
                     alt="CAPTCHA">

                <input type="text"
                       name="captcha_word"
                       class="form-control form-te"
                       required=""
                       size="30"
                       maxlength="50"
                       value="">
                <i><?=Loc::getMessage("MFT_CAPTCHA_CODE")?> *</i>
            </div>
        </div>
    <?endif;?>

    <div class="widget-popup--body--form--field">
        <button type="submit"
                class="widget-popup--body--form--field--btn text-semibold btn--mm"><?=Loc::getMessage("MFT_SUBMIT")?></button>
    </div>

</form>