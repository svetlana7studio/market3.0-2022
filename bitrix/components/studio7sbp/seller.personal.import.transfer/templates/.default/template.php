<?
/**
 * @var array $arResult
 * @var array $arParams
 * @var CMain $APPLICATION
 * @var $this CBitrixComponentTemplate
 */

use Bitrix\Main\Localization\Loc;

$hasErrors = false;
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
Loc::loadLanguageFile(__FILE__);

if(!empty($arResult["MESSAGES"])):
    foreach ($arResult["MESSAGES"] as $message):
        $hasErrors = true;
        ?>
            <div class="mx-2 alert alert-<?=$message["TYPE"]?>"><?=$message["TEXT"]?></div>
        <?
    endforeach;
endif;

if($arResult["COMPANY"]["ID"] > 0):
?>

<div class="mx-2">


    <div class="delivery-service__title">
        <?=Loc::getMessage("IMPORT_STEP", ["NUM" => 1])?>. <?=Loc::getMessage("IMPORT_SAMPLE")?>
    </div>
    <p class="mb-2"><?=Loc::getMessage("IMPORT_SELECT_IB_DESCRIPTION")?></p>
    <div class="row align-items-center mb-5">
        <div class="col-auto loader-title"><?=Loc::getMessage("IMPORT_SELECT_IB")?></div>
        <div class="col">
            <select class="select" onchange="lancyImport.chooseCategory(this.value)">
                <?foreach ($arResult["IBLOCKS"] as $value):?>
                    <option <?
                            if($arResult["CURRENT_IBLOCK"] == $value["ID"])
                                echo " selected ";
                            ?>
                            value="<?=$value["ID"]?>"><?=$value["NAME"]?></option>
                <?endforeach;?>
            </select>
        </div>
    </div>

    <?foreach ($arResult["IBLOCKS"] as $value):?>
        <div class="btn-list__product btn-sample btn-sample-<?=$value["ID"]?> <?
        if($arResult["CURRENT_IBLOCK"] <> $value["ID"])
            echo " d-none ";
        ?>">
            <a href="<?=SITE_DIR?>seller/products/sample/<?=$value["ID"]?>/" download="<?=Loc::getMessage("IMPORT_DOWNLOAD_SAMPLE")?>"><?=Loc::getMessage("IMPORT_DOWNLOAD_SAMPLE")?></a>
        </div>
    <?endforeach;?>

    <div class="loader">


        <div class="row align-items-end">
            <form method="post"
                  onsubmit="disableElement(BX('import_file_btn'))"
                  action="<?echo $APPLICATION->GetCurPage()?>"
                  class="col"
                  enctype="multipart/form-data"
                  name="post_form">

                <?=bitrix_sessid_post();?>

                <input type="hidden"
                       id="current-iblock"
                       name="iblock"
                       value="<?=$arResult["CURRENT_IBLOCK"]?>">

                <div class="delivery-service__title">
                    <?=Loc::getMessage("IMPORT_STEP", ["NUM" => 2])?>. <?=Loc::getMessage("IMPORT_FILE_DOWNLOAD")?>
                </div>

                <p class="mb-2"><?=Loc::getMessage("IMPORT_FILE_DOWNLOAD_DESCRIPTION")?></p>

                <div class="row align-items-center mb-5">
                    <div class="col-auto loader-title"><?=Loc::getMessage("IMPORT_SELECT_SELECT")?> *</div>
                    <div class="col">

                        <select class="select"
                                id="user-import-iblock"
                                name="iblock">
                            <option value=""><?=Loc::getMessage("IMPORT_SECTION_EMPTY")?></option>
                            <?foreach ($arResult["IBLOCKS"] as $value):?>
                                <option <?
                                if($arResult["CURRENT_IBLOCK"] == $value["ID"])
                                    echo " selected ";
                                ?>
                                        value="<?=$value["ID"]?>"><?=$value["NAME"]?></option>
                            <?endforeach;?>
                        </select>

                    </div>
                </div>


                <div class="loader-title">
                    <?if(is_array($arResult["COMPANY"]["PROPERTY_IMPORT_FILE"])):?>
                        <label>
                            <?=Loc::getMessage("IMPORT_FILE_ALTER")?>:
                            <?=$arResult["COMPANY"]["PROPERTY_IMPORT_FILE"]["ORIGINAL_NAME"]?>
                        </label>
                        <input type="file" class="form-control" name="file" />
                    <?else:?>
                        <label><?=Loc::getMessage("IMPORT_FILE_CHOOSE")?> *</label>
                        <input type="file" class="form-control" name="file" />
                    <?endif;?>
                </div>

                <div class="btn-list__product">
                    <button type="submit"
                            name="import_file"
                            id="import_file_btn">
                        <?=$arResult["COMPANY"]["PROPERTY_IMPORT_FILE_VALUE"] > 0 ? Loc::getMessage("IMPORT_FILE_UPDATE") : Loc::getMessage("IMPORT_FILE_ADD")?>
                    </button>
                </div>

            </form>

            <form method="post"
                  onsubmit="disableElement(document.getElementById('import_file_delete'))"
                  action="<?echo $APPLICATION->GetCurPage()?>"
                  class="col"
                  enctype="multipart/form-data"
                  name="post_form">

                <?=bitrix_sessid_post();?>

                <input type="hidden"
                       name="file"
                       value="delete_file" />

                <div class="btn-list__product">

                    <button type="submit"
                            id="import_file_delete">
                        <?=Loc::getMessage("IMPORT_FILE_DELETE")?>
                    </button>

                </div>

            </form>

        </div>
    </div>

    <?if($arResult["COMPANY"]["PROPERTY_IMPORT_FILE_VALUE"] > 0 && $arResult["CURRENT_IBLOCK"] > 0 && $hasErrors == false):?>

        <div class="loader">
            <div class="delivery-service__title">
                <?=Loc::getMessage("IMPORT_STEP", ["NUM" => 3])?>. <?=Loc::getMessage("IMPORT_FILE_VALIDATE")?>
            </div>
            <p class="mb-2">
                <?=Loc::getMessage("IMPORT_FILE_VALIDATE_DESC")?>
            </p>
            <div id="progress-import-bar"
                 class="progress d-none">
                <div class="progress-bar bg-success" style="width: 100%;"></div>
            </div>
            <div id="progress-import-statistic" class="alert alert-light d-none">
                Добавлено товаров: <span id="progress-import-add">0</span>
                <br>
                Обновлено товаров: <span id="progress-import-update">0</span>
            </div>
            <div id="progress-import-status" class="alert alert-light d-none"></div>

            <div class="btn-list__product">
                <div class="mb-2">
                    <button class="btn btn-success"
                            id="start-cheking-btn"
                            onclick="lancyImport.setStart(1)"><?=Loc::getMessage("IMPORT_VALIDATE_START")?></button>
                </div>
                <div class="mb-2">
                    <button class="btn btn-success pulsate-btn d-none"
                            id="start-import-btn"
                            onclick="lancyImport.importStart(1)"><?=Loc::getMessage("IMPORT_PROCESS_START")?></button>
                </div>
                <div class="mb-2 d-none" id="products-list-link">
                    <button class="btn btn-success"
                            onclick="lancyImport.goToProducts()"><?=Loc::getMessage("IMPORT_PRODUCTS_LIST")?></button>
                </div>
            </div>


        </div>

    <?endif;?>
</div>

<?else:?>
    <div class="s7sbp--marketplace--saler--lk--form ff--roboto">
        <div class="alert alert-danger mt-3">Вы не являетесь администратором компании.</div>
    </div>
<?
endif;
unset($value);
?>