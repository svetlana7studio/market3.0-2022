<?
/**
 * @var array $arResult
 * @var array $arParams
 * @var $this CBitrixComponentTemplate
 */

$hasErrors = false;
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");

if(!empty($arResult["MESSAGES"])):
    foreach ($arResult["MESSAGES"] as $message):
        $hasErrors = true;
        ?>
            <div class="alert alert-<?=$message["TYPE"]?>"><?=$message["TEXT"]?></div>
        <?
    endforeach;
endif;

if($arResult["COMPANY"]["ID"] > 0):
?>

<div class="mx-2">
    <form method="post"
          onsubmit="disableElement(document.getElementById('import_file_btn'))"
          action="<?echo $APPLICATION->GetCurPage()?>"
          enctype="multipart/form-data"
          class="mb-3"
          name="post_form">

        <?=bitrix_sessid_post();?>

        <a href="<?=SITE_DIR?>upload/excel/sample/import.xlsx" class="text-success text-14 excell-download mr-3">Excel - образец файла загрузки</a>
        <br>
        <br>


        <label>Файл</label>
        <input type="file" name="file">
        <br>
        <br>

        <input type="submit"
               name="import_file"
               id="import_file_btn"
               class="btn"
               value="<?=$arResult["COMPANY"]["PROPERTY_IMPORT_FILE_VALUE"] > 0 ? "Загрузить новый файл импорта" : "Загрузить"?>">

    </form>

    <?if($arResult["COMPANY"]["PROPERTY_IMPORT_FILE_VALUE"] > 0 && $hasErrors == false):?>


        <div class="alert alert-success">Файл импорта уже загружен. Можно начинать его проверку и процесс импорта. Либо можно его заменить, загрузив новый файл импорта на первом шаге.</div>

        <div id="progress-import-bar"
             class="progress d-none">
            <div class="progress-bar bg-success" style="width: 100%;"></div>
        </div>

        <div id="progress-import-status" class="alert alert-light d-none"></div>

        <button class="btn btn-success"
                onclick="lancyImport.setStart(1)">Начать проверку файла импорта</button>
        <button class="btn btn-success d-none"
                id="start-import-btn"
                onclick="lancyImport.importStart(1)">Начать процесс импорта</button>

    <?endif;?>
</div>

<?else:?>
    <div class="s7sbp--marketplace--saler--lk--form ff--roboto">
        <div class="alert alert-danger mt-3">Вы не являетесь администратором компании.</div>
    </div>
<?endif;?>
