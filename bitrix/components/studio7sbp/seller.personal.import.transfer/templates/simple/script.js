/**
 * https://learn.javascript.ru/class
 * Может нужно использовать и класс
 * Это не просто синтаксический сахар
 * @param arParams
 */

/**
 * Create constructor function
 * @param arParams
 */
function lancyImport(arParams) {
    this.arParams = arParams;
}

/**
 * Prototipe methods
 * каждый прототип функции имеет свойство constructor по умолчанию,
 * поэтому нам нет необходимости его создавать
 */
lancyImport.prototype.showStatus = function() {
    console.info(this.arParams);
};

lancyImport.prototype.setStart = function (val) {

    var status = "<strong>" + this.arParams.messages.status + ":</strong> " + this.arParams.messages.work;
    status = '<div class="alert alert-success">' + status + '</div>';

    $(this.arParams.status)
        .removeClass("d-none")
        .html(status);
    if(val){
        // Preparato set status bar and set progress to null
        this.showWaitWindow();
        $(this.arParams.bar)
            .removeClass("d-none")
            .find(".progress-bar")
            .css({width:"0%"})
            .text("0%");
            //.addClass("progress-bar-animated")
            //.addClass("progress-bar-striped");

        // Ajax

        BX.ajax.runComponentAction(
            'studio7sbp:seller.personal.import.transfer',
            'getElements',
            {
                mode:'class',
                data: {
                    work_start: "Y",
                    sessid: BX.bitrix_sessid()
                }
            }).then(BX.delegate(this.workOnload, this));

    }
    else{
        this.closeWaitWindow();
    }

};

lancyImport.prototype.workOnload = function (response) {

    try {
        if (response.status === 'success') {

            var status = "<strong>" + this.arParams.messages.status + ":</strong> " + response.data.status;

            if($(this.arParams.status).find(".alert-success").length > 0){
                $(this.arParams.status).find(".alert-success").html(status);
            }else{
                $(this.arParams.status).append('<div class="alert alert-success">' + status + '</div>');
            }

            if(!!response.data.error){
                $(this.arParams.status).append('<div class="alert alert-danger">' + status + '<br />' + response.data.error + '</div>');
            }

            // Preparato set status bar and set progress to null
            $(this.arParams.bar)
                .removeClass("d-none")
                .find(".progress-bar")
                //.animate({width:response.data.p + "%"}, 50)
                .css({width:response.data.p + "%"})
                .text(response.data.p + "%");


            if(response.data.p == "100"){
                $(this.arParams.bar).find(".progress-bar").removeClass("progress-bar-animated").removeClass("progress-bar-striped");
            }else{
                $(this.arParams.bar).find(".progress-bar").addClass("progress-bar-animated").addClass("progress-bar-striped");
            }


            // Ajax

            if(response.data.lastid > 0 || response.data.step === "load"){
                // send
                BX.ajax.runComponentAction(
                    'studio7sbp:seller.personal.import.transfer',
                    'getElements',
                    {
                        mode:'class',
                        data: {
                            work_start: "load",
                            lastid: response.data.lastid,
                            sessid: BX.bitrix_sessid()
                        }
                    }).then(BX.delegate(this.workOnload, this));
            }else{
                // stop
                //status = "<strong>" + this.arParams.messages.status + ":</strong> " + 11111111111111;
                //$(this.arParams.status).html(status);
                //lancyImport.setStart(0);
                this.closeWaitWindow();
                if(!!response.data.can_load){
                    // check error exist
                    if($(this.arParams.status).find(".alert-danger").length <=0)
                        $(this.arParams.import.start).removeClass("d-none");
                }
            }

        }
    }
    catch(e)
    {
        this.closeWaitWindow();
        alert('Сбой в получении данных');
    }
};

lancyImport.prototype.showWaitWindow = function () {
    var html = '<div class="product-add-loading">';
        html += '<div class="main-ui-loader main-ui-loader-custom main-ui-show" data-is-shown="true" style="width: 130px; height: 130px;">';
        html += '<svg class="main-ui-loader-svg" viewBox="25 25 50 50">';
        html += '<circle class="main-ui-loader-svg-circle" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10" style="stroke: rgb(102, 58, 242);">';
        html += '</circle>';
        html += '</svg>';
        html += '</div>';
        html += '</div>';
        $("body").append(html);
};

lancyImport.prototype.importStart = function (val) {
// Ajax

    var status = "<strong>" + this.arParams.messages.status + ":</strong> " + this.arParams.messages.work;
    $(this.arParams.status)
        .removeClass("d-none")
        .html(status);
    if(val){
        // Preparato set status bar and set progress to null
        this.showWaitWindow();
        $(this.arParams.bar)
            .removeClass("d-none")
            .find(".progress-bar")
            .css({width:"0%"})
            .text("0%");
            //.addClass("progress-bar-animated")
            //.addClass("progress-bar-striped");

        // Ajax

        BX.ajax.runComponentAction(
            'studio7sbp:seller.personal.import.transfer',
            'uploadElements',
            {
                mode:'class',
                data: {
                    work_start: "Y",
                    sessid: BX.bitrix_sessid()
                }
            }).then(BX.delegate(this.importOnload, this));

    }
    else{
        this.closeWaitWindow();
    }

};

lancyImport.prototype.importOnload = function (response) {

    try {
        if (response.status === 'success') {

            var status = "<strong>" + this.arParams.messages.status + ":</strong> " + response.data.status;
            $(this.arParams.status).html(status);


            console.info(response.data);

            // Preparato set status bar and set progress to null
            $(this.arParams.bar)
                .removeClass("d-none")
                .find(".progress-bar")
                .css({width:response.data.p + "%"})
                .text(response.data.p + "%");
                //.addClass("progress-bar-animated")
                //.addClass("progress-bar-striped");

            if(response.data.p == "100"){
                $(this.arParams.bar).find(".progress-bar").removeClass("progress-bar-animated").removeClass("progress-bar-striped");
            }else{
                $(this.arParams.bar).find(".progress-bar").addClass("progress-bar-animated").addClass("progress-bar-striped");
            }

            // Ajax

            if(response.data.lastid > 0){
                // send
                BX.ajax.runComponentAction(
                    'studio7sbp:seller.personal.import.transfer',
                    'uploadElements',
                    {
                        mode:'class',
                        data: {
                            work_start: "Y",
                            lastid: response.data.lastid,
                            sessid: BX.bitrix_sessid()
                        }
                    }).then(BX.delegate(this.importOnload, this));
            }else{
                // stop
                //status = "<strong>" + this.arParams.messages.status + ":</strong> " + 11111111111111;
                //$(this.arParams.status).html(status);
                // lancyImport.importStart(0);
                this.closeWaitWindow();

            }

        }
    }
    catch(e)
    {
        this.closeWaitWindow();
        alert('Сбой в получении данных');
    }
};

lancyImport.prototype.closeWaitWindow = function () {
    $(".product-add-loading").remove();
};

/**
 * Using
 * @type {lancyImport}
 */
var lancyImport = new lancyImport({
    bar: "#progress-import-bar",
    status: "#progress-import-status",
    messages: {
        status: "Статус",
        work: "Исполняется процесс проверки данных файла импорта"
    },
    import: {
        start: "#start-import-btn"
    }
});