<?
$MESS = [
    "SALLER_UPLOAD_ERROR" => "Некоректно заполнены данные",
    "SALLER_UPLOAD_VALIDATE_EMPTY_FIELD" => "Поле FIELD имеет пустое значение",
    "SALLER_UPLOAD_VALIDATE_EMPTY_NAME" => "Не заполнено название товара",
    "SALLER_UPLOAD_VALIDATE_EMPTY_SECTION" => "Не заполнен код раздела из классификатора",
    "SALLER_UPLOAD_VALIDATE_IMAGE_EXIST" => "Заданная картинка не доступа по адресу: IMG",
    "SALLER_UPLOAD_VALIDATE_REQUIRED" => "Обязательное поле",
];