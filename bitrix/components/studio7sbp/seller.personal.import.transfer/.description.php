<?
/**
 * seller.personal.import.transfer
 * seller personal import transfer
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "seller personal import transfer",
    "DESCRIPTION" => "seller personal import transfer",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "seller.personal.import.transfer",
            "NAME" => "seller personal import transfer"
        )
    ),
);
