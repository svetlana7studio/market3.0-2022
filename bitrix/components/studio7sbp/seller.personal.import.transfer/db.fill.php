<?
use Bitrix\Main\Loader,
    Bitrix\Iblock\ElementTable,
    Bitrix\Iblock\SectionTable,
    Bitrix\Iblock\PropertyTable,
    Bitrix\Main\Web\Json,
    Studio7spb\Marketplace\ImportSettingsTable;

// <editor-fold defaultstate="collapsed" desc=" # Prepare">
$_SERVER["DOCUMENT_ROOT"] = $argv[5];
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
require $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule("iblock");

if (ini_get('mbstring.func_overload') & 2) {
    ini_set("mbstring.func_overload", 0);
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # arParams">
$arParams = [
    "IBLOCK_ID" => 6,
    "COMPANY_ID" => $argv[1],
    "USER_ID" => $argv[2],
    "FILE" => $argv[3],
    "FILE_IB" => $argv[4],
    "PATTERN" => "IMPORT_DATA_VENDOR_IB_",
    "PROFILE" => 0,
    "IMPORT_DATA_COMPANY_START" => 0,
    "IMPORT_FIELDS_LIST" => [],
    "HEAD" => []
];



if($arParams["FILE"] > 0){
    $arParams["FILE"] = CFile::GetFileArray($arParams["FILE"]);
}

if(empty($arParams["FILE"])){
    echo Json::encode([
        "table_db" => $arParams["DB_TABLE"],
        "p" => 100,
        "lastid" => null,
        "status" => null
    ]);
    die;
}else{
    $arParams["FILE"] = $_SERVER["DOCUMENT_ROOT"] . $arParams["FILE"]["SRC"];
}

$arParams["DB_TABLE"] = "b_studio7spb_import_" . $arParams["COMPANY_ID"];

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # get profile">
$value = ImportSettingsTable::getList(array(
    "filter" => [
        "%CODE" => $arParams["PATTERN"],
        "VALUE" => $arParams["FILE_IB"]
    ],
    "limit" => 1,
    "select" => ["CODE"]
));
if($value = $value->fetch())
    $arParams["PROFILE"] = (int) str_replace($arParams["PATTERN"], "", $value["CODE"]);

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # get start parsing IMPORT_DATA_CATALOG_START_*">
$value = ImportSettingsTable::getList(array(
    "filter" => ["CODE" => "IMPORT_DATA_CATALOG_START_" . $arParams["PROFILE"]],
    "limit" => 1,
    "select" => ["VALUE"]
));
if($value = $value->fetch())
    $arParams["IMPORT_DATA_COMPANY_START"] = $value["VALUE"];

$value = ImportSettingsTable::getList(array(
    "filter" => ["CODE" => "IMPORT_FIELDS_LIST_" . $arParams["PROFILE"]],
    "limit" => 1,
    "select" => ["VALUE"]
));
if($value = $value->fetch())
    $arParams["IMPORT_FIELDS_LIST"] = unserialize($value["VALUE"]);

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Get Company">
$DB->Query("DROP TABLE IF EXISTS " . $arParams["DB_TABLE"] . ";");
$DB->Query("CREATE TABLE IF NOT EXISTS " . $arParams["DB_TABLE"] . "
(
    ID INT NOT NULL AUTO_INCREMENT,
    DATA TEXT,
    PRIMARY KEY (ID)
);");
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Load">
$objReader = PHPExcel_IOFactory::createReaderForFile($arParams["FILE"]);
$objReader->setReadDataOnly(true);
$objPHPExcel = $objReader->load($arParams["FILE"]);
// </editor-fold>

$worksheet = $objPHPExcel->getSheet(0);


# Size
# $highestRow = $worksheet->getHighestDataRow();
# $highestColumn = $worksheet->getHighestColumn();

$arResult["error"] = [];
foreach ($worksheet->getRowIterator() as $row) {

    if($row->getRowIndex() >= $arParams["IMPORT_DATA_COMPANY_START"]){

        $cellIterator = $row->getCellIterator();

        # ignore blank empty rows
        $blank = true;
        foreach ($cellIterator as $cell) {

            $value = $cell->getValue();
            if (!$value || $value == "null")
                $value = "";

            if(!empty($value))
            {
                $blank = false;
                break;
            }

        }

        if($blank === true)
            break;

        foreach ($cellIterator as $cell){

            $value = $cell->getValue();

            if(strlen($value) <= 0 || $value == 'null')
                $value = "";

            foreach ($arParams["IMPORT_FIELDS_LIST"][$cell->getColumn()] as $field){

                if($field == "IE_NAME" && empty($value)){
                    $arResult["error"][$cell->getColumn()] = [
                        "NAME" => "Не заполненное обязательное поле",
                        "FIELD" => $cell->getColumn(),
                        "COUNT" => $arResult["error"][$cell->getColumn()]["COUNT"] + 1
                    ];
                }

                elseif ($field == "IE_IBLOCK_SECTION_ID"){
                    // FILE_IB
                    if(empty($value)) {
                        $arResult["error"][$cell->getColumn()] = [
                            "NAME" => "Не заполненное обязательное поле FIELD (COUNT)",
                            "FIELD" => $cell->getColumn(),
                            "COUNT" => $arResult["error"][$cell->getColumn()]["COUNT"] + 1
                        ];
                    }
                    else{
                        $section = SectionTable::getList([
                            "limit" => 1,
                            "select" => ["ID"],
                            "filter" => [
                                "IBLOCK_ID" => $arParams["FILE_IB"],
                                "ID" => $value
                            ]
                        ])->getSelectedRowsCount();
                        if($section <= 0)
                            $arResult["error"][$cell->getColumn()] = [
                                "NAME" => "В поле FIELD указан несуществующий раздел (COUNT)",
                                "FIELD" => $cell->getColumn(),
                                "COUNT" => $arResult["error"][$cell->getColumn()]["COUNT"] + 1
                            ];
                    }

                }

                elseif(substr( $field, 0, 7 ) === "IP_PROP"){
                    $property = substr($field, 7);
                    $property = PropertyTable::getList([
                        "filter" => ["ID" => $property, "ACTIVE" => "Y"],
                        "select" => ["ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_TYPE", "DEFAULT_VALUE", "LINK_IBLOCK_ID", "IS_REQUIRED"]
                    ]);
                    if($property = $property->fetch()){

                        if($property["IS_REQUIRED"] == "Y"){

                            if(empty($value)){
                                $arResult["error"][$cell->getColumn()] = [
                                    "NAME" => "Не заполненное обязательное поле FIELD (COUNT)",
                                    "FIELD" => $cell->getColumn(),
                                    "COUNT" => $arResult["error"][$cell->getColumn()]["COUNT"] + 1
                                ];
                            }else{

                                switch ($property["PROPERTY_TYPE"]){
                                    case "L":
                                        $property = CIBlockPropertyEnum::GetList([], [
                                            "IBLOCK_ID" => $property["IBLOCK_ID"],
                                            "PROPERTY_ID" => $property["ID"],
                                            "VALUE" => $value
                                        ]);
                                        if($property->SelectedRowsCount() <= 0){
                                            $arResult["error"][$cell->getColumn()] = [
                                                "NAME" => "Не верно указано значение в поле FIELD (COUNT)",
                                                "FIELD" => $cell->getColumn(),
                                                "COUNT" => $arResult["error"][$cell->getColumn()]["COUNT"] + 1
                                            ];
                                        }
                                        break;
                                    case "E":
                                        $property = ElementTable::getList([
                                            "select" => ["ID"],
                                            "filter" => [
                                                "IBLOCK_ID" => $property["LINK_IBLOCK_ID"],
                                                "NAME" => $value
                                            ],
                                            "limit" => 1
                                        ]);
                                        if($property->getSelectedRowsCount() <= 0){
                                            $arResult["error"][$cell->getColumn()] = [
                                                "NAME" => "Не верно указано значение в поле FIELD (COUNT)",
                                                "FIELD" => $cell->getColumn(),
                                                "COUNT" => $arResult["error"][$cell->getColumn()]["COUNT"] + 1
                                            ];
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }


            }


            // add to array
            $rows[$row->getRowIndex()][$cell->getColumn()] = $value;
        }
    }
}

if(!empty($rows)){
    foreach ($rows as $row) {
        $DB->Insert($arParams["DB_TABLE"], ["DATA" => "'".serialize($row)."'"], $err_mess.__LINE__);
    }
}

$arResult = [
    "p" => 25,
    "step" => "load",
    "status" => "Загрузка данных во временную таблицу",
    "error" => $arResult["error"]
];

if(empty($arResult["error"]))
{
    unset($arResult["error"]);
}
else
{
    $errors = [];
    foreach ($arResult["error"] as $error) {
        $error["NAME"] = str_replace([
            "FIELD",
            "COUNT"
        ], [
            $error["FIELD"],
            $error["COUNT"]
        ], $error["NAME"]);
        $errors[] = $error["NAME"];
    }
    $arResult["error"] = implode("<br>", $errors);
}

$arResult = Json::encode($arResult);
echo $arResult;