<?
/**
 * Component implement Controllerable
 * Новые Ajax-запросы в 1С-Битрикс
 * https://prominado.ru/blog/novye-ajax-zaprosy-v-bitrix/
 */

use Bitrix\Main\Application,
    Bitrix\Main\Loader,
    Bitrix\Main\Engine\Contract\Controllerable,
    Bitrix\Iblock\SectionTable,
    Bitrix\Catalog\Model\Product,
    Studio7spb\Marketplace\CMarketplaceOptions,
    Bitrix\Main\Localization\Loc,
    Bitrix\Iblock\IblockTable,
    Bitrix\Iblock\PropertyTable,
    Bitrix\Main\Web\Json,
    Studio7spb\Marketplace\ImportSettingsTable;


class sellerPersonalImportTransfer extends CBitrixComponent implements Controllerable
{

    private $_company;
    private $_messages;

    /**
     * @var array
     */
    private $_iblocks;

    /**
     * @var array
     */
    private $_iblocksID;

    /**
     * @var array
     */
    private $_iblockSections;

    /**
     * @var int
     */
    private $_currentIblock = 0;

    // <editor-fold defaultstate="collapsed" desc=" # Errors">

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->_messages;
    }

    /**
     * @param array $messages
     */
    public function setMessages($messages)
    {
        $this->_messages = $messages;
    }

    /**
     * @param array $error
     */
    public function setMessage($message)
    {
        $this->_messages[] = $message;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Company">
    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->_company = $company;
    }

    public function setCompanyFromDB()
    {
        if($this->arParams["USER_ID"] <= 0){
            global $USER;
            $this->arParams["USER_ID"] = $USER->GetID();
        }
        if($this->arParams["COMPANY_IBLOCK_ID"] <= 0){
            $this->arParams["COMPANY_IBLOCK_ID"] = CMarketplaceOptions::getInstance()->getOption("company_iblock_id");
        }

        if($this->arParams["USER_ID"] > 0){

            $company = CIBlockElement::GetList(
                [],
                [
                    "IBLOCK_TYPE" => "marketplace",
                    "IBLOCK_ID" => $this->arParams["COMPANY_IBLOCK_ID"],
                    "PROPERTY_COMP_USER" => $this->arParams["USER_ID"]
                ],
                false,
                false,
                [
                    "ID",
                    "IBLOCK_ID",
                    "PROPERTY_IMPORT_FILE",
                    "PROPERTY_COMP_COMMISSION"
                ]
            );

            if($company = $company->Fetch()){

                $company["PROPERTY_COMP_COMMISSION_VALUE"] = floatval($company["PROPERTY_COMP_COMMISSION_VALUE"]);
                if($company["PROPERTY_COMP_COMMISSION_VALUE"] <= 0)
                    $company["PROPERTY_COMP_COMMISSION_VALUE"] = 10;

                $company["PROPERTY_IMPORT_FILE"] = CFile::GetFileArray($company["PROPERTY_IMPORT_FILE_VALUE"]);
                $this->_currentIblock = $company["PROPERTY_IMPORT_FILE"]["DESCRIPTION"];
                $this->setCompany($company);

            }

        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Ajax-методы должны быть с постфиксом Action Check elements">

    /**
     * Обязательный имплиментированный метод
     * @return array
     */
    public function configureActions()
    {
        // Сбрасываем фильтры по-умолчанию (ActionFilter\Authentication и ActionFilter\HttpMethod)
        // Предустановленные фильтры находятся в папке /bitrix/modules/main/lib/engine/actionfilter/
        return [
            'getElements' => [ // Ajax-метод
                'prefilters' => [],
            ],
        ];
    }

    /**
     * @param $work_start
     * @param int $lastid
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public function getElementsAction($work_start, $lastid=0)
    {
        Loader::includeModule("iblock");

        global $DB;
        $limit = 1;
        $arResult = [
            "p" => null,
            "lastid" => null,
            "status" => null
        ];

        if(($work_start == "Y" || $work_start == "load") && check_bitrix_sessid())
        {
            switch ($work_start){
                case "Y":
                    # первый запуск
                    $this->setCompanyFromDB();
                    $this->arResult["COMPANY"] = $this->getCompany();
                    if($this->arResult["COMPANY"]["ID"] > 0){
                        $arResult = shell_exec('php ' . dirname(__FILE__) . '/db.fill.php ' . $this->arResult["COMPANY"]["ID"] . ' ' . $this->arParams["USER_ID"] . ' ' . $this->arResult["COMPANY"]["PROPERTY_IMPORT_FILE_VALUE"] . ' ' . $this->arResult["COMPANY"]["PROPERTY_IMPORT_FILE"]["DESCRIPTION"] . ' ' . $_SERVER["DOCUMENT_ROOT"]);
                        $arResult = Json::decode($arResult);
                    }

                    break;
                case "load":
                    # Работа с временной таблицей
                    // TODO start
                    $this->setCompanyFromDB();
                    $this->arResult["COMPANY"] = $this->getCompany();
                    if($lastid <= 0)
                        $lastid = 0;


                    if($this->arResult["COMPANY"]["ID"] > 0){
                        $table = "b_studio7spb_import_" . $this->arResult["COMPANY"]["ID"];
                        $rs = $DB->Query("select * from $table where ID>$lastid order by ID asc limit " . $limit .";");
                        while ($ar = $rs->Fetch())
                        {
                            /*
                             * Validate data here
                             */
                            $errors = $this->validateTmpData(unserialize($ar["DATA"]), $this->arResult["COMPANY"]["PROPERTY_IMPORT_FILE"]["DESCRIPTION"]);
                            $lastid = intval($ar["ID"]);
                        }
                    }


                    $rsLeftBorder = $DB->Query("select ID from $table where ID <= $lastid order by ID asc", false, "FILE: ".__FILE__."<br /> LINE: ".__LINE__);
                    $leftBorderCnt = $rsLeftBorder->SelectedRowsCount();

                    $rsAll = $DB->Query("select ID from $table;", false, "FILE: ".__FILE__."<br /> LINE: ".__LINE__);
                    $allCnt = $rsAll->SelectedRowsCount();

                    $p = round(100*$leftBorderCnt/$allCnt, 2);

                    $strNum = $lastid + 2;
                    if($p < 100){
                        $arResult = [
                            "p" => $p,
                            "lastid" => $lastid,
                            "status" => "Обработка строки №".$strNum,
                        ];
                    }else{
                        $arResult = [
                            "p" => $p,
                            "lastid" => null,
                            //"status" => "Обработка строки №" . $lastid . "<br> Процесс проверки данных файла импорта завершён",
                            "status" => "Обработка строки №" . $strNum . "<br> Процесс проверки данных файла импорта завершён",
                            "can_load" => "Y"
                        ];
                    }

                    if(!empty($errors)){
                        $arResult["error"] = implode("<br>", $errors);
                    }


                    // TODO end
                    break;
            }

            /// Процесс проверки данных файла импорта завершён

        }

        /**
         * отправляем данные
         */
        return $arResult;
    }

    /**
     * @param $data
     */
    public function validateTmpData($data, $iblockID){
        Loc::loadLanguageFile(__FILE__);
        $errors = array();

        // <editor-fold defaultstate="collapsed" desc=" # get IMPORT FIELDS LIST">
        $PATTERN = "IMPORT_DATA_VENDOR_IB_";
        $value = ImportSettingsTable::getList(array(
            "filter" => [
                "%CODE" => $PATTERN,
                "VALUE" => $iblockID
            ],
            "limit" => 1,
            "select" => ["CODE"]
        ));
        if($value = $value->fetch())
            $PROFILE = (int) str_replace($PATTERN, "", $value["CODE"]);

        $SIMPLE = [];
        $IMPORT_FIELDS_LIST = [];
        $values = ImportSettingsTable::getList(array(
            //"filter" => ["CODE" => $IMPORT_FIELDS_LIST],
            "filter" => ["CODE" => [
                "IMPORT_SIMPLE_ITEMS_" . $PROFILE,
                "IMPORT_FIELDS_LIST_" . $PROFILE
            ]],
            "select" => ["VALUE", "CODE"]
        ));
        while($value = $values->fetch())
        {
            switch ($value["CODE"]){
                case "IMPORT_SIMPLE_ITEMS_" . $PROFILE:
                    //$value["VALUE"] = str_replace("null", "", $value["VALUE"]);
                    $SIMPLE = unserialize($value["VALUE"]);
                    foreach ($SIMPLE as $letter=>$value){
                        if($value[3] == 'null')
                            $value[3] = '';
                        $SIMPLE[$letter] = $value;
                    }
                    break;
                case "IMPORT_FIELDS_LIST_" . $PROFILE:
                    $IMPORT_FIELDS_LIST = unserialize($value["VALUE"]);
                    break;
            }
        }
        // </editor-fold>

        foreach ($IMPORT_FIELDS_LIST as $letter=>$fields){
            foreach ($fields as $field){
                if(!empty($field)) {

                    // проверка полей
                    if(substr($field, 0, 3) === "IE_"){
                        $code = substr($field, 3);
                        switch ($code){
                            #case "NAME":
                            #    if(empty($data[$letter]))
                            #        $errors[] = Loc::getMessage("SALLER_UPLOAD_VALIDATE_EMPTY_NAME");
                            #    break;
                            case "IBLOCK_SECTION_ID":
                                if(intval($data[$letter]) <= 0)
                                    $errors[] = Loc::getMessage("SALLER_UPLOAD_VALIDATE_EMPTY_SECTION");
                                break;
                            case "PREVIEW_PICTURE":
                            case "DETAIL_PICTURE":
                                // $arFields[$code] = explode(",", $arFields[$code]);
                                // $arFields[$code] = CFile::MakeFileArray($arFields[$code][0]);

                                //$headers = @fopen($data[$letter]);
                                //if(substr($headers[0], 9, 3) == '200') {
                                #if (exif_imagetype($data[$letter], "r"))
                                #else
                                #    $errors[] = Loc::getMessage("SALLER_UPLOAD_VALIDATE_IMAGE_EXIST", ["IMG" => $data[$letter]]);
                                break;

                        }

                    }

                    if($SIMPLE[$letter][3] === Loc::getMessage("SALLER_UPLOAD_VALIDATE_REQUIRED")){
                        if(strlen($data[$letter]) <= 0)
                            $errors[] = Loc::getMessage("SALLER_UPLOAD_VALIDATE_EMPTY_FIELD", ["FIELD" =>  $letter]);
                    }

                }
            }
        }



        return $errors;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Ajax-методы должны быть с постфиксом Action Upload elements">
    public function uploadElementsAction($work_start, $lastid=0){

        if($work_start == "Y" && check_bitrix_sessid())
        {
            Loader::includeModule("iblock");

            global $DB;
            $limit = 1;
            $this->setCompanyFromDB();
            $this->arResult["COMPANY"] = $this->getCompany();

            if($this->arResult["COMPANY"]["ID"] > 0){
                $table = "b_studio7spb_import_" . $this->arResult["COMPANY"]["ID"];
                $rs = $DB->Query("select * from $table where ID>$lastid order by ID asc limit " . $limit .";");
                while ($ar = $rs->Fetch())
                {
                    /*
                     * Import data to iblock
                     */
                    $data = $ar["DATA"];
                    $data = unserialize($data);
                    $procedure = $this->setElementToIBlock($data, $this->arResult["COMPANY"]["PROPERTY_IMPORT_FILE"]["DESCRIPTION"], $this->arResult["COMPANY"]["ID"], $this->arResult["COMPANY"]["PROPERTY_COMP_COMMISSION_VALUE"]);

                    $lastid = intval($ar["ID"]);
                }
            }


            $rsLeftBorder = $DB->Query("select ID from $table where ID <= $lastid order by ID asc", false, "FILE: ".__FILE__."<br /> LINE: ".__LINE__);
            $leftBorderCnt = $rsLeftBorder->SelectedRowsCount();

            $rsAll = $DB->Query("select ID from $table;", false, "FILE: ".__FILE__."<br /> LINE: ".__LINE__);
            $allCnt = $rsAll->SelectedRowsCount();

            $p = round(100*$leftBorderCnt/$allCnt, 2);

            if($p < 100){
                $arResult = [
                    "p" => $p,
                    "lastid" => $lastid,
                    "procedure" => $procedure,
                    "status" => "Обработка строки №".$lastid,
                ];
            }else{
                $arResult = [
                    "p" => $p,
                    "lastid" => null,
                    "procedure" => $procedure,
                    "status" => "Процесс импорта завершён"
                ];
            }
        }


        /**
         * отправляем данные
         */
        return $arResult;
    }

    public function setElementToIBlock($data, $iblockID, $company, $COMP_COMMISSION){

        $neo = new CIBlockElement();
        # базовый фильтр для идентификации
        $arFilter = [
            "IBLOCK_ID" => $iblockID,
        ];

        // <editor-fold defaultstate="collapsed" desc=" # get profile">
        $PATTERN = "IMPORT_DATA_VENDOR_IB_";
        $value = ImportSettingsTable::getList(array(
            "filter" => [
                "%CODE" => $PATTERN,
                "VALUE" => $iblockID
            ],
            "limit" => 1,
            "select" => ["CODE"]
        ));
        if($value = $value->fetch())
            $PROFILE = (int) str_replace($PATTERN, "", $value["CODE"]);

        $IMPORT_FIELDS_LIST = "IMPORT_FIELDS_LIST_" . $PROFILE;
        $value = ImportSettingsTable::getList(array(
            "filter" => ["CODE" => $IMPORT_FIELDS_LIST],
            "limit" => 1,
            "select" => ["VALUE"]
        ));
        if($value = $value->fetch())
            $IMPORT_FIELDS_LIST = unserialize($value["VALUE"]);

        // IMPORT_DATA_CATALOG_AVAILABLE_
        $value = ImportSettingsTable::getList(array(
            "filter" => ["CODE" => "IMPORT_DATA_CATALOG_AVAILABLE_" . $PROFILE],
            "limit" => 1,
            "select" => ["VALUE"]
        ));
        if($value = $value->fetch())
            $CATALOG_AVAILABLE = $value["VALUE"];


        $arFields = [];
        $arProperties = [];
        $arPrices = [];
        $arPricesVendor = [];
        $arCatalog = [];

        foreach ($IMPORT_FIELDS_LIST as $letter=>$fields){
            foreach ($fields as $field){
                if(!empty($field)) {

                    // Набор полей
                    if(substr($field, 0, 3) === "IE_"){
                        $code = substr($field, 3);
                        $arFields[$code] = $data[$letter];
                        switch ($code){
                            case "PREVIEW_PICTURE":
                            case "DETAIL_PICTURE":
                                $arFields[$code] = explode(",", $arFields[$code]);
                                $arFields[$code] = CFile::MakeFileArray($arFields[$code][0]);
                                break;
                        }

                    }

                    // properties
                    if(substr( $field, 0, 7 ) === "IP_PROP"){
                        $arProperties[substr($field, 7)] = $data[$letter];
                    }

                    // sale prices
                    if($field === "ICAT_PRICE1_PRICE"){
                        $arPrices[1] = $data[$letter];
                        $arPricesVendor[3] = $data[$letter];
                    }
                    if($field === "ICAT_PRICE2_PRICE"){
                        $arPrices[2] = $data[$letter];
                        $arPricesVendor[4] = $data[$letter];
                    }

                    // catalog avaliable
                    if(ToUpper($field) == "ICAT_AVAILABLE"){

                        if(ToUpper($CATALOG_AVAILABLE) == ToUpper($data[$letter]))
                            $arCatalog["AVAILABLE"] = "Y";
                        else
                            $arCatalog["AVAILABLE"] = "N";

                    }

                    if(ToUpper($field) == "ICAT_IDENT"){

                        foreach ($fields as $tmpField){
                            if($tmpField !== $field){
                                if(substr( $tmpField, 0, 7 ) === "IP_PROP"){
                                    $arFilter["PROPERTY_" . substr($tmpField, 7)] = $data[$letter];
                                }
                            }
                        }

                    }

                    // product tp groopen ICAT_GROUP
                    if($field === "ICAT_GROUP"){
                        $arCatalog["TP_GROUP"] = $data[$letter];
                    }

                }
            }
        }

        // </editor-fold>

        $PROPERTY_VALUES = $arProperties;
        $properties = [];
        foreach ($PROPERTY_VALUES as $key=>$value){
            $properties[] = $key;
        }

        if(!empty($properties)){
            $properties = PropertyTable::getList([
                "filter" => ["ID" => $properties, "ACTIVE" => "Y"],
                "select" => ["ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_TYPE", "DEFAULT_VALUE", "LINK_IBLOCK_ID", "IS_REQUIRED"]
            ]);
            while ($value = $properties->fetch()){
                if($value["CODE"] == "H_COMPANY"){
                    $PROPERTY_VALUES[$value["ID"]] = $company;
                    continue;
                }

                switch ($value["PROPERTY_TYPE"]){
                    case "L":
                        if(!empty($PROPERTY_VALUES[$value["ID"]]))
                        {
                            $property_enums = CIBlockPropertyEnum::GetList([], [
                                "IBLOCK_ID" => $value["IBLOCK_ID"],
                                "PROPERTY_ID" => $value["ID"],
                                "VALUE" => $PROPERTY_VALUES[$value["ID"]]
                            ]);
                            while($enum_fields = $property_enums->Fetch())
                            {
                                $PROPERTY_VALUES[$value["ID"]] = $enum_fields["ID"];
                            }

                        }
                        break;
                    case "E":
                        if(!empty($PROPERTY_VALUES[$value["ID"]]))
                        {
                            $enum_fields = \Bitrix\Iblock\ElementTable::getList([
                                "select" => ["ID"],
                                "filter" => [
                                    "IBLOCK_ID" => $value["LINK_IBLOCK_ID"],
                                    "NAME" => $PROPERTY_VALUES[$value["ID"]]
                                ],
                                "limit" => 1
                            ]);
                            if($enum_fields = $enum_fields->fetch()){
                                $PROPERTY_VALUES[$value["ID"]] = $enum_fields["ID"];
                            }

                        }
                        break;
                }

            }
        }


        // $arFilter
        // formating filter props
        foreach ($arFilter as $key=>$value) {
            if (strpos($key, "PROPERTY_") !== false) {
                $property = str_replace("PROPERTY_", "", $key);
                if($property > 0){
                    $property = PropertyTable::getList([
                        "filter" => [
                            "IBLOCK_ID" => $arFilter["IBLOCK_ID"],
                            "ID" => $property
                        ],
                        "limit" => 1
                    ]);
                    if($property = $property->fetch()){
                        if($property["CODE"] == "H_COMPANY") {
                            unset($arFilter[$key]);
                        }else{
                            switch ($property["PROPERTY_TYPE"]) {
                                case "E":
                                    $property = \Bitrix\Iblock\ElementTable::getList([
                                        "filter" => [
                                            "IBLOCK_ID" => $property["LINK_IBLOCK_ID"],
                                            "NAME" => $value
                                        ],
                                        "limit" => 1,
                                        "select" => ["ID"]
                                    ]);

                                    if($property = $property->fetch()){
                                        $arFilter[$key] = $property["ID"];
                                    }
                                    break;
                                case "L":
                                    $property = CIBlockPropertyEnum::GetList([], [
                                        "IBLOCK_ID" => $property["IBLOCK_ID"],
                                        "PROPERTY_ID" => $property["ID"],
                                        "VALUE" => $value
                                    ]);
                                    if($property = $property->Fetch()){
                                        $arFilter[$key] = $property["ID"];
                                    }

                                    break;
                            }
                        }
                    }
                }

            }
        }

        // Add or update
        $procedure = "update";
        $element = CIBlockElement::GetList([],$arFilter,false,false, ["ID", "NAME", "XML_ID", "IBLOCK_ID"]);
        if($element = $element->Fetch())
        {

            if(ToUpper($CATALOG_AVAILABLE) == ToUpper($arFields["ACTIVE"]))
                $arFields["ACTIVE"] = "Y";
            else
                $arFields["ACTIVE"] = "N";

            $arFields["DETAIL_TEXT_TYPE"] = "text";
            $arFields["PREVIEW_TEXT_TYPE"] = "text";

            $neo->Update($element["ID"], $arFields);

            CIBlockElement::SetPropertyValuesEx($element["ID"], $iblockID, $PROPERTY_VALUES);
            CIBlockElement::SetPropertyValues($element["ID"], $iblockID, $company, "H_COMPANY");
            
            foreach ($arPricesVendor as $PRICE_ID=>$PRICE)
                $this->setNormalPrice($element["ID"], floatval($PRICE), $PRICE_ID);

            foreach ($arPrices as $PRICE_ID=>$PRICE)
            {
                // Добавляем комиссию к цене
                $PRICE = floatval($PRICE);
                if(is_numeric($COMP_COMMISSION) && $COMP_COMMISSION > 0)
                {
                    // $PRICE = $PRICE / (1 - floatval($COMP_COMMISSION) * 0.01);
                    $COMMISSION_PRICE = floatval($COMP_COMMISSION) * $PRICE / 100;
                    $PRICE += $COMMISSION_PRICE;
                }

                $this->setNormalPrice($element["ID"], $PRICE, $PRICE_ID);
            }

        }
        else{
            
            $procedure = "add";
            $arFields["IBLOCK_ID"] = $iblockID;
            if(ToUpper($CATALOG_AVAILABLE) == ToUpper($arFields["ACTIVE"]))
                $arFields["ACTIVE"] = "Y";
            else
                $arFields["ACTIVE"] = "N";
            $arFields["DETAIL_TEXT_TYPE"] = "text";
            $arFields["PREVIEW_TEXT_TYPE"] = "text";
            $arFields["PROPERTY_VALUES"] = $PROPERTY_VALUES;
            $arFields["IBLOCK_SECTION_ID"] = intval($arFields["IBLOCK_SECTION_ID"]);
            ///unset($arFields["PREVIEW_PICTURE"]);
            ///unset($arFields["DETAIL_PICTURE"]);
            /// https://cdn.vseinstrumenti.ru/images/goods/electrika-i-svet/aksessuary/744718/1200x800/52329644.jpg

            $ID = $neo->Add($arFields);

            if($ID > 0){
                CIBlockElement::SetPropertyValues($ID, $iblockID, $company, "H_COMPANY");
                Product::add(array(
                    "ID" => $ID,
                    "MEASURE" => 5,
                    "QUANTITY" => 100000,
                    "VAT_INCLUDED" => "N",
                    //"VAT_ID" => $vat // VAT_ID
                ));

                foreach ($arPricesVendor as $PRICE_ID=>$PRICE)
                    $this->setNormalPrice($ID, floatval($PRICE), $PRICE_ID);

                foreach ($arPrices as $PRICE_ID=>$PRICE)
                {
                    // Добавляем комиссию к цене
                    $PRICE = floatval($PRICE);
                    if(is_numeric($COMP_COMMISSION) && $COMP_COMMISSION > 0)
                    {
                        // $PRICE = $PRICE / (1 - floatval($COMP_COMMISSION) * 0.01);
                        $COMMISSION_PRICE = floatval($COMP_COMMISSION) * $PRICE / 100;
                        $PRICE += $COMMISSION_PRICE;
                    }

                    $this->setNormalPrice($ID, $PRICE, $PRICE_ID);
                }

            }
        }

        return $procedure;
    }

    /**
     * Remove the directory and its content (all files and subdirectories).
     * @param string $dir the directory name
     */
    public function rmrf($dir) {
        foreach (glob($dir) as $file) {
            if (is_dir($file)) {
                $this->rmrf("$file/*");
                rmdir($file);
            } else {
                unlink($file);
            }
        }
    }

    public function setNormalPrice($PRODUCT_ID, $price, $PRICE_TYPE_ID){
        if($price > 0){
            $arFields = Array(
                "PRODUCT_ID" => $PRODUCT_ID,
                "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
                "PRICE" => $price,
                "CURRENCY" => "RUB"
            );

            $res = CPrice::GetList(
                array(),
                array(
                    "PRODUCT_ID" => $PRODUCT_ID,
                    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
                )
            );

            if ($arr = $res->Fetch())
            {
                CPrice::Update($arr["ID"], $arFields);
            }
            else
            {
                CPrice::Add($arFields);
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Save import file">

    public function deleteImportFile()
    {
        $this->setCompanyFromDB();
        CIBlockElement::SetPropertyValuesEx($this->_company["ID"], $this->_company["IBLOCK_ID"], ["IMPORT_FILE" => ["VALUE" => ["del" => "Y"]]]);
    }

    /**
     * Set import file to company
     * @param $file
     */
    public function setImportFile($file, $iblocID){

        if($this->checkImportFile($file, $iblocID > 0)){
            if($file["tmp_name"]){
                $this->setCompanyFromDB();
                if($this->_company["PROPERTY_IMPORT_FILE_VALUE"] > 0){
                    CFile::Delete($this->_company["PROPERTY_IMPORT_FILE_VALUE"]);
                }
                CIBlockElement::SetPropertyValuesEx(
                    $this->_company["ID"],
                    $this->_company["IBLOCK_ID"],
                    ["IMPORT_FILE" => ["VALUE" => $file, "DESCRIPTION" => $iblocID]]
                );
            }
        }else{
            $this->setMessage(array(
                "TYPE" => "danger",
                "TEXT" => Loc::getMessage("SALLER_UPLOAD_ERROR")
            ));
        }
    }

    /**
     * @param $file
     * @return bool
     */
    public function checkImportFile($file){
        $file = $file["name"];
        $file = explode(".", $file);
        $file = array_pop($file);
        $file = $file === "xlsx";
        return $file;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Iblocks">
    /**
     * @return array
     */
    public function getIblocks()
    {
        return $this->_iblocks;
    }

    /**
     * @param array $iblocks
     */
    public function setDBIblocks()
    {
        $iblocks = IblockTable::getList([
            "order" => ["NAME" => "ASC"],
            "filter" => [
                "IBLOCK_TYPE_ID" => "marketplace",
                "ID" => $this->arParams["IBLOCK_ID"],
                "ACTIVE" => "Y"
            ],
            "select" => [
                "ID",
                "NAME",
                "CODE"
            ]
        ]);
        while ($iblock = $iblocks->fetch())
        {
            $this->_iblocks[] = $iblock;
            $this->_iblocksID[] = $iblock["ID"];
        }
    }
    // </editor-fold>

    /**
     * @return array
     */
    public function getIblockSections()
    {
        return $this->_iblockSections;
    }

    /**
     * @param array $iblockSections
     */
    public function setIblockSections()
    {
        $iblockSections = [];

        $sections = SectionTable::getList([
            "order" => ["left_margin"=>"asc"],
            "filter" => [
                "IBLOCK_ID" => $this->_iblocksID
            ],
            "select" => [
                "ID",
                "IBLOCK_ID",
                "DEPTH_LEVEL",
                "NAME"
            ]
        ]);
        while ($section = $sections->fetch())
            $iblockSections[$section["IBLOCK_ID"]][] = $section;

        $this->_iblockSections = $iblockSections;
    }

    public function executeComponent()
    {

        $request = Application::getInstance()->getContext()->getRequest();

        if($request->isPost()){

            if($request->getPost("file") == "delete_file"){
                $this->deleteImportFile();
            }else{
                $this->setImportFile($request->getFile("file"), $request->getPost("iblock"));
            }
        }

        $this->setDBIblocks();
        $this->setCompanyFromDB();
        $this->setIblockSections();

        $this->arResult["COMPANY"] = $this->getCompany();
        $this->arResult["MESSAGES"] = $this->getMessages();
        $this->arResult["IBLOCKS"] = $this->getIblocks();
        # $this->arResult["IBLOCK_SECTIONS"] = $this->getIblockSections();
        $this->arResult["CURRENT_IBLOCK"] = $this->_currentIblock;

        $this->includeComponentTemplate();
    }
}