<?
/**
 * price_changer
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "price_changer",
    "DESCRIPTION" => "price_changer",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "price_changer",
            "NAME" => "price_changer"
        )
    ),
);