<?
use Bitrix\Main\Loader,
    Bitrix\Main\Entity\ExpressionField,
    Bitrix\Iblock\ElementTable;

/**
 * Class s7CatalogTop
 * Вариант старого API
 */
class CatalogTopRecomended extends CBitrixComponent
{

    // <editor-fold defaultstate="collapsed" desc=" # Statemnt">

    /**
     * @var array
     */
    private $_products;

    /**
     * @var array
     */
    private $_recomData;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Iblock Elements">

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->_products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->_products = $products;
    }

    /**
     * @param $product
     */
    public function setProduct($product)
    {
        $this->_products[] = $product;
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public function setRecomData()
    {
        $this->_recomData = [];

        if($this->arParams["ELEMENT_ID"] > 0)
        {
            if (Loader::includeModule("sale"))
            {
                $orders = array();
                $items = \Bitrix\Sale\Basket::getList(array(
                    "filter" => array(
                        "!ORDER_ID" => false,
                        "PRODUCT_ID" => $this->arParams["ELEMENT_ID"]
                    ),
                    "limit" => 5,
                    "select" => array("ORDER_ID")
                ));
                while ($item = $items->fetch())
                {
                    $orders[] = $item["ORDER_ID"];
                }

                /**
                 * Get basket items by orders exept that product
                 */
                if(!empty($orders)){
                    $this->_recomData = [];
                    $items = \Bitrix\Sale\Basket::getList(array(
                        "filter" => array(
                            "ORDER_ID" => $orders,
                            "!PRODUCT_ID" => $this->arParams["ELEMENT_ID"]
                        ),
                        "limit" => 20,
                        "select" => array("PRODUCT_ID")
                    ));
                    while ($item = $items->fetch())
                    {
                        $this->_recomData[] = $item["PRODUCT_ID"];
                    }
                }

            }
        }

    }


    /**
     * @return array
     */
    public function getRecomData()
    {
        return $this->_recomData;
    }

    /**
     * From
     */
    public function setProductsIb()
    {

        $products = CIBlockElement::GetList(
            [
                //"ID" => "ASC"
                //$this->arParams["ELEMENT_SORT_FIELD"] => $this->arParams["ELEMENT_SORT_ORDER"],
                //$this->arParams["ELEMENT_SORT_FIELD2"] => $this->arParams["ELEMENT_SORT_ORDER2"]
            ],
            [
                'IBLOCK_TYPE' => $this->arParams["IBLOCK_TYPE"],
                'IBLOCK_ID' => $this->arParams["IBLOCK_ID"],
                '!IBLOCK_SECTION_ID' => false,
                "ID" => $this->getRecomData(),
                'ACTIVE' => 'Y'
            ],
            false,
            ["nTopCount" => $this->arParams["ELEMENT_COUNT"]],
            [
                "ID",
                "IBLOCK_ID",
                "NAME",
                "IBLOCK_SECTION_ID",
                "PREVIEW_PICTURE",
                "DETAIL_PICTURE",
                "DETAIL_PAGE_URL",
                "PROPERTY_DISCOUNT",
                "PROPERTY_ANYTOS_VENDOR",
                "PROPERTY_TRADE_MARK.CODE",
                "PROPERTY_TRADE_MARK.NAME",
                "PROPERTY_MULTIPLICITY", // Кратность
                "PROPERTY_MOQ", // Минимальный опт
                "PROPERTY_ANYTOS_DETAIL_PICTURE", // Картинка с anytos.ru
                "CATALOG_GROUP_4", // price_red
                "CATALOG_GROUP_6", // price_yellow
                "CATALOG_GROUP_8", // price_green
            ]
        );

        while ($product = $products->GetNext()){

            if($product["PREVIEW_PICTURE"] > 0){

                $product["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                    $product["PREVIEW_PICTURE"],
                    [
                        "height" => 200,
                        "width" => 200
                    ],
                    BX_RESIZE_IMAGE_EXACT,
                    false
                );
                $product["PREVIEW_PICTURE"] = [
                    "SRC" => $product["PREVIEW_PICTURE"]["src"],
                    "HEIGHT" => 200,
                    "WIDTH" => 200,
                    "TITLE" => $product["NAME"],
                    "ALT" => "..."
                ];

            }else{
                if(!empty($product["PROPERTY_ANYTOS_DETAIL_PICTURE_VALUE"])){
                    $product["PREVIEW_PICTURE"] = [
                        "SRC" => $product["PROPERTY_ANYTOS_DETAIL_PICTURE_VALUE"],
                        "HEIGHT" => 200,
                        "WIDTH" => 200,
                        "TITLE" => $product["NAME"],
                        "ALT" => "..."
                    ];
                }
            }

            $this->setProduct($product);
        }
    }

    // </editor-fold>


    // <editor-fold defaultstate="collapsed" desc=" # Execute Component">

    public function executeComponent()
    {

        if($this->startResultCache(false, false)) {
            if (!Loader::includeModule("iblock")) {
                $this->abortResultCache();
                ShowError("IBLOCK_MODULE_NOT_INSTALLED");
                return;
            }

            $this->setRecomData();

            if(empty($this->getRecomData())){
                $this->abortResultCache();
            }else{
                $this->setProductsIb();

                $this->arResult = [
                    "ITEMS" => $this->getProducts()
                ];
            }


            $this->includeComponentTemplate();

        }

    }

    // </editor-fold>

}