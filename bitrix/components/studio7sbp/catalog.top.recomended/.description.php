<?
/**
 * catalog_top_recomended
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "catalog_top_recomended",
    "DESCRIPTION" => "catalog_top_recomended",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "catalog_top_recomended",
            "NAME" => "catalog_top_recomended"
        )
    ),
);