<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult['ITEMS']))
    return;

$arParams["LINE_ELEMENT_COUNT"] = 5;
?>


<div class="position-relative">

    <!-- Start Partners -->
    <div class="hm306 s7spb-row s7spb-row-justify-start d-overflow-hidden" id="recomended-items">

        <?
        foreach ($arResult['ITEMS'] as $key=>$arItem):

            $generalParams["KEY"] = $key;
            $arItem["JS_ID_BASIS"] = "recomended-item";

            $APPLICATION->IncludeComponent(
                'bitrix:catalog.item',
                'catalog.top',
                array(
                    'RESULT' => array(
                        'ITEM' => $arItem,
                        'PRICES' => $arResult["PRICES"],
                        'PRODUCT_PRICES' => $arResult["PRODUCT_PRICES"],
                        'TYPE' => 'card',
                        'BIG_LABEL' => 'N',
                        'BIG_DISCOUNT_PERCENT' => 'N',
                        'BIG_BUTTONS' => 'N',
                        'SCALABLE' => 'N'
                    ),
                    'PARAMS' => $generalParams
                        + array('SKU_PROPS' => $arResult['SKU_PROPS'][$arItem['IBLOCK_ID']])
                ),
                $component,
                array('HIDE_ICONS' => 'Y')
            );

            unset($arItem);

        endforeach;
        ?>

    </div>

    <?if(count($arResult['ITEMS']) > $arParams["LINE_ELEMENT_COUNT"]):?>
        <div class="carousel-controls">
            <div onclick="koorochkaRecomendedSlider.slidePrev();" class="carousel-control-prev"></div>
            <div onclick="koorochkaRecomendedSlider.slideNext();" class="carousel-control-next"></div>
        </div>
    <?endif;?>

</div>