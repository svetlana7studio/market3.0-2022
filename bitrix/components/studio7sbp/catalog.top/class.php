<?

use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Loader,
    Bitrix\Iblock\ElementTable,
    Bitrix\Iblock\SectionTable;

/**
 * Class s7CatalogTop
 * Вариант старого API
 */
class s7CatalogTop extends CBitrixComponent
{

    // <editor-fold defaultstate="collapsed" desc=" # Statemnt">

    /**
     * @var array
     */
    private $_products;

    /**
     * @var array
     */
    private $_sections;

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Iblock Elements">

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->_products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->_products = $products;
    }

    /**
     * @param $product
     */
    public function setProduct($product)
    {
        $this->_productsp[] = $product;
    }

    /**
     * From
     */
    public function setProductsIb()
    {

        $this->setProductsD7();
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     * https://dev.1c-bitrix.ru/support/forum/forum6/topic87635/
     * Вероятно CIBlock::ReplaceDetailUrl()
     * CIBlockSection::GetNavChain
     * https://dev.1c-bitrix.ru/support/forum/forum6/topic35920/
     */
    public function setProductsD7()
    {
        $products = ElementTable::getList([
            "filter" => [
                'IBLOCK_ID' => $this->arParams["IBLOCK_ID"],
                'ACTIVE' => 'Y'
            ],
            "select" => [
                'ID',
                'IBLOCK_ID',
                'IBLOCK_SECTION_ID'
            ],
            "limit" => $this->arParams["ELEMENT_COUNT"]
        ]);
        d($products->getSelectedRowsCount());
        while ($product = $products->fetch())
        {
            //$product['DETAIL_PAGE_URL'] = makeUrl($product["IBLOCK_SECTION_ID"]);
            //$product['DETAIL_PAGE_URL'] =


        }

        // Вероятно CIBlock::ReplaceDetailUrl()

    }

    public function setProductsOld()
    {
        $products = CIBlockElement::GetList(
            [
                //$this->arParams["ELEMENT_SORT_FIELD"] => $this->arParams["ELEMENT_SORT_ORDER"],
                //$this->arParams["ELEMENT_SORT_FIELD2"] => $this->arParams["ELEMENT_SORT_ORDER2"]
            ],
            [
                //'IBLOCK_TYPE' => $this->arParams["IBLOCK_TYPE"],
                'IBLOCK_ID' => $this->arParams["IBLOCK_ID"],
                'ACTIVE' => 'Y'
            ],
            false,
            ["nTopCount" => $this->arParams["ELEMENT_COUNT"]],
            [
                "ID",
                "NAME",
            ]
        );
        d($products->SelectedRowsCount());
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Sections">

    /**
     * @return array
     */
    public function getSections()
    {
        return $this->_sections;
    }

    /**
     * @param array $sections
     */
    public function setSections($sections)
    {

    }

    /**
     * @param array $sections
     */
    public function setSectionsIb()
    {
        /**
         * https://dev.1c-bitrix.ru/community/webdev/user/71716/
         * Ребят акститесь делать запросы в рекурсии это нонсенс
         * Ваша задача решаеться проще и изящнее
         *
         * SELECT bis.ID FR OM b_iblock_section as bis
        LEFT JOIN b_iblock_section as bis2 ON bis2.ID = #ID_Раздела  товара#
        where bis.LEFT_MARGIN < bis2.LEFT_MARGIN
        AND bis.RIGHT_MARGIN > bis2.RIGHT_MARGIN
        AND bis.IBLOCK_ID = bis2.IBLOCK_ID
        AND bis.DEPTH_LEVEL = 1
         *
         * Дерево
         *
        SELECT bis.ID, bis.CODE, bis.DEPTH_LEVEL FROM b_iblock_section as bis
        LEFT JOIN b_iblock_section as bis2 ON bis2.ID = 614
        where bis.LEFT_MARGIN < bis2.LEFT_MARGIN
        AND bis.RIGHT_MARGIN > bis2.RIGHT_MARGIN
        AND bis.IBLOCK_ID = bis2.IBLOCK_ID

         *
         * Path
         *
        SELECT bis.CODE FROM b_iblock_section as bis
        LEFT JOIN b_iblock_section as bis2 ON bis2.ID = 614
        where bis.LEFT_MARGIN < bis2.LEFT_MARGIN
        AND bis.RIGHT_MARGIN > bis2.RIGHT_MARGIN

         */

        $sections = SectionTable::getList([
            "order" => [
                "BS_DEPTH_LEVEL" => "asc"
            ],
            "filter" => [
                'IBLOCK_ID' => $this->arParams["IBLOCK_ID"],
                'ID' => 614,
                //'CODE' => "fotoalbomy",
                //"DEPTH_LEVEL" => 6
            ],
            //"limit" => 1,
            "select" => [
                "IBLOCK_SECTION_ID",
                "ID",
                "NAME",
                "BS_CODE" => "BS.CODE",
                "BS_DEPTH_LEVEL" => "BS.DEPTH_LEVEL",
            ],
            'runtime' => [
                new Bitrix\Main\Entity\ReferenceField(
                    'BS',
                    'Bitrix\Iblock\Section',
                    [
                        '>this.LEFT_MARGIN' => 'ref.LEFT_MARGIN',
                        '<this.RIGHT_MARGIN' => 'ref.RIGHT_MARGIN'
                    ],
                    [
                        'join_type' => 'LEFT'
                    ]
                )
            ]
        ]);

        //

        /**
         * /catalog/odezhda_i_tekstil/odezhda_dlja_devochek/tekstil_dlya_detey/majki_dlja_devochek/nizhnee_bele_dlja_devochek
         * https://anytos.ru/catalog/odezhda_i_tekstil/tekstil_dlya_detey/odezhda_dlja_devochek/nizhnee_bele_dlja_devochek/majki_dlja_devochek/
         */

        while ($section = $sections->fetch())
        {
            d($section);
        }
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Execute Component">

    public function executeComponent()
    {

        if($this->startResultCache(false, false)) {
            if (!Loader::includeModule("iblock")) {
                $this->abortResultCache();
                ShowError("IBLOCK_MODULE_NOT_INSTALLED");
                return;
            }

            $this->setProductsIb();
            $this->setSectionsIb();

            $this->includeComponentTemplate();

        }

    }

    // </editor-fold>

}