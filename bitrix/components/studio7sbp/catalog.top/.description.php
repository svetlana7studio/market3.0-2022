<?
/**
 * catalog_top
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "catalog_top",
    "DESCRIPTION" => "catalog_top",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "catalog_top",
            "NAME" => "catalog_top"
        )
    ),
);