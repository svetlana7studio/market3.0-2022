<?
/**
 * iblock_sections_menu_top
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "iblock_sections_menu_top",
    "DESCRIPTION" => "iblock_sections_menu_top",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "iblock_sections_menu_top",
            "NAME" => "iblock_sections_menu_top"
        )
    ),
);