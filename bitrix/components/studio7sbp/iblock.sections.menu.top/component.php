<?
/** @var CBitrixComponent $this */

use Bitrix\Main\Loader;

/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["ID"] = intval($arParams["ID"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$aMenuLinksExt = [];

/**
 * Проблема с кешированием
 *
if($this->StartResultCache())
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
    }
    else
    {

        //$this->EndResultCache();
    }
}
*/

Loader::includeModule("iblock");
$sections = CIBlockSection::GetList(
    [],
    [
        "IBLOCK_TYPE" => "marketplace",
        "IBLOCK_ID" => "2",
        "!UF_MENU" => false,
        "ACTIVE" => "Y",
    ],
    false,
    [
        "ID",
        "NAME",
        "SECTION_PAGE_URL",
    ]
);
while ($section = $sections->GetNext())
{
    $aMenuLinksExt[] = [
        $section["NAME"],
        $section["SECTION_PAGE_URL"],
        "CAT"
    ];
}

return $aMenuLinksExt;