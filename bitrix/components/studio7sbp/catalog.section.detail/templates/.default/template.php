<?
/**
 * @var array $arParams
 * @var array $arResult
 */

if(empty($arResult["SECTION"]))
    return;
?>

<div class="bg-white border-radius-10 px4 py3 mb6"><?=$arResult["SECTION"]["DESCRIPTION"]?></div>