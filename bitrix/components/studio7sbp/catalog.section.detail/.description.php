<?
/**
 * catalog_section_detail
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "catalog_section_detail",
    "DESCRIPTION" => "catalog_section_detail",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "catalog_section_detail",
            "NAME" => "catalog_section_detail"
        )
    ),
);