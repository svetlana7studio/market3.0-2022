<?

class catalogSectionDetail extends CBitrixComponent
{
    private $_section;

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->_section;
    }

    /**
     * @param mixed $section
     */
    public function setSection($section)
    {
        $this->_section = $section;
    }

    public function setSectionFromIb()
    {
        if($this->startResultCache()) {
            if (!CModule::IncludeModule("iblock")) {
                $this->abortResultCache();
            } else {
                $rsSections = CIBlockSection::GetList(
                    [
                        "NAME" => "asc"
                    ],
                    [
                        "IBLOCK_TYPE"=>$this->arParams["IBLOCK_TYPE"],
                        "IBLOCK_ID"=>$this->arParams["IBLOCK_ID"],
                        "ID"=>$this->arParams["ID"],
                        "CODE"=>$this->arParams["CODE"],
                        "GLOBAL_ACTIVE"=>"Y",
                        "ACTIVE"=>"Y",
                        "IBLOCK_ACTIVE"=>"Y",
                        //"<=DEPTH_LEVEL"=>"3"
                    ],
                    false,
                    [
                        "ID",
                        "NAME",
                        "CODE",
                        "DESCRIPTION"
                    ]);

                if($arSection = $rsSections->Fetch())
                {
                    $this->setSection($arSection);
                }

                $this->endResultCache();
            }
        }

    }

    public function executeComponent()
    {
        $this->setSectionFromIb();

        $this->arResult = [
            "SECTION" => $this->getSection()
        ];

        $this->includeComponentTemplate();
    }
}