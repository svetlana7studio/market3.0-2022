<?
/**
 * seller_address
 * seller_address
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "seller_address",
    "DESCRIPTION" => "seller_address",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "seller_address",
            "NAME" => "seller_address"
        )
    ),
);
