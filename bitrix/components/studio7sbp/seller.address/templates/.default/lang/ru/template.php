<?
$MESS = array(
    "PROFILE_TYPE" => "Тип профиля",
    "PROFILE_NAME" => "ФИО",
    "PROFILE_ADDRESS" => "Адрес (НАЧИНАЯ С УЛИЦЫ введите местоположение вашей компании)",
    "PROFILE_TITLE" => "Выбрать профиль",
    "PROFILE_ADD" => "Добавить профиль покупателя",
    "PROFILE_EDIT_TITLE" => "Изменить профиль",
    "PROFILE_LIST_BTN" => "В список",
    "PROFILE_ADD_BTN" => "Добавить",
    "PROFILE_EDIT_BTN" => "Изменить",
    "PROFILE_DELETE_BTN" => "Удалить",
    "PROFILE_LOCATION_DELETE" => "Удалить адрес",
    "PROFILE_ADRESS_BTN" => "Ещё адрес",
    "ERROR_NAME_EMPTY" => "Не заполнено поле ФИО"
);