<?
/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */
use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>
<div class="s7sbp--marketplace--saler--lk--right--inner"
     id="user_profiles">

    <?if(
            !empty($arResult["PROFILES"])
            && empty($arResult["PROFILE"])
            && (empty($arResult["COUNT"]) || $arResult["COUNT"] > 0)
    ):?>
        <!--- Мои профили --->
        <h1><?=Loc::getMessage("PROFILE_TITLE")?></h1>

        <?foreach ($arResult["PROFILES"] as $profile):?>
            <div class="row mb-3">
                <div class="col col-md-6">
                    <div class="border border-secondary p-3">
                        <h5 class="text-secondary"><?=Loc::getMessage("PROFILE_NAME")?></h5>
                        <h4><?=$profile["NAME"]?></h4>
                        <h5 class="text-secondary mt-3"><?=Loc::getMessage("PROFILE_ADDRESS")?></h5>
                        <h5><?=$arResult["LOCATIONS"][$profile["ID"]]["LOCATION_VAL"]?></h5>
                    </div>
                </div>
                <div class="col-4 col-sm-auto">
                    <a href="<?=$APPLICATION->GetCurPageParam("action=edit&id=" . $profile["ID"], array("action", "id", "count"))?>" class="btn btn-primary fa fa-edit"></a>
                    <a href="<?=$APPLICATION->GetCurPageParam("action=delete&id=" . $profile["ID"], array("action", "id", "count"))?>" class="btn btn-primary fa fa-remove"></a>
                </div>
            </div>
        <?endforeach;?>
    <?endif;?>

    <!--- Мои профили --->
    <div class="card">
        <div class="card-header">
            <?if($arResult["PROFILE"]["ID"] > 0):?>
                <div class="card-title"><?=Loc::getMessage("PROFILE_EDIT_TITLE")?></div>
            <?else:?>
                <div class="card-title"><?=Loc::getMessage("PROFILE_ADD")?></div>
            <?endif;?>
        </div>
        <div class="card-body">

            <?if(!empty($arResult["ERRORS"])):?>
                <div class="alert alert-danger">
                    <?foreach ($arResult["ERRORS"] as $error):?>
                        <p><?=Loc::getMessage($error)?></p>
                    <?endforeach;?>
                </div>
            <?endif;?>

            <form class="form"
                  method="post"
                  action="<?=$arResult["PATH_TO_ADDRESS"]?>">

                <?if($arResult["PROFILE"]["ID"] > 0):?>
                    <input type="hidden"
                           name="id"
                           value="<?=$arResult["PROFILE"]["ID"]?>">
                <?endif;?>

                <div class="form-group">
                    <label for="bayer-name"><?=Loc::getMessage("PROFILE_NAME")?> <span class="text-danger">*</span></label>
                    <input type="text"
                           id="bayer-name"
                           name="bayer-name"
                           class="form-control"
                           value="<?=$arResult["PROFILE"]["NAME"] ? $arResult["PROFILE"]["NAME"] : ""?>" />
                </div>

                <div class="form-group" id="adress-location-selector">
                    <label><?=Loc::getMessage("PROFILE_ADDRESS")?></label>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:sale.location.selector.search",
                        ".default",
                        array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "ID" => "",
                            "CODE" => $arResult["PROFILE"]["LOCATION"]["LOCATION"],
                            "INPUT_NAME" => "LOCATION",
                            "PROVIDE_LINK_BY" => "code",
                            "FILTER_BY_SITE" => "N",
                            "SHOW_DEFAULT_LOCATIONS" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "JS_CONTROL_GLOBAL_ID" => "adress",
                            "JS_CALLBACK" => "locationUpdated",
                            "SUPPRESS_ERRORS" => "N",
                            "INITIALIZE_BY_GLOBAL_EVENT" => ""
                        ),
                        false,
                        array("HIDE_ICONS" => "Y")
                    );?>
                    <input type="hidden"
                           id="LOCATION_val"
                           name="LOCATION_val"
                           value="">
                </div>

                <?if(!empty($arResult["ORDER_PROPS"])):?>
                    <?foreach ($arResult["ORDER_PROPS"] as $property):?>
                        <div class="form-group">
                            <label for="bayer-name">
                                <?=$property["NAME"]?>
                            </label>
                            <input type="text"
                                   id="bayer-name"
                                   name="bayer-prop[<?=$property["CODE"]?>]"
                                   class="form-control"
                                   value="<?=$arResult["PROFILE"]["ORDER_PROPS"][$property["CODE"]]?>" />
                        </div>
                    <?endforeach;?>
                <?endif;?>

                <?if($arResult["PROFILE"]["ID"] > 0):?>
                    <input type="submit"
                           class="btn"
                           value="<?=Loc::getMessage("PROFILE_EDIT_BTN")?>">
                    <a class="btn btn-primary" href="<?=$APPLICATION->GetCurPageParam("count=0", array("action", "id", "count"))?>"><?=Loc::getMessage("PROFILE_ADD_BTN")?></a>
                    <a class="btn btn-primary" href="<?=$arParams["PATH"]?>"><?=Loc::getMessage("PROFILE_LIST_BTN")?></a>
                    <a class="btn btn-danger" href="<?=$APPLICATION->GetCurPageParam("action=delete&id=" . $arResult["PROFILE"]["ID"], array("action", "id", "count"))?>"><?=Loc::getMessage("PROFILE_DELETE_BTN")?></a>
                <?else:?>
                    <input type="submit"
                           class="btn"
                           value="<?=Loc::getMessage("PROFILE_ADD_BTN")?>">
                <?endif;?>


            </form>

        </div>
    </div>

</div>