<?
use Bitrix\Main\Application,
    Bitrix\Main\Loader,
    Studio7spb\Marketplace\SaleAddressTable;

class sellerAddress extends CBitrixComponent
{
    private $_modules = array("sale", "studio7spb.marketplace");
    private $_profileTypes;
    private $_profiles;
    private $_locations;
    private $_action;
    private $_errors;
    private $_orderProps;

    /**
     * @return mixed
     */
    public function getOrderProps()
    {
        return $this->_orderProps;
    }

    /**
     * @param mixed $orderProps
     */
    public function setOrderProps()
    {
        $orderProps = Bitrix\Sale\Property::getList(array(
            "filter" => array(
                "CODE" => $this->arParams["PROPS"]
            )
        ));
        while ($orderProp = $orderProps->fetch()){
            $this->_orderProps[$orderProp["ID"]] = $orderProp;
        }
    }

    /**
     * @return mixed
     */
    public function getProfileTypes()
    {
        return $this->_profileTypes;
    }

    /**
     * @param mixed $profileTypes
     */
    public function setProfileTypes()
    {
        $personTypes = \Bitrix\Sale\PersonType::getList();
        while ($personType = $personTypes->fetch())
        {
            //$this->_profileTypes[$personType["CODE"]] = $personType;
            $this->_profileTypes[] = $personType;
        }
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($error)
    {
        $this->_errors[] = $error;
    }

    /**
     * @return mixed
     */
    public function getLocations()
    {
        return $this->_locations;
    }

    /**
     * @param mixed $locations
     */
    public function setLocations()
    {
        if(Loader::includeModule($this->_modules[1])){
            $arFields = array(
                "USER_ID" => $this->arParams["USER_ID"]
            );
            $locations = SaleAddressTable::getList(array(
                "filter" => $arFields
            ));
            while ($location = $locations->fetch()){
                $this->_locations[$location["PROFILE_ID"]] = $location;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getProfiles()
    {
        return $this->_profiles;
    }

    /**
     * Set profiles from sale module
     */
    public function setProfiles()
    {
        $profiles = array();
        if(Loader::includeModule($this->_modules[0]));
        {
            $db = CSaleOrderUserProps::GetList(array(), array("USER_ID" => $this->arParams["USER_ID"]));
            while ($profile = $db->Fetch()){
                $profiles[$profile["ID"]] = $profile;
            }
        }
        $this->_profiles = $profiles;
    }

    /**
     * @return \Bitrix\Main\HttpRequest
     */
    public function getAction()
    {
        return $this->_action;
    }

    /**
     * @param mixed $action
     */
    public function initUserAction()
    {
        $action = Application::getInstance()->getContext()->getRequest();
        $this->_action = $action;
    }

    /**
     * @param mixed $action
     */
    public function setUserActivity()
    {
        $this->initUserAction();
        $arFields = array(
            "NAME" => $this->getAction()->get("bayer-name"),
            "USER_ID" => $this->arParams["USER_ID"],
            //"PERSON_TYPE_ID" => intval($this->getAction()->get("bayer-type"))
            "PERSON_TYPE_ID" => 1
        );

        if($this->arParams["USER_TYPE"] == "SALLER"){
            $arFields["PERSON_TYPE_ID"] = 2;
        }

        $profile = intval($this->getAction()->get("id"));
        if($profile > 0){
            switch ($this->getAction()->get("action")){
                case "edit":
                    if($profile > 0){
                        $this->arResult["PROFILE"] = $this->_profiles[$profile];
                        $this->getUserLocation();
                        if($this->getAction()->isPost()){
                            if(strlen($arFields["NAME"]) > 2){
                                $this->editProfile($arFields);
                            }
                            else{
                                $this->setErrors("ERROR_NAME_EMPTY");
                            }
                        }
                    }
                    break;
                case "delete":
                    if($profile > 0){
                        $this->deleteProfile($profile);
                        $this->deleteUserAllLocations($profile);
                    }
                    break;
            }
        }
        else{
            // add profile
            if($this->getAction()->isPost()){
                if(strlen($arFields["NAME"]) > 2){
                    $this->addProfile($arFields);
                }
                else{
                    $this->setErrors("ERROR_NAME_EMPTY");
                }
            }
        }

        // some request data
        $this->arResult["COUNT"] = $this->getAction()->get("count");
    }

    /**
     *  Edit user profile
     */
    public function addProfile($arFields){

        if($this->getAction()->isPost()){
            if($ID = CSaleOrderUserProps::Add($arFields)){
                $this->setProfiles();

                $this->arResult["PROFILE"] = $this->_profiles[$ID];

                if(intval($this->getAction()->get("LOCATION")) > 0 && $ID > 0){
                    $this->addLocationToProfile($this->getAction()->get("LOCATION"));
                }

            }
        }
    }

    /**
     * @param $arFields
     */
    public function editProfile($arFields){

        if(
            $this->getAction()->isPost() &&
            !empty($this->arResult["PROFILE"])
        ){
            if(CSaleOrderUserProps::Update($this->arResult["PROFILE"]["ID"], $arFields)){
                $this->arResult["PROFILE"]["NAME"] = $arFields["NAME"];
                $this->_profiles[$this->arResult["PROFILE"]["ID"]]["NAME"] = $this->arResult["PROFILE"]["NAME"];
            }
            // add adress to profile
            if(intval($this->getAction()->get("LOCATION")) > 0){
                $this->addLocationToProfile($this->getAction()->get("LOCATION"));
            }

        }
    }

    /**
     * @param $profile
     */
    public function deleteProfile($profile){
        // use exisionalism
        $profile = $this->_profiles[$profile];
        if(!empty($profile)){
            CSaleOrderUserProps::Delete($profile["ID"]);
            unset($this->_profiles[$profile["ID"]]);
        }
    }

    /**
     * @throws Exception
     */
    public function deleteUserLocations(){
        foreach ($this->arResult["PROFILE"]["LOCATIONS"] as $array){
            if(in_array($array["LOCATION"], $this->getAction()->getPost("bayer-locations-delete"))){
                SaleAddressTable::delete($array["ID"]);
                unset($this->arResult["PROFILE"]["LOCATIONS"][$array["LOCATION"]]);

            }
        }
    }
    /**
     * @throws Exception
     */
    public function deleteUserAllLocations($profile){
        if($profile > 0){
            $locations = SaleAddressTable::getList(array(
                "filter" => array(
                    "USER_ID" => $this->arParams["USER_ID"],
                    "PROFILE_ID" => $profile
                )
            ));
            while ($location = $locations->fetch()){
                SaleAddressTable::delete($location["ID"]);
            }
        }
    }

    public function addLocationToProfile($location){
        // order props
        if(!empty($this->arParams["PROPS"])){
            $orderProps = array();
            $requestProps = $this->getAction()->get("bayer-prop");
            foreach ($this->arParams["PROPS"] as $property){
                $orderProps[$property] = $requestProps[$property];
            }
        }
        // location

        if($location){

            $arParams = array(
                "PROFILE_ID" => $this->arResult["PROFILE"]["ID"],
                "USER_ID" => $this->arParams["USER_ID"],
                "LOCATION_VAL" => $this->getAction()->get("LOCATION_val"),
                "LOCATION" => $location,
            );
            if(count($orderProps) > 0){
                $orderProps = serialize($orderProps);
                $arParams["ORDER_PROPS"] = $orderProps;
            }

            if($this->arResult["PROFILE"]["LOCATION"]["ID"] > 0){
                SaleAddressTable::update($this->arResult["PROFILE"]["LOCATION"]["ID"], $arParams);
            }
            else{
                SaleAddressTable::add($arParams);
            }
            $this->getUserLocation();

        }
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getUserLocation(){
        $locations = SaleAddressTable::getList(array(
            "filter" => array(
                "USER_ID" => $this->arParams["USER_ID"],
                "PROFILE_ID" => $this->arResult["PROFILE"]["ID"]
            )
        ));
        while ($location = $locations->fetch()){
            $this->arResult["PROFILE"]["ORDER_PROPS"] = unserialize($location["ORDER_PROPS"]);
            $this->arResult["PROFILE"]["LOCATION"] = $location;
        }

    }

    /**
     * Executin proucess
     * @return bool
     */
    public function executeComponent()
    {
        if($this->arParams["USER_AUTORIZED"]){
            $this->setProfiles();
            $this->setLocations();
            $this->setUserActivity();
            $this->setProfileTypes();
            $this->setOrderProps();
        }
        $this->arResult["ERRORS"] = $this->getErrors();
        $this->arResult["PROFILE_TYPES"] = $this->getProfileTypes();
        $this->arResult["PROFILES"] = $this->getProfiles();
        $this->arResult["LOCATIONS"] = $this->getLocations();
        $this->arResult["ORDER_PROPS"] = $this->getOrderProps();

        $this->includeComponentTemplate();
        return false;
    }
}