<?
/**
 * sale_basket_add_from_file
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "sale_basket_add_from_file",
    "DESCRIPTION" => "sale_basket_add_from_file",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "sale_basket_add_from_file",
            "NAME" => "sale_basket_add_from_file"
        )
    ),
);
