<?

use Bitrix\Main\IO\File;
use Bitrix\Main\Loader;

class saleBasketAddFromFile extends CBitrixComponent
{
    private $_file;
    private $_content;
    private $fileItems = [];
    private $_articles = [];
    private $_articleIDs = [];
    private $_elements = [];
    private $_elementIDs = [];

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->_file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->_file = $file;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->_content = $content;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->_content;
    }

    /**
     * @param array $fileItems
     */
    public function setFileItems($fileItems)
    {
        $this->fileItems = $fileItems;
    }

    /**
     * @return array
     */
    public function getFileItems()
    {
        return $this->fileItems;
    }

    /**
     * @return array
     */
    public function getArticles()
    {
        return $this->_articles;
    }

    /**
     * @param array $articles
     */
    public function setArticles($articles)
    {
        $this->_articles = $articles;
    }

    /**
     * @return array
     */
    public function getElements()
    {
        return $this->_elements;
    }

    /**
     * @param array $elements
     */
    public function setElements($elements)
    {
        $this->_elements = $elements;
    }

    public function setElementsByArticles()
    {
        if(empty($this->getArticles()))
            return false;

        $elements = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                "PROPERTY_ARTICLE" => $this->_articleIDs,
                "ACTIVE" => "Y"
            ],
            false,
            false, //["nTopCount" => 24],
            [
                "ID",
                "NAME",
                "PROPERTY_MOQ",
                "PROPERTY_ARTICLE",
                "PROPERTY_MULTIPLICITY"
            ]
        );
        while ($element = $elements->Fetch()){
            $this->_elementIDs[$element["ID"]] = $element["ID"];
            $this->_elements[$element["ID"]] = $element;
        }

        return true;
    }

    public function updateBasket()
    {
        if(!empty($this->_elementIDs)){
            $productID = $this->_elementIDs;
            if(!empty($productID)){
                $dbBasketItems = \CSaleBasket::GetList(
                    [],
                    [
                        "FUSER_ID" => \CSaleBasket::GetBasketUserID(),
                        "LID" => SITE_ID,
                        "ORDER_ID" => "NULL"
                    ],
                    false,
                    false,
                    [
                        "ID", "PRODUCT_ID", "QUANTITY", "DELAY"
                    ]
                );
                // обновить те что есть в корзине

                while ($dbBasketItem = $dbBasketItems->Fetch()){

                    if($productID[$dbBasketItem["PRODUCT_ID"]] > 0) {

                        $quantity = $dbBasketItem["QUANTITY"];
                        $quantity += intval($this->_articles[$this->_elements[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_ARTICLE_VALUE"]]);

                        if($quantity < $this->_elements[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_MOQ_VALUE"])
                            $quantity = $this->_elements[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_MOQ_VALUE"];

                        if($this->_elements[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_MULTIPLICITY_VALUE"] > 1)
                        {
                            //$quantity += $this->_elements[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_MULTIPLICITY_VALUE"];
                            $ostatok = $quantity % $this->_elements[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_MULTIPLICITY_VALUE"];
                            if($ostatok == 0) {
                                // Количество кратно свойству кратности
                            }else{
                                // тнимаем остаток от количества и добавляем свойство кратности
                                $quantity = $quantity - $ostatok + $this->_elements[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_MULTIPLICITY_VALUE"];
                            }
                        }
                        else
                        {
                            $quantity += 1;
                        }

                        if($quantity < 1){
                            $quantity = 1;
                        }

                        \CSaleBasket::Update($dbBasketItem["ID"], ["QUANTITY" => $quantity]);

                        unset($productID[$dbBasketItem["PRODUCT_ID"]]);
                    }

                }

                // Если после обновления остались товары - добавим их в корзину c учётом MOQ
                if(!empty($productID)){
                    //$basket = \Bitrix\Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), SITE_ID);
                    //$quantity = 1;
                    //$quantity += intval($this->_articles[$this->_elements[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_ARTICLE_VALUE"]]);
                    foreach ($productID as $product){

                        $quantity = intval($this->_articles[$this->_elements[$product]["PROPERTY_ARTICLE_VALUE"]]);

                        if($quantity < $this->_elements[$product]["PROPERTY_MOQ_VALUE"])
                            $quantity = $this->_elements[$product]["PROPERTY_MOQ_VALUE"];


                        if($this->_elements[$product]["PROPERTY_MULTIPLICITY_VALUE"] > 1){
                            $ostatok = $quantity % $this->_elements[$product]["PROPERTY_MULTIPLICITY_VALUE"];

                            if($ostatok == 0) {
                                // Количество кратно свойству кратности
                            }else{
                                // тнимаем остаток от количества и добавляем свойство кратности
                                $quantity = $quantity - $ostatok + $this->_elements[$product]["PROPERTY_MULTIPLICITY_VALUE"];
                            }
                        }

                        if($quantity < 1){
                            $quantity = 1;
                        }
                        Add2BasketByProductID($product, $quantity, [], []);
                    }
                }

            }
        }
        return true;
    }

    /**
     * Execute component
     * @return mixed|void
     */
    public function executeComponent()
    {
        Loader::includeModule("iblock");
        Loader::includeModule("sale");
        if(empty($this->arParams["FILE"]["tmp_name"])){

        }else{
            $this->setFile($this->arParams["FILE"]);
            $this->setContent(File::getFileContents($this->arParams["FILE"]["tmp_name"]));
        }

        $this->arResult = [
            "FILE" => $this->getFile(),
            "CONTENT" => $this->getContent()
        ];

        //$text = mb_convert_encoding($text, 'UTF-8', 'AUTO');
        //$text = mb_convert_encoding($text, 'UTF-8', 'ANSI');
        //$text = mb_convert_encoding($text, 'UTF-8', 'WINDOWS-1251');
        $this->arResult["CONTENT"] = iconv('WINDOWS-1251', 'UTF-8', $this->arResult["CONTENT"]);

        if(!empty($this->getContent())){
            $lines = explode(PHP_EOL, $this->arResult["CONTENT"]);
            foreach ($lines as $line) {
                $this->fileItems[] = str_getcsv($line);
            }
            unset($this->fileItems[0]);
        }

        if(!empty($this->getFileItems())){
            $articles = [];
            foreach ($this->getFileItems() as $article){
                $article = $article[0];
                $article = explode(";", $article);
                if(!empty($article[0])) {
                    $articles[$article[0]] = $article[1];
                    $this->_articleIDs[] = $article[0];
                }
            }

            $this->setArticles($articles);
            $this->setElementsByArticles();

            if($this->updateBasket()){
                LocalRedirect($this->arParams["BASKET_URL"]);
            }
        }

        $this->includeComponentTemplate();
    }
}