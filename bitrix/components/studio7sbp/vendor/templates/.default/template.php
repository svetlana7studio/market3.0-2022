<?php
/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

$this->setFrameMode(true);

$this->addExternalJs(SITE_TEMPLATE_PATH . "/assets/js/catalog.section.js");
$this->addExternalJs(SITE_TEMPLATE_PATH . "/components/bitrix/catalog/main/script.js");
$this->addExternalCss(SITE_TEMPLATE_PATH . "/assets/css/catalog.section.css");
?>
<div class="text-20 text-uppercase p3">
    <span><?=Loc::getMessage("VENDOR_TITLE", ["VENDOR" => $arResult["VENDOR"]["title"]])?></span>
</div>
