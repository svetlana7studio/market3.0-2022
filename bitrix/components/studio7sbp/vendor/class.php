<?

use Studio7spb\Marketplace\VendorTable;

class studio7sbpVendor extends CBitrixComponent
{
    private $_vendorID;
    private $_vendor;

    /**
     * @param int $vendorID
     */
    public function setVendorID($vendorID)
    {
        $this->_vendorID = $vendorID;
    }

    public function setVendorIDByUrl()
    {
        $vendorID = 0;
        $page = $this->arParams["PAGE"];
        $page = explode("/", $page);
        $vendorID = $page[$this->arParams["VENDOR_IN_URL"]];
        $this->setVendorID($vendorID);
    }

    /**
     * @return int
     */
    public function getVendorID()
    {
        return $this->_vendorID;
    }

    /**
     * @return mixed
     */
    public function getVendor()
    {
        return $this->_vendor;
    }

    /**
     * @param mixed $vendor
     */
    public function setVendor($vendor)
    {
        $this->_vendor = $vendor;
    }

    /**
     * @param mixed $vendor
     */
    public function setVendorFromORM()
    {
        if($this->getVendorID() > 0){
            CModule::AddAutoloadClasses(
                "studio7spb.marketplace",
                array(
                    "\\Studio7spb\\Marketplace\\VendorTable" => "lib/anytos/vendor.php"
                )
            );
            $vendors = VendorTable::getList([
                "filter" => ["id" => $this->getVendorID()]
            ]);
            if($vendor = $vendors->fetch()){
                $this->_vendor = $vendor;
            }
        }

    }

    public function executeComponent()
    {
        global $APPLICATION;
        $this->setVendorIDByUrl();
        $this->setVendorFromORM();

        $this->arResult = [
            "ID" => $this->getVendorID(),
            "VENDOR" => $this->getVendor()
        ];

        $this->includeComponentTemplate();

        if($this->arParams["SET_TITLE_TO_CHAIN"] == "Y"){
            if($this->arParams["SET_ID_TO_CHAIN"] == "Y"){
                $APPLICATION->AddChainItem($this->_vendor["id"]);
            }else{
                $APPLICATION->AddChainItem($this->_vendor["title"]);
            }
        }

        return $this->getVendorID();
    }
}