<?
$MESS["BFDND_FILE_LOADING"] = "Загрузка";
$MESS["BFDND_FILE_EXISTS"] = "Файл с таким именем уже существует. Вы можете выбрать текущую папку, в этом случае старая версия файла будет сохранена в истории документа.";
$MESS["BFDND_UPLOAD_ERROR"] = "При сохранении файла произошла ошибка.";
$MESS["BFDND_FILES"] = "Изображения:";
$MESS["BFDND_DROPHERE"] = "Перетащите одно или несколько изображений (jpg, jpeg, png, gif, bmp) в эту область";
$MESS["BFDND_SELECT_EXIST"] = "или выберите изображение на компьютере";
$MESS["BFDND_SELECT_LOCAL"] = "Загрузить изображения";
$MESS["BFDND_ACCESS_DENIED"] = "Доступ запрещен";
$MESS["BFDND_UPLOAD_IMAGES"] = "Загрузить картинки";
$MESS["BFDND_UPLOAD_FILES"] = "Загрузить файлы";

?>
