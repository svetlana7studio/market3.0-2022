<?
/**
 * seller_personal_product_add
 * seller personal product add
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "seller personal product add",
    "DESCRIPTION" => "seller personal product add",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "seller_personal_product_add",
            "NAME" => "seller personal product add"
        )
    ),
);
