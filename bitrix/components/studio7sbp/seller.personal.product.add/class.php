<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
	\Bitrix\Sale,
	\Bitrix\Main\Loader,
    \Bitrix\Iblock\IblockTable,
	\Studio7spb\Marketplace\CMarketplaceSeller;

Loc::loadMessages(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/studio7spb.marketplace/options.php");

if (!\Bitrix\Main\Loader::includeModule('studio7spb.marketplace')) {
	ShowError(Loc::getMessage('studio7spb.marketplace_MODULE_NOT_INSTALLED'));
	die();
}

class SellerPersonalProductAdd extends CBitrixComponent
{

	protected $error = array();
	protected $moduleOptionList = array();
	protected $handlers = array();
	protected $iblocks =  array();

	function __construct($component = null) {
		$this->loadModuleOptions();
		parent::__construct($component);
	}

	protected function loadModuleOptions() {
		$this->moduleOptionList = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOptionList();
	}

	protected function getAccess() {
		return true;
	}

	protected function getIblockPropertyList() {
		Loader::includeModule("iblock");

		if(empty($this->arResult["iblock_property_list"])) {
			$this->arResult["iblock_property_list"] = array();
			$this->arResult["iblock_property_list_by_code"] = array();

			$propertyIterator = \Bitrix\Iblock\PropertyTable::getList(array(
				'filter' => array('=IBLOCK_ID' => $this->arParams['IBLOCK_ID'], '=ACTIVE' => 'Y'),
				'order' => array('SORT' => 'ASC', 'ID' => 'ASC')
			));
			while ($property = $propertyIterator->fetch()) {
				$this->arResult["property_list"][$property["ID"]] = $property;
				$this->arResult["iblock_property_list_by_code"][$property["CODE"]] = $property["ID"];
			}
		}
	}

	protected function loadElementProperties() {
		if($this->arParams['ELEMENT_ID'] <= 0) {
			return null;
		}

		Loader::includeModule("iblock");

		$rsElementProperties = \CIBlockElement::GetProperty($this->arParams["IBLOCK_ID"], $this->arParams['ELEMENT_ID'], $by="sort", $order="asc");
		$this->arResult["ELEMENT_PROPERTIES"] = array();
		while ($arElementProperty = $rsElementProperties->Fetch()) {

			if(!array_key_exists($arElementProperty["ID"], $this->arResult["ELEMENT_PROPERTIES"]))
				$this->arResult["ELEMENT_PROPERTIES"][$arElementProperty["ID"]] = array();

			if(is_array($arElementProperty["VALUE"])) {
				$htmlvalue = array();
				foreach($arElementProperty["VALUE"] as $k => $v) {
					if(is_array($v)) {
						$htmlvalue[$k] = array();
						foreach($v as $k1 => $v1)
							$htmlvalue[$k][$k1] = htmlspecialcharsbx($v1);
					} else {
						$htmlvalue[$k] = htmlspecialcharsbx($v);
					}
				}
			} else {
				$htmlvalue = htmlspecialcharsbx($arElementProperty["VALUE"]);
			}

			$this->arResult["ELEMENT_PROPERTIES"][$arElementProperty["ID"]][] = array(
				"ID" => htmlspecialcharsbx($arElementProperty["ID"]),
				"VALUE" => $htmlvalue,
				"CODE" => $arElementProperty["CODE"],
				"~VALUE" => $arElementProperty["VALUE"],
				"VALUE_ID" => htmlspecialcharsbx($arElementProperty["PROPERTY_VALUE_ID"]),
				"VALUE_ENUM" => htmlspecialcharsbx($arElementProperty["VALUE_ENUM"]),
			);
		}
		$this->arParams["ELEMENT_PROPERTIES"] = $this->arResult["ELEMENT_PROPERTIES"];

	}

	protected function loadElementProducInformation() {
		if($this->arParams['ELEMENT_ID'] <= 0) {
			return null;
		}

		Loader::includeModule("catalog");

		$this->arResult["ELEMENT_PRODUCT_INFO"] = \CCatalogProduct::GetByID($this->arParams['ELEMENT_ID']);

	}

	protected function loadElementPriceInfo() {
		if($this->arParams['ELEMENT_ID'] <= 0) {
			return null;
		}
		if(Loader::includeModule("catalog")){

            $elementPrices = \Bitrix\Catalog\Model\Price::getList([
                "filter" => [
                    "PRODUCT_ID" => $this->arParams['ELEMENT_ID']
                ]
            ]);
            while ($elementPrice = $elementPrices->fetch())
            {
                switch ($elementPrice["CATALOG_GROUP_ID"]){
                    case 3:
                        $this->arResult["ELEMENT_PRICE_INFO"] = $elementPrice;
                        break;
                    case 4:
                        $this->arResult["ELEMENT_PRICE_DISCOUNT_INFO"] = $elementPrice;
                        break;
                }

                $this->arResult["ELEMENT_PRICES"][$elementPrice["CATALOG_GROUP_ID"]] = $elementPrice;
            }

        }
	}

	protected function getElementById() {
		if($this->arParams['ELEMENT_ID'] <= 0) {
			return null;
		}
		Loader::includeModule("iblock");
		$oElement = \CIBlockElement::GetByID($this->arParams['ELEMENT_ID']);
		$element = $oElement->Fetch();

		if(empty($element)) return null;

		$this->arResult["ELEMENT"] = array();
		foreach($element as $key => $value) {
			$this->arResult["ELEMENT"]["~".$key] = $value;
			if(!is_array($value) && !is_object($value)) {
				$this->arResult["ELEMENT"][$key] = htmlspecialcharsbx($value);
			} else {
				$this->arResult["ELEMENT"][$key] = $value;
			}
		}

		$this->arResult["ELEMENT"]["DETAIL_TEXT"] = $this->arResult["ELEMENT"]["~DETAIL_TEXT"];
		$this->arResult["ELEMENT"]["PREVIEW_TEXT"] = $this->arResult["ELEMENT"]["~PREVIEW_TEXT"];

		$this->loadElementProperties();
		$this->loadElementProducInformation();
		$this->loadElementPriceInfo();
	}

	protected function getCategoryTree() {
		if(empty($this->arResult["cat_tree"])) {
			Loader::includeModule("iblock");
			$oCatTree = \CIBlockSection::GetTreeList(Array("IBLOCK_ID"=>$this->arParams['IBLOCK_ID']), array("ID", "NAME", "DEPTH_LEVEL"));
			while($aCatTree = $oCatTree->GetNext()){
				$this->arResult["cat_tree"][] = $aCatTree;
			}
		}
	}

    /**
     * @return array
     */
    public function getIblocks()
    {
        return $this->iblocks;
    }

    /**
     * @param array $iblocks
     */
    public function setIblocks($iblocks)
    {
        $this->iblocks = $iblocks;
    }

    public function initIblocks()
    {
        $arIblocks = [];
        $iblocks = IblockTable::getList([
            "filter" => [
                "IBLOCK_TYPE_ID" => $this->arParams["IBLOCK_TYPE"],
                "ACTIVE" => "Y"
            ],
            "select" => [
                "ID",
                "NAME"
            ]
        ]);
        while ($iblock = $iblocks->fetch()){
            $arIblocks[] = $iblock;
        }
        $this->setIblocks($arIblocks);

        $this->arResult["IBLOCKS"] = $this->getIblocks();

    }



	protected function getMeasureList() {
		if(empty($this->arResult["measure_list"])) {
			Loader::includeModule("catalog");
			$oMeasure = \CCatalogMeasure::getList(array(), array(), false, false, Array("ID","MEASURE_TITLE"));
			while($aMeasure = $oMeasure->Fetch()) {
				$this->arResult["measure_list"][$aMeasure["ID"]] = $aMeasure;
			}
		}
	}

	protected function getLib() {
		$this->getCategoryTree();
		$this->getMeasureList();
		$this->arResult["COMPANY"] = CMarketplaceSeller::getCompanyByUser();
	}


	/**
	 * Process incoming request
	 * @return void
	 */
	protected function processRequestUploadFile()
	{
		if ($this->request->isPost() && isset($this->request['mfi_mode']) && ($this->request['mfi_mode'] == "upload")) {
			$this->handlers["main.file.input.upload"] = \AddEventHandler('main',  "main.file.input.upload", array(__CLASS__, 'onAddImageResize'));
			foreach($this->handlers as $eventName => $handlerID){
				if ($handlerID){
					\RemoveEventHandler("main", $eventName, $handlerID);
				}
			}
		}

	}

	/**
	 * Process incoming request
	 * @return void
	 */
	protected function processRequest()
	{
        global $APPLICATION;

		if($this->request->isPost() && isset($this->request['Update']) && $this->request['Update'] == "Y" && check_bitrix_sessid())
		{
			$postFields = $this->request->getPostList();
			$this->loadElementProperties();
			$this->getIblockPropertyList();

			Loader::includeModule("iblock");
			Loader::includeModule("catalog");

			if((int)$postFields["SECTION_ID"] <= 0) {
				$this->error[] = "Не выбрана категория";
			}

			$translitParams = array("replace_space"=>"-","replace_other"=>"-");
			$translitName = Cutil::translit($postFields["NAME"], "ru", $translitParams);
			$newElementFields = array(
				"IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
				"IBLOCK_SECTION_ID" => $postFields["SECTION_ID"],
				"ACTIVE" => $postFields["ACTIVE"],
				"NAME" => $postFields["NAME"],
				"CODE" => time(),
				"PREVIEW_TEXT" => $postFields["PREVIEW_TEXT"],
				"PREVIEW_TEXT_TYPE" => "html",
				"DETAIL_TEXT" => $postFields["DETAIL_TEXT"],
				"DETAIL_TEXT_TYPE" => "html",

				"PROPERTY_VALUES" => array()
			);

			if($postFields["DETAIL_PICTURE_del"]) {
				$newElementFields["DETAIL_PICTURE"] = \CIBlock::makeFileArray($postFields["DETAIL_PICTURE"], true);
				$newElementFields["PREVIEW_PICTURE"] = $newElementFields["DETAIL_PICTURE"];
			} else {


                $newElementFields["DETAIL_PICTURE"] = \CIBlock::makeFileArray($postFields["DETAIL_PICTURE"], false, null, array("allow_file_id" => true));

                $aImgThumb = CFile::ResizeImageGet(
                    $postFields["DETAIL_PICTURE"],
                    array("width" => 300, "height" => 300),
                    BX_RESIZE_IMAGE_EXACT,
                    true
                );
                $newElementFields["PREVIEW_PICTURE"] = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$aImgThumb["src"]);

			}


			$postProperty = $postFields["PROPERTY"];
			$postProperty["H_COMPANY"] = $this->userCompany["ID"];

			foreach ($postProperty as $keyPostField => $postFieldsItem) {
				if(is_array($postFieldsItem)) {
					foreach ($postFieldsItem as $keyValue => $postFieldsItemValue) {
						if($this->arParams['ELEMENT_ID'] <= 0) {
							$newElementFields["PROPERTY_VALUES"][$keyPostField]["n".$keyValue] = $postFieldsItemValue;
						} else {
							$newElementFields["PROPERTY_VALUES"][$keyPostField][] = $postFieldsItemValue;
						}
					}
				} else {
					$newElementFields["PROPERTY_VALUES"][$keyPostField] = $postFieldsItem;
				}
			}

			$aPictureValue = array();
			$nVali = 0;
			$PropertyImagesId = "H_PICTURES";

			foreach ($postFields["D_N_D_PICTURES"] as $key => $fileItem) {
				$isIsset = false;
				foreach ($this->arParams["ELEMENT_PROPERTIES"][$PropertyImagesId] as $keyProp => $propValue) {
					if($propValue["VALUE"] == $fileItem) {
						if(!in_array($fileItem, $aPictureValue)) {
							$aPictureValue[$propValue["VALUE_ID"]] = $fileItem;
						}
						$isIsset = true;
					}
				}
				if(!$isIsset) {
					$aPictureValue["n".$nVali++] = $fileItem;
				}
			}
			$newElementFields["PROPERTY_VALUES"][$PropertyImagesId] = $aPictureValue;

			if((int)$postFields["PRICE"] <= 0) {
				$this->error[] = "Не установлена цена";
			}
			if(!empty($this->error)) {
				return false;
			}

			$oIblockElement = new \CIBlockElement;
			if($this->arParams['ELEMENT_ID'] <= 0) {
				$newElementId = $oIblockElement->add($newElementFields, false, true, true);
				if(!$newElementId) {
					$this->error[] = $oIblockElement->LAST_ERROR;
					return false;
				}
				$oIblockElement->Update($newElementId, array("CODE" => $translitName."-".$newElementId));

				$oProduct = \Bitrix\Catalog\Model\Product::add(array(
					"ID" => $newElementId,
					"QUANTITY" => $postFields["QUANTITY"],
					"MEASURE" => $postFields["MEASURE_ID"],
                    "VAT_INCLUDED" => "N"
				));
				if(!$oProduct->isSuccess()) {
                    $this->error = array_merge($this->error, $oProduct->getErrorMessages());
					return false;
				}
				$newProductId = $oProduct->getId();

                $COMP_COMMISSION = 10;
                if(is_numeric($this->userCompany["PROPERTY_COMP_COMMISSION_VALUE"]) && $this->userCompany["PROPERTY_COMP_COMMISSION_VALUE"] > 0)
                    $COMP_COMMISSION = $this->userCompany["PROPERTY_COMP_COMMISSION_VALUE"];

                # Set vendor price
                $PRICE = floatval($postFields["PRICE"]);
				$oProductPrice = \Bitrix\Catalog\Model\Price::add(array(
					"CURRENCY"         => "RUB",       // валюта
					"PRICE"            => $PRICE,      // значение цены
					"CATALOG_GROUP_ID" => 3,           // ID типа цены
					"PRODUCT_ID"       => $newProductId,  // ID товара (элемента инфоблока)
				));

				# set market price
                $COMMISSION_PRICE = floatval($COMP_COMMISSION) * $PRICE / 100;
                $PRICE += $COMMISSION_PRICE;
                \Bitrix\Catalog\Model\Price::add(array(
                    "CURRENCY"         => "RUB",       // валюта
                    "PRICE"            => $PRICE,      // значение цены
                    "CATALOG_GROUP_ID" => 1,           // ID типа цены
                    "PRODUCT_ID"       => $newProductId,  // ID товара (элемента инфоблока)
                ));

                # Set vendor discount price
                $PRICE = floatval($postFields["PRICE_DISCOUNT"]);
                \Bitrix\Catalog\Model\Price::add([
                    "CURRENCY"         => "RUB",
                    "PRICE"            => $PRICE,
                    "CATALOG_GROUP_ID" => 4,
                    "PRODUCT_ID"       => $newProductId
                ]);
                $COMMISSION_PRICE = floatval($COMP_COMMISSION) * $PRICE / 100;
                $PRICE += $COMMISSION_PRICE;
                \Bitrix\Catalog\Model\Price::add([
                    "CURRENCY"         => "RUB",
                    "PRICE"            => $PRICE,
                    "CATALOG_GROUP_ID" => 2,
                    "PRODUCT_ID"       => $newProductId
                ]);

				if(!$oProductPrice->isSuccess()) {
                    $this->error = array_merge($this->error, $oProductPrice->getErrorMessages());
					return false;
				}
                \localRedirect($this->arParams["SEF_FOLDER"]."product/");
			} else {
				unset($newElementFields["IBLOCK_ID"], $newElementFields["CODE"]);

				// есть системный св-ва, которые нужно пропустить 100%, например рейтинг, комментарии
				$aSkipProperty = \Studio7spb\Marketplace\CMarketplaceTools::getSkipCatalogProperty();

				foreach ($this->arResult["ELEMENT_PROPERTIES"] as $keyProp => $aProperty) {
					if(in_array($this->arResult["property_list"][$keyProp]["CODE"], $aSkipProperty)) {
						$newElementFields["PROPERTY_VALUES"][$keyProp] = $aProperty[0]["VALUE"];
					}
				}

				$oIblockElement->update($this->arParams['ELEMENT_ID'], $newElementFields, false, true, true);

				$this->loadElementProducInformation();
				$this->loadElementPriceInfo();

				\Bitrix\Catalog\Model\Product::update($this->arResult["ELEMENT_PRODUCT_INFO"]["ID"], array(
					"QUANTITY" => $postFields["QUANTITY"],
					"MEASURE" => $postFields["MEASURE_ID"],
                    "VAT_INCLUDED" => "N"
				));

                $PRICE = floatval($postFields["PRICE"]);
                $COMP_COMMISSION = 10;
                if(is_numeric($this->userCompany["PROPERTY_COMP_COMMISSION_VALUE"]) && $this->userCompany["PROPERTY_COMP_COMMISSION_VALUE"] > 0)
                    $COMP_COMMISSION = $this->userCompany["PROPERTY_COMP_COMMISSION_VALUE"];

                if($PRICE > 0)
                {
                    # Set vendor price
                    if(empty($this->arResult["ELEMENT_PRICE_INFO"])){
                        \Bitrix\Catalog\Model\Price::add([
                            "CURRENCY"         => "RUB",
                            "PRICE"            => $PRICE,
                            "CATALOG_GROUP_ID" => 3,
                            "PRODUCT_ID"       => $this->arParams['ELEMENT_ID'],
                        ]);
                    }
                    else{
                        \Bitrix\Catalog\Model\Price::update($this->arResult["ELEMENT_PRICE_INFO"]["ID"], ["PRICE" => $PRICE]);
                    }
                    # set market price
                    $COMMISSION_PRICE = floatval($COMP_COMMISSION) * $PRICE / 100;
                    $PRICE += $COMMISSION_PRICE;
                    if(empty($this->arResult["ELEMENT_PRICES"][1])){
                        \Bitrix\Catalog\Model\Price::add([
                            "CURRENCY"         => "RUB",
                            "PRICE"            => $PRICE,
                            "CATALOG_GROUP_ID" => 1,
                            "PRODUCT_ID"       => $this->arParams['ELEMENT_ID'],
                        ]);
                    }else{
                        \Bitrix\Catalog\Model\Price::update($this->arResult["ELEMENT_PRICES"][1]["ID"], ["PRICE" => $PRICE]);
                    }
                }

                $PRICE = floatval($postFields["PRICE_DISCOUNT"]);
                if($PRICE >= 0)
                {
                    # Set vendor discount price
                    if(empty($this->arResult["ELEMENT_PRICE_DISCOUNT_INFO"])){
                        \Bitrix\Catalog\Model\Price::add([
                            "CURRENCY"         => "RUB",
                            "PRICE"            => $PRICE,
                            "CATALOG_GROUP_ID" => 4,
                            "PRODUCT_ID"       => $this->arParams['ELEMENT_ID'],
                        ]);
                    }
                    else{
                        if($PRICE > 0)
                            \Bitrix\Catalog\Model\Price::update($this->arResult["ELEMENT_PRICE_DISCOUNT_INFO"]["ID"], ["PRICE" => $PRICE]);
                        else
                            \Bitrix\Catalog\Model\Price::delete($this->arResult["ELEMENT_PRICE_DISCOUNT_INFO"]["ID"]);
                    }
                    # set market discount price
                    $COMMISSION_PRICE = floatval($COMP_COMMISSION) * $PRICE / 100;
                    $PRICE += $COMMISSION_PRICE;
                    if(empty($this->arResult["ELEMENT_PRICES"][2])){
                        \Bitrix\Catalog\Model\Price::add([
                            "CURRENCY"         => "RUB",
                            "PRICE"            => $PRICE,
                            "CATALOG_GROUP_ID" => 2,
                            "PRODUCT_ID"       => $this->arParams['ELEMENT_ID'],
                        ]);
                    }else{
                        if($PRICE > 0)
                            \Bitrix\Catalog\Model\Price::update($this->arResult["ELEMENT_PRICES"][2]["ID"], ["PRICE" => $PRICE]);
                        else
                            \Bitrix\Catalog\Model\Price::delete($this->arResult["ELEMENT_PRICES"][2]["ID"]);
                    }
                }

                \localRedirect($APPLICATION->GetCurPageParam("message=success", array("message")));
			}

		}
	}


	function onAddImageResize(&$arCustomFile, $arParams = 90) {
		static $arResizeParams = array();

		if ($arParams !== null)
		{
			if (is_array($arParams) && array_key_exists("width", $arParams) && array_key_exists("height", $arParams))
			{
				$arResizeParams = $arParams;
			}
			elseif(intVal($arParams) > 0)
			{
				$arResizeParams = array("width" => intVal($arParams), "height" => intVal($arParams));
			}
		}

		if ((!is_array($arCustomFile)) || !isset($arCustomFile['fileID']))
			return false;

		if (array_key_exists("ID", $arCustomFile))
		{
			$arFile = $arCustomFile;
			$fileID = $arCustomFile['ID'];
		}
		else
		{
			$fileID = $arCustomFile['fileID'];
			$arFile = CFile::MakeFileArray($fileID);
			$arFile1 = CFile::GetByID($fileID)->fetch();
			if (is_array($arFile) && is_array($arFile1))
			{
				// $arCustomFile = array_merge($arFile, $arFile1, $arCustomFile);
			}
		}

		if (CFile::CheckImageFile($arFile) === null)
		{
			$aImgThumb = CFile::ResizeImageGet(
				$fileID,
				array("width" => 90, "height" => 90),
				BX_RESIZE_IMAGE_EXACT,
				true
			);
			$arCustomFile['img_thumb_src'] = $aImgThumb['src'];

			if (!empty($arResizeParams))
			{
				$aImgSource = CFile::ResizeImageGet(
					$fileID,
					array("width" => $arResizeParams["width"], "height" => $arResizeParams["height"]),
					BX_RESIZE_IMAGE_PROPORTIONAL,
					true
				);
				$arCustomFile['img_source_src'] = $aImgSource['src'];
				$arCustomFile['img_source_width'] = $aImgSource['width'];
				$arCustomFile['img_source_height'] = $aImgSource['height'];
			}
		}
	}

	protected function getUserCompany() {
		if(empty($this->userCompany)) {
			$this->userCompany = CMarketplaceSeller::getCompanyByUser();
		}
	}

	protected function getCompanyByElement($elementId = 0) {
		if((int)$elementId <= 0) {
			return false;
		}
		Loader::includeModule('iblock');
		$rsElementProperties = \CIBlockElement::GetProperty($this->arParams["IBLOCK_ID"], $elementId, array("sort" => "asc"), Array("CODE"=>"H_COMPANY"));
		if($arElementProperty = $rsElementProperties->Fetch()) {
			return $arElementProperty["VALUE"];
		}
		return false;
	}

	protected function isCanAddEditEntity($entityCopmanyId = 0) {
		$isCanEdit = false;
		if(CMarketplaceSeller::canDoOperation("edit_product")){
			$isCanEdit = true;
		}
		$this->getUserCompany();
		if(empty($this->userCompany)){
			$isCanEdit = false;
		}
		if((int)$entityCopmanyId > 0 && !empty($this->userCompany) && (int)$this->userCompany["ID"] > 0) {
			return $this->userCompany["ID"] == $this->getCompanyByElement((int)$entityCopmanyId);
		}
		return $isCanEdit;
	}

	public function executeComponent() {

		if(!$this->isCanAddEditEntity($this->arParams['ELEMENT_ID'])) {
			\localRedirect("/auth/");
			return false;
		}

		$this->initIblocks();
		$this->processRequestUploadFile();
		$this->processRequest();

		$this->getElementById();

		$this->getLib();

		if(!empty($this->error)) {
			$this->arResult["error"] = $this->error;
		}

		$this->includeComponentTemplate();

	}
}