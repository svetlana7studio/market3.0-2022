<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

foreach ($arResult["FILES"] as $keyFile => $aFile) {	
	$aImgThumb = CFile::ResizeImageGet(
		$aFile["ID"],
		array("width" => 90, "height" => 90),
		BX_RESIZE_IMAGE_EXACT,
		true
	);
	$arResult["FILES"][$keyFile]["img_thumb_src"] = $aImgThumb["src"];
}

?>