<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array('popup', 'ajax', 'loader'));

?>

<div class="s7sbp--marketplace--saler--lk--right--inner">

	<div class="s7sbp--marketplace--saler--lk--title ff--roboto">Компания</div>

	<div class="s7sbp--marketplace--saler--lk--form ff--roboto">

        <?if($arResult["company"]["ID"] > 0):?>
    		<form method="POST" id="seller-company">
			
			<?=bitrix_sessid_post()?>

			<input type="hidden" name="Update" value="Y">
		
			<div class="s7sbp--marketplace--saler--lk--product-add--field">
				<div class="s7sbp--marketplace--saler--lk--product-add--field--label">
					<label for="NAME"><b>Название</b></label>
				</div>
				<div class="s7sbp--marketplace--saler--lk--product-add--field--value">
					<textarea name="NAME"><?=$arResult["company"]["NAME"]?></textarea>
				</div>
			</div>

			<div class="s7sbp--marketplace--saler--lk--product-add--field">
				<div class="s7sbp--marketplace--saler--lk--product-add--field--label w-100">
					<b>Описание</b> <span class="required">*</span>
				</div>
				<?$APPLICATION->IncludeComponent(
					"studio7sbp:lhe",
					"",
					Array(
						"LHE_NAME" => "lhe_preview_text_form",
						"LHE_ID" => "lhe_preview_text_form",
						"INPUT_NAME" => "PREVIEW_TEXT",
						"INPUT_VALUE" => $arResult["company"]["PREVIEW_TEXT"],
					),
					$component,
					Array("HIDE_ICONS" => "Y")
				);?>
			</div>

			<br>
			<div class="s7sbp--marketplace--saler--lk--product-add--field">
				<div class="s7sbp--marketplace--saler--lk--product-add--field--label w-100">
					<b>Гарантии продавца</b> <span class="required">*</span>
				</div>
				<?$APPLICATION->IncludeComponent(
					"studio7sbp:lhe",
					"",
					Array(
						"LHE_NAME" => "lhe_garanty_form",
						"LHE_ID" => "lhe_garanty_form",
						"INPUT_NAME" => "DETAIL_TEXT",
						"INPUT_VALUE" => $arResult["company"]["DETAIL_TEXT"],
					),
					$component,
					Array("HIDE_ICONS" => "Y")
				);?>
			</div>

            <?if(!empty($arResult["PROPERTIES"])):?>
                <?
                foreach($arResult["PROPERTIES"] as $code=>$property):
                    if(!in_array($code, $arParams["BANK"])):
                ?>
                    <div class="s7sbp--marketplace--saler--lk--product-add--field">
                        <div class="s7sbp--marketplace--saler--lk--product-add--field--label">
                            <label for="NAME">
                                <b><?=$property["NAME"]?></b>
                                <?if($property["IS_REQUIRED"] == "Y"):?>
                                    <span class="required">*</span>
                                <?endif;?>
                            </label>
                        </div>
                        <div class="s7sbp--marketplace--saler--lk--product-add--field--value">
                            <input type="text"
                                   name="PROPERTIES[<?=$property["CODE"]?>]"
                                   value="<?
                                   if(empty($arResult["company"]["PROPERTY_" . $property["CODE"] . "_VALUE"]))
                                       echo $property["VALUE"];
                                   else
                                       echo $arResult["company"]["PROPERTY_" . $property["CODE"] . "_VALUE"];
                                   ?>" />
                        </div>
                    </div>
                <?
                    unset($arResult["PROPERTIES"][$code]);
                    endif;
                endforeach;
                ?>
            <?endif;?>

            <?if(!empty($arResult["PROPERTIES"])):?>
                <div class="s7sbp--marketplace--saler--lk--title ff--roboto">Банковские счета</div>
                <?foreach($arResult["PROPERTIES"] as $code=>$property):?>
                        <div class="s7sbp--marketplace--saler--lk--product-add--field">
                            <div class="s7sbp--marketplace--saler--lk--product-add--field--label">
                                <label for="NAME">
                                    <b><?=$property["NAME"]?></b>
                                    <?if($property["IS_REQUIRED"] == "Y"):?>
                                        <span class="required">*</span>
                                    <?endif;?>
                                </label>
                            </div>
                            <div class="s7sbp--marketplace--saler--lk--product-add--field--value">
                            <input type="text"
                                   name="PROPERTIES[<?=$property["CODE"]?>]"
                                   value="<?
                                   if(empty($arResult["company"]["PROPERTY_" . $property["CODE"] . "_VALUE"]))
                                       echo $property["VALUE"];
                                   else
                                       echo $arResult["company"]["PROPERTY_" . $property["CODE"] . "_VALUE"];
                                   ?>" />
                            </div>
                        </div>
                <?endforeach;?>
            <?endif;?>

            <div class="s7sbp--marketplace--saler--lk--title ff--roboto">Документы</div>
                <?$APPLICATION->IncludeComponent("bitrix:main.file.input", "dnd",
                    array(
                        "INPUT_NAME"=>"FILES",
                        "INPUT_VALUE" => $arResult["DOCS"],
                        "MULTIPLE"=>"Y",
                        "MODULE_ID"=>"iblock",
                        "MAX_FILE_SIZE"=>"",
                        "ALLOW_UPLOAD"=>"",
                        "ALLOW_UPLOAD_EXT"=>""
                    ),
                    $component,
                    Array("HIDE_ICONS" => "Y"))?>


			<div class="s7sbp--marketplace--saler--lk--product-add--field" style="text-align: right">
				<button type="submit" class="s7sbp--marketplace--saler--lk--product-add--field--btn">Сохранить</button>
			</div>

		</form>
        <?else:?>
            <div class="alert alert-danger mt-3">Вы не являетесь администратором компании.</div>
        <?endif;?>


	</div>
</div>