<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Iblock\PropertyTable,
    \Bitrix\Main\Localization\Loc,
	\Bitrix\Main\Loader,
	\Studio7spb\Marketplace\CMarketplaceSeller;

Loc::loadMessages(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/studio7spb.marketplace/options.php");

if (!\Bitrix\Main\Loader::includeModule('studio7spb.marketplace')) {
	ShowError(Loc::getMessage('studio7spb.marketplace_MODULE_NOT_INSTALLED'));
	die();
}

class SellerPersonalCompany extends CBitrixComponent
{
    private $_properties;
    private $_docsFileName = "FILES";
    private $_propertyDocsCode = "COMP_REGISTER_DOCS";

    /**
     * @return mixed
     */
    public function getProperties()
    {
        return $this->_properties;
    }

    /**
     * @param mixed $properties
     */
    public function setProperties($properties)
    {
        if(!empty($properties)){
            $properties = PropertyTable::getList(array(
                "filter" => array(
                    "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                    "CODE" => $properties
                )
            ));
            while ($property = $properties->fetch()){
                $property["VALUE"] = "";
                $this->_properties[$property["CODE"]] = $property;
            }
        }
    }

	public function onPrepareComponentParams($params)
	{
		// $params["user_company"] = CMarketplaceSeller::getCompanyByUser();
		return $params;
	}

	/**
	 * Process incoming request
	 * @return void
	 */
	protected function processRequest()
	{

		if($this->request->isPost() && isset($this->request['Update']) && $this->request['Update'] == "Y" && check_bitrix_sessid())
		{
			$postFields = $this->request->getPostList();

			Loader::includeModule("iblock");

			$updateElementFields = array(
				"NAME" => htmlspecialcharsbx($postFields["NAME"]),
				"PREVIEW_TEXT" => $postFields["PREVIEW_TEXT"],
				"DETAIL_TEXT" => $postFields["DETAIL_TEXT"]
			);

			$oIblockElement = new \CIBlockElement;
			$oIblockElement->update($this->arResult["company"]["ID"], $updateElementFields);

            if(!empty($this->arParams["PROPERTIES"])){
                $arProperties = array();
                foreach ($this->arParams["PROPERTIES"] as $property){
                    $arProperties[$property] = $postFields["PROPERTIES"][$property];
                }

                if(!empty($postFields[$this->_docsFileName])){
                    $files = array();
                    $i = 0;
                    $this->getCompanyDocs();
                    foreach($postFields[$this->_docsFileName] as $fileID){
                        if($fileID > 0 && !in_array($fileID, $this->arResult["DOCS"])){
                            $files["n".$i++] = $fileID;
                        }
                    }

                    CIBlockElement::SetPropertyValues($this->arResult["company"]["ID"], $this->arResult["company"]["IBLOCK_ID"], $files, $this->_propertyDocsCode);
                }

                \CIBlockElement::SetPropertyValuesEx(
                    $this->arResult["company"]["ID"],
                    $this->arResult["company"]["IBLOCK_ID"],
                    $arProperties
                );
            }

			\localRedirect($this->arParams["SEF_FOLDER"]."company/");

		}
	}

	public function getCompany(){
        global $USER;
	    $select = array(
	        "ID",
	        "NAME",
            "IBLOCK_ID",
            "PREVIEW_TEXT",
            "DETAIL_TEXT"
        );
        if(!empty($this->arParams["PROPERTIES"])){
            foreach ($this->arParams["PROPERTIES"] as $property){
                $select[] = "PROPERTY_" . $property;
            }
        }
        $company = CIBlockElement::GetList(array(),
            array(
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                "PROPERTY_COMP_USER" => $USER->GetID()
            ),
            false,
            false,
            $select
        );

        if($company = $company->Fetch()){

            $this->arResult["company"] = $company;
            $this->getCompanyDocs();
        }
    }

    public function getCompanyDocs(){

        if($this->arResult["company"]["ID"] > 0){
            $rsElementProperties = \CIBlockElement::GetProperty(
                $this->arParams["IBLOCK_ID"],
                $this->arResult["company"]["ID"],
                array(),
                array(
                    "CODE"=>$this->_propertyDocsCode,
                    "!VALUE" => false
                )
            );
            while($arElementProperty = $rsElementProperties->Fetch()) {
                $this->arResult["DOCS"][] = $arElementProperty["VALUE"];
            }
        }

    }

	public function executeComponent()
	{
		if(!CMarketplaceSeller::canDoOperation("edit_company")){
			\localRedirect("/auth/");
			return false;
		}
        $this->getCompany();
		//$this->arResult["company"] = CMarketplaceSeller::getCompanyByUser();
        if(!empty($this->arParams["PROPERTIES"])){
            $this->setProperties($this->arParams["PROPERTIES"]);
        }
        $this->arResult["PROPERTIES"] = $this->getProperties();
		$this->processRequest();
		$this->includeComponentTemplate();
	}
}