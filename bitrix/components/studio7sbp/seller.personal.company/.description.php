<?
/**
 * seller_personal_company
 * seller_personal_company
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "seller_personal_company",
    "DESCRIPTION" => "seller_personal_company",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "seller_personal_company",
            "NAME" => "seller_personal_company"
        )
    ),
);
