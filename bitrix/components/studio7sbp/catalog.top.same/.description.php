<?
/**
 * catalog_top_same
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "catalog_top_same",
    "DESCRIPTION" => "catalog_top_same",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "catalog_top_same",
            "NAME" => "catalog_top_same"
        )
    ),
);