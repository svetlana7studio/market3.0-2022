<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogTopComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult['ITEMS']))
    return;

$arParams["LINE_ELEMENT_COUNT"] = 3;

Loc::loadLanguageFile(__FILE__);
?>

<div class="position-relative">

    <!-- Start Partners -->
    <div class="hm306 s7spb-row s7spb-row-justify-start d-overflow-hidden" id="same-items">

        <?
        foreach ($arResult['ITEMS'] as $key=>$arItem):

            $generalParams["KEY"] = $key;
            $arItem["JS_ID_BASIS"] = "same-item";
            # prices
            $arItem["PRICES"] = [
                "price_red" => [
                    "VALUE" => $arItem["CATALOG_PRICE_4"],
                    "PRINT_VALUE" => number_format($arItem["CATALOG_PRICE_4"], 2, '.', ' ') . Loc::getMessage("CURRUNCY_RUB")
                ],
                "price_green" => [
                    "VALUE" => $arItem["CATALOG_PRICE_6"],
                    "PRINT_VALUE" => number_format($arItem["CATALOG_PRICE_6"], 2, '.', ' ') . Loc::getMessage("CURRUNCY_RUB")
                ],
                "price_yellow" => [
                    "VALUE" => $arItem["CATALOG_PRICE_8"],
                    "PRINT_VALUE" => number_format($arItem["CATALOG_PRICE_8"], 2, '.', ' ') . Loc::getMessage("CURRUNCY_RUB")
                ],
            ];
            # properties
            $arItem["DISPLAY_PROPERTIES"] = [
                "ANYTOS_VENDOR" => [
                    "CODE" => "ANYTOS_VENDOR",
                    "NAME" => Loc::getMessage("PROPERTY_ANYTOS_VENDOR_NAME"),
                    "DISPLAY_VALUE" => $arItem["PROPERTY_ANYTOS_VENDOR_VALUE"]
                ],
                "TRADE_MARK" => [
                    "CODE" => "TRADE_MARK",
                    "NAME" => Loc::getMessage("PROPERTY_TRADE_MARK_NAME"),
                    "DISPLAY_VALUE" => '<a href="/brands/' . $arItem["PROPERTY_TRADE_MARK_CODE"] . '/">' . TruncateText($arItem["PROPERTY_TRADE_MARK_NAME"], 8) . '</a>'


                ],
            ];

            # stikers
            if(!empty($arItem["PROPERTY_DISCOUNT_VALUE"])){
                foreach ($arItem["PROPERTY_DISCOUNT_VALUE"] as $key=>$value){
                    switch ($key){
                        case 12:
                            $arItem["STIKER"][] = [
                                "CODE" => "new",
                                "TITLE" => $value,
                                "CLASS" => ""
                            ];
                            break;
                        case 13:
                            $arItem["STIKER"][] = [
                                "CODE" => "hit",
                                "TITLE" => $value,
                                "CLASS" => "stiker-primary"
                            ];
                            break;
                        case 14:
                            $arItem["STIKER"][] = [
                                "CODE" => "hit",
                                "TITLE" => $value,
                                "CLASS" => "stiker-warnin"
                            ];
                            break;
                    }
                }
            }

            $arItem["PROPERTIES"] = [
                    "MOQ" => $arItem["PROPERTY_MOQ_VALUE"],
                    "MULTIPLICITY" => $arItem["PROPERTY_MULTIPLICITY_VALUE"]
            ];

            // PROPERTY_DISCOUNT_VENDOR


            $APPLICATION->IncludeComponent(
                'bitrix:catalog.item',
                'catalog.top',
                [
                    'RESULT' => array(
                        'ITEM' => $arItem,
                        'PRICES' => [
                            "price_red" => [
                                "TITLE" => $arItem["CATALOG_GROUP_NAME_4"]
                            ],
                            "price_green" => [
                                "TITLE" => $arItem["CATALOG_GROUP_NAME_6"]
                            ],
                            "price_yellow" => [
                                "TITLE" => $arItem["CATALOG_GROUP_NAME_8"]
                            ],
                        ],
                    ),
                    'PARAMS' => $generalParams
                ],
                $component,
                array('HIDE_ICONS' => 'Y')
            );

            unset($arItem);

        endforeach;
        ?>

    </div>

    <?if(count($arResult['ITEMS']) > $arParams["LINE_ELEMENT_COUNT"]):?>
        <div class="carousel-controls">
            <div onclick="koorochkaSamedSlider.slidePrev();" class="carousel-control-prev"></div>
            <div onclick="koorochkaSamedSlider.slideNext();" class="carousel-control-next"></div>
        </div>
    <?endif;?>
    <!-- End Partners -->

</div>