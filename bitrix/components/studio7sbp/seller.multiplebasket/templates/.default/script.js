var multiplebasket = {
    body: {
        obj: null,
        closed: "d-none",
    },
    toggle: function (t) {
        this.body.obj = $(t).next();
        if(this.body.obj.hasClass(this.body.closed)){
            this.body.obj.removeClass(this.body.closed);
            $(t).find(".arrow").removeClass("arrow-down");
            $(t).find(".arrow").addClass("arrow-up");
        }else{
            this.body.obj.addClass(this.body.closed);
            $(t).find(".arrow").removeClass("arrow-up");
            $(t).find(".arrow").addClass("arrow-down");
        }
    }
};


/**
  * multiplebasket Popup
  */

multiplebasketPopup = {

    obPopupWin: null,

    initPopupWindow: function()
    {
        if (this.obPopupWin)
            return;

        this.obPopupWin = BX.PopupWindowManager.create('multiplebasket_popup', null, {
            autoHide: true,
            offsetLeft: 0,
            offsetTop: 0,
            overlay : true,
            closeByEsc: true,
            titleBar: true,
            closeIcon: true,
            contentColor: 'white',
            className: "multiplebasketPopup",
        });
    },

    open: function (t) {

        this.initPopupWindow();

        var popupContent = '<form>';
            popupContent += '<div class="form-group">';
            popupContent += '<lable>Введите название корзины</lable>';
            popupContent += '<input type="text" class="form-control" name="name">';
            //popupContent += '<small id="emailHelp" class="form-text text-muted">Имя корзины поможет легко найти её в списке</small>';
            popupContent += '</div>';
            popupContent += '<input type="submit" class="btn" value="Сохранить" />';
            popupContent += '</form>';

        this.obPopupWin.setTitleBar("Сохранение корзины");
        this.obPopupWin.setContent(popupContent);
        this.obPopupWin.show();

        return false;
    }
};