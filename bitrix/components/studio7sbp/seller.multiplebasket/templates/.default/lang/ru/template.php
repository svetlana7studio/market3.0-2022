<?
$MESS = array(
    "MULTIBASKET_TITLE" => "Мои корзины",
    "MULTIBASKET_EMPTY" => "У вас пока нет собранных и сохраненных корзин.",
    "MULTIBASKET_PRODUCT_ID" => "id товара",
    "MULTIBASKET_NAME" => "Наименование",
    "MULTIBASKET_QUANTITY" => "Количество",
    "MULTIBASKET_PRICE" => "Цена",
    "MULTIBASKET_USE" => "Использовать эту корзину как текущую",
    "MULTIBASKET_ADD" => "Добавить товары с этой корзины в текущую текущую",
    "MULTIBASKET_DELETE" => "Удалить эту корзину из списка",
    "MULTIBASKET_TOTAL_SUM" => "Товаров на: PRICE руб.",
    "MULTIBASKET_TOTAL_WEIGHT" => "Общий вес: WEIGHT кг",
    "MULTIBASKET_TOTAL_SPACE" => "Общий объем: SPACE м3"
);