<?
/**
 * Povered by artem@koorochka.com
 * @var array $arParams
 * @var array $arResult
 * @var $this CBitrixComponentTemplate
 */

use Bitrix\Main\Localization\Loc;

$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
Loc::loadLanguageFile(__FILE__);
?>
<div id="multiplebasket">
<h1 class="h1"><?=Loc::getMessage("MULTIBASKET_TITLE")?></h1>
<?if(empty($arResult["BASKETS"])):?>
    <div class="alert alert-danger text-center"><?=Loc::getMessage("MULTIBASKET_EMPTY")?></div>
<?else:?>
    <?foreach ($arResult["BASKETS"] as $basket):?>
        <div class="card my-3">
            <div class="card-header s7spb-row cursor-pointer"
                 onclick="multiplebasket.toggle(this)">
                <div class="s7spb-col"><?=$basket["NAME"]?></div>
                <div class="s7spb-col s7spb-col-auto"><div class="arrow arrow-down mt1"></div></div>
            </div>
            <div class="card-body d-none">

                <table class="table table-striped">

                    <tr class="table-warning">
                        <th class="text-nowrap"><?=Loc::getMessage("MULTIBASKET_PRODUCT_ID")?></th>
                        <th class="text-nowrap"><?=Loc::getMessage("MULTIBASKET_NAME")?></th>
                        <th class="text-center text-nowrap"><?=Loc::getMessage("MULTIBASKET_QUANTITY")?></th>
                        <th class="text-center text-nowrap">
                            <?
                            echo Loc::getMessage("MULTIBASKET_PRICE");
                            $current = current($basket["PARAMS"]);
                            $arResult["CURRENCY_FORMAT"][$current["CURRENCY"]][0] = " ";
                            echo $arResult["CURRENCY_FORMAT"][$current["CURRENCY"]];
                            ?>
                        </th>
                    </tr>

                    <?foreach ($basket["PARAMS"] as $basketItem):?>
                        <tr>
                            <td><?=$basketItem["PRODUCT_ID"]?></td>
                            <td><?=TruncateText($basketItem["NAME"], 80)?></td>
                            <td class="text-nowrap text-center"><?=$basketItem["QUANTITY"]?></td>
                            <td class="text-nowrap text-center">
                                <?=round($basketItem["PRICE"], 2)?>
                            </td>
                        </tr>
                    <?endforeach;?>

                </table>
            </div>
            <div class="card-footer">
                <div class="row justify-content-between">

                    <div class="col-12 col-sm-auto">
                        <div class="text-nowrap"><?=Loc::getMessage("MULTIBASKET_TOTAL_SUM", array("PRICE" => $basket["TOTAL"]["SUM"]))?></div>
                        <div class="text-nowrap"><?=Loc::getMessage("MULTIBASKET_TOTAL_WEIGHT", array("WEIGHT" => $basket["TOTAL"]["WEIGHT"]))?></div>
                        <div class="text-nowrap"><?=Loc::getMessage("MULTIBASKET_TOTAL_SPACE", array("SPACE" => $basket["TOTAL"]["SPACE"]))?></div>
                    </div>

                    <div class="col-12 col-sm-auto text-sm-right">
                        <a href="/system/7studio/sale/multiplebasket/procedures/use.php?basket=<?=$basket["ID"]?>"
                           class="text-nowrap mb-2"><?=Loc::getMessage("MULTIBASKET_USE")?></a>
                        <br>
                        <a href="/system/7studio/sale/multiplebasket/procedures/compare.php?basket=<?=$basket["ID"]?>"
                           class="text-nowrap mb-2"><?=Loc::getMessage("MULTIBASKET_ADD")?></a>
                        <br>
                        <a href="/system/7studio/sale/multiplebasket/procedures/delete.php?basket=<?=$basket["ID"]?>"
                           class="text-nowrap "><?=Loc::getMessage("MULTIBASKET_DELETE")?></a>
                    </div>
                </div>
            </div>
        </div>
    <?endforeach;?>
<?endif;?>
</div>