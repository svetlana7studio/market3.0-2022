<?
/**
 * seller_multiplebasket
 * seller multiplebasket
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "seller multiplebasket",
    "DESCRIPTION" => "seller multiplebasket",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "seller_multiplebasket",
            "NAME" => "seller multiplebasket"
        )
    ),
);
