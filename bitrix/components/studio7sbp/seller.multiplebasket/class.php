<?
/**
 * Povered by artem@koorochka.com
 */

use Studio7spb\Marketplace\MultipleBasketTable,
    Bitrix\Currency\CurrencyLangTable,
    Bitrix\Main\Loader;

class sellerMultiplebasket extends CBitrixComponent
{
    private $_baskets;
    private $_currencies;

    /**
     * @return mixed
     */
    public function getBaskets()
    {
        return $this->_baskets;
    }

    /**
     * @param mixed $baskets
     */
    public function setBaskets($baskets)
    {
        $this->_baskets = $baskets;
    }

    public function setDBBaskets()
    {
        $baskets = array();
        $db = MultipleBasketTable::getList(array(
            "order" => array("ID" => "DESC"),
            "limit" => 30,
            "filter" => array(
                "FUSER_ID" => \CSaleBasket::GetBasketUserID()
            )
        ));
        while ($basket = $db->fetch()){
            $basket["PARAMS"] = unserialize($basket["PARAMS"]);

            // basket items calculate and formate
            $basket["TOTAL"] = array(
                "SUM" => 0,
                "WEIGHT" => 0,
                "SPACE" => 0
            );
            foreach ($basket["PARAMS"] as $id=>$basketItem){
                $basket["PARAMS"][$id]["QUANTITY"] = round($basketItem["QUANTITY"]);
                $basket["TOTAL"]["SUM"] += $basketItem["PRICE"] * $basketItem["QUANTITY"];
                $basket["TOTAL"]["WEIGHT"] += $basketItem["WEIGHT"] * $basketItem["QUANTITY"] / 1000;
                $basket["TOTAL"]["WEIGHT"] = round($basket["TOTAL"]["WEIGHT"], 0);

                // get element params for space
                $element = CIBlockElement::GetList(
                    array(),
                    array("ID" => $id),
                    false,
                    false,
                    array(
                        "ID",
                        "IBLOCK_ID",
                        "PROPERTY_Master_CTN_PCS",
                        "PROPERTY_Master_CTN_CBM"
                    )
                );

                if($element = $element->Fetch()){
                    if($basket["PARAMS"][$id]["QUANTITY"] > 0 && $element["PROPERTY_MASTER_CTN_PCS_VALUE"] > 0) {
                        $basket["TOTAL"]["SPACE"] += round($basket["PARAMS"][$id]["QUANTITY"] / $element["PROPERTY_MASTER_CTN_PCS_VALUE"] * $element["PROPERTY_MASTER_CTN_CBM_VALUE"], 2);
                    }
                }
            }

            $baskets[] = $basket;
        }
        $this->setBaskets($baskets);
    }

    /**
     * @return mixed
     */
    public function getCurrencies()
    {
        return $this->_currencies;
    }

    /**
     * @param mixed $currencies
     */
    public function setCurrencies($currencies)
    {
        $this->_currencies = $currencies;
    }

    public function setDBCurrencies()
    {
        $currencies = array();
        $db = Bitrix\Currency\CurrencyLangTable::getList(array(
            "filter" => array(
                "LID" =>LANGUAGE_ID
            ),
            "select" => array(
                "FORMAT_STRING",
                "CURRENCY"
            )
        ));
        while ($currency = $db->fetch())
        {
            $currencies[$currency["CURRENCY"]] = $currency["FORMAT_STRING"];
        }
        $this->setCurrencies($currencies);
    }



    public function executeComponent()
    {
        if(
            !Loader::includeModule("sale") &&
            !Loader::includeModule("iblock")
        )
            return;
        if($this->startResultCache()){
            $this->setDBCurrencies();
            $this->endResultCache();
        }
        $this->setDBBaskets();

        $this->arResult = array(
            "CURRENCY_FORMAT" => $this->getCurrencies(),
            "BASKETS" => $this->getBaskets()
        );

        $this->includeComponentTemplate();
    }
}