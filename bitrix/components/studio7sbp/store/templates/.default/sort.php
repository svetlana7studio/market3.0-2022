<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

	$request = \Bitrix\Main\Context::getCurrent()->getRequest();

	$aDisplayVariants = array("card", "line");
	if(!isset($_SESSION["sectionItemType"])) {
		$_SESSION["sectionItemType"] = $aDisplayVariants[0];
	}
	$requestDisplay = $request->get('display');
	if(in_array($requestDisplay, $aDisplayVariants)) {
		$_SESSION["sectionItemType"] = $requestDisplay;
	}
	$sectionItemType = $_SESSION["sectionItemType"];


	$arAvailableSort = array();
	$arSorts = $arParams["SORT_BUTTONS"];
	if(in_array("POPULARITY", $arSorts)){
		$arAvailableSort["SHOWS"] = array("SHOWS", "desc");
	}
	if(in_array("NAME", $arSorts)){
		$arAvailableSort["NAME"] = array("NAME", "asc");
	}
	if(in_array("PRICE", $arSorts)){ 
		$arSortPrices = $arParams["SORT_PRICES"];
		if($arSortPrices == "MINIMUM_PRICE" || $arSortPrices == "MAXIMUM_PRICE"){
			$arAvailableSort["PRICE"] = array("PROPERTY_".$arSortPrices, "desc");
		} else{
			$price = CCatalogGroup::GetList(array(), array("NAME" => $arParams["SORT_PRICES"]), false, false, array("ID", "NAME"))->GetNext();
			$arAvailableSort["PRICE"] = array("CATALOG_PRICE_".$price["ID"], "desc"); 
		}
	}
	if(in_array("QUANTITY", $arSorts)){
		$arAvailableSort["CATALOG_AVAILABLE"] = array("QUANTITY", "desc");
	}
	$sort = "SHOWS";
	$requestSort = $request->get("sort");
	if(($requestSort && array_key_exists(ToUpper($requestSort), $arAvailableSort)) || (array_key_exists("sort", $_SESSION) && array_key_exists(ToUpper($_SESSION["sort"]), $arAvailableSort)) || $arParams["ELEMENT_SORT_FIELD"]){
		if($requestSort){
			$sort = ToUpper($requestSort); 
			$_SESSION["sort"] = ToUpper($requestSort);
		}
		elseif($_SESSION["sort"]){
			$sort = ToUpper($_SESSION["sort"]);
		}
		else{
			$sort = ToUpper($arParams["ELEMENT_SORT_FIELD"]);
		}
	}

	$sort_order = $arAvailableSort[$sort][1];
	$requestOrder = $request->get("order");
	if(($requestOrder && in_array(ToLower($requestOrder), Array("asc", "desc"))) || ($requestOrder && in_array(ToLower($requestOrder), Array("asc", "desc")) ) || $arParams["ELEMENT_SORT_ORDER"]){
		if($requestOrder){
			$sort_order = $requestOrder;
			$_SESSION["order"] = $requestOrder;
		}
		elseif($_SESSION["order"]){
			$sort_order = $_SESSION["order"];
		}
		else{
			$sort_order = ToLower($arParams["ELEMENT_SORT_ORDER"]);
		}
	}
?>

	<div class="s7sbp--marketplace--catalog-section" data-entity="<?=$containerName?>">
		<div class="s7sbp--marketplace--section-title">
			<span><?=$APPLICATION->ShowTitle()?></span>
			<span class="s7sbp--marketplace--section-item-type">
				<!--noindex-->
				<a class="s7sbp--marketplace--section-item-type--icon card<?=$sectionItemType == "card" ? " active" : ""?>" href="<?=$APPLICATION->GetCurPageParam('display=card', array('display'));?>"></a>
				<a class="s7sbp--marketplace--section-item-type--icon list<?=$sectionItemType == "line" ? " active" : ""?>" href="<?=$APPLICATION->GetCurPageParam('display=line', array('display'));?>"></a>
				<!--/noindex-->
			</span>
		</div>
		<div class="s7sbp--marketplace--section-item-sort">
			<span class="s7sbp--marketplace--section-item-sort--title"><?=GetMessage('SECT_SORT_TITLE')?></span>
			<?foreach($arAvailableSort as $key => $val):?>
				<?$newSort = $sort_order == 'desc' ? 'asc' : 'desc';
				$current_url = $APPLICATION->GetCurPageParam('sort='.$key.'&order='.$newSort, 	array('sort', 'order'));
				$url = str_replace('+', '%2B', $current_url);?>
				<a href="<?=$url;?>" class="s7sbp--marketplace--section-item-sort--item <?=($sort == $key ? 'active' : '')?> <?=$sort_order?> <?=$key?>" rel="nofollow">
					<span><?=GetMessage('SECT_SORT_'.$key)?></span>
				</a>
			<?endforeach;?>
		</div>

<?
	if($sort == "PRICE"){
		$sort = $arAvailableSort["PRICE"][0];
	}
	if($sort == "CATALOG_AVAILABLE"){
		$sort = "CATALOG_QUANTITY";
	}
?>