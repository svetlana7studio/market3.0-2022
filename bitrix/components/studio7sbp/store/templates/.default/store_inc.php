<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

var_export($arResult);


$arFilterStore = array(
	"IBLOCK_ID" => $arParams["IBLOCK_STORE_ID"],
	"ACTIVE" => "Y",
	"GLOBAL_ACTIVE" => "Y",
);
if (0 < intval($arResult["VARIABLES"]["STORE_ID"]))
	$arFilterStore["ID"] = $arResult["VARIABLES"]["STORE_ID"];
elseif ('' != $arResult["VARIABLES"]["STORE_CODE"])
	$arFilterStore["=CODE"] = $arResult["VARIABLES"]["STORE_CODE"];

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilterStore), "/iblock/store"))
{
	$arStore = $obCache->GetVars();
}
elseif ($obCache->StartDataCache())
{
	$arStore = array();
	if (Loader::includeModule("iblock"))
	{
		$dbRes = CIBlockElement::GetList(array(), $arFilterStore, false, false, array("ID", "NAME", "PREVIEW_PICTURE", "PREVIEW_TEXT"));

		if(defined("BX_COMP_MANAGED_CACHE"))
		{
			global $CACHE_MANAGER;
			$CACHE_MANAGER->StartTagCache("/iblock/store");

			if ($arStore = $dbRes->Fetch())
				$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_STORE_ID"]);

			$CACHE_MANAGER->EndTagCache();
		}
		else
		{
			if(!$arStore = $dbRes->Fetch())
				$arStore = array();
		}
	}
	$obCache->EndDataCache($arStore);
}
if (!isset($arStore))
	$arStore = array();


if(empty($arStore)) {
	// \Bitrix\Iblock\Component\Tools::process404(
	// 	""
	// 	,($arParams["SET_STATUS_404"] === "Y")
	// 	,($arParams["SET_STATUS_404"] === "Y")
	// 	,($arParams["SHOW_404"] === "Y")
	// 	,$arParams["FILE_404"]
	// );
}



