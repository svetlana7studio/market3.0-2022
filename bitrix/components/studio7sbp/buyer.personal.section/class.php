<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
	\Bitrix\Sale,
	\Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/studio7spb.marketplace/options.php");

if (!\Bitrix\Main\Loader::includeModule('studio7spb.marketplace')) {
	ShowError(Loc::getMessage('studio7spb.marketplace_MODULE_NOT_INSTALLED'));
	die();
}

class BuyerPersonalOrderSection extends CBitrixComponent
{

	protected $moduleOptionList = array();

	// public function onPrepareComponentParams($params)
	// {
	// 	if (!isset($params['MAIN_CHAIN_NAME']))
	// 	{
	// 		$params['MAIN_CHAIN_NAME'] = Loc::getMessage("SPS_CHAIN_MAIN");
	// 	}
	// 	return $params;
	// }
	function __construct($component = null) {
		$this->loadModuleOptions();
		parent::__construct($component);
	}

	protected function loadModuleOptions() {
		$this->moduleOptionList = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOptionList();
	}

	public function executeComponent()
	{
		$sectionsList = array();

		$this->setFrameMode(false);

		$this->arResult["LEFT_MENU_ITEMS"] = array(
			"orders",
            "baskets",
			"address",
			"favorite",
			"store",
			"requisits",
			//"messages",
			"settings",
		);

		$defaultUrlTemplates404 = array(
			"index" => "index.php",
			"payment" => "orders/payment",
			"order_detail" => "orders/#ID#",
			"order_cancel" => "orders/order_cancel/#ID#",
			"order_excel" => "orders/order_excel/#ID#",
		);

		foreach ($this->arResult["LEFT_MENU_ITEMS"] as $aAvailablePagesItem) {
			$defaultUrlTemplates404[$aAvailablePagesItem] = $aAvailablePagesItem."/";
		}

		global $APPLICATION;
		$this->arResult["CURRENT_URL"] = $APPLICATION->GetCurDir();



		// if (!CBXFeatures::IsFeatureEnabled('SaleAccounts') && true === false)
		// {
		// 	$this->arParams['SHOW_ACCOUNT_PAGE'] = 'N';
		// 	unset($this->arParams["SEF_URL_TEMPLATES"]['account']);
		// }
		// else
		// {
		// 	$defaultUrlTemplates404["account"] = "account/";
		// 	$sectionsList[] = "account";
		// }

		$componentVariables = array("CANCEL", "COPY_ORDER", "ID");
		$variables = array();

		if ($this->arParams["SEF_MODE"] == "Y")
		{
			$templatesUrls = CComponentEngine::makeComponentUrlTemplates($defaultUrlTemplates404, $this->arParams["SEF_URL_TEMPLATES"]);

			foreach ($templatesUrls as $url => $value)
			{
				$this->arResult["PATH_TO_".ToUpper($url)] = $this->arParams["SEF_FOLDER"].$value;
			}

			$this->arResult["PATH_TO_ORDER_COPY"] = $this->arResult["PATH_TO_ORDERS"]."?COPY_ORDER=Y&ID=#ID#";

			$variableAliases = CComponentEngine::makeComponentVariableAliases(array(), $this->arParams["VARIABLE_ALIASES"]);

			$componentPage = CComponentEngine::parseComponentPath(
				$this->arParams["SEF_FOLDER"],
				$templatesUrls,
				$variables
			);

			if ($componentPage === "order_detail_old")
			{
				$componentPage = "order_detail";
			}

			CComponentEngine::initComponentVariables($componentPage, $componentVariables, $variableAliases, $variables);

			if (empty($componentPage))
			{
				$componentPage = 'index';
			}

			$this->arResult = array_merge(
				Array(
					"SEF_FOLDER" => $this->arParams["SEF_FOLDER"],
					"URL_TEMPLATES" => $templatesUrls,
					"VARIABLES" => $variables,
					"ALIASES" => $variableAliases,
				),
				$this->arResult
			);
		}
		else
		{
			$variableAliases = CComponentEngine::makeComponentVariableAliases(array(), $this->arParams["VARIABLE_ALIASES"]);
			CComponentEngine::initComponentVariables(false, $componentVariables, $variableAliases, $variables);

			// $request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();

			$componentPage = $this->request->get('SECTION');

			if ($componentPage === "orders"
				&& $this->request->get('ID')
				&& !$this->request->get('COPY_ORDER'))
			{
				if ($this->request->get('CANCEL') === "Y")
				{
					$componentPage = "order_cancel";
				}
				else
				{
					$componentPage = "order_detail";
				}
			}

			if ($componentPage === "profile" && $this->request->get('ID'))
			{
				$componentPage = "profile_detail";
			}

			if (empty($componentPage))
			{
				if ($this->request->get('ID') && $this->request->get('COPY_ORDER') === 'Y')
				{
					$componentPage = "orders";
				}
				else
				{
					$componentPage = "index";
				}
			}

			$currentPage = $this->request->getRequestedPage();

			$this->arResult = array(
				"VARIABLES" => $variables,
				"ALIASES" => $variableAliases,
				"SEF_FOLDER" => $currentPage,
				""
			);

			$sectionsList = array_merge($sectionsList, array("orders", "profile", "private"));

			foreach ($sectionsList as $sectionName)
			{
				if ($sectionName === "orders")
				{
					$this->arResult["PATH_TO_ORDERS"] = $currentPage."?SECTION=orders";
					$this->arResult["PATH_TO_ORDER_DETAIL"] = $this->arResult["PATH_TO_ORDERS"]."&ID=#ID#";
					$this->arResult["PATH_TO_ORDER_CANCEL"] = $this->arResult["PATH_TO_ORDERS"]."&ID=#ID#";
					$this->arResult["PATH_TO_ORDER_COPY"] = $currentPage."?COPY_ORDER=Y&ID=#ID#&SECTION=orders";
				}
				elseif ($sectionName === "profile")
				{
					$this->arResult["PATH_TO_PROFILE"] = $currentPage."?SECTION=".$sectionName;
					$this->arResult["PATH_TO_PROFILE_DETAIL"] = $this->arResult["PATH_TO_PROFILE"]."&ID=#ID#";
					$this->arResult["PATH_TO_PROFILE_DELETE"] = $this->arResult["PATH_TO_PROFILE"]."&del_id=#ID#";
				}
				else
				{
					$this->arResult["PATH_TO_".ToUpper($sectionName)] = $currentPage."?SECTION=".$sectionName;
				}
			}
		}

		if ($componentPage == "index" && $this->getTemplateName() !== "")
			$componentPage = "template";

		if ($componentPage == "order_detail")
		{
			Loader::includeModule('sale');
			$id = urldecode(urldecode($variables["ID"]));
			$registry = Sale\Registry::getInstance(Sale\Order::getRegistryType());
			$orderClassName = $registry->getOrderClassName();

			$order = $orderClassName::loadByAccountNumber($id);
			if (!$order)
			{
				$order = $orderClassName::load((int)$id);
			}

			/** @var Sale\Order $order */
			if ($order)
			{
				if (
					(is_array($this->arParams["ORDER_HISTORIC_STATUSES"]) && in_array($order->getField('STATUS_ID'), $this->arParams["ORDER_HISTORIC_STATUSES"]))
					|| $order->isCanceled()
				)
				{
					$delimeter = (strpos($this->arResult["PATH_TO_ORDERS"], '?' ) !== false) ? '&' : '?';
					$this->arResult["PATH_TO_ORDERS"] .=  $delimeter . "filter_history=Y";
					if ($order->isCanceled())
					{
						$this->arResult["PATH_TO_ORDERS"] .=  "&show_canceled=Y";
					}
				}
			}
		}

		$this->includeComponentTemplate($componentPage);
	}
}