<?
$arDetParams = array(
    "PATH_TO_LIST" => $arResult["PATH_TO_ORDERS"],
    "PATH_TO_CANCEL" => $arResult["PATH_TO_ORDER_CANCEL"],
    "PATH_TO_COPY" => $arResult["PATH_TO_ORDER_COPY"],
    "PATH_TO_PAYMENT" => $arParams["PATH_TO_PAYMENT"],
    "SET_TITLE" =>$arParams["SET_TITLE"],
    "ID" => $arResult["VARIABLES"]["ID"],
    "ACTIVE_DATE_FORMAT" => $arParams["ACTIVE_DATE_FORMAT"],
    "ALLOW_INNER" => $arParams["ALLOW_INNER"],
    "ONLY_INNER_FULL" => $arParams["ONLY_INNER_FULL"],
    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
    "CACHE_TIME" => $arParams["CACHE_TIME"],
    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    "RESTRICT_CHANGE_PAYSYSTEM" => $arParams["ORDER_RESTRICT_CHANGE_PAYSYSTEM"],
    "DISALLOW_CANCEL" => $arParams["ORDER_DISALLOW_CANCEL"],
    "REFRESH_PRICES" => $arParams["ORDER_REFRESH_PRICES"],
    "HIDE_USER_INFO" => $arParams["ORDER_HIDE_USER_INFO"],

    "CUSTOM_SELECT_PROPS" => $arParams["CUSTOM_SELECT_PROPS"]
);
foreach($arParams as $key => $val)
{
    if(strpos($key, "PROP_") !== false)
        $arDetParams[$key] = $val;
}
$APPLICATION->RestartBuffer();

if (ini_get('mbstring.func_overload') & 2) {
    ini_set("mbstring.func_overload", 0);
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: filename=master.vendor.file.xlsx");
header('Cache-Control: max-age=0'); //no cache

require $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';

$APPLICATION->IncludeComponent(
    "bitrix:sale.personal.order.detail",
    "excel",
    $arDetParams,
    $component
);
die;