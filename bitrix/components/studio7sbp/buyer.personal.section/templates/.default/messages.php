<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetPageProperty('title', Loc::getMessage("SPS_TITLE_PRODUCTS"));
if($USER->IsAuthorized()) {
    include("left_menu.php");
}
?>

<div class="s7sbp--marketplace--saler--lk--right--inner">
    <div class="alert alert-success">Раздел в разработке</div>
</div>