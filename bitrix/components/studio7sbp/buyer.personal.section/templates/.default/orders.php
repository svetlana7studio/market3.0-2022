<?
/**
 * @var CBitrixComponentTemplate $this
 */

// $_REQUEST["filter_history"] = "Y";

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init("jquery");
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
$this->addExternalCss("/about/ui/select/css/nice.select.css");
$this->addExternalJs("/about/ui/select/js/jquery.nice-select.min.js");
$this->addExternalJs("/about/ui/select/js/script.js");



use Bitrix\Main\Localization\Loc;


Loc::loadMessages(__FILE__);

$APPLICATION->SetPageProperty('title', Loc::getMessage("SPS_TITLE_PRODUCTS"));

if($USER->IsAuthorized()) {
    include("left_menu.php");
}

if ($arParams['SHOW_ORDER_PAGE'] !== 'Y')
{
	LocalRedirect($arParams['SEF_FOLDER']);
}	

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_ORDERS"), $arResult['PATH_TO_ORDERS']);


?>
<div class="s7sbp--marketplace--saler--lk--right--inner">
    <?$APPLICATION->IncludeComponent(
        "bitrix:sale.personal.order.list",
        "bootstrap_v4",
        array(
            //"AJAX_MODE" => "Y",
            "DIAPAZONE" => 15,
            "PATH" => $APPLICATION->GetCurPage(),
            "PATH_TO_DETAIL" => $arResult["PATH_TO_ORDER_DETAIL"],
            "PATH_TO_CANCEL" => $arResult["PATH_TO_ORDER_CANCEL"],
            "PATH_TO_CATALOG" => $arParams["PATH_TO_CATALOG"],
            "PATH_TO_COPY" => $arResult["PATH_TO_ORDER_COPY"],
            "PATH_TO_BASKET" => $arParams["PATH_TO_BASKET"],
            "PATH_TO_PAYMENT" => $arParams["PATH_TO_PAYMENT"],
            "SAVE_IN_SESSION" => $arParams["SAVE_IN_SESSION"],
            "ORDERS_PER_PAGE" => $arParams["ORDERS_PER_PAGE"],
            "SET_TITLE" =>$arParams["SET_TITLE"],
            "ID" => $arResult["VARIABLES"]["ID"],
            "NAV_TEMPLATE" => $arParams["NAV_TEMPLATE"],
            "ACTIVE_DATE_FORMAT" => $arParams["ACTIVE_DATE_FORMAT"],
            "HISTORIC_STATUSES" => $arParams["ORDER_HISTORIC_STATUSES"],
            "ALLOW_INNER" => $arParams["ALLOW_INNER"],
            "ONLY_INNER_FULL" => $arParams["ONLY_INNER_FULL"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "DISALLOW_CANCEL" => $arParams["ORDER_DISALLOW_CANCEL"],
            "DEFAULT_SORT" => $arParams["ORDER_DEFAULT_SORT"],
            "RESTRICT_CHANGE_PAYSYSTEM" => $arParams["ORDER_RESTRICT_CHANGE_PAYSYSTEM"],
            "REFRESH_PRICES" => $arParams["ORDER_REFRESH_PRICES"],
        ),
        $component
    );
    ?>
</div>