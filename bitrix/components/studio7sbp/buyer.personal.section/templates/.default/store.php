<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetPageProperty('title', Loc::getMessage("SPS_TITLE_PRODUCTS"));
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
include("left_menu.php");
?>

<div class="s7sbp--marketplace--saler--lk--right--inner">
    <?$APPLICATION->IncludeComponent("studio7sbp:favorite.store", "favorite", array(
        "IBLOCK_ID" => 1,
        "USER_ID" => $USER->GetID(),
        "AJAX_MODE" => "Y"
    ))?>
</div>