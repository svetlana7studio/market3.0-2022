<?
/**
 * @var array $arResult
 * @var CBitrixComponent $this
 */

// <editor-fold defaultstate="collapsed" desc=" # prepare">
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Application;
Loc::loadLanguageFile(__FILE__);
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
CJSCore::Init();
Loc::loadMessages(__FILE__);
$APPLICATION->SetPageProperty('title', Loc::getMessage("SPS_TITLE_PRODUCTS"));
if($USER->IsAuthorized()) {
    include("left_menu.php");
}
// </editor-fold>

$arParams = [
    "AJAX_MODE" => "Y",
    "USER_ID" => $USER->GetID(),
    "USER_AUTORIZED" => $USER->IsAuthorized(),
    "PATH" => $APPLICATION->GetCurPage(),
    "ADRESS_COUNT" => 2,
    "USER_TYPE" => "BUYER",
    "PROPS" => array(
        "HOUSE",
        "FLAT"
    )
];

$oOption = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance();
$aGroups = array(
    $oOption->getOption("seller_admin_group"),
    $oOption->getOption("seller_moderator_group")
);

if(CSite::InGroup($aGroups)){
    $arParams["USER_TYPE"] = "SALLER";
}

$APPLICATION->IncludeComponent("studio7sbp:seller.address", "", $arParams);