<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
$this->addExternalCss("/about/ui/select/css/nice.select.css");
$this->addExternalJs("/about/ui/select/js/jquery.nice-select.min.js");
$this->addExternalJs("/about/ui/select/js/script.js");

if($USER->IsAuthorized()) {
    include("left_menu.php");
}

$oOption = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance();
$aGroups = array(
    $oOption->getOption("seller_admin_group"),
    $oOption->getOption("seller_moderator_group")
);

?>
<div class="s7sbp--marketplace--saler--lk--right--inner">

<?if(CSite::InGroup($aGroups)):?>
    <?$APPLICATION->IncludeComponent(
        "studio7sbp:seller.personal.company",
        "",
        [
            "SEF_FOLDER" => $arParams["SEF_FOLDER"],
            "IBLOCK_ID" => 6,
            "PROPERTIES" => array(
                "COMP_INN",
                "KPP",
                "OKPO",
                "OGRN",
                "OKATO",
                "BUHGALTER",
                "DIRECTOR",
                "INN",
                "SCORE",
                "BANK",
                "CORE_SCORE",
                "BIK"
            ),
            "BANK" => array(
                "SCORE",
                "BANK",
                "CORE_SCORE",
                "BIK",
            )
        ],
        false,
        array("HIDE_ICONS" => "Y")
    );?>
<?else:?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.profile",
        "buyer",
        [
            "FIELDS" => [
                "WORK_CITY",
                "WORK_COMPANY",
                "WORK_WWW",
                "WORK_POSITION",
                "PERSONAL_PHONE",
                "PERSONAL_MOBILE",
                "WORK_PHONE",
                "NAME",
                "SECOND_NAME",
                "LAST_NAME",
                "EMAIL",
            ],
            "USER_PROPERTY" => array(
                0 => "UF_SOCIALNET",
                1 => "UF_WORK_TYPE",
            ),
            "SET_TITLE" =>$arParams["SET_TITLE"],
            "AJAX_MODE" => $arParams['AJAX_MODE_PRIVATE'],
            "SEND_INFO" => $arParams["SEND_INFO_PRIVATE"],
            "CHECK_RIGHTS" => $arParams['CHECK_RIGHTS_PRIVATE'],
            "EDITABLE_EXTERNAL_AUTH_ID" => $arParams['EDITABLE_EXTERNAL_AUTH_ID'],
        ],
        $component
    );?>
<?endif;?>
</div>