<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
use Bitrix\Main\Localization\Loc;

//  http://teatr-msk.ru/seller/company/
// Маркетплэйс Партнеры Администраторы
// 5

if(in_array(5, $USER->GetUserGroupArray()))
{
    LocalRedirect(SITE_DIR . "seller/company/");
}

if($APPLICATION->GetCurPage(true) === SITE_DIR . "personal/index.php"){
    LocalRedirect(SITE_DIR . "personal/orders/");
}

Loc::loadMessages(__FILE__);
$APPLICATION->SetPageProperty('title', Loc::getMessage("SPS_TITLE_PRODUCTS"));
include("left_menu.php");

$alerts = array(
    "light",
    /*
    "danger",
    "success",
    "link",
    "dismissible",
    "warning",
    "primary",
    "secondary",
    "dark",
    "heading",
    "info",
    "dismissable"
    */
);

foreach ($alerts as $alert):
?>
    <div class="s7sbp--marketplace--saler--lk--right--inner">

        <div class="alert alert-<?=$alert?> row align-items-center">
            <div class="col-auto">
                <div class="ga">

                </div>
            </div>
            <div class="col">
                <div class="s7sbp--marketplace--saler--lk--left-menu--icon icon--messages">
                    У вас есть непрочитанные сообщения <span class="text-danger">0</span>
                </div>
            </div>
        </div>

    </div>
<?endforeach;?>

<style>
    .ga{
        background: url("/bitrix/components/studio7sbp/buyer.personal.section/templates/.default/images/avatar.png") no-repeat;
        width: 80px;
        height: 80px;
        background-size: 80px 80px;
        border-radius: 50%;
        display: block;
        margin: -1px;
        overflow: hidden;
        position: relative;
        z-index: 0;
    }
</style>