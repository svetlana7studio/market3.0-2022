<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;
?>
<?$this->SetViewTarget('left_menu');?>
	<ul id="s7sbp-sidebur-menu">
		<?foreach ($arResult["LEFT_MENU_ITEMS"] as $aLeftMenuItem):
			$itemUrl = $arParams["SEF_FOLDER"].$aLeftMenuItem."/";
		?>
			<li class="s7sbp--marketplace--saler--lk--left-menu--item s7sbp--marketplace--saler--lk--left-menu--icon icon--<?=$aLeftMenuItem?>" <?if(strrpos($arResult["CURRENT_URL"], $itemUrl) !== false):?> data-active-link="y"<?endif;?>>
				<a href="<?=$itemUrl?>"><?=Loc::getMessage("LEFT_MENU_ITEM_".ToUpper($aLeftMenuItem))?></a>
			</li>
		<?endforeach?>
	</ul>
<?$this->EndViewTarget();?>


