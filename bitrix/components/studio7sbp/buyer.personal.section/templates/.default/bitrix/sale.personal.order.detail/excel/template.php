<?
/**
 * @var array $arResult
 */

use Bitrix\Main\Localization\Loc;

$arParams["ALFAVITE"] = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q");
$arParams["MATRIX"] = array(
    "START" => array(
        "X" => "B",
        "Y" => 3,
        "ADRESS" => "B3"
    ),
    "CELL" => array(
        "X" => "B",
        "Y" => 3,
        "ADRESS" => "B3"
    )
);

$objPHPExcel = new PHPExcel();
$i = 1;

// <editor-fold defaultstate="collapsed" desc=" # Stylesheet">
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Total">
$arParams["MATRIX"]["CELL"]["Y"]++;
$arParams["MATRIX"]["CELL"]["Y"]++;
$arParams["MATRIX"]["CELL"]["X"] = $arParams["MATRIX"]["START"]["X"];
$arParams["MATRIX"]["CELL"]["ADRESS"] = $arParams["MATRIX"]["CELL"]["X"] . $arParams["MATRIX"]["CELL"]["Y"];
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Head">

# Stylesheet
$objPHPExcel->getActiveSheet()->freezePane('C6');
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setIndent(2);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getRowDimension($arParams["MATRIX"]["CELL"]["Y"])->setRowHeight(18);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getFont()->setSize(14);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('CECECE');

# Data filling
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], Loc::getMessage("BASKET_ITEMS_HEAD_NAME"), PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->getColumnDimension($arParams["ALFAVITE"][$i])->setWidth(24);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], Loc::getMessage("BASKET_ITEMS_HEAD_PRICE_NDS", array("CURRENCY" => str_replace("#", "", $arResult["CURRENCIES_FORMAT"][$arResult["CURRENCY"]]))), PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->getColumnDimension($arParams["ALFAVITE"][$i])->setWidth(24);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], Loc::getMessage("BASKET_ITEM_PROPERTY_LHW_ctn"), PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->getColumnDimension($arParams["ALFAVITE"][$i])->setWidth(24);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], Loc::getMessage("BASKET_ITEM_PROPERTY_DISPLAY_COUNT"), PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->getColumnDimension($arParams["ALFAVITE"][$i])->setWidth(24);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], Loc::getMessage("BASKET_ITEM_PROPERTY_Master_CTN_PCS"), PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->getColumnDimension($arParams["ALFAVITE"][$i])->setWidth(24);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], Loc::getMessage("BASKET_ITEMS_HEAD_QUANTITY"), PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->getColumnDimension($arParams["ALFAVITE"][$i])->setWidth(24);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], Loc::getMessage("BASKET_ITEM_PROPERTY_Master_CTN_CBM"), PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->getColumnDimension($arParams["ALFAVITE"][$i])->setWidth(24);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], Loc::getMessage("BASKET_ITEM_PROPERTY_WEIGHT"), PHPExcel_Cell_DataType::TYPE_STRING);
$i++;

$objPHPExcel->getActiveSheet()->getColumnDimension($arParams["ALFAVITE"][$i])->setWidth(24);
$objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], Loc::getMessage("BASKET_ITEMS_HEAD_SUM_NDS", array("CURRENCY" => str_replace("#", "", $arResult["CURRENCIES_FORMAT"][$arResult["CURRENCY"]]))), PHPExcel_Cell_DataType::TYPE_STRING);

# send matrix position to new row for busket items
$arParams["MATRIX"]["CELL"]["Y"]++;
$arParams["MATRIX"]["CELL"]["X"] = $arParams["MATRIX"]["START"]["X"];
$arParams["MATRIX"]["CELL"]["ADRESS"] = $arParams["MATRIX"]["CELL"]["X"] . $arParams["MATRIX"]["CELL"]["Y"] ;
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Elements">
$arResult["ELEMENTS"] = [];
if(!empty($arResult["BASKET"])){
    foreach ($arResult["BASKET"] as $arItem){
        $arResult["ELEMENTS"][] = $arItem["PRODUCT_ID"];
    }
}
if(!empty($arResult["ELEMENTS"])){
    CModule::IncludeModule("iblock");
    $elements = CIBlockElement::GetList(
        [],
        ["ID" => $arResult["ELEMENTS"]],
        false,
        false,
        [
            "ID",
            "IBLOCK_ID",
            "PROPERTY_LHW_ctn",
            "PROPERTY_DISPLAY_COUNT",
            "PROPERTY_Master_CTN_PCS",
            "PROPERTY_Master_CTN_CBM",
            "PROPERTY_WEIGHT",
        ]
    );
    while ($element = $elements->Fetch())
    {
        $arResult["ELEMENTS"][$element["ID"]] = $element;
    }
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Basket">
// PROPERTY_LHW_ctn
foreach ($arResult["BASKET"] as $arItem){
    $i = 1;
    # Stylesheet
    $objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setIndent(2);
    $objPHPExcel->getActiveSheet()->getStyle($arParams["MATRIX"]["CELL"]["Y"])->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getRowDimension($arParams["MATRIX"]["CELL"]["Y"])->setRowHeight(22);

    # Data filling
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arItem["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
    $i++;
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], round($arItem["PRICE"], 2), PHPExcel_Cell_DataType::TYPE_NUMERIC);
    $i++;
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], round($arResult["ELEMENTS"][$arItem["PRODUCT_ID"]]["PROPERTY_LHW_CTN_VALUE"], 2), PHPExcel_Cell_DataType::TYPE_NUMERIC);
    $i++;
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], round($arResult["ELEMENTS"][$arItem["PRODUCT_ID"]]["PROPERTY_DISPLAY_COUNT_VALUE"], 2), PHPExcel_Cell_DataType::TYPE_NUMERIC);
    $i++;
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arItem["QUANTITY"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
    $i++;
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], round($arResult["ELEMENTS"][$arItem["PRODUCT_ID"]]["PROPERTY_MASTER_CTN_PCS_VALUE"], 2), PHPExcel_Cell_DataType::TYPE_NUMERIC);
    $i++;
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], round($arResult["ELEMENTS"][$arItem["PRODUCT_ID"]]["PROPERTY_MASTER_CTN_CBM_VALUE"], 2), PHPExcel_Cell_DataType::TYPE_NUMERIC);
    $i++;
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], round($arResult["ELEMENTS"][$arItem["PRODUCT_ID"]]["PROPERTY_WEIGHT_VALUE"], 2), PHPExcel_Cell_DataType::TYPE_NUMERIC);
    $i++;

    ############
    $arItem["SUM"] = $arItem["QUANTITY"] * $arItem["PRICE"];
    $arItem["SUM"] = round($arItem["SUM"], 2);
    $objPHPExcel->getActiveSheet()->setCellValueExplicit($arParams["ALFAVITE"][$i] . $arParams["MATRIX"]["CELL"]["Y"], $arItem["SUM"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
    $i++;

    # send matrix position to new row for busket items
    $arParams["MATRIX"]["CELL"]["Y"]++;
    $arParams["MATRIX"]["CELL"]["X"] = $arParams["MATRIX"]["START"]["X"];
    $arParams["MATRIX"]["CELL"]["ADRESS"] = $arParams["MATRIX"]["CELL"]["X"] . $arParams["MATRIX"]["CELL"]["Y"] ;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Output">
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
// </editor-fold>