<?
$MESS = [
    "WORK_CITY" => "Город",
    "WORK_COMPANY" => "Наименование Компании(юридическое/фактическое)",
    "WORK_WWW" => "Название сайта компании",
    "WORK_POSITION" => "Должность контактного лица",
    "PERSONAL_PHONE" => "Телефон *",
    "PERSONAL_PHONE_DESC" => "Необходим для уточнения деталей заказа.",
    "PERSONAL_MOBILE" => "Телефон мобильный",
    "WORK_PHONE" => "Телефон рабочий",

    "NEW_PASSWORD" => "Новый пароль (мин. 6 символов):",
    "NEW_PASSWORD_CONFIRM" => "Подтверждение нового пароля:",
    "NEW_PASSWORD_REQ" => "Новый пароль:",
    "SAVE" => "Сохранить изменения",
    "RESET" => "Сбросить",
    "MAIN_RESET" => "Отмена",
    "PROFILE_DATA_SAVED" => "Изменения сохранены",
    "NAME" => "Имя:",
    "SECOND_NAME" => "Отчество:",
    "LAST_NAME" => "Фамилия:",
    "EMAIL" => "E-Mail:",
];





$MESS['LOGIN'] = "Логин (мин. 3 символа):";

