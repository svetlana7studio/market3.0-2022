<?
/**
 * @var array $arParams
 * @var array $arResult
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Bitrix\Main\Localization\Loc;

?>

<div class="bx_profile">
	<?
	ShowError($arResult["strProfileError"]);

	if ($arResult['DATA_SAVED'] == 'Y')
	{
		ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
	}

	?>
	<form method="post" name="form1" action="<?=$APPLICATION->GetCurUri()?>" enctype="multipart/form-data" role="form">
		<?=$arResult["BX_SESSION_CHECK"]?>
		<input type="hidden" name="lang" value="<?=LANG?>" />
		<input type="hidden" name="ID" value="<?=$arResult["ID"]?>" />
		<input type="hidden" name="LOGIN" value="<?=$arResult["arUser"]["LOGIN"]?>" />
		<div class="main-profile-block-shown" id="user_div_reg">

            <?foreach ($arParams["FIELDS"] as $code):?>
                <div class="form-group row align-items-center">
                    <label class="main-profile-form-label col-sm-12 col-md-5 text-md-right"
                           for="main-profile-<?=$code?>">
                        <?=Loc::getMessage($code)?>
                    </label>
                    <div class="col-sm-12 col-md-7">
                        <input class="form-control"
                               type="text"
                               name="<?=$code?>"
                               maxlength="50"
                               id="main-profile-<?=$code?>"
                               value="<?=$arResult["arUser"][$code]?>" />
                        <?if(!empty(Loc::getMessage($code . "_DESC"))):?>
                            <div class="iblock text_block"><?=Loc::getMessage($code . "_DESC")?></div>
                        <?endif;?>
                    </div>
                </div>
            <?endforeach;?>

			<?
			if ($arResult['CAN_EDIT_PASSWORD'])
			{
				?>

				<div class="form-group row align-items-center">
					<label class="main-profile-form-label col-sm-12 col-md-5 text-md-right" for="main-profile-password"><?=Loc::getMessage('NEW_PASSWORD_REQ')?></label>
                    <div class="col-sm-12 col-md-7">
						<input class=" form-control bx-auth-input main-profile-password" type="password" name="NEW_PASSWORD" maxlength="50" id="main-profile-password" value="" autocomplete="off"/>
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            <?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?>
                        </small>

                    </div>
				</div>

				<div class="form-group row align-items-center">
					<label class="main-profile-form-label main-profile-password col-sm-12 col-md-5 text-md-right" for="main-profile-password-confirm">
						<?=Loc::getMessage('NEW_PASSWORD_CONFIRM')?>
					</label>
                    <div class="col-sm-12 col-md-7">
						<input class="form-control" type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" id="main-profile-password-confirm" autocomplete="off" />
                    </div>
				</div>

				<?
			}
			?>

            <?// ********************* User properties ***************************************************?>
            <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>

                <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>

                    <div class="form-group row align-items-center">
                        <label class="main-profile-form-label main-profile-password col-sm-12 col-md-5 text-md-right">
                            <?=$arUserField["EDIT_FORM_LABEL"]?>
                            <?if ($arUserField["MANDATORY"]=="Y"):?>
                                <span class="starrequired">*</span>
                            <?endif;?>:
                        </label>
                        <div class="col-sm-12 col-md-7">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:system.field.edit",
                                $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                                array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y"));?>
                        </div>
                    </div>

                <?endforeach;?>

            <?endif;?>
            <?// ******************** /User properties ***************************************************?>
		</div>
		<p class="col-sm-9 col-md-offset-3">
			<input type="submit"
                   name="save"
                   class="btn btn-success btn-md px-3"
                   value="<?=(($arResult["ID"]>0) ? Loc::getMessage("MAIN_SAVE") : Loc::getMessage("MAIN_ADD"))?>">
			&nbsp;
			<input type="submit"
                   class="btn btn-warning btn-md px-3"
                   name="reset"
                   value="<?echo GetMessage("MAIN_RESET")?>">
		</p>
	</form>
	<div class="col-sm-12 main-profile-social-block">
		<?
		if ($arResult["SOCSERV_ENABLED"])
		{
			$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
				"SHOW_PROFILES" => "Y",
				"ALLOW_DELETE" => "Y"
			),
				false
			);
		}
		?>
	</div>
	<div class="clearfix"></div>
</div>