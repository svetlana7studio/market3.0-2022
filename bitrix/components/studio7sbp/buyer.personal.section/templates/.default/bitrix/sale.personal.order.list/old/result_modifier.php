<?
use Bitrix\Main\Loader,
    Bitrix\Sale\Order,
    Bitrix\Sale\Basket,
    Bitrix\Main\Application;

/**
 * get user orders for calculating
 */
if($USER->IsAuthorized()){
    $arResult["ORDERS_FILTER"] = array(
        "ORDERS" => array(),
        "ORDER_ID" => array(),
        "ITEMS" => array()
    );
    // get orders
    $orders = Order::getList(array(
        "filter" => array(
            "USER_ID" => $USER->GetID(),
            "LID" => SITE_ID,
            "!@STATUS_ID" => "F",
            "CANCELED" => "N"
        ),
        "select" => array(
            "ID",
            "STATUS_ID"
        )
    ));
    $arResult["INFO"]["ORDERS_COUNT"] = $orders->getSelectedRowsCount();
    while ($order = $orders->fetch())
    {
        $arResult["ORDERS_FILTER"]["ORDERS"][] = $order;
        $arResult["ORDERS_FILTER"]["ORDER_ID"][] = $order["ID"];
        if(is_set($arResult["INFO"]["STATUS"][$order["STATUS_ID"]]["COUNT"])){
            $arResult["INFO"]["STATUS"][$order["STATUS_ID"]]["COUNT"]++;
        }else{
            $arResult["INFO"]["STATUS"][$order["STATUS_ID"]]["COUNT"] = 1;
        }
    }
    // get items
    $items = Basket::getList(array(
        "filter" => array(
            "ORDER_ID" => $arResult["ORDERS_FILTER"]["ORDER_ID"]
        ),
        "select" => array("ID", "NAME", "PRODUCT_ID")
    ));
    while ($item = $items->fetch()){
        $arResult["ORDERS_FILTER"]["ITEMS"][$item["PRODUCT_ID"]] = $item;
    }

}

/**
 * search event order
 */
$request = Application::getInstance()->getContext()->getRequest();
if($request->isPost() && empty($request->get("clear-filter"))){

    $arResult["ORDERS_FILTER"]["ID"] = intval($request->get("search-order-id"));
    $arResult["ORDERS_FILTER"]["ITEM"] = intval($request->get("search-order-item"));

    if($arResult["ORDERS_FILTER"]["ID"] > 0){
        foreach ($arResult["ORDERS"] as $key=>$order){
            if($order["ORDER"]["ID"] <> $arResult["ORDERS_FILTER"]["ID"]){
                unset($arResult["ORDERS"][$key]);
            }
        }
    }

    if($arResult["ORDERS_FILTER"]["ITEM"] > 0){
        foreach ($arResult["ORDERS"] as $key=>$order){
            $delete = true;
            foreach ($order["BASKET_ITEMS"] as $arBasketItem){
                if($arBasketItem["ID"] == $arResult["ORDERS_FILTER"]["ITEM"]){
                    $delete = false;
                    break;
                }
            }
            if($delete){
                unset($arResult["ORDERS"][$key]);
            }
        }
    }

}


if(!empty($arResult["ORDERS"])){

    foreach ($arResult["ORDERS"] as $key=>$arOrder){

        // set image to basket item
        foreach ($arOrder["BASKET_ITEMS"] as $k=>$arBasketItem){

            $element = CIBlockElement::GetList(
                array(),
                array(
                    "ID" => $arBasketItem["PRODUCT_ID"]
                ),
                false,
                false,
                array(
                    "ID",
                    "IBLOCK_ID",
                    "PREVIEW_PICTURE",
                    "DETAIL_PICTURE",
                    //"PROPERTY_H_COMPANY.NAME"
                    "PROPERTY_FACTORY",
                    "PROPERTY_Production_time_days"
                )
            );

            if($element = $element->Fetch()){

                // fill factories and prodation time to order
                $arOrder["ORDER"]["FACTORIES"][] = $element["PROPERTY_FACTORY_VALUE"];
                $arOrder["ORDER"]["PRODUCTION_TIME"][floor($element["PROPERTY_PRODUCTION_TIME_DAYS_VALUE"] / $diapazone)][] = $element["ID"];

                if($element["PREVIEW_PICTURE"] <= 0){
                    $element["DETAIL_PICTURE"] = $element["PREVIEW_PICTURE"];
                }
                if($element["PREVIEW_PICTURE"] > 0){
                    $element["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                        $element["PREVIEW_PICTURE"],
                        array(
                            "height" => 150,
                            "width" => 50
                        ),
                        BX_RESIZE_IMAGE_PROPORTIONAL,
                        true
                    );
                }

                $arResult["ORDERS"][$key]["BASKET_ITEMS"][$k]["PREVIEW_PICTURE"] = $element["PREVIEW_PICTURE"];
                $arResult["ORDERS"][$key]["BASKET_ITEMS"][$k]["FACTORY"] = $element["PROPERTY_FACTORY_VALUE"];
                $arResult["ORDERS"][$key]["BASKET_ITEMS"][$k]["PRODUCTION_TIME"] = $element["PROPERTY_PRODUCTION_TIME_DAYS_VALUE"];
                //$arResult["ORDERS"][$key]["BASKET_ITEMS"][$k]["COMPANY"] = $element["PROPERTY_H_COMPANY_NAME"];
            }
        }

    }
}