var personalOrders = {

    toggleItems: function (t, order_id) {

        var basket = $(".order-" + order_id),
            switcher = $(t).find(".switcher");

        if(switcher.hasClass("switcher-close")){
            switcher.removeClass("switcher-close");
        }else{
            switcher.addClass("switcher-close");
        }

        if(basket.hasClass("d-none")){
            basket.removeClass("d-none");
            basket.addClass("d-block");
            basket.addClass("d-block d-md-table-row");
        }else{
            basket.removeClass("d-block");
            basket.removeClass("d-block d-md-table-row");
            basket.addClass("d-none");
        }

    }
};