<?
/**
 *
 */

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Bitrix\Main\Web\Cookie,
    Bitrix\Main\Web\Json,
    Bitrix\Iblock\ElementTable,
    Studio7spb\Marketplace\FavoriteTable;


class studio7sbpFavorite extends CBitrixComponent
{
    private $_module = "studio7spb.marketplace";
    private $_favorites;
    private $_favoriteName = "favorite";

    public function onPrepareComponentParams($arParams)
    {
        if($arParams["USER_ID"] > 0){
            $this->arParams["USER_ID"] = $arParams["USER_ID"];
        }
        if($this->arParams["USER_ID"] <= 0){
            global $USER;
            $this->arParams["USER_ID"] = $USER->GetID();
        }
    }

    /**
     * @return mixed
     */
    public function getFavorites()
    {
        return $this->_favorites;
    }

    /**
     * Set Favorite from db table filter
     */
    public function setFavorites()
    {
        if($this->arParams["USER_ID"] > 0){
            $favorites = array();
            $result = FavoriteTable::getList(array(
                "filter" => array("USER_ID" => $this->arParams["USER_ID"])
            ));
            while ($favorite = $result->fetch()){
                $favorites[] = $favorite["IBLOCK_ELEMENT_ID"];
            }
            $this->_favorites = $favorites;
        }
    }

    /**
     *
     * @param $element
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function addUserFavorite($element){
        $element = intval($element);

        if($element > 0){
            $favorites = $this->getUserFavorite();
            $favorites[] = $element;
            $favorites = array_unique($favorites);
            $favorites = Json::encode($favorites);
            $this->setFavoritesToCookie($favorites);
        }

        // in case user is autorize and element is set
        // add data to db
        if($element > 0 && $this->arParams["USER_ID"]){

            // check if exist
            $check = FavoriteTable::getList(array(
                "select" => array("ID"),
                "filter" => array(
                    "IBLOCK_ELEMENT_ID" => $element,
                    "USER_ID" => $this->arParams["USER_ID"]
                )
            ));
            if($check->getSelectedRowsCount() <= 0){
                FavoriteTable::add(array(
                    "IBLOCK_ELEMENT_ID" => $element,
                    "USER_ID" => $this->arParams["USER_ID"]
                ));
            }
        }

    }

    public function deleteUserFavorite($element)
    {
        $element = intval($element);

        if($element > 0){
            $favorites = $this->getUserFavorite();
            foreach ($favorites as $key=>$favorite){
                if($favorite == $element){
                    unset($favorites[$key]);
                }
            }
            $favorites = array_unique($favorites);
            $favorites = Json::encode($favorites);
            $this->setFavoritesToCookie($favorites);
        }

        // in case user is autorize and element is set
        // delete data from db
        if($element > 0 && $this->arParams["USER_ID"]){
            // get favorite if exist
            $favorite = FavoriteTable::getList(array(
                "select" => array("ID"),
                "filter" => array(
                    "IBLOCK_ELEMENT_ID" => $element,
                    "USER_ID" => $this->arParams["USER_ID"]
                )
            ));
            if($favorite = $favorite->fetch()){
                FavoriteTable::delete($favorite["ID"]);
            }
        }
    }

    /**
     * @param $favorites
     * @throws \Bitrix\Main\SystemException
     */
    public function setFavoritesToCookie($favorites){
        $context = Application::getInstance()->getContext();
        // create coocie
        $cookie = new Cookie(
            $this->_favoriteName,
            $favorites,
            time() + 60*60*24*30*12*2
        );
        $cookie->setDomain($context->getServer()->getHttpHost());
        $cookie->setHttpOnly(false);
        $cookie->setSecure(false);
        $context->getResponse()->addCookie($cookie);
        // https://dev.1c-bitrix.ru/api_d7/bitrix/main/web/cookie/index.php
        // Если вы будете добавлять cookie через ajax, подключая только prolog_before.php, то обязательно нужно обновить заголовки, иначе cookie на запишутся.
        $context->getResponse()->flush("");
    }

    /**
     * @return array|mixed|string|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function getUserFavorite($IBLOCK_ID=false){
        $favorites = Application::getInstance()->getContext()->getRequest()->getCookie($this->_favoriteName);
        if(empty($favorites)){
            $favorites = array();
            // get from db
            if($this->arParams["USER_ID"] > 0){
                $result = FavoriteTable::getList(array(
                    "filter" => array(
                        "USER_ID" => $this->arParams["USER_ID"]
                    ),
                ));
                while ($favorite = $result->fetch()){
                    $favorites[] = $favorite["IBLOCK_ELEMENT_ID"];
                }
                $this->setFavoritesToCookie(Json::encode($favorites));
            }
        }else{
            $favorites = Json::decode($favorites);
        }


        $favorites = $this->filterByIblockElements($favorites, $IBLOCK_ID);

        return $favorites;
    }

    public function filterByIblockElements($favorites, $IBLOCK_ID=false){
        $arResult = array();
        $arFilter = array(
            "ID" => $favorites,
            "ACTIVE" => "Y"
        );
        if($IBLOCK_ID > 0){
            $arFilter["IBLOCK_ID"] = $IBLOCK_ID;
        }
        Loader::includeModule("iblock");
        $elements = ElementTable::getList(array(
            "filter" => $arFilter,
            "select" => array("ID")
        ));
        while ($element = $elements->fetch())
        {
            $arResult[] = $element["ID"];
        }

        return $arResult;
    }

    /**
     * Responce to user request
     * @throws \Bitrix\Main\SystemException
     */
    public function userRequest(){
        global $APPLICATION;
        $request = Application::getInstance()->getContext()->getRequest();

        if(intval($request->get($this->_favoriteName)) > 0){
            $this->addUserFavorite($request->get($this->_favoriteName));
        }
    }

    /**
     * En execute
     * @return bool|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        // check module
        if(!Loader::includeModule($this->_module)){
            return false;
        }

        $this->userRequest();
        $this->arResult = $this->getUserFavorite();
        return $this->arResult;
    }
}