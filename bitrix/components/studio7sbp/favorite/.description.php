<?
/**
 * favorite
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "favorite",
    "DESCRIPTION" => "favorite",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "favorite",
            "NAME" => "favorite"
        )
    ),
);