<?
/**
 * seller_personal_shop
 * seller personal shop
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "seller personal shop",
    "DESCRIPTION" => "seller personal shop",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "seller_personal_shop",
            "NAME" => "seller personal shop"
        )
    ),
);
?>