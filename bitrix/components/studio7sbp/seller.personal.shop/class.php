<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
	\Bitrix\Main\Loader,
	\Studio7spb\Marketplace\CMarketplaceSeller;

Loc::loadMessages(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/studio7spb.marketplace/options.php");

if (!\Bitrix\Main\Loader::includeModule('studio7spb.marketplace')) {
	ShowError(Loc::getMessage('studio7spb.marketplace_MODULE_NOT_INSTALLED'));
	die();
}

class SellerPersonalShop extends CBitrixComponent
{
    private $_properties;

    /**
     * @return mixed
     */
    public function getProperties()
    {
        return $this->_properties;
    }

    /**
     * @param mixed $properties
     */
    public function setProperties($properties)
    {
        if(!empty($properties)){
            $properties = Bitrix\Iblock\PropertyTable::getList(array(
                "filter" => array(
                    "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                    "CODE" => $properties
                )
            ));
            while ($property = $properties->fetch()){
                $property["VALUE"] = "";
                $this->_properties[$property["CODE"]] = $property;
            }
        }
    }

	/**
	 * Process incoming request
	 * @return void
	 */
	protected function processRequest()
	{
        global $USER;
		if($this->request->isPost() && isset($this->request['Update']) && $this->request['Update'] == "Y" && check_bitrix_sessid())
		{
			$postFields = $this->request->getPostList();

			Loader::includeModule("iblock");

			$updateElementFields = array(
				"NAME" => $postFields["NAME"],
				"PREVIEW_TEXT" => $postFields["PREVIEW_TEXT"],
				"PREVIEW_TEXT_TYPE" => "html",
			);
			// set logo
			if($postFields["PREVIEW_PICTURE_del"]) {
				$updateElementFields["PREVIEW_PICTURE"] = \CIBlock::makeFileArray($postFields["PREVIEW_PICTURE"], true);
			} else {
				$updateElementFields["PREVIEW_PICTURE"] = \CIBlock::makeFileArray($postFields["PREVIEW_PICTURE"], false, null, array("allow_file_id" => true));
			}

            $oIblockElement = new \CIBlockElement;
			if($this->arResult["shop"]["ID"] > 0){
                $oIblockElement->update($this->arResult["shop"]["ID"], $updateElementFields);
            }
			else{
                $updateElementFields["IBLOCK_ID"] =  $this->arParams["IBLOCK_ID"];
                $updateElementFields["ACTIVE"] = "Y";
                $this->arResult["shop"]["ID"] = $oIblockElement->Add($updateElementFields);
                $this->arResult["shop"]["IBLOCK_ID"] = $this->arParams["IBLOCK_ID"];

                // COMP_USER
                CIBlockElement::SetPropertyValuesEx(
                    $this->arResult["shop"]["ID"],
                    $this->arResult["shop"]["IBLOCK_ID"],
                    array("COMP_USER" => array($USER->GetID()))
                );
            }


			// set props
            if(!empty($this->arParams["PROPERTIES"])){
                $values = array();
                foreach ($this->arParams["PROPERTIES"] as $property){
                    $values[$property] = $postFields["PROPERTIES"][$property];
                    $this->arResult["PROPERTIES"][$property]["VALUE"] = $postFields["PROPERTIES"][$property];
                }
                CIBlockElement::SetPropertyValuesEx(
                    $this->arResult["shop"]["ID"],
                    $this->arResult["shop"]["IBLOCK_ID"],
                    $values
                );
            }

			\localRedirect($this->arParams["SEF_FOLDER"]."shop/");

		}
	}

	public function getShop(){
        global $USER;
        $element = array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE");
        if(!empty($this->arParams["PROPERTIES"])){
            foreach ($this->arParams["PROPERTIES"] as $property) {
                $element[] = "PROPERTY_" . $property;
            }
        }

        $element = CIBlockElement::GetList(array(), array(
	        "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
            "PROPERTY_COMP_USER" => $USER->GetID()
        ), false,
            false,
            $element
        );
	    if($element = $element->Fetch()){
            $this->arResult["shop"] = $element;
        }

    }

	public function executeComponent()
	{
		if(!CMarketplaceSeller::canDoOperation("edit_shop")){
			\localRedirect("/auth/");
			return false;
		}

		//$this->arResult["shop"] = CMarketplaceSeller::getShopByUser();
        $this->getShop();

		if(!empty($this->arParams["PROPERTIES"])){
		    $this->setProperties($this->arParams["PROPERTIES"]);
        }
		$this->arResult["PROPERTIES"] = $this->getProperties();


        $this->processRequest();

		$this->includeComponentTemplate();
	}
}