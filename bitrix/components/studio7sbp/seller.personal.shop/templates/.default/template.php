<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array('popup', 'ajax', 'loader'));
?>

<div class="s7sbp--marketplace--saler--lk--right--inner">

    <div class="s7sbp--marketplace--saler--lk--title ff--roboto">Данные магазина</div>

    <div class="s7sbp--marketplace--saler--lk--form ff--roboto">

        <form method="POST">

            <?=bitrix_sessid_post()?>

            <input type="hidden" name="Update" value="Y">

            <div class="s7sbp--marketplace--saler--lk--product-add--field">
                <div class="s7sbp--marketplace--saler--lk--product-add--field--label">
                    <label for="NAME"><b>Название</b> <span class="required">*</span></label>
                </div>
                <div class="s7sbp--marketplace--saler--lk--product-add--field--value">
                    <textarea name="NAME"><?=$arResult["shop"]["NAME"]?></textarea>
                </div>
            </div>

            <div class="s7sbp--marketplace--saler--lk--product-add--field">
                <div class="s7sbp--marketplace--saler--lk--product-add--field--label w-100">
                    <b>Описание</b> <span class="required">*</span>
                </div>
                <?$APPLICATION->IncludeComponent(
                    "studio7sbp:lhe",
                    "",
                    Array(
                        "LHE_NAME" => "lhe_preview_text_form",
                        "LHE_ID" => "lhe_preview_text_form",
                        "INPUT_NAME" => "PREVIEW_TEXT",
                        "INPUT_VALUE" => $arResult["shop"]["PREVIEW_TEXT"],
                    ),
                    $component,
                    Array("HIDE_ICONS" => "Y")
                );?>
            </div>

            <br>
            <div class="s7sbp--marketplace--saler--lk--product-add--field">
                <div class="s7sbp--marketplace--saler--lk--product-add--field--label w-100">
                    <b>Логотип</b> <span class="required">*</span>
                </div>
                <?$APPLICATION->IncludeComponent("bitrix:main.file.input", "",
                    array(
                        "INPUT_NAME"=>"PREVIEW_PICTURE",
                        "INPUT_VALUE" => $arResult["shop"]["PREVIEW_PICTURE"],
                        "MULTIPLE"=>"N",
                        "MODULE_ID"=>"iblock",
                        "MAX_FILE_SIZE"=>"",
                        "ALLOW_UPLOAD"=>"I",
                        "ALLOW_UPLOAD_EXT"=>"",
                        "ENTITY_TITLE" => "логотип"
                    ),
                    $component,
                    Array("HIDE_ICONS" => "Y")
                );?>
            </div>

            <br>
            <div class="s7sbp--marketplace--saler--lk--product-add--field">
                <div class="s7sbp--marketplace--saler--lk--product-add--field--label w-100">
                    <b>Баннеры</b>
                </div>
                <?
                if(!empty($arResult["shop"]["BUNNERS"])){
                    foreach ($arResult["shop"]["BUNNERS"] as $id=>$bunner){
                        $APPLICATION->IncludeComponent("bitrix:main.file.input", "",
                            array(
                                "INPUT_NAME" => "BUNNER_" . $id,
                                "INPUT_VALUE" => $bunner["PREVIEW_PICTURE"],
                                "MULTIPLE"=>"N",
                                "MODULE_ID"=>"iblock",
                                "MAX_FILE_SIZE"=>"",
                                "ALLOW_UPLOAD"=>"I",
                                "ALLOW_UPLOAD_EXT"=>"",
                                "ENTITY_TITLE" => "баннер"
                            ),
                            $component,
                            Array("HIDE_ICONS" => "Y")
                        );
                    }
                }

                if(count($arResult["shop"]["BUNNERS"]) < 3){
                    $APPLICATION->IncludeComponent("bitrix:main.file.input", "",
                        array(
                            "INPUT_NAME" => "BUNNER_neo",
                            "INPUT_VALUE" => null,
                            "MULTIPLE"=>"N",
                            "MODULE_ID"=>"iblock",
                            "MAX_FILE_SIZE"=>"",
                            "ALLOW_UPLOAD"=>"I",
                            "ALLOW_UPLOAD_EXT"=>"",
                            "ENTITY_TITLE" => "баннер"
                        ),
                        $component,
                        Array("HIDE_ICONS" => "Y")
                    );
                }
                ?>
            </div>


            <?if(!empty($arResult["PROPERTIES"])):?>
                <?foreach($arResult["PROPERTIES"] as $property):?>
                    <div class="s7sbp--marketplace--saler--lk--product-add--field">
                        <div class="s7sbp--marketplace--saler--lk--product-add--field--label">
                            <label for="NAME">
                                <b><?=$property["NAME"]?></b>
                                <?if($property["IS_REQUIRED"] == "Y"):?>
                                    <span class="required">*</span>
                                <?endif;?>
                            </label>
                        </div>
                        <div class="s7sbp--marketplace--saler--lk--product-add--field--value">
                            <textarea name="PROPERTIES[<?=$property["CODE"]?>]"><?
                                if(empty($arResult["shop"]["PROPERTY_" . $property["CODE"] . "_VALUE"]))
                                    echo $property["VALUE"];
                                else
                                    echo $arResult["shop"]["PROPERTY_" . $property["CODE"] . "_VALUE"];
                                ?></textarea>
                        </div>
                    </div>
                <?endforeach;?>
            <?endif;?>

            <div class="s7sbp--marketplace--saler--lk--product-add--field" style="text-align: right">
                <button type="submit" class="s7sbp--marketplace--saler--lk--product-add--field--btn">Сохранить</button>
            </div>

        </form>

    </div>
</div>