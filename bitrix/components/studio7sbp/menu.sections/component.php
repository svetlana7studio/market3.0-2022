<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */


if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["ID"] = intval($arParams["ID"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["DEPTH_LEVEL"] = intval($arParams["DEPTH_LEVEL"]);
if($arParams["DEPTH_LEVEL"]<=0)
	$arParams["DEPTH_LEVEL"]=1;

$arResult["SECTIONS"] = array();
$arResult["ELEMENT_LINKS"] = array();

if($this->StartResultCache())
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
	}
	else
	{
		$rsSections = CIBlockSection::GetList(
			["left_margin"=>"asc"],
			[
				"IBLOCK_ID"=>$arParams["IBLOCK_ID"],
				"GLOBAL_ACTIVE"=>"Y",
				"IBLOCK_ACTIVE"=>"Y",
				"<="."DEPTH_LEVEL" => $arParams["DEPTH_LEVEL"]
			],
			false,
			[
				"ID",
				"DEPTH_LEVEL",
				"NAME",
				"XML_ID",
				"SECTION_PAGE_URL",
			]);


		while($arSection = $rsSections->GetNext())
		{
			$arResult["SECTIONS"][] = [
				"ID" => $arSection["ID"],
				"DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
				"~NAME" => $arSection["~NAME"],
				"XML_ID" => $arSection["XML_ID"],
				"~SECTION_PAGE_URL" => $arSection["~SECTION_PAGE_URL"]
			];
		}

		$this->EndResultCache();
	}
}


$aMenuLinksNew = [];
$menuIndex = 0;
$previousDepthLevel = 1;
foreach($arResult["SECTIONS"] as $arSection)
{
	if ($menuIndex > 0)
		$aMenuLinksNew[$menuIndex - 1][3]["IS_PARENT"] = $arSection["DEPTH_LEVEL"] > $previousDepthLevel;
	$previousDepthLevel = $arSection["DEPTH_LEVEL"];

	$arResult["ELEMENT_LINKS"][$arSection["ID"]][] = urldecode($arSection["~SECTION_PAGE_URL"]);
	$aMenuLinksNew[$menuIndex++] = array(
		htmlspecialcharsbx($arSection["~NAME"]),
		$arSection["~SECTION_PAGE_URL"],
		null,
		[
			"FROM_IBLOCK" => true,
			"IS_PARENT" => false,
			"DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
			"XML_ID" => $arSection["XML_ID"]
		]
	);
}

return $aMenuLinksNew;