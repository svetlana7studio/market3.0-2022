<?
/**
 * @var array $arParams
 * @var array $arResult
 */
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
?>

<?if(!empty($arResult["MESSAGE"])):?>
    <?if($arResult["MESSAGE"]["TYPE"] === "SUCCESS"):?>
        <div class="alert alert-success text-center"><?=$arResult["MESSAGE"]["TEXT"]?></div>
    <?endif;?>
    <?if($arResult["MESSAGE"]["TYPE"] === "ERROR"):?>
        <div class="alert alert-danger text-center"><?=$arResult["MESSAGE"]["TEXT"]?></div>
    <?endif;?>
<?endif;?>

<form class="form-inline"
      enctype="multipart/form-data"
      method="post"
      action="<?=$arParams["PAGE"]?>">

    <?=bitrix_sessid_post()?>

    <div class="mx-sm-3 mb-2">


        <?$APPLICATION->IncludeComponent("bitrix:main.file.input", "dnd",
            array(
                "INPUT_NAME"=>"FILES",
                "INPUT_VALUE" => $arResult["DOCS"],
                "MULTIPLE"=>"Y",
                "MODULE_ID"=>"iblock",
                "MAX_FILE_SIZE"=>"",
                "ALLOW_UPLOAD"=>"",
                "ALLOW_UPLOAD_EXT"=>""
            ),
            $component,
            Array("HIDE_ICONS" => "Y"))?>

    </div>

    <input type="submit"
           onclick="disableElement(this)"
           class="btn mb-2"
           value="Отправить регистрационные документы организации">

</form>