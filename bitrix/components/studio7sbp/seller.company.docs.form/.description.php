<?
/**
 * seller_company_docs_form
 * seller_company_docs_form
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "seller_company_docs_form",
    "DESCRIPTION" => "seller_company_docs_form",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "seller_company_docs_form",
            "NAME" => "seller_company_docs_form"
        )
    ),
);
