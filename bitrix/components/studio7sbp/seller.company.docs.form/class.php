<?
use Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc;

class sellerCompanyDocsForm extends CBitrixComponent
{

    private $_fileName = "FILES";
    private $_propertyCode = "COMP_REGISTER_DOCS";

    public function getCompanyDocs(){

        $rsElementProperties = \CIBlockElement::GetProperty(
            $this->arParams["IBLOCK_ID"],
            $this->arParams["ID"],
            array(),
            array(
                "CODE"=>$this->_propertyCode,
                "!VALUE" => false
            )
        );
        while($arElementProperty = $rsElementProperties->Fetch()) {
            $this->arResult["DOCS"][] = $arElementProperty["VALUE"];
            //$this->arResult["DOCS_FILES"][] = $arElementProperty;
            //CFile::Delete($arElementProperty["VALUE"]);
        }
    }

    public function executeComponent()
    {
        Loc::loadLanguageFile(__FILE__);

        $request = Application::getInstance()->getContext()->getRequest();

        if(check_bitrix_sessid() && $request->isPost()){

            $postFields = $request->getPostList();
            if(empty($postFields[$this->_fileName])){
                $this->arResult["MESSAGE"] = array(
                    "TYPE" => "ERROR",
                    "TEXT" => Loc::getMessage("MESSAGE_ERROR_FILE_EMPTY")
                );
            }else{

                $this->arResult["MESSAGE"] = array(
                    "TYPE" => "SUCCESS",
                    "TEXT" => Loc::getMessage("MESSAGE_SUCCESS_FILE_SAVE")
                );
                $files = array();
                $i = 0;
                $this->getCompanyDocs();
                foreach($postFields[$this->_fileName] as $fileID){
                    if($fileID > 0 && !in_array($fileID, $this->arResult["DOCS"])){
                        $files["n".$i++] = $fileID;
                    }
                }

                CIBlockElement::SetPropertyValues($this->arParams["ID"], $this->arParams["IBLOCK_ID"], $files, $this->_propertyCode);
            }

        }

        $this->getCompanyDocs();
        $this->includeComponentTemplate();
    }
}