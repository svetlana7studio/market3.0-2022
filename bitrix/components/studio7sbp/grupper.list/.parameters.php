<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"DISPLAY_PROPERTIES" => array(
			"NAME" => GetMessage("S7_MARKET_MSG_DISPLAY_PROPERTIES"),
			"TYPE" => "STRING",
			"PARENT" => "BASE",
		),
	)
);