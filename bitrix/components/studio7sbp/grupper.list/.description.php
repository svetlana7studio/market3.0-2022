<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("S7_MARKET_COM_GRUPPER_NAME"),
	"DESCRIPTION" => GetMessage("S7_MARKET_COM_GRUPPER_DESCR"),
	"ICON" => "",
	"PATH" => array(
		"ID" => "studio7spb_grupper",
		"SORT" => 2000,
	),
);
?>