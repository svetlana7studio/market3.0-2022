<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["GROUPED_ITEMS"] as $arrValue):?>
	<?if(is_array($arrValue["PROPERTIES"]) && count($arrValue["PROPERTIES"])>0):?>

		<tr><td colspan="2" style="background: transparent"><div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body--item--title-sub"><?=$arrValue["GROUP"]["NAME"]?></div></td></tr>
		
		<?foreach($arrValue["PROPERTIES"] as $arProp):?>
			<tr itemprop="additionalProperty" itemscope itemtype="http://schema.org/PropertyValue">
				<td class="char_name">
					<span <?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"){?>class="whint"<?}?>><?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"):?><div class="hint"><span class="icon"><i>?</i></span><div class="tooltip"><?=$arProp["HINT"]?></div></div><?endif;?><span itemprop="name"><?=$arProp["NAME"]?></span></span>
				</td>
				<td class="char_value">
					<span itemprop="value">
						<?if(count($arProp["DISPLAY_VALUE"]) > 1):?>
							<?=implode(', ', $arProp["DISPLAY_VALUE"]);?>
						<?else:?>
							<?=$arProp["DISPLAY_VALUE"];?>
						<?endif;?>
					</span>
				</td>
			</tr>
		<?endforeach;?>
	<?endif;?>
<?endforeach;?>

<?if(is_array($arResult["NOT_GROUPED_ITEMS"]) && count($arResult["NOT_GROUPED_ITEMS"])>0):?>
	<tr><td colspan="2" style="background: transparent"><div class="s7sbp--marketplace--catalog-element-detail-product--tabs--body--item--title-sub"><?=GetMessage("PRODUCT_TABS_ABOUT_TITLE_PROPERTY")?></div></td></tr>
	
	<?foreach($arResult["NOT_GROUPED_ITEMS"] as $arProp):?>
		<tr itemprop="additionalProperty" itemscope itemtype="http://schema.org/PropertyValue">
			<td class="char_name">
				<span <?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"){?>class="whint"<?}?>><?if($arProp["HINT"] && $arParams["SHOW_HINTS"] == "Y"):?><div class="hint"><span class="icon"><i>?</i></span><div class="tooltip"><?=$arProp["HINT"]?></div></div><?endif;?><span itemprop="name"><?=$arProp["NAME"]?></span></span>
			</td>
			<td class="char_value">
				<span itemprop="value">
					<?if(count($arProp["DISPLAY_VALUE"]) > 1):?>
						<?=implode(', ', $arProp["DISPLAY_VALUE"]);?>
					<?else:?>
						<?=$arProp["DISPLAY_VALUE"];?>
					<?endif;?>
				</span>
			</td>
		</tr>
	<?endforeach;?>
	
<?endif;?>