<?
use \Studio7spb\Marketplace\Search\SynonymTable,
    \Bitrix\Main\Loader,
    Bitrix\Main\Type\DateTime;

class Studio7spbSearchSynonymTable extends CBitrixComponent
{
    /**
     * @var array
     */
    private $_synonym;

    /**
     * @var array
     */
    private $_fields;

    /**
     * @return array
     */
    public function getFields()
    {
        $this->_fields = SynonymTable::getMap();
        foreach ($this->_fields as $code=>$field){
            if(in_array($code, ["ID", "DATE_CREATE", "DATE_UPDATE", "USER_CREATE", "USER_UPDATE"]))
                unset($this->_fields[$code]);
        }
        return $this->_fields;
    }

    /**
     * @param $ID
     * @return array|false
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getSynonymByID($ID)
    {
        if(Loader::includeModule("studio7spb.synonym"))
            $this->_synonym = SynonymTable::getById($ID)->fetch();
        return $this->_synonym;
    }

    private function saveSynonym()
    {
        global $USER;
        if($this->arResult["CURRENT_ID"] > 0){
            unset($this->arResult["SYNONYM"]["USER_CREATE"]);
            unset($this->arResult["SYNONYM"]["DATE_CREATE"]);
            $this->arResult["SYNONYM"]["USER_UPDATE"] = $USER->GetID();
            $this->arResult["SYNONYM"]["DATE_UPDATE"] = new DateTime();
            SynonymTable::update($this->arResult["CURRENT_ID"], $this->arResult["SYNONYM"]);
        }else{
            $this->arResult["SYNONYM"]["USER_CREATE"] = $USER->GetID();
            $this->arResult["SYNONYM"]["DATE_CREATE"] = new DateTime();
            $this->arResult["CURRENT_ID"] = SynonymTable::add($this->arResult["SYNONYM"]);
            $this->arResult["CURRENT_ID"] = $this->arResult["CURRENT_ID"]->getId();
        }

    }

    public function executeComponent()
    {
        if(!Loader::includeModule("studio7spb.synonym"))
            return;
        $this->arResult["GRID_ID"] = SynonymTable::getTableName();
        $this->arResult["FIELDS"] = $this->getFields();

        if($this->request->get("ID") > 0)
        {
            $this->arResult["SYNONYM"] = $this->getSynonymByID($this->request->get("ID"));
            $this->arResult["CURRENT_ID"] = $this->request->get("ID");
        }

        if($this->request->isPost())
        {
            foreach ($this->arResult["FIELDS"] as $code=> $value)
                $this->arResult["SYNONYM"][$code] = htmlspecialcharsEx($this->request[$code]);
            $this->saveSynonym();
            $this->arResult["MODE"] = "SAVE";
        }

        $this->includeComponentTemplate();
    }
}