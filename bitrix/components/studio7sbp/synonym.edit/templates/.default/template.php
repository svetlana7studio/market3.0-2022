<?
/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
switch ($arResult["MODE"]){
    case "SAVE":
        $this->addExternalJs("/bitrix/js/studio7spb.synonym/content_products.js");
        echo '<script>top.BX.spb7synonym.Page.reloadGrid("' . $arResult["GRID_ID"] . '");</script>';
        break;
}
?>

<form action="<?=$arParams["CURRENT_PAGE"]?>" method="post">
    <?=bitrix_sessid_post()?>
    <?if($arResult["CURRENT_ID"] > 0):?>
        <input type="hidden" name="ID" value="<?=$arResult["CURRENT_ID"]?>">
    <?endif;?>
    <?foreach ($arResult["FIELDS"] as $code=>$field):?>
    <div class="bx-sender-letter-field">
        <div class="bx-sender-caption"><?=$field["title"]?></div>
        <div class="bx-sender-value">
            <?if(empty($field["values"])):?>
                <input type="text"
                       name="<?=$code?>"
                       value="<?=$arResult["SYNONYM"][$code]?>"
                       size="100"
                       class="form-control" />
            <?else:?>
                <?foreach ($field["values"] as $value):?>
                    <div>
                        <label>
                            <input type="radio"
                                   name="<?=$code?>"
                                   <?
                                   if($arResult["SYNONYM"][$code] == $value)
                                       echo "checked";
                                   ?>
                                   value="<?=$value?>">
                            <?=Loc::getMessage("SYNONYM_" . $code . "_" . $value)?>
                        </label>
                    </div>
                <?endforeach;?>
            <?endif;?>
        </div>
    </div>
    <br>
    <?endforeach;?>
    <br>
    <input type="submit" value="<?=Loc::getMessage("SYNONYM_ACTIVE_SUBMIT")?>">
</form>