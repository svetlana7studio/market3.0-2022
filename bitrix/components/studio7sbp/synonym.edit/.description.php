<?
/**
 * synonym.edit
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "synonym.edit",
    "DESCRIPTION" => "synonym.edit",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "synonym.edit",
            "NAME" => "synonym.edit"
        )
    ),
);