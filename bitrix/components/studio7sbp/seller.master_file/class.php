<?php

use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\IO\File;
use Bitrix\Main\Loader;
use Bitrix\Main\Web\Json;
use Studio7spb\Marketplace\CMarketplaceOptions;

class sellerMasterFile extends CBitrixComponent implements Controllerable
{
    private $_company;
    private $_messages;

    // <editor-fold defaultstate="collapsed" desc=" # Errors">

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->_messages;
    }

    /**
     * @param array $messages
     */
    public function setMessages($messages)
    {
        $this->_messages = $messages;
    }

    /**
     * @param array $error
     */
    public function setMessage($message)
    {
        $this->_messages[] = $message;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Company">
    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->_company = $company;
    }

    public function setCompanyFromDB()
    {
        if($this->arParams["USER_ID"] <= 0){
            global $USER;
            $this->arParams["USER_ID"] = $USER->GetID();
        }
        if($this->arParams["COMPANY_IBLOCK_ID"] <= 0){
            $this->arParams["COMPANY_IBLOCK_ID"] = CMarketplaceOptions::getInstance()->getOption("company_iblock_id");
        }

        if($this->arParams["USER_ID"] > 0){

            $company = CIBlockElement::GetList(
                array(),
                array(
                    "IBLOCK_ID" => $this->arParams["COMPANY_IBLOCK_ID"],
                    "PROPERTY_COMP_USER" => $this->arParams["USER_ID"]
                ),
                false,
                false,
                array(
                    "ID",
                    "IBLOCK_ID",
                    "PROPERTY_IMPORT_FILE",
                    "PROPERTY_COMP_COMMISSION"
                )
            );

            if($company = $company->Fetch()){
                $this->setCompany($company);
            }

        }

        $this->arParams["UPLOAD_PATH"] = $_SERVER["DOCUMENT_ROOT"] . "/upload/excel/download/vendor_catalog_" . $this->arParams["USER_ID"] . "/";
        $this->arParams["UPLOAD_PATH_TMP"] = $this->arParams["UPLOAD_PATH"] . "tmp/";
        $this->arParams["TMP_FILE_NAME"] = 0;
        $this->arParams["IBLOCK_ID"] = CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id");
        $this->arParams["LIMIT"] = 10;
        $this->arParams["FILE_LIMIT"] = 50;

    }
    // </editor-fold>

    /**
     * Обязательный имплиментированный метод
     * @return array
     */
    public function configureActions()
    {
        // Сбрасываем фильтры по-умолчанию (ActionFilter\Authentication и ActionFilter\HttpMethod)
        // Предустановленные фильтры находятся в папке /bitrix/modules/main/lib/engine/actionfilter/
        return [
            'getElements' => [ // Ajax-метод
                'prefilters' => [],
            ],
        ];
    }

    /**
     * @param $work_start
     * @param int $lastid
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     */
    public function getElementsAction($work_start, $lastid=0)
    {
        Loader::includeModule("iblock");

        $arResult = [
            "p" => null,
            "lastid" => null,
            "status" => null
        ];

        if(($work_start == "Y" || $work_start == "load") && check_bitrix_sessid())
        {



            switch ($work_start){
                case "load":

                    # Последний запуск
                    if(check_bitrix_sessid()){

                        $this->setCompanyFromDB();
                        $this->arResult["COMPANY"] = $this->getCompany();
                        if($this->arResult["COMPANY"]["ID"] > 0){
                            $arResult = shell_exec('php ' . dirname(__FILE__) . '/create.excell.php ' . $this->arResult["COMPANY"]["ID"] . ' ' . $this->arParams["USER_ID"]  . ' ' . $_SERVER["DOCUMENT_ROOT"] . ' ' . $this->arParams["UPLOAD_PATH"]);
                            $arResult = Json::decode($arResult);
                        }

                    }

                    break;

                case "Y":

                    # Работа с временной таблицей
                    // TODO start
                    $this->setCompanyFromDB();
                    $this->arResult["COMPANY"] = $this->getCompany();
                    if($lastid <= 0)
                        $lastid = 0;


                    if($this->arResult["COMPANY"]["ID"] > 0){

                        $rsEl = CIBlockElement::GetList(
                            ["ID" => "ASC"],
                            [
                                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                                ">ID" => $_REQUEST["lastid"],
                                "PROPERTY_H_COMPANY" => 45431
                            ],
                            false,
                            ["nTopCount" => $this->arParams["LIMIT"]],
                            [
                                "ID",
                                "IBLOCK_ID",
                                "NAME",
                                "ACTIVE",
                                "DETAIL_PICTURE",
                                "IBLOCK_SECTION_ID",
                                "PROPERTY_VENDOR_ARTICLE",
                                "PROPERTY_FACTORY",
                                "PROPERTY_COUNTRY_BORN",
                                "PROPERTY_FOB_RMB",
                                "PROPERTY_DISPLAY_COUNT",
                                "PROPERTY_LHW_ctn",
                                "PROPERTY_INNER_BOX",
                                "PROPERTY_Master_CTN_PCS",
                                "PROPERTY_Master_CTN_CBM",
                                "PROPERTY_WEIGHT",
                                "PROPERTY_WEIGHT_NETTO",
                                "PROPERTY_L_ctn",
                                "PROPERTY_H_ctn",
                                "PROPERTY_W_ctn",
                                "PROPERTY_MOQ",
                                "PROPERTY_Production_time_days",
                                "PROPERTY_CATEGORY",
                                "PROPERTY_TRADE_MARK.NAME",
                                "PROPERTY_PACKAGE.NAME",
                                "PROPERTY_MATERIALS",
                                "PROPERTY_Expire_time_from_production_date",
                                "PROPERTY_SERTIFICATE",
                                "PROPERTY_NDS"
                            ]
                        );
                        $jsonExcel = [];
                        while ($arEl = $rsEl->Fetch())
                        {
                            /*
                             * Collect data here
                             */
                            // get Section
                            $arEl["SECTION"] = SectionTable::getList([
                                "filter" => [
                                    "IBLOCK_ID" => $arEl["IBLOCK_ID"],
                                    "ID" => $arEl["IBLOCK_SECTION_ID"]
                                ],
                                "select" => ["NAME", "XML_ID"]
                            ]);
                            $arEl["SECTION"] = $arEl["SECTION"]->fetch();

                            // get picture
                            if($arEl["DETAIL_PICTURE"] > 0){
                                $arEl["DETAIL_PICTURE"] = CFile::GetFileArray($arEl["DETAIL_PICTURE"]);
                            }

                            $jsonExcel[] = $arEl;

                            $lastid = intval($arEl["ID"]);
                        }
                        $jsonExcel = Json::encode($jsonExcel);

                        $rsLeftBorder = CIBlockElement::GetList(
                            ["ID" => "ASC"],
                            [
                                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                                "<=ID" => $lastid,
                                "PROPERTY_H_COMPANY" => 45431
                            ],
                            false,
                            false,
                            ["ID"]
                        );
                        $leftBorderCnt = $rsLeftBorder->SelectedRowsCount();
                        $rsAll = CIBlockElement::GetList(
                            ["ID" => "ASC"],
                            [
                                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                                "PROPERTY_H_COMPANY" => 45431
                            ],
                            false,
                            false,
                            ["ID"]
                        );

                        $allCnt = $rsAll->SelectedRowsCount();

                        $p = round(100*$leftBorderCnt/$allCnt, 2);

                        $strNum = $lastid + 2;
                        if($p < 100){
                            $arResult = [
                                "p" => $p,
                                "lastid" => $lastid,
                                "status" => "Обработка строки №".$strNum,
                            ];
                        }else{
                            $arResult = [
                                "p" => $p,
                                "lastid" => null,
                                //"status" => "Обработка строки №" . $lastid . "<br> Процесс проверки данных файла импорта завершён",
                                "status" => "Последний шаг процесса - сборка файла, может біть долгим. Пожалуйста, дождитесь окончания.",
                                "can_load" => "Y"
                            ];
                        }

                        if(!empty($errors)){
                            $arResult["error"] = implode("<br>", $errors);
                        }

                        // save tmp file
                        $this->arParams["TMP_FILE_NAME"] = $leftBorderCnt / $this->arParams["LIMIT"] / $this->arParams["FILE_LIMIT"];
                        $this->arParams["TMP_FILE_NAME"] = round($this->arParams["TMP_FILE_NAME"]);


                        if (File::isFileExists($this->arParams["UPLOAD_PATH_TMP"] . $this->arParams["TMP_FILE_NAME"])) {
                            File::putFileContents($this->arParams["UPLOAD_PATH_TMP"] . $this->arParams["TMP_FILE_NAME"], $jsonExcel . "\n" , File::APPEND);
                        }
                        else{
                            @mkdir($this->arParams["UPLOAD_PATH"], 0777);
                            @mkdir($this->arParams["UPLOAD_PATH_TMP"], 0777);
                            $fp = fopen($this->arParams["UPLOAD_PATH_TMP"] . $this->arParams["TMP_FILE_NAME"], "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту),мы создаем файл
                            fwrite($fp, $jsonExcel . "\n");
                            fclose($fp);
                        }

                    }

                    // TODO end
                    break;
            }

        }

        /**
         * отправляем данные
         */
        return $arResult;
    }

    public function executeComponent()
    {



        $this->setCompanyFromDB();
        $this->arResult["COMPANY"] = $this->getCompany();
        $this->includeComponentTemplate();
    }
}