<?
use Bitrix\Main\Loader,
    Bitrix\Iblock\ElementTable,
    Bitrix\Main\Web\Json,
    Studio7spb\Marketplace\ImportSettingsTable;
use Bitrix\Main\Localization\Loc;

// <editor-fold defaultstate="collapsed" desc=" # Prepare">
$_SERVER["DOCUMENT_ROOT"] = $argv[3];
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
require $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loc::loadLanguageFile(__FILE__);
Loader::includeModule("iblock");

if (ini_get('mbstring.func_overload') & 2) {
    ini_set("mbstring.func_overload", 0);
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # arParams">
$arParams = [
    "IBLOCK_ID" => 6,
    "COMPANY_ID" => $argv[1],
    "USER_ID" => $argv[2],
    "UPLOAD_PATH" => $argv[4],
    "UPLOAD_FILE_NAME" => "lansi.catalog." . date("d_m_Y_H_i_s") . ".xlsx",
    "ALPHAVITE" => [
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE"
    ],
    "ALPHAVITE_REQUIRED" => [
        "A", "D", "E", "F", "G", "I", "J", "K", "M", "N", "O", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC"
    ]
];

$arParams["UPLOAD_PATH_TMP"] = $arParams["UPLOAD_PATH"] . "tmp/";
@mkdir($arParams["UPLOAD_PATH"], 0777);
@mkdir($arParams["UPLOAD_PATH_TMP"], 0777);

// </editor-fold>

$arResult = [
    "FILES" => []
];

$arResult["FILES"] = array_diff(
    scandir($arParams["UPLOAD_PATH_TMP"]),
    array('..', '.')
);


if(!empty($arResult["FILES"]))
{
    /*
    $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_discISAM;
    $cacheSettings = array(
        'dir' => $arParams["UPLOAD_PATH_CACHE"]
    );
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
*/

    $objPHPExcel = new PHPExcel();
    $row = 1;

    # Stylesheet
    $objPHPExcel->getActiveSheet()->freezePane('D3');
    $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    #$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(22);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getFont()->setSize(18);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setWrapText(true);

    $objPHPExcel->getActiveSheet()->mergeCells("M" . $row . ":O" . $row);
    $objPHPExcel->getActiveSheet()->mergeCells("U" . $row . ":W" . $row);

    // <editor-fold defaultstate="collapsed" desc=" # Add header">
    foreach ($arParams["ALPHAVITE"] as $latter){
        $objPHPExcel->getActiveSheet()->SetCellValue($latter.$row, Loc::getMessage("XML_HEADER_" . $latter));

        if(in_array($latter, $arParams["ALPHAVITE_REQUIRED"])){
            $objPHPExcel->getActiveSheet()->getStyle($latter.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('a5b557');
            $objPHPExcel->getActiveSheet()->getStyle($latter.$row)->getFont()->getColor()->setARGB('2b4e0c');
        }



        ######## Merge M N O
        ######## Merge U V W

        switch ($latter){
            case "A":
                $objPHPExcel->getActiveSheet()->getColumnDimension($latter)->setWidth(16);
                break;
            default:
                $objPHPExcel->getActiveSheet()->getColumnDimension($latter)->setWidth(24);
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Add header description">
    $row++;
    $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setWrapText(true);

    foreach ($arParams["ALPHAVITE"] as $latter){
        $objPHPExcel->getActiveSheet()->SetCellValue($latter.$row, Loc::getMessage("XML_HEADER_DESC_" . $latter));
        switch ($latter){
            case "A":
                $objPHPExcel->getActiveSheet()->getColumnDimension($latter)->setWidth(16);
                break;
            default:
                $objPHPExcel->getActiveSheet()->getColumnDimension($latter)->setWidth(24);
        }
    }
    // </editor-fold>


    // <editor-fold defaultstate="collapsed" desc=" # Parse products from files">
    foreach ($arResult["FILES"] as $file){
        $jsonObjects = file($arParams["UPLOAD_PATH_TMP"] . $file);
        if(!empty($jsonObjects)){
            foreach ($jsonObjects as $arElements){
                $arElements = Json::decode($arElements);
                if(!empty($arElements)){
                    foreach ($arElements as $arEl){
                        $row++;

                        if(!empty($arEl["PROPERTY_LHW_CTN_VALUE"])){
                            $PROPERTY_LHW_CTN_VALUE = explode("х", $arEl["PROPERTY_LHW_CTN_VALUE"]);
                            foreach ($PROPERTY_LHW_CTN_VALUE as $key=>$value){
                                $value = str_replace(Loc::getMessage("XML_MEASURE_SM"), "", $value);
                                $value = trim($value);
                                $PROPERTY_LHW_CTN_VALUE[$key] = $value;
                            }
                        }

                        foreach ($arParams["ALPHAVITE"] as $latter){

                            // row styles
                            $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setWrapText(true);
                            $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(50);

                            // cells
                            switch ($latter){
                                case "A":

                                    if(is_array($arEl["DETAIL_PICTURE"])){
                                        $objDrawing = new PHPExcel_Worksheet_Drawing();
                                        $objDrawing->setName($arEl["NAME"]);
                                        $objDrawing->setDescription($arEl["NAME"]);
                                        $objDrawing->setPath($DOCUMENT_ROOT . $arEl["DETAIL_PICTURE"]["SRC"]);
                                        $objDrawing->setCoordinates($latter.$row);
                                        //setOffsetX works properly
                                        $objDrawing->setOffsetX(5);
                                        $objDrawing->setOffsetY(5);
                                        //set width, height
                                        $objDrawing->setWidth(100);
                                        $objDrawing->setHeight(40);
                                        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
                                        unset($objDrawing);
                                    }

                                    break;
                                case "B":
                                    if($arEl["ACTIVE"] == "Y"){
                                        $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, 1, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    }
                                    else{
                                        $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, 0, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    }
                                    break;
                                case "D":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "F":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_FACTORY_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "G":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_COUNTRY_BORN_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "I":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_VENDOR_ARTICLE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "K":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_FOB_RMB_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "L":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_DISPLAY_COUNT_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "M":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $PROPERTY_LHW_CTN_VALUE[0], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "N":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $PROPERTY_LHW_CTN_VALUE[1], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "O":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $PROPERTY_LHW_CTN_VALUE[2], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "P":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_INNER_BOX_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "Q":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_MASTER_CTN_PCS_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "R":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_MASTER_CTN_CBM_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "S":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_WEIGHT_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "T":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_WEIGHT_NETTO_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "U":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_L_CTN_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "V":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_H_CTN_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "W":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_W_CTN_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "X":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_MOQ_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "Y":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_PRODUCTION_TIME_DAYS_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;

                                case "AA":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_EXPIRE_TIME_FROM_PRODUCTION_DATE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "AB":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_SERTIFICATE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "AC":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_NDS_VALUE"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                case "AD":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["ID"], PHPExcel_Cell_DataType::TYPE_NUMERIC);
                                    break;
                                // section
                                case "E":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["SECTION"]["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                #case "G":
                                #    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["SECTION"]["XML_ID"], PHPExcel_Cell_DataType::TYPE_STRING);
                                #    break;
                                // higload-blocks
                                case "C":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_CATEGORY_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                // Enum type property
                                case "H":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_TRADE_MARK_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "J":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_PACKAGE_NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;
                                case "Z":
                                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($latter . $row, $arEl["PROPERTY_MATERIALS_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                                    break;

                            }
                        }
                    }
                }

            }
        }

        unlink($arParams["UPLOAD_PATH_TMP"] . $file);
    }
    // </editor-fold>

    ############# Start Write to excel file ###############
    $arResult["FILES"] = array_diff(
        scandir($arParams["UPLOAD_PATH"]),
        array('..', '.')
    );
    if(!empty($arResult["FILES"]))
    {
        foreach ($arResult["FILES"] as $file){
            unlink($arParams["UPLOAD_PATH"] . $file);
        }
    }
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save($arParams["UPLOAD_PATH"] . $arParams["UPLOAD_FILE_NAME"]);
    ############# End Write to excel file ###############
}

# Output
$PATH = $arParams["UPLOAD_PATH"] . $arParams["UPLOAD_FILE_NAME"];
$PATH = str_replace($DOCUMENT_ROOT, "", $PATH);

$arResult = [
    "p" => 100,
    "step" => "load",
    "status" =>  Loc::getMessage("MASTER_PROCESS_CREATE_SUCCESS", ["PATH" => $PATH])
];



$arResult = Json::encode($arResult);
echo $arResult;
# echo 'CurrentStatus = Array(100,"","' . Loc::getMessage("MASTER_PROCESS_CREATE_SUCCESS", ["PATH" => $PATH]) .'");';

unset($objPHPExcel);
die;
