<?
/**
 * seller_master_file
 * seller master file
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "seller master file",
    "DESCRIPTION" => "seller master file",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "seller_master_file",
            "NAME" => "seller master file"
        )
    ),
);
