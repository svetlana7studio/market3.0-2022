<?
/**
 * @var array $arResult
 * @var array $arParams
 * @var $this CBitrixComponentTemplate
 */

$hasErrors = false;
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");

if(!empty($arResult["MESSAGES"])):
    foreach ($arResult["MESSAGES"] as $message):
        $hasErrors = true;
        ?>
            <div class="alert alert-<?=$message["TYPE"]?>"><?=$message["TEXT"]?></div>
        <?
    endforeach;
endif;

if($arResult["COMPANY"]["ID"] > 0):
?>

    <div class="mx-2">

        <div id="progress-import-bar"
             class="progress d-none">
            <div class="progress-bar bg-success" style="width: 100%;"></div>
        </div>

        <div id="progress-import-status" class="alert alert-light d-none"></div>

        <button class="btn btn-success"
                onclick="lancyImport.setStart(1)">Начать процесс</button>


    </div>

<?else:?>
    <div class="s7sbp--marketplace--saler--lk--form ff--roboto">
        <div class="alert alert-danger mt-3">Вы не являетесь администратором компании.</div>
    </div>
<?endif;

d($arResult);

?>
