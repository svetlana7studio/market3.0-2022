<?
$MESS = array(
    "MESSAGE_ERROR_UNNOAN" => "Произошла системная ошибка",
    "MESSAGE_ERROR_MODULE_INSTALL" => "Не подключены необходимые модули",
    "MESSAGE_SUCCESS_EDIT" => "Данные успешно сохранены",
    "FIELD_OWNERSHIP_VALUE_CHOOSE" => "Выбрать",
    "FIELD_OWNERSHIP_VALUE_1" => "ООО",
    "FIELD_OWNERSHIP_VALUE_2" => "ПАО",
    "FIELD_OWNERSHIP_VALUE_3" => "ИП",
    "FIELD_OWNERSHIP_VALUE_4" => "АО",
);