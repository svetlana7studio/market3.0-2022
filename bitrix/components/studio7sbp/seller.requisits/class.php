<?
use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Studio7spb\Marketplace\RequisitsTable,
    Bitrix\Main\Application,
    Bitrix\Main\Type\Date;

class sellerRequisits extends CBitrixComponent
{

    private $_module = "studio7spb.marketplace";
    private $_fields;
    private $_userData;
    private $_messages = array(
        "ERROR" => array(),
        "WARNING"  => array(),
        "INFO"  => array(),
        "SUCCESS"  => array()
    );

    // <editor-fold defaultstate="collapsed" desc=" # Messages">
    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->_messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessage($message = array())
    {
        if(empty($message["TYPE"])){
            $message["TYPE"] = "ERROR";
        }
        if(empty($message["CODE"])){
            $message["TYPE"] = "SYSTEM";
        }
        if(empty($message["TEXT"])){
            $message["TYPE"] = Loc::getMessage("MESSAGE_ERROR_UNNOAN");
        }

        $this->_messages[$message["TYPE"]][$message["CODE"]] = $message["TEXT"];
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # Fields">

    /**
     * @return mixed
     */
    public function getFields()
    {
        return $this->_fields;
    }

    /**
     * @return mixed
     */
    public function getFormatFields()
    {
        foreach ($this->getFields() as $code=>$arField){
            if(
                $code == "ID" ||
                $code == "TIMESTAMP_X" ||
                $code == "USER_ID"
            ){
                unset($this->_fields[$code]);
            }
            if($code == "OWNERSHIP"){
                $this->_fields[$code]["data_type"] = "list";
                $this->_fields[$code]["values"] = array(
                    Loc::getMessage("FIELD_OWNERSHIP_VALUE_1"),
                    Loc::getMessage("FIELD_OWNERSHIP_VALUE_2"),
                    Loc::getMessage("FIELD_OWNERSHIP_VALUE_3"),
                    Loc::getMessage("FIELD_OWNERSHIP_VALUE_4")
                );
            }
        }

        return $this->getFields();
    }

    /**
     * Set fields from map
     */
    public function setFields()
    {
        $fields = RequisitsTable::getMap();

        d($this->arParams);
        d($fields);

        $this->_fields = $fields;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" # User data">

    /**
     * @return mixed
     */
    public function getUserData()
    {
        return $this->_userData;
    }

    /**
     * Set user data from db
     */
    public function setUserData()
    {
        if($this->arParams["USER_ID"] > 0){

            $userData = RequisitsTable::getList(array(
                "filter" => array(
                    "USER_ID" => $this->arParams["USER_ID"]
                )
            ));
            if($userData = $userData->fetch()){
                $this->_userData = $userData;
            }
            else
            {
                $arFields = array(
                    "USER_ID" => $this->arParams["USER_ID"],
                    "TIMESTAMP_X" => new Date()
                );
                $result = RequisitsTable::add($arFields);
                if($result->isSuccess()){
                    $this->_userData["ID"] = $result->getId();
                }else{
                    if(!empty($result->getErrorMessages())){
                        foreach ($result->getErrorMessages() as $key=>$message){
                            $this->setMessage(array(
                                "TYPE" => "ERROR",
                                "CODE" => $key,
                                "TEXT" => $message
                            ));
                        }
                    }
                }
            }

            $this->editUserData();
        }
    }

    public function editUserData(){
        $request = Application::getInstance()->getContext()->getRequest();
        if($request->isPost()){
            if($this->_userData["ID"] > 0){
                $arFields = $this->getUserData();
                foreach ($arFields as $code=>$field){
                    $arFields[$code] = $request->getPost($this->arParams["PREFIX"] . "-" . $code);
                    // LIKE_ADRESS_JUR
                    if(is_set($request->getPost("LIKE_ADRESS_JUR_" . $code))){
                        $arFields[$code] = $request->getPost($this->arParams["PREFIX"] . "-ADRESS_JUR");
                    }
                }

                unset($arFields["ID"]);
                $arFields["TIMESTAMP_X"] = new Date();
                $arFields["USER_ID"] = $this->arParams["USER_ID"];
                RequisitsTable::update($this->_userData["ID"], $arFields);
                LocalRedirect($this->arParams["PAGE_SUCCESS"]);
            }
        }else{
            if(
                !empty($request->get("action") == "edit") &&
                !empty($request->get("status") == "success")
            ){
                $this->setMessage(array(
                    "TYPE" => "SUCCESS",
                    "CODE" => "EDIT",
                    "TEXT" => Loc::getMessage("MESSAGE_SUCCESS_EDIT")
                ));
            }
        }
    }

    // </editor-fold>


    public function executeComponent()
    {
        if(!Loader::includeModule($this->_module)){
            $this->setMessage(array(
                "TYPE" => "ERROR",
                "CODE" => "SYSTEM",
                "TEXT" => Loc::getMessage("MESSAGE_ERROR_MODULE_INSTALL"))
            );
        }
        $this->setUserData();
        $this->setFields();

        $this->arResult["MESSAGES"] = $this->getMessages();
        $this->arResult["FIELDS"] = $this->getFormatFields();
        $this->arResult["USER_DATA"] = $this->getUserData();

        $this->includeComponentTemplate();
    }

}