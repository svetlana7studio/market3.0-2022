<?
/**
 * seller_requisits
 * seller requisits
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "seller requisits",
    "DESCRIPTION" => "seller requisits",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "seller_requisits",
            "NAME" => "seller requisits"
        )
    ),
);
?>