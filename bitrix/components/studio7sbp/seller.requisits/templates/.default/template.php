<?
/**
 * @var array $arParams
 * @var array $arResult
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>

<form id="requisits"
      action="<?=$arParams["PAGE"]?>"
      method="post">

    <?foreach ($arResult["MESSAGES"] as $type=>$messgas):?>
        <?if(!empty($messgas)):?>
            <div class="alert alert-<?
                switch ($type){
                    case "SUCCESS":
                        echo "success";
                        break;
                    case "INFO":
                        echo "info";
                        break;
                    case "WARNING":
                        echo "warning";
                        break;
                    default:
                        echo "danger";
                }
            ?>">
                <?=implode("<br>", $messgas)?>
            </div>
        <?endif;?>
    <?endforeach;?>

    <?foreach ($arResult["FIELDS"] as $code=>$arFields):?>
        <div class="form-group row">
            <label for="<?=$arParams["PREFIX"]?>-<?=$code?>"
                   class="text-sm-right col-sm-4 col-form-label">
                <?=$arFields["title"]?>:
            </label>
            <div class="col">
                <?
                switch ($arFields["data_type"]){
                    case "list":
                ?>
                    <select class="form-control"
                            id="<?=$arParams["PREFIX"]?>-<?=$code?>"
                            name="<?=$arParams["PREFIX"]?>-<?=$code?>">
                        <option value=""><?=Loc::getMessage("FIELD_OWNERSHIP_VALUE_CHOOSE")?></option>
                        <?foreach ($arFields["values"] as $value):?>
                            <option value="<?=$value?>" <?
                                if($value == $arResult["USER_DATA"][$code])
                                    echo "selected";
                            ?>>
                                <?=$value?>
                            </option>
                        <?endforeach;?>
                    </select>
                <?
                    break;
                    case "text":
                ?>
                    <div>
                        <?if($code === "ADRESS_JUR"):?>
                            <textarea class="form-control"
                                      id="<?=$arParams["PREFIX"]?>-<?=$code?>"
                                      name="<?=$arParams["PREFIX"]?>-<?=$code?>"><?=$arResult["USER_DATA"][$code]?></textarea>
                        <?else:?>
                            <label><input type="checkbox"
                                    <?if(
                                            empty($arResult["USER_DATA"][$code]) ||
                                            $arResult["USER_DATA"][$code] === $arResult["USER_DATA"]["ADRESS_JUR"]
                                    ):?>
                                        checked="checked"
                                    <?endif;?>
                                          onchange="sellerRequisits.likeAdressJur(this)"
                                          name="LIKE_ADRESS_JUR_<?=$code?>"> <?=Loc::getMessage("LIKE_ADRESS_JUR")?></label>
                            <textarea class="<?=empty($arResult["USER_DATA"][$code]) ? "form-control d-none" : "form-control"?>"
                                      onkeypress="sellerRequisits.fillIndividualAdress(this)"
                                      id="<?=$arParams["PREFIX"]?>-<?=$code?>"
                                      name="<?=$arParams["PREFIX"]?>-<?=$code?>"><?=$arResult["USER_DATA"][$code]?></textarea>
                        <?endif;?>
                    </div>
                <?
                    break;
                    default:
                ?>
                <input type="text"
                       class="form-control"
                       id="<?=$arParams["PREFIX"]?>-<?=$code?>"
                       name="<?=$arParams["PREFIX"]?>-<?=$code?>"
                       value="<?=$arResult["USER_DATA"][$code]?>">
                <?}?>
            </div>
        </div>
    <?endforeach;?>

    <div class="text-center">
        <input type="submit"
               class="btn btn-danger px-5 py-3"
               value="<?=Loc::getMessage("REQISITS_SUBMIT")?>">
    </div>

</form>