<?
/**
 * shop_user_message
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "shop_user_message",
	"DESCRIPTION" => "shop_user_message",
	"ICON" => "/images/icon.gif",
	"PATH" => array(
		"ID" => "studio7sbp",
		"CHILD" => array(
			"ID" => "shop_user_message",
			"NAME" => "shop_user_message"
		)
	),
);