<?
use Bitrix\Main\Application,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Type\DateTime;


class shopUserMessage extends CBitrixComponent
{

    private $_request;

    public function sendLetter($arFields){
        $arFields["MAIL_TO"] = "artem@koorochka.com";
        CEvent::Send("S7SBP_MARKETPLACE_SHOP_MESSAGE", SITE_ID, $arFields);
    }

    public function saveMessage($arFields){
        // save to db
        // TODO validate

        // Seve to db
        \Studio7spb\Marketplace\ShopMessagesTable::add(array(
            "DATETIME" => new DateTime(),
            "SORT" => 500,
            "ACTIVE" => "Y",
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
            "ELEMENT_ID" => $this->arParams["ELEMENT_ID"],
            "USER_ID" => $this->arResult["USER"]["ID"],
            "USER_MAIL" => $this->arResult["USER"]["MAIL"],
            "USER_MESSAGE" => $this->arResult["USER"]["MESSAGE"],
            "PARAMS" => null
        ));

        // Send bitrix event
        $this->sendLetter($arFields);
    }

    public function executeComponent()
    {
        Loc::loadLanguageFile(__FILE__);
        global $USER;
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arResult["USER"] = array(
            "MAIL" => null,
            "MESSAGE" => null,
            "AUTH" => "N"
        );

        if($USER->IsAuthorized()){
            $this->arResult["USER"]["AUTH"] = "Y";
        }

        if($request->isPost()){

            if($request->getPost("show")){
                $this->arResult["SHOW_FORM"] = "Y";
            }else{

                $this->arResult["USER"]["MAIL"] = $request->getPost("mail");
                $this->arResult["USER"]["MESSAGE"] = $request->getPost("message");

                // guest variant
                if($this->arResult["USER"]["AUTH"] == "N"){
                    if(empty($this->arResult["USER"]["MAIL"])){
                        $this->arResult["ERRORS"][] = Loc::getMessage("SHOP_USER_MAIL_EMPTY");
                    }
                    elseif(!check_email($this->arResult["USER"]["MAIL"])){
                        $this->arResult["ERRORS"][] = Loc::getMessage("SHOP_USER_MAIL_NOTVALID");
                    }
                }else{
                    $this->arResult["USER"]["MAIL"] = $USER->GetEmail();
                    $this->arResult["USER"]["ID"] = $USER->GetID();
                }



                if(empty($this->arResult["USER"]["MESSAGE"])){
                    $this->arResult["ERRORS"][] = Loc::getMessage("SHOP_USER_MESSAGE_EMPTY");
                }

            }

            if($request->getPost("send")){
                if(empty($this->arResult["ERRORS"])){
                    $this->arResult["MESSAGE"] = Loc::getMessage("SHOP_USER_MESSAGE_SUCCESS");
                    $this->saveMessage($this->arResult["USER"]);
                }else{
                    $this->arResult["SHOW_FORM"] = "Y";
                }
            }

        }

        $this->includeComponentTemplate();
    }
}