<?
$MESS = array(
    "SHOP_USER_MAIL_EMPTY" => "Не заполнено поле E-mail",
    "SHOP_USER_MAIL_NOTVALID" => "Поле E-mail заполнено некоректно",
    "SHOP_USER_MESSAGE_EMPTY" => "Не заполнено поле Сообщение",
    "SHOP_USER_MESSAGE_SUCCESS" => "Сообщение отправлено!"
);