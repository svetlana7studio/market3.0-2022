<?
/**
 * @var array $arParams
 * @var array $arResult
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>

<form class="techsupport"
             action="<?=$arParams["CURRENT_PAGE"]?>"
             method="post">
    <div class="techsupport_container">
        <svg class="techsupport_svg">
            <image width="26"
                   height="24"
                   data-name="002-mail"
                   xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAYCAQAAABOiyVOAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfjDBYXAStASFs6AAAB8klEQVQ4y52TTUiUURSGn28cU9toJSQSElQEQYIKBYFQgv3TomWLFtUignBEIggJhNy56G/TpkVF0KJFi0AIJLKE6EeTqCBKCXHExL5iKGWmeVrM+PfNKNV5N+eee+6957z3PUhRbLbPPjcV3y0WDDxjyhFHTHnaoDAjkLztZmPeO0YrE3QDnaynl3v5+CiPc87CoW9UsbKFrMk5sflQQDvBCmgnmEuN8R8WB6CKq1Tm1ydYHcn5yU0yQCU36CAFiHsd84dpE2KNScMIktaICTOm/GyzYI/aZ52hiWX+LIeEoVsc8LddMU4Br5n8q2bGGSZGWwx4wVleUQ5ALemCR9LUAlDBEMd5miPiLie5TRlNxElyhLLI/bMkKWMHq5jmMAfYTr6XUi+Z8aXbivZT75BpO43nemMRATv94IwdxsRyr3jdCrHE88761oYFQljCWoWXzdpvi4PO+Mth9/nMrD2WL2aRAqr3+FX9ZIP1flSnbI5SH5VRnINU84AmBhmmifus4yilkawlL9X6xIznFs1QYLtpB9ywXHktTjgeKQZxl2NO2lpYXsAFHvGORvoLdDBAI2/o5eL8TBiacK0P1W5LllVeiV1m7bV6rrxbjjrtoRXlirjfKb94xzAwpJIs1xj9C8HW0UaM74HP2fqPg/v+D3Fo1Pc1cnEHAAAAAElFTkSuQmCC"></image>
        </svg>
    </div>

    <?if(!empty($arResult["ERRORS"])):?>
        <div class="alert alert-danger"><?=implode("<br>", $arResult["ERRORS"])?></div>
    <?endif;?>

    <?if(!empty($arResult["MESSAGE"])):?>
        <div class="alert alert-success text-center"><?=$arResult["MESSAGE"]?></div>
    <?endif;?>

    <?if($arResult["SHOW_FORM"] == "Y"):?>

        <?=bitrix_sessid_post()?>

        <?if($arResult["USER"]["AUTH"] == "N"):?>
            <label><?=Loc::getMessage("SHOP_USER_MESSAGE_MAIL")?></label>
            <input type="text"
                   value="<?=$arResult["USER"]["MAIL"]?>"
                   name="mail" />
        <?endif;?>

        <label><?=Loc::getMessage("SHOP_USER_MESSAGE")?></label>

        <textarea name="message"
                  rows="5"><?=$arResult["USER"]["MESSAGE"]?></textarea>

        <button type="submit"
                name="send"
                value="Y"
                class="techsupport_btn mt-2"><?=Loc::getMessage("SHOP_USER_MESSAGE_SEND")?></button>

    <?else:?>

        <button type="submit"
                name="show"
                value="Y"
                class="techsupport_btn"><?=Loc::getMessage("SHOP_USER_MESSAGE_SHOW")?></button>

    <?endif;?>

</form>