<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetPageProperty('title', Loc::getMessage("SPS_TITLE_PRODUCT_ADD"));
include("left_menu.php");
?>
<?$APPLICATION->IncludeComponent(
	"studio7sbp:seller.personal.product.add", 
	"", 
	array(
		"SEF_FOLDER" => $arParams["SEF_FOLDER"],
		"IBLOCK_ID" => $arParams["catalog_iblock_id"],
		"ELEMENT_ID" => $arResult["VARIABLES"]["ID"]
	),
	false
);?>