<?
/**
 * @var CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

use Studio7spb\Marketplace\ImportSettingsTable,
    Bitrix\Iblock\SectionTable,
    Bitrix\Iblock\PropertyTable,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Studio7spb\Marketplace\Import\ImportProfileListTable;

Loc::loadLanguageFile(__FILE__);

$iblockID = $arResult["VARIABLES"]["ID"];
$iblockID = (int) $iblockID;
if($iblockID <= 0)
    die;

$arFields = [];
$start = 0;
$startData = 1;
$properties = [];
$listProperties = [];

Loader::registerAutoLoadClasses("studio7spb.marketplace", [
    "\\Studio7spb\\Marketplace\\Import\\ImportProfileListTable" => "lib/import/importprofilelisttable.php"
]);

$profile = [
    "filter" => [
        "IBLOCK_ID" => $arParams["catalog_iblock_id"],
        "ACTIVE" => "Y",
        ">CUR" => 0
    ]
];

$profile = ImportProfileListTable::getList($profile);
if($profile = $profile->fetch())
{

    //d($profile["START"]);

    $profile["FIELDS"] = unserialize($profile["FIELDS"]);
    $profile["SAMPLE"] = unserialize($profile["SAMPLE"]);
    $arFields = $profile["SAMPLE"];



    foreach ($profile["FIELDS"] as $letter=>$fields)
        foreach ($fields as $field)
            if (!empty($field) && substr( $field, 0, 7 ) === "IP_PROP")
                $properties[$letter] = substr($field, 7);

    $values = PropertyTable::getList([
        "filter" => [
            "IBLOCK_ID" => $iblockID,
            "ID" => $properties,
            //"PROPERTY_TYPE" => "L",
            "ACTIVE" => "Y",
        ],
        "select" => ["ID", "NAME", "CODE", "IBLOCK_ID", "PROPERTY_TYPE", "DEFAULT_VALUE", "LINK_IBLOCK_ID", "IS_REQUIRED"]
    ]);
    while ($value = $values->fetch()){
        foreach ($properties as $letter=>$id){
            if($id == $value["ID"]){

                switch ($value["PROPERTY_TYPE"]){
                    case "L":
                        $property_enums = CIBlockPropertyEnum::GetList([], [
                            "IBLOCK_ID" => $value["IBLOCK_ID"],
                            "PROPERTY_ID" => $value["ID"],
                        ]);
                        while ($enum_fields = $property_enums->Fetch()) {
                            $value["VALUES"][] = $enum_fields["VALUE"];
                        }

                        $listProperties[$letter] = $value;
                        break;
                    case "E":

                        if($value["CODE"] === "H_COMPANY"){
                            $properties[$letter] = $value;
                        }
                        else{
                            $value["LETTER"] = $letter;
                            $IBprops[] = $value;
                        }

                        break;
                }

                $properties[$letter] = $value;

            }
        }
    }
}

// <editor-fold defaultstate="collapsed" desc=" # pack to excell">
$APPLICATION->RestartBuffer();

if (ini_get('mbstring.func_overload') & 2) {
    ini_set("mbstring.func_overload", 0);
}

#header("Content-Type: application/vnd.ms-excel");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: filename=" . str_replace(" ", "_", Loc::getMessage("PRODUCT_SAMPLE_FILE_NAME", ["NAME" => $profile["NAME"]])) . ".xlsx");  // Excel5 = .xls  Excel2007 = .xlsx
header('Cache-Control: max-age=0'); //no cache
require $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';
$objPHPExcel = new PHPExcel();

$startRow = 1 + $start;

AddMessage2Log($profile);


$objPHPExcel->getActiveSheet()->setTitle("Образец"); // $iblock["NAME"]

$objPHPExcel->getActiveSheet()->getStyle($startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension($startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->getStyle($startRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle($startRow++)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FBD4B4');

$objPHPExcel->getActiveSheet()->getStyle($startRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getRowDimension($startRow++)->setRowHeight(30);

$objPHPExcel->getActiveSheet()->getStyle($startRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getRowDimension($startRow)->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle($startRow)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);

foreach ($arFields as $char => $arItems){
    foreach ($arItems as $n=>$value){
        $n = $n + $start;
        if($value == "null")
            $value = "";
        switch ($n){
            case 1:

                break;
        }

        if($n == $startData){

            switch ($properties[$char]["IS_REQUIRED"]){
                case "Y":
                    $value = Loc::getMessage("PRODUCT_SAMPLE_IS_REQUIRED");
                    break;
                case "N":
                    $value = "";
                    break;
            }

        }

        $objPHPExcel->getActiveSheet()->getColumnDimension($char)->setWidth(24);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit($char . $n, $value, PHPExcel_Cell_DataType::TYPE_STRING);

    }
}
$endDataRow = $n;
/* Create a new worksheet, after the default sheet*/
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);

$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(14);
$startRow = 1;

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFont()->setSize(24);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('eeece2');
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Как работать с шаблоном');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9d9d9');
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Скачивайте шаблоны в личном кабинете');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Перед каждой загрузкой скачивайте новый шаблон в личном кабинете. Мы регулярно обновляем шаблоны,  чтобы они были максимально точными и удобными.');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9d9d9');
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Не меняйте структуру файла');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Не добавляйте и не удаляйте листы и столбцы. Не меняйте порядок столбцов и не скрывайте их.');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9d9d9');
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Правильно заполняйте строки');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Для каждого артикула используйте отдельную строку. Например:');

$objPHPExcel->getActiveSheet()->getStyle($startRow)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow, 'Артикул');
$objPHPExcel->getActiveSheet()->setCellValue('B' . $startRow, 'Название');
$objPHPExcel->getActiveSheet()->setCellValue('C' . $startRow++, 'Размер');

$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow, 'R17');
$objPHPExcel->getActiveSheet()->setCellValue('B' . $startRow, 'Кольцо «Романтика»');
$objPHPExcel->getActiveSheet()->setCellValue('C' . $startRow++, '17');

$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow, 'R18');
$objPHPExcel->getActiveSheet()->setCellValue('B' . $startRow, 'Кольцо «Романтика»');
$objPHPExcel->getActiveSheet()->setCellValue('C' . $startRow++, '18');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9d9d9');
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Заполните нужные поля');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Чтобы заполнить поле "Код раздела из классификатора" воспользуйтесь вкладкой "Классификатор" и выберите раздел в который вы загружаете товары. Для полей "Новинка", "Рекомендуем", "Хит продаж" нужно заполнять одно из значений 0 или 1, 0-элемент каталога не имеет данной метки, 1- имеет.');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('d9d9d9');
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Не меняйте тип значений');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'В шаблонах есть поля нескольких видов, не меняйте их и заполняйте правильно:');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, '• Выбор из списка');

$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Когда вы нажмете на ячейку, рядом с ней появится значок стрелки и подсказка «Выберите значение из списка». Если значение не помещается полностью в видимую область - растяните столбец по ширине.');

$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Нажмите на стрелку и выберите подходящее значение. Или впишите значение вручную.  Если значение не будет соответствовать допустимому значению из списка это выдаст ошибку. Примеры полей: Тип АКБ.');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Чтобы выбрать несколько значений из списка:');

$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, '    1. Нажмите на стрелку и посмотрите доступные значения. Например, в поле Цвет есть значения: белый, голубой, зеленый.');

$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, '    2. В любой свободной ячейке запишите все нужные значения через точку с запятой. Значения нужно записать так же, как они записаны в выпадающем списке. Например: белый;голубой;зеленый.');

$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, '    3. Скопируйте строку со значениями и вставьте ее в поле параметра.');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, '• Число');

$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Когда вы нажмете на ячейку, рядом с ней появится подсказка «Введите значение от 0 до 9999».');

$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Или эта информация может быть в подсказке над полем. В такие поля вписывайте только числа, без единиц измерения.');

$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'Примеры полей: Вес, Ширина, Цена.');

$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, '• Текст');

$objPHPExcel->getActiveSheet()->getStyle('A' . $startRow)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getRowDimension('A' . $startRow)->setRowHeight(-1);
$objPHPExcel->getActiveSheet()->mergeCells('A' . $startRow . ':G' . $startRow);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow++, 'В таких полях пишите информацию, которая нужна для параметров. Примеры полей: Название товара, Бренд, Подробное описание.');


/* Rename 2nd sheet*/
$objPHPExcel->getActiveSheet()->setTitle('Как работать с шаблоном');

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(2);

$objPHPExcel->getActiveSheet()->setTitle('Классификатор');
$startRow = 1;

$objPHPExcel->getActiveSheet()->getStyle($startRow)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getRowDimension($startRow)->setRowHeight(40);
$objPHPExcel->getActiveSheet()->getStyle($startRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->setCellValue('A' . $startRow, 'ID раздела');
$objPHPExcel->getActiveSheet()->setCellValue('B' . $startRow, 'Название');
$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(76);

$sections = SectionTable::getList([
    "order" => ["NAME" => "ASC"],
    "filter" => [
        "IBLOCK_ID" => $iblockID
    ],
    "select" => [
        "ID",
        "NAME"
    ]
]);



while ($section = $sections->fetch())
{

    $objPHPExcel->getActiveSheet()->setCellValue('A' . ++$startRow, $section["ID"]);
    $objPHPExcel->getActiveSheet()->setCellValue('B' . $startRow, $section["NAME"]);
}

$objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()->calculateWorksheetDimension());
$objPHPExcel->setActiveSheetIndex(0);

if(!empty($IBprops)) {
    foreach ($IBprops as $key => $IBprop) {

        $values = \Bitrix\Iblock\ElementTable::getList([
            "order" => ["NAME" => "ASC"],
            "filter" => [
                "IBLOCK_ID" => $IBprop["LINK_IBLOCK_ID"],
                "ACTIVE" => "Y"
            ],
            "select" => ["NAME"]
        ]);

        $arNames = [];
        while ($value = $values->fetch())
            $arNames[] = $value["NAME"];

        if(!empty($arNames)){
            $endDataRow += 1000;
            for($i=$startDataRow; $i <= $endDataRow; $i++){

                $objPHPExcel->getActiveSheet()->getCell($IBprop["LETTER"] . $i)->getDataValidation()
                    ->setType(PHPExcel_Cell_DataValidation::TYPE_LIST)
                    ->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION)
                    ->setShowDropDown(true)
                    ->setFormula1('"' . implode(",", $arNames) . '"');

            }
        }

    }
}

if(!empty($listProperties))
{
    foreach ($listProperties as $char=>$value){
        if(!empty($value["VALUES"])){
            $endDataRow += 1000;
            for($i=$startDataRow; $i <= $endDataRow; $i++){
                $objPHPExcel->getActiveSheet()->getCell($char . $i)->getDataValidation()
                    ->setType(PHPExcel_Cell_DataValidation::TYPE_LIST)
                    ->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION)
                    ->setShowDropDown(true)
                    ->setFormula1('"' . implode(",", $value["VALUES"]) . '"');
            }
        }
    }
}


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
#$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
// </editor-fold>

unset($value);
die;