<?
$MESS["SPOL_TPL_EMPTY_ORDER_LIST"] = 'У вас пока нет заказов <a href="/catalog/">Перейти к подбору</a>';
$MESS["SPOL_TPL_EMPTY_HISTORY_ORDER_LIST"] = "История заказов отсутствует";
$MESS["SPOL_TPL_EMPTY_CANCELED_ORDER"] = "Отмененных заказов нет";
$MESS["SPOL_TPL_LINK_TO_CATALOG"] = "Перейти в каталог";
$MESS["SPOL_TPL_VIEW_ALL_ORDERS"] = "Посмотреть все заказы";
$MESS["SPOL_TPL_VIEW_ORDERS_HISTORY"] = "Посмотреть историю заказов";
$MESS["SPOL_TPL_VIEW_ORDERS_CANCELED"] = "Посмотреть историю отмененных заказов";
$MESS["SPOL_TPL_ORDERS_CANCELED_HEADER"] = "Отмененные заказы";
$MESS["SPOL_TPL_CUR_ORDERS"] = "Посмотреть текущие заказы";
$MESS["SPOL_TPL_ORDER_IN_STATUS"] = "Заказы в статусе";
$MESS["SPOL_TPL_ORDER_IN_STATUSES"] = "Заказы в статусе";
$MESS["SPOL_TPL_ORDER_DATE_INSERT"] = "Время заказа";
$MESS["SPOL_TPL_LEFT_QUOTE"] = "&laquo;";
$MESS["SPOL_TPL_RIGHT_QUOTE"] = "&raquo;";
$MESS["SPOL_TPL_ORDER"] = "Заказ";
$MESS["SPOL_TPL_ORDER_TITLE"] = "Мои заказы";
$MESS["SPOL_TPL_STATUS_ALL"] = "Все заказы";
$MESS["SPOL_TPL_NUMBER_SIGN"] = "№";
$MESS["SPOL_TPL_FROM_DATE"] = "от";
$MESS["SPOL_TPL_GOOD"] = "товар";
$MESS["SPOL_TPL_GOODS"] = "товаров";
$MESS["SPOL_TPL_TWO_GOODS"] = "товара";
$MESS["SPOL_TPL_SUMOF"] = "на сумму";
$MESS["SPOL_TPL_PAYMENT"] = "Оплата";
$MESS["SPOL_TPL_BILL"] = "Счет";
$MESS["SPOL_TPL_NOTPAID"] = "Не оплачено";
$MESS["SPOL_TPL_RESTRICTED_PAID"] = "На проверке менеджером";
$MESS["SOPL_TPL_RESTRICTED_PAID_MESSAGE"] = "<b>Обратите внимание:</b> оплата заказа будет доступна после подтверждения менеджером";
$MESS["SPOL_TPL_PAID"] = "Оплачено";
$MESS["SPOL_TPL_SUM_TO_PAID"] = "Сумма к оплате по счету";
$MESS["SPOL_TPL_CHECK_POSTID"] = "проверить идентификатор отправления";
$MESS["SPOL_TPL_MORE_ON_ORDER"] = "Подробнее о заказе";
$MESS["SPOL_TPL_REPEAT_ORDER"] = "Повторить заказ";
$MESS["SPOL_TPL_CANCEL_ORDER"] = "Отменить заказ";
$MESS["SPOL_TPL_POSTID"] = "Идентификатор отправления";
$MESS["SPOL_TPL_DELIVERY"] = "Доставка";
$MESS["SPOL_TPL_PAY"] = "Оплатить сейчас";
$MESS["SPOL_TPL_CHANGE_PAY_TYPE"] = "сменить способ оплаты";
$MESS["SPOL_TPL_LOAD"] = "Отгрузка";
$MESS["SPOL_TPL_DELIVERY_COST"] = "стоимость доставки";
$MESS["SPOL_TPL_DELIVERY_SERVICE"] = "Служба доставки";
$MESS["SPOL_TPL_ORDER_FINISHED"] = "Заказ выполнен";
$MESS["SPOL_TPL_ORDER_CANCELED"] = "Заказ отменен";
$MESS["SPOL_TPL_LOADED"] = "Отгружено";
$MESS["SPOL_TPL_NOTLOADED"] = "Не отгружено";
$MESS["SPOL_CANCEL_PAYMENT"] = "назад";
$MESS["SPOL_ORDER_SHIPMENT_STATUS"] = "Статус отгрузки";
$MESS["SPOL_CHECK_TITLE"] = "Список чеков";
$MESS["SPOL_CHECK_NUM"] = "Чек №#CHECK_NUMBER#";

/**
 * Seach
 */
$MESS["SPOL_TPL_ORDER_SEARCH"] = "Поиск заказа:";
$MESS["SPOL_TPL_ORDER_NUMBER"] = "Номер заказа";
$MESS["SPOL_TPL_BASKET_ITEM_NAME"] = "Ваши заказы";
$MESS["SPOL_TPL_BASKET_ITEM_SUBMIT"] = "Искать";
$MESS["SPOL_TPL_BASKET_ITEM_CLEAR"] = "Сбросить";
/**
 * Order List
 */
$MESS["SPOL_TPL_ORDER_LIST_ITEMS"] = "Наименования товаров";
$MESS["SPOL_TPL_ORDER_LIST_ITEM"] = "Ваши заказы";
$MESS["SPOL_TPL_ORDER_LIST_STORE"] = "Магазин";
$MESS["SPOL_TPL_ORDER_LIST_STATUS"] = "Статус заказа";
$MESS["SPOL_TPL_ORDER_LIST_ACTION"] = "Цена";
?>