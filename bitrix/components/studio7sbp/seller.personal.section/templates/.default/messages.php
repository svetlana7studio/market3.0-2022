<?
/**
 * https://dev.1c-bitrix.ru/api_d7/bitrix/main/systemcomponents/gridandfilter/mainuigrid.php
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @var array $arResult
 * @var array $arParams
 * @var CBitrixComponentTemplate $this
 */

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Grid\Options as GridOptions,
    \Bitrix\Main\UI\PageNavigation,
    \Bitrix\Main\Application,
    Studio7spb\Marketplace\CMarketplaceSeller,
    \Studio7spb\Marketplace\VendorMessageTable;

function personLink($userID)
{
    $link = "";
    if($userID > 0){

    }else{
        $link = Loc::getMessage("SELLER_USER_GUEST");
    }
    return $link;
}

$request = Application::getInstance()->getContext()->getRequest();
$this->addExternalJs("/bitrix/js/sender/page/script.js");
include("left_menu.php");
Loc::loadLanguageFile(__FILE__);
Loader::registerAutoLoadClasses(null, [
    "\Studio7spb\Marketplace\VendorMessageTable" => "/bitrix/modules/studio7spb.marketplace/lib/market2/vendormessagetable.php"
]);

switch($request->get("op")){
    case "delete":
        $id = intval($request->get("id"));
        if($id > 0){
            VendorMessageTable::delete($id);
        }
        LocalRedirect($arResult["CURRENT_URL"]);
        break;
    case "activate":
        $id = intval($request->get("id"));
        if($id > 0){
            VendorMessageTable::update($id, ["ACTIVE" => "Y"]);
        }
        LocalRedirect($arResult["CURRENT_URL"]);
        break;
    case "deactivate":
        $id = intval($request->get("id"));
        if($id > 0){
            VendorMessageTable::update($id, ["ACTIVE" => "N"]);
        }
        LocalRedirect($arResult["CURRENT_URL"]);
        break;
}


$arResult = [
    "GRID_ID" => VendorMessageTable::getTableName(),
    "COLUMNS" => [],
    "ROWS" => [],
    "OPTIONS" => null,
    "ORDER" => [],
    "ACTIONS" => [
        "VIEW" => $arResult["PATH_TO_MESSAGES_VIEW"],
        "DELETE" => $arResult["PATH_TO_MESSAGES"],
        "ACTIVATE" => $arResult["PATH_TO_MESSAGES"],
        "DEACTIVATE" => $arResult["PATH_TO_MESSAGES"]
    ],
    "COMPANY" => CMarketplaceSeller::getCompanyByUser(),
    "NAV_PARAMS" => [],
    "NAV" => [],
    "NAV_COUNT" => [],
];

# filter for all grid
$filterOption = new Bitrix\Main\UI\Filter\Options($arResult["GRID_ID"]);
$filterFields = $filterOption->getFilter();
$filter = ["VENDOR_ID" => $arResult["COMPANY"]["ID"]];

$arResult["OPTIONS"] = new GridOptions($arResult["GRID_ID"]);
$arResult["ORDER"] = $arResult["OPTIONS"]->GetSorting(['sort' => ['ID' => 'DESC'], 'vars' => ['by' => 'by', 'order' => 'order']]);

$arResult["NAV_PARAMS"] = $arResult["OPTIONS"]->GetNavParams();
$arResult["NAV"] = new PageNavigation($arResult["GRID_ID"]);
$arResult["NAV"]->allowAllRecords(true)
    ->setPageSize($arResult["NAV_PARAMS"]['nPageSize'])
    ->initFromUri();


# filter with ui fulter
foreach ($filterFields as $key => $value)
{
    switch ($key){
        // integer
        case "ID_from":
            $filter[">=ID"] = $value;
            break;
        case "ID_to":
            $filter["<=ID"] = $value;
            break;
        case "SORT_from":
            $filter[">=ELEMENT_ID"] = $value;
            break;
        case "SORT_ID_to":
            $filter["<=ELEMENT_ID"] = $value;
            break;
        case "USER_CREATE_from":
            $filter[">=USER_ID"] = $value;
            break;
        case "USER_CREATE_to":
            $filter["<=USER_ID"] = $value;
            break;
        case "USER_UPDATE_from":
            $filter[">=USER_ID"] = $value;
            break;
        case "USER_UPDATE_to":
            $filter["<=USER_ID"] = $value;
            break;
        // date
        case "DATE_CREATE_from":
            $filter[">=DATE_CREATE"] = $value;
            break;
        case "DATE_CREATE_to":
            $filter["<=DATE_CREATE"] = $value;
            break;
        case "DATE_UPDATE_from":
            $filter[">=DATE_CREATE"] = $value;
            break;
        case "DATE_UPDATE_to":
            $filter["<=DATE_CREATE"] = $value;
            break;
        // enum
        case "ACTIVE":
            $filter["=ACTIVE"] = $value;
            break;
        // string
        case "TEXT":
            $filter["%TEXT"] = $value;
            break;
        case "NAME":
            $filter["%NAME"] = $value;
            break;
        case "EMAIL":
            $filter["%EMAIL"] = $value;
            break;
    }
}

/**
 * @var Bitrix\Main\ORM\Fields\IntegerField [] $values
 */
$values = VendorMessageTable::getMap();
$visible = 0;
foreach ($values as $value)
{
    if(!in_array($value->getName(), ["VENDOR_ID", "USER_CREATE", "USER_UPDATE", "CATEGORY"])){
        $visible++;
        switch ($value->getDataType()){
            case "integer":
                $ui_filter[] = [
                    "id" => $value->getName(),
                    "name" => $value->getTitle(),
                    "type" => "number",
                    "default" => ($visible > 5) ? false : true
                ];
                break;
            default:
                $ui_filter[] = [
                    "id" => $value->getName(),
                    "name" => $value->getTitle(),
                    "type" => "text",
                    "default" => ($visible > 5) ? false : true
                ];
        }

        $arResult["COLUMNS"][] = [
            "id" => $value->getName(),
            "name" => $value->getTitle(),
            "sort" => $value->getName(),
            "default" => true
        ];

    }


}

ob_start();
$APPLICATION->IncludeComponent('bitrix:main.ui.filter', '', [
    'FILTER_ID' => $arResult["GRID_ID"],
    'GRID_ID' => $arResult["GRID_ID"],
    'FILTER' => $ui_filter,
    'ENABLE_LIVE_SEARCH' => true,
    'ENABLE_LABEL' => true
]);
$filterLayout = ob_get_clean();

$values = VendorMessageTable::getList(['filter' => $filter, 'select' => ["ID"]]);
$arResult["NAV_COUNT"] = $values->getSelectedRowsCount();
$arResult["NAV"]->setRecordCount($arResult["NAV_COUNT"]);

$values = VendorMessageTable::getList([
    'filter' => [], // $filter
    'offset' => $arResult["NAV"]->getOffset(),
    'limit'  => $arResult["NAV"]->getLimit(),
    'order'  => $arResult["ORDER"]["sort"]
]);
$data = [];

while ($value = $values->fetch())
{

    foreach ($arResult["COLUMNS"] as $column){

        switch ($column["id"]){
            case "ACTIVE":
                $data[$column["id"]] = Loc::getMessage("SELLER_MESSAGES_" . $value[$column["id"]]);
                break;
            case "EMAIL":
                $data[$column["id"]] = '<a href="mailto:' . $value[$column["id"]] . '">' . $value[$column["id"]] . "</a>";
                break;
            case "ID":
                $data[$column["id"]] = '<strong>' . $value[$column["id"]] . "</strong>";
                break;
            case "CATEGORY":
                $data[$column["id"]] = Loc::getMessage("SELLER_CATEGORY_" . ToUpper($value[$column["id"]]));
                break;
            default:
                $data[$column["id"]] = $value[$column["id"]];
        }

    }

    $arResult["ROWS"][] = [
        'data' => $data,
        'actions' => [
            [
                'text'    => Loc::getMessage("SELLER_ACTION_VIEW"),
                'default' => true,
                //'onclick' => str_replace("#ID#", $value['ID'], $arResult["ACTIONS"]["VIEW"])
                'onclick' => "BX.Sender.Page.open('".CUtil::JSEscape(str_replace("#ID#", $value['ID'], $arResult["ACTIONS"]["VIEW"]))."')"
            ],
            [
                'text'    => Loc::getMessage("SELLER_ACTION_VIEW_Y"),
                'default' => true,
                'onclick' => "document.location.href='" . $arResult["ACTIONS"]["ACTIVATE"] . "?op=activate&id=" . $value['ID'] . "'"
            ],
            [
                'text'    => Loc::getMessage("SELLER_ACTION_VIEW_N"),
                'default' => true,
                'onclick' => "document.location.href='" . $arResult["ACTIONS"]["DEACTIVATE"] . "?op=deactivate&id=" . $value['ID'] . "'"
            ],
            [
                'text'    => Loc::getMessage("SELLER_MESSAGES_DELETE"),
                'default' => true,
                'onclick' => 'if(confirm("Точно?")){document.location.href="?op=delete&id='.$value['ID'].'"}'
            ]
        ]
    ];
}
unset($data, $value, $values);

$snippet = new \Bitrix\Main\Grid\Panel\Snippet();
$arParams = [
    "GRID" => [
        'GRID_ID' => $arResult["GRID_ID"],
        'COLUMNS' => $arResult["COLUMNS"],
        'ROWS' => $arResult["ROWS"],
        'AJAX_MODE'           => 'Y',
        'AJAX_OPTION_JUMP'    => 'N',
        'AJAX_OPTION_HISTORY' => 'N',
        'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
        "PAGE_SIZES" => [
            ['NAME' => "5", 'VALUE' => '5'],
            ['NAME' => '10', 'VALUE' => '10'],
            ['NAME' => '20', 'VALUE' => '20'],
            ['NAME' => '50', 'VALUE' => '50']
        ],
        'SHOW_ROW_CHECKBOXES'       => false,
        'SHOW_CHECK_ALL_CHECKBOXES' => false,
        'SHOW_ROW_ACTIONS_MENU'     => true,
        'SHOW_GRID_SETTINGS_MENU'   => true,
        'SHOW_SELECTED_COUNTER'     => true,
        'SHOW_TOTAL_COUNTER'        => true,
        'SHOW_PAGESIZE'             => true,
        'ALLOW_COLUMNS_SORT'        => true,
        'ALLOW_COLUMNS_RESIZE'      => true,
        'ALLOW_HORIZONTAL_SCROLL'   => true,
        'ALLOW_SORT'                => true,
        'ALLOW_PIN_HEADER'          => true,
        'SHOW_ACTION_PANEL'         => false,

        'SHOW_NAVIGATION_PANEL'     => true,
        'SHOW_PAGINATION'           => true,
        'NAV_OBJECT' => $arResult["NAV"],
        "TOTAL_ROWS_COUNT" => $arResult["NAV_COUNT"],
    ],
    "PANEL" => array('LIST' => [
        [
            'type' => 'filter',
            'content' => $filterLayout
        ]
    ])
];

$APPLICATION->IncludeComponent("bitrix:sender.ui.panel.title", "", $arParams["PANEL"]);
$APPLICATION->IncludeComponent("bitrix:main.ui.grid", "", $arParams["GRID"]);