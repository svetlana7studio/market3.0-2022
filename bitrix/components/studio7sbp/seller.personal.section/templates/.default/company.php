<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetPageProperty('title', Loc::getMessage("SPS_TITLE_PRODUCTS"));
include("left_menu.php");

$APPLICATION->IncludeComponent(
	"studio7sbp:seller.personal.company", 
	"", 
	array(
		"SEF_FOLDER" => $arParams["SEF_FOLDER"],
        "IBLOCK_ID" => 6,
        "PROPERTIES" => array(
            "COMP_INN",
            "KPP",
            "OKPO",
            "OGRN",
            "OKATO",
            "BUHGALTER",
            "DIRECTOR",
            "INN",
            "SCORE",
            "BANK",
            "CORE_SCORE",
            "BIK"
        ),
        "BANK" => array(
            "SCORE",
            "BANK",
            "CORE_SCORE",
            "BIK",
        )
	),
	false,
    array("HIDE_ICONS" => "Y")
);?>