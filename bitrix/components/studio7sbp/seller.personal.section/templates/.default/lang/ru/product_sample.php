<?
$MESS = [
    "PRODUCT_SAMPLE_FILE_NAME" => "Образец для категории NAME",
    "PRODUCT_SAMPLE_IS_REQUIRED" => "Обязательное поле",
    "PRODUCT_SAMPLE_INPUT_ERROR" => "Ошибка ввода",
    "PRODUCT_SAMPLE_VALUE_NOT_IN_LIST" => "Значения нет в списке.",
    "PRODUCT_SAMPLE_PICK_FROM_LIST" => "Выбрать из списка",
    "PRODUCT_SAMPLE_PLEASE" => "Выберите значение из раскрывающегося списка.",
];
