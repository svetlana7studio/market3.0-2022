<?
$MESS["LEFT_MENU_ITEM_SHOP"] = "Данные вендора";
$MESS["LEFT_MENU_ITEM_COMPANY"] = "Компания";
$MESS["LEFT_MENU_ITEM_PRODUCTS"] = "Мои товары";
$MESS["LEFT_MENU_ITEM_WAREHOUSES"] = "Склады";
$MESS["LEFT_MENU_ITEM_ORDERS"] = "Заказы";
$MESS["LEFT_MENU_ITEM_STATISTICS"] = "Статистика";
$MESS["LEFT_MENU_ITEM_MASTER_FILE"] = "Выгрузка мастер-файла";
$MESS["LEFT_MENU_ITEM_PAYMENTS"] = "Платежи";
$MESS["LEFT_MENU_ITEM_DOCS"] = "Документы";
$MESS["LEFT_MENU_ITEM_SHOPSALE"] = "Скидки магазина";
$MESS["LEFT_MENU_ITEM_MESSAGES"] = "Сообщения";
$MESS["LEFT_MENU_ITEM_PAYOPTIONS"] = "Платные опции";
$MESS["LEFT_MENU_ITEM_SETTINGS"] = "Настройка";
?>