<?
$MESS = [
    "SELLER_MESSAGES_TITLE" => "Сообщения от посетителей",
    "SELLER_MESSAGES_DELETE" => "Удалить",
    "SELLER_MESSAGES_Y" => "Да",
    "SELLER_MESSAGES_N" => "Нет",
    "SELLER_ACTION_VIEW_Y" => "Отметить как просмотрено",
    "SELLER_ACTION_VIEW_N" => "Отметить как не просмотрено",
    "SELLER_ACTION_VIEW" => "Просмотреть",
    "SELLER_CATEGORY_M" => "Мастер-класс",
    "SELLER_CATEGORY_W" => "Дизайнер",
    "SELLER_CATEGORY_SHOP" => "Магазин",
    "SELLER_USER_GUEST" => "Гость",
];