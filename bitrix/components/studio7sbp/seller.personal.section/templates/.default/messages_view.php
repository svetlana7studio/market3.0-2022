<?
/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @var array $arResult
 * @var array $arParams
 * @var CBitrixComponentTemplate $this
 */

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Application,
    \Studio7spb\Marketplace\CMarketplaceSeller,
    \Studio7spb\Marketplace\VendorMessageTable;

$request = Application::getInstance()->getContext()->getRequest();

if(empty($request->get("IFRAME")))
    include("left_menu.php");

Loc::loadLanguageFile(__FILE__);

$message = (int) $arResult["VARIABLES"]["ID"];
if($message <= 0)
    LocalRedirect($arResult["PATH_TO_MESSAGES"]);

if($request->get("IFRAME") == "Y"){
    $arParams = [
        "COMPANY" => CMarketplaceSeller::getCompanyByUser(),
        "ID" => $arResult["VARIABLES"]["ID"],
        "PATH_TO_MESSAGES_VIEW" => $arResult["PATH_TO_MESSAGES_VIEW"],
        "IFRAME" => "Y"
    ];
}else{
    $arParams = [
        "COMPANY" => CMarketplaceSeller::getCompanyByUser(),
        "ID" => $arResult["VARIABLES"]["ID"],
        "PATH_TO_MESSAGES_VIEW" => $arResult["PATH_TO_MESSAGES_VIEW"],
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "AJAX_OPTION_HISTORY" => "N",
    ];
}
?>

<section class="lk-page">
    <div class="container">
        <div class="catalog__inner">
            <?$APPLICATION->ShowViewContent("LEFT_MENU_ITEMS")?>
            <div class="catalog__content">
                <?if($arParams["IFRAME"] == "Y")
                {
                    $APPLICATION->RestartBuffer();
                    $APPLICATION->ShowCSS();
                }
                $this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
                ?>
                    <div class="container py-5 mx-2">
                        <h1 class="catalog__content-title"><?=Loc::getMessage("SELLER_MESSAGE_VIEW_TITLE", ["#ID#" => $arResult["VARIABLES"]["ID"]])?></h1>
                        <?$APPLICATION->IncludeComponent("studio7sbp:vendor.messages.view", "hobby", $arParams, false, ['HIDE_ICONS' => 'Y'])?>
                    </div>
                <?if($arParams["IFRAME"] == "Y")
                    die();
                ?>
            </div>
        </div>
    </div>
</section>
