<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Bitrix\Sale\Internals\OrderTable,
    Bitrix\Sale\Basket;

/*
if ($arParams['SHOW_ORDER_PAGE'] !== 'Y')
{
    LocalRedirect($arParams['SEF_FOLDER']);
}
*/

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
    $APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

include("left_menu.php");
Loader::includeModule("sale");
Loader::includeModule("iblock");
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_ORDERS"), $arResult['PATH_TO_ORDERS']);

$arResult = array(
    "MONTH" => [
      '01' => 'январь',
      '02' => 'февраль',
      '03' => 'март',
      '04' => 'апрель',
      '05' => 'май',
      '06' => 'июнь',
      '07' => 'июль',
      '08' => 'август',
      '09' => 'сентябрь',
      '10' => 'октябрь',
      '11' => 'ноябрь',
      '12' => 'декабрь'
    ],
    "ORDERS" => [],
    "COMPANY" => []
);

// <editor-fold defaultstate="collapsed" desc=" # Company">
$arResult["COMPANY"] = \Studio7spb\Marketplace\CMarketplaceSeller::getCompanyByUser();
// </editor-fold>

if($arResult["COMPANY"]["ID"] > 0){
    $arResult["COMPANY"]["PRODUCTS"] = array();
    $products = CIBlockElement::GetList(
        array(),
        array(
            "IBLOCK_ID" => 2,
            "PROPERTY_H_COMPANY" => $arResult["COMPANY"]["ID"]
        ),
        false,
        false,
        array(
            "ID"
        )
    );
    while ($product = $products->Fetch())
    {
        $arResult["COMPANY"]["PRODUCTS"][] = $product["ID"];
    }

}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" # Basket Items calculate order sum">
if($arResult["COMPANY"]["ID"] > 0 && !empty($arResult["COMPANY"]["PRODUCTS"])){

    $basketItems = Basket::getList(array(
        "filter" => array(
            "!ORDER_ID" => false,
            "PRODUCT_ID" => $arResult["COMPANY"]["PRODUCTS"]
        ),
        "select" => array("ID", "NAME", "PRICE", "ORDER_ID")
    ));
    while ($basketItem = $basketItems->fetch()){
        $arResult["COMPANY"]["ITEMS"][] = $basketItem;
        $arResult["ORDERS"][] = $basketItem["ORDER_ID"];
    }
}

//d($arResult["COMPANY"]["PRODUCTS"]);
//d($arResult["COMPANY"]["ITEMS"]);

// <editor-fold defaultstate="collapsed" desc=" # Order">
$orders = OrderTable::getList(array(
    "order" => ["ID" => "desc"],
    "filter" => array(
        ">PRICE" => 0,
        "ID" => $arResult["ORDERS"],
        "STATUS_ID" => ["OP", "F"] // OP => В пути, оплачен F => // Заказ доставлен и оплачен
     )
));
$arResult["ORDERS"] = array();
while ($order = $orders->fetch()){
    $order["DATE_INSERT"] = $arResult["MONTH"][$order["DATE_INSERT"]->format("m")] . " " . $order["DATE_INSERT"]->format("Y");
    $arResult["ORDERS"][$order["DATE_INSERT"]][] = $order;
}
// </editor-fold>
?>


<div class="s7sbp--marketplace--saler--lk--right--inner">

    <?if(empty($arResult["ORDERS"])):?>
        <div class="alert alert-danger text-center">У вас ещё нет данных для вывода.</div>
    <?else:?>
        <?foreach ($arResult["ORDERS"] as $date=>$orders):?>

            <table class="table mb-5">
                <thead class="bg-warning">
                <tr>
                    <th><?=$date?></th>
                    <th>Сумма заказа, руб.</th>
                    <th>Вознаграждение маркетплейса, руб.</th>
                    <th>Сумма за товар к выплате, руб.</th>
                </tr>
                </thead>
                <tbody>

                    <?
                    $sum = 0;
                    $sumComission = 0;
                    $sumResult = 0;
                    foreach ($orders as $order):

                        if(is_numeric($order["PRICE"]) && $order["PRICE"] > 0){
                            $comission = $order["PRICE"] * $arResult["COMPANY"]["PROPERTY_COMP_COMMISSION_VALUE"] / 100;
                        }else{
                            $comission = 0;
                        }

                        $result = $order["PRICE"] - $comission;

                        $sum += $order["PRICE"];
                        $sumComission += $order["PRICE"] * $arResult["COMPANY"]["PROPERTY_COMP_COMMISSION_VALUE"] / 100;
                        $sumResult += $order["PRICE"] - $comission;
                    ?>
                        <tr class="text-center">
                            <td class="text-success">
                                <?
                                echo "Заказ №";
                                echo $arResult["COMPANY"]["ID"];
                                echo str_pad($order["ACCOUNT_NUMBER"], 4, "0", STR_PAD_LEFT);
                                ?>
                            </td>
                            <td>
                                <?
                                echo \CCurrencyLang::CurrencyFormat($order["PRICE"], "RUB", false);
                                ?>
                            </td>
                            <td>
                                <?

                                echo \CCurrencyLang::CurrencyFormat($comission, "RUB", false);
                                ?>
                            </td>
                            <th><?

                                echo \CCurrencyLang::CurrencyFormat($result, "RUB", false);
                                ?></th>
                        </tr>
                    <?endforeach;?>

                </tbody>
                <tbody class="text-center bg-light">
                    <th>Итог за месяц</th>
                    <th><?=\CCurrencyLang::CurrencyFormat($sum, "RUB", false)?></th>
                    <th><?=\CCurrencyLang::CurrencyFormat($sumComission, "RUB", false)?></th>
                    <th><?=\CCurrencyLang::CurrencyFormat($sumResult, "RUB", false)?></th>
                </tbody>
            </table>

        <?endforeach;?>
    <?endif;?>
</div>