window.salePersonalSectionLoader = null;
window.salePersonalSectionLoaderContainer = null;

BX.ready(function () {
    window.salePersonalSectionLoaderContainer = BX.create('div',{
        attrs:{className:'product-add-loading'}
    });

    window.salePersonalSectionLoader = new BX.Loader({size: 130, color: "#663af2", mode: "custom"});
});