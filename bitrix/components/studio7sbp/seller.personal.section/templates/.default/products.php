<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetPageProperty('title', Loc::getMessage("SPS_TITLE_PRODUCTS"));
include("left_menu.php");

/**
 * И после регистрации (до подтверждения) они должны быть без возможности добавлять товар.
 * Активация (подтверждение) проводится менеджером маркетплейса в админке - соответственно,
 * при регистрации надо им ставить статус "На модерации", а менеджер в селекте выставляет "Верифицирован
 */
$company = CIBlockElement::GetList(
    array(),
    array(
        "IBLOCK_ID" => 6,
        "PROPERTY_COMP_USER" => $USER->GetID()
    ),
    false,
    false,
    array(
        "ID",
        "IBLOCK_ID",
        "ACTIVE",
        "PROPERTY_COMP_STATUS"
    )
);

if($company = $company->Fetch()){

    if($company["PROPERTY_COMP_STATUS_ENUM_ID"] == "11"){
        $APPLICATION->IncludeComponent(
            "studio7sbp:seller.personal.product.import",
            "",
            array(
                "SEF_FOLDER" => $arParams["SEF_FOLDER"],
                "IBLOCK_ID" => $arParams["catalog_iblock_id"]
            ),
            false
        );
    }
    else{
        ?>
        <div class="s7sbp--marketplace--saler--lk--right--inner text-center">

            <div class="s7sbp--marketplace--saler--lk--title ff--roboto">Компания находится на модерации</div>
            <div class="alert alert-danger">
                Пока не пройдёт модерация компании возможности добавлять товары нет. Вы можете загрузить регистрационные документы организации в разделе «Документы»
            </div>


            <?$APPLICATION->IncludeComponent("studio7sbp:seller.company.docs.form", "", array_merge(
                $company,
                array(
                    "AJAX_MODE" => "Y",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "USER_ID" => $USER->GetID(),
                    "PAGE" => $APPLICATION->GetCurPage()
                )
            ))?>

        </div>

        <?
    }
}
