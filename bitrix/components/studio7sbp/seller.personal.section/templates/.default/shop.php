<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$APPLICATION->SetPageProperty('title', Loc::getMessage("SPS_TITLE_SHOP"));
include("left_menu.php");

$APPLICATION->IncludeComponent(
	"studio7sbp:seller.personal.shop", 
	"", 
	array(
		"SEF_FOLDER" => $arParams["SEF_FOLDER"],
        "IBLOCK_ID" => 1,
        "PROPERTIES" => array(
            "PERSONE_NAME",
            "PERSONE_LAST_NAME",
            "PERSONE_EMAIL"
        )
	),
	false,
    array("HIDE_ICONS" => "Y")
);
?>