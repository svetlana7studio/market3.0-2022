<?
/**
 * @var CBitrixComponentTemplate $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->addExternalCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
$this->addExternalCss("/about/ui/select/css/nice.select.css");
$this->addExternalJs("/about/ui/select/js/jquery.nice-select.min.js");
$this->addExternalJs("/about/ui/select/js/script.js");

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$APPLICATION->SetPageProperty('title', Loc::getMessage("SPS_TITLE_PRODUCTS"));

if($USER->IsAuthorized()) {
    include("left_menu.php");
}

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
    $APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_ORDERS"), $arResult['PATH_TO_ORDERS']);
?>
<div class="s7sbp--marketplace--saler--lk--right--inner">
    <?$APPLICATION->IncludeComponent(
        "studio7sbp:vendor.order.list",
        "",
        array(
            "PATH" => $APPLICATION->GetCurPage(),
            "DIAPAZONE" => 15
        ),
        false
    );
    ?>
</div>