<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Engine\ActionFilter;

class SellerPersonalProductImportAjaxController extends \Bitrix\Main\Engine\Controller
{

	public function configureActions() {
		return array(
			'getElements' => array(
				'prefilters' => array(
					new ActionFilter\Authentication(),
				),
				'postfilters' => array()
			),
			'changeElements' => array(
				'prefilters' => array(
					new ActionFilter\Authentication(),
				),
				'postfilters' => array()
			),
			'setActiveStatus' => array(
				'prefilters' => array(
					new ActionFilter\Authentication(),
				),
				'postfilters' => array()
			),
			'changeSection' => array(
				'prefilters' => array(
					new ActionFilter\Authentication(),
				),
				'postfilters' => array()
			),
			'deleteElements' => array(
				'prefilters' => array(
					new ActionFilter\Authentication(),
				),
				'postfilters' => array()
			)
		);
	}

    public static function getElementsAction($page_count = 0, $page_number = 1, $filter_name = "", $filter_section_id = 0)
    {

        \Bitrix\Main\Loader::includeModule('studio7spb.marketplace');
        //$moduleOptionCatalogId = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id");
        $userCompany = \Studio7spb\Marketplace\CMarketplaceSeller::getCompanyByUser();

        \Bitrix\Main\Loader::includeModule("iblock");

        $filter = array(
            //"IBLOCK_ID" => $moduleOptionCatalogId,
            "PROPERTY_H_COMPANY" => $userCompany["ID"]
        );
        if(strlen($filter_name) > 3) {
            $filter["NAME"] = "%".$filter_name."%";
        }
        if($filter_section_id > 0) {
            $filter["IBLOCK_SECTION_ID"] = $filter_section_id;
        }

        if($filter["PROPERTY_H_COMPANY"] > 0){

            $elementList = array();
            $oElement = \CIBlockElement::GetList(array("ID" => "DESC"), $filter, false, array(
                'nPageSize' => $page_count,
                'iNumPage' => $page_number,
            ), [
                "*",
                "CATALOG_PRICE_3", // VENDOR_BASE
                "CATALOG_PRICE_4" // VENDOR_DISCOUNT
            ]);
            $countElement = $oElement->SelectedRowsCount();

            $optionComission = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOption("marketplace_comission");

            while($aElement = $oElement->Fetch()){

                $aElement["ACTIVE_STRING"] = $aElement["ACTIVE"] == "Y" ? "Да" : "Нет";
                $aElement["PROPERTY_H_PRICE_DISCOUNT_VALUE"] = floatval($aElement["CATALOG_PRICE_4"]);
                if($aElement["PROPERTY_H_PRICE_DISCOUNT_VALUE"] <= 0)
                    $aElement["PROPERTY_H_PRICE_DISCOUNT_VALUE"] = "-";
                $aElement["CATALOG_PRICE_1"] = floatval($aElement["CATALOG_PRICE_3"]);
                $elementList[] = $aElement;

            }
        }

        return array(
            'count' => $countElement,
            'page' => $page_number,
            'pageSize' => $page_count,
            'items' => $elementList
        );
    }

	/**
	 * @return array
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function changeElementsAction()
	{
		$app = \Bitrix\Main\Application::getInstance();
		$request = $app->getContext()->getRequest();

		$elementList = $request->getPost('items');
		$elementListByItemId = array();
		foreach ($elementList as $key => $elementListItem) {
			$elementListByItemId[$elementListItem['id']] = array(
				"ACTIVE" => $elementListItem["active"],
				"PRICE" => $elementListItem["price"],
				"H_PRICE_DISCOUNT" => $elementListItem["price_discount"],
			);
		}
		unset($elementList, $elementListItem);

		\Bitrix\Main\Loader::includeModule('studio7spb.marketplace');
		\Bitrix\Main\Loader::includeModule("iblock");
		\Bitrix\Main\Loader::includeModule("catalog");

		$moduleOptionCatalogId = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id");
		$userCompany = \Studio7spb\Marketplace\CMarketplaceSeller::getCompanyByUser();

		$oElementUpdate = new \CIBlockElement;
		$oElement = \CIBlockElement::GetList(array("ID" => "DESC"), Array(
			"IBLOCK_ID" => $moduleOptionCatalogId,
			"PROPERTY_H_COMPANY" => $userCompany["ID"],
			"ID" => array_keys($elementListByItemId)
		), false, false, array("ID"));
		while($aElement = $oElement->Fetch()){
			if(empty($elementListByItemId[$aElement["ID"]])) continue;

			\CIBlockElement::SetPropertyValues($aElement["ID"], $moduleOptionCatalogId, $elementListByItemId[$aElement["ID"]]["H_PRICE_DISCOUNT"], "H_PRICE_DISCOUNT");
			$oElementUpdate->Update($aElement["ID"], array("ACTIVE" => $elementListByItemId[$aElement["ID"]]["ACTIVE"]));
			$aPriceInfo = \CPrice::GetBasePrice($aElement["ID"]);
			\Bitrix\Catalog\Model\Price::update($aPriceInfo["ID"], array("PRICE" => $elementListByItemId[$aElement["ID"]]["PRICE"]));
		}
	}

	/**
	 * @return array
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function changeSectionAction()
	{
		$app = \Bitrix\Main\Application::getInstance();
		$request = $app->getContext()->getRequest();

		$elementList = $request->getPost('items');
		$sectionId = (int)$request->getPost('sectionId');

		$elementListByItemId = array();
		foreach ($elementList as $key => $elementListItem) {
			$elementListByItemId[] = $elementListItem['id'];
		}
		unset($elementList, $elementListItem);

		\Bitrix\Main\Loader::includeModule('studio7spb.marketplace');
		\Bitrix\Main\Loader::includeModule("iblock");

		$moduleOptionCatalogId = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id");
		$userCompany = \Studio7spb\Marketplace\CMarketplaceSeller::getCompanyByUser();

		$oElementUpdate = new \CIBlockElement;
		$oElement = \CIBlockElement::GetList(array("ID" => "DESC"), Array(
			"IBLOCK_ID" => $moduleOptionCatalogId,
			"PROPERTY_H_COMPANY" => $userCompany["ID"],
			"ID" => $elementListByItemId
		), false, false, array("ID"));
		while($aElement = $oElement->Fetch()){
			if(!in_array($aElement["ID"], $elementListByItemId)) continue;
			$oElementUpdate->Update($aElement["ID"], array("IBLOCK_SECTION_ID" => $sectionId));
		}
	}

	/**
	 * @return array
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function setActiveStatusAction()
	{
		$app = \Bitrix\Main\Application::getInstance();
		$request = $app->getContext()->getRequest();

		$elementList = $request->getPost('items');
		$selectAction = $request->getPost('selectAction');

		$selectActionValue = "N";
		if($selectAction == "activate") {
			$selectActionValue = "Y";
		}

		$elementListByItemId = array();
		foreach ($elementList as $key => $elementListItem) {
			$elementListByItemId[] = $elementListItem['id'];
		}
		unset($elementList, $elementListItem);

		\Bitrix\Main\Loader::includeModule('studio7spb.marketplace');
		\Bitrix\Main\Loader::includeModule("iblock");

		$moduleOptionCatalogId = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id");
		$userCompany = \Studio7spb\Marketplace\CMarketplaceSeller::getCompanyByUser();

		$oElementUpdate = new \CIBlockElement;
		$oElement = \CIBlockElement::GetList(array("ID" => "DESC"), Array(
			"IBLOCK_ID" => $moduleOptionCatalogId,
			"PROPERTY_H_COMPANY" => $userCompany["ID"],
			"ID" => $elementListByItemId
		), false, false, array("ID"));
		while($aElement = $oElement->Fetch()){
			if(!in_array($aElement["ID"], $elementListByItemId)) continue;
			$oElementUpdate->Update($aElement["ID"], array("ACTIVE" => $selectActionValue));
		}
	}

	/**
	 * @return array
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function deleteElementsAction()
	{
		$app = \Bitrix\Main\Application::getInstance();
		$request = $app->getContext()->getRequest();

		$elementList = $request->getPost('items');

		\Bitrix\Main\Loader::includeModule('studio7spb.marketplace');
		\Bitrix\Main\Loader::includeModule("iblock");

		$moduleOptionCatalogId = \Studio7spb\Marketplace\CMarketplaceOptions::getInstance()->getOption("catalog_iblock_id");
		$userCompany = \Studio7spb\Marketplace\CMarketplaceSeller::getCompanyByUser();

		$oElementUpdate = new \CIBlockElement;
		$oElement = \CIBlockElement::GetList(array("ID" => "DESC"), Array(
			"IBLOCK_ID" => $moduleOptionCatalogId,
			"PROPERTY_H_COMPANY" => $userCompany["ID"],
			"ID" => $elementList
		), false, false, array("ID"));
		while($aElement = $oElement->Fetch()){
			if(!in_array($aElement["ID"], $elementList)) continue;
			\CIBlockElement::Delete($aElement["ID"]);
		}
	}
}