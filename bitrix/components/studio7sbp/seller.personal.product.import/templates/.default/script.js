var mp_product = {
	productListAjaxSend: false,
	cntProductByPage: 30,
	pageNumber: 1,
	filterName: "",
	filterSectionId: 0,
	templateId: 'product-table-item-template',
	tableId: 'product-table-list'
}

mp_product.loadProductList = function() {
	if(mp_product.productListAjaxSend) return false;

	mp_product.startAjaxLoader();

	mp_product.productListAjaxSend = true;

	BX.ajax.runComponentAction('studio7sbp:seller.personal.product.import', 'getElements', {
		mode:'ajax',
		data: {
			page_count: mp_product.cntProductByPage,
			page_number: mp_product.pageNumber,
			filter_name: mp_product.filterName,
			filter_section_id: mp_product.filterSectionId,
		}
	}).then(function(response){
		mp_product.drawRow(response.data.items);
		mp_product.drawPagination(response.data.pageSize, response.data.page, response.data.count);

		mp_product.stopAjaxLoader();
	});
}

mp_product.drawPagination = function(pageSize, pageNumber, elementCount) {
	$("#"+mp_product.tableId).find("tfoot").empty();
	$("#footer-info-count").text(elementCount);

	var paginatonHtml = mp_product.generatePaginationHtml(pageSize, pageNumber, elementCount);
	if(paginatonHtml.length) {
		$("#"+mp_product.tableId).find("tfoot").append(paginatonHtml);
	}
}

mp_product.drawRow = function(items) {
	var rowList = "";
	$("#"+mp_product.tableId).find("tbody").empty();
	for (var key in items) {
		var item = {
			item_id: items[key].ID,
			item_name: items[key].NAME,
			item_price: items[key].CATALOG_PRICE_1,
			item_price_discount: items[key].PROPERTY_H_PRICE_DISCOUNT_VALUE,
			item_active: items[key].ACTIVE,
			item_active_string: items[key].ACTIVE_STRING,
			item_section: catTree[items[key].IBLOCK_SECTION_ID].NAME
		};

		rowList += mp_product.renderTemplate(item);
	}
	$("#"+mp_product.tableId).find("tbody").append(rowList);
}

mp_product.renderTemplate = function(data) {
	var template = $("#"+mp_product.templateId).html();

	for (var property in data) {
		if (data.hasOwnProperty(property)) {
			var search = new RegExp('{' + property + '}', 'g');
			template = template.replace(search, data[property]);
		}
	}

	return template;
}

mp_product.startAjaxLoader = function() {
	document.body.appendChild(window.salePersonalSectionLoaderContainer);
	window.salePersonalSectionLoader.show(window.salePersonalSectionLoaderContainer);
}
mp_product.stopAjaxLoader = function() {
	mp_product.productListAjaxSend = false;
	window.salePersonalSectionLoader.hide();
	window.salePersonalSectionLoader.destroy();
	BX.remove(window.salePersonalSectionLoaderContainer);
}

mp_product.generatePaginationHtml = function(pageSize, pageNumber, elementCount) {
	pageNumber = parseInt(pageNumber);
	if(pageNumber == 1) {
		if(elementCount <= pageSize) {
			return "";
		}
	}

	var tmpl = '<tr><td colspan="5" class="s7sbp--marketplace--saler--pagination"><div class="s7sbp--marketplace--saler--pagination-pages-list">';
	var pageNumbers = Math.ceil(elementCount/pageSize);


	for (var i = 1; i <= pageNumbers; i++) {
		if(pageNumber == i) {
			tmpl += '<span class="s7sbp--marketplace--saler--pagination--item active">'+i+'</span>';
		} else {
			tmpl += '<a href="#" data-handler="onClick" data-action="setPagination" data-page="'+i+'" class="s7sbp--marketplace--saler--pagination--item">'+i+'</a>';
		}
	}
	tmpl += '</div><div class="s7sbp--marketplace--saler--pagination-arrows">';

	// set prev
	if(pageNumber == 1) {
		tmpl += '<span class="s7sbp--marketplace--saler--pagination-arrows-item prev">Предыдущая</span>';
	} else {
		tmpl += '<a href="" data-handler="onClick" data-action="setPagination" data-page="'+(pageNumber-1)+'" class="s7sbp--marketplace--saler--pagination-arrows-item prev">Предыдущая</a>';
	}

	if(pageNumber == pageNumbers) {
		tmpl += '<span class="s7sbp--marketplace--saler--pagination-arrows-item next">Следующая</span>';
	} else {
		tmpl += '<a href="" data-handler="onClick" data-action="setPagination" data-page="'+(pageNumber+1)+'" class="s7sbp--marketplace--saler--pagination-arrows-item next">Следующая</a>';
	}
	tmpl += '</div></td></tr>';

	return tmpl;
}

$(function() {
	mp_product.loadProductList();

	var timeoutID = null;

	$('body').on('click', '.s7sbp--marketplace--saler--lk--product-import--tabs--control--item', function(e) {
		e.preventDefault();
		if($(this).attr('data-active-tab') == "y") {
			return false;
		}
		var tabTarget = $(this).attr('data-target');
		$('.s7sbp--marketplace--saler--lk--product-import--tabs--item, .s7sbp--marketplace--saler--lk--product-import--tabs--control--item').attr('data-active-tab', 'n');

		$(this).attr('data-active-tab', 'y');
		$('.s7sbp--marketplace--saler--lk--product-import--tabs--item[data-name="'+tabTarget+'"]').attr('data-active-tab', 'y');

	}).on('change', '[name="productListCount"]', function(e) {
		var val = $(this).val();
		clearTimeout(timeoutID);
		if(val == '' || val == undefined) {
			return;
		}

		// clear togler
		$("#products-check-toggler").attr("checked", null);

		timeoutID = setTimeout(function() {
			mp_product.cntProductByPage = parseInt(val);
			mp_product.loadProductList();
		}, 300);
	}).on('change', '.s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--body--row.product-item .product-item--checkbox input', function(e) {
		if(!$(this).is(":checked")) {
			$(this).parents('.product-item').removeClass('checked');
		} else {
			$(this).parents('.product-item').addClass('checked');
		}

		if($("#product-table-list").find(".active").length > 0){
			$("#footer-info-choose-count").text($("#product-table-list").find(".active").text() + "/" + $('.product-item--checkbox input:checked').length);
		}else{
			$("#footer-info-choose-count").text("1/" + $('.product-item--checkbox input:checked').length);
		}

	}).on('click', '[data-handler="onClick"]', function(e) {
		e.preventDefault();
		var action = $(this).attr('data-action');
		switch(action) {
			case 'setPagination':
				var pageNumber = $(this).attr('data-page');
				mp_product.pageNumber = pageNumber;
				mp_product.loadProductList();
				$("#footer-info-choose-count").text(pageNumber + "/0");
				$("#products-check-toggler").attr("checked", null);
			break;
			case 'filterSet':
				console.log('filterSet');
				var filterName = $('#productName').val(),
					filterSectionId = $('#productSection').val();

				mp_product.filterName = filterName;
				if(filterSectionId != "" || filterSectionId != undefined) {
					mp_product.filterSectionId = parseInt(filterSectionId);
				}
				
				mp_product.loadProductList();

			break;
			case 'filterCansel':
				$('#productName').val('');
				$('#productSection').val('').trigger('change');

				mp_product.filterName = "";
				mp_product.filterSectionId = 0;
				
				mp_product.loadProductList();
				
			break;
			case 'editProduct': 
				var row = $('.s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--body--row.product-item');
				if(row.find(".product-item--checkbox input:checked").length <= 0) {
					alert("Не выбраны товары для редактирования!");
					return false;
				}
				row.each(function(index, el) {
					if($(el).find(".product-item--checkbox input").is(":checked")) {
						$(el).addClass('edit-mode');
						var select = $(el).find(".product-item--select");
						select.val(select.parents('.product-item--active').attr('data-active'));
						select.trigger('change');
					}
				});
				$(this).hide();
				$('[data-handler="onClick"][data-action="acceptEditProduct"]').css({display: "inline-block"});
			break;
			case 'acceptEditProduct':
				var row = $('.s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--body--row.product-item.edit-mode'),
					items = [];
				if(row.length <= 0) {
					alert("Не выбраны товары для редактирования!");
					return false;
				}
				row.each(function(index, el) {
					var itemId = $(el).attr('data-entity-id');
					var item = {
						id: itemId,
						price: $(el).find('[name="product\['+itemId+'\]\[price\]"]').val(),
						price_discount: $(el).find('[name="product\['+itemId+'\]\[price_discount\]"]').val(),
						active: $(el).find('[name="product\['+itemId+'\]\[active\]"]').val(),
					}
					items.push(item);
				});

				if(mp_product.productListAjaxSend) return false;
				mp_product.startAjaxLoader();
				mp_product.productListAjaxSend = true;
				BX.ajax.runComponentAction('studio7sbp:seller.personal.product.import', 'changeElements', {
					mode:'ajax',
					data: {
						items: items,
					}
				}).then(function(response){
					mp_product.stopAjaxLoader();
					$('[data-handler="onClick"][data-action="acceptEditProduct"]').hide();
					$('[data-handler="onClick"][data-action="editProduct"]').css({display: "inline-block"});
					mp_product.loadProductList();
				});
			
			break;
			case 'deleteProduct':
				var row = $('.s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--body--row.product-item');

				if($('.s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--body--row.product-item.edit-mode').length) {
					row.each(function(index, el) {
						if($(el).find(".product-item--checkbox input").is(":checked")) {
							$(el).removeClass('edit-mode');
						}
					});
					$('[data-handler="onClick"][data-action="acceptEditProduct"]').hide();
					$('[data-handler="onClick"][data-action="editProduct"]').css({display: "inline-block"});
					return false;
				}

				if(row.find(".product-item--checkbox input:checked").length <= 0) {
					alert("Не выбраны товары для редактирования!");
					return false;
				}

				var isConfirm = confirm("Вы уверены что хотите удалить товары? Данная операция необратима!");
				
				if(!isConfirm) {
					return false;
				}
				var items = [];

				row.each(function(index, el) {
					if($(el).find(".product-item--checkbox input").is(":checked")) {
						var itemId = $(el).attr('data-entity-id');
						items.push(itemId);
					}
				});

				if(mp_product.productListAjaxSend) return false;
				mp_product.startAjaxLoader();
				mp_product.productListAjaxSend = true;
				BX.ajax.runComponentAction('studio7sbp:seller.personal.product.import', 'deleteElements', {
					mode:'ajax',
					data: {
						items: items,
					}
				}).then(function(response){
					mp_product.stopAjaxLoader();
					mp_product.loadProductList();
				});

			break;
			case 'deleteProductSelf':

				var isConfirm = confirm("Вы уверены что хотите удалить товар? Данная операция необратима!");
				
				if(!isConfirm) {
					return false;
				}

				var itemId = $(this).parents('.product-item').attr('data-entity-id'),
					items = [];

				items.push(itemId);

				if(mp_product.productListAjaxSend) return false;
				mp_product.startAjaxLoader();
				mp_product.productListAjaxSend = true;
				BX.ajax.runComponentAction('studio7sbp:seller.personal.product.import', 'deleteElements', {
					mode:'ajax',
					data: {
						items: items,
					}
				}).then(function(response){
					mp_product.stopAjaxLoader();
					mp_product.loadProductList();
				});

			break;
			case 'actionProductExecute':
				var row = $('.s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--body--row.product-item'),
					items = [],
					action = $('#productAction').val();
				if(row.find(".product-item--checkbox input:checked").length <= 0) {
					alert("Не выбраны товары для редактирования!");
					return false;
				}
				if(action == "" || action == undefined) {
					alert("Не выбрано действие");
					return false;
				}

				row.each(function(index, el) {
					if($(el).find(".product-item--checkbox input").is(":checked")) {
						var itemId = $(el).attr('data-entity-id');
						var item = {
							id: itemId,
						}
						items.push(item);
					}
				});

				var compAction = '',
					sectionId = 0;
				if(action == 'activate' || action == 'deactivate') {
					compAction = 'setActiveStatus';
				}
				if(action == 'change_section') {
					var sectionId = $('[data-target="actionProductChangeSection"] select').val();
					if(sectionId == "" || sectionId == undefined) {			
						alert("Не выбран раздел");
						return false;
					}
					compAction = 'changeSection';
				}

				if(mp_product.productListAjaxSend) return false;
				mp_product.startAjaxLoader();
				mp_product.productListAjaxSend = true;
				BX.ajax.runComponentAction('studio7sbp:seller.personal.product.import', compAction, {
					mode:'ajax',
					data: {
						sectionId: parseInt(sectionId),
						selectAction: action,
						items: items,
					}
				}).then(function(response){
					mp_product.stopAjaxLoader();
					$('[data-target="actionProductChangeSection"], [data-target="actionProductAccept"]').hide();
					mp_product.loadProductList();
				});

			break;

		}

	}).on('change', '[data-handler="onChange"]', function(e) {
		var action = $(this).attr('data-action');
		switch(action) {
			case 'actionProduct':
				var val = $(this).val();
				if(val == "" || val == undefined) {
					$('[data-target="actionProductChangeSection"], [data-target="actionProductAccept"]').hide();
					return false;
				}
				
				if(val == 'change_section') {
					$('[data-target="actionProductChangeSection"]').css({display: "inline-block"});
				}

				$('[data-target="actionProductAccept"]').css({display: "inline-block"});

			break;
		}
	});

});

/**
 *
 * @param t
 */
function toggleAllProducts(t) {
	var checkboxes = ".product-item--checkbox";
	checkboxes = $(checkboxes).find("input");
	if($(t).is(":checked")){
		checkboxes.attr("checked", "checked");
		if($("#product-table-list").find(".active").length > 0){
			$("#footer-info-choose-count").text($("#product-table-list").find(".active").text() + "/" + checkboxes.length);
		}else{
			$("#footer-info-choose-count").text("1/" + checkboxes.length);
		}
	}else{
		checkboxes.attr("checked", null);
		$("#footer-info-choose-count").text("1/0");
	}
}

setTimeout(function (){
	$('select').niceSelect("destroy");
}, 2000);