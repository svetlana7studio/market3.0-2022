<?
/**
 * @var array $arParams
 * @var CMain $APPLICATION
 * @var CUser $USER
 */
$APPLICATION->IncludeComponent("studio7sbp:seller.personal.import.transfer", "", array(
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "USER_ID" => $USER->GetID(),
    "COMPANY_IBLOCK_ID" => 6
));