<?
/**
 * @var CBitrixComponentTemplate $this
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array('popup', 'ajax', 'loader'));
?>

<script>
    var catTree = <?=CUtil::PhpToJSObject($arResult["cat_tree"])?>;
</script>

<div class="s7sbp--marketplace--saler--lk--right--inner">

	<div class="s7sbp--marketplace--saler--lk--title ff--roboto">Мои товары</div>

	<div class="s7sbp--marketplace--saler--lk--product-import ff--roboto">

		<div class="s7sbp--marketplace--saler--lk--product-import--tabs--control">
			<div class="s7sbp--marketplace--saler--lk--product-import--tabs--control--item" data-active-tab="y" data-target="productList">Список товаров</div>
			<div class="s7sbp--marketplace--saler--lk--product-import--tabs--control--item" data-target="productImort">Импорт</div>
			<div class="clearfix"></div>
		</div>

		<div class="s7sbp--marketplace--saler--lk--product-import--tabs">
			<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item" data-active-tab="y" data-name="productList">			
				<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter">
					<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter-title">Фильтр</div>
					<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--left">
						<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--fields">
							<label for="productName" class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--fields--label">Название</label>
							<input type="text" name="productName" id="productName" class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--fields--input-text">
						</div>
						<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--fields">
							<label for="productSection" class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--fields--label">Раздел</label>
							<select name="productSection" id="productSection" class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--fields--select">
								<option value="">dss</option>
								<?foreach ($arResult["cat_tree"] as $aCatTreeItem):?>
									<option value="<?=$aCatTreeItem["ID"]?>"><?=str_repeat(" . ", $aCatTreeItem["DEPTH_LEVEL"])?><?=$aCatTreeItem["NAME"]?></option>
								<?endforeach?>
							</select>
						</div>
					</div>
					<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--right">
						<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--fields">
							<button data-action="filterSet" data-handler="onClick" class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--fields--btn search">Найти</button>
							<button data-action="filterCansel" data-handler="onClick" class="s7sbp--marketplace--saler--lk--product-import--tabs--item--filter--fields--btn cansel">Отменить</button>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--control">
					<button class="s7sbp--marketplace--saler--lk--product-import--tabs--item--control--btn add" onClick="location.href='/seller/products/add/'">Добавить товар</button>
					<!-- <button class="s7sbp--marketplace--saler--lk--product-import--tabs--item--control--btn add">Добавить раздел</button> -->
					<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--control--item-count">
						<span>Элементов на странице</span>
                        <select name="productListCount">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30" selected>30</option>
                            <option value="35">35</option>
                            <option value="40">40</option>
                            <option value="45">45</option>
                            <option value="50">50</option>
                        </select>


					</div>
				</div>
				<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list" id="product-table-list">
					
					<table class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table">
						<thead>
							<tr class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--header">
								<th><input type="checkbox" id="products-check-toggler" onclick="toggleAllProducts(this)" /></th>
								<th>Название товара</th>
                                <th>Цена</th>
                                <th>Цена со скидкой</th>
                                <th>Опубликовано</th>
                                <th>Удалить</th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
						</tfoot>
					</table>

					<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table--control">
						<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table--control--action-btn">
							<span class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table--control--action-btn--label">Выполнить:</span>
							<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table--control--action-btn--item" data-handler="onClick" data-action="editProduct"></div>
							<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table--control--action-btn--item" data-handler="onClick" data-action="acceptEditProduct"></div>
							<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table--control--action-btn--item" data-handler="onClick" data-action="deleteProduct"></div>
						</div>
						<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table--control--action-select">
							<select name="productAction"  id="productAction" data-handler="onChange" data-action="actionProduct">
								<option value="">- Действие</option>
								<option value="activate">Активировать</option>
								<option value="deactivate">Деактивировать</option>
								<option value="change_section">Перенести в раздел</option>
							</select>
						</div>
						<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table--control--action-select" style="display:none" data-target="actionProductChangeSection">
							<select name="productActionSection" id="productActionSection" style="width: 300px">				
								<option value=""></option>
								<?foreach ($arResult["cat_tree"] as $aCatTreeItem):?>
									<option value="<?=$aCatTreeItem["ID"]?>"><?=str_repeat(" . ", $aCatTreeItem["DEPTH_LEVEL"])?><?=$aCatTreeItem["NAME"]?></option>
								<?endforeach?>
							</select>
						</div>
						<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table--control--action-btn--item" style="display:none" data-target="actionProductAccept" data-handler="onClick" data-action="actionProductExecute">Применить</div>

                        <div class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--table--control--action-select">
                            Отмечено: <span id="footer-info-choose-count">1/0</span>
                            Всего: <span id="footer-info-count">0</span>
                        </div>

					</div>
				</div>

			</div>
			<div class="s7sbp--marketplace--saler--lk--product-import--tabs--item" data-name="productImort">
				<?include "inc/import.php"?>
			</div>
		</div>
	</div>
</div>

<script type="html/tpl" id="product-table-item-template">
	<tr class="s7sbp--marketplace--saler--lk--product-import--tabs--item--product-list--body--row product-item" data-entity-id="{item_id}">
		<td>
		<span class="product-item--checkbox">
				<input type="checkbox" name="product[{item_id}][checked]" value="Y">
			</span>
</td>
		<td>
			 <span class="product-item--name">
				<a href="/seller/products/add/{item_id}/">{item_name}</a>
			</span>
			<br>
			<span class="product-item--section">{item_section}</span>
		</td>
		<td class="product-item--price">
			<span class="product-item--input--text">{item_price}</span>
			<input type="text" class="product-item--input" name="product[{item_id}][price]" value="{item_price}">
		</td>
		<td class="product-item--price-discout">
			<span class="product-item--input--text">{item_price_discount}</span>
			<input type="text" class="product-item--input" name="product[{item_id}][price_discount]" value="{item_price_discount}">
		</td>
		<td class="product-item--active" data-active="{item_active}">
			<span class="product-item--input--text">{item_active_string}</span>
			<select  class="product-item--select" name="product[{item_id}][active]">
				<option value="Y">Да</option>
				<option value="N">Нет</option>
			</select>
		</td>
		<td class="product-item--remove"><span data-handler="onClick" data-action="deleteProductSelf"></span></td>
	</tr>
</script>