<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc,
	\Bitrix\Main\Loader,
	\Studio7spb\Marketplace\CMarketplaceSeller;

Loc::loadMessages(__FILE__);
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/studio7spb.marketplace/options.php");

if (!\Bitrix\Main\Loader::includeModule('studio7spb.marketplace')) {
	ShowError(Loc::getMessage('studio7spb.marketplace_MODULE_NOT_INSTALLED'));
	die();
}

class SellerPersonalProductImport extends CBitrixComponent
{
	public function onPrepareComponentParams($params)
	{
		// $params["user_company"] = CMarketplaceSeller::getCompanyByUser();
		return $params;
	}

	function getCategoryTree() {
		if(empty($this->arResult["cat_tree"])) {
			Loader::includeModule("iblock");

			$oCatTree = \CIBlockSection::GetTreeList(Array("IBLOCK_ID"=>$this->arParams['IBLOCK_ID']), array("ID", "NAME", "DEPTH_LEVEL"));
			while($aCatTree = $oCatTree->GetNext())
				$this->arResult["cat_tree"][$aCatTree["ID"]] = $aCatTree;

		}
	}

	public function executeComponent()
	{
		if(!CMarketplaceSeller::canDoOperation("edit_product")){
			\localRedirect("/auth/");
			return false;
		}

		$this->getCategoryTree();
		$this->includeComponentTemplate();
	}
}