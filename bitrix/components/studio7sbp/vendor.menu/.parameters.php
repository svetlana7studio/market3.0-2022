<?
$arComponentParameters = [
    "PARAMETERS" => [
        "VENDOR_ID" => [
            "NAME" => "VENDOR_ID",
            "TYPE" => "STRING"
        ],
        "VENDOR_TYPE" => [
            "NAME" => "VENDOR_TYPE",
            "TYPE" => "LIST",
            "VALUES" => [
                "Shop",
                "M",
                "W",
            ]
        ]
    ]
];