<?
use \Bitrix\Main\Loader,
    \Studio7spb\Marketplace\VendorMessageTable,
    \Studio7spb\Marketplace\CMarketplaceSeller,
    \Bitrix\Main\Engine\Contract\Controllerable,
    Bitrix\Main\Engine\ActionFilter,
    \Bitrix\Iblock\ElementTable,
    Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

class hobbyVendorMenu extends CBitrixComponent implements Controllerable
{
    /**
     * @var array
     */
    private $_users;

    /**
     * @var int
     */
    private $_productsCount;
    /**
     * @var int
     */
    private $_mCount;
    /**
     * @var int
     */
    private $_wCount;

    /**
     * @return array
     */
    public function getUsers()
    {
        return $this->_users;
    }

    /**
     * @param array $users
     */
    public function setCompanyUsers()
    {
        if($this->arParams["VENDOR_ID"] > 0)
            $this->_users = CMarketplaceSeller::getCompanyUsers($this->arParams["VENDOR_ID"]);
    }

    public function setProductsCount()
    {
        $count = ElementTable::getList([
            "filter" => [
                "IBLOCK_ID" => $this->arParams["IBLOCK_CATALOG"],
                "ACTIVE" => "Y",
                "CREATED_BY" => $this->getUsers()
            ],
            "select" => ["ID"]
        ]);
        $this->_productsCount = $count->getSelectedRowsCount();
    }

    public function setMCount()
    {
        $count = ElementTable::getList([
            "filter" => [
                "IBLOCK_ID" => $this->arParams["IBLOCK_M"],
                "ACTIVE" => "Y",
                "CREATED_BY" => $this->getUsers()
            ],
            "select" => ["ID"]
        ]);
        $this->_mCount = $count->getSelectedRowsCount();
    }

    public function setWCount()
    {
        $count = ElementTable::getList([
            "filter" => [
                "IBLOCK_ID" => $this->arParams["IBLOCK_W"],
                "ACTIVE" => "Y",
                "CREATED_BY" => $this->getUsers()
            ],
            "select" => ["ID"]
        ]);
        $this->_wCount = $count->getSelectedRowsCount();
    }

    /**
     * @return int
     */
    public function getProductsCount()
    {
        return $this->_productsCount > 0 ? $this->_productsCount : 0;
    }

    /**
     * @return int
     */
    public function getMCount()
    {
        return $this->_mCount > 0 ? $this->_mCount : 0;
    }

    /**
     * @return int
     */
    public function getWCount()
    {
        return $this->_wCount > 0 ? $this->_wCount : 0;
    }

    public function getLink($section)
    {
        $link = $this->arParams["SEF"]["FOLDER"] . $this->arParams["SEF"]["URL_TEMPLATES"]["detail"];
        $link = str_replace([
            "#ELEMENT_ID#",
            "#SECTION_ID#"
        ], [
            $this->arParams["VENDOR_ID"],
            $section
        ], $link);

        return $link;
    }

    public function executeComponent()
    {
        CJSCore::Init([]);
        $this->setCompanyUsers();

        $this->arResult = [
            "USERS" => $this->getUsers()
        ];


        $this->useCurrentUser();
        $this->includeComponentTemplate();
        return $this->getUsers();
    }

    public function useCaptcha()
    {
        global $APPLICATION, $USER;
        if($USER->IsAuthorized())
        {
            $this->arParams["USE_CAPTCHA"] = "Y";
        }
        if($this->arParams["USE_CAPTCHA"] == "Y"){
            $this->arResult["capCode"] = htmlspecialcharsbx($APPLICATION->CaptchaGetCode());
        }
    }

    public function useCurrentUser()
    {
        global $USER;
        $this->arResult["CURRENT_USER"] = [
            "EMAIL" => $USER->GetEmail(),
            "NAME" => $USER->GetFullName()
        ];
    }

    public function configureActions()
    {
        // TODO: Implement configureActions() method.
        return [
            'saveMessage' => [ // Ajax-метод
                'prefilters' => [
                    //new ActionFilter\Authentication(),
                    //new ActionFilter\HttpMethod([ActionFilter\HttpMethod::METHOD_POST])
                ],
                'postfilters' => []
            ],
        ];
    }

    /**
     * @param string $name
     * @param string $mail
     * @param string $text
     * @return string[]
     */
    public static function saveMessageAction($name="", $mail="", $text="", $vendor=0, $category="", $captcha_sid="", $captcha_word="")
    {
        global $USER;
        $message = [
            "TYPE" => "OK",
            "TEXT" => Loc::getMessage("ACTION_SAVE_SUCCESS"),
        ];

        Loader::registerAutoLoadClasses(null, [
            "\Studio7spb\Marketplace\VendorMessageTable" => "/bitrix/modules/studio7spb.marketplace/lib/market2/vendormessagetable.php"
        ]);

        $arFields = [
            "NAME" => htmlspecialcharsEx($name),
            "EMAIL" => htmlspecialcharsEx($mail),
            "TEXT" => htmlspecialcharsEx($text),
            "VENDOR_ID" => intval($vendor),
            "SORT" => 100,
            "ACTIVE" => "N",
            "CATEGORY" => htmlspecialcharsEx($category),
            "DATE_CREATE" => new \Bitrix\Main\Type\DateTime(),
            "USER_CREATE" => $USER->GetID()
        ];

        VendorMessageTable::add($arFields);

        return $message;
    }

}
