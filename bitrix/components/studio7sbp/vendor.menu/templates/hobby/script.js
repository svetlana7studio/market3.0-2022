var vendorMenu = {
    form: null,
    wait: null,
    fields: [],
    data: [],
    errorCls: "error-field",
    errors: [],

    // actions
    submit: function (form){
        this.form = form;
        this.formWait();
        this.data = new FormData(this.form);

        this.validate();

        if(this.errors.length > 0){
            this.showErros();
            this.closeFormWait();
        }else{
            BX.ajax.runComponentAction('hobby:vendor.menu', 'saveMessage', {
                mode:'class',
                data: this.data,
                method: "post"
            }).then(function(response){
                vendorMenu.clearForm();
                vendorMenu.showMessageForm(response.data);
                vendorMenu.closeFormWait();
            });
        }



        return false;
    },

    validate: function (){
        this.removeErrors();
        this.data.forEach((value, name) => {
            if(!value || value.length < 1){
                vendorMenu.errors.push($(this.form).find("[name='" + name + "']"))
            }

        });
    },

    showErros: function (){
        var i;
        for(i=0; i < this.errors.length; i++)
        {
            this.errors[i].addClass(this.errorCls);
        }
    },

    hideErrors: function (){
        var i;
        for(i=0; i < this.errors.length; i++)
        {
            this.errors[i].removeClass(this.errorCls);
        }
    },
    removeErrors: function (){
        this.hideErrors();
        this.errors = [];
    },

    // form
    clearForm: function (){
        $(this.form).find(".lk-design-inner").hide();
        $(this.form).find(".modal__color-btn").hide();
    },
    setFormTitle: function (title){
        $(this.form).find(".modal__title").text(title);
    },
    showMessageForm: function (message){
        vendorMenu.setFormTitle(message.TEXT);
    },
    formToDefault: function (){
        $(this.form).find(".lk-design-inner").show();
        $(this.form).find(".modal__color-btn").show();
        $(this.form).find("input").each(function (){
            $(this).val($(this).data("value"));
        });
        $(this.form).find("textarea").each(function (){
            $(this).val($(this).data("value"));
        });
        var title = $(this.form).find(".modal__title");
        title.text(title.data("value"));
    },
    formWait: function (){
        this.wait = BX.showWait(this.form);
    },
    closeFormWait: function (){
        BX.closeWait(this.form, this.wait);
    },

    // console
    consoleForm: function (){
        this.d("console Form");
        this.d(this.form);
    },
    consoleData: function (){
        this.d("console Data");
        this.d(this.data);
    },
    consoleErrors: function (){
        this.d("console Errors");
        this.d(this.errors);
    },
    d: function (value){
        console.info(value);
    }

};