<?
/**
 * @var array $arParams
 * @var array $arResult
 */

use Bitrix\Main\Localization\Loc;
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
Loc::loadLanguageFile(__FILE__);
?>

<a href="#modal-vendor-message"
   onclick="vendorMenu.formToDefault()"
   class="fancybox"><?=Loc::getMessage("VENDOR_MENU_MESSAGE")?></a>

<form id="modal-vendor-message"
      onsubmit="return vendorMenu.submit(this)"
      class="d-none">

    <?=bitrix_sessid_post()?>
    <input type="hidden" name="vendor" value="<?=$arParams["VENDOR_ID"]?>">

    <h3 class="modal__title"
        data-value="<?=Loc::getMessage("VENDOR_MENU_MESSAGE_SPAM")?>"><?=Loc::getMessage("VENDOR_MENU_MESSAGE")?></h3>

    <div class="lk-design-inner">
        <div class="lk-design-item">
            <div class="lk-design-name text-nowrap">
                <?=Loc::getMessage("VENDOR_MENU_MESSAGE_NAME")?> <span>*</span>:
            </div>
            <div class="lk-design-input">
                <input type="text"
                       name="name"
                       value="<?=$arResult["CURRENT_USER"]["NAME"]?>"
                       data-value="<?=$arResult["CURRENT_USER"]["NAME"]?>"
                       placeholder="<?=Loc::getMessage("VENDOR_MENU_MESSAGE_NAME_PLACEHOLDER")?>">
            </div>
        </div>

        <div class="lk-design-item">
            <div class="lk-design-name text-nowrap">
                <?=Loc::getMessage("VENDOR_MENU_MESSAGE_EMAIL")?> <span>*</span>:
            </div>
            <div class="lk-design-input">
                <input type="text"
                       name="mail"
                       value="<?=$arResult["CURRENT_USER"]["EMAIL"]?>"
                       data-value="<?=$arResult["CURRENT_USER"]["EMAIL"]?>"
                       placeholder="<?=Loc::getMessage("VENDOR_MENU_MESSAGE_EMAIL_PLACEHOLDER")?>">
            </div>
        </div>

        <div class="lk-design-item d-block">
            <div class="lk-design-name">
                <?=Loc::getMessage("VENDOR_MENU_MESSAGE_TEXT")?> <span>*</span>
            </div>
            <div class="lk-design-input">
                <textarea name="text"
                          placeholder="<?=Loc::getMessage("VENDOR_MENU_MESSAGE_TEXT_PLACEHOLDER")?>"></textarea>
            </div>
        </div>

        <?if($arParams["USE_CAPTCHA"] == "Y"):?>
            <div class="lk-design-item d-block">
                <div class="lk-design-name">
                    <?=Loc::getMessage("VENDOR_MENU_MESSAGE_CAPTCHA")?> <span>*</span>:
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
                    <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                </div>
                <div class="lk-design-input">
                    <input type="text" name="captcha_word" value="">
                </div>
            </div>
        <?endif;?>

    </div>

    <div class="modal__color-btn text-center">
        <button class="choose" type="submit"><?=Loc::getMessage("VENDOR_MENU_MESSAGE_SUBMIT")?></button>
    </div>

</form>