<?
$MESS = [
    "VENDOR_MENU_REVIEWS" => "Отзывы",
    "VENDOR_MENU_M" => "Мастер-классы",
    "VENDOR_MENU_W" => "Дизайны",
    "VENDOR_MENU_SHOP" => "Магазин",
    "VENDOR_MENU_PROFILE" => "Профиль",
    "VENDOR_MENU_MESSAGE" => "Написать сообщение",
    "VENDOR_MENU_MESSAGE_SPAM" => "Написать ещё сообщение",
    "VENDOR_MENU_MESSAGE_NAME" => "Имя",
    "VENDOR_MENU_MESSAGE_NAME_PLACEHOLDER" => "Иванов Иван Иванович",
    "VENDOR_MENU_MESSAGE_EMAIL" => "E-mail",
    "VENDOR_MENU_MESSAGE_EMAIL_PLACEHOLDER" => "ivan@ivanov.ru",
    "VENDOR_MENU_MESSAGE_TEXT" => "Текст сообщения",
    "VENDOR_MENU_MESSAGE_TEXT_PLACEHOLDER" => "Текст сообщения",
    "VENDOR_MENU_MESSAGE_CAPTCHA" => "Введите слово на картинке",
    "VENDOR_MENU_MESSAGE_SUBMIT" => "Отправить",
];