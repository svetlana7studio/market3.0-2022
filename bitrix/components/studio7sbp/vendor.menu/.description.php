<?
/**
 * vendor.menu
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "vendor.menu",
    "DESCRIPTION" => "vendor.menu",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "vendor.menu",
            "NAME" => "vendor.menu"
        )
    ),
);