<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */


if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["ID"] = intval($arParams["ID"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["DEPTH_LEVEL"] = intval($arParams["DEPTH_LEVEL"]);
if($arParams["DEPTH_LEVEL"]<=0)
    $arParams["DEPTH_LEVEL"]=1;

global $aMenuLinksNew, $currentSection;
$aMenuLinksNew = [];
$arResult["SECTIONS"] = [];
$arParentSections = [];
$arChieldSections = [];
$currentSection = [];

if($this->StartResultCache())
{
    if(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
    }
    else
    {

        // <editor-fold defaultstate="collapsed" desc=" # Выборка без урла и формирорвание урла хелпером">
        global $sections;
        $sections = array();
        $resCatalog = CIBlockSection::GetList(
            [
                "NAME" => "asc"
            ],
            [
                "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
                "GLOBAL_ACTIVE"=>"Y",
                "ACTIVE"=>"Y",
                "IBLOCK_ACTIVE"=>"Y",
                //"<=DEPTH_LEVEL"=>"3"
            ],
            false,
            [
                "ID",
                "DEPTH_LEVEL",
                "NAME",
                "CODE",
                //"SECTION_PAGE_URL",
                "UF_PARENT",
                //"LEFT_MARGIN",
                //"RIGHT_MARGIN",
                "IBLOCK_SECTION_ID",
            ]
        );
        while($arCatalog = $resCatalog->Fetch()){
            $sections[$arCatalog['ID']] = $arCatalog;
        }
        // </editor-fold>

        foreach($sections as &$arSection){
            $arSection['SECTION_PAGE_URL'] = makeUrl($arSection['ID']);
            $arSection['~SECTION_PAGE_URL'] = $arSection['SECTION_PAGE_URL'];

            $arSection["NAME"] = $arSection["NAME"];
            $arSection["~NAME"] = $arSection["NAME"];

            if(empty($currentSection)){
                if($arParams["IBLOCK_SECTION_ID"] == $arSection["CODE"]){
                    $currentSection = $arSection;
                    $currentSection["CHILDS_LEVEL"] = $arSection["DEPTH_LEVEL"]+1;
                    $currentSection["END_LEVEL"] = $arSection["DEPTH_LEVEL"] + $arParams["DEPTH_LEVEL"];
                }
            }

            // fill all sections
            $arResult["SECTIONS"][] = array(
                "ID" => $arSection["ID"],
                "DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
                "~NAME" => $arSection["~NAME"],
                "~SECTION_PAGE_URL" => $arSection["~SECTION_PAGE_URL"],
            );

            if(!empty($arSection["UF_PARENT"])){
                foreach ($arSection["UF_PARENT"] as $parent){
                    $arParentSections[$parent] = $parent;

                    if($parent != $arSection["ID"]){
                        $arChieldSections[$parent][$arSection["ID"]] = $arSection;
                    }

                }
            }
        }




        $this->EndResultCache();
    }
}

if($currentSection["ID"] > 0){
    unset($arResult["SECTIONS"]);

    foreach($arChieldSections[$currentSection["ID"]] as $arSection)
    {
        if($arSection["DEPTH_LEVEL"] == $currentSection["CHILDS_LEVEL"]){
            recursiveMenuItems($arSection, $arParentSections, $arChieldSections, $currentSection["CHILDS_LEVEL"]);
        }
    }
}else{
    $currentSection["DEPTH_LEVEL"] = 1;
    $currentSection["END_LEVEL"] = $arParams["DEPTH_LEVEL"];

    foreach($arResult["SECTIONS"] as $arSection)
    {
        if($arSection["DEPTH_LEVEL"] == $currentSection["DEPTH_LEVEL"]) {
            recursiveMenuItems($arSection, $arParentSections, $arChieldSections, $currentSection["DEPTH_LEVEL"]);
        }
    }

    unset($arResult["SECTIONS"]);
}

/**
 * Recursive Helper for all Menu Items
 * @param $arSection
 * @param $arParentSections
 * @param $arChieldSections
 * @param $DEPTH_LEVEL
 * @param int $parentID
 * @param int $granyID
 * @return bool
 */
function recursiveMenuItems($arSection, $arParentSections, $arChieldSections, $DEPTH_LEVEL, $parentID=0, $granyID=0){

    global $aMenuLinksNew, $currentSection;

    if($DEPTH_LEVEL > $currentSection["END_LEVEL"]){
        return false;
    }

    $params = [
        "FROM_IBLOCK" => true,
        "IS_PARENT" => false,
        "DEPTH_LEVEL" => $DEPTH_LEVEL,
        "ID" => $arSection["ID"]
    ];

    // Проверяем есть ли этот раздел в массиве родителей
    if($arParentSections[$arSection["ID"]] > 0)
    {
        // Корректированный массив потомков
        unset($arChieldSections[$arSection["ID"]][$arSection["ID"]]);
        unset($arChieldSections[$arSection["ID"]][$parentID]);
        unset($arChieldSections[$arSection["ID"]][$granyID]);
        // Корректированный массив родителей

        // Проверка корректированного массива детей
        if(!empty($arChieldSections[$arSection["ID"]])){

            $params["IS_PARENT"] = true;
        }

    }

    if($DEPTH_LEVEL >= $currentSection["END_LEVEL"]){
        $params["IS_PARENT"] = false;
    }


    // Сначала выводим раздел актуального уровня
    $aMenuLinksNew[] = [
        //htmlspecialcharsbx($arSection["~NAME"]) . "IS_PARENT = " . ($params["IS_PARENT"] ? "Y" : "N") . "; DEPTH_LEVEL = " . $DEPTH_LEVEL . "; parent Id = " . $parentID . "; granyID  = " . $granyID . "; current Id = " . $arSection["ID"],
        htmlspecialcharsbx($arSection["~NAME"]),
        $arSection["~SECTION_PAGE_URL"],
        false,
        $params
    ];


    // если раздел родительский - то дополняем родителя детьми из массива детей
    if($params["IS_PARENT"]){

        $granyID = $parentID;
        $parentID = $arSection["ID"];
        $DEPTH_LEVEL++;

        foreach($arChieldSections[$parentID] as $arSection){
            unset($arParentSections[$parentID]);
            recursiveMenuItems($arSection, $arParentSections, $arChieldSections, $DEPTH_LEVEL, $parentID, $granyID);
        }


    }

}

return $aMenuLinksNew;