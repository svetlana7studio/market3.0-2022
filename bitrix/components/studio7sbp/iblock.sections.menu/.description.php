<?
/**
 * iblock_sections_menu
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "iblock_sections_menu",
    "DESCRIPTION" => "iblock_sections_menu",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "iblock_sections_menu",
            "NAME" => "iblock_sections_menu"
        )
    ),
);