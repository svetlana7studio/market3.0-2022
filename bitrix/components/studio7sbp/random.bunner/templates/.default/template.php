<?
/**
 * @var array $arParams
 * @var array $arResult
 */

if(is_array($arResult["DETAIL_PICTURE"])):
?>
<div class="text-center">
    <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
         width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
         height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
         title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
         alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>" />

</div>
<?
endif;
?>