<?
/**
 * random_bunner
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "random_bunner",
    "DESCRIPTION" => "random_bunner",
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "studio7sbp",
        "CHILD" => array(
            "ID" => "random_bunner",
            "NAME" => "random_bunner"
        )
    ),
);