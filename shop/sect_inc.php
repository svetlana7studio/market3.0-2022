<?
global $APPLICATION;

$APPLICATION->ShowViewContent('left_menu_shop');
$APPLICATION->ShowViewContent('catalog_filter_shop');

$shopID = 0;
$shops = explode("/", $APPLICATION->GetCurDir());
foreach ($shops as $shop){
    if($shop > 0)
        $shopID = $shop;
    if($shopID > 0)
        break;
}

$APPLICATION->IncludeComponent(
    "studio7sbp:shop.user.message",
    "",
    array(
        "CURRENT_PAGE" => $APPLICATION->GetCurPage(),
        "IBLOCK_ID" => "1",
        "ELEMENT_ID" => $shopID,
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",

    ),
    false,
    array("HIDE_ICONS" => "Y")
);
?>