<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О сервисе");
?>
    <div class="s7spb-container py4">

        <div class="p3">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "",
                Array(
                    "PATH" => "",
                    "SITE_ID" => SITE_ID,
                    "START_FROM" => "0"
                )
            );?>
        </div>

        <div class="bg-white p4 py3 mb3">
            <h1>О сервисе</h1>

            <p>Наш сервис сделан для вашего удобства и объединяет на своей площадке разных поставщиков (вендоров) для формирования каталога товаров и оптимального выбора.</p>
            <p>Ассортимент нашего Маркетплейса более 10 000 товаров разных направлений.</p>
            
        </div>


        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            Array(
                "AREA_FILE_RECURSIVE" => "Y",
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "",
                "CART" => $componentElementParams["CART"],
                "COMPONENT_TEMPLATE" => ".default",
                "EDIT_TEMPLATE" => "standard.php",
                "PATH" => SITE_DIR."include/top.user.view.products.php"
            ),
            false,
            Array(
                'HIDE_ICONS' => 'Y'
            )
        );?>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>