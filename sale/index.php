<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Скидки");
$GLOBALS["arrFilterSales"][">PROPERTY_H_PRICE_DISCOUNT"] = 0;
$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "catalog.search.form",
    [
        "COMPONENT_TEMPLATE" => "cataloglevel",
        "ROOT_MENU_TYPE" => "catalog.level_1",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "36000000",
        "MENU_CACHE_USE_GROUPS" => "N",
        "MENU_CACHE_GET_VARS" => [],
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "catalog.level_1",
        "USE_EXT" => "Y",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N"
    ],
    false,
    ["HIDE_ICONS" => "Y"]
); 
?>

	<div class="s7spb-container py4">
		<div class="p3">
			<?$APPLICATION->IncludeComponent(
				"bitrix:breadcrumb",
				"",
				Array(
					"PATH" => "",
					"SITE_ID" => SITE_ID,
					"START_FROM" => "0"
				)
			);?>
		</div>
		<div class="bg-white p4 py3 mb3 sale--items">
			<h1>Скидки</h1>

			<?$APPLICATION->IncludeComponent(
				"bitrix:catalog.top",
				"tiles",
				Array(
					"ACTION_VARIABLE" => "action",
					"ADD_PROPERTIES_TO_BASKET" => "N",
					"ADD_TO_BASKET_ACTION" => "ADD",
					"BASKET_URL" => "/basket/",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
					"COMPATIBLE_MODE" => "Y",
					"CONVERT_CURRENCY" => "N",
					"DETAIL_URL" => "",
					"DISPLAY_COMPARE" => "N",
					"ELEMENT_COUNT" => "50",
					"ELEMENT_SORT_FIELD" => "sort",
					"ELEMENT_SORT_FIELD2" => "id",
					"ELEMENT_SORT_ORDER" => "asc",
					"ELEMENT_SORT_ORDER2" => "desc",
					"ENLARGE_PRODUCT" => "STRICT",
					"FILTER_NAME" => "arrFilterSales",
					"HIDE_NOT_AVAILABLE" => "N",
					"HIDE_NOT_AVAILABLE_OFFERS" => "N",
					"IBLOCK_ID" => "2",
					"IBLOCK_TYPE" => "marketplace",
					"LINE_ELEMENT_COUNT" => "5",
					"MESS_BTN_ADD_TO_BASKET" => "В корзину",
					"MESS_BTN_BUY" => "Купить",
					"MESS_BTN_COMPARE" => "Сравнить",
					"MESS_BTN_DETAIL" => "Подробнее",
					"MESS_NOT_AVAILABLE" => "Нет в наличии",
					"MESS_BLOCK_LABEL" => "Скидки",
					"BLOCK_LABEL_TYPE" => "NEW",
					"OFFERS_LIMIT" => "5",
					"PARTIAL_PRODUCT_PROPERTIES" => "N",
					"PRICE_CODE" => array("BASE"),
					"PRICE_VAT_INCLUDE" => "Y",
					"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
					"PRODUCT_ID_VARIABLE" => "id",
					"PRODUCT_PROPERTIES" => array(""),
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'5','BIG_DATA':false},{'VARIANT':'5','BIG_DATA':false},{'VARIANT':'5','BIG_DATA':false}]",
					"PRODUCT_SUBSCRIPTION" => "Y",
					"PROPERTY_CODE" => array("",""),
					"ROTATE_TIMER" => "30",
					"SECTION_URL" => "",
					"SEF_MODE" => "N",
					"SHOW_CLOSE_POPUP" => "N",
					"SHOW_DISCOUNT_PERCENT" => "Y",
					"SHOW_MAX_QUANTITY" => "N",
					"SHOW_OLD_PRICE" => "Y",
					"SHOW_PAGINATION" => "Y",
					"SHOW_PRICE_COUNT" => "1",
					"SHOW_SLIDER" => "N",
					"SLIDER_INTERVAL" => "3000",
					"SLIDER_PROGRESS" => "N",
					"TEMPLATE_THEME" => "blue",
					"USE_ENHANCED_ECOMMERCE" => "N",
					"USE_PRICE_COUNT" => "N",
					"USE_PRODUCT_QUANTITY" => "Y",
					"VIEW_MODE" => "SECTION"
				)
			);?>

		</div>


		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_RECURSIVE" => "Y",
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "",
				"CART" => $componentElementParams["CART"],
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "standard.php",
				"PATH" => SITE_DIR."include/top.user.view.products.php"
			),
			false,
			Array(
				'HIDE_ICONS' => 'Y'
			)
		);?>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>