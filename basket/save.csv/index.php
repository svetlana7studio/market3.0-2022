<?
/**
 * Search by article
 * for exaple 066077
 */

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-Disposition: attachment; filename="Список товаров.csv";');
Loader::includeModule("iblock");

$arParams = [
    "PRODUCTS" => [],
    "QUANTITY" => []
];
$arResult = [];

$request = Application::getInstance()->getContext()->getRequest();
$products = $request->get("products");
$products = explode(",", $products);

if(!empty($products)){
    foreach ($products as $product){
        $product = explode(":", $product);
        if(intval($product[0]) > 0){
            $arParams["PRODUCTS"][] = intval($product[0]);
            $arParams["QUANTITY"][intval($product[0])] = intval($product[1]);
        }
    }
}

$elements = CIBlockElement::GetList(
    [],
    [
        "IBLOCK_ID" => 2,
        "ID" => $arParams["PRODUCTS"],
        "ACTIVE" => "Y",
        "!PROPERTY_ARTICLE" => false
    ],
    false,
    ["nTopCount" => 100],
    [
        "ID",
        "IBLOCK_ID",
        "PROPERTY_ARTICLE",
        "PROPERTY_MOQ"
    ]
);
while($element = $elements->Fetch())
{
    $arResult[] = $element;
}

if(!empty($arResult)){

    echo utf8win1251("Артикул;Количество");
    echo "\r\n";

    $quantity = 1;
    foreach ($arResult as $element){
        $quantity = $arParams["QUANTITY"][$element["ID"]];
        if($element["PROPERTY_MOQ_VALUE"] > 1){
            $quantity = $element["PROPERTY_MOQ_VALUE"];
        }
        if(is_array($element["PROPERTY_ARTICLE_VALUE"]))
            $element["PROPERTY_ARTICLE_VALUE"] = $element["PROPERTY_ARTICLE_VALUE"][0];
        echo utf8win1251($element["PROPERTY_ARTICLE_VALUE"]);
        echo ";";
        echo $quantity;
        echo "\r\n";
    }

}