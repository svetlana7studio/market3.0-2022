<?
use Bitrix\Main\Application,
    Bitrix\Main\Loader;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$request = Application::getInstance()->getContext()->getRequest();
Loader::includeModule("sale");
Loader::includeModule("iblock");

$article = $request->get("article");
$quantity = $request->get("quantity");

/**
 * Get iblock element by article
 */
if(empty($article)){
    /**
     * Error
     * There is no article
     */
}else{
    $element = CIBlockElement::GetList(
        [],
        [
            "IBLOCK_ID" => 2,
            "PROPERTY_ARTICLE" => $article,
            "ACTIVE" => "Y"
        ],
        false,
        ["nTopCount" => 1],
        [
            "ID",
            "PROPERTY_MOQ",
            "PROPERTY_MULTIPLICITY"
        ]
    );
    if($element = $element->Fetch()){
        // corect quantity
        $quantity = intval($quantity);
        if($quantity < 1)
            $quantity = 1;
        // MOQ
        if($quantity < $element["PROPERTY_MOQ_VALUE"])
            $quantity = $element["PROPERTY_MOQ_VALUE"];
        // MULTIPLICITY
        if($quantity < $element["PROPERTY_MULTIPLICITY_VALUE"]){
            //    $quantity = $element["PROPERTY_MULTIPLICITY_VALUE"];

            $calculate = $quantity%$element["PROPERTY_MULTIPLICITY_VALUE"];
            if($calculate > 0){
                $quantity = $quantity - $calculate;
                $quantity += $element["PROPERTY_MULTIPLICITY_VALUE"];
            }

        }

        Add2BasketByProductID($element["ID"], $quantity, [], []);
        ?>
        <li class="ml5">В корзину добавлен товар <b><?=$article?></b>, в количестве: <b><?=$quantity?> шт</b></li>
        <?
    }else{
        ?>
        <li class="ml5">Товар с артиклом <b><?=$article?></b>, не найден шт</b></li>
        <?
    }
}

