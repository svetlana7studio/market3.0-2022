<?
use Bitrix\Main\Application,
    Bitrix\Main\IO\File,
    Bitrix\Main\Web\Json;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$request = Application::getInstance()->getContext()->getRequest();



$arResult = [
    "ERROR" => [],
    "ARTICLES" => []
];

if($request->isPost()){
    $file = $request->getFile("file");
    $file = $file["tmp_name"];
    if(empty($file)){

    }else{


        $file = File::getFileContents($file);

        //$text = mb_convert_encoding($text, 'UTF-8', 'AUTO');
        //$text = mb_convert_encoding($text, 'UTF-8', 'ANSI');
        //$text = mb_convert_encoding($text, 'UTF-8', 'WINDOWS-1251');
        $file = iconv('WINDOWS-1251', 'UTF-8', $file);

        $fileItems = [];
        $lines = explode(PHP_EOL, $file);
        foreach ($lines as $line) {
            $fileItems[] = str_getcsv($line);
        }
        unset($fileItems[0]);


        if(empty($fileItems)){

        }else{
            $articles = [];
            foreach ($fileItems as $article){
                $article = $article[0];
                $article = explode(";", $article);

                if(!empty($article[0])) {
                    $arResult["ARTICLES"][] = [
                        "ARTICLE" => $article[0],
                        "QUANTITY" => $article[1]
                    ];
                }
            }
        }

    }


}

$arResult = Json::encode($arResult);
echo $arResult;