<?
/**
 * Search by article
 * for exaple 066077
 */

use Bitrix\Main\Loader;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-Disposition: attachment; filename="Пример файла.csv";');
Loader::includeModule("iblock");
$arResult = [];

$elements = CIBlockElement::GetList(
    ["rand" => "asc"],
    [
        "IBLOCK_ID" => 2,
        "ACTIVE" => "Y",
        "!PROPERTY_ARTICLE" => false
    ],
    false,
    ["nTopCount" => 10],
    [
        "ID",
        "IBLOCK_ID",
        "PROPERTY_ARTICLE",
        "PROPERTY_MOQ"
    ]
);
while($element = $elements->Fetch())
{
    //// Fill article from id
    ///
    /// CIBlockElement::SetPropertyValues($element["ID"], $element["IBLOCK_ID"], $element["ID"], "ARTICLE");


    $arResult[] = $element;
}

if(!empty($arResult)){

    echo utf8win1251("Артикул;Количество");
    echo "\r\n";

    $quantity = 1;
    foreach ($arResult as $element){
        if($element["PROPERTY_MOQ_VALUE"] > 1){
            $quantity = $element["PROPERTY_MOQ_VALUE"];
        }
        echo $element["PROPERTY_ARTICLE_VALUE"];
        echo ";";
        echo $quantity;
        echo "\r\n";
    }

}