<?
/**
 * @var CMain $APPLICATION
 */

use Bitrix\Main\Application,
    Bitrix\Main\Loader;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
Loader::includeModule("sale");
Loader::includeModule("catalog");
$request = Application::getInstance()->getContext()->getRequest();

if(empty($request->get("products"))){

}else{


    $dbBasketItems = \CSaleBasket::GetList(
        [],
        [
            "FUSER_ID" => \CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ],
        false,
        false,
        ["ID"]
    );
    while ($dbBasketItem = $dbBasketItems->Fetch())
    {
        \CSaleBasket::Delete($dbBasketItem["ID"]);
    }

    // Add
    $products = $request->get("products");
    $products = explode(",", $products);

    if(!empty($products)){
        foreach ($products as $product){
            $product = explode(":", $product);
            if(intval($product[0]) > 0){
                Add2BasketByProductID($product[0], $product[1], [], []);
            }
        }
    }
}

LocalRedirect("/basket/");