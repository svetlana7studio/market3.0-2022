<?
/**
 * @var CMain $APPLICATION
 */

use Bitrix\Main\Application;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$file = Application::getInstance()->getContext()->getRequest()->getFile("file");
?>

<div class="s7spb-container py4">

    <div class="bg-white border-radius-10 p4 py3">
        <div class="text-24 text-uppercase pb1">Добавление из файла</div>

        <a href="/system/cart/sample.csv" class="pb1 d-block">Скачать пример файла:</a>

        <?$APPLICATION->IncludeComponent(
            "studio7sbp:sale.basket.add.from.file",
                "",
                [
                    "BASKET_URL" => "/basket/",
                    "IBLOCK_ID" => 2,
                    "FILE" => $file
                ],
            ["HIDE_ICONS" => "Y"]
        )?>

    </div>

</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>