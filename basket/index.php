<?
/**
 * @var CMain $APPLICATION
 */

$modeExcel = false;
if(!empty($_REQUEST["mode"]))
    $modeExcel = $_REQUEST["mode"] == "ex";
$template = "bootstrap.4";

if($modeExcel){
    // change template
    $template = "excel";

    // check mbstring.func_overload
    if (ini_get('mbstring.func_overload') & 2) {
        ini_set("mbstring.func_overload", 0);
    }

    define("NO_KEEP_STATISTIC", true);
    define("NOT_CHECK_PERMISSIONS",true);
    define("BX_CRONTAB", true);
    define('BX_WITH_ON_AFTER_EPILOG', true);
    define('BX_NO_ACCELERATOR_RESET', true);

    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    header("Content-Type: application/vnd.ms-excel");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: filename=Ваша корзина.xlsx");
    header('Cache-Control: max-age=0'); //no cache

    require $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/excel/git/PHPExcel/Classes/PHPExcel.php';
}
else{
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Ваша корзина");
    $APPLICATION->IncludeComponent(
        "bitrix:menu",
        "catalog.search.form",
        [
            "COMPONENT_TEMPLATE" => "cataloglevel",
            "ROOT_MENU_TYPE" => "catalog.level_1",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "36000000",
            "MENU_CACHE_USE_GROUPS" => "N",
            "MENU_CACHE_GET_VARS" => [],
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "catalog.level_1",
            "USE_EXT" => "Y",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "N"
        ],
        false,
        ["HIDE_ICONS" => "Y"]
    );
    CJSCore::Init(["jquery"]);
}



?>

<div class="s7spb-container py3">

	<div class="s7spb-row pb3">

		<div class="s7spb-col">

            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "",
                [
                    "PATH" => "",
                    "SITE_ID" => SITE_ID,
                    "START_FROM" => "0"
                ],
                false,
                ["HIDE_ICONS" => "Y"]
            );?>

        </div>

	</div>

	<div class="s7spb-row">

		<div class="s7spb-col">

            <?
            if($modeExcel)
                $APPLICATION->RestartBuffer();
            $APPLICATION->IncludeComponent(
                "bitrix:sale.basket.basket",
                $template,
                array(
                    "COLUMNS_LIST" => array(
                        0 => "NAME",
                        1 => "DISCOUNT",
                        2 => "PROPS",
                        3 => "DELETE",
                        4 => "DELAY",
                        5 => "TYPE",
                        6 => "PRICE",
                        7 => "QUANTITY",
                        8 => "SUM",
                    ),
                    "OFFERS_PROPS" => array(
                        0 => "SIZES",
                        1 => "COLOR_REF",
                    ),
                    "PATH_TO_ORDER" => SITE_DIR."order/",
                    "PATH_TO_BASKET" => SITE_DIR."basket/",
                    "HIDE_COUPON" => "N",
                    "PRICE_VAT_SHOW_VALUE" => "N",
                    "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                    "USE_PREPAYMENT" => "N",
                    "SET_TITLE" => "N",
                    "AJAX_MODE_CUSTOM" => "Y",
                    "SERVICE_ELECTRIC_IBLOCK" => "22",
                    "SERVICE_GLOBAL_IBLOCK" => "24",
                    "SHOW_MEASURE" => "Y",
                    "PICTURE_WIDTH" => "100",
                    "PICTURE_HEIGHT" => "100",
                    "SHOW_FULL_ORDER_BUTTON" => "Y",
                    "SHOW_FAST_ORDER_BUTTON" => "Y",
                    "COMPONENT_TEMPLATE" => ".default",
                    "QUANTITY_FLOAT" => "N",
                    "ACTION_VARIABLE" => "action",
                    "TEMPLATE_THEME" => "blue",
                    "AUTO_CALCULATION" => "Y",
                    "COMPOSITE_FRAME_MODE" => "A",
                    "COMPOSITE_FRAME_TYPE" => "AUTO",
                    "USE_GIFTS" => "Y",
                    "GIFTS_PLACE" => "BOTTOM",
                    "GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
                    "GIFTS_HIDE_BLOCK_TITLE" => "N",
                    "GIFTS_TEXT_LABEL_GIFT" => "Подарок",
                    "GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
                    "GIFTS_SHOW_OLD_PRICE" => "Y",
                    "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
                    "GIFTS_SHOW_NAME" => "Y",
                    "GIFTS_SHOW_IMAGE" => "Y",
                    "GIFTS_MESS_BTN_BUY" => "Выбрать",
                    "GIFTS_MESS_BTN_DETAIL" => "Подробнее",
                    "GIFTS_PAGE_ELEMENT_COUNT" => "10",
                    "GIFTS_CONVERT_CURRENCY" => "N",
                    "GIFTS_HIDE_NOT_AVAILABLE" => "N",
                    "DEFERRED_REFRESH" => "N",
                    "USE_DYNAMIC_SCROLL" => "Y",
                    "SHOW_FILTER" => "N",
                    "SHOW_RESTORE" => "Y",
                    "COLUMNS_LIST_EXT" => array(
                        0 => "PREVIEW_PICTURE",
                        1 => "DISCOUNT",
                        2 => "PROPS",
                        3 => "DELETE",
                        4 => "DELAY",
                        5 => "SUM",
                        6 => "PROPERTY_STRANA",
                        7 => "PROPERTY_PROIZVODITEL",
                        8 => "PROPERTY_OBYEM_ML",
                    ),
                    "COLUMNS_LIST_MOBILE" => array(
                        0 => "PREVIEW_PICTURE",
                        1 => "DISCOUNT",
                        2 => "DELETE",
                        3 => "DELAY",
                        4 => "SUM",
                    ),
                    "TOTAL_BLOCK_DISPLAY" => array(
                        0 => "bottom",
                    ),
                    "DISPLAY_MODE" => "extended",
                    "PRICE_DISPLAY_MODE" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "Y",
                    "DISCOUNT_PERCENT_POSITION" => "bottom-right",
                    "PRODUCT_BLOCKS_ORDER" => "props,sku,columns",
                    "USE_PRICE_ANIMATION" => "Y",
                    "LABEL_PROP" => array(
                    ),
                    "CORRECT_RATIO" => "Y",
                    "COMPATIBLE_MODE" => "Y",
                    "EMPTY_BASKET_HINT_PATH" => "/",
                    "ADDITIONAL_PICT_PROP_34" => "-",
                    "ADDITIONAL_PICT_PROP_35" => "-",
                    "BASKET_IMAGES_SCALING" => "adaptive",
                    "USE_ENHANCED_ECOMMERCE" => "N"
                ),
                false,
                ["HIDE_ICONS" => "Y"]
            );
            if($modeExcel)
                die();
            ?>

        </div>

	</div>

</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>