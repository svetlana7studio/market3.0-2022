<?
/**
 * Search by article
 * for exaple 066077
 */

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Bitrix\Main\Web\Json;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$request = Application::getInstance()->getContext()->getRequest();

$arParams = [
    "ARTICLE" => $request->get("article"),
    "IBLOCK_ID" => 2
];

if(empty($arParams["ARTICLE"])){
    die;
}else{
    $tmpArticles = $arParams["ARTICLE"];
    $arParams["ARTICLE"] = [];
    $canAddToBasket = false;
    foreach ($tmpArticles as $tmpArticle){
        $tmpArticle = trim($tmpArticle);
        $tmpArticle = htmlspecialcharsbx($tmpArticle);
        if(strlen($tmpArticle) > 2){
            $canAddToBasket = true;
        }
        $arParams["ARTICLE"][] = $tmpArticle;
    }
    unset($tmpArticles, $tmpArticle);

    $arResult = [];

    if(
        Loader::includeModule("iblock") &&
        Loader::includeModule("sale")
    ){
        $elements = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "PROPERTY_ARTICLE" => $arParams["ARTICLE"],
                "ACTIVE" => "Y"
            ],
            false,
            ["nTopCount" => 24],
            [
                "ID",
                "NAME",
                "PROPERTY_MOQ",
                "PROPERTY_MULTIPLICITY"
            ]
        );
        $productID = [];
        while($element = $elements->Fetch()){
            $arResult[$element["ID"]] = $element;
            $productID[$element["ID"]] = $element["ID"];
        }

        if(!empty($productID)){
            $dbBasketItems = \CSaleBasket::GetList(
                [],
                [
                    "FUSER_ID" => \CSaleBasket::GetBasketUserID(),
                    "LID" => SITE_ID,
                    "ORDER_ID" => "NULL"
                ],
                false,
                false,
                [
                    "ID", "PRODUCT_ID", "QUANTITY", "DELAY"
                ]
            );
            // обновить те что есть в корзине
            $quantity = 1;
            while ($dbBasketItem = $dbBasketItems->Fetch()){

                if($productID[$dbBasketItem["PRODUCT_ID"]] > 0) {

                    $quantity = $dbBasketItem["QUANTITY"];

                    if($quantity < $arResult[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_MOQ_VALUE"])
                        $quantity = $arResult[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_MOQ_VALUE"];

                    if($arResult[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_MULTIPLICITY_VALUE"] > 1)
                        $quantity += $arResult[$dbBasketItem["PRODUCT_ID"]]["PROPERTY_MULTIPLICITY_VALUE"];
                    else
                        $quantity += 1;

                    if($quantity < 1){
                        $quantity = 1;
                    }

                    \CSaleBasket::Update($dbBasketItem["ID"], ["QUANTITY" => $quantity]);

                    unset($productID[$dbBasketItem["PRODUCT_ID"]]);
                }

            }

            // Если после обновления остались товары - добавим их в корзину c учётом MOQ
            if(!empty($productID)){
                $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), SITE_ID);
                $quantity = 1;
                foreach ($productID as $product){
                    $quantity = $arResult[$product]["PROPERTY_MOQ_VALUE"];
                    if($quantity < 1){
                        $quantity = 1;
                    }
                    Add2BasketByProductID($product, $quantity, [], []);
                }
            }

        }

        if(is_array($arResult)){
            $arResult = Json::encode([
                "COUNT" => count($arResult),
                "ITEMS" => $arResult
            ]);
            echo $arResult;
        }

    }
}