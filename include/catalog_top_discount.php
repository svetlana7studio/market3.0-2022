<?
/**
 * @var CMain $APPLICATION
 */

$APPLICATION->IncludeComponent("studio7sbp:bestseller.list", "", [
    "IBLOCK_TYPE" => "marketplace",
    "IBLOCK_ID" => "2",
    "TP_IBLOCK_TYPE" => "marketplace",
    "TP_IBLOCK_ID" => "3",
    "DAY_DISCONT_COUNT" => "3",
    "PRICE_CODE" => [
        1, 2
    ],
    "RESIZE" => [
        "width" => 300,
        "height" => 300
    ],
],
    false,
    ["HIDE_ICONS" => "Y"]);