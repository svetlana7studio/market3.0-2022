<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.products.viewed",
    "slider",
    array(
        "COMPONENT_TEMPLATE" => "slider",
        "IBLOCK_MODE" => "single",
        "IBLOCK_TYPE" => "marketplace",
        "IBLOCK_ID" => "2",
        "SHOW_FROM_SECTION" => "N",
        "SECTION_ID" => $GLOBALS["CATALOG_CURRENT_SECTION_ID"],
        "SECTION_CODE" => "",
        "SECTION_ELEMENT_ID" => $GLOBALS["CATALOG_CURRENT_ELEMENT_ID"],
        "SECTION_ELEMENT_CODE" => "",
        "DEPTH" => "2",
        "HIDE_NOT_AVAILABLE" => "N",
        "HIDE_NOT_AVAILABLE_OFFERS" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "CACHE_GROUPS" => "Y",
        "ACTION_VARIABLE" => "action_cpv",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRICE_CODE" => array(
            0 => "price_red",
            1 => "price_yellow",
            2 => "price_green",
        ),
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "CONVERT_CURRENCY" => "N",
        "BASKET_URL" => "/basket/",
        "USE_PRODUCT_QUANTITY" => "Y",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "DISPLAY_COMPARE" => "N",
        "PROPERTY_CODE_2" => array(
            0 => "ARTICLE",
            1 => "MOQ",
        ),
        "CART_PROPERTIES_2" => array(
            0 => "",
            1 => "",
        ),
        "ADDITIONAL_PICT_PROP_2" => "-",
        "LABEL_PROP_2" => array(
        ),
        "PROPERTY_CODE_3" => array(
            0 => "",
            1 => "",
        ),
        "CART_PROPERTIES_3" => array(
            0 => "",
            1 => "",
        ),
        "ADDITIONAL_PICT_PROP_3" => "-",
        "OFFER_TREE_PROPS_3" => array(
        ),
        "PAGE_ELEMENT_COUNT" => "24"
    ),
    false
);?>