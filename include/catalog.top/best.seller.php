<?
global $USER_FAVORITES;
if(!empty($GLOBALS)){
    $GLOBALS["arrFilterBestseller"]["!PROPERTY_IS_BESTSELLER"] = false;
}
?>
<div class="bg-white position-relative bg-warning">
    <div class="spb7-title bg-primary-infras text-white ml5">БЕСТСЕЛЛЕРЫ</div>
    <div>
	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.top",
		"best.seller.slider",
		Array(
            "USER_ID" => $USER->GetID(),
            "CART" => $arParams["CART"],
			"USER_FAVORITES" => $USER_FAVORITES,
			"ACTION_VARIABLE" => "action",
			"ADD_PROPERTIES_TO_BASKET" => "N",
			"ADD_TO_BASKET_ACTION" => "ADD",
			"BASKET_URL" => "/basket/",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
			"COMPATIBLE_MODE" => "Y",
			"CONVERT_CURRENCY" => "N",
			"DETAIL_URL" => "",
			"DISPLAY_COMPARE" => "N",
			"ELEMENT_COUNT" => "10",
			"ELEMENT_SORT_FIELD" => "sort",
			"ELEMENT_SORT_FIELD2" => "id",
			"ELEMENT_SORT_ORDER" => "asc",
			"ELEMENT_SORT_ORDER2" => "desc",
			"ENLARGE_PRODUCT" => "STRICT",
			"FILTER_NAME" => "arrFilterBestseller",
			"HIDE_NOT_AVAILABLE" => "N",
			"HIDE_NOT_AVAILABLE_OFFERS" => "N",
			"IBLOCK_ID" => "2",
			"IBLOCK_TYPE" => "marketplace",
			"LINE_ELEMENT_COUNT" => "5",
			"MESS_BTN_ADD_TO_BASKET" => "В корзину",
			"MESS_BTN_BUY" => "Купить",
			"MESS_BTN_COMPARE" => "Сравнить",
			"MESS_BTN_DETAIL" => "Подробнее",
			"MESS_NOT_AVAILABLE" => "Зарегистрируйтесь, чтобы увидеть цены",
			"MESS_BLOCK_LABEL" => "Бестселлеры",
			"BLOCK_LABEL_TYPE" => "BESTSELLERS",
			"OFFERS_LIMIT" => "5",
			"PARTIAL_PRODUCT_PROPERTIES" => "N",
			"PRICE_CODE" => ["BASE"],
			"PRICE_VAT_INCLUDE" => "Y",
			"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
			"PRODUCT_ID_VARIABLE" => "id",
			"PRODUCT_PROPERTIES" => array(""),
			"PRODUCT_PROPS_VARIABLE" => "prop",
			"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'5','BIG_DATA':false},{'VARIANT':'5','BIG_DATA':false},{'VARIANT':'5','BIG_DATA':false}]",
			"PRODUCT_SUBSCRIPTION" => "Y",
			"PROPERTY_CODE" => array("MOQ",""),
			"ROTATE_TIMER" => "30",
			"SECTION_URL" => "",
			"SEF_MODE" => "N",
			"SHOW_CLOSE_POPUP" => "N",
			"SHOW_DISCOUNT_PERCENT" => "N",
			"SHOW_MAX_QUANTITY" => "N",
			"SHOW_OLD_PRICE" => "N",
			"SHOW_PAGINATION" => "Y",
			"SHOW_PRICE_COUNT" => "1",
			"SHOW_SLIDER" => "N",
			"SLIDER_INTERVAL" => "3000",
			"SLIDER_PROGRESS" => "N",
			"TEMPLATE_THEME" => "blue",
			"USE_ENHANCED_ECOMMERCE" => "N",
			"USE_PRICE_COUNT" => "N",
			"USE_PRODUCT_QUANTITY" => "Y",
			"VIEW_MODE" => "SECTION"
		)
	);?>
    </div>
</div>
