<div class="s7sbp--marketplace--footer--left--logo">
    <a href="/">
        <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/logo-footer.svg" alt="">
    </a>
</div>
<div class="s7sbp--marketplace--footer--copy">© <?=date("Y")?></div>
<div class="s7sbp--marketplace--footer--email">Email: <a href="mailto:mailto:info@lansiworld.com:">info@lansiworld.com</a></div>
<div class="s7sbp--marketplace--footer--link"><a href="/license/">Пользовательское соглашение</a></div>