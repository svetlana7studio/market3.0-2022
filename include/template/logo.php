<?
/**
 * @var array $arParams
 */
?>
<a href="<?=$arParams["LINK"]["HREF"]?>" class="<?=$arParams["LINK"]["CLASS"]?> logo-web-header">
    <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/logo-header.svg"
         width="244"
         title="..."
         alt="..." />
</a>
<a href="<?=$arParams["LINK"]["HREF"]?>" class="mob-logo-header">
    <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/logo-footer.svg"
         width="244"
         title="..."
         alt="..." />
</a>
