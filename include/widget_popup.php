<?
use Bitrix\Main\Application;
$arResult = [
    "IS_AJAX" => false
];
$request = Application::getInstance()->getContext()->getRequest();
/**
 * @var CMain $APPLICATION
 */
$JS_OBJ = "wPopup";

if(
    $request->isPost() &&
    $request->getPost("WEB_FORM_ID") == 1 &&
    $request->getPost("mode") == "wPopup"
)
{
    $arResult["IS_AJAX"] = true;
}

?>
<div class="widget-popup--wrapper" id="<?=$JS_OBJ?>-popup" onclick="<?=$JS_OBJ?>.closeByOverlay(event)">
    <div class="widget-popup">
        <button class="widget-popup--close" onclick="<?=$JS_OBJ?>.close()"><span class="widget-popup--close--span"></span></button>
        <div class="widget-popup--body">

            <?if($arResult["IS_AJAX"]){
                $APPLICATION->RestartBuffer();
            }?>
            <!-- start popup body -->
            <div class="widget-popup--body--title">Заказать обратный звонок</div>
            <div class="widget-popup--body--form">
                <?
                /*
                $APPLICATION->IncludeComponent("studio7sbp:form.result.new", "popup", Array(
                    "FORM_NOTE_ADDOK" => "Ваша заявка принята. Мы с вами свяжемся в ближайшее время.",
                    "PAGE" => $APPLICATION->GetCurPage(),
                    "JS_OBJ" => $JS_OBJ,
                    "COMPONENT_TEMPLATE" => "popup",
                    //"AJAX_MODE" => "Y",
                    "WEB_FORM_ID" => "1",	// ID веб-формы
                    "IGNORE_CUSTOM_TEMPLATE" => "Y",	// Игнорировать свой шаблон
                    "USE_EXTENDED_ERRORS" => "N",	// Использовать расширенный вывод сообщений об ошибках
                    "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                    "CACHE_TYPE" => "A",	// Тип кеширования
                    "CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
                    "COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
                    "LIST_URL" => "",	// Страница со списком результатов
                    "EDIT_URL" => "",	// Страница редактирования результата
                    "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
                    "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
                    "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
                    "VARIABLE_ALIASES" => array(
                        "WEB_FORM_ID" => "WEB_FORM_ID",
                        "RESULT_ID" => "RESULT_ID",
                    )
                ),
                    false,
                    array("HIDE_ICONS" => "Y")
                );
                */
                ?>
                <?$APPLICATION->IncludeComponent(
                    "studio7sbp:main.feedback",
                    "popup",
                    array(
                        "COMPONENT_TEMPLATE" => "popup",
                        "USE_CAPTCHA" => "Y",
                        "OK_TEXT" => "Ваша заявка принята. Мы с вами свяжемся в ближайшее время.",
                        "EMAIL_TO" => "info@anytos.ru",
                        "REQUIRED_FIELDS" => array(
                            0 => "NAME",
                            1 => "PHONE",
                        ),
                        "EVENT_MESSAGE_ID" => array(
                        ),
                        "AJAX_MODE" => "Y",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => ""
                    ),
                    false
                );
                ?>
            </div>
            <!-- end popup body -->
            <?if($arResult["IS_AJAX"]){
                die;
            }?>

        </div>
    </div>
</div>