<div class="s7spb-row bg-white advent-items">

    <div class="s7spb-col-20">
        <div class="s7spb-row s7spb-row-center p3">
            <div class="s7spb-col-4">
                <span class="s7sbp--marketplace--index--rewards--item--icon gifts"></span>
            </div>
            <div class="s7spb-col-8">
                <div class="pl2">Подарочные сертификаты</div>
            </div>
        </div>
    </div>

    <div class="s7spb-col-20">
        <div class="s7spb-row s7spb-row-center p3">
            <div class="s7spb-col-4">
                <span class="s7sbp--marketplace--index--rewards--item--icon store-verify"></span>
            </div>
            <div class="s7spb-col-8">
                <div class="pl2">Все продавцы проверены</div>
            </div>
        </div>
    </div>

    <div class="s7spb-col-20">
        <div class="s7spb-row s7spb-row-center p3">
            <div class="s7spb-col-4">
                <span class="s7sbp--marketplace--index--rewards--item--icon support"></span>
            </div>
            <div class="s7spb-col-8">
                <div class="pl2">Круглосуточная поддержка</div>
            </div>
        </div>
    </div>

    <div class="s7spb-col-20">
        <div class="s7spb-row s7spb-row-center p3">
            <div class="s7spb-col-4">
                <span class="s7sbp--marketplace--index--rewards--item--icon delivery"></span>
            </div>
            <div class="s7spb-col-8">
                <div class="pl2">Удобная доставка</div>
            </div>
        </div>
    </div>

    <div class="s7spb-col-20">
        <div class="s7spb-row s7spb-row-center p3">
            <div class="s7spb-col-4">
                <span class="s7sbp--marketplace--index--rewards--item--icon best-price"></span>
            </div>
            <div class="s7spb-col-8">
                <div class="pl2">Лучшие цены</div>
            </div>
        </div>
    </div>

</div>