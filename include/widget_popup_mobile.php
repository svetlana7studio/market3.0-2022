<div class="widget-popup--wrapper" id="mobile-widget-popup">
    <div class="widget-popup">
        <button class="widget-popup--close"><span class="widget-popup--close--span"></span></button>
        <div class="widget-popup--body">
            <div class="widget-popup--body--title">Заказать обратный звонок</div>
            <div class="widget-popup--body--title--sub"><a href="tel:+78126047003">8888888</a></div>
            <br>
            <div class="widget-popup--body--title">Или записывайтесь на</div>
            <div class="widget-popup--body--title--sub">Бесплатную консультацию</div>
            <div class="widget-popup--body--form">
                <div class="widget-popup--body--form--field">
                    <input type="tel" name="phone" class="widget-popup--phone-mask" placeholder="Номер телефона">
                </div>
                <div class="widget-popup--body--form--field">
                    <button class="widget-popup--body--form--field--btn js--send-consult">Отправить</button>
                </div>
            </div>
            <div class="widget-popup--body--inform">
                Нажимая на кнопку "Отправить", вы принимаете условия <a href="#">Публичной оферты</a>
            </div>
        </div>
    </div>
</div>