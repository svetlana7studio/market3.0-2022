<div class="text-30 mb2">Anytos.ru</div>
                <div class="mb3 text-16">
                    <div class="mb1">&copy; 2012-2020</div>
                    <div class="mb1">Все права защищены</div>
                    <div class="mb1">Пользовательское соглашение</div>
                </div>
                <div class="text-30 mb2">Контакты</div>
                <div class="text-16">
                    <a href="tel:8-800-555-05-84" class="d-block mb2 text-white text-decoration-none">8-800-555-05-84</a>
                    <a href="tel:+7(495) 646-05-84" class="d-block mb2 text-white text-decoration-none">+7(495) 646-05-84</a>
                    <a href="mailto:info@anytos.ru" class="d-block mb2 text-white text-decoration-none">info@anytos.ru</a>
                </div>