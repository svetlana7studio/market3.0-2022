<?
/**
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Page\Asset;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$moduleId = 'studio7spb.marketplace';
$moduleJsId = str_replace('.', '_', $moduleId);
$pathJS = '/bitrix/js/'.$moduleId;
$pathCSS = '/bitrix/css/'.$moduleId;

$arParams = [
    'js' => $pathJS.'/products.snippet.js',
    'css' => $pathCSS.'/products.snippet.css'
];

Asset::getInstance()->addCss($arParams["css"]);
Asset::getInstance()->addJs($arParams["js"]);

d($pathJS);
d($arParams);

$list_id = "system_grids";
$ui_filter = [
    [
        "id" => "ID",
        "name" => "ID",
        "default" => true,
        "type" => "string"
    ],
    [
        "id" => "SORT",
        "name" => "SORT",
        "default" => true,
        "type" => "string"
    ],
    [
        "id" => "NAME",
        "name" => "NAME",
        "default" => true,
        "type" => "string"
    ],
];

$columns = [
    [
        "id" => "ID",
        "name" => "ID",
        "sort" => "ID",
        "default" => true
    ],
    [
        "id" => "NAME",
        "name" => "NAME",
        "sort" => "NAME",
        "default" => true
    ],
    [
        "id" => "SORT",
        "name" => "SORT",
        "sort" => "SORT",
        "default" => true
    ]
];
$list = [];

// <editor-fold defaultstate="collapsed" desc="Panel">
ob_start();
$APPLICATION->IncludeComponent('bitrix:main.ui.filter', '', [
    'FILTER_ID' => $list_id,
    'GRID_ID' => $list_id,
    'FILTER' => $ui_filter,
    'ENABLE_LIVE_SEARCH' => true,
    'ENABLE_LABEL' => true
]);

$filterLayout = ob_get_clean();

$APPLICATION->IncludeComponent("bitrix:sender.ui.panel.title", "", array('LIST' => array(
    array('type' => 'filter', 'content' => $filterLayout),
    array('type' => 'buttons', 'list' => [
        [
            'type' => 'add',
            'id' => 'SYNONYM_BUTTON_ADD',
            'caption' => "Добавить синоним",
            //'href' => $arParams['PATH_TO_ADD']
            'href' => "koorochka_synonym_edit.php",

        ],
        [
            'type' => 'add',
            'id' => 'SYNONYM_BUTTON_IMPORT',
            'caption' => "Импортировать синонимы",
            //'href' => $arParams['PATH_TO_ADD']
            'href' => "koorochka_synonym_import.php"

        ],
        [
            'type' => 'add',
            'id' => 'SYNONYM_BUTTON_EXPORT',
            'caption' => "Экспортировать синонимы",
            //'href' => $arParams['PATH_TO_ADD']
            'href' => "koorochka_synonym_export.php"

        ],
    ]),
)));

// </editor-fold>
$APPLICATION->IncludeComponent('bitrix:main.ui.grid', '', [
    'GRID_ID' => $list_id,
    'COLUMNS' => $columns,
    'ROWS' => $list,
    'SHOW_ROW_CHECKBOXES' => false,
    'AJAX_MODE' => 'Y',
    'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
    'PAGE_SIZES' =>  [
        ['NAME' => '5', 'VALUE' => '5'],
        ['NAME' => '10', 'VALUE' => '10'],
        ['NAME' => '20', 'VALUE' => '20'],
        ['NAME' => '50', 'VALUE' => '30'],
        ['NAME' => '40', 'VALUE' => '40'],
        ['NAME' => '50', 'VALUE' => '50']
    ],
    'AJAX_OPTION_JUMP'          => 'N',
    'SHOW_CHECK_ALL_CHECKBOXES' => false,
    'SHOW_ROW_ACTIONS_MENU'     => true,
    'SHOW_GRID_SETTINGS_MENU'   => true,
    'SHOW_NAVIGATION_PANEL'     => true,
    'SHOW_PAGINATION'           => true,
    'SHOW_SELECTED_COUNTER'     => true,
    'SHOW_TOTAL_COUNTER'        => true,
    'SHOW_PAGESIZE'             => true,
    'SHOW_ACTION_PANEL'         => true,
    'ALLOW_COLUMNS_SORT'        => true,
    'ALLOW_COLUMNS_RESIZE'      => true,
    'ALLOW_HORIZONTAL_SCROLL'   => true,
    'ALLOW_SORT'                => true,
    'ALLOW_PIN_HEADER'          => true,
    'AJAX_OPTION_HISTORY'       => 'N'
]);
?>

<button onclick="return BX.Sender.Page.open('sender.open.php')">button</button>

<div class="alert alert-danger">
    BX.Bitrix24

    <ol>
        <li>https://estrin.pw/bitrix-d7-snippets/s/uskorenie-otklyuchaem-js-biblioteku-bitriksa/</li>
        <li>https://dev.1c-bitrix.ru/community/blogs/hazz/jscore-register-extention.php</li>
    </ol>
</div>

<h3>Создание своей JS библиотеки: JS, CSS, Фразы, Зависимости.</h3>
    Регистрация вашего скрипта в JS Core, например в include.php модуля:
<div class="alert alert-danger">
    CJSCore::RegisterExt('your_js', array(<br>
    'js' => '/bitrix/js/your_module/functions.js',<br>
    'css' => '/bitrix/js/your_module/css/functions.css',<br>
    'lang' => '/bitrix/modules/your_module/lang/'.LANGUAGE_ID.'/functions_js.php',<br>
    'rel' => array('popup', 'ajax', 'fx', 'ls', 'date', 'json')<br>
    ));
</div>
<div>
    <h3>Ключи:</h3>
    js - путь до вашего JS скрипта<br>
    css - путь до вашего CSS файла (не обязательный параметр)<br>
    lang - путь до вашего языкового файла с учетом языковой константы (не обязательный параметр)<br>
    rel - зависимости, здесь перечисляются какие библиотеки нужны например jquery или json, список можно посмотреть в файле /bitrix/modules/main/jscore.php (не обязательный параметр)<br>
    skip_core - если у вас нет зависимостей и bitrix core вам не нужен, смело ставьте true (не обязательный параметр)
</div>
    Инициализация вашего скрипта, в компоненте:
    CJSCore::Init(array('your_js'));

    При отработке компонента, будут подключены все необходимые файлы и зависимости.

    Языковые файлы, составляются в привычном для битрикс-компонентов формате:

    $MESS["YM_TEXT_1"] = "Текст 1";
    $MESS["YM_TEXT_2"] = "Текст 2";

    Для вывода фразы в JS нужно использовать функцию BX.message, например:
    BX.message('YM_TEXT_1');


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");