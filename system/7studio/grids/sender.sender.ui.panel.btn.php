<?
/**
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Page\Asset,
    Bitrix\Main\Grid\Panel\Actions;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->IncludeComponent("bitrix:sender.ui.panel.title", "", array('LIST' => array(

    array('type' => 'buttons', 'list' => [
        [
            'type' => 'add',
            'id' => 'SOME_BTN',
            'caption' => "Some button",
            'href' => ''

        ],

    ]),
)));

?>

    <button onclick="return BX.Sender.Page.open('sender.open.php')">button</button>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");