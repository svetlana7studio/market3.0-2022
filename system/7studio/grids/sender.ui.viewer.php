<?
/**
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Page\Asset,
    Bitrix\Main\Grid\Panel\Actions;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

CJSCore::Init([ 'ui.viewer' ]);
?>


    <button
            class="ui-btn ui-btn-link"
            data-viewer="null"
            data-object-id="1234"
            data-viewer-type="document"
            data-src="/disk/downloadFile/1234/?&ncc=1&filename=ИмяФайла.pdf"
            data-title="ИмяФайла.pdf"
            data-actions='[{"type":"download"}]'>ОТКРЫТЬ ФАЙЛ</button>




<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");