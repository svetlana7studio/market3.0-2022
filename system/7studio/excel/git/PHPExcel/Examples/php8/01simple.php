<?


require_once dirname(__FILE__) . '/../../Classes/PHPExcel.php';
# require_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/kda.importexcel/lib/PHPExcel/PHPExcel.php';

// KDAPHPExcel
echo date('H:i:s') , " Create new PHPExcel object";
$objPHPExcel = new PHPExcel(); // new KDAPHPExcel() new PHPExcel()
# $objPHPExcel = new KDAPHPExcel(); // new KDAPHPExcel() new PHPExcel()

// Set document properties
echo date('H:i:s') , " Set document properties";
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("PHPExcel Test Document")
    ->setSubject("PHPExcel Test Document")
    ->setDescription("Test document for PHPExcel, generated using PHP classes.")
    ->setKeywords("office PHPExcel php")
    ->setCategory("Test result file");