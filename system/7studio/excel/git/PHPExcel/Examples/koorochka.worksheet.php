<?
/**
 * https://github.com/PHPOffice/PHPExcel/blob/develop/Documentation/markdown/Overview/04-Configuration-Settings.md
 * system/7studio/excel/git/PHPExcel/Examples/24readfilter.php
 */
if (ini_get('mbstring.func_overload') & 2) {
    ini_set("mbstring.func_overload", 0);
}

global $objReader;

$arParams = array();
$arResult = array();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';
$arResult = array(
    "FILE" => $_SERVER["DOCUMENT_ROOT"] . "/upload/excel/sample/test1.xls"
);

class MyReadFilter implements PHPExcel_Reader_IReadFilter
{
    public function readCell($column, $row, $worksheetName = '') {
        // Read title row and rows 20 - 30
        if ($row == 1 || ($row >= 20 && $row <= 30)) {
            return true;
        }

        return false;
    }
}


$objReader = PHPExcel_IOFactory::createReaderForFile($arResult["FILE"]);
$objReader->setReadFilter( new MyReadFilter() );



$sheetnames = array('Data Sheet #1','Data Sheet #3');

$objReader->setLoadSheetsOnly($sheetnames);
/**  Load $inputFileName to a PHPExcel Object  **/

//$objPHPExcel = $objReader->load($arResult["FILE"]);

set_time_limit(0);

function barber($type)
{
    global $objReader;
    $objPHPExcel = $objReader->load($type);
    d("You wanted a $type haircut, no problem");
}
//call_user_func('barber', "mushroom");
//call_user_func('barber', $arResult["FILE"]);


$output = shell_exec('php ' . $_SERVER["DOCUMENT_ROOT"] . '/system/7studio/shell/index.php');
echo "<pre>$output</pre>";