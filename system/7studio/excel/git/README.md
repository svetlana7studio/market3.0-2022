# PHPExcel
PHPExcel adaptet to php8 and bitrix-framework

Create a new repository on the command line
```
git init
git add README.md
git commit -m "first commit"
git branch -M phpexcel
git remote add origin https://github.com/7StudioSpb/market3.0-2022.git
git push -u origin phpexcel
```

Push an existing repository from the command line

```
git remote add origin https://github.com/7StudioSpb/market3.0-2022.git
git branch -M phpexcel
git push -u origin phpexcel
```

```
git remote add origin https://github.com/7StudioSpb/market3.0-2022.git
git branch -M phpexcel
git pull origin phpexcel
```

```
git rm -rf about
git commit -m "Delete about folder"
git push origin phpexcel
```