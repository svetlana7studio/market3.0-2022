/**
 * @type {{options: {}, url: string, open: sidePanelInstance.open}}
 */
var sidePanelInstance = {
    url: "/system/7studio/ui/side.panel/side.panel.open.php",
    options: {},
    open: function (){
        BX.SidePanel.Instance.open(this.url, this.options);
    }
};