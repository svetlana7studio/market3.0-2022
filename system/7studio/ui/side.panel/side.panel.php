<?
/**
 * BX.SidePanel
 */
use Bitrix\Main\Page\Asset;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
\CJSCore::init("sidepanel");
Asset::getInstance()->addJs("/system/7studio/ui/side.panel/js/side.panel.js");
?>

<h1>BX.SidePanel.Instance</h1>
<p>Класс - основной программный интерфейс для работы со слайдером. Является объектом-синглтоном.</p>
    https://dev.1c-bitrix.ru/api_help/js_lib/sidepanel/sidepanel_instance.php

    system/7studio/ui/side.panel.open.php

<ul>
    <li><button onclick="sidePanelInstance.open()">BX.SidePanel.Instance.open</button></li>
</ul>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>