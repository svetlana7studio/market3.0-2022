<?

use Bitrix\Sale,
    Bitrix\Main\Loader;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule("sale");
Loader::includeModule("iblock");

$arResult = [
    "BASKET_ITEMS" => [],
    "PRODUCTS" => [],
    "ARTICLES" => [],
    "ID" => []
];

/**
 * получить для текущего юзера
 */
$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

foreach ($basket as $basketItem) {
    $arResult["ID"][] = $basketItem->getField('PRODUCT_ID');
}

$elements = CIBlockElement::GetList(
    [],
    [
        "IBLOCK_ID" => 2
    ],
    false,
    false, //["nTopCount" => 24],
    [
        "ID",
        "NAME",
        "PROPERTY_ARTICLE"
    ]
);
while ($element = $elements->Fetch()){
    $arResult["PRODUCTS"][$element["ID"]] = $element;
    $arResult["ARTICLES"][$element["ID"]] = $element["PROPERTY_ARTICLE_VALUE"];
}

$i = 0;
foreach ($basket as $basketItem) {
    $i++;

    echo "#" .$i . " "
        .$basketItem->getField('PRODUCT_ID') . ' - '
        . $basketItem->getQuantity()

        . " Article: " . $arResult["PRODUCTS"][$basketItem->getField('PRODUCT_ID')]["PROPERTY_ARTICLE_VALUE"]

        . '<br />';
}
