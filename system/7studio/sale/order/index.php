<?
/**
 * Заказ (Sale\Order)
 * https://mrcappuccino.ru/blog/post/work-with-order-bitrix-d7
 */
use Bitrix\Sale,
    Bitrix\Main\Loader;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
Loader::includeModule("sale");

$order = 49118;
$order = Sale\Order::load($order);
# $order = Sale\Order::loadByAccountNumber($orderNumber);

# Поля заказа можно получить короткими вызовами:
/**
$order->getId(); // ID заказа
$order->getSiteId(); // ID сайта
$order->getDateInsert(); // объект Bitrix\Main\Type\DateTime
$order->getPersonTypeId(); // ID типа покупателя
$order->getUserId(); // ID пользователя

$order->getPrice(); // Сумма заказа
$order->getDiscountPrice(); // Размер скидки
$order->getDeliveryPrice(); // Стоимость доставки
$order->getSumPaid(); // Оплаченная сумма
$order->getCurrency(); // Валюта заказа

$order->isPaid(); // true, если оплачен
$order->isAllowDelivery(); // true, если разрешена доставка
$order->isShipped(); // true, если отправлен
$order->isCanceled(); // true, если отменен
 */

d($order->getId());
d($order->getPersonTypeId());
d($order->getUserId());
d($order->getSiteId());


/** @var \Bitrix\Sale\PropertyValueCollection $propertyCollection */
if($propertyCollection = $order->getPropertyCollection()) {

    foreach($propertyCollection as $orderProperty) {

        switch ($orderProperty->getField('CODE')){
            case "STRAHOVKA":
                    d($orderProperty->getValue());
                break;
            case "COMMENTS":
                    d($orderProperty->getValue());
                break;
            case "LOCATION":
                    d($orderProperty->getViewHtml());
                break;
        }

    }
}

