<?

/**
 * https://www.php.net/manual/ru/function.usort.php
 * @var CMain $APPLICATION
 */

use Bitrix\Sale\Order,
    Bitrix\Sale\Payment,
    Bitrix\Main\Loader;

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->RestartBuffer();
Loader::includeModule("sale");

$arr = array(
    array('id'=>1, 'age'=>1, 'sex'=>6, 'name'=>'a'),
    array('id'=>2, 'age'=>3, 'sex'=>1, 'name'=>'c'),
    array('id'=>3, 'age'=>3, 'sex'=>1, 'name'=>'b'),
    array('id'=>4, 'age'=>2, 'sex'=>1, 'name'=>'d'),
);

d(arrayOrderBy($arr, 'age asc,sex asc,name desc'));

function arrayOrderBy(array &$arr, $order = null) {
    if (is_null($order)) {
        return $arr;
    }
    $orders = explode(',', $order);
    usort($arr, function($a, $b) use($orders) {
        $result = array();
        foreach ($orders as $value) {
            list($field, $sort) = array_map('trim', explode(' ', trim($value)));
            if (!(isset($a[$field]) && isset($b[$field]))) {
                continue;
            }
            if (strcasecmp($sort, 'desc') === 0) {
                $tmp = $a;
                $a = $b;
                $b = $tmp;
            }
            if (is_numeric($a[$field]) && is_numeric($b[$field]) ) {
                $result[] = $a[$field] - $b[$field];
            } else {
                $result[] = strcmp($a[$field], $b[$field]);
            }
        }
        return implode('', $result);
    });
    return $arr;
}


die;

$items = \Bitrix\Sale\Basket::getList(array(
    "filter" => [
        "FUSER_ID" => \CSaleBasket::GetBasketUserID(),
        "LID" => SITE_ID
    ],
    "select" => [
        "NAME",
        "ID",
        "PRODUCT_ID",
        "PRICE",
        "SUM",
        //"ARTICLE",
        "DATE_INSERT"
    ]
));
while ($item = $items->fetch()) {
    $arResult[] = $item;
}

/**
 * по дате добавления ↑ по новизне по цене ↓ по сумме по алфавиту по артикулу
 */

d($arResult);
