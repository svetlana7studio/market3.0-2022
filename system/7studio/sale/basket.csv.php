<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>


<form onsubmit="return sendCsv(this)"
      action="/basket/csv/get.file.php"
      method="post"
      enctype="multipart/form-data">

    <div class="alert py2 mb2 d-none"></div>
    <div class="form-group pb2">
        <lable class="pb1">Введите в поле товары каждый строки в формате CSV - Артикул;Количество:</lable>
        <a href="/system/cart/sample.csv" class="pb1 d-block">Пример файла:</a>
        <input type="file" id="file" name="file"></div>
    <input type="submit" class="btn btn-danger" value="Добавить товары в корзину">

</form>

    <ol id="result"></ol>

<style>
    #result{
        display: none;
        border: 1px solid;
        height: 150px;
        overflow: auto;
    }
</style>

<script>

    function sendCsv(t){
        $("#result").html(null);
        $.ajax({
            url: $(t).attr("action"),
            data: new FormData( t ),
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function(data){
                var i;
                if(!!data.ARTICLES && data.ARTICLES.length > 0){
                    $("#result").html('<li>Идёт процесс добавления</li>').show();
                    for(i=0; i < data.ARTICLES.length; i++){
                        addProductToBasketByArticle(data.ARTICLES[i]["ARTICLE"], data.ARTICLES[i]["QUANTITY"]);
                    }
                }

            }
        });

        return false;
    }

    function addProductToBasketByArticle(article, quantity) {

        $.get("/basket/csv/add_product_by_article.php", {
            article: article,
            quantity: quantity
        }, function (li) {
            $("#result").append(li);
        });


    }


</script>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>