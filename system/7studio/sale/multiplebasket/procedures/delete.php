<?
/**
 * @var CMain $APPLICATION
 */

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Studio7spb\Marketplace\MultipleBasketTable;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule("studio7spb.marketplace");
Loader::includeModule("sale");

$basket = Application::getInstance()->getContext()->getRequest()->get("basket");
$basket = intval($basket);
if($basket > 0){
    $basket = MultipleBasketTable::getList([
        "filter" => [
            "FUSER_ID" => \CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ID" => $basket
        ],
        "select" => ["ID"]
    ]);
    if($basket = $basket->fetch()){
        MultipleBasketTable::delete($basket["ID"]);
    }
}


// clear
/*
$arParams = array(
    "FUSER_ID" => \CSaleBasket::GetBasketUserID(),
    "LID" => SITE_ID,
    "ORDER_ID" => "NULL"
);
$basketItems = Basket::getList(array(
    "filter" => $arParams,
    "select" => array("ID")
));
$neo = new CSaleBasket();
while ($basketItem = $basketItems->fetch())
{
    $neo->Delete($basketItem["ID"]);
}
*/




LocalRedirect("/personal/baskets/");