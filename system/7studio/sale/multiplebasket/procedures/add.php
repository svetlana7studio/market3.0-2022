<?
/**
 * @var CMain $APPLICATION
 */

use Bitrix\Sale\Basket,
    Bitrix\Main\Loader,
    Bitrix\Main\Web\Json,
    Studio7spb\Marketplace\MultipleBasketTable;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule("sale");




/**
 * Get basket items
 */
$arParams = array(
    "FUSER_ID" => \CSaleBasket::GetBasketUserID(),
    "LID" => SITE_ID
);
$arResult = array(
    "ITEMS" => array(),
    "PRODUCTS" => array(),
    "PARAMS" => array(),
    //
    "BASKETS" => array(),
    "ALERT" => null
);

/**
 * Get Baskets
 */

$baskets = MultipleBasketTable::getList(array(
    "filter" => $arParams
));
if($baskets->getSelectedRowsCount() >= 5)
{
    $arResult["ALERT"] = array("type" => "danger", "text" => "Сохраненных корзин не может быть более пяти");
}
else{
    $arParams["ORDER_ID"] = "NULL";
    $basketItems = Basket::getList(array(
        "filter" => $arParams,
        /*
        "select" => array(
            "ID",
            "NAME",
            "FUSER_ID",
            "PRODUCT_ID",
            "PRICE",
            "BASE_PRICE",
            "PRODUCT_PRICE_ID",
            "CURRENCY",
            "QUANTITY",
        )
        */
    ));

    while ($basketItem = $basketItems->fetch())
    {
        $arResult["ITEMS"][] = $basketItem;
        $arResult["PRODUCTS"][] = $basketItem["PRODUCT_ID"];
        unset($basketItem["ID"]);
        $arResult["PARAMS"][$basketItem["PRODUCT_ID"]] = $basketItem;
    }

    if(empty($arResult["PRODUCTS"])) {
        $arResult["ALERT"] = array("type" => "danger", "text" => "Текущая корзина пуста, необходимо вначале в разделе <a href='/catalog.'>каталог</a> наполнить корзину товарами");
    }
    else{
        $arResult["PARAMS"] = serialize($arResult["PARAMS"]);
        $arResult["PRODUCTS"] = array(
            "LID" => SITE_ID,
            "FUSER_ID" => \CSaleBasket::GetBasketUserID(),
            "NAME" => htmlspecialcharsbx($_REQUEST["name"]),
            "PARAMS" => $arResult["PARAMS"]
        );

        # add to multiple basket

        $result =  MultipleBasketTable::add($arResult["PRODUCTS"]);

        if($result->isSuccess())
        {
            unset($arResult["PARAMS"]);
            unset($arResult["PRODUCTS"]);
            $neo = new CSaleBasket();

            foreach ($arResult["ITEMS"] as $basketItem){
                $neo->Delete($basketItem["ID"]);
            }


            // Корзина сохранена, перейти к ней вы можете в Личном кабинете по ссылке
            $arResult["ALERT"] = '<div>';
            $arResult["ALERT"] .= "Текущая корзина добавлена в список Моих корзин.<br>";
            $arResult["ALERT"] .= "Перейти к ней вы можете в Личном кабинете или по ";
            $arResult["ALERT"] .= "<a href='" . SITE_DIR . "personal/baskets/'>этой ссылке</a>";
            $arResult["ALERT"] .= '</div>';
            $arResult["ALERT"] = array(
                "type" => "success",
                "title" => "Корзина сохранена",
                "text" => $arResult["ALERT"]
            );
        }
        else{
            $arResult["ALERT"] = array("type" => "danger", "text" => implode(", ", $result->getErrorMessages()));
        }
    }
}

echo Json::encode($arResult["ALERT"]);