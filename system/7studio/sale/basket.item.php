<?

use Bitrix\Sale,
    Bitrix\Main\Loader;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule("sale");

$arResult = [
    "BASKET_ITEMS" => [],
    "PRODUCTS" => [],
    "ARTICLES" => [],
    "ID" => []
];

/**
 * получить для текущего юзера
 */
$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

foreach ($basket as $basketItem) {
    $arResult["ID"][] = $basketItem->getField('PRODUCT_ID');
}

Loader::includeModule("sale");

$fUserId = \Bitrix\Sale\Fuser::getId();

$arResult["BASKET_ITEMS"] = (int)\Bitrix\Sale\BasketComponentHelper::getFUserBasketQuantity($fUserId);

// output
d($arResult);