https://getcomposer.org/download/
https://getcomposer.org/doc/00-intro.md
https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&LESSON_ID=4637&LESSON_PATH=3913.3516.4776.2483.4637

   / ____/___  ____ ___  ____  ____  ________  _____
 / /   / __ \/ __ `__ \/ __ \/ __ \/ ___/ _ \/ ___/
/ /___/ /_/ / / / / / / /_/ / /_/ (__  )  __/ /
\____/\____/_/ /_/ /_/ .___/\____/____/\___/_/


Local variant
/usr/bin/composer

.
..

```phpt
php bitrix.php
```

Для удобства вы можете создать символическую ссылку без постфикса php:

```shell
$ chmod +x bitrix.php
$ ln -s bitrix.php bitrix
$ ./bitrix
```

./bitrix