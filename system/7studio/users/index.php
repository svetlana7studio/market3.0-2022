<?
/**
 * @global CMain $APPLICATION
 */
use Bitrix\Main\UserTable;


define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$arResult = [];

$users = \Bitrix\Main\UserTable::getList([
    "select" => [
        "ID",
        "LOGIN",
        "EMAIL"
    ]
]);
while ($user = $users->fetch())
{
    $arResult[]  = $user;
}

d($arResult);