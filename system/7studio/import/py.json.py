import xlrd
import sys
# xlrd

file = "synonym.xls"

workbook = xlrd.open_workbook(file)
worksheet = workbook.sheet_by_index(0)

start = 1
elementField = int(sys.argv[2])
synonymField = int(sys.argv[3])
json = ''

json += "["
for i in range(0, len(worksheet.col_values(elementField, start))):
    if worksheet.col_values(elementField,start)[i] != '':
        json += "{"
        json += '"element":"{}", "synonym":"{}"'.format(worksheet.col_values(elementField, start)[i],
                                                                     worksheet.col_values(synonymField, start)[i])
        json += "}"
        if i < (worksheet._dimnrows - 1):
            json += ","
json += "]"
print(json)