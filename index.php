<?
/**
 * @global CMain $APPLICATION
 */

use Bitrix\Main\Page\Asset;
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("Главная");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/marketplace.rewards.css");
?>

    <!--- Контейнер с двух колонок --->
    <div class="s7spb-container">

        <div class="s7spb-row s7spb-row12 mb3">

            <!--- Первая колонка --->
            <div class="s7spb-col s7spb-col-auto bg-white left-block">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "cataloglevel",
                    array(
                        "COMPONENT_TEMPLATE" => "cataloglevel",
                        "ROOT_MENU_TYPE" => "cataloglevel",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "36000000",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MAX_LEVEL" => "4",
                        "CHILD_MENU_TYPE" => "cataloglevel",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                );?>
            </div>

            <!--- Вторая колонка --->
            <div class="s7spb-col"><?$APPLICATION->IncludeComponent(
                    "bitrix:advertising.banner",
                    "unique1",
                    array(
                        "TYPE" => "main",
                        "NOINDEX" => "Y",
                        "QUANTITY" => "5",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "360000"
                    ),
                    false
                );
                ?>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => SITE_DIR."include/index.section_carusel.php",
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "",
                        "AREA_FILE_RECURSIVE" => "Y",
                        "EDIT_TEMPLATE" => "standard.php"
                    ),
                    false
                );?>

                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => SITE_DIR."include/index.rewards.php",
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "",
                        "AREA_FILE_RECURSIVE" => "Y",
                        "EDIT_TEMPLATE" => "standard.php"
                    ),
                    false
                );?>

            </div>

        </div>

        <div class="mb3 product-day">
        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "PATH" => SITE_DIR."include/catalog_top_discount.php",
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_TEMPLATE" => "standard.php"
            ),
            false
        );?>
        </div>

        <?$APPLICATION->IncludeComponent(
            "bitrix:advertising.banner",
            "two.banner.block",
            array(
                "COMPONENT_TEMPLATE" => "two.banner.block",
                "TYPE" => "two_banner_block",
                "NOINDEX" => "Y",
                "QUANTITY" => "2",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "360000"
            ),
            false
        );?>

        <div class="mb5">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "CART" => $componentElementParams["CART"],
                    "PATH" => SITE_DIR."include/catalog.top/recomend.php",
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_TEMPLATE" => "standard.php"
                ),
                false,
                array('HIDE_ICONS' => 'Y')
            );?>
        </div>

        <div class="mb5">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", "adv.register",
                array(
                    "COMPONENT_TEMPLATE" => "adv.register",
                    "PATH" => SITE_DIR . "include/index.banner.register.php",
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_TEMPLATE" => "standard.php"
                ),
                false,
                array('HIDE_ICONS' => 'Y')
            );?>
        </div>

        <div class="mb5 bg-white">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "CART" => $componentElementParams["CART"],
                    "PATH" => SITE_DIR."include/brands.list.php",
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_TEMPLATE" => "standard.php"
                ),
                false,
                array('HIDE_ICONS' => 'Y')
            );?>
        </div>

        <div class="mb5">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                array(
                    "COMPONENT_TEMPLATE" => "",
                    "PATH" => SITE_DIR . "include/catalog.top/new.php",
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_TEMPLATE" => "standard.php"
                ),
                false,
                array('HIDE_ICONS' => 'Y')
            );?>
        </div>

        <div class="mb5 sales-ban"><?$APPLICATION->IncludeComponent("bitrix:advertising.banner",
                    "unique2",
                    array(
                        "COMPONENT_TEMPLATE" => "anitos",
                        "TYPE" => "main_two",
                        "NOINDEX" => "Y",
                        "QUANTITY" => "5",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "360000"
                    ),
                    false
                );
                ?></div>

        <div class="mb5">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", "",
                array(
                    "COMPONENT_TEMPLATE" => "",
                    "PATH" => SITE_DIR . "include/catalog.top/best.seller.php",
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "",
                    "AREA_FILE_RECURSIVE" => "Y",
                    "EDIT_TEMPLATE" => "standard.php"
                ),
                false,
                array('HIDE_ICONS' => 'Y')
            );?>
        </div>
		<div class="mb5">
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"articles", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "/articles/index.php?ID=#ELEMENT_ID#",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "9",
		"IBLOCK_TYPE" => "marketplace",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "LINK",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "articles"
	),
	false
);?>
		</div>

    </div>

<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>