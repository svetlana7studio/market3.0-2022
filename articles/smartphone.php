<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Смартфоны");
?><div class="s7spb-container py4" style="font-size: 16px;">
	<div class="bg-white p4 py3 mb3 smartphone-page">
		<h1>Смартфоны по летним ценам</h1>
		<p>
			 Вечная борьба — iOS или Android. В этом стане ничего не поменялось, третьей конкурентоспособной операционной системы не предвидится. Что касается Google и Apple, то они давно копируют фишки друг у друга, поэтому интерфейсы и возможности устройств во многом схожи.
		</p>
		<p>
			 А вот сервисы и в целом экосистема различаются, и пользоваться iCloud с «андроида» не получится, а сервисы от Google не то чтобы гладко интегрированы в iOS. Выбирать стоит, скорее, по привычке: менять вашу текущую операционную систему вряд ли разумно. А если всё-таки соберётесь, то покупайте максимально новую и, в случае с Android, мощную модель смартфона, чтобы переход не стал разочарованием.
		</p>
		<h2>
		Дисплей </h2>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.element", 
	"single", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_DETAIL_TO_SLIDER" => "N",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_TO_BASKET_ACTION" => array(
			0 => "BUY",
			1 => "ADD",
		),
		"ADD_TO_BASKET_ACTION_PRIMARY" => array(
			0 => "BUY",
		),
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/basket/",
		"BRAND_USE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_SECTION_ID_VARIABLE" => "N",
		"COMPATIBLE_MODE" => "Y",
		"COMPONENT_TEMPLATE" => "single",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_PICTURE_MODE" => array(
			0 => "POPUP",
		),
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "254",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "marketplace",
		"IMAGE_RESOLUTION" => "16by9",
		"LABEL_PROP" => array(
			0 => "IS_NEW",
			1 => "IS_RECOMMEND",
			2 => "IS_BESTSELLER",
		),
		"LABEL_PROP_MOBILE" => array(
		),
		"LABEL_PROP_POSITION" => "top-left",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"MAIN_BLOCK_PROPERTY_CODE" => array(
		),
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_COMMENTS_TAB" => "Комментарии",
		"MESS_DESCRIPTION_TAB" => "Описание",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MESS_PRICE_RANGES_TITLE" => "Цены",
		"MESS_PROPERTIES_TAB" => "Характеристики",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "0",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
			1 => "FOB_LC_USD",
			2 => "normal_price",
			3 => "quickly_price",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
		"PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"SECTION_CODE" => "",
		"SECTION_CODE_PATH" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SEF_MODE" => "Y",
		"SEF_RULE" => "",
		"SET_BROWSER_TITLE" => "Y",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SET_VIEWED_IN_COMPONENT" => "N",
		"SHOW_404" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_COMMENTS" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_GIFTS_DETAIL" => "Y",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"USE_RATIO_IN_RANGES" => "N",
		"USE_VOTE_RATING" => "N"
	),
	false
);?>
		<p>
			 Главный элемент, с которым вы будете взаимодействовать, поэтому ему надо уделить особое внимание.
		</p>
		<p>
			 Гонка разрешений закончилась. Золотой стандарт — Full HD с приставкой «+», так как экраны стали более вытянутыми. FHD+ отлично смотрится на всех диагоналях, от 5 до 7 дюймов. Отдельные флагманские модели имеют разрешение больше, QHD+ и подобные форматы, но разительного преимущества в качестве картинки они не дают.
		</p>
		<h2>
		Тип матрицы </h2>
		<p>
			 На текущий момент есть 2 основных типа матриц — OLED/AMOLED и IPS. Экраны, выполненные по технологии TN, TFT и LTPS, встречаются совсем редко. Дисплеи на органических диодах OLED/AMOLED отличаются высокой яркостью, контрастностью, скоростью отрисовки, а также меньшим энергопотреблением. Из недостатков — неточная цветопередача у моделей попроще и ШИМ — экран начинает мерцать при понижении яркости, и это вызывает дискомфорт у некоторых пользователей. Матрицы IPS отличается более натуральной цветопередачей, они дешевле и долговечнее, но в 2021 году их удел — средний и нижний ценовой диапазон устройств. Флагманы с IPS можно пересчитать по пальцам одной руки. Все современные iPhone перешли на OLED и большинство устройств стоимостью от 30 000 рублей тоже.
		</p>
		<h2>
		Частота обновления </h2>
		<p>
			 Вокруг этого параметра разворачивается чуть ли не главная гонка характеристик последних двух лет. Стандартная частота обновления экрана — 60 Гц, то есть каждую секунду изображение на экране успевает поменяться 60 раз. Этого достаточно для комфортной работы с интерфейсом, просмотра видео и игр. Но если увеличить частоту обновления, то в динамичных сценах картинка становится плавнее. Например, шрифты и изображения остаются читаемыми при пролистывании сайтов и лент соцсетей. В играх с поддержкой этой технологии также можно получить преимущество.
		</p>
		
		<h2>
		Гибкие дисплеи </h2>
		<p>
			Гибкие дисплеи. В&nbsp;продаже вы можете встретить складные устройства с&nbsp;гибким дисплеем. Это такие очень дорогие технологичные игрушки для тех, кто может себе их позволить. Возможно, во второй половине года представят&nbsp;<nobr>что-то</nobr>&nbsp;более практичное и&nbsp;доступное. Подождём!
		</p>
		<h2>Процессор</h2>
		<p>
			 Есть несколько ведущих производителей процессоров&nbsp;— Apple, HiSilicon, MediaTek, Qualcomm, Samsung. Все они делают многоядерные процессоры, и&nbsp;количество ядер сегодня мало что решает. Лучше обратить внимание на тесты производительности в&nbsp;бенчмарках и&nbsp;в&nbsp;реальных задачах.
		</p>
		<p>
			 При прочих равных устройства на Qualcomm Snapdragon предпочтительнее любых других: они меньше греются, медленнее расходуют заряд батареи, лучше обрабатывают снимки, под них оптимизировано больше софта и&nbsp;игр. Самая мощная серия&nbsp;— восьмисотая, Snapdragon 8xx. Почти так&nbsp;же быстры чипсеты серии 7xx, а&nbsp;вот 6xx и&nbsp;4xx предназначены только для устройств среднего и&nbsp;бюджетного сегмента, они далеко не так хороши в&nbsp;играх, хотя с&nbsp;основными задачами справляются.
		</p>
<p>
			 Обратите внимание на данные модели:
		</p>
		<div class="bg-white position-relative bg-warning">
			 <!--div class="spb7-title bg-primary text-white ml5">Суперцены</div--> <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.top",
	"recomend.slider",
	Array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"BASKET_URL" => "/personal/basket.php",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPATIBLE_MODE" => "Y",
		"COMPONENT_TEMPLATE" => "recomend.slider",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[{\"CLASS_ID\":\"CondIBProp:2:235\",\"DATA\":{\"logic\":\"Equal\",\"value\":251}}]}",
		"DETAIL_URL" => "",
		"DISPLAY_COMPARE" => "N",
		"ELEMENT_COUNT" => "9",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "arrFilterNast",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "marketplace",
		"LABEL_PROP" => "",
		"LINE_ELEMENT_COUNT" => "3",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"OFFERS_LIMIT" => "5",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(0=>"BASE",1=>"FOB_LC_USD",2=>"normal_price",3=>"quickly_price",),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"ROTATE_TIMER" => "30",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PAGINATION" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"VIEW_MODE" => "SECTION"
	)
);?>
		</div>
		<p>
			 Самые производительные чипсеты Samsung&nbsp;— прошлогодний Exynos 990, а&nbsp;также новейшие 1080&nbsp;и&nbsp;2100. Схожей производительностью обладают HiSilicon Kirin 980, 985&nbsp;и&nbsp;990. Самые прогрессивные продукты MediaTek&nbsp;— чипы Dimensity 800&nbsp;и&nbsp;1000, но чаще можно встретить устройства на чипах серии Helio, относящихся к&nbsp;среднему сегменту.
		</p>
		<p>
			 Только в период проведения акции с 22 мая по 30 июня - самые лучшие цены на смартфоны!
		</p>
		<p>
			 В подарок - чехол на выбор!
		</p>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>