<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Карта сайта");
?>
<div class="s7spb-container pb5 mt5">
    <div class="bg-white p4 py3 mb3">
        <h1><?$APPLICATION->ShowTitle(false);?></h1>
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.map",
            "sitemap",
            array(
                "LEVEL" => "3",
                "COL_NUM" => "3",
                "SHOW_DESCRIPTION" => "N",
                "SET_TITLE" => "Y",
                "CACHE_TIME" => "36000000",
                "COMPONENT_TEMPLATE" => ".default",
                "CACHE_TYPE" => "A"
            ),
            false
        );?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>