<?
$aMenuLinksExt = array();

$elements = CIBlockElement::GetList(
    array(),
    array(
        "IBLOCK_TYPE" => "marketplace",	// Тип инфоблока
        "IBLOCK_ID" => "7",	// Инфоблок
        "ACTIVE" => "Y",
        "!NAME" => "Other"
    ),
    false,
    false,
    array(
        "ID",
        "NAME",
        "DETAIL_PAGE_URL"
    )
);

while ($element = $elements->GetNext())
{
    $aMenuLinksExt[] = array($element["NAME"], $element["DETAIL_PAGE_URL"]);
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>